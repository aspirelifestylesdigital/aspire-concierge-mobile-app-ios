//
//  UITest.swift
//  Mobile ConciergeUITests
//
//  Created by Dai on 7/9/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

import XCTest

class UITest: XCTestCase {
    
    let app = XCUIApplication()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = true

        app.launch()
        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // MARK: - TEST
    func testFlowVerfifyPasscodeAndSignin() {
        
        XCTAssertTrue(verifybin("6445785", isPass: true),"Verify bin failed with UI")
        
        XCTAssertTrue(forgotPassword(email: "1111", password: "12345", retypePassword: "1111", answer: "122"))
//        XCTAssertTrue(signInWith(username: "uda11@email.com", password: "Kakalot1989@", isPass: true))
        
    }
    
    // MARK: - FLOW
    func verifybin(_ bin:String,isPass:Bool) -> Bool{
        
        let programPasscodeSecureTextField = app.secureTextFields["Access Code"]
        programPasscodeSecureTextField.tap()
        programPasscodeSecureTextField.typeText(bin)
        app.buttons["SUBMIT"].tap()
        
        let predicate = NSPredicate(format: "exists == 1")
        
        let btn = app.buttons[isPass ? "SIGN IN" : "OK"]
        
        let expect = expectation(for: predicate, evaluatedWith: btn, handler: nil);
        
        let resultOK = XCTWaiter().wait(for: [expect], timeout: 10)
        
        return resultOK == .completed
    }
    
    func signInWith(username:String, password:String, isPass:Bool) -> Bool{
        let txtEmail = app.textFields["Email"]
        let txtPassword = app.secureTextFields["Password"]
        
        txtEmail.tap()
        txtEmail.typeText(username)
        
        txtPassword.tap()
        txtPassword.typeText(password)
        
        app.buttons["SIGN IN"].tap()
        
        let predicate = NSPredicate(format: isPass ? "exists == 0" : "exists == 1")
        
        let btn = app.buttons["SIGN IN"]
        
        let expect = expectation(for: predicate, evaluatedWith: btn, handler: nil);
        
        let resultOK = XCTWaiter().wait(for: [expect], timeout: 60)
        
        return resultOK == .completed
    }
    
    func forgotPassword(email:String,password:String,retypePassword:String,answer:String) ->Bool{
        app.buttons["Forgot Password?"].tap()
        
        let predicate = NSPredicate(format: "exists == 1")
        
        let btn = app.buttons["Forgot"]
        
        let expect = expectation(for: predicate, evaluatedWith: btn, handler: nil);
        
        let resultOK = XCTWaiter().wait(for: [expect], timeout: 60)
        
        return resultOK == .completed
    }
    // MARK: - PRIVATE
}
