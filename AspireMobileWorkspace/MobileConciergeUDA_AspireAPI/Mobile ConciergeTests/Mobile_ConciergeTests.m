//
//  Mobile_ConciergeTests.m
//  Mobile ConciergeTests
//
//  Created by 💀 on 5/28/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SyncService.h"
@import AspireApiFramework;
#import <AspireApiFramework/AspireAPIValidate.h>
@interface Mobile_ConciergeTests : XCTestCase

@end

@interface NSString (HTMLString)
- (NSString *)HTMLString;
@end

@implementation NSString (HTMLString)
- (NSString *)HTMLString {
    NSMutableString* temp = [NSMutableString stringWithString:@""];
    for (int i = 0; i < self.length; i++) {
        const unichar hs = [self characterAtIndex:i];
        // surrogate pair
        if (hs > 127) {
                [temp appendFormat:@"&#%d;",hs];
            } else {
                [temp appendFormat:@"%c",hs];
            }
        }
    return temp;
}
@end

@implementation Mobile_ConciergeTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void) testValidatefromSchema {
    NSString* test = @"Dr";
    
    XCTAssert([test isValidSalutation]);
}

- (void) testLogout {
    XCTestExpectation* expect = [self expectationWithDescription:@"open"];
    
    [ModelAspireApiManager logout];
    
    XCTAssert([ASPIREAPI_AccessToken user] == nil);
    XCTAssert([User current] == nil);
    XCTAssert([AccountAuthentication username] == nil);

    [expect fulfill];
    
    [self waitForExpectationsWithTimeout:60 handler:^(NSError * _Nullable error) {
        NSLog(@"TEST DONE");
    }];
}

- (void) testLogin {
    XCTestExpectation* expect = [self expectationWithDescription:@"open"];
    
    [ModelAspireApiManager loginWithUsername:@"phamdaiit@gmail.com" password:@"Kakalot1989@" completion:^(NSError *error) {
        XCTAssert(error == nil);
        
        NSLog(@"[RESULT] => %lu",(unsigned long)[AspireApiError getErrorTypeAPISignInFromError:error]);
        
        [expect fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:60 handler:^(NSError * _Nullable error) {
        NSLog(@"TEST DONE");
    }];
}

- (void) testCreateProfile {
    XCTestExpectation* expect = [self expectationWithDescription:@"open"];
    
    [ModelAspireApiManager createProfileWithUserInfo:@{
                                                       @"Car Type Preference" : @"none",
                                                       @"City" : @"unknown",
                                                       @"Country" : @"USA",
                                                       @"Dining Preference" : @"none",
                                                       @"Email" : @"uda2@email.com",
                                                       @"FirstName" : @"Hoang",
                                                       @"Hotel Preference" : @"none",
                                                       @"LastName" : @"Hoang",
                                                       @"MobileNumber" : @"10195813485",
                                                       @"Other Preferences" : @"789123",
                                                       @"Password" : @"Kakalot1989@",
                                                       @"Salutation" : @"Dr",
                                                       @"addressLine5" : @"YES",
                                                       @"referenceName" : @"UDA"
                                                       } completion:^(User*user,NSError *error) {
                                                           XCTAssert(error == nil);
                                                           
                                                           NSLog(@"[RESULT] => %lu",(unsigned long)[AspireApiError getErrorTypeAPISignupFromError:error]);
                                                           
                                                           [expect fulfill];
                                                       }];
    
    [self waitForExpectationsWithTimeout:60 handler:^(NSError * _Nullable error) {
        NSLog(@"TEST DONE");
    }];
}

- (void) testUpdateProfile {
    XCTestExpectation* expect = [self expectationWithDescription:@"open"];
    
    [ModelAspireApiManager updateProfileCurrentUserWithUserInfo:@{
                                                                  @"Car Type Preference" : @"car test99",
                                                                  @"City" : @"unknown",
                                                                  @"Country" : @"USA",
                                                                  @"Dining Preference" : @"dining test1",
                                                                  @"Email" : @"phamdaiit@gmail.com",
                                                                  @"FirstName" : @"Hoang",
                                                                  @"Hotel Preference" : @"hotel test",
                                                                  @"LastName" : @"Hoang",
                                                                  @"MobileNumber" : @"10195813485",
                                                                  @"Other Preferences" : @"789123",
                                                                  @"Password" : @"abcefgh",
                                                                  @"Salutation" : @"Dr",
                                                                  @"addressLine5" : @"NO",
                                                                  @"referenceName" : @"UDA"
                                                                  } completion:^(NSError *error) {
        XCTAssert(error == nil);
        
        NSLog(@"[RESULT] => %@",error.description);
        
        [expect fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:60 handler:^(NSError * _Nullable error) {
        NSLog(@"TEST DONE");
    }];
}

- (void)testRetrieveProfile {
    XCTestExpectation* expect = [self expectationWithDescription:@"open"];
    
    [ModelAspireApiManager retrieveProfileCurrentUser:^(User *user, NSError *error) {
        XCTAssert(error == nil);
        if (user) {
            NSLog(@"[RESULT] => %@",[user.locations firstObject].address.addressLine5);
        }        
        [expect fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:60 handler:^(NSError * _Nullable error) {
        NSLog(@"TEST DONE");
    }];
}

- (void) testCreateRequest {
    XCTestExpectation* expect = [self expectationWithDescription:@"open"];
    
    [ModelAspireApiManager createRequestWithUserInfo:@{
                                                       
                                                       @"requestCity" : @"N/A",
                                                       @"requestDetails" : @"Respond by hoangth1191@gmail.com\n\n\nWine\n1-877-Spirits\n",
                                                       @"requestType" : @"V AIRPORT TRANSFER"
                                                       
                                                       }
 completion:^(NSError *error) {
        XCTAssert(error == nil);
     if (error != nil)
         NSLog(@"[RESULT] => %lu",(unsigned long)[AspireApiError getErrorTypeAPICreateRequestFromError:error]);
        
        [expect fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:60 handler:^(NSError * _Nullable error) {
        NSLog(@"TEST DONE");
    }];
}

- (void) testChangePassword {
    
    XCTestExpectation* expect = [self expectationWithDescription:@"open"];
    
    [ModelAspireApiManager changePasswordWithNewPassword:@"Kakalot1989@4" andOldPassword:@"Kakalot1989@4"
                                              completion:^(NSError *error) {
                                              XCTAssert(error == nil);
                                              
                                                  NSLog(@"[RESULT] => %lu",(unsigned long)[AspireApiError getErrorTypeAPIChangePasswordFromError:error]);
                                              
                                              [expect fulfill];
                                          }];
    
    [self waitForExpectationsWithTimeout:60 handler:^(NSError * _Nullable error) {
        NSLog(@"TEST DONE");
    }];
}

- (void) testResetPassword {
    
    XCTestExpectation* expect = [self expectationWithDescription:@"open"];
    
    [ModelAspireApiManager resetPasswordForEmail:@"dai.pham.198100@gmail.com"
                                              completion:^(NSError *error) {
                                                  XCTAssert(error == nil);
                                                  
                                                  NSLog(@"[RESULT] => %lu",(unsigned long)[AspireApiError getErrorTypeAPIResetPasswordFromError:error]);
                                                  
                                                  [expect fulfill];
                                              }];
    
    [self waitForExpectationsWithTimeout:60 handler:^(NSError * _Nullable error) {
        NSLog(@"TEST DONE");
    }];
}

- (void) testHTMLEntities {
    NSString* htmlString = [@"Los Ángeles" HTMLString];
    NSData* stringData = [htmlString dataUsingEncoding:NSWindowsCP1250StringEncoding allowLossyConversion:true];
    NSDictionary* options = @{NSDocumentTypeDocumentAttribute:NSPlainTextDocumentType};
    NSAttributedString* decodedAttributedString = [[NSAttributedString alloc] initWithData:stringData options:options documentAttributes:NULL error:NULL];
    NSString* decodedString = [decodedAttributedString string];
    NSLog(@"RESULT => %@",htmlString);
    XCTAssert(true);
}
@end


