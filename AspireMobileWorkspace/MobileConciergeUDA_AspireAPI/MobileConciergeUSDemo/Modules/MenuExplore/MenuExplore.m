//
//  MenuExplore.m
//  Test
//
//  Created by 😱 on 7/14/17.
//  Copyright © 2017 dai.pham.s3corp.com.vn. All rights reserved.
//

#import "MenuExplore.h"

// style
#import "UtilStyle.h"
#import "Constant.h"
// extension
#import "UIView+Extension.h"

#pragma mark - 🚸 UIViewSeparateLineMenuExplore 🚸
@interface UIViewSeparateLineMenuExplore: UIView@end
@implementation UIViewSeparateLineMenuExplore
- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setbackgroundColorForLineMenu];
}
@end

#pragma mark - 🚸 UIButtonMenuExplore 🚸
@interface UIButtonMenuExplore:UIButton@end

@implementation UIButtonMenuExplore
- (void) awakeFromNib {
    [super awakeFromNib];
    
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTitleColor:[AppColor highlightButtonColor] forState:UIControlStateHighlighted];
    [self.titleLabel setFont:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionTextSize]]];
}

- (void) centerVertically
{
    //    self.titleLabel.numberOfLines = 1;
    //    self.titleLabel.adjustsFontSizeToFitWidth = YES;
    //    self.titleLabel.lineBreakMode = NSLineBreakByClipping;
    
    CGSize imageSize = self.imageView.frame.size;
    CGSize titleSize = self.titleLabel.frame.size;
    self.titleLabel.adjustsFontSizeToFitWidth = YES;
    CGFloat totalHeight = (imageSize.height + titleSize.height);
    
    self.imageEdgeInsets = UIEdgeInsetsMake(- (totalHeight - imageSize.height),
                                            0.0f,
                                            0.0f,
                                            - titleSize.width);
    
    self.titleEdgeInsets = UIEdgeInsetsMake(0.0f,
                                            - imageSize.width,
                                            - (totalHeight - titleSize.height),
                                            0.0f);
}

- (void) setTitle:(NSString *)title forState:(UIControlState)state {
    [super setTitle:title forState:state];
    [self centerVertically];
}

- (void) setImage:(UIImage *)image forState:(UIControlState)state {
    [super setImage:image forState:state];
    [self centerVertically];
}
@end

#pragma mark - 🚸 MenuExplore 🚸
@implementation MenuExplore

#pragma mark - INIT
- (id)init
{
    self = [super init];
    
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
    }
    return self;
}

- (void) awakeFromNib {
    [super awakeFromNib];
    
    // set Font, State for Label/Button
    for (UIView* view in @[_vwAction1,_vwAction2,_vwAction3]) {
        for (id control in view.subviews) {
            if([control isKindOfClass:[UIButton class]]) {
                UIButton* btn = (UIButton*)control;
                NSString* imgNormal = @"location_white_icon";
                NSString* imgHighlight = @"location_interaction_icon";
                NSString* title = @"City";
                
                if(btn.tag == 2) {
                    title = @"Category";
                    imgNormal = @"category_white_icon";
                    imgHighlight = @"category_interaction_icon";
                } else if (btn.tag == 3) {
                    title = @"Search";
                    imgNormal = @"search_white_icon";
                    imgHighlight = @"search_interaction_icon";
                }
                
                [btn setTitle:title forState:UIControlStateNormal];
                [btn setImage:[UIImage imageNamed:imgNormal] forState:UIControlStateNormal];
                [btn setImage:[UIImage imageNamed:imgHighlight] forState:UIControlStateHighlighted];
            }
        }
    }
    
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:100]];
}

#pragma mark - INTERFACE
- (void) setTitle:(NSString *)title forButtonAtIndex:(NSInteger)index {
    UIView* view = _vwAction1;
    if(index == 2) {
        view = _vwAction2;
    } else if(index == 3) {
        view = _vwAction3;
    }
    
    for (id control in view.subviews) {
        if([control isKindOfClass:[UIButton class]]) {
            UIButton* btn = (UIButton*)control;
            [btn setTitle:title forState:UIControlStateNormal];
        }
    }
}

- (void) setTitleForButtons:(NSArray *)title atIndexes:(NSArray*)indexes {
    if(title.count != indexes.count)
        return;
    NSInteger i = 0;
    for (NSString* index in indexes) {
        [self setTitle:[title objectAtIndex:i] forButtonAtIndex:[index integerValue]];
        i++;
    }
}

#pragma mark - ACTION
- (IBAction)actionAtIndex:(UIButton*)sender {
    if(self.delegate) {
        [self.delegate MenuExplore:self onSelectButton:sender atIndex:sender.tag];
    }
}

@end
