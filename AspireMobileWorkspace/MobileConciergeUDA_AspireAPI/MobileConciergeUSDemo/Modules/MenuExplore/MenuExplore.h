//
//  MenuExplore.h
//  Test
//
//  Created by 😱 on 7/14/17.
//  Copyright © 2017 dai.pham.s3corp.com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MenuExplore;

@protocol MenuExploreProtocol <NSObject>

@optional

/**
 get Button touched

 @param view MenuExplore
 @param button button touched
 @param index button index
 */
- (void) MenuExplore:(MenuExplore*)view onSelectButton:(UIButton*) button atIndex:(NSInteger)index;


@end

@interface MenuExplore : UIView

@property (weak,nonatomic) id<MenuExploreProtocol> delegate;

#pragma mark - View
@property (weak, nonatomic) IBOutlet UIView *vwAction1;
@property (weak, nonatomic) IBOutlet UIView *vwAction2;
@property (weak, nonatomic) IBOutlet UIView *vwAction3;


/**
 set title for button

 @param title title need set
 @param index button index
 */
- (void) setTitle:(NSString*)title forButtonAtIndex:(NSInteger) index;



/**
 Set title for mutil buttons

 @param title list title
 @param index list index
 */
- (void) setTitleForButtons:(NSArray*)title atIndexes:(NSArray*)index;
@end
