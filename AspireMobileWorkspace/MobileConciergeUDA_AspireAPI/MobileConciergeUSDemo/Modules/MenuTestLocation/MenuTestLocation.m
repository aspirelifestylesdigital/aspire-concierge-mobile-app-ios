//
//  MenuTestLocation.m
//  MobileConciergeUSDemo
//
//  Created by Dai Pham on 8/2/17.
//  Copyright © 2017 Sunrixe Software Solutions Corporation. All rights reserved.
//

#import "MenuTestLocation.h"
#import "Constant.h"

@interface MenuTestLocation()<UITextFieldDelegate> {
    
    IBOutlet UITextField *txtLongi;
    IBOutlet UITextField *txtLati;
    
    IBOutletCollection(UIButton) NSArray *listButtons;
}

@end

@implementation MenuTestLocation

#pragma mark - INIT
- (id)init
{
    self = [super init];
    
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
    }
    
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    txtLati.delegate = self;
    txtLongi.delegate = self;
}

#pragma mark - INTERFACE
- (void)setData:(NSArray<NSString *> *)data {
    if(data.count == 0) return;
    if(data.count == 1) {
        txtLongi.text = [data firstObject];
    } else {
        txtLongi.text = [data firstObject];
        txtLati.text = [data objectAtIndex:1];
    }
}

- (void)setTitleForButtons:(NSArray<NSString *> *)titles {
    if(titles.count == 0)
        return;
    NSInteger maxCheck = listButtons.count;
    if(maxCheck > titles.count)
        maxCheck = titles.count;
    
    for (NSInteger i = 0; i< maxCheck; i++) {
        UIButton* button = (UIButton*)[listButtons objectAtIndex:i];
        button.titleLabel.text = [titles objectAtIndex:i];
    }
}

- (void)clear {
    for (UITextField* txt in @[txtLongi,txtLati]) {
        txt.text = @"";
    }
}

- (void)hideKeyboard {
    [self endEditing:YES];
}

#pragma mark - EVENT
- (IBAction)clearACtion:(id)sender {
    if(self.delegate)
        [self.delegate MenuTestLocation:self onSelectEvent:0 withUserInfo:nil];
}

- (IBAction)setAction:(id)sender {
    if(self.delegate)
        [self.delegate MenuTestLocation:self onSelectEvent:1 withUserInfo:@[[txtLongi.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0?txtLongi.text:@"0",[txtLati.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0?txtLati.text:@"0"]];
}

#pragma mark - UITEXTFIELD DELEGATE
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if(textField.text.length > 0) {
        [textField selectAll:textField];
    }
}
@end
