//
//  ViewLog.h
//  MobileConciergeUSDemo
//
//  Created by Dai Pham on 8/5/17.
//  Copyright © 2017 Sunrixe Software Solutions Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewLog : UIView

- (void) displayLog:(NSString*)log;

@end
