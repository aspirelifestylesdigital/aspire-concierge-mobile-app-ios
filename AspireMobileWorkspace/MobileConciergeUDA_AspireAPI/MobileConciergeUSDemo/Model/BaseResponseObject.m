//
//  BaseResponseObject.m
//  ALC
//
//  Created by Anh Tran on 8/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "BaseResponseObject.h"
#import "NSDictionary+SBJSONHelper.h"
#import "Constant.h"

@implementation BaseResponseObject

-(id)initFromDict:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        
    }
    return self;
}

- (BOOL) isSuccess{
    if(self.status){
        return YES;
    }
    return NO;
}

-(id)parseJson:(NSDictionary *)dict toObject:(NSString *)className
{
    return nil;
}

-(void)parseCommonResponse:(NSDictionary *)dict
{
    self.status = [dict boolForKey:@"success"];
    NSArray* mesArr = [dict arrayForKey:@"message"];
    if(mesArr && mesArr.count > 0){
        NSDictionary * error = [mesArr objectAtIndex:0];
        self.message = [error stringForKey:@"message"];
        self.errorCode = [error stringForKey:@"code"];
    }
    
    self.message = [dict objectForKey:@"message"];
}

@end
