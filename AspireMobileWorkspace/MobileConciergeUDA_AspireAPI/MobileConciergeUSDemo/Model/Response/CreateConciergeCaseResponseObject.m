//
//  CreateNewConciergeCaseResponseObject.m
//  MobileConcierge
//
//  Created by user on 5/15/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "CreateConciergeCaseResponseObject.h"
#import "CreateConciergeCaseItem.h"
#import "NSDictionary+SBJSONHelper.h"

@implementation CreateConciergeCaseResponseObject
-(id) init
{
    self = [super init];
    if(self)
    {
        
    }
    return self;
}

-(id)initFromDict:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        [self parseCommonResponse:dict];
    }
    return self;
}

- (id)parseJson:(NSDictionary *)dict toObject:(NSString *)className{
    if ([className isEqualToString:NSStringFromClass([CreateConciergeCaseItem class])]) {
        CreateConciergeCaseItem *item = [[CreateConciergeCaseItem alloc] init];
        item.transactionId = [dict stringForKey:@"transactionId"];
        return item;
    }
    return nil;
}
-(void)parseCommonResponse:(NSDictionary *)dict
{
    self.status = [dict[@"status"] boolForKey:@"success"];
    self.message = [dict[@"status"] objectForKey:@"message"];
}
@end
