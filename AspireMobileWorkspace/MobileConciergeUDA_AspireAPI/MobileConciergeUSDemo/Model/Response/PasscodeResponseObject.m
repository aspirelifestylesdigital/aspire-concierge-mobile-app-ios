//
//  PasscodeResponseObject.m
//  MobileConciergeUSDemo
//
//  Created by Chung Mai on 10/19/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "PasscodeResponseObject.h"
#import "PasscodeItem.h"

@implementation PasscodeResponseObject

-(id)initFromDict:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        dict = [dict dictionaryForKey:@"CheckPasscodeResult"];
        [self parseCommonResponse:dict];
        self.data = [self parseJson:dict toObject:NSStringFromClass([PasscodeItem class])];
    }
    return self;
}

-(id)parseJson:(NSDictionary *)dict toObject:(NSString *)className
{
    NSMutableArray *datas = [[NSMutableArray alloc] init];
    PasscodeItem *item = [[PasscodeItem alloc] init];
    item.valid = [dict boolForKey:@"Valid"];
    item.message = [dict stringForKey:@"Status"];
    [datas addObject:item];
    return datas;
}

-(void)parseCommonResponse:(NSDictionary *)dict
{
    self.status = [dict boolForKey:@"Success"];
    NSArray* mesArr = [dict arrayForKey:@"message"];
    if(mesArr && mesArr.count > 0){
        NSDictionary * error = [mesArr objectAtIndex:0];
        self.message = [error stringForKey:@"message"];
        self.errorCode = [error stringForKey:@"code"];
    }
    else
    {
        self.message = [dict objectForKey:@"message"];
    }
}

@end
