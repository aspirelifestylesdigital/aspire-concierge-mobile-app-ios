//
//  ConciergeContentResponseObject.m
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/21/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "ConciergeContentResponseObject.h"
#import "ConciergeContentItem.h"
#import "NSDictionary+SBJSONHelper.h"

@implementation ConciergeContentResponseObject

-(id) init
{
    self = [super init];
    if(self)
    {
        
    }
    return self;
}

-(id)initFromDict:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        dict = [dict dictionaryForKey:@"GetAllContentResult"];
        self.data = [dict arrayForKey:@"Content" withProcessing:self toObject:NSStringFromClass([ConciergeContentItem class])];
        [self parseCommonResponse:dict];
    }
    return self;
}

-(id)parseJson:(NSDictionary *)dict toObject:(NSString *)className
{
    if([className isEqualToString:NSStringFromClass([ConciergeContentItem class])])
    {
        ConciergeContentItem *item = [[ConciergeContentItem alloc] init];
        item.parentCategory = [dict stringForKey:@"ParentCategory"];
        item.termsOfUse = [dict stringForKey:@"TermsOfUse"];
        item.descriptionContent = [dict stringForKey:@"Description"];
        item.shortDescriptionContent = [dict stringForKey:@"ShortDescription"];
        item.relatedCities = [dict arrayForKey:@"RelatedCities"];
        item.ID = [dict longForKey:@"ID"];
        item.internalDescription = [dict stringForKey:@"InternalDescription"];
        item.tileImage = [dict stringForKey:@"TileImage"];
        item.tileLink = [dict stringForKey:@"TileLink"];
        item.zipCodes = [dict stringForKey:@"ZipCodes"];
        item.offerInd = [dict stringForKey:@"OfferInd"];
        item.subCategory = [dict stringForKey:@"SubCategory"];
        item.benefitInd = [dict stringForKey:@"BenefitInd"];
        item.callOutBox = [dict stringForKey:@"CallOutBox"];
        item.tileText = [dict stringForKey:@"TileText"];
        item.title = [dict stringForKey:@"Title"];
        item.category = [dict stringForKey:@"Category"];
        item.hotInd = [dict stringForKey:@"HotInd"];
        item.offerText = [dict stringForKey:@"OfferText"];
        
        return item;
    }
    
    return nil;
}

-(void)parseCommonResponse:(NSDictionary *)dict
{
    self.status = [dict boolForKey:@"Success"];
    NSArray* mesArr = [dict arrayForKey:@"message"];
    if(mesArr && mesArr.count > 0){
        NSDictionary * error = [mesArr objectAtIndex:0];
        self.message = [error stringForKey:@"message"];
        self.errorCode = [error stringForKey:@"code"];
    }
    
    self.message = [dict objectForKey:@"message"];
}

@end
