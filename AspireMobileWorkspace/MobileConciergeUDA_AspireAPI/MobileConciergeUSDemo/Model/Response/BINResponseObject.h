//
//  BINResponseObject.h
//  MobileConcierge
//
//  Created by Chung Mai on 7/5/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseResponseObject.h"

@interface BINResponseObject : BaseResponseObject

@end
