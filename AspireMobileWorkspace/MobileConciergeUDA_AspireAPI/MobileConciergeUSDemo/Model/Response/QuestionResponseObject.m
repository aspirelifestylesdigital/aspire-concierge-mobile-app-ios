//
//  QuestionResponseObject.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/31/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "QuestionResponseObject.h"
#import "NSDictionary+SBJSONHelper.h"
#import "QuestionItem.h"
#import "AnswerItem.h"
#import "LocationComponent.h"
#import "AppData.h"

@implementation QuestionResponseObject


-(id)initFromDict:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        dict = [dict dictionaryForKey:@"GetQuestionsAndAnswersResult"];
        [LocationComponent setNumbersItemCheck:[dict arrayForKey:@"QuestionsAndAnswers"].count];
        self.data = [dict arrayForKey:@"QuestionsAndAnswers" withProcessing:self toObject:NSStringFromClass([QuestionItem class])];
        [self parseCommonResponse:dict];
    }
    return self;
}

-(id)initForSearchItemFromDict:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        dict = [dict dictionaryForKey:@"GetQuestionAndAnswersResult"];
        [LocationComponent setNumbersItemCheck:[dict arrayForKey:@"QuestionsAndAnswers"].count];
        self.data = [dict arrayForKey:@"Answers" withProcessing:self toObject:NSStringFromClass([AnswerItem class])];
        [self parseCommonResponse:dict];
    }
    return self;
}

-(id)parseJson:(NSDictionary *)dict toObject:(NSString *)className
{
    if([className isEqualToString:NSStringFromClass([QuestionItem class])])
    {
        return [self parseJsonForQuestionItem:dict];
    }
    if([className isEqualToString:NSStringFromClass([AnswerItem class])])
    {
        return [self parseJsonForAnswerItem:dict];
    }
    
    
    return nil;
}

-(QuestionItem *)parseJsonForQuestionItem:(NSDictionary *)dict
{
    QuestionItem *item = [[QuestionItem alloc] init];
    item.ID = [dict stringForKey:@"ID"];
    item.hitCount = [dict stringForKey:@"HitCount"];
    item.hotQuestion = [dict stringForKey:@"HotQuestion"];
    item.lastUpdateDate = [dict stringForKey:@"LastUpdateDate"];
    item.name = [dict stringForKey:@"QuestionText"];
    item.descriptionContent = [dict stringForKey:@"ShortQuestionText"];
    item.userDefined1 = [dict stringForKey:@"UserDefined1"];
    item.userDefined2 = [dict stringForKey:@"UserDefined2"];
    item.answers = [dict arrayForKey:@"Answers" withProcessing:self toObject:@"AnswerItem"];
    return item;
}

-(AnswerItem *)parseJsonForAnswerItem:(NSDictionary *)dict
{
    AnswerItem *item = [[AnswerItem alloc] init];
    item.categoryCode = self.categoryCode;
    item.categoryName = self.categoryName;
    item.address1 = [dict stringForKey:@"Address"];
    item.address2 = [dict stringForKey:@"Address2"];
    item.address3 = [dict stringForKey:@"Address3"];
    
    /// substring answer
    NSString *string = [dict stringForKey:@"AnswerText"];
    NSRange range2 = NSMakeRange(NSNotFound, 0);
    if(string.length > 0)
        range2 = [string.lowercaseString rangeOfString:@"<b>Insider Tip</b>:".lowercaseString];
    
    if(range2.location != NSNotFound)
    {
        NSString *insiderTip = [[string substringFromIndex:NSMaxRange(range2)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        NSString* result = [insiderTip stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        item.insiderTip = result;
        item.answerDescription = [[string substringToIndex:range2.location] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    } else{
        item.answerDescription = string;
        item.insiderTip = @"";
    }
    item.answerText = [dict stringForKey:@"AnswerText"];
    item.cityName = [dict stringForKey:@"City"];
    item.commentCount = [dict integerForKey:@"CommentCount"];
    item.countryName = [dict stringForKey:@"Country"];
    item.hasContentSystemOffers = [dict boolForKey:@"HasContentSystemOffers"];
    item.hoursOfOperation = [dict stringForKey:@"HoursOfOperation"];
    item.ID = [dict stringForKey:@"ID"];
    item.imageURL = [dict stringForKey:@"ImageURL"];
    item.answerName = [dict stringForKey:@"Name"];
    item.offer1 = [dict stringForKey:@"Offer"];
    item.offer2 = [dict stringForKey:@"Offer2"];
    item.phone = [dict stringForKey:@"Phone"];
    item.price = [dict floatForKey:@"Price"];
    item.ratingsAverage = [dict floatForKey:@"RatingsAverage"];
    item.ratingsCount = [dict floatForKey:@"RatingsCount"];
    item.state = [dict stringForKey:@"State"];
    item.URL = [dict stringForKey:@"URL"];
    item.usefulCount = [dict integerForKey:@"UsefulCount"];
    item.userDefined1 = [dict stringForKey:@"UserDefined1"];
    item.userDefined2 = [dict stringForKey:@"UserDefined2"];
    item.zipCode = [dict stringForKey:@"ZipCode"];
    
    if([self.categoryCode isEqualToString:@"dining"] && ![self.currentCategory.supperCategoryID isEqualToString:@"80"] && [[SessionData shareSessiondata] isUseLocation] && [AppData shouldGetDistanceForListDiningFromCity:self.currentCity])
        [item getDistance];
    
    return item;
}

-(void)parseCommonResponse:(NSDictionary *)dict
{
    self.status = [dict boolForKey:@"Success"];
    NSArray* mesArr = [dict arrayForKey:@"message"];
    if(mesArr && mesArr.count > 0){
        NSDictionary * error = [mesArr objectAtIndex:0];
        self.message = [error stringForKey:@"message"];
        self.errorCode = [error stringForKey:@"code"];
    }
    
    self.message = [dict objectForKey:@"message"];
}
@end
