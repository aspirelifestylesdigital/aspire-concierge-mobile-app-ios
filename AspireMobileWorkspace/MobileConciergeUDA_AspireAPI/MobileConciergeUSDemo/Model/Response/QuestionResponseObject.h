//
//  QuestionResponseObject.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/31/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "BaseResponseObject.h"

@interface QuestionResponseObject : BaseResponseObject

-(id)initForSearchItemFromDict:(NSDictionary *)dict;

@end
