//
//  SubCategoryResponseObject.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/30/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "SubCategoryResponseObject.h"
#import "NSDictionary+SBJSONHelper.h"
#import "SubCategoryItem.h"

@implementation SubCategoryResponseObject

-(id)initFromDict:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        dict = [dict dictionaryForKey:@"GetSubCategoriesResult"];
        self.data = [dict arrayForKey:@"Categories" withProcessing:self toObject:NSStringFromClass([SubCategoryItem class])];
        [self parseCommonResponse:dict];
    }
    return self;
}

-(id)parseJson:(NSDictionary *)dict toObject:(NSString *)className
{
    if([className isEqualToString:NSStringFromClass([SubCategoryItem class])])
    {
        SubCategoryItem *item = [[SubCategoryItem alloc] init];
        item.ID = [dict stringForKey:@"ID"];
        item.name = [dict stringForKey:@"Description"];
        item.descriptionItem = [dict stringForKey:@"ExtendedDescription"];
        item.imageURL = [dict stringForKey:@"ImageURL"];
        item.level = [dict stringForKey:@"Level"];
        item.parentCategoryID = [dict stringForKey:@"ParentCategoryID"];
        item.superCategoryID = [dict stringForKey:@"SuperCategoryID"];
        [item setMappingDataForSubCategory];
        return item;
    }
    
    return nil;
}

-(void)parseCommonResponse:(NSDictionary *)dict
{
    self.status = [dict boolForKey:@"Success"];
    NSArray* mesArr = [dict arrayForKey:@"message"];
    if(mesArr && mesArr.count > 0){
        NSDictionary * error = [mesArr objectAtIndex:0];
        self.message = [error stringForKey:@"message"];
        self.errorCode = [error stringForKey:@"code"];
    }
    
    self.message = [dict objectForKey:@"message"];
}
@end
