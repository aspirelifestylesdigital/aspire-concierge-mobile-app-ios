//
//  BaseResponseObjectProtocol.h
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/18/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BaseResponseObjectProtocol <NSObject>

@property (nonatomic, assign) NSInteger task;
@property (nonatomic, assign) NSInteger status;
@property (nonatomic, strong) NSString* message;
@property (nonatomic, strong) NSArray* data;
@property (nonatomic, strong) NSArray* errors;
@property (nonatomic, copy) NSString* errorCode;

-(id) initFromDict:(NSDictionary*) dict;
-(id)parseJson:(NSDictionary *)dict toObject:(NSString *)className;
-(void) parseCommonResponse:(NSDictionary *)dict;
-(BOOL) isSuccess;

@end
