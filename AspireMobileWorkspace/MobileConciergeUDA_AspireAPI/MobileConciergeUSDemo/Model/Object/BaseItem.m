//
//  ExploreItem.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/9/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "BaseItem.h"
#import "LocationComponent.h"
#import "AppData.h"

@implementation BaseItem
- (id) init {
    if(self = [super init]) {
        self.distance = CGFLOAT_MAX;
    }
    return self;
}

- (void) getDistance {
    
    NSMutableString* temp = [NSMutableString new];
    
//    if([[self.name lowercaseString] containsString:@"beijing"]) {

        
//        if([self.name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0) {
//            [temp appendFormat:temp.length > 0?@", %@":@"%@",[[self.name stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""]];
//        }
        
//    } else {
    
//    if(![[AppData getSelectedCity].name isEqualToString:@"Asia Pacific"]) {
    
//        if([self.name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0) {
//            [temp appendFormat:@"%@",self.name];
//        }
        
//    } else {
    
    if([self.name containsString:@"M Restaurant"]) {
        NSLog(@"");
    }
    
    NSString* addres1 = [[self.address1 componentsSeparatedByString:@","] firstObject];
    if([[self.cityName lowercaseString] isEqualToString:@"london"]) {
        addres1 = self.address1;
    }
    if([self.state isEqualToString:@"HI"] || [self.state isEqualToString:@"NV"]){
        addres1 = [[self.address1 componentsSeparatedByString:@","] lastObject];
        if([self.address1 componentsSeparatedByString:@","].count > 1) {
            addres1 = [[self.address1 componentsSeparatedByString:@","] objectAtIndex:1];
        }
    }
    if([addres1 containsString:@"St."])
        addres1 = [addres1 stringByReplacingOccurrencesOfString:@"St." withString:@"St"];
    if([self.state isEqualToString:@"GA"]) {
        addres1 = [addres1 stringByReplacingOccurrencesOfString:@"NW" withString:@"Northwest"];
    }
        if([self.address1 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0) {
            [temp appendFormat:temp.length > 0?@", %@":@"%@",[self.state isEqualToString:@"HI"]?addres1:[addres1 stringByReplacingOccurrencesOfString:@" St" withString:@" Street"]];
        }
//    }
    
//    if([self.address3 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0) {
//        [temp appendFormat:temp.length > 0?@", %@":@"%@",self.address3];
//    }
    
    if([self.cityName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0) {
        [temp appendFormat:temp.length > 0?@" %@":@"%@",self.cityName];
    }
    
//    if([self.countryName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0) {
//        [temp appendFormat:temp.length > 0?@", %@":@"%@",self.countryName];
//    }
    
    if([self.state stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0) {
        [temp appendFormat:temp.length > 0?@", %@":@"%@",self.state];
    }
    //
//    if([self.zipCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0) {
//        [temp appendFormat:temp.length > 0?@" %@":@"%@",self.zipCode];
//    }
//    }
    if(temp.length == 0) return;
    NSLog(@"%@",temp);
    self.addressForMap = temp;
    self.isWaitingGetDistance = YES;
    [LocationComponent getDistanceForObject:self fromAddress:[self strimStringFrom:temp withPatterns:@[@"\\(.*\\),",@"^(\\+\\d{1,9}\\s)?\\(?\\d{1,9}\\)?[\\s.-]?\\d{1,9}[\\s.-]?\\d{1,9},"]] onDone:^(id object,double distance, double lng, double lati){
        BaseItem* obj = (BaseItem*)object;
        obj.distance = distance;
        obj.latitude = lati;
        obj.longitude = lng;
        obj.isWaitingGetDistance = NO;
        
        // default distance of object setted = CGFLOAT_MAX
        if(obj.longitude == 0 && obj.latitude == 0) {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [[NSNotificationCenter defaultCenter] postNotificationName:@"SHOW_LOG_APP" object:nil userInfo:@{@"message":[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]}];
//            });
            NSLog(@"%s Cant get location for item: %@",__PRETTY_FUNCTION__,obj.name);
        }
    }];
    temp = nil;
}

- (NSString*) strimStringFrom:(NSString*) from withPatterns:(NSArray*) pattern {
    NSString* temp = from;
    for (NSString* p in pattern) {
        temp = [temp stringByReplacingOccurrencesOfString:p withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0,temp.length)];
    }
    return temp;
}
@end
