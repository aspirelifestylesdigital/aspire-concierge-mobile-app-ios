//
//  AnswerItem.h
//  MobileConcierge
//
//  Created by Chung Mai on 6/1/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseItem.h"
#import <UIKit/UIKit.h>

@interface AnswerItem : BaseItem
@property (nonatomic, strong) NSString *answerText;
@property (nonatomic, strong) NSString *insiderTip;
@property (nonatomic, strong) NSString *answerDescription;
@property (nonatomic, assign) NSInteger commentCount;
@property (nonatomic, assign) BOOL hasContentSystemOffers;
@property (nonatomic, strong) NSString *hoursOfOperation;
@property (nonatomic, strong) NSString *answerName;
@property (nonatomic, strong) NSString *offer1;
@property (nonatomic, strong) NSString *offer2;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, assign) CGFloat price;
@property (nonatomic, assign) CGFloat ratingsAverage;
@property (nonatomic, assign) CGFloat ratingsCount;

@property (nonatomic, strong) NSString *URL;
@property (nonatomic, assign) NSUInteger usefulCount;
@property (nonatomic, strong) NSString *userDefined2;
@property (nonatomic, strong) NSString *termOfUse;
@end
