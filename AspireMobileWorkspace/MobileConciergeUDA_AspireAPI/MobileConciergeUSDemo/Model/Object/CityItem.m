//
//  CityItem.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "CityItem.h"

@implementation CityItem

-(NSString *)address
{
    NSString *midString = nil;
    if(self.stateCode && self.countryCode)
    {
        midString = @", ";
    }
    return [NSString stringWithFormat:@"%@%@%@", self.stateCode ? self.stateCode : @"",midString ? midString : @"", self.countryCode ? self.countryCode : @""];
}
@end
