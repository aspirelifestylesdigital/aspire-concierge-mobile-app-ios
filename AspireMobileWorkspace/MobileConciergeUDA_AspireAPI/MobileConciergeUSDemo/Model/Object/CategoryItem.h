//
//  CategoryItem.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CategoryItem : NSObject<NSCopying>

@property(nonatomic, strong) NSString *ID;
@property(nonatomic, strong) NSString *supperCategoryID;
@property(nonatomic, strong) NSString *categoryName;
@property(nonatomic, strong) NSString *subCategoryName;
@property(nonatomic, strong) NSString *code;
@property(nonatomic, strong) NSString *imageURL;
@property(nonatomic, strong) UIImage *categoryImg;
@property(nonatomic, strong) UIImage *categoryImgDetail;
@property(nonatomic, strong) NSArray *categoriesForService;//For CCA Data
@property(nonatomic, strong) NSArray *categoryIDs;//Use to create category name in the case of getting all data
@property(assign,nonatomic) BOOL isCheckGeographicRegion;
@end
