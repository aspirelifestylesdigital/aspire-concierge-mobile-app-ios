//
//  PolicyInfoItem.h
//  MobileConcierge
//
//  Created by Chung Mai on 6/19/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PolicyInfoItem : NSObject

@property (nonatomic, strong) NSString *textInfo;
@property (nonatomic, strong) NSString *CurrentVersion;

@end
