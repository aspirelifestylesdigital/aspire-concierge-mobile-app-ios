//
//  TileItem.h
//  MobileConcierge
//
//  Created by Chung Mai on 6/6/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "BaseItem.h"

@interface TileItem : BaseItem

@property(nonatomic, strong) NSString *tileImage;
@property(nonatomic, strong) NSString *tileLink;
@property(nonatomic, strong) NSString *tileText;
@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSString *shortDescription;
@property(nonatomic, strong) NSString *GeographicRegion;
@property(nonatomic, strong) NSString *tileSubCategory;
@property(nonatomic, strong) NSString *tileCategory;
@property(nonatomic, strong) NSString *currentCategoryCode;

@end
