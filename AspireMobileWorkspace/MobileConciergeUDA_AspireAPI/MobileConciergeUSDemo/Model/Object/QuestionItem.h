//
//  QuestionItem.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/31/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseItem.h"


@interface QuestionItem : BaseItem

@property(nonatomic, strong) NSString *hitCount;
@property(nonatomic, strong) NSString *hotQuestion;
@property(nonatomic, strong) NSString *lastUpdateDate;
@property(nonatomic, strong) NSString *userDefined1;
@property(nonatomic, strong) NSString *userDefined2;
@property(nonatomic, strong) NSArray *answers;

@end
