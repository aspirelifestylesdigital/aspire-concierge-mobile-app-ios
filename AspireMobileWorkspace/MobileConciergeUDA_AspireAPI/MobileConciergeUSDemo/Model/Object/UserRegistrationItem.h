//
//  UserRegistrationItem.h
//  MobileConcierge
//
//  Created by user on 6/20/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserRegistrationItem : NSObject
@property (strong, nonatomic) NSString *OnlineMemberDetailIDs;
@property (strong, nonatomic) NSString *OnlineMemberID;
@property (strong, nonatomic) NSString *message;
@property (nonatomic) BOOL hasForgotAccount;
@end
