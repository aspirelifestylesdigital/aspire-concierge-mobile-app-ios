//
//  ConciergeContentItem.h
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/21/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "BaseResponseObject.h"

@interface ConciergeContentItem : NSObject

@property(nonatomic, copy) NSString *parentCategory;
@property(nonatomic, copy) NSString *termsOfUse;
@property(nonatomic, copy) NSString *descriptionContent;
@property(nonatomic, copy) NSString *shortDescriptionContent;
@property(nonatomic, strong) NSArray *relatedCities;
@property(nonatomic, assign) NSUInteger ID;
@property(nonatomic, copy) NSString *internalDescription;
@property(nonatomic, copy) NSString *tileImage;
@property(nonatomic, copy) NSString *tileLink;
@property(nonatomic, copy) NSString *zipCodes;
@property(nonatomic, copy) NSString *offerInd;
@property(nonatomic, copy) NSString *subCategory;
@property(nonatomic, copy) NSString *benefitInd;
@property(nonatomic, copy) NSString *callOutBox;
@property(nonatomic, copy) NSString *tileText;
@property(nonatomic, copy) NSString *title;
@property(nonatomic, copy) NSString *category;
@property(nonatomic, copy) NSString *hotInd;
@property(nonatomic, copy) NSString *offerText;

@end
