//
//  CitiesManager.h
//  TestIOS
//
//  Created by Dai Pham on 7/12/17.
//  Copyright © 2017 Dai Pham. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CityObject: NSObject
@property (strong,nonatomic,setter=setName:) NSString* name;
@property (strong,nonatomic,setter=setGroup:) NSString* group;
@property (assign,nonatomic,setter=setIsRegion:) BOOL isRegion;
@property (assign,nonatomic,setter=setIsDisplayForDining:) BOOL isDisplayForDining;
@property (assign,nonatomic,setter=setIsDisplayForOther:) BOOL isDisplayForOther;
@property (assign,nonatomic,setter=setIsDisplayForHotel:) BOOL isDisplayForHotel;
@property (assign,nonatomic,setter=setIsValid:) BOOL isValid;
@end

@interface CitiesManager : NSObject {
    NSArray* allCities;
    NSArray* citiesForDining;
    NSArray* citiesForHotel;
    NSArray* citiesForOther;
}
+ (CitiesManager*) shared;

- (NSArray*) getListCitiesForDining;
- (NSArray*) getListCitiesForHotel;
- (NSArray*) getListCitiesForOther;
@end
