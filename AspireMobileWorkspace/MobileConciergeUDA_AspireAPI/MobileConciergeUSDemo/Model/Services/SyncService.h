//
//  SyncService.h
//  USDemo
//
//  Created by Dai Pham on 6/7/17.
//  Copyright (c) 2017. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"

@interface SyncService : NSObject

#pragma mark STATIC - API
+ (void) requestTokenOnDone:(void(^)(id))onDone onError:(void(^)(id))onError;
+ (void) postUrl:(NSString*) url withParams:(NSDictionary*) params OnDone:(void(^)(id))onDone onError:(void(^)(id))onError;
+ (void) getUrl:(NSString*) url withParams:(NSDictionary*) params OnDone:(void(^)(id))onDone onError:(void(^)(id))onError;
+ (void) getUrl:(NSString *)url withTimeout:(NSTimeInterval)timeoutInterval withParams:(NSDictionary *)params OnDone:(void (^)(id))onDone onError:(void (^)(id))onError;

#pragma mark AspireAPI
+ (void) requestAspireTokenWithUsername:(NSString*)username password:(NSString*)password completion:(void(^)(_Nullable id,_Nullable id))completion;
+ (void) retrieveCurrentUserFromAccessToken:(NSString*)token tokenType:(NSString*)tokenType completion:(void(^)(_Nullable id,_Nullable id))completion;
+ (void) createUserFromAccessToken:(NSString*)token tokenType:(NSString*)tokenType withUserInfo:(NSDictionary*)userInfo completion:(void(^)(_Nullable id))completion;
+ (void) updateUserFromAccessToken:(NSString*)token tokenType:(NSString*)tokenType withUserInfo:(NSDictionary*)userInfo completion:(void(^)(_Nullable id))completion;
+ (void) createRequestFromAccessToken:(NSString*)token tokenType:(NSString*)tokenType withUserInfo:(NSDictionary*)userInfo completion:(void(^)(_Nullable id))completion;
+ (void) changePasswordWithToken:(NSString*)token tokenType:(NSString*)tokenType email:(NSString*)email userInfo:(NSDictionary*) userinfo completion:(void(^)(_Nullable id))completion;
+ (void) resetPasswordWithToken:(NSString*)token tokenType:(NSString*)tokenType email:(NSString*)email completion:(void(^)(_Nullable id))completion;
@end
