//
//  SyncService.h
//  USDemo
//
//  Created by Dai Pham on 6/7/17.
//  Copyright (c) 2017. All rights reserved.
//

//app
#import "SyncService.h"
#import "AFHTTPRequestOperationManager.h"

#define MSG_SERVER_WRONG            local(@"I'm currently having trouble connecting to the server. Please try again shortly.")

//**************************************************
@interface AFHTTPRequestOperationManager (Timeout)

- (AFHTTPRequestOperation*)POST:(NSString *)URLString
                     parameters:(NSDictionary *)parameters
                timeoutInterval:(NSTimeInterval)timeoutInterval
                        success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError* error))failure;
@end

@implementation AFHTTPRequestOperationManager (Timeout)

- (AFHTTPRequestOperation*) PUT:(NSString *)URLString
                      parameters:(NSDictionary *)parameters
                 timeoutInterval:(NSTimeInterval)timeoutInterval
                         success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                         failure:(void (^)(AFHTTPRequestOperation *operation, NSError* error))failure;
{
    NSMutableURLRequest *request = [self.requestSerializer requestWithMethod:@"PUT" URLString:[[NSURL URLWithString:URLString relativeToURL:self.baseURL] absoluteString] parameters:parameters error:NULL];
    
    [request setTimeoutInterval:timeoutInterval];
    AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request success:success failure:failure];
    [self.operationQueue addOperation:operation];
    
    return operation;
}

- (AFHTTPRequestOperation*) POST:(NSString *)URLString
                     parameters:(NSDictionary *)parameters
                timeoutInterval:(NSTimeInterval)timeoutInterval
                        success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError* error))failure;
{
    NSMutableURLRequest *request = [self.requestSerializer requestWithMethod:@"POST" URLString:[[NSURL URLWithString:URLString relativeToURL:self.baseURL] absoluteString] parameters:parameters error:NULL];

    [request setTimeoutInterval:timeoutInterval];
    AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request success:success failure:failure];
    [self.operationQueue addOperation:operation];
    
    return operation;
}

- (AFHTTPRequestOperation*) GET:(NSString *)URLString
                      parameters:(NSDictionary *)parameters
                 timeoutInterval:(NSTimeInterval)timeoutInterval
                         success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                         failure:(void (^)(AFHTTPRequestOperation *operation, NSError* error))failure;
{
    NSMutableURLRequest *request = [self.requestSerializer requestWithMethod:@"GET" URLString:[[NSURL URLWithString:URLString relativeToURL:self.baseURL] absoluteString] parameters:parameters error:NULL];
    [request setTimeoutInterval:timeoutInterval];
    AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request success:success failure:failure];
    [self.operationQueue addOperation:operation];
    
    return operation;
}
@end

//**************************************************
@interface SyncService ()@end

@implementation SyncService

+ (void)requestTokenOnDone:(void (^)(id))onDone onError:(void (^)(id))onError {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [manager.requestSerializer setValue:B2C_ConsumerKey forHTTPHeaderField:@"ConsumerKey"];
    [manager.requestSerializer setValue:B2C_ConsumerSecret forHTTPHeaderField:@"ConsumerSecret"];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    [dictKeyValues setObject:B2C_Callback_Url forKey:@"CallBackURL"];
    [dictKeyValues setObject:[[SessionData shareSessiondata] OnlineMemberID] forKey:@"OnlineMemberId"];
    [dictKeyValues setObject:B2C_DeviceId forKey:@"MemberDeviceId"];
    [manager GET:[MCD_API_URL stringByAppendingString:GetRequestToken] parameters:dictKeyValues success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(onDone) {
            onDone(responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if(onError)
        {
            onError(error);
        }
    }];
}

#pragma mark STATIC - API register device, get user info of device
+ (void)postUrl:(NSString *)url withParams:(NSDictionary *)params OnDone:(void (^)(id))onDone onError:(void (^)(id))onError {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    NSLog(@"%s: %@%@",__PRETTY_FUNCTION__,url,params);
    manager.requestSerializer =  [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    AFJSONResponseSerializer *serializer = [AFJSONResponseSerializer serializer];
    [serializer setRemovesKeysWithNullValues:YES];
    [manager setResponseSerializer:serializer];
    
    [manager POST:url parameters:params timeoutInterval:60 success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if(onDone) {
            onDone(responseObject);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if(onError)
        {
            onError(error);
        }
    }];
}

+ (void)getUrl:(NSString *)url withParams:(NSDictionary *)params OnDone:(void (^)(id))onDone onError:(void (^)(id))onError {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(onDone) {
            onDone(responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if(onError)
        {
            onError(error);
        }
    }];
    
}

+ (void)getUrl:(NSString *)url withTimeout:(NSTimeInterval)timeoutInterval withParams:(NSDictionary *)params OnDone:(void (^)(id))onDone onError:(void (^)(id))onError {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    [manager GET:url parameters:params timeoutInterval:timeoutInterval success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(onDone) {
            onDone(responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if(onError)
        {
            onError(error);
        }
    }];
    
}

#pragma mark - AspireAPI
#define ASPIREAPI_AUTH_ACC @"0oaeanvkzevwMjXAY0h7"
#define ASPIREAPI_AUTH_PAS @"4eNOLghFGDCDHBm2SdcE48wcwGfPtKYmoLX8pI7F"
+ (void)requestAspireTokenWithUsername:(NSString *)username password:(NSString *)password completion:(void (^)(id, id))completion {
    
    if ([username componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].count == 0 ||
        [password componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].count == 0) {
        if (completion)
            completion(nil,[NSError errorWithDomain:@"com.error" code:-1011 userInfo:@{@"message":@"username or password is invalid"}]);
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"no-cache" forHTTPHeaderField:@"Cache-Control"];
    [manager.requestSerializer setCachePolicy:(NSURLRequestUseProtocolCachePolicy)];
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", ASPIREAPI_AUTH_ACC, ASPIREAPI_AUTH_PAS];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString* endcode = [authData base64EncodedStringWithOptions:0];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", endcode];
    [manager.requestSerializer setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    NSMutableDictionary* params = [NSMutableDictionary new];
    [params setObject:@"password" forKey:@"grant_type"];
    [params setObject:@"openid" forKey:@"scope"];
    [params setObject:username forKey:@"username"];
    [params setObject:password forKey:@"password"];
    
    [manager POST:ASPIREAPI_TOKEN_URL parameters:params timeoutInterval:60 success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if(completion) {
            completion(responseObject,nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSError* err = nil;
        NSDictionary* jsonError = [NSJSONSerialization  JSONObjectWithData:operation.responseData options:0 error:&err];
        if(completion) {
            if (err == nil) {
                err = [NSError errorWithDomain:@"com.dai.pham.authenerror" code:-1011 userInfo:jsonError];
            } else {
                err = [NSError errorWithDomain:@"com.dai.pham.authenerror" code:-1011 userInfo:@{@"message":@"Unkhown error"}];
            }
            completion(nil,err);
        }
    }];
}

+ (void) retrieveCurrentUserFromAccessToken:(NSString *)token tokenType:(NSString*)tokenType completion:(void (^)(id _Nullable, id _Nullable))completion {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"no-cache" forHTTPHeaderField:@"Cache-Control"];
    [manager.requestSerializer setCachePolicy:(NSURLRequestUseProtocolCachePolicy)];
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    [manager.requestSerializer setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    [manager GET:ASPIREAPI_PROFILE_URL parameters:nil timeoutInterval:60 success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(completion) {
            completion(responseObject,nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if(completion)
        {
            completion(nil,error);
        }
    }];
}

+ (void)createUserFromAccessToken:(NSString *)token tokenType:(NSString *)tokenType withUserInfo:(NSDictionary *)userInfo completion:(void (^)(id _Nullable))completion {
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:userInfo options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:ASPIREAPI_CREATE_CUSTOMER_URL]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json"};
    
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
#if DEBUG
                                                        NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
                                                        if(completion) {
                                                            completion(error);
                                                        }
                                                    } else {
                                                        NSError* err;
                                                        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                                                             options:kNilOptions
                                                                                                               error:&err];
                                                        if (err == nil) {
                                                            if(completion) {
                                                                completion([[json allKeys] containsObject:@"errorCode"] ? [NSError errorWithDomain:@"com.dai.pham.error" code:-1021 userInfo:json] : nil);
                                                            }
                                                        } else {
                                                            if(completion) {
                                                                completion(error);
                                                            }
                                                        }
                                                    }
                                                    
                                                }];
    [dataTask resume];
}

+ (void)updateUserFromAccessToken:(NSString *)token tokenType:(NSString *)tokenType withUserInfo:(NSDictionary *)userInfo completion:(void (^)(id _Nullable))completion {
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:userInfo options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:ASPIREAPI_PROFILE_URL]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json"};
    
    [request setHTTPMethod:@"PUT"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
#if DEBUG
                                                        NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
                                                        if(completion) {
                                                            completion(error);
                                                        }
                                                    } else {
                                                        NSError* err;
                                                        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                                                             options:kNilOptions
                                                                                                               error:&err];
                                                        if (err == nil) {
                                                            if(completion) {
                                                                completion([[json allKeys] containsObject:@"errorCode"] ? [NSError errorWithDomain:@"com.dai.pham.error" code:-1011 userInfo:json] : nil);
                                                            }
                                                        } else {
                                                            if(completion) {
                                                                completion(error);
                                                            }
                                                        }
                                                    }
                                                    
                                                }];
    [dataTask resume];
}

+ (void)createRequestFromAccessToken:(NSString *)token tokenType:(NSString *)tokenType withUserInfo:(NSDictionary *)userInfo completion:(void (^)(id _Nullable))completion {
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:userInfo options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:ASPIREAPI_REQUEST_URL]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json"};
    
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
#if DEBUG
                                                        NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
                                                        if(completion) {
                                                            completion(error);
                                                        }
                                                    } else {
                                                        NSError* err;
                                                        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                                                             options:kNilOptions
                                                                                                               error:&err];
                                                        if (err == nil) {
                                                            if(completion) {
                                                                completion([[json allKeys] containsObject:@"errorCode"] ? [NSError errorWithDomain:@"com.dai.pham.error" code:-1011 userInfo:json] : nil);
                                                            }
                                                        } else {
                                                            if(completion) {
                                                                completion(error);
                                                            }
                                                        }
                                                    }
                                                    
                                                }];
    [dataTask resume];
}

+ (void)changePasswordWithToken:(NSString *)token tokenType:(NSString*)tokenType email:(NSString*)email userInfo:(NSDictionary *)userinfo completion:(void (^)(id _Nullable)) completion{
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:userinfo options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[ASPIREAPI_CHANGE_PASSWORD_URL stringByReplacingOccurrencesOfString:@"[@email]" withString:email]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json"};
    
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
#if DEBUG
                                                        NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
                                                        if(completion) {
                                                            completion(error);
                                                        }
                                                    } else {
                                                        NSError* err;
                                                        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                                                             options:kNilOptions
                                                                                                               error:&err];
                                                        if (err == nil) {
                                                            if(completion) {
                                                                completion([[json allKeys] containsObject:@"errorCode"] ? [NSError errorWithDomain:@"com.dai.pham.error" code:-1011 userInfo:json] : nil);
                                                            }
                                                        } else {
                                                            if(completion) {
                                                                completion(error);
                                                            }
                                                        }
                                                    }
                                                    
                                                }];
    [dataTask resume];
}

+ (void)resetPasswordWithToken:(NSString *)token tokenType:(NSString *)tokenType email:(NSString *)email completion:(void (^)(id _Nullable))completion {
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[ASPIREAPI_RESET_PASSWORD_URL stringByReplacingOccurrencesOfString:@"[@email]" withString:email]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json"};
    
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
#if DEBUG
                                                        NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
                                                        if(completion) {
                                                            completion(error);
                                                        }
                                                    } else {
                                                        NSError* err;
                                                        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                                                             options:kNilOptions
                                                                                                               error:&err];
                                                        if (err == nil) {
                                                            if(completion) {
                                                                completion([[json allKeys] containsObject:@"errorCode"] ? [NSError errorWithDomain:@"com.dai.pham.error" code:-1011 userInfo:json] : nil);
                                                            }
                                                        } else {
                                                            if(completion) {
                                                                completion(error);
                                                            }
                                                        }
                                                    }
                                                    
                                                }];
    [dataTask resume];
}
@end
