//
//  AppColor.m
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 10/19/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "AppColor.h"
#import "Common.h"

@implementation AppColor

#pragma MARK - VIEW
+(UIColor *) backgroundColor {
    return colorFromHexString(@"#011627");
}
+(UIColor *) lineViewBackgroundColor {
    return colorFromHexString(@"#253847");
}
+(UIColor *) separatingLineColor {
    return colorFromHexString(@"#253847");
}
+(UIColor *) selectedViewColor {
    return colorFromHexString(@"#E5EBEE");
}

#pragma MARK - BUTTON
+(UIColor *) disableButtonColor {
    return colorFromHexString(@"#500D29");
}
+(UIColor *) normalButtonColor {
    return colorFromHexString(@"#9D0329");
}
+(UIColor *) highlightButtonColor {
    return colorFromHexString(@"#C54B53");
}

#pragma MARK - LABEL
+(UIColor *) titleViewControllerColor{
    return colorFromHexString(@"#FFFFFF");
}
+(UIColor *) placeholderTextColor {
    return colorFromHexString(@"#99A1A8");
}
+(UIColor *) textColor {
    return colorFromHexString(@"#011627");
}
+(UIColor *) disableTextColor {
    return colorFromHexString(@"");
}
+(UIColor *) normalTextColor {
    return colorFromHexString(@"#FFFFFF");
}
+(UIColor *) highlightTextColor {
    return colorFromHexString(@"#C54B53");
}

+(UIColor *) linkTextColor {
    return colorFromHexString(@"#253847");
}

@end
