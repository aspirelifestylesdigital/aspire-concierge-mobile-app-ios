//
//  UIButton+Extension.h
//  MobileConcierge
//
//  Created by Home on 5/7/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Extension)

-(void)setBackgroundColorForNormalStatus;
-(void)setBackgroundColorForTouchingStatus;
-(void)setBackgroundColorForSelectedStatus;
-(void)setBackgroundColorForDisableStatus;
-(void)configureDecorationForButton;
-(void)configureDecorationForExploreButton;
-(void)configureDecorationBlackTitleButton;
-(void)configureDecorationBlackTitleButtonForTouchingStatus;
-(void)setWhiteBackgroundColorForTouchingStatus;
-(void)centerVertically;
-(void)setUnderlineForTextWithDefaultColorForNormalStatus:(NSString*)title;
-(void)setUnderlineForTextWithCustomizedColorForTouchingStatus:(NSString*)title;
-(void)setUnderlineForText:(NSString*)title withColor:(UIColor *)color withStatus:(UIControlState)status;
@end
