//
//  SessionData.m
//  MobileConcierge
//
//  Created by user on 6/21/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "SessionData.h"
#import "Constant.h"
#import "NSData+AESCrypt.h"
#import "PreferenceObject.h"
@import AspireApiFramework;

@implementation SessionData
{
    UserObject *userObject;
    BOOL isShownLocationService;
    NSString *passcode;
}


+ (instancetype)shareSessiondata{
    static SessionData *sharedMySession = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMySession = [[self alloc] init];
    });
    return sharedMySession;
}

-(id)init {
    self = [super init];
    if (self != nil) {
        // initialize stuff here
    }
    return self;
}
/////////
//                                      UUID
/////////
- (void)setUUID:(NSString *)uuid{
    if (uuid) {
        [self setUserDefaultValue:uuid withKey:keyUUID];
    }
}
- (NSString *)UUID{
    return [self getUserDefaultValueWithKey:keyUUID];
}
/////////
//                                      Passcode
/////////
-(void)setPasscode:(NSString *)pc
{
    passcode = pc;
}

-(NSString *)passcode
{
    return [self getPasscode];
}
/////////
//                                      OnlineMemberID
/////////
- (void) setOnlineMemberID:(NSString *)onlineMemberID{
    if (onlineMemberID) {
        [self setUserDefaultValue:onlineMemberID withKey:keyOnlineMemberID];
    }
}
- (NSString *)OnlineMemberID{
    return [self getUserDefaultValueWithKey:keyOnlineMemberID];
}

/////////
//                                      OnlineMemberDetailIDs
/////////
- (void)setOnlineMemberDetailIDs:(NSString *)onlineMemberDetailIDs{
    if (onlineMemberDetailIDs) {
        [self setUserDefaultValue:onlineMemberDetailIDs withKey:keyOnlineMemberDetailIDs];
    }
}
- (NSString *)OnlineMemberDetailIDs{
    return [self getUserDefaultValueWithKey:keyOnlineMemberDetailIDs];
}
/////////
//                                      BINNumber
/////////
- (void)setBINNumber:(NSString *)bin{
    if (bin) {
        [self setUserDefaultValue:bin withKey:keyBINNumber];
    }
}
- (NSString *)BINNumber{
    if ([User isValid] && ![self getUserDefaultValueWithKey:keyBINNumber])
        return [[User current] getPasscode];
    
    return [self getUserDefaultValueWithKey:keyBINNumber];
}
/////////
//                                      RequestToken
/////////
- (void)setRequestToken:(NSString *)requestToken{
    if (requestToken) {
        [self setUserDefaultValue:requestToken withKey:keyRequestToken];
    }
}
- (NSString *)RequestToken{
    return [self getUserDefaultValueWithKey:keyRequestToken];
}
/////////
//                                      AccessToken
/////////
- (void)setAccessToken:(NSString *)accessToken{
    if (accessToken) {
        [self setUserDefaultValue:accessToken withKey:keyAccessToken];
    }
}
- (NSString *)AccessToken{
    return [self getUserDefaultValueWithKey:keyAccessToken];
}
/////////
//                                      RefreshToken
/////////
- (void) setRefreshToken:(NSString *)refreshToken{
    if (refreshToken) {
        [self setUserDefaultValue:refreshToken withKey:keyRefreshToken];
    }
}
- (NSString *)RefreshToken{
    return [self getUserDefaultValueWithKey:keyRefreshToken];
}
/////////
//                                      ExprirationTime
/////////
- (void) setExpirationTime:(NSNumber *)exprirationTime{
    if (exprirationTime) {
        [self setUserDefaultNumberValue:exprirationTime withKey:keyExprirationTime];
    }
}
- (NSNumber *)ExpirationTime{
    return [self getUserDefaultNumberValueWithKey:keyExprirationTime];
}
/////////
//                                      CurrentPolicyVersion
/////////
- (void) setCurrentPolicyVersion:(NSString *)currentPolicyVersion{
    if (currentPolicyVersion) {
        [self setUserDefaultValue:currentPolicyVersion withKey:keyCurrentPolicyVersion];
    }
}
- (NSString *)CurrentPolicyVersion{
    return [self getUserDefaultValueWithKey:keyCurrentPolicyVersion];
}

- (void)setIsUseLocation:(BOOL)isLocation {
    userObject.isUseLocation = isLocation;
}

- (BOOL)isUseLocation {
    return userObject.isUseLocation;
}

- (void)setIsShownLocationServiceAlert:(BOOL)isLocation {
    isShownLocationService = isLocation;
    /*
    [[NSUserDefaults standardUserDefaults] setBool:isLocation forKey:keyIsShownLocationServiceAlert];
    [[NSUserDefaults standardUserDefaults] synchronize];
     */
}

- (BOOL)isShownLocationServiceAlert {
    //return [[NSUserDefaults standardUserDefaults] boolForKey:keyIsShownLocationServiceAlert];
    return isShownLocationService;
}

- (void)setHasForgotAccount:(BOOL)isForgot {
    [[NSUserDefaults standardUserDefaults] setBool:isForgot forKey:keyHasForgotAccount];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)hasForgotAccount {
    return [[NSUserDefaults standardUserDefaults] boolForKey:keyHasForgotAccount];
}


/////////
//                                      Userobject
/////////
- (UserObject *)UserObject{
    return  userObject;
}

- (void)setUserObjectWithDict:(NSDictionary *)dict{
    
    userObject = [[UserObject alloc] init];
    userObject.firstName = [dict stringForKey:keyFirstName];
    userObject.lastName = [dict stringForKey:keyLastName];
    userObject.email = [dict stringForKey:keyEmail];
    userObject.mobileNumber = [dict stringForKey:keyMobileNumber];
    userObject.ConsumerKey = [dict stringForKey:keyConsumerKey];
    userObject.Program = [dict stringForKey:keyProgram];
    userObject.zipCode = [dict stringForKey:keyZipCode];
    userObject.salutation = [dict stringForKey:keySalutation];
    userObject.optStatus = [dict boolForKey:keyOptStatus];
    
    NSData *dataUser = [NSKeyedArchiver archivedDataWithRootObject:dict];
    
    if (dataUser) {
        if (![self isEncryptUserData]) {
            [self setIsEncryptUserData];
        }
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
        [[NSUserDefaults standardUserDefaults] setObject:dataUser forKey:keyProfile];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void) setUserObject:(UserObject *)userObject1 {
    
    if (userObject1 == nil) {
        userObject = nil;
        [ModelAspireApiManager logout];
        return;
    }
    
    userObject = userObject1;
    _arrayPreferences = [NSMutableArray arrayWithArray:userObject.arrayPreferences];
    NSData *dataUser = [NSKeyedArchiver archivedDataWithRootObject:[userObject convertToDictionary]];
    
    if (dataUser) {
        if (![self isEncryptUserData]) {
            [self setIsEncryptUserData];
        }
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
        [[NSUserDefaults standardUserDefaults] setObject:dataUser forKey:keyProfile];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)setArrayPreferences:(NSMutableArray *)arrayPreferences {
    _arrayPreferences = arrayPreferences;
    userObject.arrayPreferences = _arrayPreferences;
}

- (NSString *)getPasscode {
    return [[User current] getPasscode];
}

- (void) updateUserObjectFromAPI {
    
    if (![User isValid]) {
        return;
    }
    
    // update user & change to home
    UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
    NSMutableDictionary *userDict = [[NSMutableDictionary alloc] init];
    [userDict checkAndSetValue:userObject.salutation forKey:@"Salutation"];
    [userDict checkAndSetValue:userObject.firstName forKey:@"FirstName"];
    [userDict checkAndSetValue:userObject.lastName forKey:@"LastName"];
    [userDict checkAndSetValue:[userObject.mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""] forKey:@"MobileNumber"];
    [userDict checkAndSetValue:userObject.email forKey:@"Email"];
    [userDict checkAndSetValue:[[SessionData shareSessiondata] BINNumber] forKey:APP_USER_PREFERENCE_Passcode];
    [userDict checkAndSetValue:userObject.isUseLocation ? @"ON" : @"OFF" forKey:APP_USER_PREFERENCE_Location];
    if (userObject.currentCity.length > 0)
        [userDict checkAndSetValue:userObject.currentCity forKey:APP_USER_PREFERENCE_Selected_City];
    [userDict checkAndSetValue:[self getPreferenceByType:DiningPreferenceType].value forKey:@"Dining Preference"];
    [userDict checkAndSetValue:[self getPreferenceByType:HotelPreferenceType].value forKey:@"Hotel Preference"];
    [userDict checkAndSetValue:[self getPreferenceByType:TransportationPreferenceType].value forKey:@"Car Type Preference"];
    [userDict checkAndSetValue:[userObject.currentCity isEqualToString:@""] ? @"Unknown" : userObject.currentCity forKey:@"City"];
    [userDict checkAndSetValue:@"USA" forKey:@"Country"];
    [userDict checkAndSetValue:@"USA" forKey:@"homeCountry"];
    [userDict checkAndSetValue:APP_NAME forKey:@"referenceName"];
    
    if (userObject.currentCity) {
        if (userObject.currentCity.length > 0)
            [userDict setObject:userObject.currentCity forKey:APP_USER_PREFERENCE_Selected_City];
    }
    
    [ModelAspireApiManager updateProfileCurrentUserWithUserInfo:userDict completion:^(NSError *error) {
        if (!error) {
            [[SessionData shareSessiondata] setUserObject:(UserObject *)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([UserObject class])]];
        }
    }];
}


- (NSDictionary*)getUserInfo {
    NSData *dataUser = [[NSUserDefaults standardUserDefaults] dataForKey:keyProfile];
    if (dataUser) {
        if (![self isEncryptUserData]) {
            [self setIsEncryptUserData];
            [[NSUserDefaults standardUserDefaults] setObject:[dataUser AES256EncryptWithKey:EncryptKey] forKey:keyProfile];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }else {
            dataUser = [dataUser AES256DecryptWithKey:EncryptKey];
        }
    }
    return [NSKeyedUnarchiver unarchiveObjectWithData:dataUser];
}

-(BOOL) isEncryptUserData
{
    return [[NSUserDefaults standardUserDefaults]
            boolForKey:@"IsEncryptUserData"];
}

-(void) setIsEncryptUserData
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:YES] forKey:@"IsEncryptUserData"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

-(PreferenceObject*)getPreferenceByType:(PreferenceType)type{
    if ([SessionData shareSessiondata].arrayPreferences.count > 0) {
        for (PreferenceObject *preference in [SessionData shareSessiondata].arrayPreferences) {
            if (preference.type == type) {
                return preference;
            }
        }
    }
    return nil;
}
///*
    //
///*

- (void)setUserDefaultValue:(NSString*)value withKey:(NSString*)key{
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString*)getUserDefaultValueWithKey:(NSString*)key{
    return [[NSUserDefaults standardUserDefaults]
                            stringForKey:key];
}

- (void)setUserDefaultNumberValue:(NSNumber*)value withKey:(NSString*)key{
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSNumber*)getUserDefaultNumberValueWithKey:(NSString*)key{
    return (NSNumber *)[[NSUserDefaults standardUserDefaults]
            objectForKey:key];
}


@end
