//
//  MCActivityItemSource.h
//  MobileConcierge
//
//  Created by Chung Mai on 6/30/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MCActivityItemSource : NSObject<UIActivityItemSource>
@property (nonatomic, strong) NSArray *data;
@end
