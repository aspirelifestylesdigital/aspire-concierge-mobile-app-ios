//
//  CustomPopTransition.h
//  MobileConcierge
//
//  Created by Home on 5/25/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CustomPopTransition : NSObject <UIViewControllerAnimatedTransitioning>

@end
