//
//  UIView+Extension.h
//  MobileConcierge
//
//  Created by Home on 5/7/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Extension)

-(void)setBackgroundColorForView;
-(void)setBackgroundRedColorForView;
-(void)setBackgroundColoreForNavigationBar;
-(void)setbackgroundColorForLineMenu;
@end
