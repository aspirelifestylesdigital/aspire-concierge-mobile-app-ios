//
//  UILabel+Extension.h
//  MobileConcierge
//
//  Created by Home on 5/7/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Extension)

-(void)configureDecorationForLabel;
-(void)configureDecorationForGrayLabel;
-(void)setTextStyleForText:(NSString *)text WithFontName:(NSString *)fontName WithColor:(UIColor *)color WithTextSize:(float)textSize WithTextCenter:(BOOL)isCenter WithSpacing:(float)spacing;

- (NSAttributedString*)handleHTMLString:(NSString*)str;
- (NSAttributedString*)handleHTMLString:(NSString*)str witLineSpace:(CGFloat) lineSpacing;
- (void) setLineSpacing:(CGFloat) lineSpacing;
@end
