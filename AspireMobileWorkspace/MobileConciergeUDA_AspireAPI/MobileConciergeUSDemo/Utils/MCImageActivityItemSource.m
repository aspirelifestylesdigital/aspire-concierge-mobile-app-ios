//
//  MCImageActivityItemSource.m
//  MobileConcierge
//
//  Created by Chung Mai on 7/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "MCImageActivityItemSource.h"

@implementation MCImageActivityItemSource

- (id)activityViewControllerPlaceholderItem:(UIActivityViewController *)activityViewController
{
    return @"";
}
- (id)activityViewController:(UIActivityViewController *)activityViewController itemForActivityType:(NSString *)activityType
{
    if ([activityType isEqualToString:UIActivityTypeCopyToPasteboard])
    {
        return nil;
    }
    
    return [self.data objectAtIndex:2];
}

@end
