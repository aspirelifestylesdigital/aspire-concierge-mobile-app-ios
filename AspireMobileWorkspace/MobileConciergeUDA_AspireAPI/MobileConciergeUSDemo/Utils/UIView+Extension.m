//
//  UIView+Extension.m
//  MobileConcierge
//
//  Created by Home on 5/7/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "UIView+Extension.h"
#import "Common.h"
#import "Constant.h"
#import "UtilStyle.h"

@implementation UIView (Extension)
-(void)setBackgroundColorForView
{
    [self setBackgroundColor:[AppColor backgroundColor]];
}

-(void)setBackgroundRedColorForView
{
    [self setBackgroundColor:[UIColor colorWithRed:210.0f/255.0f green:10.0f/255.0f blue:59.0f/255.0f alpha:1.0f]];
}

-(void)setBackgroundColoreForNavigationBar
{
    [self setBackgroundColor:[UIColor colorWithRed:1.0f/255.0f green:22.0f/255.0f blue:39.0f/255.0f alpha:1.0f]];
}

-(void)setbackgroundColorForLineMenu
{
    [self setBackgroundColor:[AppColor lineViewBackgroundColor]];
}
@end
