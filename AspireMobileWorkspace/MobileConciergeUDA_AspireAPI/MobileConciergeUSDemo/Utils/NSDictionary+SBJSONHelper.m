#import "NSDictionary+SBJSONHelper.h"
#import "NSString+HTML.h"
#import "BaseResponseObject.h"

@implementation NSDictionary (SBJSONHelper)

- (void)setFloat:(float)value forKey:(id)key {
    [self setValue:[NSNumber numberWithFloat:value] forKey:key];
}

- (void)setInteger:(NSInteger)value forkey:(id)key {
    [self setValue:[NSNumber numberWithInteger:value] forKey:key];
}

- (void)setBool:(BOOL)value forKey:(id)key {
    [self setValue:[NSNumber numberWithBool:value] forKey:key];
}

- (void)setInt:(int)value forKey:(id)key {
    [self setValue:[NSNumber numberWithInt:value] forKey:key];
}

- (void)setLong:(long)value forKey:(id)key {
    [self setValue:[NSNumber numberWithLong:value] forKey:key];
}

- (void)setDouble:(double)value forKey:(id)key {
    [self setValue:[NSNumber numberWithDouble:value] forKey:key];
}

- (void)setCGFloat:(CGFloat)value forKey:(id)key {
    [self setValue:[NSNumber numberWithFloat:value] forKey:key];
}

- (NSString*)uuidForKey:(id)key
{
	id obj = [self objectForKey:key];
	if ( [obj isKindOfClass:[NSNumber class]] )
		return [obj stringValue];
	if ( [obj isKindOfClass:[NSString class]] )
		return obj;
	return nil;
}

- (NSString*)stringForKey:(id)key
{
	id obj = [self objectForKey:key];
    
	if ( [obj isKindOfClass:[NSString class]] )
        return [[obj stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] stringByDecodingHTMLEntities];
    
    if ( [obj isKindOfClass:[NSNumber class]]) {
        return [obj stringValue];
    }
    
	return @"";
}

- (NSString*)stringWithDefaultNilForKey:(id)key {
    id obj = [self objectForKey:key];
    
    if ( [obj isKindOfClass:[NSString class]] )
        return obj;
    
    if ( [obj isKindOfClass:[NSNumber class]]) {
        return [obj stringValue];
    }
    
    return nil;
}

- (NSArray*)arrayForKey:(id)key
{
	id obj = [self objectForKey:key];
	if ( [obj isKindOfClass:[NSArray class]] )
		return obj;
	return nil;
}

- (NSDate*)dateForKey:(id)key
{
	id obj = [self objectForKey:key];
    if ( [obj isKindOfClass:[NSNumber class]] ||
        [obj isKindOfClass:[NSString class]])
    {
        return [NSDate dateWithTimeIntervalSince1970:( [obj doubleValue])];
    }
    
    return nil;
}

- (NSNumber*)numberForKey:(id)key
{
	id obj = [self objectForKey:key];
    if ( [obj isKindOfClass:[NSNumber class]] )
        return obj;
    
	return nil;
}

- (NSDictionary*)dictionaryForKey:(id)key
{
	id obj = [self objectForKey:key];
	if ( [obj isKindOfClass:[NSDictionary class]] )
		return obj;
	return nil;
}

- (NSInteger)integerForKey:(id)key {
    id obj = [self objectForKey:key];
    if ( [obj isKindOfClass:[NSNumber class]] ||
        [obj isKindOfClass:[NSString class]])
    {
        return [obj integerValue];
    }
    
    return 0;
}

- (float)floatForKey:(id)key {
    id obj = [self objectForKey:key];
    if ( [obj isKindOfClass:[NSNumber class]] ||
        [obj isKindOfClass:[NSString class]])
    {
        return [obj floatValue];
    }
    
    return 0;
}

- (double)doubleForKey:(id)key {
    id obj = [self objectForKey:key];
    if ( [obj isKindOfClass:[NSNumber class]] ||
        [obj isKindOfClass:[NSString class]])
    {
        return [obj doubleValue];
    }
    
    return 0;
}

- (BOOL)canConvertToNumberForKey:(id)key {
    id obj = [self objectForKey:key];
    if ( [obj isKindOfClass:[NSNumber class]] ||
        [obj isKindOfClass:[NSString class]])
    {
        return YES;
    }
    
    return NO;
}

- (BOOL)isNilOrNullForKey:(id)key {
    id obj = [self objectForKey:key];
    
    if (obj == nil)
        return YES;

    if ([obj isKindOfClass:[NSNull class]]) {
        return YES;
    }
    
    return NO;
}

- (long)longForKey:(id)key {
    id obj = [self objectForKey:key];
    if ( [obj isKindOfClass:[NSNumber class]] ||
        [obj isKindOfClass:[NSString class]])
    {
        return [obj longValue];
    }
    
    return 0;
}

- (int)intForKey:(id)key {
    id obj = [self objectForKey:key];
    if ( [obj isKindOfClass:[NSNumber class]] ||
        [obj isKindOfClass:[NSString class]])
    {
        return [obj intValue];
    }
    
    return 0;
}

- (int)intWithDefaultMinusOneForKey:(id)key {
    id obj = [self objectForKey:key];
    
    if (!obj || [obj isKindOfClass:[NSNull class]]) {
        return -1;
    }
    
    if ( [obj isKindOfClass:[NSNumber class]] ||
        [obj isKindOfClass:[NSString class]])
    {
        return [obj intValue];
    }
    
    return 0;
}

- (BOOL)boolForKey:(id)key {
    id obj = [self objectForKey:key];
    if ( [obj isKindOfClass:[NSNumber class]] ||
        [obj isKindOfClass:[NSString class]])
    {
        return [obj boolValue];
    }
    
    return NO;
}

- (NSArray*)arrayForKey:(id)key withObjectClass:(Class) aClass{
    if([aClass instancesRespondToSelector:@selector(initFromDict:)]){
        NSArray *temps = [self arrayForKey:key];
        NSMutableArray *result = [NSMutableArray array];
        for (NSDictionary *item in temps) {
            if([item isKindOfClass:[NSDictionary class]]){
                [result addObject:[[aClass alloc] initFromDict:item]];
            }
        }
        return result;
    } else {
        return [self arrayForKey:key];
    }
}

-(NSArray *)arrayForKey:(id)key withProcessing:(id<BaseResponseObjectProtocol>)responseObject toObject:(NSString *)className
{
    if([responseObject respondsToSelector:@selector(parseJson:toObject:)]){
        NSArray *temps = [self arrayForKey:key];
        NSMutableArray *result = [NSMutableArray array];
        for (NSDictionary *item in temps) {
            if([item isKindOfClass:[NSDictionary class]]){
                id itemData = [responseObject parseJson:item toObject:className];
                if(itemData)
                {
                    [result addObject:itemData];
                }
            }
        }
        return result;
    } else {
        return [self arrayForKey:key];
    }
}

- (void)checkAndSetValue:(id)value forKey:(NSString *)key {
    if (value == nil) return;
    [self setValue:value forKey:key];
}
@end
