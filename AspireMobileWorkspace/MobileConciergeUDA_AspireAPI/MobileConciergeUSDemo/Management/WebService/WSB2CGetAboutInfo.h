//
//  WSB2CGetAboutInfo.h
//  MobileConcierge
//
//  Created by Chung Mai on 6/19/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSB2CBase.h"

@interface WSB2CGetAboutInfo : WSB2CBase

-(void)retrieveDataFromServer;


@end
