//
//  WSB2CGetQuestionAndAnswers.h
//  MobileConcierge
//
//  Created by Chung Mai on 6/23/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSB2CBase.h"

@interface WSB2CGetQuestionAndAnswers : WSB2CBase

@property(strong, nonatomic) NSString *categoryId;
@property(strong, nonatomic) NSString *questionId;

-(void)retrieveDataFromServer;

@end
