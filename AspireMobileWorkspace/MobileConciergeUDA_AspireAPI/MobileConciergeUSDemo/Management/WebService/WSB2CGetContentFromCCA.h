//
//  WSB2CGetContentFromCCA.h
//  MobileConcierge
//
//  Created by Chung Mai on 6/5/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSB2CBase.h"

@interface WSB2CGetContentFromCCA : WSB2CBase

@property (nonatomic, strong) NSArray *categories;

-(void) loadContentFulls;

@end
