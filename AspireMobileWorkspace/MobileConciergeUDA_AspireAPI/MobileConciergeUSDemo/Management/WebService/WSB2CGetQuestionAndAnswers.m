//
//  WSB2CGetQuestionAndAnswers.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/23/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSB2CGetQuestionAndAnswers.h"
#import "QuestionResponseObject.h"

@implementation WSB2CGetQuestionAndAnswers


-(void) retrieveDataFromServer
{
    NSString* url = [B2C_IA_API_URL stringByAppendingString:GetQuestionAndAnswers];
    [self POST:url withParams:[self buildRequestParams]];
}


-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    
    // cause of security scanning, not allow get and set password directly
    NSString* userPass = B2C_PW;
    [dictKeyValues setObject:B2C_SUBDOMAIN forKey:@"subDomain"];
    [dictKeyValues setObject:userPass forKey:@"password"];
    [dictKeyValues setObject:self.questionId forKey:@"questionID"];
    [dictKeyValues setObject:self.categoryId forKey:@"categoryID"];
    if(self.pageSize > 0)
    {
        [dictKeyValues setObject:[NSNumber numberWithInteger:self.pageSize] forKey:@"pageSize"];
        [dictKeyValues setObject:[NSNumber numberWithInteger:self.pageIndex] forKey:@"pageNumber"];
    }
    return dictKeyValues;
}


-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        QuestionResponseObject *response = [[QuestionResponseObject alloc] init];
        response.categoryCode = self.categoryCode;
        response.categoryName = self.categoryName;
        response = [response initForSearchItemFromDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && response.isSuccess){
            self.data = [response data];
            self.isHasNextPage = self.data.count == 0 ? NO : YES;
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.isSuccess];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}


-(void)processDataResultWithError:(NSError *)error
{
    [self.delegate loadDataFailFrom:nil withErrorCode:error.code errorMessage:error.localizedDescription];
}


@end
