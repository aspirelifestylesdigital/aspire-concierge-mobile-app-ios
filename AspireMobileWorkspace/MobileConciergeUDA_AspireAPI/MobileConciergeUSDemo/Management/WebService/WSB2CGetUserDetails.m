//
//  WSB2CGetUserDetails.m
//  ALC
//
//  Created by Anh Tran on 11/3/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CGetUserDetails.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "NSDictionary+SBJSONHelper.h"
#import "BaseResponseObject.h"
#import "AppDelegate.h"
#import "GetUserDetailsResponseObject.h"


@interface WSB2CGetUserDetails()<DataLoadDelegate>

@end

@implementation WSB2CGetUserDetails
-(void) getUserDetails{
    [self startAPIAfterAuthenticate];
}

-(void)startAPIAfterAuthenticate{
    self.task = WS_B2C_GET_USER_DETAILS;
    self.subTask = WS_ST_NONE;
    
    NSString* url = [MCD_API_URL stringByAppendingString:GetUserDetailsUrl];
    if ([[SessionData shareSessiondata] AccessToken]) {
        [self POST:url withParams:[self buildRequestParams]];
    }
}

-(void) processDataResults:(NSDictionary*)jsonResult forTask:(NSInteger)ptask forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat
{
    if(jsonResult){
        GetUserDetailsResponseObject *result = [[GetUserDetailsResponseObject alloc] initFromDict:jsonResult];
        if(result.isSuccess){
            
            NSString *email = [jsonResult[keyMember] stringForKey:keyEmail.uppercaseString];
            NSString *salutation = [jsonResult[keyMember] stringForKey:keySalutation.uppercaseString];
            NSString *firstName = [jsonResult[keyMember] stringForKey:keyFirstName.uppercaseString];
            NSString *lastName = [jsonResult[keyMember] stringForKey:keyLastName.uppercaseString];
            NSString *mobileNumber = [jsonResult[keyMember] stringForKey:keyMobileNumber.uppercaseString];
            NSString *program = [jsonResult[keyMember] stringForKey:keyProgram.uppercaseString];
            NSString *onlineMemberID = [jsonResult[keyMember] stringForKey:keyOnlineMemberID.uppercaseString];
            
            NSMutableDictionary *currentProfile = [[[SessionData shareSessiondata] getUserInfo] mutableCopy];
            if (currentProfile == nil) {
                currentProfile = [[NSMutableDictionary alloc] init];
            }
            if (email.length > 0) {
                [currentProfile setValue:email forKey:keyEmail];
            }
            if (salutation.length > 0) {
                salutation = [salutation stringByReplacingOccurrencesOfString:@"." withString:@""];
                [currentProfile setValue:salutation forKey:keySalutation];
            }
            if (firstName.length > 0) {
                [currentProfile setValue:firstName forKey:keyFirstName];
            }
            if (lastName.length > 0) {
                [currentProfile setValue:lastName forKey:keyLastName];
            }
            if (mobileNumber.length > 0) {
                mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
                mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
                if (mobileNumber.length > 15) {
                    mobileNumber = [mobileNumber substringToIndex:15];
                }
                [currentProfile setValue:mobileNumber forKey:keyMobileNumber];
            }
            if (program.length > 0) {
                [currentProfile setValue:program forKey:keyProgram];
            }
            if (onlineMemberID.length > 0) {
                [currentProfile setValue:onlineMemberID forKey:keyOnlineMemberID];
            }
            
            if(currentProfile){
                NSArray* array=[jsonResult arrayForKey:keyMemberDetails];
                if(array.count>0){
                    NSDictionary *memberDetails = [array objectAtIndex:0];
                    [[SessionData shareSessiondata] setOnlineMemberDetailIDs:[memberDetails stringForKey:keyOnlineMemberID.uppercaseString]];
                    [[SessionData shareSessiondata] setOnlineMemberID:[currentProfile stringForKey:keyOnlineMemberID]];
                    if (![[SessionData shareSessiondata] CurrentPolicyVersion]) {
                        [[SessionData shareSessiondata] setCurrentPolicyVersion:[memberDetails stringForKey:keyCurrentPolicyVersion]];
                    }
                    [currentProfile setBool:[memberDetails boolForKey:keyOptStatus.uppercaseString] forKey:keyOptStatus];
                }
                
                self.userDetails = [[UserObject alloc] initFromDict:currentProfile];
                [[SessionData shareSessiondata] setUserObjectWithDict:currentProfile];
                [self.delegate loadDataDoneFrom:self];
            }
        } else if(self.delegate != nil){
            
            [self.delegate loadDataFailFrom:nil withErrorCode:400 errorMessage:result.message];
        }
    }else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}

-(void)processDataResultWithError:(NSError *)error
{
    BaseResponseObject *result = [[BaseResponseObject alloc] init];
    result.task = self.task;
    [self.delegate loadDataFailFrom:result withErrorCode:error.code];
}

-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    [dictKeyValues setObject:[[SessionData shareSessiondata] AccessToken] forKey:@"AccessToken"];
    [dictKeyValues setObject:@"GetUserDetails" forKey:@"Functionality"];
    [dictKeyValues setObject:B2C_ConsumerKey forKey:@"ConsumerKey"];
    [dictKeyValues setObject:[[SessionData shareSessiondata] OnlineMemberID] forKey:@"OnlineMemberId"];
    [dictKeyValues setObject:B2C_MemberRefNo forKey:@"MemberRefNo"];
        
    return dictKeyValues;
}

@end
