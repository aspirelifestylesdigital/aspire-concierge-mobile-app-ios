//
//  WSB2CGetAccessToken.m
//  ALC
//
//  Created by Anh Tran on 11/1/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CGetAccessToken.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "BaseResponseObject.h"
#import "NSDictionary+SBJSONHelper.h"
#import "AppDelegate.h"
#import "EnumConstant.h"

@interface WSB2CGetAccessToken (){
    
}

@end

@implementation WSB2CGetAccessToken
-(void) requestAccessToken:(NSString*) token member:(NSString*) memberId{
    self.subTask = WS_ST_NONE;
    self.task = WS_GET_ACCESS_TOKEN;
    
    onlineMemberId = memberId;
    requestToken = token;
    if(requestToken && onlineMemberId){
        NSString* url = [MCD_API_URL stringByAppendingString:GetAccessToken];
        NSString* urlString = [NSString stringWithFormat:@"%@?OnlineMemberId=%@&RequestToken=%@&ConsumerKey=%@&CallBackURL=%@",url,onlineMemberId,requestToken,B2C_ConsumerKey,B2C_Callback_Url];
        [self GET:urlString withParams:nil];
    }
}

-(void) processDataResults:(NSDictionary*)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat
{
    if(jsonResult){
        _accessToken = [jsonResult stringForKey:@"AccessToken"];
        _refreshToken = [jsonResult stringForKey:@"RefreshToken"];
        
        // store data
        [[SessionData shareSessiondata] setAccessToken:[jsonResult stringForKey:@"AccessToken"]];
        [[SessionData shareSessiondata] setRequestToken:[jsonResult stringForKey:@"RefreshToken"]];
        [[SessionData shareSessiondata] setExpirationTime:[jsonResult numberForKey:@"ExpirationTime"]];
        if(_accessToken!=nil && _accessToken.length > 0){
            if(self.delegate != nil)
                [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:nil withErrorCode:kSTATUSTOKENEXPIRE];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}

-(void)processDataResultWithError:(NSError *)error
{
    [self.delegate loadDataFailFrom:nil withErrorCode:error.code];
}

-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    [dictKeyValues setObject:B2C_ConsumerKey forKey:@"ConsumerKey"];
    //[dictKeyValues setObject:B2C_ConsumerSecret forKey:@"ConsumerSecret"];
    [dictKeyValues setObject:B2C_Callback_Url forKey:@"CallBackURL"];
    [dictKeyValues setObject:onlineMemberId forKey:@"OnlineMemberId"];
    [dictKeyValues setObject:encodeURLFromString(requestToken) forKey:@"RequestToken"];
    return dictKeyValues;
}

@end
