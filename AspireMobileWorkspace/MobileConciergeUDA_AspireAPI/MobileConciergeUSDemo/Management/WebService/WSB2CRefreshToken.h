//
//  WSB2CRefreshToken.h
//  ALC
//
//  Created by Anh Tran on 11/1/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"

@interface WSB2CRefreshToken : WSB2CBase

-(void) refreshAccessToken;
-(void) refreshAccessToken:(NSString*) token requestToken:(NSString*) requestToken;
@end
