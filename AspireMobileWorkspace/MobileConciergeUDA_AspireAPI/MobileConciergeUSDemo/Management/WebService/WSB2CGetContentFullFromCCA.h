//
//  WSB2CGetContentFullFromCCA.h
//  MobileConcierge
//
//  Created by user on 6/12/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSB2CBase.h"

@interface WSB2CGetContentFullFromCCA : WSB2CBase

@property (nonatomic, strong) NSString *itemID;
- (void) loadDataFromServer;

@end
