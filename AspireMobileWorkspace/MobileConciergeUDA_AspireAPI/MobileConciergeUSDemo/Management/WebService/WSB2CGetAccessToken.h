//
//  WSB2CGetAccessToken.h
//  ALC
//
//  Created by Anh Tran on 11/1/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"

@interface WSB2CGetAccessToken : WSB2CBase
{
    NSString *onlineMemberId;
    NSString *requestToken;
}

@property (strong, nonatomic) NSString *accessToken;
@property (strong, nonatomic) NSString *refreshToken;
//@property (strong, nonatomic) NSNumber *expirationTime;

-(void) requestAccessToken:(NSString*) token member:(NSString*) memberId;
@end
