//
//  WSB2CGetTermsOfUse.h
//  MobileConcierge
//
//  Created by user on 6/26/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSB2CBase.h"

@interface WSB2CGetTermsOfUse : WSB2CBase
-(void)retrieveDataFromServer;
@end
