//
//  WSB2CGetRequestToken.h
//  ALC
//
//  Created by Anh Tran on 11/1/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"

@interface WSB2CGetRequestToken : WSB2CBase
@property (strong, nonatomic) NSString* requestToken;
@property (strong, nonatomic) NSString* onlineMemberId;
-(void) getRequestToken;
@end
