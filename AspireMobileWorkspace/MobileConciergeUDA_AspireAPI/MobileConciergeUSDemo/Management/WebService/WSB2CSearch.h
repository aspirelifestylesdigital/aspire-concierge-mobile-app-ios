//
//  WSB2CSearch.h
//  MobileConcierge
//
//  Created by Chung Mai on 6/21/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSB2CBase.h"
#import "CityItem.h"

@interface WSB2CSearch : WSB2CBase

@property(nonatomic, strong) NSString *searchText;
@property(nonatomic, strong) NSArray *cities;
@property(nonatomic, assign) BOOL hasOffer;
@property(nonatomic, strong) CityItem *currentCity;


-(void)retrieveDataFromServer;

@end
