//
//  WSB2CBINVerification.h
//  MobileConcierge
//
//  Created by Home on 5/6/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSB2CBase.h"

@interface WSB2CBINVerification : WSB2CBase<DataLoadDelegate>
-(void)verifyBINNumber:(NSString *) binNumber;
@end
