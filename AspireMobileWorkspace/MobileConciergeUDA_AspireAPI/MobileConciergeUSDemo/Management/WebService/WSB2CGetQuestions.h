//
//  WSB2CGetQuestions.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/31/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSB2CBase.h"

@interface WSB2CGetQuestions : WSB2CBase

@property(strong, nonatomic) NSString *categoryId;

-(void)loadQuestionsForCategory;

@end
