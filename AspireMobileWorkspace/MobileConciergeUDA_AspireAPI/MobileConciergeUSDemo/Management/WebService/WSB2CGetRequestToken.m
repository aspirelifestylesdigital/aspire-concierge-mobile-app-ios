//
//  WSB2CGetRequestToken.m
//  ALC
//
//  Created by Anh Tran on 11/1/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CGetRequestToken.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "BaseResponseObject.h"
#import "Common.h"
#import "NSDictionary+SBJSONHelper.h"
#import "AppDelegate.h"
#import "UserObject.h"

#import "SyncService.h"
@interface WSB2CGetRequestToken (){
}

@end

@implementation WSB2CGetRequestToken
-(void) getRequestToken{
    self.subTask = WS_ST_NONE;
    self.task = WS_GET_REQUEST_TOKEN;
    if(!isNetworkAvailable())
    {
        [self showErrorNetwork];
        if([self.delegate respondsToSelector:@selector(WSBaseNetworkUnavailable)]) {
            [self.delegate WSBaseNetworkUnavailable];
        }
    }else{
        typeof(self) __weak _self =self;
        [SyncService requestTokenOnDone:^(id responseObject) {
            typeof(self) __weak __self =_self;
            [__self processDataResults:responseObject forTask:self.task forSubTask:self.subTask returnFormat:400];
        } onError:^(NSError* error){
            typeof(self) __weak __self =_self;
            [__self processDataResultWithError:error];
        }];
    }
    
    
}

-(void) processDataResults:(NSDictionary*)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat
{
    if(jsonResult){
        NSString *token = [jsonResult stringForKey:@"RequestToken"];
        if(token!=nil && token.length > 0){
            _requestToken = token;
//            _onlineMemberId = [jsonResult stringForKey:@"OnlineMemberId"];
            if(self.delegate)
                [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:nil withErrorCode:kSTATUSTOKENEXPIRE];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}

-(void)processDataResultWithError:(NSError *)error
{
    if(self && self.delegate){
       [self.delegate loadDataFailFrom:nil withErrorCode:error.code];
    }
}

-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
//    [dictKeyValues setObject:B2C_ConsumerKey forKey:@"ConsumerKey"];
//    [dictKeyValues setObject:B2C_ConsumerSecret forKey:@"ConsumerSecret"];
    [dictKeyValues setObject:B2C_Callback_Url forKey:@"CallBackURL"];
    [dictKeyValues setObject:[[SessionData shareSessiondata] OnlineMemberID] forKey:@"OnlineMemberId"];
    [dictKeyValues setObject:B2C_DeviceId forKey:@"MemberDeviceId"];
    
    return dictKeyValues;
}

@end
