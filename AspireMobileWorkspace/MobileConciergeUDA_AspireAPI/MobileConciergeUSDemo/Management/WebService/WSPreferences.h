//
//  WSPreferences.h
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 7/25/17.
//  Copyright © 2017 Sunrixe Software Solutions Corporation. All rights reserved.
//

#import "WSB2CBase.h"

@interface WSPreferences : WSB2CBase <DataLoadDelegate>

@property (strong, nonatomic) NSMutableDictionary *dataDict;

-(void)addPreference:(NSMutableDictionary*)data;

-(void)updatePreference:(NSMutableDictionary*)data;

-(void)getPreference;

@end
