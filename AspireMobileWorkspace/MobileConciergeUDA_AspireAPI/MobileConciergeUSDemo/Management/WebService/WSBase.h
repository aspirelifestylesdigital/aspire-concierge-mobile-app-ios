//
//  WSBase.h
//  MobileMap
//
//  Created by Huy Tran on 5/10/12.
//  Copyright (c) 2012 S3Corp. All rights reserved.
//
@class BaseResponseObject;

#import "Common.h"
#import "SBJson.h"
#import "Constant.h"
#import "BaseResponseObjectProtocol.h"
#import "WSBaseProtocol.h"
#import "EnumConstant.h"
@protocol DataLoadDelegate;

@interface WSBase : NSObject<WSBaseProtocol>
{
    SBJsonParser *jsonParser;
}
@property (atomic, strong)  NSURLSessionDataTask *servicetask;
@property (nonatomic, weak) id<DataLoadDelegate>delegate;
@property (nonatomic, assign) enum WSTASK task;
@property (nonatomic, assign) enum WS_SUB_TASK subTask;
@property (nonatomic, strong) NSArray *data;
@property (nonatomic, assign) NSInteger pageIndex;
@property (nonatomic, assign) NSInteger pageSize;
@property (nonatomic, assign) BOOL isHasNextPage;
@property (nonatomic, assign) NSUInteger session;
//@property (strong, nonatomic) SuccessObj* successObj;

-(void)startAPIAfterAuthenticate;
-(void)checkAuthenticate;
-(void) processDataResults:(NSDictionary*)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat;
-(void)processDataResultWithError:(NSError *)errorCode;

-(void)cancelRequest;

-(void)POST:(NSString*) url withParams:(NSMutableDictionary*)params;
-(void)GET:(NSString*) url withParams:(NSMutableDictionary*)params;
-(void) nextPage;
-(BOOL) hasNextItem;

@end

@protocol DataLoadDelegate <NSObject>

//@optional
-(void)loadDataDoneFrom:(id<WSBaseProtocol>)ws;
-(void)loadDataFailFrom:(id<BaseResponseObjectProtocol>) result withErrorCode:(NSInteger)errorCode;
@optional
-(void)loadDataFailFrom:(id<WSBaseProtocol>)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message;
-(void)WSBaseNetworkUnavailable;

@end


