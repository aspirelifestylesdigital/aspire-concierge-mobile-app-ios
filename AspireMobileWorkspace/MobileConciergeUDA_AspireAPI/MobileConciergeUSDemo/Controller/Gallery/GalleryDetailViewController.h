//
//  GalleryDetailViewController.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/8/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@protocol ChangingBackgroundColorDelegate <NSObject>

-(void)setColorForPage:(NSInteger)currentPageIndex;
-(void)getMenuView:(UIPanGestureRecognizer *)panGesture;
-(void)updatePageControlPosition:(CGFloat)yPos;

@end

@interface GalleryDetailViewController : BaseViewController

@property (assign, nonatomic) NSInteger index;
@property (nonatomic, weak) IBOutlet UIImageView* galleryImageView;
@property (nonatomic, weak) IBOutlet UILabel* galleryTitle;
@property (nonatomic, weak) IBOutlet UILabel* galleryDescription;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionLabelTopConstraint;

@property (nonatomic, copy) NSString *titleText;
@property (nonatomic, copy) NSString *descriptionText;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, copy) NSString *colorHex;
@property (nonatomic, weak) id<ChangingBackgroundColorDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *panToMenuView;
@property (weak, nonatomic) IBOutlet UIView *dummyViewForPageControl;

@end
