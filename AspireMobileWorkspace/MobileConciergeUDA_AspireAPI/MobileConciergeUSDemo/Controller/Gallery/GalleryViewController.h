//
//  GalleryViewController.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/8/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface GalleryViewController : BaseViewController<UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@property (nonatomic, strong) UIPageViewController *pageViewController;
@property (nonatomic, strong) NSArray *galleryList;
@property (nonatomic, weak) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIView *dummyNavigationBar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pageControlBottomConstraint;

@end
