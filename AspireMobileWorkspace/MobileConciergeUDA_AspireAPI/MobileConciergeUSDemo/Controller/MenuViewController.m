//
//  MenuViewController.m
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/12/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "MenuViewController.h"
#import "SWRevealViewController.h"
#import "Constant.h"
#import "UIView+Extension.h"
#import "AppData.h"
#import "ExploreViewController.h"
#import "AskConciergeViewController.h"
#import "AboutAppViewController.h"
#import "TermsOfUseViewController.h"
#import "PrivacyPolicyViewController.h"
#import "SWRevealViewController.h"

//
#import "HomeViewController.h"
#import "MyProfileViewController.h"
#import "PreferencesViewController.h"
#import "AppDelegate.h"
#import "Common.h"
#import "PolicyViewController.h"
#import "UtilStyle.h"
#import "CityItem.h"
#import "CityViewController.h"
#import "SignInViewController.h"
#import "ChangePasswordViewController.h"
#import "PreferenceObject.h"
#import "WSPreferences.h"
#import "PasscodeViewController.h"
@import AspireApiFramework;

@interface MenuViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    NSIndexPath *presentedRow;
    AppDelegate *appDelegate;
    WSPreferences *wsPreferences;
}


@property (nonatomic, strong) NSDictionary *menuDataDict;
@property (nonatomic, strong) NSArray *menuSectionTitle;
@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createMenuBarButton];
    
    self.menuDataDict = [AppData getMenuItems];
    self.menuSectionTitle = [self.menuDataDict allKeys];
    
    self.menuTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.menuTableView setBackgroundColorForView];
    self.menuTableView.alwaysBounceVertical = NO;
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(displayFrontView:)];
    [self.view addGestureRecognizer:panGesture];
    
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"", nil)];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

-(void)createMenuBarButton
{
    SWRevealViewController *revealViewController = [self revealViewController];
    [revealViewController tapGestureRecognizer];

    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [menuButton addTarget:revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [menuButton setBackgroundImage:[UIImage imageNamed:@"menu_white_icon"] forState:UIControlStateNormal];
    [menuButton setBackgroundImage:[UIImage imageNamed:@"menu_interaction_icon"] forState:UIControlStateHighlighted];
    UIBarButtonItem *menuButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    self.navigationItem.leftBarButtonItem = menuButtonItem;
}

-(void)displayFrontView:(UIPanGestureRecognizer*)panGesture
{
    SWRevealViewController *revealViewController = [self revealViewController];
    [revealViewController _handleRevealGesture:panGesture];
}
-(void)askConciergeAction:(id)sender
{
    SWRevealViewController *revealController = self.revealViewController;
    UIViewController *newFrontController = nil;
    newFrontController = [[AskConciergeViewController alloc] init];
    if([revealController.frontViewController isKindOfClass:[UINavigationController class]])
    {
        UINavigationController *currentNavigation = (UINavigationController *)revealController.frontViewController;
        [currentNavigation pushViewController:newFrontController animated:NO];
        [revealController revealToggle:nil];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.menuTableView setUserInteractionEnabled:YES];
    UIColor *backgroundColor = [AppColor backgroundColor];
    [self setNavigationBarWithColor:backgroundColor];
    [self.view setBackgroundColor:backgroundColor];
    [self.menuTableView setBackgroundColor:backgroundColor];
    
    // Refine index menu 
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    UINavigationController *currentNavigation = (UINavigationController *)self.revealViewController.frontViewController;
//    if(currentNavigation.navigationController.viewControllers.count > 0) {
        if([[currentNavigation.viewControllers lastObject] isKindOfClass:[AskConciergeViewController class]]) {
            indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        } else if([[currentNavigation.viewControllers lastObject] isKindOfClass:[ExploreViewController class]]) {
            indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
        } else if([[currentNavigation.viewControllers lastObject] isKindOfClass:[MyProfileViewController class]]) {
            indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
        }
        presentedRow = indexPath;
//    }
    [self.menuTableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma marl - UITableView Data Source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0f * SCREEN_SCALE;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    {
        return 0.0f;
    }
    return 1.0f;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, SCREEN_WIDTH, 1.0f)];
    UIView* line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH*90/100, 1)];
    [headerView addSubview:line];
    [line setBackgroundColor:[AppColor separatingLineColor]];
    return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *menuSection = [self.menuDataDict allKeys];
    NSString *menuTitle = [menuSection objectAtIndex:section];
    NSArray *allValuesInSection = [self.menuDataDict objectForKey:menuTitle];
    return allValuesInSection.count;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.menuDataDict allKeys].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"MenuCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (nil == cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    NSString *sectionTitle = [self.menuSectionTitle objectAtIndex:indexPath.section];
    NSArray *valuesInSection = [self.menuDataDict objectForKey:sectionTitle];
    cell.textLabel.text = [valuesInSection objectAtIndex:indexPath.row];
    if (presentedRow.section == 2 && presentedRow.row == 3) {
        presentedRow = indexPath;
    }
    if (presentedRow.row == indexPath.row && presentedRow.section == indexPath.section) {
        cell.textLabel.textColor = [AppColor highlightTextColor];
    }else{
        cell.textLabel.textColor = [UIColor whiteColor];
    }
    cell.textLabel.attributedText = [[NSAttributedString alloc] initWithString:[valuesInSection objectAtIndex:indexPath.row] attributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize titleTextSize]],NSKernAttributeName:@(1.2)}];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setBackgroundColor:[UIColor clearColor]];
    
    
    if(indexPath.row == 4 && indexPath.section == 2)
    {
        if([[AppData getInstance] isValidPasscode])
        {
            cell.textLabel.text = @"INVALID PASSCODE";
        }
        else
        {
            cell.textLabel.text = @"VALID PASSCODE";
        }
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Set back default Color
    [self.menuTableView setUserInteractionEnabled:NO];
    appDelegate.currentCollorForMenu = [AppColor backgroundColor];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:presentedRow];
    cell.textLabel.textColor = [UIColor whiteColor];
    
    cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.textColor = [AppColor highlightTextColor];
    
    presentedRow = indexPath;

    SWRevealViewController *revealController = self.revealViewController;
    UIViewController *newFrontController = nil;

    if(indexPath.section == 0)
    {
        if(indexPath.row == 0)
        {
            newFrontController = [[HomeViewController alloc] init];
            
        }
        else if(indexPath.row == 1)
        {
            newFrontController = [[AskConciergeViewController alloc] init];
            if([revealController.frontViewController isKindOfClass:[UINavigationController class]])
            {
                UINavigationController *currentNavigation = (UINavigationController *)revealController.frontViewController;
                [currentNavigation pushViewController:newFrontController animated:NO];
                [revealController revealToggle:nil];
                return;
            }
        }
        else if(indexPath.row == 2)
        {
            CityItem *selectedCity = [AppData getSelectedCity];
            if(selectedCity)
            {
                UINavigationController *currentNavigation = (UINavigationController *)revealController.frontViewController;
                for (UIViewController* viewController in currentNavigation.viewControllers) {
                    if([viewController isKindOfClass:[ExploreViewController class]]) {
                        newFrontController = viewController;
                        break;
                    }
                }
                if(!newFrontController) {
                    ExploreViewController *exploreViewController = [[ExploreViewController alloc] init];
                    exploreViewController.currentCity = selectedCity;
                    newFrontController = exploreViewController;
                }
            }
            else
            {
                newFrontController = [[CityViewController alloc] init];
            }
            
        }
    }
    else if(indexPath.section == 1)
    {
        if(indexPath.row == 0)
        {
            newFrontController = [[MyProfileViewController alloc] init];
            if([revealController.frontViewController isKindOfClass:[UINavigationController class]])
            {
                UINavigationController *currentNavigation = (UINavigationController *)revealController.frontViewController;
                [currentNavigation pushViewController:newFrontController animated:NO];
                [revealController revealToggle:nil];
                return;
            }
        }else if(indexPath.row == 1){
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            PreferencesViewController *preferenceViewController = [storyboard instantiateViewControllerWithIdentifier:@"PreferencesViewController"];
            newFrontController = preferenceViewController;
            
            if([revealController.frontViewController isKindOfClass:[UINavigationController class]]) {
                UINavigationController *currentNavigation = (UINavigationController *)revealController.frontViewController;
                [currentNavigation pushViewController:newFrontController animated:NO];
                [revealController revealToggle:nil];
                return;
            }
        }else if (indexPath.row == 2) {
            newFrontController = [[ChangePasswordViewController alloc] init];
            if([revealController.frontViewController isKindOfClass:[UINavigationController class]])
            {
                UINavigationController *currentNavigation = (UINavigationController *)revealController.frontViewController;
                [currentNavigation pushViewController:newFrontController animated:NO];
                [revealController revealToggle:nil];
                return;
            }
        }
    }
    else if(indexPath.section == 2)
    {
        if(indexPath.row == 0)
        {
            newFrontController = [[AboutAppViewController alloc] init];
            if([revealController.frontViewController isKindOfClass:[UINavigationController class]])
            {
                UINavigationController *currentNavigation = (UINavigationController *)revealController.frontViewController;
                [currentNavigation pushViewController:newFrontController animated:NO];
                [revealController revealToggle:nil];
                return;
            }
        }
        else if(indexPath.row == 1)
        {
            newFrontController = [[PolicyViewController alloc] init];
            if([revealController.frontViewController isKindOfClass:[UINavigationController class]])
            {
                UINavigationController *currentNavigation = (UINavigationController *)revealController.frontViewController;
                [currentNavigation pushViewController:newFrontController animated:NO];
                [revealController revealToggle:nil];
                return;
            }
        }
        else if(indexPath.row == 2)
        {
            newFrontController = [[TermsOfUseViewController alloc] init];
            if([revealController.frontViewController isKindOfClass:[UINavigationController class]])
            {
                UINavigationController *currentNavigation = (UINavigationController *)revealController.frontViewController;
                [currentNavigation pushViewController:newFrontController animated:NO];
                [revealController revealToggle:nil];
                return;
            }
        }
        else if (indexPath.row == 3){
            [self trackingEventByName:@"Sign out" withAction:ClickActionType withCategory:SignOutCategoryType];
            [self signOutCurrentUser];
        }
    }
    
    UINavigationController *newNavigationViewController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
    [revealController pushFrontViewController:newNavigationViewController animated:YES];
}



- (void)updatePreferences{
    
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    NSMutableArray *arraySubDictValues = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *hotelDict = [[NSMutableDictionary alloc] init];
    [hotelDict setValue:@"Hotel" forKey:@"Type"];
    
    PreferenceObject *hotelPref = [self getPreferenceByType:HotelPreferenceType];
    NSString *value = ([[SessionData shareSessiondata] isUseLocation])?@"YES":@"NO";
    
    if ([SessionData shareSessiondata].arrayPreferences.count > 0 && hotelPref.preferenceID.length > 0){
        [hotelDict setValue:hotelPref.preferenceID forKey:@"MyPreferencesId"];
        [hotelDict setValue:hotelPref.value forKey:@"Preferredstarrating"];
        [hotelDict setValue:value forKey:@"SmokingPreference"];
        [hotelDict setValue:@"456654" forKey:@"RoomPreference"];
        [hotelDict setValue:hotelPref.userName forKey:@"BedPreference"];
        [arraySubDictValues addObject:hotelDict];
        [dataDict setValue:arraySubDictValues forKey:@"PreferenceDetails"];
        
        wsPreferences = [[WSPreferences alloc] init];
        [wsPreferences updatePreference:dataDict];
    }
}

@end
