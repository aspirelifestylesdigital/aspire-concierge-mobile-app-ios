//
//  SplashScreenViewController.m
//  MobileConciergeUSDemo
//
//  Created by Nghia Dinh on 7/13/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "SplashScreenViewController.h"
#import "SWRevealViewController.h"
#import "SignInViewController.h"
#import "Constant.h"
#import "EnumConstant.h"
#import "HomeViewController.h"
#import "UIView+Extension.h"
#import "AppData.h"
#import "PrivacyPolicyViewController.h"
#import "CreateProfileViewController.h"
#import "Common.h"
#import "WSB2CGetRequestToken.h"
#import "WSB2CGetAccessToken.h"
#import "WSB2CGetUserDetails.h"
#import "WSB2CGetPolicyInfo.h"
#import "WSB2CVerifyBIN.h"
#import "PolicyInfoItem.h"
#import "BINItem.h"
#import "WSSignIn.h"
#import "MenuViewController.h"
#import "ChangePasswordViewController.h"

#import "AppDelegate.h"
#import "WSB2CPasscodeVerfication.h"
#import "PasscodeItem.h"
#import "PasscodeViewController.h"
@import AspireApiFramework;
#import "SignInViewController.h"

@interface SplashScreenViewController () <DataLoadDelegate>
{
    AppDelegate* appdelegate;
    WSB2CPasscodeVerfication *wsPasscodeVerification;
}
@end

@implementation SplashScreenViewController
{
    NSInteger currentTask;
    BOOL isLoadPolicy;
    WSB2CGetRequestToken *wsRequestToken;
    WSB2CGetAccessToken *wsAccessToken;
    WSB2CGetPolicyInfo *wsPolicy;
    
    NSNumber *currentPolicy;
    dispatch_group_t group;
    dispatch_queue_t queue;
    float policyVersion;
    PasscodeItem *passcodeItem;
}

- (void)viewDidLoad {
    isIgnoreScaleView = YES;
    [super viewDidLoad];
    [self trackingScreenByName:@"Splash"];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.view layoutIfNeeded];
    
    [self getUUID];
    
    group = dispatch_group_create();
    queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    dispatch_group_notify(group, queue, ^{
        NSLog(@"All tasks done");
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self goToNextViewController];
    });
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getUUID{
    if (![[SessionData shareSessiondata] UUID]) {
        NSString *uuidString = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [[SessionData shareSessiondata] setUUID:uuidString];
    }
}

- (void)appWillEnterForeground:(NSNotification*) noti {
    if(!isNetworkAvailable()) {
        [self showNetWorkingStatusArlet];
    }
}

- (void) CheckBINNumber{
    
    if(!isNetworkAvailable()) {
        [self showNetWorkingStatusArlet];
    }
}

-(void)goToNextViewController {
    
    if(isJailbroken())
    {
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = nil;
        alert.msgAlert = NSLocalizedString(@"jailbroken_message", nil);
        alert.firstBtnTitle = @"OK";
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
        });
        
        alert.blockFirstBtnAction = ^(void){
            //Quit app
            exit(0);
        };
        [self showAlert:alert forNavigation:NO];
        
        return;
    }
    
//    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
    
    if ([User isValid]) {
//        __block NSDate* startTime = [NSDate date];
        __weak typeof (self) _self = self;
        [self startActivityIndicator];
        [ModelAspireApiManager retrieveProfileCurrentUser:^(User *user, NSError *error) {
            if (!error) {
                UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
                [[SessionData shareSessiondata] setUserObject:userObject];
                
                NSString *passcodeText = [[User current] getPasscode];
                
                if(passcodeText.length > 0)
                {
                    [ModelAspireApiManager verifyBin:passcodeText completion:^(BOOL isPass, NSString *bin, id  _Nullable error) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [_self stopActivityIndicator];
                            if (error) {
                                NSString* mess = NSLocalizedString(@"api_common_error_message", nil);
                                switch ([AspireApiError getErrorTypeAPIVerifyBinFromError:error]) {
                                    case BIN_ERR:
                                        [_self onVerifyBinError];
                                        break;
                                    case NETWORK_UNAVAILABLE_ERR:
                                        mess = @"";
                                        [_self showErrorNetwork];
                                        break;
                                    default:
                                        NSLog(@"[RESULT] => VERIFY BIN ERROR: %@",@"OUT OF ENUM AspireApiErrorType");
                                        [_self showCommonErrorWithOKAction:^{
                                            [_self okActionForBinError];
                                        }];
                                        break;
                                }
                                return;
                            }
                            
                            if(isPass)
                            {
                                // set bin for this account
                                [ModelAspireApiManager registerServiceUsername:AspireApiUsername
                                                                      password:AspireApiPassword
                                                                       program:AspireApiProgam
                                                                           bin:bin];
                                [_self navigationToHomeViewController];
                            }
                            else
                            {
                                [_self showAlertInvalidBin];
                            }
                        });
                    }];

                }else{
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        NSInteger wait = [[NSDate date] timeIntervalSinceDate:startTime];
//                        if (wait < 0) wait = 0;
                      dispatch_async(dispatch_get_main_queue(), ^{
                            [_self handleInvalidPasscode];
                       });
//                    });
                }
                
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_self stopActivityIndicator];
                    NSString *mess;
                    switch ([AspireApiError getErrorTypeAPIRetrieveProfileFromError:error]) {
                        case UNKNOWN_ERR:
                            // show general message for update profile
                            mess = @"";
                            NSLog(@"[RESULT] => LOGIN FAIL: %@",@"UNKNOWN_ERR");
                            break;
                        case ACCESSTOKEN_INVALID_ERR:
                            [_self handleInvalidCredentials];
                            // show message invalid accesstoken
                            NSLog(@"[RESULT] => LOGIN FAIL: %@",@"ACCESSTOKEN_INVALID_ERR");
                            // login again
                            break;
                        case NETWORK_UNAVAILABLE_ERR:
                            [_self showErrorNetwork];
                            break;
                        default:
                            mess = @"";
                            NSLog(@"[RESULT] => LOGIN FAIL: %@",@"OUT OF ENUM AspireApiErrorType");
                            break;
                    }
                    if (mess != nil){
                        AlertViewController *alert = [[AlertViewController alloc] init];
                        alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);
                        alert.msgAlert = NSLocalizedString(@"api_common_error_message", nil);
                        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
                        alert.blockFirstBtnAction = ^{
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            [ModelAspireApiManager logout];
                            [self gotoSignin];
                        };
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            alert.seconBtn.hidden = YES;
                            alert.midView.alpha = 0.0f;
                        });
                        
                        [_self showAlert:alert forNavigation:NO];
                    }
                });
            }
        }];
        
    }else{
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self gotoSignin];
//        });
    }
}

- (void) okActionForBinError {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [ModelAspireApiManager logout];
    PasscodeViewController *passcodeViewController = [[PasscodeViewController alloc] init];
    appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    appdelegate.window.rootViewController = passcodeViewController;
    [appdelegate.window makeKeyAndVisible];
}

- (void) onVerifyBinError {
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);
    alert.msgAlert = NSLocalizedString(@"check_invalid_passcode_msg", nil);
    alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
    SplashScreenViewController __weak *weakSelf = self;
    alert.blockFirstBtnAction = ^{
        [weakSelf okActionForBinError];
    };
    
    [self showAlert:alert forNavigation:NO];
}

- (void) showAlertInvalidBin {
    void(^gotoSignin)(void) = ^{
        [self stopActivityIndicator];
        if (self.navigationController ) {
            [self.navigationController popViewControllerAnimated:true];
            return;
        }
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [ModelAspireApiManager logout];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SignInViewController *signinViewController = [storyboard instantiateViewControllerWithIdentifier:@"UDASignInViewController"];
        UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:signinViewController];
        AppDelegate *appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
        appdelegate.window.rootViewController = navigationController;
        [appdelegate.window makeKeyAndVisible];
    };
    
    [self stopActivityIndicator];
    [self showAlertWithTitle:NSLocalizedString(@"alert_error_title", nil)
                     message:NSLocalizedString(@"check_invalid_passcode_msg", nil)
                     buttons:@[NSLocalizedString(@"arlet_cancel_button", nil), NSLocalizedString(@"ok_button_title", nil)]
                     actions:@[gotoSignin, ^{
        [self handleInvalidPasscode];
    }]
            messageAlignment:NSTextAlignmentCenter];
}

- (void) handleInvalidPasscode{
    PasscodeViewController *passcodeViewController = [[PasscodeViewController alloc] init];
    passcodeViewController.needInputNewPasscode = true;
    appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    appdelegate.window.rootViewController = passcodeViewController;
    [appdelegate.window makeKeyAndVisible];
}

- (void) gotoSignin{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SignInViewController *signInViewController = [storyboard instantiateViewControllerWithIdentifier:@"UDASignInViewController"];
    UINavigationController *fontViewController = [[UINavigationController alloc] initWithRootViewController:signInViewController];
    appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    appdelegate.window.rootViewController = fontViewController;
    [appdelegate.window makeKeyAndVisible];
}

- (void) loadPolicy{
    // load policy privacy
    wsPolicy = [[WSB2CGetPolicyInfo alloc] init];
    wsPolicy.delegate = self;
    isLoadPolicy = YES;
    [wsPolicy retrieveDataFromServer];
}

- (void) getRequestToken{
    wsRequestToken = [[WSB2CGetRequestToken alloc] init];
    wsRequestToken.delegate = self;
    currentTask = WS_GET_REQUEST_TOKEN;
    [wsRequestToken getRequestToken];
}


- (void)loadDataDoneFrom:(WSBase *)ws {
    if ([ws isKindOfClass:[WSSignIn class]]) {
        [self navigationToHomeViewController];
    }
    else if(ws.task == WS_B2C_VERIFY_PASSCODE)
    {
        passcodeItem = (PasscodeItem *)ws.data[0];
        if(passcodeItem.valid)
        {
            [self navigationToHomeViewController];
        }
        else
        {
            typeof(self) weakSelf = self;
            [self showAlertForRetryPasscodeWithFirstBlock:^{
                [weakSelf navigateToPasscodeViewController];
            } withSecondBlock:^{
                [weakSelf navigateToPasscodeViewController];
            }];
        }
    }
}

-(void)navigateToPasscodeViewController
{
    PasscodeViewController *passcodeViewController = [[PasscodeViewController alloc] init];
    passcodeViewController.needInputNewPasscode = true;
    appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    appdelegate.window.rootViewController = passcodeViewController;
    [appdelegate.window makeKeyAndVisible];
}

- (void)loadDataFailFrom:(id<BaseResponseObjectProtocol>)result withErrorCode:(NSInteger)errorCode{
    NSLog(@"faillllllllll lllll");
}

- (void)loadDataFailFrom:(id<WSBaseProtocol>)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message{
    
    if(ws.task == WS_GET_MY_PREFERENCE)
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [ModelAspireApiManager logout];
        [self navigationToUDASignInViewController];
    }
    else
    {
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);;
        alert.msgAlert = message;
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
        });
        
        [self showAlert:alert forNavigation:NO];
    }
}


- (void) showNetWorkingStatusArlet{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = nil;
    alert.msgAlert = NSLocalizedString(@"no_network_connection", nil);
    alert.secondBtnTitle = NSLocalizedString(@"arlet_retry_button", nil);
    alert.firstBtnTitle = NSLocalizedString(@"home_button_title", nil);
    alert.blockSecondBtnAction = ^(void){
        [self CheckBINNumber];
    };
    
    [self showAlert:alert forNavigation:NO];
}

-(void)navigationToInitialBinViewController:(BOOL)invalidBIN {
    [self navigationToUDASignInViewController];
}

-(void)navigationToHomeViewController
{
    appdelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    [UIView transitionWithView:appdelegate.window
                      duration:0.5
                       options:UIViewAnimationOptionPreferredFramesPerSecond60
                    animations:^{
                        
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        
                        MenuViewController *menuViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
                        UINavigationController *menuNavigationViewController = [[UINavigationController alloc] initWithRootViewController:menuViewController];
                        UIViewController *fontViewController = [[HomeViewController alloc] init];
                        
                        SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:menuNavigationViewController frontViewController:fontViewController];
                        
                        revealController.delegate = appdelegate;
                        revealController.rearViewRevealWidth = SCREEN_WIDTH;
                        revealController.rearViewRevealOverdraw = 0.0f;
                        revealController.rearViewRevealDisplacement = 0.0f;
                        appdelegate.viewController = revealController;
                        appdelegate.window.rootViewController = appdelegate.viewController;
                        [appdelegate.window makeKeyWindow];
                        
                        UIViewController *newFrontController = [[HomeViewController alloc] init];
                        UINavigationController *newNavigationViewController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                        [revealController pushFrontViewController:newNavigationViewController animated:YES];
                    }
                    completion:nil];
}

-(void)navigationToPrivacyPolicyViewController
{
    SWRevealViewController *revealViewController = self.revealViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PrivacyPolicyViewController *privacyPolicyViewController = [storyboard instantiateViewControllerWithIdentifier:@"PrivacyPolicyViewController"];
    privacyPolicyViewController.isRecheckPolicy = YES;
    NSString *str = ((PolicyInfoItem *)[wsPolicy.data objectAtIndex:0]).textInfo;
    privacyPolicyViewController.policyText = str;
    NSLog(@"return policy version: %@", ((PolicyInfoItem *)[wsPolicy.data objectAtIndex:0]).CurrentVersion);
    privacyPolicyViewController.policyVersion = ((PolicyInfoItem *)[wsPolicy.data objectAtIndex:0]).CurrentVersion;
    [revealViewController pushFrontViewController:privacyPolicyViewController animated:YES];
}

-(void)navigationToUDASignInViewController
{
    [UIView transitionWithView:appdelegate.window
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        SignInViewController *createProfileViewController = [storyboard instantiateViewControllerWithIdentifier:@"UDASignInViewController"];
                        UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:createProfileViewController];
                        appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
                        appdelegate.window.rootViewController = navigationController;
                    }
                    completion:^(BOOL a){
                    [appdelegate.window makeKeyAndVisible];
                    }];
    
}
-(void)navigationToCreateProfileViewController
{
    SWRevealViewController *revealViewController = self.revealViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CreateProfileViewController *createProfileViewController = [storyboard instantiateViewControllerWithIdentifier:@"CreateProfileViewController"];
    [revealViewController pushFrontViewController:createProfileViewController animated:YES];
}

- (void) crashApp{
    //[self performSelector:@selector(die_die)];
}

@end
