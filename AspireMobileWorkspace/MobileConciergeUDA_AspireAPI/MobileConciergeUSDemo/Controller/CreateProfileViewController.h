//
//  SignInViewController.h
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/13/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "UserObject.h"
#import "DropDownView.h"
#import "PasswordTextField.h"
#import "CustomTextField.h"
#import "UserRegistrationItem.h"

#define MASKED_ANSWER @"*******"

@interface CreateProfileViewController : BaseViewController
{
    NSString *currentFirstName;
    BOOL isCheck;
    BOOL isUseLocation;
    CGFloat backupBottomConstraint;
    //CGRect signFrame;
    //CGFloat signBtnWidth;
    //CGFloat signBtnHeight;
    UserObject* userInfo;
    NSString *onlineMemberId;
    NSString *currentLastName;
    NSString *currentEmail;
    NSString *currentPhone;
    NSString *currentPasscode;
    NSString *currentPassword;
    NSString *currentConfirmPassword;
    UIImageView *asteriskImage;
    CGFloat keyboardHeight;
    
}

//@property (nonatomic, weak) IBOutlet UIButton *signinBtn;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIView *myView;
@property (weak, nonatomic) IBOutlet DropDownView *salutationDropDown;

@property (nonatomic, weak) IBOutlet UITextField *firstNameText;
@property (weak, nonatomic) IBOutlet UITextField *lastNameText;
@property (weak, nonatomic) IBOutlet CustomTextField *emailText;
@property (weak, nonatomic) IBOutlet CustomTextField *phoneNumberText;
@property (weak, nonatomic) IBOutlet UITextField *answerTextField;

@property (weak, nonatomic) IBOutlet UIButton *switchLocationBtn;
@property (weak, nonatomic) IBOutlet UIImageView *locationStatusImage;

//@property (weak, nonatomic) IBOutlet UIButton *checkBoxBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UILabel *commitmentLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *signinBottomConstraint;
@property (strong, nonatomic) UIButton *signInButton;
@property (assign, nonatomic) BOOL isUpdateProfile; //YES = UpdateProfile ; NO = create profile
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstNameHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewActionBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *checkBoxImageTopConstraint;

@property (weak, nonatomic) IBOutlet UIButton *checkBoxButton;
@property (weak, nonatomic) IBOutlet UIImageView *checkBoxImage;

@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIView *confirmMessageView;
@property (strong,nonatomic) NSString* emailChecked, *lastNameChecked, *firstNameChecked, *phoneChecked, *partyId;
@property (strong,nonatomic) NSString*inputNewAnswer;

-(IBAction)checkBox:(id)sender;
-(BOOL)updateTextFiel:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
-(void)checkTextFieldIsChange:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
-(void)setTextViewsDefaultBottomBolder;
-(void)verifyAccountData:(BOOL)isUpdate;
-(void) setCheckBoxState:(BOOL)check;
-(void) updateSuccessRedirect;
-(void) createAsteriskForTextField:(UITextField *)textField;
//-(IBAction)signinAction:(id)sender;
-(void)dismissKeyboard;
- (void) getUserInfo;
- (void) handleAsterickIcon;
- (void)updateProfileButtonStatus;
- (void) changeUseLocation;
- (void)changeValueDropDown;
- (void)setImageLocation:(BOOL)isLocation;
- (void)setUserDefaultInfoWithDict:(UserRegistrationItem*)item;
- (void)addPreferences;
- (void)updateProfileWithAPI;

-(void)handleMaskAnswer;

-(void) onTextFieldChanged:(UITextField*) textfield;
@end
