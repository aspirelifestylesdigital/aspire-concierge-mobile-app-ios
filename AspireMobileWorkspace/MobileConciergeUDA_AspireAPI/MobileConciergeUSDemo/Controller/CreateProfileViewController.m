//
//  SignInViewController.m
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/13/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "CreateProfileViewController.h"
#import "MenuViewController.h"
#import "SWRevealViewController.h"
#import "SignInViewController.h"
#import "AppDelegate.h"
#import "NSString+Utis.h"
#import "UITextField+Extensions.h"
#import "UIButton+Extension.h"
#import "UIView+Extension.h"
#import "UILabel+Extension.h"
#import "HomeViewController.h"
#import "Constant.h"
#import "MyProfileViewController.h"
#import "AppData.h"
#import "AppDelegate.h"
#import "UserObject.h"
#import "UserRegistrationItem.h"
#import "LocationComponent.h"
#import "PreferenceObject.h"
#import "PasscodeItem.h"
#import "UtilStyle.h"
#import "NSString+AESCrypt.h"
#import "SessionData.h"
#import "TermsOfUseViewController.h"
#import "PolicyViewController.h"
#import "NSDictionary+SBJSONHelper.h"
#import "CreateSecureView.h"

@import AspireApiControllers;
@import AspireApiFramework;

#define TEXT_FIELD_FONT_SIZE 18

@interface CreateProfileViewController ()<UITextFieldDelegate, DropDownViewDelegate, UIGestureRecognizerDelegate>
{
    AppDelegate* appdelegate;
    UITapGestureRecognizer *tappedOutsideKeyboards;
    UILabel *termOfUseLbl;
    UILabel *policyLbl;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnSC;

@end

@implementation CreateProfileViewController
@synthesize partyId;
#pragma mark - LIFECYCLE
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateStatusLocation:)
                                                 name:LOCATION_COMPONENT_ISAUTHORIZED_NOTIFICATION
                                               object:nil];
    
    UITapGestureRecognizer *tab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkBox:)];
    self.commitmentLabel.userInteractionEnabled = YES;
    [self.commitmentLabel addGestureRecognizer:tab];
    
    [self setUIStyte];
    if (_isUpdateProfile == NO || !_isUpdateProfile) {
        [self trackingScreenByName:@"Sign up"];
    }else{
        [self trackingScreenByName:@"My profile"];
    }
    [self buildAgreeTextViewFromString:NSLocalizedString(@"message_confirm_policy", nil)];
    
    
    [self setEmail:_emailChecked];
    [self setPhone:_phoneChecked];
    [self setLastName:_lastNameChecked];
    [self setFirstName:_firstNameChecked];
}

- (void) setEmail:(NSString *)email {
    if (email != nil) {
        _emailText.text = email;
        currentEmail = email;
        [_emailText setOriginText:email];
        [_emailText showTextMask:true];
    }
}

- (void) setPhone:(NSString *)phone {
    if (phone != nil) {
        NSMutableString* newString = [NSMutableString stringWithString:@""];
        if(![phone isValidPhoneNumber]) {
            NSString* temp = phone;
            NSMutableArray *characters = [NSMutableArray array];
            for (int i=0; i<temp.length; i++) {
                [characters addObject:[temp substringWithRange:NSMakeRange(i, 1)]];
            }
            [newString insertString:@"1-" atIndex:0];
            
            NSInteger start = characters.count > 10 ? characters.count - 10 : 0;
            for (NSInteger i = start; i < characters.count; i++) {
                [newString appendString:characters[i]];
                if (i - start == 2 || i - start == 5)
                    [newString appendString:@"-"];
            }
        } else {
            newString = [NSMutableString stringWithString:phone];
        }
        //        [self updateTextFiel:_phoneNumberText shouldChangeCharactersInRange:NSMakeRange(0, 0) replacementString:newString];
        _phoneNumberText.text = newString;
        currentPhone = newString;
        [_phoneNumberText setOriginText:newString];
        [_phoneNumberText showTextMaskPhone];
    }
}

- (void) setFirstName:(NSString*) firstName {
    if (firstName != nil) {
        _firstNameText.text = firstName;
        currentFirstName = firstName;
        [_firstNameText setOriginText:firstName];
        [_firstNameText showTextMask:false];
    }
}

- (void) setLastName:(NSString*) lastName {
    if (lastName != nil) {
        currentLastName = lastName;
        _lastNameText.text = lastName;
        [_lastNameText setOriginText:lastName];
        [_lastNameText showTextMask:false];
    }
}

- (void)changeValueDropDown{
//    [self setButtonStatus:[self checkFieldIsChangeString]];
}

-(void)viewWillAppear:(BOOL)animated
{
    if (![self isKindOfClass:[MyProfileViewController class]]) {
        [self.navigationController setNavigationBarHidden:YES animated:NO];
    }
    [super viewWillAppear:animated];
    tappedOutsideKeyboards = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    //tappedOutsideKeyboards.cancelsTouchesInView = false;
    tappedOutsideKeyboards.delegate = self;
    [self.navigationController.view addGestureRecognizer:tappedOutsideKeyboards];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self setTextViewsDefaultBottomBolder];
    [self.view layoutIfNeeded];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController.view removeGestureRecognizer:tappedOutsideKeyboards];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SETUP UI
- (void) setUIStyte {
    
    self.phoneNumberText.keyboardType = UIKeyboardTypePhonePad;
    self.phoneNumberText.tag = PHONE_NUMBER_TEXTFIELD_TAG;
    self.emailText.keyboardType = UIKeyboardTypeEmailAddress;
    self.firstNameText.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    self.lastNameText.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    
    self.emailText.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailText.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.emailText.textColor = [AppColor placeholderTextColor];
    
    self.salutationDropDown.leadingTitle = 8.0f;
    self.salutationDropDown.title = @"Please select a salutation.";
    self.salutationDropDown.listItems = @[@"Dr", @"Miss", @"Mr", @"Mrs", @"Ms"];
    self.salutationDropDown.titleColor = [AppColor placeholderTextColor];
    self.salutationDropDown.delegate = self;
    
    self.firstNameText.delegate = self;
    self.lastNameText.delegate = self;
    self.emailText.delegate = self;
    self.phoneNumberText.delegate = self;
    
    
    if ([User isValid]) {
        UserObject *profileObject= (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
        NSString *salutation = profileObject.salutation;
        if (salutation.length > 1) {
            self.salutationDropDown.titleColor = [AppColor textColor];
            self.salutationDropDown.title = salutation;
        }
        self.firstNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:profileObject.firstName];
        self.lastNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:profileObject.lastName];
        self.emailText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:profileObject.email];
        self.phoneNumberText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:profileObject.mobileNumber];
        
        isUseLocation = [[SessionData shareSessiondata] isUseLocation];
        [self setImageLocation:isUseLocation];
        
        currentFirstName = profileObject.firstName;
        currentEmail = profileObject.email;
        NSString *tempPhone = profileObject.mobileNumber;
        currentPhone = (tempPhone.length < 15) && ![tempPhone containsString:@"+"] ? [NSString stringWithFormat:@"+%@",tempPhone] : tempPhone;
        currentLastName = profileObject.lastName;
        
        self.emailText.enabled = NO;
        
        [self.answerTextField setText:MASKED_ANSWER];
        
    }else{
        
        currentFirstName = @"";
        currentEmail = @"";
        currentPhone = @"";
        currentLastName = @"";
        
        self.firstNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.firstNameText.text];
        self.lastNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.lastNameText.text];
        self.phoneNumberText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.phoneNumberText.text];
        
        self.firstNameText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.firstNameText.placeholder];
        self.lastNameText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.lastNameText.placeholder];
        self.phoneNumberText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.phoneNumberText.placeholder];
    }
    
    self.firstNameText.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.emailText.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.lastNameText.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.phoneNumberText.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    
    [self.firstNameText showTextMask:false];
    [self.lastNameText showTextMask:false];
    [self.emailText showTextMask:true];
    [self.phoneNumberText showTextMask:false];
    
    [self.firstNameText showAsterisk:5];
    [self.lastNameText showAsterisk:5];
    [self.emailText showAsterisk:5];
    [self.phoneNumberText showAsterisk:5];
    
    self.answerTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.answerTextField.text];
    self.answerTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.answerTextField.placeholder];
    self.answerTextField.font =  self.firstNameText.font;
    self.answerTextField.delegate = self;
    [self.answerTextField setMarginLeftRight:5];
    
    [self.myView setBackgroundColorForView];
    [self.scrollView setBackgroundColorForView];
    [self.signInButton setBackgroundColorForNormalStatus];
    [self.signInButton setBackgroundColorForTouchingStatus];
    [self.signInButton configureDecorationForButton];
    
    //[self backgroundColorForDisableStatusButton];
    [self.submitButton setBackgroundColorForNormalStatus];
    self.submitButton.enabled = NO;
    [self.submitButton setBackgroundColorForTouchingStatus];
    [self.submitButton configureDecorationForButton];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.checkBoxImageTopConstraint.constant = (IPAD) ? 12.0f : 2.0f;
    });
}
- (void)backgroundColorForDisableStatusButton
{
    UIGraphicsBeginImageContext(CGSizeMake(1.0f, 1.0f));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [AppColor disableButtonColor].CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 1));
    UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.submitButton setBackgroundImage:colorImage forState:UIControlStateNormal];
}

- (void)initView{
    
    // Text Style For Title
    self.titleLable.attributedText = [UtilStyle setTextStyleForTitleViewControllerWithMessage:@"REGISTER"];
    
    // Text Style For Greet Message
    NSString *message = NSLocalizedString(@"commitment_profile_message", nil);
    self.commitmentLabel.attributedText = [UtilStyle setLargeSizeStyleForLabelWithMessage:message];
    //[self setCheckBoxState:isCheck];
}

#pragma mark - SETUP DATA
- (void)setUserObjectWithDict:(NSDictionary*)dict{
    [[SessionData shareSessiondata] setUserObjectWithDict:dict];
}

- (void)setUserDefaultInfoWithDict:(UserRegistrationItem*)item{
    [[SessionData shareSessiondata] setOnlineMemberID:item.OnlineMemberID];
    [[SessionData shareSessiondata] setOnlineMemberDetailIDs:item.OnlineMemberDetailIDs];
}

- (void) getUserInfo{
    if ([User isValid]) {
        UserObject* user = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
        NSString *salutation = user.salutation;
        self.salutationDropDown.titleColor = (salutation.length > 1)?[AppColor textColor]:[AppColor placeholderTextColor];
        self.salutationDropDown.title = (salutation.length > 1)?salutation:@"Please select a salutation.";
        currentFirstName = user.firstName;
        currentEmail = user.email ;
        NSString *tempPhone = user.mobileNumber;
        currentPhone = (tempPhone.length < 18) && ![tempPhone containsString:@"+"] ? [NSString stringWithFormat:@"+%@",tempPhone] : tempPhone;
        currentLastName = user.lastName;
        self.firstNameText.text = [currentFirstName createMaskForText:NO];
        self.emailText.text = [currentEmail createMaskForText:YES];
        self.lastNameText.text = [currentLastName createMaskForText:NO];
        self.phoneNumberText.text = [currentPhone createMaskForText:NO];
        
//        isUseLocation = false;
        if ([User current].locations.count > 0)
            isUseLocation = user.isUseLocation;
        [self setImageLocation:isUseLocation];
    }
}
#pragma mark - ACTIONS
- (void) setCheckBoxState:(BOOL)check
{
    if (!check) {
        check = NO;
    }
    (check == YES) ? [self.checkBoxButton setImage:[UIImage imageNamed:@"checkbox_check"] forState:UIControlStateNormal] : [self.checkBoxButton setImage:[UIImage imageNamed:@"checkbox_uncheck"] forState:UIControlStateNormal];
}
- (IBAction)checkBox:(id)sender {
    isCheck = !isCheck;
    [self setCheckBoxState:isCheck];
}

- (IBAction)actionSwitchLocation:(id)sender {
    
    isUseLocation = !isUseLocation;
    [self setImageLocation:isUseLocation];
    if (![self isKindOfClass:[MyProfileViewController class]]) {
        [[SessionData shareSessiondata] setIsUseLocation:isUseLocation];
        if (isUseLocation) {
            [LocationComponent startRequestLocation];
        }
    }
    else{
        [[SessionData shareSessiondata] setIsUseLocation:isUseLocation];
        if (isUseLocation) {
            [LocationComponent startRequestLocation];
        }
    }
   [self changeUseLocation];
}

- (void)setImageLocation:(BOOL)isLocation {
    self.locationStatusImage.image = [UIImage imageNamed:(isLocation) ? @"location_on" : @"location_off"];
}

-(void)updateStatusLocation:(NSNotification *)notification {
    
    NSDictionary* dictNoti = notification.userInfo;
    BOOL isLocation = [dictNoti boolForKey:LOCATION_COMPONENT_ISAUTHORIZED_NOTIFICATION];
    if (!isLocation) {
        if ([dictNoti intForKey:@"ERROR_CODE"] == 2) {
//            isUseLocation = NO;
//            [self setImageLocation:isUseLocation];
//            [[SessionData shareSessiondata] setIsUseLocation:isUseLocation];
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
                [self setRequestLocation:YES];
            });
        }else if ([dictNoti intForKey:@"ERROR_CODE"] == 1) {
            if([[NSUserDefaults standardUserDefaults] boolForKey:@"IsRequestLocation"]) {
                [self setRequestLocation:NO];
            }else{
                [self showErrorLocationService];
            }
        }
    }
    [self changeUseLocation];
}

- (void)setRequestLocation:(BOOL)isRequest{
    [[NSUserDefaults standardUserDefaults] setBool:isRequest forKey:@"IsRequestLocation"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (void)updateProfileButtonStatus{

}
-(void) enableSignInButton:(UIButton *)button
{
    [button setHighlighted:NO];
    self.checkBoxButton.selected = !(button.selected);
    [button setBackgroundColor:[UIColor clearColor]];
    [self.submitButton setEnabled:button.selected];
}

- (IBAction)CheckboxAction:(id)sender {
    if (isCheck) {
        self.checkBoxImage.image = [UIImage imageNamed:@"checkbox_uncheck"];
        isCheck = NO;
    }else{
        self.checkBoxImage.image = [UIImage imageNamed:@"checkbox_check"];
        isCheck = YES;
    }
    [self enableSubmitButton:isCheck];
}

- (void)enableSubmitButton:(BOOL)isEnable {
    self.submitButton.enabled = isEnable;
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)SubmitAction:(id)sender {
    [self dismissKeyboard];
    [self verifyAccountData:NO];
}

- (IBAction)CancelAction:(id)sender {
    if ([AppData isCreatedProfile]) {
    SWRevealViewController *revealViewController = self.revealViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SignInViewController *signInViewController = [storyboard instantiateViewControllerWithIdentifier:@"UDASignInViewController"];
    [revealViewController pushFrontViewController:signInViewController animated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - ALERT
- (void)showErrorLocationService {
    
    //    isUseLocation = NO;
    //    [self setImageLocation:isUseLocation];
    //    [[SessionData shareSessiondata] setIsUseLocation:isUseLocation];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Location Service Disabled" message:@"To enable, please go to Settings and turn on Location Service for this app." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction*  action) {
        
    }];
    
    UIAlertAction *settingAction = [UIAlertAction actionWithTitle:@"Setting" style:UIAlertActionStyleDefault handler:^(UIAlertAction  *action) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }];
    
    [alertController addAction:okAction];
    [alertController addAction:settingAction];
    alertController.preferredAction = settingAction;
    
    [self presentViewController:alertController animated: YES completion: nil];
}

#pragma mark API PROCESS
- (void)updateProfileWithAPI{

    NSMutableDictionary *userDict = [[NSMutableDictionary alloc] init];
    [userDict checkAndSetValue:(self.salutationDropDown.title.length > 1) ? self.salutationDropDown.title : @" " forKey:@"Salutation"];
    [userDict checkAndSetValue:[currentFirstName stringByTrimmingCharactersInSet:
                                [NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"FirstName"];
    [userDict checkAndSetValue:[currentLastName stringByTrimmingCharactersInSet:
                                [NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"LastName"];
    [userDict checkAndSetValue:[currentPhone stringByReplacingOccurrencesOfString:@"+" withString:@""] forKey:@"MobileNumber"];
    [userDict checkAndSetValue:currentEmail forKey:@"Email"];
    [userDict checkAndSetValue:currentPassword forKey:@"Password"];
    [userDict checkAndSetValue:[[SessionData shareSessiondata] BINNumber] forKey:APP_USER_PREFERENCE_Passcode];
    [userDict checkAndSetValue:isUseLocation? @"ON": @"OFF" forKey:APP_USER_PREFERENCE_Location];
    [userDict checkAndSetValue:@"USA" forKey:@"Country"];
    [userDict checkAndSetValue:APP_NAME forKey:@"referenceName"];
    if (partyId) {
        [userDict setValue:partyId forKey:@"partyId"];
    }
    
    [userDict checkAndSetValue:@"Unknown" forKey:@"City"];
    
    CreateSecureView* v = [[CreateSecureView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    AACForgotPasswordController* vc = [AACForgotPasswordController new];
    vc.subview = v;
    v.controller = vc;
    [v updateUserInfor:userDict];
    [self.navigationController pushViewController:vc animated:true];
}

#pragma mark KEYBOARD PROCESS
-(void)handleKeyboardWillShow:(NSNotification *)paramNotification
{
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         keyboardHeight = keyboardRect.size.height;
                         [self updateEdgeInsetForShowKeyboard];
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

-(void)handleKeyboardWillHide:(NSNotification *)paramNotification
{
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         [self updateEdgeInsetForHideKeyboard];
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
    
}


-(void)updateEdgeInsetForShowKeyboard
{
    _btnSC.constant =  keyboardHeight + 80;

}


-(void)updateEdgeInsetForHideKeyboard
{
    _btnSC.constant = 80;
}

-(void)resignFirstResponderForAllTextField
{
    if(self.firstNameText.isFirstResponder)
    {
        [self.firstNameText resignFirstResponder];
    }
    else if(self.lastNameText.isFirstResponder)
    {
        [self.lastNameText resignFirstResponder];
    }
    else if(self.emailText.isFirstResponder)
    {
        [self.emailText resignFirstResponder];
    }
    else if(self.phoneNumberText.isFirstResponder)
    {
        [self.phoneNumberText resignFirstResponder];
    }else if(self.answerTextField.isFirstResponder)
    {
        [self.answerTextField resignFirstResponder];
    }
}

-(void)dismissKeyboard
{
    [self updateEdgeInsetForHideKeyboard];
    [self resignFirstResponderForAllTextField];
}

#pragma mark LOCAL PROCESS
-(BOOL)verifyValueForTextField:(UITextField *)textFied andMessageError:(NSMutableString *)message
{
    NSString *currentText = nil;
    NSString *errorMsg = nil;
    BOOL isValid = NO;
    if(textFied == self.firstNameText)
    {
        currentText = currentFirstName;
        
        NSMutableString *tempError = [[NSMutableString alloc] init];
        if (![(textFied.isFirstResponder ? [textFied.text removeRedudantWhiteSpaceInText] :currentText) isValidName]) {
            isValid = NO;
            if (tempError.length > 0) {
                [tempError appendString:@"\n* "];
            }
            [tempError appendString:NSLocalizedString(@"input_invalid_first_name_msg", nil)];
        }else if (![(textFied.isFirstResponder ? textFied.text :currentText) isValidNameWithSpecialCharacter]) {
            isValid = NO;
            if (tempError.length > 0) {
                [tempError appendString:@"\n* "];
            }
            [tempError appendString:@"Please enter a valid first name. Acceptable special characters are -, ' and space."];
            
        }else{
            isValid = YES;
        }
        
        errorMsg = tempError;

    }
    else if(textFied == self.lastNameText)
    {
        currentText = currentLastName;
        
        NSMutableString *tempError = [[NSMutableString alloc] init];
        if (![(textFied.isFirstResponder ? [textFied.text removeRedudantWhiteSpaceInText] :currentText) isValidName]) {
            isValid = NO;
            if (tempError.length > 0) {
                [tempError appendString:@"\n* "];
            }
            [tempError appendString:NSLocalizedString(@"input_invalid_last_name_msg", nil)];
        }else if (![(textFied.isFirstResponder ? textFied.text :currentText) isValidNameWithSpecialCharacter]) {
            isValid = NO;
            if (tempError.length > 0) {
                [tempError appendString:@"\n* "];
            }
            [tempError appendString:@"Please enter a valid last name. Acceptable special characters are -, ' and space."];
        }else{
            isValid = YES;
        }
        
        errorMsg = tempError;
    }
    else if(textFied == self.emailText)
    {
        currentText = currentEmail;
        errorMsg = NSLocalizedString(@"input_invalid_email_msg", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidEmail];
    }
    else if(textFied == self.phoneNumberText)
    {
        currentText = (textFied.isFirstResponder) ? textFied.text : currentPhone;
        errorMsg = NSLocalizedString(@"input_invalid_phone_number_msg", nil);
        
        if (![currentText isValidPhoneNumber]) {
            isValid = NO;
            errorMsg = NSLocalizedString(@"input_invalid_phone_number_msg", nil);
        }else{
            if ([currentText isValidCountryCode]) {
                currentText = [currentText stringByReplacingOccurrencesOfString:@"+" withString:@""];
                NSString *countryCode = [currentText getCoutryCode];
                if (currentText.length <= countryCode.length) {
                    isValid = NO;
                    errorMsg = NSLocalizedString(@"input_invalid_phone_number_msg", nil);
                }else {
                    isValid = YES;
                }
            }else {
                isValid = NO;
                errorMsg = NSLocalizedString(@"input_invalid_country_code_msg", nil);
            }
        }
    }
    
    else if(textFied == self.answerTextField)
    {
        errorMsg = NSLocalizedString(@"input_invalid_answer_msg", nil);
        if (self.answerTextField.text.length == 0) {
            isValid = NO;
        }else
        {
            if (self.answerTextField.text.length > 0 && self.inputNewAnswer.length == 0)
            {
                isValid = YES;
            }
            else
            {
                NSString *answer = [self.inputNewAnswer stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
                isValid = answer.length > 3;
            }
        }
    }
    
    if (textFied == self.answerTextField) {
        if (isValid) {
            [textFied setBottomBolderDefaultColor];
        } else {
            (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
            [message appendString:errorMsg];
            [textFied setBottomBorderRedColor];
        }
        return isValid;
    }
    
    if((textFied.isFirstResponder ? textFied.text :currentText).length > 0)
    {
        if(!isValid)
        {
            (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
            [message appendString:errorMsg];
            [textFied setBottomBorderRedColor];
        }
        else
        {
            [textFied setBottomBolderDefaultColor];
        }
    }
    else
    {
        [textFied setBottomBorderRedColor];
    }
    
    return isValid;
}
- (BOOL)verifyValueForDropdown:(DropDownView*)dropdown andMessageError:(NSMutableString *)message{
    
    BOOL isValid = NO;
    if (dropdown == self.salutationDropDown) {
        if (self.salutationDropDown.valueStatus == DefaultValueStatus) {
            (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
            [message appendString:@"You have to choose a salutation."]; //All fields are required.
            isValid = NO;
            [self.salutationDropDown setBottomBorderRedColor];
        }else {
            isValid = YES;
            [self.salutationDropDown setBottomBolderDefaultColor];
        }
    }
    return isValid;
}

-(void)verifyAccountData:(BOOL)isUpdate
{
    NSMutableString *message = [[NSMutableString alloc] init];
    if (isUpdate) {
        if(self.firstNameText.text.length == 0 || self.lastNameText.text.length == 0 || self.emailText.text.length == 0 || self.phoneNumberText.text.length == 0)
        {
            [message appendString:@"* "];
            [message appendString:@"All fields are required."];
            if (self.salutationDropDown.title.length > 10) {
                [self verifyValueForDropdown:self.salutationDropDown andMessageError:message];
            }
            [self verifyValueForTextField:self.firstNameText andMessageError:message];
            [self verifyValueForTextField:self.lastNameText andMessageError:message];
            [self verifyValueForTextField:self.emailText andMessageError:message];
            [self verifyValueForTextField:self.phoneNumberText andMessageError:message];
            [self verifyValueForTextField:self.answerTextField andMessageError:message];
            [self createProfileInforWithMessage:message isCreatedSuccessful:NO];
        }
        else{
            if (self.salutationDropDown.title.length > 10) {
                [self verifyValueForDropdown:self.salutationDropDown andMessageError:message];
            }
            [self verifyValueForTextField:self.firstNameText andMessageError:message];
            [self verifyValueForTextField:self.lastNameText andMessageError:message];
            [self verifyValueForTextField:self.emailText andMessageError:message];
            [self verifyValueForTextField:self.phoneNumberText andMessageError:message];
            [self verifyValueForTextField:self.answerTextField andMessageError:message];
            [self createProfileInforWithMessage:message isCreatedSuccessful:(message.length == 0)];
        }
    }else{
        if(self.firstNameText.text.length == 0 || self.lastNameText.text.length == 0 || self.emailText.text.length == 0 || self.phoneNumberText.text.length == 0)
        {
            [message appendString:@"* "];
            [message appendString:@"All fields are required."];
            
            [self verifyValueForDropdown:self.salutationDropDown andMessageError:message];
            
            [self verifyValueForTextField:self.firstNameText andMessageError:message];
            [self verifyValueForTextField:self.lastNameText andMessageError:message];
            [self verifyValueForTextField:self.emailText andMessageError:message];
            [self verifyValueForTextField:self.phoneNumberText andMessageError:message];
            
            [self createProfileInforWithMessage:message isCreatedSuccessful:NO];
        }
        else{
            
            [self verifyValueForDropdown:self.salutationDropDown andMessageError:message];

            [self verifyValueForTextField:self.firstNameText andMessageError:message];
            [self verifyValueForTextField:self.lastNameText andMessageError:message];
            [self verifyValueForTextField:self.emailText andMessageError:message];
            [self verifyValueForTextField:self.phoneNumberText andMessageError:message];
            
            [self createProfileInforWithMessage:message isCreatedSuccessful:(message.length == 0)];
        }
    }
}

-(void)createProfileInforWithMessage:(NSString *)message isCreatedSuccessful:(BOOL)isSuccessful
{
    if(isSuccessful)
    {
        [self updateEdgeInsetForHideKeyboard];
        [self setTextViewsDefaultBottomBolder];
        [self updateProfileWithAPI];
        
    }
    else
    {
        NSMutableString *newMessage = [[NSMutableString alloc] init];
        [newMessage appendString:@"\n"];
        [newMessage appendString:message];
        [newMessage appendString:@"\n"];
        
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = @"Please confirm that the value entered is correct:";
        alert.msgAlert = newMessage;
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
            alert.lblAlertMessage.textAlignment = NSTextAlignmentLeft;
            [alert.view layoutIfNeeded];
        });
        
        alert.blockFirstBtnAction = ^(void){
            [self makeBecomeFirstResponderForTextField];
        };
        
        [self showAlert:alert forNavigation:NO];
         
    }
}

- (void) updateSuccessRedirect{
    
    appdelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
   
    [UIView transitionWithView:appdelegate.window
                      duration:0.5
                       options:UIViewAnimationOptionPreferredFramesPerSecond60
                    animations:^{
                        
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        
                        MenuViewController *menuViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
                        UINavigationController *menuNavigationViewController = [[UINavigationController alloc] initWithRootViewController:menuViewController];
                        UIViewController *fontViewController = [[HomeViewController alloc] init];
                        
                        SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:menuNavigationViewController frontViewController:fontViewController];
                        
                        revealController.delegate = appdelegate;
                        revealController.rearViewRevealWidth = SCREEN_WIDTH;
                        revealController.rearViewRevealOverdraw = 0.0f;
                        revealController.rearViewRevealDisplacement = 0.0f;
                        appdelegate.viewController = revealController;
                        appdelegate.window.rootViewController = appdelegate.viewController;
                        [appdelegate.window makeKeyWindow];
                        
                        UIViewController *newFrontController = [[HomeViewController alloc] init];
                        UINavigationController *newNavigationViewController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                        [revealController pushFrontViewController:newNavigationViewController animated:YES];
                        
                    }
                    completion:nil];
}

#pragma mark TEXT FIELD DELEGATE
-(void) makeBecomeFirstResponderForTextField
{
    [self resignFirstResponderForAllTextField];
    if(![currentFirstName isValidName] || ![currentFirstName isValidNameWithSpecialCharacter])
    {
        [self.firstNameText becomeFirstResponder];
    }
    else if(![currentLastName isValidName] || ![currentLastName isValidNameWithSpecialCharacter])
    {
        [self.lastNameText becomeFirstResponder];
    }
    else if(![currentPhone isValidPhoneNumber] || ![currentPhone isValidCountryCode])
    {
        [self.phoneNumberText becomeFirstResponder];
    }
    
    else if([currentPhone isValidCountryCode])
    {
        currentPhone = [currentPhone stringByReplacingOccurrencesOfString:@"+" withString:@""];
        NSString *countryCode = [currentPhone getCoutryCode];
        if (currentPhone.length <= countryCode.length) {
            [self.phoneNumberText becomeFirstResponder];
        }
        
        else if(self.answerTextField.text.length == 0){
            [self.answerTextField becomeFirstResponder];
        }
        else if(self.answerTextField.text.length == self.inputNewAnswer.length){
            NSString *answer = [self.inputNewAnswer stringByTrimmingCharactersInSet:
                                [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if(answer.length < 4){
                [self.answerTextField becomeFirstResponder];
            }
        }
        
    }
    
    else if(self.answerTextField.text.length == 0){
        [self.answerTextField becomeFirstResponder];
    }
    else if(self.answerTextField.text.length == self.inputNewAnswer.length){
        if([self.inputNewAnswer stringByTrimmingCharactersInSet:
            [NSCharacterSet whitespaceAndNewlineCharacterSet]].length < 3){
            [self.answerTextField becomeFirstResponder];
        }
    }
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField == self.emailText) {
        [self dismissKeyboard];
        self.emailText.text = currentEmail;
        return NO;
    }
    else{
        [self.emailText showTextMask:true];
    }
    if (textField == self.answerTextField) {
        textField.text = @"";
    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.text = [textField.text removeRedudantWhiteSpaceInText];
    [self onTextFieldChanged:textField];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == self.firstNameText)
    {
        currentFirstName = textField.text;
        textField.text = [textField.text createMaskForText:NO];

    }
    else if(textField == self.lastNameText)
    {
        currentLastName = textField.text;
        textField.text =  [textField.text createMaskForText:NO];

    }
    else if(textField == self.phoneNumberText) {
        currentPhone = textField.text;
        textField.text =  [textField.text createMaskForText:NO];
    }

    else if(textField == self.emailText)
    {
        currentEmail = textField.text;
        if([textField.text containsString:@"@"])
        {
            textField.text = [textField.text createMaskForText:YES];
        }
    }
    else if(textField == self.answerTextField)
    {
        //self.answerTextField.text = MASKED_ANSWER;
        
        if (self.inputNewAnswer.length > 0) {
            NSMutableString* temp = [NSMutableString stringWithString:@""];
            for (int i=0; i < self.inputNewAnswer.length; i++) {
                [temp appendString:@"*"];
            }
            self.answerTextField.text = temp;
        }else{
            self.answerTextField.text = @"";
            [self handleMaskAnswer];
        }
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == self.firstNameText)
    {
        textField.text = currentFirstName;
    }
    
    else if(textField == self.lastNameText)
    {
        textField.text = currentLastName;
    }
    
    else if(textField == self.phoneNumberText)
    {
        if (currentPhone.length == 0) return;
        NSString* temp = currentPhone;
        if (![[currentPhone substringToIndex:1] isEqualToString:@"+"]) {
            temp = [NSString stringWithFormat:@"+%@",temp];
        }
        textField.text = temp;
    }
    else if(textField == self.emailText)
    {
        textField.text = currentEmail;
    }
    else if (textField == self.answerTextField) {
        textField.text = self.inputNewAnswer.length > 0 ? self.inputNewAnswer : @"";
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([self isKindOfClass:[MyProfileViewController class]]) {
        if (textField == self.emailText) {
            return NO;
        }
    }
    
    NSString *inputString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (textField == self.firstNameText || textField == self.lastNameText) {
        if (inputString.length <= 25) {
            [self checkTextFieldIsChange:textField shouldChangeCharactersInRange:range replacementString:string];
        }
    }else if (textField == self.phoneNumberText) {
        if (inputString.length <= 19) {
            [self checkTextFieldIsChange:textField shouldChangeCharactersInRange:range replacementString:string];
        }
    }else{
        [self checkTextFieldIsChange:textField shouldChangeCharactersInRange:range replacementString:string];
    }
    
    return [self updateTextFiel:textField shouldChangeCharactersInRange:range replacementString:string];
}

-(void)checkTextFieldIsChange:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
}

-(BOOL)updateTextFiel:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(string.length > 1)
    {
        NSMutableString *newString = [[NSMutableString alloc] initWithString:[string removeRedudantWhiteSpaceInText]];
        NSString *resultText = [textField.text stringByReplacingCharactersInRange:range
                                                                       withString:string];
        resultText = [resultText removeRedudantWhiteSpaceInText];
        
        if(textField == self.firstNameText || textField == self.lastNameText)
        {
            textField.text = (resultText.length > 25) ? [resultText substringWithRange:NSMakeRange(0, 25)] : resultText;

        }
        if(textField == self.phoneNumberText)
        {
            if([newString isValidPhoneNumber] && [newString isValidCountryCode])
            {
                textField.text = newString;
            }
        }
        
        if(textField == self.emailText)
        {
            //textField.text = newString;
            textField.text = (resultText.length > 96)?[resultText substringWithRange:NSMakeRange(0, 96)] : resultText;
        }
        
        return NO;
    }
    
    if(textField == self.firstNameText || textField == self.lastNameText)
    {
        if(textField.text.length == 25 && ![string isEqualToString:@""])
        {
            return NO;
        }
        return YES;
    }
    else if(textField == self.phoneNumberText)
    {
        if(textField.text.length == 19 && ![string isEqualToString:@""])
        {
            return NO;
        }
        [self.phoneNumberText updateAsPhoneNumberWhileChangeCharactersInRange:range replacementString:string];
        return NO;
    }
    else if(textField == self.emailText)
    {
        if([textField.text occurrenceCountOfCharacter:'@'] == 1 && [string isEqualToString:@"@"]){
            return NO;
        }
        
        return YES;
    }
    
    return YES;
}

-(void) onTextFieldChanged:(UITextField*) textfield {
}

#pragma mark LOGICAL FUNCTION
-(void)setTextViewsDefaultBottomBolder
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.firstNameText setBottomBolderDefaultColor];
        [self.lastNameText setBottomBolderDefaultColor];
        [self.emailText setBottomBolderDefaultColor];
        [self.phoneNumberText setBottomBolderDefaultColor];
        [self.salutationDropDown setBottomBolderDefaultColor];
        [self.answerTextField setBottomBolderDefaultColor];
    });
}
-(void) createAsteriskForTextField:(UITextField *)textField
{
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 8, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [textField setLeftView:leftView];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    
    asteriskImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"asterisk_icon"]];
    
    asteriskImage.frame = CGRectMake(0.0f, 0.0f, 20.0f, 20.0f);
    asteriskImage.contentMode = UIViewContentModeCenter;
    [textField setRightView:asteriskImage];
    [textField setRightViewMode:UITextFieldViewModeAlways];
    
}

- (void) changeUseLocation {
    
}

- (void) handleAsterickIcon{
    
}

-(NSString *)reformatForText:(UITextField *)textField WithNewString:(NSString *)newString WithRange:(NSRange)range
{
    NSMutableString *mutableString = [[NSMutableString alloc] initWithString:textField.text];
    NSRange newRange = range;
    if([newString isEqualToString:@""] && (range.location == 5 ||  range.location == 9))
    {
        newRange = NSMakeRange(range.location - 1, 1);
    }
    [mutableString replaceCharactersInRange:newRange withString:newString];
    mutableString = [[NSMutableString alloc] initWithString:[mutableString stringByReplacingOccurrencesOfString:@"-" withString:@""]];
    
    if(mutableString.length >= 1 && textField.tag == PHONE_NUMBER_TEXTFIELD_TAG)
    {
        [mutableString insertString:@"-" atIndex:1];
    }
    
    if(mutableString.length > 5)
    {
        [mutableString insertString:@"-" atIndex:5];
    }
    
    if(mutableString.length > 9 && textField.tag == PHONE_NUMBER_TEXTFIELD_TAG)
    {
        [mutableString insertString:@"-" atIndex:9];
    }
    
    return mutableString;
}
#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    
    CGPoint touchPoint = [touch locationInView:touch.view];
    
    UIView *view = touch.view.superview;
    if ([view isKindOfClass:[UITableViewCell class]]) {
        return NO;
    }else {
        if ([self isKindOfClass:[MyProfileViewController class]]) {
            for (UIView* view in touch.view.subviews) {
                if ((UITextField*)view == self.emailText && CGRectContainsPoint(self.emailText.frame, touchPoint)) {
                    if ([User isValid]) {
                        UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
                        self.emailText.text = userObject.email;
                        break;
                    }
//                    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
//                    if (profileDictionary.count > 2) {
//                        self.emailText.text = [profileDictionary objectForKey:keyEmail];
//                        break;
//                    }
                }else{
                    if([self.emailText.text containsString:@"@"])
                    {
                        self.emailText.text = [self.emailText.text createMaskForText:YES];
                    }
                }
            }
        }
        
    }
    
    return YES;
    
}

#pragma mark - DropDownViewDelegate
- (void)didSelectItem:(DropDownView *)dropDownView atIndex:(int)index{
    [self changeValueDropDown];
}

#pragma mark - Links Tapped

- (void)buildAgreeTextViewFromString:(NSString *)localizedString
{
    // 1. Split the localized string on the # sign:
    NSArray *localizedStringPieces = [localizedString componentsSeparatedByString:@"#"];
    self.confirmMessageView.userInteractionEnabled = YES;
    // 2. Loop through all the pieces:
    NSUInteger msgChunkCount = localizedStringPieces ? localizedStringPieces.count : 0;
    CGPoint wordLocation = CGPointMake(0.0, 0.0);
    for (NSUInteger i = 0; i < msgChunkCount; i++)
    {
        NSString *chunk = [localizedStringPieces objectAtIndex:i];
        if ([chunk isEqualToString:@""])
        {
            continue;     // skip this loop if the chunk is empty
        }
        
        // 3. Determine what type of word this is:
        BOOL isTermsOfServiceLink = [chunk hasPrefix:@"<ts>"];
        BOOL isPrivacyPolicyLink  = [chunk hasPrefix:@"<pp>"];
        BOOL isLink = (BOOL)(isTermsOfServiceLink || isPrivacyPolicyLink);
        
        // 4. Create label, styling dependent on whether it's a link:
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont fontWithName:FONT_MarkForMC_MED size:18.0];
        
        label.text = chunk;
        label.userInteractionEnabled = isLink;
        
        if (isLink)
        {
            // 5. Set tap gesture for this clickable text:
            SEL selectorAction = isTermsOfServiceLink ? @selector(tapOnTermsOfServiceLink:) : @selector(tapOnPrivacyPolicyLink:);
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                         action:selectorAction];
            [label addGestureRecognizer:tapGesture];
            
            // Trim the markup characters from the label:
            if (isTermsOfServiceLink)
            {
                label.text = [label.text stringByReplacingOccurrencesOfString:@"<ts>" withString:@""];
                termOfUseLbl = label;
            }
            
            
            if (isPrivacyPolicyLink)
            {
                label.text = [label.text stringByReplacingOccurrencesOfString:@"<pp>" withString:@""];
                policyLbl = label;
            }
            
            
            NSArray * objects = [[NSArray alloc] initWithObjects:[UIColor whiteColor], [NSNumber numberWithInt:NSUnderlineStyleSingle], nil];
            NSArray * keys = [[NSArray alloc] initWithObjects:NSForegroundColorAttributeName, NSUnderlineStyleAttributeName, nil];
            
            NSDictionary * linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
            
            NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:label.text attributes:linkAttributes];
            
            [label setAttributedText:attributedString];
            
        }
        else
        {
            UITapGestureRecognizer *tappedOutsideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enableSubmitButtonByConfirmation)];
            
            [label addGestureRecognizer:tappedOutsideKeyboard];
            label.userInteractionEnabled = YES;
            [label setTextColor:[UIColor whiteColor]];
            label.tag = 1000;
        }
        
        // 6. Lay out the labels so it forms a complete sentence again:
        
        // If this word doesn't fit at end of this line, then move it to the next
        // line and make sure any leading spaces are stripped off so it aligns nicely:
        
        [label sizeToFit];
        
        if (self.confirmMessageView.frame.size.width < wordLocation.x + label.bounds.size.width)
        {
            wordLocation.x = 0.0;                       // move this word all the way to the left...
            wordLocation.y += label.frame.size.height;  // ...on the next line
            
            // And trim of any leading white space:
            NSRange startingWhiteSpaceRange = [label.text rangeOfString:@"^\\s*"
                                                                options:NSRegularExpressionSearch];
            if (startingWhiteSpaceRange.location == 0)
            {
                label.text = [label.text stringByReplacingCharactersInRange:startingWhiteSpaceRange
                                                                 withString:@""];
                [label sizeToFit];
            }
        }
        
        // Set the location for this label:
        label.frame = CGRectMake(wordLocation.x,
                                 wordLocation.y,
                                 label.frame.size.width,
                                 label.frame.size.height);
        
        // Show this label:
        [self.confirmMessageView addSubview:label];
        
        // Update the horizontal position for the next word:
        wordLocation.x += label.frame.size.width;
    }
    
    if (self.confirmMessageView.subviews.count > 0) {
        UILabel* label = (UILabel*)self.confirmMessageView.subviews.lastObject;
        for (NSLayoutConstraint* c in self.confirmMessageView.constraints) {
            if ([c.firstItem isEqual:self.confirmMessageView] && c.firstAttribute == NSLayoutAttributeHeight) {
                c.constant = CGRectGetMaxY(label.frame);
                NSMutableArray* temp = [NSMutableArray new];
                if (termOfUseLbl) [temp addObject:termOfUseLbl];
                if (policyLbl) [temp addObject:policyLbl];
                [temp enumerateObjectsUsingBlock:^(UITextField*  _Nonnull label, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSArray * objects = [[NSArray alloc] initWithObjects:[UIColor whiteColor], [NSNumber numberWithInt:NSUnderlineStyleSingle], nil];
                    NSArray * keys = [[NSArray alloc] initWithObjects:NSForegroundColorAttributeName, NSUnderlineStyleAttributeName, nil];
                    
                    NSDictionary * linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
                    
                    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:label.text attributes:linkAttributes];
                    
                    [label setAttributedText:attributedString];
                }];
            }
        }
    }
}

- (void)enableSubmitButtonByConfirmation {
    self.submitButton.enabled = !self.submitButton.enabled;
    if (self.submitButton.enabled) {
        self.checkBoxImage.image = [UIImage imageNamed:@"checkbox_check"];
    }else{
        self.checkBoxImage.image = [UIImage imageNamed:@"checkbox_uncheck"];
    }
}

- (void)tapOnTermsOfServiceLink:(UITapGestureRecognizer *)tapGesture
{
    if([self isDisableSubmitTapped:tapGesture])
    {
        return;
    }
    
    [self dismissKeyboard];
    
    if (tapGesture.state == UIGestureRecognizerStateEnded)
    {
        TermsOfUseViewController *vc = [[TermsOfUseViewController alloc] init];
        //vc.delegate = self;
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        [self.navigationController pushViewController:vc animated:NO];
    }
}


- (void)tapOnPrivacyPolicyLink:(UITapGestureRecognizer *)tapGesture
{
    if([self isDisableSubmitTapped:tapGesture])
    {
        return;
    }
    
    [self dismissKeyboard];
    
    if (tapGesture.state == UIGestureRecognizerStateEnded)
    {
        PolicyViewController *vc = [[PolicyViewController alloc] init];
        //vc.delegate = self;
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        [self.navigationController pushViewController:vc animated:NO];
    }
}

-(BOOL)isDisableSubmitTapped:(id)sender
{
    if([sender isKindOfClass:[UITapGestureRecognizer class]])
    {
        UITapGestureRecognizer *tapRecognization = (UITapGestureRecognizer *)sender;
        CGPoint location = [tapRecognization locationInView:self.view];
        CGRect fingerRect = CGRectMake(location.x-5, location.y-5, 10, 10);
        
        if(CGRectIntersectsRect(fingerRect, self.submitButton.frame) && !self.submitButton.enabled)
        {
            [self dismissKeyboard];
            return YES;
        }
    }
    
    return NO;
}

@end
