//
//  SelectingCategoryDelegate.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/26/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SelectingCategoryDelegate <NSObject>

-(void)Explore:(id)view onSelectCategory:(id)item;

@end
