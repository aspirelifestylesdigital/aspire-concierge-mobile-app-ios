//
//  ForgotPasswordViewController.m
//  MobileConciergeUSDemo
//
//  Created by Den on 8/3/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "UITextField+Extensions.h"
#import "UIView+Extension.h"
#import "UIButton+Extension.h"
#import "ChallengeQuestionObject.h"
#import "NSString+Utis.h"
#import <AspireApiFramework/AspireApiFramework.h>

@interface ForgotPasswordViewController () <DropDownViewDelegate, UITextFieldDelegate> {
    CGFloat keyboardHeight;
    UITapGestureRecognizer *tappedOutsideKeyboards;
    NSMutableArray *questionArray;
    NSMutableArray *questionTextArray;
    ChallengeQuestionObject *selectedChallengeQuestion;

}

@end

@implementation ForgotPasswordViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpData];
    [self setUIStyte];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setTextViewsDefaultBottomBolder];
    tappedOutsideKeyboards = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tappedOutsideKeyboards.delegate = self;
    [self.navigationController.view addGestureRecognizer:tappedOutsideKeyboards];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController.view removeGestureRecognizer:tappedOutsideKeyboards];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}



#pragma mark - Setup Layout



- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}


- (void) setUIStyte {
    
    self.titleLable.attributedText = [UtilStyle setTextStyleForTitleViewControllerWithMessage:@"SIGN UP"];
  
    self.emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
   
    
    self.answerTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    self.emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    
    [self createAsteriskForTextField:self.emailTextField];
    //    [self createAsteriskForTextField:self.questionDropdown];
    [self createAsteriskForTextField:self.answerTextField];
    [self createAsteriskForTextField:self.passwordTextField];
    [self createAsteriskForTextField:self.confirmPasswordTextField];
    

    
    self.questionDropdown.leadingTitle = 8.0f;
    self.questionDropdown.title = @"Challenge Question";
    self.questionDropdown.listItems = questionTextArray;
    self.questionDropdown.titleColor = [AppColor placeholderTextColor];
    self.questionDropdown.isBreakLine = true;
    self.questionDropdown.delegate = self;
    
    self.emailTextField.delegate = self;
    self.answerTextField.delegate = self;
    self.passwordTextField.delegate = self;
    self.confirmPasswordTextField.delegate = self;    
    
    
    self.emailTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.emailTextField.text];
    self.answerTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.answerTextField.text];
    self.passwordTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.passwordTextField.text];
    self.confirmPasswordTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.confirmPasswordTextField.text];
   
    
    self.emailTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.emailTextField.placeholder];
    self.answerTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.answerTextField.placeholder];
    self.passwordTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.passwordTextField.placeholder];
    self.confirmPasswordTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.confirmPasswordTextField.placeholder];
   
    
    
    [self.view setBackgroundColorForView];
   
 
    
//    [self.submitButton setBackgroundColorForDisableStatus];
    [self.submitButton setBackgroundColorForNormalStatus];
    self.submitButton.enabled = NO;
    [self.submitButton setBackgroundColorForTouchingStatus];
    [self.submitButton configureDecorationForButton];
    
    
    [self.emailTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.answerTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.passwordTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.confirmPasswordTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
}


-(void) createAsteriskForTextField:(UITextField *)textField
{
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 8, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [textField setLeftView:leftView];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
}

-(void)setTextViewsDefaultBottomBolder
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.emailTextField setBottomBolderDefaultColor];
        [self.answerTextField setBottomBolderDefaultColor];
        [self.passwordTextField setBottomBolderDefaultColor];
        [self.confirmPasswordTextField setBottomBolderDefaultColor];
        [self.questionDropdown setBottomBolderDefaultColor];
    });
}

#pragma mark - Setup data

-(void) setUpData {
    questionArray = [[NSMutableArray alloc] init];
    questionTextArray = [[NSMutableArray alloc] init];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"ChallengeQuestion" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSError *jsonError;
    id allKeys = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
    
    
    for (int i=0; i<[allKeys count]; i++) {
        NSDictionary *arrayResult = [allKeys objectAtIndex:i];
        ChallengeQuestionObject *object = [[ChallengeQuestionObject alloc] initFromDict:arrayResult];
        [questionArray addObject:object];
        [questionTextArray addObject:object.questionText];
    }
}


#pragma mark - handle keyboard event

-(void)handleKeyboardWillShow:(NSNotification *)paramNotification
{
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         keyboardHeight = keyboardRect.size.height;
                         [self updateEdgeInsetForShowKeyboard];
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

-(void)handleKeyboardWillHide:(NSNotification *)paramNotification
{
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         [self updateEdgeInsetForHideKeyboard];
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
    
}


-(void)updateEdgeInsetForShowKeyboard
{
    _bottomButtonConstraint.constant =  keyboardHeight + 20;
    _heightConstraintHidenView.constant = 0;
    
}


-(void)updateEdgeInsetForHideKeyboard
{
    _bottomButtonConstraint.constant = 20;
    _heightConstraintHidenView.constant = 140;
    [self.view endEditing:true];
    [self resignFirstResponderForAllTextField];
    
}

-(void)resignFirstResponderForAllTextField
{
//    if(self.firstNameText.isFirstResponder)
//    {
//        [self.firstNameText resignFirstResponder];
//    }
//    else if(self.lastNameText.isFirstResponder)
//    {
//        [self.lastNameText resignFirstResponder];
//    }
//    else if(self.emailText.isFirstResponder)
//    {
//        [self.emailText resignFirstResponder];
//    }
//    else if(self.phoneNumberText.isFirstResponder)
//    {
//        [self.phoneNumberText resignFirstResponder];
//    }
//    else if(self.passwordText.isFirstResponder)
//    {
//        [self.passwordText resignFirstResponder];
//    }
//    else if(self.confirmPasswordText.isFirstResponder)
//    {
//        [self.confirmPasswordText resignFirstResponder];
//    }
}

-(void)dismissKeyboard {
    [self updateEdgeInsetForHideKeyboard];
}


#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    
//    CGPoint touchPoint = [touch locationInView:touch.view];
    
    UIView *view = touch.view.superview;
    if ([view isKindOfClass:[UITableViewCell class]]) {
        return NO;
    }
    return YES;
}

#pragma mark - Dropdown delegate
- (void)didSelectItem:(DropDownView *)dropDownView atIndex:(int)index {
    
    
//    NSString *valueSelected = [dropDownView.listItems objectAtIndex:index];
    selectedChallengeQuestion = [questionArray objectAtIndex:index];
    [self checkEnableSubmitButton];
//    _questionDropdown.title = valueSelected;
   
//    [self setButtonStatus:[self isChangeData]];
    
}

-(void)didShow:(DropDownView *)dropDownView {
    [self.view endEditing:true];
}

#pragma mark - UITextField delegate


-(void) makeBecomeFirstResponderForTextField
{
    [self resignFirstResponderForAllTextField];
    if(![self.emailTextField.text isValidEmail])
    {
        [self.emailTextField becomeFirstResponder];
    }
    else if(![self.passwordTextField.text isValidStrongPassword])
    {
        [self.passwordTextField becomeFirstResponder];
    }
    else if(![self.confirmPasswordTextField.text isEqualToString:self.passwordTextField.text])
    {
        [self.confirmPasswordTextField becomeFirstResponder];
    }
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.text = [textField.text removeRedudantWhiteSpaceInText];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
//    if(textField == self.emailTextField)
//    {
//        currentFirstName = textField.text;
        //textField.text = [textField.text createMaskForText:NO];
        
//    }
//    else if(textField == self.answerTextField)
//    {
//        currentLastName = textField.text;
        //textField.text =  [textField.text createMaskForText:NO];
        
//    }
//    else if(textField == self.passwordTextField)
//    {
        //currentPassword = textField.text;
//    }
//    else if(textField == self.confirmPasswordTextField)
//    {
        //currentConfirmPassword = textField.text;
//    }
}

//- (void)textFieldDidBeginEditing:(UITextField *)textField
//{
//    if(textField == self.firstNameText)
//    {
//        textField.text = currentFirstName;
//    }
//
//    else if(textField == self.lastNameText)
//    {
//        textField.text = currentLastName;
//    }
//
//    else if(textField == self.phoneNumberText)
//    {
//        textField.text = currentPhone;
//
//    }
//    else if(textField == self.emailText)
//    {
//        textField.text = currentEmail;
//    }
//    else if(textField == self.passwordText)
//    {
//        textField.text = currentPassword;
//    }
//}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.passwordTextField || textField == self.confirmPasswordTextField) {
        if ([string isEqualToString:@" "]) {
            return NO;
        }
    }
    return [self updateTextFiel:textField shouldChangeCharactersInRange:range replacementString:string];
}

-(void)checkTextFieldIsChange:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
}

- (void)textFieldDidChange:(UITextField *)textField {
    [self checkEnableSubmitButton];
}

-(BOOL)updateTextFiel:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(string.length > 1)
    {
        NSMutableString *newString = [[NSMutableString alloc] initWithString:[string removeRedudantWhiteSpaceInText]];
        NSString *resultText = [textField.text stringByReplacingCharactersInRange:range
                                                                       withString:string];
        resultText = [resultText removeRedudantWhiteSpaceInText];
        
        
        if(textField == self.passwordTextField || textField == self.confirmPasswordTextField)
        {
            if([newString isValidStrongPassword])//isValidStrongPassword
            {
                textField.text = newString;
            }
            else if(string.length > 25)
            {
                textField.text = [newString substringToIndex:25];
            }
            else
            {
                textField.text = newString;
            }
        }
        
        
        if(textField == self.emailTextField)
        {
            //textField.text = newString;
            textField.text = (resultText.length > 100)?[resultText substringWithRange:NSMakeRange(0, 100)] : resultText;
        }
        
        return NO;
    }
    
    
    if(textField == self.passwordTextField || textField == self.confirmPasswordTextField)
    {
        if(textField.text.length == 25 && ![string isEqualToString:@""])
        {
            return NO;
        }
        return YES;
    }
    
    else if(textField == self.emailTextField)
    {
        if([textField.text occurrenceCountOfCharacter:'@'] == 1 && [string isEqualToString:@"@"]){
            return NO;
        }
        
        return YES;
    }
    
   
    
    return YES;
}

-(void)checkEnableSubmitButton {
    [_submitButton setEnabled:![_emailTextField.text isEqualToString:@""] && ![_answerTextField.text isEqualToString:@""] && ![_passwordTextField.text isEqualToString:@""] && ![_confirmPasswordTextField.text isEqualToString:@""] && (_questionDropdown.valueStatus != DefaultValueStatus)];
}


#pragma mark - Action for button

-(IBAction)submitButtonTapped:(UIButton*)sender {
    [self dismissKeyboard];
    [self verifyData];
}



#pragma mark - Validate fields


-(void)verifyData {
     NSMutableString *message = [[NSMutableString alloc] init];
    [self verifyValueForTextField:self.emailTextField andMessageError:message];
    [self verifyValueForTextField:self.answerTextField andMessageError:message];
    [self verifyValueForTextField:self.passwordTextField andMessageError:message];
    [self verifyValueForTextField:self.confirmPasswordTextField andMessageError:message];
    [self forgotPasswordWithMessage:message isCreatedSuccessful:(message.length == 0)];
}


-(void)forgotPasswordWithMessage:(NSString *)message isCreatedSuccessful:(BOOL)isSuccessful
{
    if(isSuccessful)
    {
        [self updateEdgeInsetForHideKeyboard];
        [self setTextViewsDefaultBottomBolder];
        //[self updateProfileWithAPI];
        [self requestForgotPassword];
        
    }
    else
    {
        NSMutableString *newMessage = [[NSMutableString alloc] init];
        [newMessage appendString:@"\n"];
        [newMessage appendString:message];
        [newMessage appendString:@"\n"];
        
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = @"Please confirm that the value entered is correct:";
        alert.msgAlert = newMessage;
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
            alert.lblAlertMessage.textAlignment = NSTextAlignmentLeft;
            [alert.view layoutIfNeeded];
        });
        
        alert.blockFirstBtnAction = ^(void){
            [self makeBecomeFirstResponderForTextField];
        };
        
        [self showAlert:alert forNavigation:NO];
        
    }
}

-(BOOL)verifyValueForTextField:(UITextField *)textFied andMessageError:(NSMutableString *)message
{
    NSString *currentText = nil;
    NSString *errorMsg = nil;
    BOOL isValid = NO;
    if(textFied == self.emailTextField)
    {
        currentText = textFied.text;
        errorMsg = NSLocalizedString(@"input_invalid_email_msg", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidEmail];
    }
    else if(textFied == self.answerTextField)
    {
        currentText = textFied.text;
        errorMsg = NSLocalizedString(@"input_invalid_anwser_msg", nil);
        isValid = (textFied.isFirstResponder ? textFied.text: currentText).length > 0;
    }
    else if(textFied == self.passwordTextField)
    {
        currentText = textFied.text;
        //errorMsg = NSLocalizedString(@"input_invalid_weak_password_msg", nil);
        errorMsg = NSLocalizedString(@"input_invalid_password_msg", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidStrongPassword];//isValidStrongPassword
    }
    else if(textFied == self.confirmPasswordTextField)
    {
        currentText = textFied.text;
        errorMsg = NSLocalizedString(@"input_invalid_confirm_password_msg", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isEqualToString:_passwordTextField.text];
    }
    
    if((textFied.isFirstResponder ? textFied.text :currentText).length > 0)
    {
        if(!isValid)
        {
            (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
            [message appendString:errorMsg];
            [textFied setBottomBorderRedColor];
        }
        else
        {
            [textFied setBottomBolderDefaultColor];
        }
    }
    else
    {
        [textFied setBottomBorderRedColor];
    }
    
    return isValid;
}


#pragma mark - API

-(void)requestForgotPassword {
    [self startActivityIndicator];
    __weak typeof(self) _self = self;
   [ModelAspireApiManager forgotPasswordForEmail:_emailTextField.text withQuestion:[[AAFRecoveryQuestion alloc] initWithQuestion:selectedChallengeQuestion.questionText answer:_answerTextField.text    ] newPassword:[[AAFPassword alloc] initWithPassword:_passwordTextField.text] completion:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [_self stopActivityIndicator];
            if (!error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    AlertViewController *alert = [[AlertViewController alloc] init];
                    alert.msgAlert = NSLocalizedString(@"alert_noti_check_email", nil);
                    alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
                    alert.blockFirstBtnAction = ^{
                        [_self forgotPasswordSuccessRedirect];
                    };
                    dispatch_async(dispatch_get_main_queue(), ^{
                        alert.seconBtn.hidden = YES;
                        alert.midView.alpha = 0.0f;;
                    });
                    
                    [_self showAlert:alert forNavigation:NO];
                });
                
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UITextField *currentTextField = nil;
                    NSString* msg = @"* Enter a valid email address.";
                    switch ([AspireApiError getErrorTypeAPIForgetPasswordFromError:error]) {
                        case NETWORK_UNAVAILABLE_ERR:
                            msg = @"";
                            [_self showErrorNetwork];
                            break;
                        case EMAIL_INVALID_ERR:
                            msg = @"* Enter a valid email address.";
                            [_self.emailTextField setBottomBorderRedColor];
                            currentTextField = _emailTextField;
                            break;
                        case RECOVERYQUESTION_ERR:
                            currentTextField = _answerTextField;
                            msg = @"* The answer that you entered does not match. Click OK to try again.If you need assistance, contact us at xxx-xxx-xxxx.";
                            [_questionDropdown setBottomBorderRedColor];
                            [_answerTextField setBottomBorderRedColor];
                            break;
                        case FORGETPW_ERR:
                            currentTextField = _passwordTextField;
                            msg = @"* Password has been changed too recently";
                            [_self.passwordTextField setBottomBorderRedColor];
                            [_self.confirmPasswordTextField setBottomBorderRedColor];
                            break;
                        default:
                            msg = @"";
                            [_self showApiErrorWithMessage:@""];
                            break;
                    }
                    if (msg.length == 0) return;
                    AlertViewController *alert = [[AlertViewController alloc] init];
                    alert.titleAlert = @"Please confirm that the value entered is correct:";
                    alert.msgAlert = msg;
                    alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
                
                    alert.blockFirstBtnAction = ^{
                        if (currentTextField != nil) {
                            [currentTextField becomeFirstResponder];
                        }
                    };
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        alert.seconBtn.hidden = YES;
                        alert.midView.alpha = 0.0f;;
                        alert.lblAlertMessage.textAlignment = NSTextAlignmentLeft;
                    });
                    
                    
                    
                    [_self showAlert:alert forNavigation:NO];
                });
            }
        });
        
        
    }];
}

#pragma mark - Navigation
-(void) forgotPasswordSuccessRedirect {
    [self.navigationController popViewControllerAnimated:true];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
