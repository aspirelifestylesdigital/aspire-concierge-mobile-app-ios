//
//  SubCategoryViewController.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/26/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryViewController.h"

@interface SubCategoryViewController : CategoryViewController

@end
