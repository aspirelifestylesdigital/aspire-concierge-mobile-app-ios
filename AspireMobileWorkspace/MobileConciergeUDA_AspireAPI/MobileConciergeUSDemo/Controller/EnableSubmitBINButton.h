//
//  EnableSubmitBINButton.h
//  MobileConcierge
//
//  Created by Chung Mai on 7/3/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol EnableSubmitBINButton <NSObject>

-(void)enableSubmitBINButton:(NSString *)currentViewController;

@end
