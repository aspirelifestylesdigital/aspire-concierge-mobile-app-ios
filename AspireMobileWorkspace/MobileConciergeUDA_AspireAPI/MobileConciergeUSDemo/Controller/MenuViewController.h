//
//  MenuViewController.h
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/12/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"


@protocol ShowingRevealController <NSObject>

-(void)showReavealController;

@end

@interface MenuViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UITableView *menuTableView;
@property (nonatomic,weak) id<ShowingRevealController> delegate;
@end
