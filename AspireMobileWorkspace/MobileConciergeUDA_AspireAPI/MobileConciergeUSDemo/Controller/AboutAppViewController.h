//
//  AboutAppViewController.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/16/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface AboutAppViewController : BaseViewController

@property(weak, nonatomic) IBOutlet UIWebView *webView;
@end
