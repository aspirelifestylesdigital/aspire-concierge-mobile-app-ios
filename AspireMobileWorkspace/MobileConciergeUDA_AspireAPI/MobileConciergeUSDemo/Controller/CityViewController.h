//
//  CityViewController.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@class CityItem;
@protocol SelectingCityDelegate <NSObject>

-(void)getDataBaseOnCity:(CityItem *)item;

@end

@interface CityViewController : BaseViewController

@property(nonatomic, weak) IBOutlet UITableView *tableView;
@property(nonatomic, strong) NSArray *cityLst;
@property(nonatomic, weak) id<SelectingCityDelegate> delegate;
@property(nonatomic, strong) CityItem *currentCity;

@end
