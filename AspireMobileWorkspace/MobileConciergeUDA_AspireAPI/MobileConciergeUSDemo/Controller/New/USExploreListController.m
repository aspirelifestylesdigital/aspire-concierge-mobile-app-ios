//
//  USExploreSearchController.m
//  Test
//
//  Created by 😱 on 7/14/17.
//  Copyright © 2017 dai.pham.s3corp.com.vn. All rights reserved.
//

// views
#import "ExploreListView.h"

// controllers
#import "USCitiesListController.h"
#import "USExploreSearchController.h"

#import "USExploreListController.h"

@interface USExploreListController ()<ExploreListViewProtocol>

@end

@implementation USExploreListController

#pragma mark - 🚸 INIT 🚸
- (instancetype) init {
    if(self = [super init]) {
        ExploreListView* view = [ExploreListView new];
        view.delegate = self;
        self.view = view;
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

#pragma mark - 🚸 Explore List DELEGATE 🚸
- (void) ExploreListView:(ExploreListView *)view onSelectItem:(id)item {
    
}

- (void) ExploreListViewOnNavigateToViewSearch:(ExploreListView *)view {
    USExploreSearchController* vc = [USExploreSearchController new];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)ExploreListView:(ExploreListView *)view onSelectWithButtonIndex:(NSInteger)index {
    switch (index) {
        case 1: {
            USCitiesListController* vc = [USCitiesListController new];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 2: {
            USCitiesListController* vc = [USCitiesListController new];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        default:
            break;
    }
}
@end
