//
//  USExploreSearchController.m
//  Test
//
//  Created by 😱 on 7/14/17.
//  Copyright © 2017 dai.pham.s3corp.com.vn. All rights reserved.
//

// view
#import "ExploreSearchView.h"

#import "USExploreSearchController.h"

@interface USExploreSearchController ()<ExploreSearchViewProtocol>

@end

@implementation USExploreSearchController

#pragma mark - 🚸 INIT 🚸
- (instancetype) init {
    if(self = [super init]) {
        ExploreSearchView* view = [ExploreSearchView new];
//        view.delegate = self;
        self.view = view;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark - 🚸 Explore Search View Delegate 🚸
@end
