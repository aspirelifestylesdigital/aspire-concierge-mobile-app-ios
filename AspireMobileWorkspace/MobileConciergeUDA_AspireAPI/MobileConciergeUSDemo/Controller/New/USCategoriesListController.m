//
//  USCategoriesListController.m
//  Test
//
//  Created by Dai Pham on 7/19/17.
//  Copyright © 2017 dai.pham.s3corp.com.vn. All rights reserved.
//

// views
#import "CollectSimpleView.h"

// components


#import "USCategoriesListController.h"

@interface USCategoriesListController ()<CollectSimpleViewProtocol> {
    CollectSimpleView* viewSimple;
}

@end

@implementation USCategoriesListController

#pragma mark - 🚸 INIT 🚸
- (instancetype)init {
    if(self = [super init]) {
        viewSimple = [CollectSimpleView new];
        viewSimple.delegate = self;
        self.view = viewSimple;
//        [viewSimple reloadWithData:[[CitiesManager shared] getListCitiesForDining]];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark - 🚸 CollectSimpleView DELEGATE 🚸
- (void)CollectSimpleView:(CollectSimpleView *)view onSelectItem:(id)item {
    // do anything with item got
}

@end
