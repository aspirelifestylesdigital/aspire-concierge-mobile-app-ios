
#import "ConversationViewController.h"
#import "AppDelegate.h"

@interface ConversationViewController()

@property (nonatomic, strong) UIAlertController *alertController;

@end

@implementation ConversationViewController

-(void) viewDidLoad
{
    isNotAskConciergeBarButton = YES;
    [super viewDidLoad];
    //[self setNavigationBarWithDefaultColorAndTitle:@"", nil)];
    [self trackingScreenByName:@"Chat to concierge"];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setUpCustomizedPanGesturePopRecognizer];
 
    //Reset badge number
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)handlePopRecognizer:(UIPanGestureRecognizer*)recognizer {

}

- (IBAction)backButtonPressed:(id)sender {
    [[LPMessagingSDK instance] removeConversation:self.conversationQuery];
    
    AppDelegate *ap = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    if(ap.isPushMessageArrived)
    {
        ap.isPushMessageArrived = false;
        
        //If launched from push message then navigate back to origin
        if(self.navigationController.viewControllers.count >= 3)
        {
            UIViewController *popTo = self.navigationController.viewControllers[self.navigationController.viewControllers.count - 3];
            [[self navigationController] popToViewController:popTo animated:true];
        }
        else
            [[self navigationController] popViewControllerAnimated:true];
    }
    else
    {
        [[self navigationController] popViewControllerAnimated:true];
    }
    
}

- (IBAction)menuButtonPressed:(id)sender {
    
    bool isChatActive = [[LPMessagingSDK instance] checkActiveConversation:self.conversationQuery];
    
    self.alertController = [UIAlertController
                                          alertControllerWithTitle:@"Menu"
                                          message:@"Choose an option"
                                          preferredStyle:UIAlertControllerStyleActionSheet];
    
    /**
     This is how to resolve a conversation
     */
    UIAlertAction *resolveAction = [UIAlertAction actionWithTitle:@"Resolve the conversation" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self showResolveConfirmation:@"Resolve the conversation" message:@"Are you sure this topic is resolved?"];
    }];
    
    
    NSString *urgentTitle = [[LPMessagingSDK instance] isUrgent:self.conversationQuery] ? @"Dismiss urgency" : @"Mark as urgent";
    
    /**
     This is how to manage the urgency state of the conversation
     */
    UIAlertAction *urgentAction = [UIAlertAction actionWithTitle:urgentTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        bool isUrgent = [[LPMessagingSDK instance] isUrgent:self.conversationQuery];
        NSString* title = (isUrgent)? @"Dismiss urgency" : @"Mark as urgent";
        NSString* message = (isUrgent)? @"Are you sure you want to mark this conversation as not urgent?" : @"Are you sure you want to mark this conversation as urgent?";
        
        [self showUrgentConfirmation:title message:message];
        
    }];
    
    UIAlertAction *clearHistoryAction = [UIAlertAction actionWithTitle:@"Clear history" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self showClearConfirmation:@"Clear history" message:@"All of your existing conversation history will be lost. Are you sure?"];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [_alertController addAction:urgentAction];
    [_alertController addAction:clearHistoryAction];
    [_alertController addAction:resolveAction];
    [_alertController addAction:cancelAction];
    
    [urgentAction setEnabled:isChatActive];
    [resolveAction setEnabled:isChatActive];
    
    [self presentViewController:_alertController animated:true completion:^{
        
    }];
}

-(void) showResolveConfirmation:(NSString*)title message:(NSString*)message
{
    self.alertController = [UIAlertController
                                          alertControllerWithTitle:title
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    [_alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            }]];
    
    [_alertController addAction:[UIAlertAction actionWithTitle:@"Resolve" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [[LPMessagingSDK instance] resolveConversation:self.conversationQuery];

        
    }]];
    
    [self presentViewController:_alertController animated:true completion:^{
        
    }];
}

-(void) showUrgentConfirmation:(NSString*)title message:(NSString*)message
{
    self.alertController = [UIAlertController
                                          alertControllerWithTitle:title
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    [_alertController addAction:[UIAlertAction actionWithTitle:title style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if ([[LPMessagingSDK instance] isUrgent:self.conversationQuery]) {
            [[LPMessagingSDK instance] dismissUrgent:self.conversationQuery];
        } else {
            [[LPMessagingSDK instance] markAsUrgent:self.conversationQuery];
        }
        
    }]];
    
    [_alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    [self presentViewController:_alertController animated:true completion:^{
        
    }];
}

-(void) showClearConfirmation:(NSString*)title message:(NSString*)message
{
    self.alertController = [UIAlertController
                                          alertControllerWithTitle:title
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    [_alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    [_alertController addAction:[UIAlertAction actionWithTitle:@"Clear" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        bool isChatActive = [[LPMessagingSDK instance] checkActiveConversation:self.conversationQuery];
        if(isChatActive)
        {
            //Show alert to resolve first
            [self showResolveAlert:@"Clear history" message:@"Please resolve the conversation first"];
        }
        else
        {
            //Clear the conversation
            NSError* error;
            [[LPMessagingSDK instance] clearHistory:self.conversationQuery error:&error];
        }
    }]];
    
    [self presentViewController:_alertController animated:true completion:^{
        
    }];
}

-(void) showResolveAlert:(NSString*)title message:(NSString*)message
{
    self.alertController = [UIAlertController
                                          alertControllerWithTitle:title
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    [_alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
//    [_alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//
//    }]];
    
    [self presentViewController:_alertController animated:true completion:^{
        
    }];
}

-(void) dismissAlert
{
    [self.alertController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

@end
