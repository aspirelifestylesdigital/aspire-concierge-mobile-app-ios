
#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <LPMessagingSDK/LPMessagingSDK.h>

@interface ConversationViewController : BaseViewController

@property (nonatomic, strong) id <ConversationParamProtocol> conversationQuery;
@property (nonatomic, weak) NSString *account;

-(void) dismissAlert;

@end
