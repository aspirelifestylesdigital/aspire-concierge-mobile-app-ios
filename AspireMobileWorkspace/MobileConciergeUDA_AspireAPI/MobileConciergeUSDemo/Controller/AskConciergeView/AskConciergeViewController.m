//
//  AskConciergeViewController.m
//  MobileConcierge
//
//  Created by user on 5/9/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "AskConciergeViewController.h"
#import "Constant.h"
#import "UIButton+Extension.h"
#import "Constant.h"
#import "Common.h"
#import "WSB2CCreateConciergeCase.h"
#import "SWRevealViewController.h"
#import "HomeViewController.h"
#import "AskConciergeConfirmationViewController.h"
#import "AppDelegate.h"
#import<CoreTelephony/CTTelephonyNetworkInfo.h>
#import<CoreTelephony/CTCarrier.h>
#import <LPMessagingSDK/LPMessagingSDK.h>
#import <LPInfra/LPInfra.h>
#import <LPAMS/LPAMS.h>
@import AspireApiFramework;

@interface AskConciergeViewController ()<UITextViewDelegate, DataLoadDelegate, LPMessagingSDKdelegate>

@property (nonatomic, strong) id <ConversationParamProtocol> conversationQuery;

@end

@implementation AskConciergeViewController
{
    UILabel *lblPlaceholder;
    float backupHeightContentView;
    
    BOOL checkPhone;
    BOOL checkMail;
    BOOL viewControllerIsPopped;
    int timeToChange;
}

- (void)viewDidLoad {
    isNotAskConciergeBarButton = YES;
    [super viewDidLoad];
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"ask_concierge_title", nil)];
    [self createBackBarButton];
    [self trackingScreenByName:@"Ask concierge"];
    
    //Initialize Chat SDK
    [self initializeChatSDK];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self saveDraffInputContentRequestWithContent:self.tvAskConcierge.text];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
    
    [self dismissKeyboard];
    
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setUpCustomizedPanGesturePopRecognizer];
    if (viewControllerIsPopped == YES)
    {
        //[self.tvAskConcierge becomeFirstResponder];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    AppDelegate *ap = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    if(ap.isPushMessageArrived)
    {        
        //If launched from push message then navigate to Chat view
        [self showConversation];
    }
}

- (void)touchBack{
    /*
    AlertViewController *alert = [[AlertViewController alloc] init];
    //alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);
    alert.msgAlert = NSLocalizedString(@"askconcierge_back_confirm", nil);
    alert.firstBtnTitle = NSLocalizedString(@"arlet_ok_button", nil).uppercaseString;
    alert.blockFirstBtnAction = ^(void){
        [self.navigationController popViewControllerAnimated:YES];
    };
    [self showAlert:alert forNavigation:YES];
     */
//    [self saveDraffInputContentRequestWithContent:self.tvAskConcierge.text];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)saveDraffInputContentRequestWithContent:(NSString*)content {
    [[NSUserDefaults standardUserDefaults] setObject:content forKey:@"INPUT_CONTENT_REQUEST"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setInputContentRequest {
    NSString *draffInputContent = [[NSUserDefaults standardUserDefaults] objectForKey:@"INPUT_CONTENT_REQUEST"];
    draffInputContent = [draffInputContent stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.tvAskConcierge.text = draffInputContent;
    if (draffInputContent.length > 0) {
        lblPlaceholder.hidden = YES;
        self.btnAskConcierge.hidden = NO;
        [self showAccessoryView];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initView{
    [self scaleConstraintForSubView];
    [self.viewAccessories setBackgroundColor:[AppColor backgroundColor]];
    [self.btnCallConcierge configureDecorationBlackTitleButtonForTouchingStatus];
    [self.btnCallConcierge configureDecorationBlackTitleButtonForTouchingStatus];
    [self.btnCallConcierge setTitle:@"(800) 373-4830" forState:UIControlStateNormal];
    [self.btnLivechatOutlet configureDecorationBlackTitleButtonForTouchingStatus];
    [self.btnLivechatOutlet configureDecorationBlackTitleButton];
    
    [self.btnLivechatOutlet setTitle:@"LIVE CHAT" forState:UIControlStateNormal];
    
    [self.btnAskConcierge configureDecorationBlackTitleButtonForTouchingStatus];
    [self.btnAskConcierge setImage:[UIImage imageNamed:@"send_filled_icon"] forState:UIControlStateNormal];
    [self.btnAskConcierge setImage:[UIImage imageNamed:@"send_interact_icon"] forState:UIControlStateHighlighted];
    [self addAskConciergeTextViewPlaceHolder];
    if (self.itemName) {
        self.tvAskConcierge.text = [NSString stringWithFormat:@"%@\n",self.itemName];
        lblPlaceholder.hidden = YES;
        self.btnAskConcierge.hidden = NO;
        [self showAccessoryView];
    }else{
        self.btnAskConcierge.hidden = YES;
        [self hideAccessoryView];
        [self setInputContentRequest];
    }
    
    UITapGestureRecognizer *dismiss = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:dismiss];
    
    self.lblMessage.attributedText = letterSpacing(1.6, NSLocalizedString(@"call_the_concierge", nil));
    
    self.lblchat.attributedText = letterSpacing(1.6, NSLocalizedString(@"CHAT WITH A CONCIERGE NOW", nil));
    
    UIFont *font = [UIFont fontWithName:FONT_MarkForMC_BOLD size:[AppSize descriptionTextSize]];
    [self.lblRespondToMe setFont:font];
    self.lblRespondToMe.attributedText = letterSpacing(1.6, NSLocalizedString(@"respond_to_me", nil));
    
    self.tvAskConcierge.delegate = self;
    
    
    backupHeightContentView = self.heightConstraintAskConciergeView.constant;
    
//    [self setCheckButtonsStateWithPhone:NO andEmail:NO];
   
        [self.btnCheckPhone setAttributedTitle:[self stringWithImageName:@"checkbox_uncheck" andString:NSLocalizedString(@"checkbox_phone", nil)] forState:UIControlStateNormal];
  
        [self.btnCheckPhone setAttributedTitle:[self stringWithImageName:@"checkbox_check" andString:NSLocalizedString(@"checkbox_phone", nil)] forState:UIControlStateSelected];
 
        [self.btnCheckMail setAttributedTitle:[self stringWithImageName:@"checkbox_uncheck" andString:NSLocalizedString(@"checkbox_email", nil)] forState:UIControlStateNormal];

        [self.btnCheckMail setAttributedTitle:[self stringWithImageName:@"checkbox_check" andString:NSLocalizedString(@"checkbox_email", nil)] forState:UIControlStateSelected];
    
    if ([self.tvAskConcierge hasText]) {
        
    }

}


- (void)initData{
    
}


- (void)scaleConstraintForSubView{
    self.topConstraintAskConciergeView.constant = self.topConstraintAskConciergeView.constant*SCREEN_SCALE;
    self.topMessageLabel.constant = self.topMessageLabel.constant*SCREEN_SCALE;
    self.topConstrainAskConciergeTextView.constant = self.topConstrainAskConciergeTextView.constant*SCREEN_SCALE;
    self.heightConstraintAskConciergeView.constant = self.heightConstraintAskConciergeView.constant*SCREEN_SCALE;
    self.heightConstraintViewCallConcierge.constant = self.heightConstraintViewCallConcierge.constant*SCREEN_SCALE;
    
    self.leadConstraintAskConciergeTextView.constant = self.leadConstraintAskConciergeTextView.constant*SCREEN_SCALE;
    self.trailConstraintAskConciergeTextView.constant = self.trailConstraintAskConciergeTextView.constant*SCREEN_SCALE;
}

-(void)addAskConciergeTextViewPlaceHolder{
    lblPlaceholder = [[UILabel alloc] initWithFrame:CGRectMake(5.0, 2.0,self.tvAskConcierge.frame.size.width, (IPAD)?44.0:34.0)];
    
    [lblPlaceholder setText:NSLocalizedString(@"ask_concierge_placeholder", nil)];
    [lblPlaceholder setBackgroundColor:[UIColor clearColor]];
    [lblPlaceholder setTextColor:[AppColor placeholderTextColor]];
    lblPlaceholder.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR_It size:23*SCREEN_SCALE];
    dispatch_async(dispatch_get_main_queue(), ^{
        [lblPlaceholder setFrame:CGRectMake(5.0, 2.0,self.tvAskConcierge.frame.size.width, (IPAD)?44.0:34.0)];
    });
    [self.tvAskConcierge addSubview:lblPlaceholder];
}


- (void) showAccessoryView{
    if (self.viewAccessories.hidden == YES) {
        self.viewAccessories.alpha = 0;
        self.viewAccessories.hidden = NO;
        [UIView animateWithDuration:0.3 animations:^{
            self.viewAccessories.alpha = 1;
        }];
    }
}
- (void) hideAccessoryView{
    if (self.viewAccessories.hidden == NO){
        [UIView animateWithDuration:0.3 animations:^{
            self.viewAccessories.alpha = 0;
        } completion: ^(BOOL finished) {//creates a variable (BOOL) called "finished" that is set to *YES* when animation IS completed.
            self.viewAccessories.hidden = finished;//if animation is finished ("finished" == *YES*), then hidden = "finished" ... (aka hidden = *YES*)
        }];
    }
}

- (void)dismissKeyboard{
    [self.tvAskConcierge resignFirstResponder];
}

- (NSAttributedString*) stringWithImageName:(NSString*)imageName andString:(NSString*)string{
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    attachment.image = [UIImage imageNamed:imageName];
    attachment.bounds = CGRectMake(0, -2*SCREEN_SCALE, 18*SCREEN_SCALE, 18*SCREEN_SCALE);
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    
    NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithAttributedString:attachmentString];
    
    
    [myString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"   %@", string]]];
    [myString addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize titleTextSize]] range:NSMakeRange(4, string.length)];
    [myString addAttribute:NSForegroundColorAttributeName value:[AppColor normalTextColor] range:NSMakeRange(4, string.length)];
    
    return myString;
}

- (void) expandTextView{
    [UIView animateWithDuration:0.2 animations:^{
//        self.heightConstraintAskConciergeView.constant = backupHeightContentView +  100*SCREEN_SCALE_BY_HEIGHT;
        self.heightConstraintAskConciergeView.constant = backupHeightContentView;
        [self.view layoutIfNeeded];
    }];
}

- (void) collapseTextView{
    [UIView animateWithDuration:0.2 animations:^{
        self.heightConstraintAskConciergeView.constant = backupHeightContentView;
        [self.view layoutIfNeeded];
    }];
}

- (IBAction)touchAskConcierge:(id)sender {
    
    [self.tvAskConcierge endEditing:YES];
    if (self.btnCheckPhone.selected == NO && self.btnCheckMail.selected == NO) {
        [self stopActivityIndicator];
        AlertViewController *alert = [[AlertViewController alloc] init];
        //alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);
        alert.msgAlert = NSLocalizedString(@"noncheck_respond_to_me_alert", nil);;
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
        });
        
        [self showAlert:alert forNavigation:NO];
    }else{
        [self startActivityIndicator];
        
        WSB2CCreateConciergeCase *wSB2CCreateConciergeCase = [[WSB2CCreateConciergeCase alloc] init];
        wSB2CCreateConciergeCase.delegate = self;
        if (!self.categoryName) {
            self.categoryName = @"";
        }
        if (!self.cityName) {
            self.cityName = @"";
        }
        
        UserObject *userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
        
        NSString *requestDetails = [NSString stringWithFormat:@"%@%@\n%@",
                                    [self.cityName isEqualToString:@""] ? @"" : [NSString stringWithFormat:@"\n%@", self.cityName],
                                    [self.categoryName isEqualToString:@""] ? @"" : [NSString stringWithFormat:@"\n%@", self.categoryName],
                                    self.tvAskConcierge.text];
        NSString *responseString = @"";
        
        if (self.btnCheckMail.selected == YES && self.btnCheckPhone.selected == YES) {
            responseString = [responseString stringByAppendingString:[NSString stringWithFormat:@"Respond by %@ or Respond by %@\n\n",userObject.mobileNumber,userObject.email]];
        } else {
            if (self.btnCheckPhone.selected == YES) {
                responseString = [responseString stringByAppendingString:[NSString stringWithFormat:@"Respond by %@\n\n",userObject.mobileNumber]];
            }else if (self.btnCheckMail.selected == YES) {
                responseString = [responseString stringByAppendingString:[NSString stringWithFormat:@"Respond by %@\n\n",userObject.email]];
            }
        }
        requestDetails = [responseString stringByAppendingString:requestDetails];
        
        NSDictionary *dict = @{@"requestDetails": requestDetails,
//                               @"requestType":self.categoryName,
                               @"requestType":@"O Client Specific",
                               @"requestCity":[self.cityName isEqualToString:@""] ? @"N/A" : self.cityName};
        __weak typeof (self) _self = self;
        [ModelAspireApiManager createRequestWithUserInfo:dict completion:^(NSError *error) {
            [_self stopActivityIndicator];
            if (!error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_self saveDraffInputContentRequestWithContent:@""];
                    [_self trackingEventByName:@"Request accepted" withAction:SubmitActionType withCategory:RequestCategoryType];
                    AskConciergeConfirmationViewController *vc = [[AskConciergeConfirmationViewController alloc] init];
                    [_self.navigationController pushViewController:vc animated:YES];
                    [_self.tvAskConcierge setText:@""];
                    lblPlaceholder.hidden = NO;
                    _self.btnAskConcierge.hidden = YES;
                    _self.btnCheckMail.selected = NO;
                    _self.btnCheckPhone.selected = NO;
                    viewControllerIsPopped = YES;
                    [_self hideAccessoryView];
                });
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_self trackingEventByName:@"Request denied" withAction:SubmitActionType withCategory:RequestCategoryType];
                    NSString *mess;
                    switch ([AspireApiError getErrorTypeAPICreateRequestFromError:error]) {
                        case ACCESSTOKEN_INVALID_ERR:
                            [_self handleInvalidCredentials];
                            break;
                        case UNKNOWN_ERR:
                            mess = @"";
                            break;
                        case NETWORK_UNAVAILABLE_ERR:
                            [_self showErrorNetwork];
                            break;
                        default:
                            mess = @"";
                            break;
                    }
                    if(mess != nil){
                        AlertViewController *alert = [[AlertViewController alloc] init];
                        //alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);
                        alert.msgAlert = NSLocalizedString(@"askconcierge_fail_message", nil);
                        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
                        dispatch_async(dispatch_get_main_queue(), ^{
                            alert.seconBtn.hidden = YES;
                            alert.midView.alpha = 0.0f;;
                        });
                        
                        [_self showAlert:alert forNavigation:YES];
                    }
                });
            }
        }];
        return;
        [wSB2CCreateConciergeCase askConciergeWithMessage:dict];
    }
}


- (IBAction)touchCallConcierge:(id)sender {
    self.btnCallConcierge.enabled = false;
    [self setUpTimeDisableButton];
    NSString *phone_number = @"8003734830";
    [self callWithPhoneString:phone_number];
}

- (IBAction)touchInternationalCall:(id)sender {
    self.btnInternationalCall.enabled = false;
    [self setUpTimeDisableButton];
    NSString *phone_number = @"17038914643";
    [self callWithPhoneString:phone_number];
}

-(void)setUpTimeDisableButton {
    [NSTimer scheduledTimerWithTimeInterval:1.0f
                                     target:self
                                   selector:@selector(timerCheck:)
                                   userInfo:nil
                                    repeats:YES];
}


- (void)timerCheck:(NSTimer*)timer{
    timeToChange += 1;
    if (timeToChange == 2) {
        self.btnCallConcierge.enabled = true;
        self.btnInternationalCall.enabled = true;
        [timer invalidate];
        timeToChange = 0;
    }
}


- (void) callWithPhoneString:(NSString*)phoneString{
    CTTelephonyNetworkInfo* info = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier* carrier = info.subscriberCellularProvider;
    NSString *isoCountryCode = carrier.isoCountryCode;
    
    if (!isoCountryCode && iosVersion() < 10.0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:phoneString message:@"" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *call = [UIAlertAction actionWithTitle:@"Call" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSString *phoneStr = [[NSString alloc] initWithFormat:@"tel:%@",phoneString];
            NSURL *phoneURL = [[NSURL alloc] initWithString:phoneStr];
            [[UIApplication sharedApplication] openURL:phoneURL];
        }];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:call];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        NSString *phoneStr = [[NSString alloc] initWithFormat:@"tel:%@",phoneString];
        NSURL *phoneURL = [[NSURL alloc] initWithString:phoneStr];
        [[UIApplication sharedApplication] openURL:phoneURL];
        
    }

}

/*
 // UITextViewDelegate
 */
- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (![textView hasText]) {
        lblPlaceholder.hidden = NO;
        self.btnAskConcierge.hidden = YES;
    }
}


- (void) textViewDidBeginEditing:(UITextView *)textView{
    lblPlaceholder.hidden = YES;
}



-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    NSString *resultText = [textView.text stringByReplacingCharactersInRange:range
                                                                   withString:text];
    NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
    resultText = [resultText stringByTrimmingCharactersInSet:charSet];

    if ([resultText isEqualToString:@""]) {
        lblPlaceholder.hidden = YES;
        self.btnAskConcierge.hidden = YES;
        [self hideAccessoryView];
    }else{
        lblPlaceholder.hidden = YES;
        self.btnAskConcierge.hidden = NO;
        [self showAccessoryView];
    }
    return YES;
}


/*
 // DataLoadDelegate
 */
- (void)loadDataDoneFrom:(id<WSBaseProtocol>)ws{
    [self stopActivityIndicator];
    [self trackingEventByName:@"Request accepted" withAction:SubmitActionType withCategory:RequestCategoryType];
    AskConciergeConfirmationViewController *vc = [[AskConciergeConfirmationViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
    [self.tvAskConcierge setText:@""];
    lblPlaceholder.hidden = NO;
    self.btnAskConcierge.hidden = YES;
    self.btnCheckMail.selected = NO;
    self.btnCheckPhone.selected = NO;
    viewControllerIsPopped = YES;
    [self hideAccessoryView];
}

- (void)loadDataFailFrom:(id<BaseResponseObjectProtocol>)result withErrorCode:(NSInteger)errorCode{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self stopActivityIndicator];
        [self trackingEventByName:@"Request denied" withAction:SubmitActionType withCategory:RequestCategoryType];
        if(errorCode == 1005 || errorCode == -1009){
            [self showErrorNetwork];
        }else{
            AlertViewController *alert = [[AlertViewController alloc] init];
            //alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);
            alert.msgAlert = NSLocalizedString(@"askconcierge_fail_message", nil);
            alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
            dispatch_async(dispatch_get_main_queue(), ^{
                alert.seconBtn.hidden = YES;
                alert.midView.alpha = 0.0f;;
            });
            
            [self showAlert:alert forNavigation:YES];
        }
    });
}


- (void)loadDataFailFrom:(id<WSBaseProtocol>)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message{
    [self stopActivityIndicator];
    [self trackingEventByName:@"Request denied" withAction:SubmitActionType withCategory:RequestCategoryType];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }
}

- (IBAction)touchPhone:(id)sender {
    self.btnCheckPhone.selected = !((UIButton *)sender).selected;
}


- (IBAction)touchMail:(id)sender {
    self.btnCheckMail.selected = !((UIButton *)sender).selected;
}

- (void)WSBaseNetworkUnavailable{
    [self stopActivityIndicator];
}

- (IBAction)btnLivechatAction:(id)sender {
    self.outlLvechat.highlighted = NO;
    [self.outlLvechat setTitleColor:colorFromHexString(@"#C54B53") forState:UIControlStateHighlighted];
    [self.outlLvechat setWhiteBackgroundColorForTouchingStatus];
  
    [self showConversation];
}

#pragma mark - Chat Stuffs
-(void) initializeChatSDK
{
    [LPMessagingSDK instance].delegate = self;
    [self setChatSDKConfigurations];
    [[LPMessagingSDK instance] subscribeLogEvents:LogLevelInfo logEvent:^(LPLog *log) {
        NSLog(@"LPMessaging Log: %@", log.text);
    }];
    
    NSString *account = CHAT_ACCOUNT_ID;
    if (account.length == 0) {
        return;
    }
    
    NSError *error = nil;
    [[LPMessagingSDK instance] initialize:account monitoringInitParams:nil error:&error];
    if (error) {
        NSLog(@"LPMessagingSDK Initialize Error: %@",error);
        return;
    }
}

/**
 This method sets the SDK configurations.
 For example:
 Change background color of remote user (such as Agent)
 Change background color of user (such as Consumer)
 */
-(void) setChatSDKConfigurations
{
    LPConfig *configurations = [LPConfig defaultConfiguration];
    configurations.remoteUserBubbleBackgroundColor = colorFromHexString(@"#FFFFFF");
    configurations.remoteUserBubbleBorderColor = colorFromHexString(@"#FFFFFF");
    configurations.remoteUserBubbleLinkColor = colorFromHexString(@"#9D0329");
    configurations.remoteUserBubbleTextColor =  colorFromHexString(@"#011627");
    configurations.remoteUserBubbleBorderWidth = 0;
    configurations.remoteUserBubbleTimestampColor = colorFromHexString(@"#F2F2F2");
    configurations.remoteUserTypingTintColor = colorFromHexString(@"#99A1A8");
    configurations.remoteUserBubbleLongPressOverlayColor = colorFromHexString(@"#C7D4D9");
    configurations.remoteUserBubbleLongPressOverlayAlpha = 0.3;
    configurations.remoteUserBubbleTopLeftCornerRadius = 8;
    configurations.remoteUserBubbleTopRightCornerRadius = 8;
    configurations.remoteUserBubbleBottomLeftCornerRadius = 8;
    configurations.remoteUserBubbleBottomRightCornerRadius = 8;
    
    configurations.userBubbleBackgroundColor = colorFromHexString(@"#FFFFFF");
    configurations.userBubbleBorderColor = colorFromHexString(@"#FFFFFF");
    configurations.userBubbleLinkColor = colorFromHexString(@"#9D0329");
    configurations.userBubbleTextColor = colorFromHexString(@"#001627");
    configurations.userBubbleBorderWidth = 0;
    configurations.userBubbleTimestampColor =  colorFromHexString(@"#F2F2F2");
    configurations.userBubbleSendStatusTextColor = colorFromHexString(@"#F2F2F2");
    configurations.userBubbleErrorTextColor = colorFromHexString(@"#C54B53");
    configurations.userBubbleErrorBorderColor = colorFromHexString(@"#C54B53");
    configurations.userBubbleLongPressOverlayColor = colorFromHexString(@"#C7D4D9");
    configurations.userBubbleLongPressOverlayAlpha = 0.3;
    configurations.userBubbleTopLeftCornerRadius = 8;
    configurations.userBubbleTopRightCornerRadius = 8;
    configurations.userBubbleBottomLeftCornerRadius = 8;
    configurations.userBubbleBottomRightCornerRadius = 8;
    configurations.bubbleEmailLinksRegex = nil;
    configurations.bubbleUrlLinksRegex = nil;
    configurations.bubblePhoneLinksRegex = nil;
    configurations.bubbleTopPadding = 10;
    configurations.bubbleBottomPadding = 10;
    configurations.bubbleLeadingPadding = 10;
    configurations.bubbleTrailingPadding = 10;
    configurations.bubbleTimestampTopPadding = 5;
    configurations.bubbleTimestampBottomPadding = 5;
    configurations.enableLinkPreview = true;
    
    configurations.linkPreviewBackgroundColor = colorFromHexString(@"#FFFFFF");
    configurations.linkPreviewTitleTextColor = colorFromHexString(@"#001627");
    configurations.linkPreviewDescriptionTextColor = colorFromHexString(@"#99A1A8");
    configurations.linkPreviewSiteNameTextColor = colorFromHexString(@"#9D0329");
    configurations.linkPreviewBorderWidth = 0;
    configurations.urlRealTimePreviewBackgroundColor = colorFromHexString(@"#FFFFFF");
    configurations.urlRealTimePreviewBorderColor = colorFromHexString(@"#F2F2F2");
    configurations.urlRealTimePreviewBorderWidth = 0;
    configurations.urlRealTimePreviewTitleTextColor = colorFromHexString(@"#9D0329");
    configurations.urlRealTimePreviewDescriptionTextColor = colorFromHexString(@"#99A1A8");
    configurations.useNonOGTagsForLinkPreview = true;
    configurations.enablePhotoSharing = true;
    configurations.maxNumberOfSavedFilesOnDisk = 20;
    
    configurations.photosharingMenuBackgroundColor = colorFromHexString(@"#C7D4D9");
    configurations.photosharingMenuButtonsBackgroundColor = [UIColor grayColor];
    configurations.photosharingMenuButtonsTintColor = colorFromHexString(@"#FFFFFF");
    configurations.photosharingMenuButtonsTextColor = colorFromHexString(@"#9D0329");
    configurations.cameraButtonEnabledColor = colorFromHexString(@"#4D0315");
    configurations.cameraButtonDisabledColor = colorFromHexString(@"#9D0329");
    // configurations.fileCellLoaderFillColor = colorFromHexString(@"#FFFFFF");
    configurations.fileCellLoaderRingProgressColor = colorFromHexString(@"#E5EBEE");
    configurations.fileCellLoaderRingBackgroundColor = colorFromHexString(@"#FFFFFF");
    
    configurations.sendButtonDisabledColor = colorFromHexString(@"#4D0315");
    configurations.sendButtonEnabledColor = colorFromHexString(@"#9D0329");
    configurations.isSendMessageButtonInTextMode = true;
    configurations.systemBubbleTextColor = colorFromHexString(@"#E5EBEE");
    // configurations.customButtonIconName = nil;
    configurations.checkmarkVisibility = CheckmarksStateAll;
    configurations.checkmarkSentColor = colorFromHexString(@"#E5EBEE");
    configurations.checkmarkDistributedColor = colorFromHexString(@"#F2F2F2");
    configurations.checkmarkReadColor = colorFromHexString(@"#FFAFOE");
    configurations.isReadReceiptTextMode = true;
    configurations.csatSubmitButtonCornerRadius = 30;
    configurations.csatSubmitButtonBackgroundColor = colorFromHexString(@"#9D0329");
    configurations.csatSubmitButtonTextColor = colorFromHexString(@"#FFFFFF");
    configurations.csatRatingButtonSelectedColor = colorFromHexString(@"#9D0329");
    //configurations.csatResolutionButtonSelectedColor = colorFromHexString(@"#D20A3B");
    
    configurations.csatAllTitlesTextColor = [UIColor blackColor];
    configurations.csatResolutionHidden = true;
    configurations.csatAgentViewHidden = false;
    configurations.csatThankYouScreenHidden = false;
    configurations.csatNavigationBackgroundColor = colorFromHexString(@"#001629");
    configurations.csatNavigationTitleColor = colorFromHexString(@"#FFFFFF");
    configurations.csatSkipButtonColor = [UIColor blackColor];
    configurations.csatUIStatusBarStyleLightContent = true;
    configurations.csatShowSurveyView = true;
    configurations.csatSurveyExpirationInMinutes = 1440;
    configurations.maxPreviousConversationToPresent = 2;
    configurations.deleteClosedConversationOlderThanMonths = 13;
    configurations.sendingMessageTimeoutInMinutes = 60;
    configurations.enableConversationSeparatorTextMessage = true;
    configurations.enableConversationSeparatorTextMessage = true;
    configurations.enableConversationSeparatorLine = true;
    configurations.conversationSeparatorTextColor = colorFromHexString(@"#E5EBEE");
    configurations.conversationSeparatorFontSize = UIFontTextStyleCaption1;
    configurations.conversationSeparatorTopPadding = 1.1;
    configurations.conversationSeparatorBottomPadding = 7;
    configurations.enableVibrationOnMessageFromRemoteUser = false;
    //configurations.scrollToBottomButtonBackgroundColor = UIColor.white
    //configurations.scrollToBottomButtonMessagePreviewTextColor = UIColor.white
    //configurations.scrollToBottomButtonBadgeBackgroundColor = UIColor.white
    //configurations.scrollToBottomButtonBadgeTextColor = UIColor.white
    //configurations.scrollToBottomButtonArrowColor = UIColor.white
    //configurations.unreadMessagesDividerBackgroundColor = UIColor.white
    //configurations.unreadMessagesDividerTextColor = UIColor.white
    configurations.scrollToBottomButtonEnabled = false;
    configurations.scrollToBottomButtonMessagePreviewEnabled = false;
    configurations.unreadMessagesDividerEnabled = false;
    //configurations.unreadMessagesCornersRadius = 1.1;
    //configurations.scrollToBottomButtonCornerRadius = 1.1;
    // configurations.scrollToBottomButtonBadgeCornerRadius = 1.1;
    
    configurations.country = nil;
    configurations.language = LPLanguageDevice;
    configurations.brandName = @"CHAT";
    configurations.conversationBackgroundColor = colorFromHexString(@"#011627");
    configurations.customFontNameConversationFeed = nil;
    configurations.customFontNameNonConversationFeed = nil;
    configurations.customRefreshControllerImagesArray = nil;
    configurations.customRefreshControllerAnimationSpeed = 2;
    configurations.dateSeparatorTitleBackgroundColor = colorFromHexString(@"#011627");
    configurations.dateSeparatorTextColor =  colorFromHexString(@"#E5EBEE");
    configurations.dateSeparatorLineBackgroundColor = colorFromHexString(@"#E5EBEE");
    configurations.dateSeparatorBackgroundColor = colorFromHexString(@"#011627");
    configurations.dateSeparatorFontSize = UIFontTextStyleFootnote;
    configurations.customFontNameDateSeparator = nil;
    configurations.dateSeparatorTopPadding = 0;
    configurations.dateSeparatorBottomPadding = 0;
    configurations.inputTextViewContainerBackgroundColor = colorFromHexString(@"#F2F2F2");
    configurations.inputTextViewTopBorderColor = colorFromHexString(@"#F2F2F2");
    configurations.retrieveAssignedAgentFromLastClosedConversation = true;
    configurations.notificationShowDurationInSeconds = 3;
    configurations.ttrShowShiftBanner = true;
    configurations.ttrFirstTimeDelay = 10;
    configurations.ttrShouldShowTimestamp = false;
    configurations.ttrShowFrequencyInSeconds = 8;
    configurations.showUrgentButtonInTTRNotification = false;
    configurations.showOffHoursBanner = true;
    configurations.ttrBannerBackgroundColor =  colorFromHexString(@"#253847");
    configurations.ttrBannerTextColor = colorFromHexString(@"#FFFFFF");
    configurations.ttrBannerOpacityAlpha = 0.8;
    configurations.offHoursTimeZoneName = @" ";
    configurations.lpDateFormat = nil;
    configurations.lpTimeFormat = nil;
    configurations.lpDateTimeFormat = nil;
    configurations.toastNotificationsEnabled = true;
    
    //configurations.csdsDomain = [NSString stringWithFormat:@"https://adminlogin.liveperson.net/csdr/account/%@/service/baseURL.json?version=1.0", CHAT_ACCOUNT_ID];
    configurations.remoteUserAvatarBackgroundColor = colorFromHexString(@"#E5EBEE");
    configurations.remoteUserAvatarLeadingPadding = 8;
    configurations.remoteUserAvatarTrailingPadding = 8;
    configurations.remoteUserAvatarIconColor  = colorFromHexString(@"#001627");
    configurations.remoteUserDefaultAvatarImage  = nil;
    configurations.brandAvatarImage = nil;
    configurations.csatAgentAvatarBackgroundColor = colorFromHexString(@"#E5EBEE");
    configurations.csatAgentAvatarIconColor = colorFromHexString(@"#001627");
    configurations.enableClientOnlyMasking = false;
    configurations.enableRealTimeMasking = false;
    configurations.clientOnlyMaskingRegex = @" ";
    configurations.realTimeMaskingRegex = @" ";
    
    configurations.conversationNavigationBackgroundColor = colorFromHexString(@"#001627");
    configurations.conversationNavigationTitleColor = colorFromHexString(@"#FFFFFF");
    configurations.conversationStatusBarStyle = UIStatusBarStyleLightContent;
    configurations.secureFormBackButtonColor = colorFromHexString(@"#FFFFFF");
    configurations.secureFormUIStatusBarStyleLightContent = true;
    configurations.secureFormNavigationBackgroundColor = colorFromHexString(@"#001627");
    configurations.secureFormNavigationTitleColor = colorFromHexString(@"#FFFFFF");
    configurations.secureFormBubbleBackgroundColor = [UIColor whiteColor];
    configurations.secureFormBubbleBorderColor = colorFromHexString(@"#FFFFFF");
    configurations.secureFormBubbleBorderWidth = 0;
    configurations.secureFormBubbleTitleColor = colorFromHexString(@"#011627");
    configurations.secureFormBubbleDescriptionColor = colorFromHexString(@"#011627");
    configurations.secureFormBubbleFillFormButtonTextColor = colorFromHexString(@"#FFFFFF");
    configurations.secureFormBubbleFillFormButtonBackgroundColor = colorFromHexString(@"#9D0329");
    configurations.secureFormBubbleFormImageTintColor = colorFromHexString(@"#253847");
    configurations.secureFormCustomFontName = @"Proxima nova";
    
    configurations.secureFormHideLogo = false;
    configurations.secureFormBubbleLoadingIndicatorColor = colorFromHexString(@"#9D0329");
    configurations.enableStrucutredContent = false;
    configurations.structuredContentBubbleBorderWidth = 0.3;
    configurations.structuredContentBubbleBorderColor = [UIColor clearColor];
    configurations.structuredContentMapLatitudeDeltaDeltaSpan = 0.01;
    configurations.structuredContentMapLongitudeDeltaSpan = 0.01;
    configurations.connectionStatusConnectingBackgroundColor = colorFromHexString(@"#253847");
    configurations.connectionStatusConnectingTextColor = colorFromHexString(@"#F2F2F2");
    configurations.connectionStatusFailedToConnectBackgroundColor = colorFromHexString(@"#C7D4D9");
    configurations.connectionStatusFailedToConnectTextColor = colorFromHexString(@"#9D0329");
    configurations.controllerBubbleTextColor = colorFromHexString(@"#FFFFFF");
    configurations.announceAgentTyping = true;
    [LPConfig printAllConfigurations];
    
    [self setCustomButton];
}

/**
 This method sets the user details such as first name, last name, profile image and phone number.
 */
- (void)setUserDetails {
        UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
    NSString* account = CHAT_ACCOUNT_ID;
    
    NSString* firstName = userObject.firstName;
    NSString* lastName = userObject.lastName;
    NSString* email = userObject.email;
    NSString* phone = userObject.mobileNumber;
    
    LPUser *user = [[LPUser alloc] initWithFirstName:firstName lastName:lastName nickName:@"" uid:nil profileImageURL:@"" phoneNumber:[NSString stringWithFormat:@"%@|%@",phone,email] employeeID:nil];
    [[LPMessagingSDK instance] setUserProfile:user brandID:account];

//    NSString* account = CHAT_ACCOUNT_ID;
//
//    NSDictionary* userDict = [[SessionData shareSessiondata] getUserInfo];
//    NSString* firstName = [userDict valueForKey:keyFirstName];
//    NSString* lastName = [userDict valueForKey:keyLastName];
//    NSString* email = [userDict valueForKey:keyEmail];
//    NSString* phone = [userDict valueForKey:keyMobileNumber];
//
//    LPUser *user = [[LPUser alloc] initWithFirstName:firstName lastName:lastName nickName:@"" uid:nil profileImageURL:@"" phoneNumber:[NSString stringWithFormat:@"%@|%@",phone,email] employeeID:nil];
//    [[LPMessagingSDK instance] setUserProfile:user brandID:account];
}

/**
 This method lets you enter a UIBarButton to the navigation bar (in window mode).
 When the button is pressed it will call the following delegate method: LPMessagingSDKCustomButtonTapped
 */
- (void)setCustomButton {
    UIImage *customButtonImage = [UIImage imageNamed:@"phone_icon"];
    [[LPMessagingSDK instance] setCustomButton: customButtonImage];
}

/**
 This method shows the conversation screen. It considers different modes:
 */
- (void)showConversation {
    NSString *account = CHAT_ACCOUNT_ID;
    self.conversationQuery = [[LPMessagingSDK instance] getConversationBrandQuery:account campaignInfo:nil];
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    self.conversationViewController = [storyBoard instantiateViewControllerWithIdentifier:@"ConversationViewController"];
    self.conversationViewController.account = account;
    self.conversationViewController.conversationQuery = self.conversationQuery;
    
    LPConversationViewParams *conversationViewParams = [[LPConversationViewParams alloc] initWithConversationQuery:self.conversationQuery containerViewController:self.conversationViewController isViewOnly:NO conversationHistoryControlParam:nil];
    
    [[LPMessagingSDK instance] showConversation:conversationViewParams authenticationParams:nil];
    
    [[self navigationController] pushViewController:self.conversationViewController animated:true];
    
    [self setUserDetails];
    [[self view] endEditing:YES];
}

#pragma mark - LPMessagingSDKDelegate

/**
 This delegate method is required.
 It is called when authentication process fails
 */
- (void)LPMessagingSDKAuthenticationFailed:(NSError *)error {
    NSLog(@"Error: %@",error);
}

/**
 This delegate method is required.
 It is called when the SDK version you're using is obselete and needs an update.
 */
- (void)LPMessagingSDKObseleteVersion:(NSError *)error {
    NSLog(@"Error: %@",error);
}

/**
 This delegate method is optional.
 It is called each time the SDK receives info about the agent on the other side.
 
 Example:
 You can use this data to show the agent details on your navigation bar (in view controller mode)
 */
- (void)LPMessagingSDKAgentDetails:(LPUser *)agent {
    if (agent != nil) {
        [self.conversationViewController setNavigationBarWithDefaultColorAndTitle:agent.nickName];
    } else {
        [self.conversationViewController setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"concierge", nil)];
    }
}

/**
 This delegate method is optional.
 It is called each time the SDK menu is opened/closed.
 */
- (void)LPMessagingSDKActionsMenuToggled:(BOOL)toggled {
    
}

/**
 This delegate method is optional.
 It is called each time the agent typing state changes.
 */
- (void)LPMessagingSDKAgentIsTypingStateChanged:(BOOL)isTyping {
    
}

/**
 This delegate method is optional.
 It is called after the customer satisfaction page is submitted with a score.
 */
- (void)LPMessagingSDKCSATScoreSubmissionDidFinish:(NSString *)accountID rating:(NSInteger)rating {
    
}

/**
 This delegate method is optional.
 If you set a custom button, this method will be called when the custom button is clicked.
 */
- (void)LPMessagingSDKCustomButtonTapped {
    
}

/**
 This delegate method is optional.
 It is called whenever an event log is received.
 */
- (void)LPMessagingSDKDidReceiveEventLog:(NSString *)eventLog {
    
}

/**
 This delegate method is optional.
 It is called when the SDK has connections issues.
 */
- (void)LPMessagingSDKHasConnectionError:(NSString *)error {
    
}

/**
 This delegate method is required.
 It is called when the token which used for authentication is expired
 */
- (void)LPMessagingSDKTokenExpired:(NSString *)brandID {
    
}

/**
 This delegate method is required.
 It lets you know if there is an error with the SDK and what the error is
 */
- (void)LPMessagingSDKError:(NSError *)error {
    
}

/**
 This delegate method is optional.
 It is called when a new conversation has started, from the agent or from the consumer side.
 */
- (void)LPMessagingSDKConversationStarted:(NSString *)conversationID {
    
}

/**
 This delegate method is optional.
 It is called when a conversation has ended, from the agent or from the consumer side.
 */
- (void)LPMessagingSDKConversationEnded:(NSString *)conversationID {
    [self.conversationViewController dismissAlert];
}

/**
 This delegate method is optional.
 It is called each time connection state changed for a brand with a flag whenever connection is ready.
 Ready means that all conversations and messages were synced with the server.
 */
- (void)LPMessagingSDKConnectionStateChanged:(BOOL)isReady brandID:(NSString *)brandID {
    
}

/**
 This delegate method is optional.
 It is called when the customer satisfaction survey is dismissed after the user has submitted the survey/
 */
- (void)LPMessagingSDKConversationCSATDismissedOnSubmittion:(NSString *)conversationID {
    
}

/**
 This delegate method is optional.
 It is called when the conversation view controller removed from its container view controller or window.
 */
- (void)LPMessagingSDKConversationViewControllerDidDismiss {
    
}

/**
 This delegate method is optional.
 It is called when the user tapped on the agent’s avatar in the conversation and also in the navigation bar within window mode.
 */
- (void)LPMessagingSDKAgentAvatarTapped:(LPUser *)agent {
    
}

/**
 This delegate method is optional.
 It is called when the Conversation CSAT did load
 */
- (void)LPMessagingSDKConversationCSATDidLoad:(NSString *)conversationID {
    
}

/**
 This delegate method is optional.
 It is called when the Conversation CSAT skipped by the consumer
 */
- (void)LPMessagingSDKConversationCSATSkipped:(NSString *)conversationID {
    
}

/**
 This delegate method is optional.
 It is called when the user is opening photo sharing gallery/camera and the persmissions denied
 */
- (void)LPMessagingSDKUserDeniedPermission:(enum LPPermissionTypes)permissionType {
    
}

@end
