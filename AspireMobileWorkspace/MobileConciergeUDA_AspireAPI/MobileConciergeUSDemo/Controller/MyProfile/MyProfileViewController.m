//
//  MyProfileViewController.m
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/13/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "MyProfileViewController.h"
#import "ImageUtilities.h"
#import "NSString+Utis.h"
#import "Constant.h"
#import "SWRevealViewController.h"
#import "UIButton+Extension.h"
#import "HomeViewController.h"
#import "ChallengeQuestionObject.h"


#import "AppDelegate.h"
#import "UserObject.h"
#import "UITextField+Extensions.h"
#import "UtilStyle.h"
#import "LocationComponent.h"
@import AspireApiFramework;
#import "PreferenceObject.h"

@interface MyProfileViewController ()<DropDownViewDelegate> {
}


@end

@implementation MyProfileViewController
{
    NSString *firstNameOnTyping;
    NSString *lastNameOnTyping;
    NSString *emailOnTyping;
    NSString *phoneOnTyping;
    NSString *oldSalutation;
    BOOL isTempUseLocation;
    
    NSMutableArray *questionArray;
    NSMutableArray *questionTextArray;
    ChallengeQuestionObject *selectedChallengeQuestion;
    
    BOOL isFirstLoad;
    
    int indexQuestionFirstTime;
    int currentIndexQuestion;
}

#pragma mark - Life Cycle
- (void)viewDidLoad
{
    isFirstLoad = true;
    isTempUseLocation = [[SessionData shareSessiondata] isUseLocation];
    isNotAskConciergeBarButton = YES;
    self.isUpdateProfile = YES;
    [super viewDidLoad];
    [self createBackBarButton];
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"my_profile_menu_title", nil)];
    backupBottomConstraint = self.viewActionBottomConstraint.constant;
    [self setButtonStatus:NO];
    
    if([[SessionData shareSessiondata] isUseLocation]) {
        [[SessionData shareSessiondata] setIsShownLocationServiceAlert:NO];
        [LocationComponent startRequestLocation];
    }
    
    [self setUpData];
    
    __block UITextField* answerTextfield = self.answerTextField;
    
    // retrieve profile
    typeof(self) __weak _self = self;
    [self startActivityIndicator];
    [ModelAspireApiManager retrieveProfileCurrentUser:^(User *user, NSError *error) {
        if (!_self) return;
        typeof(self) __self = _self;
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error) {
                // store user local
                UserObject* u = (UserObject *)[user convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([UserObject class])];
                [[SessionData shareSessiondata] setUserObject:u];
                
                [_self getUserInfo];
                
                __self->firstNameOnTyping = u.firstName;
                __self->lastNameOnTyping = u.lastName;
                __self->phoneOnTyping = [u.mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
                __self->emailOnTyping = u.email;
                __self->oldSalutation = u.salutation;
                
                // get sercure question
                [ModelAspireApiManager getProfileFromEmail:[[SessionData shareSessiondata] UserObject].email completion:^(OKTAUserProfile * _Nullable OKTAuser, User * _Nullable user, NSError *error) {
                    if (!_self || !OKTAuser) return;
                    [_self stopActivityIndicator];
                    typeof(self) __self = _self;
                    int i = 0;
                    for (ChallengeQuestionObject* obj in __self->questionArray) {
                        if ([OKTAuser.credentials.recoveryQuestion.question isEqualToString:obj.questionText]) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                answerTextfield.text = MASKED_ANSWER;
                                [__self->_questionDropdown setSelectedIndex:i];
                            });
                            break;
                        }
                        i++;
                    }
                }];
            } else {
                [_self stopActivityIndicator];
                if ([AspireApiError getErrorTypeAPIRetrieveProfileFromError:error] == ACCESSTOKEN_INVALID_ERR) {
                    [_self showAlertInvalidUserAndShouldLoginAgain:true];
                    return;
                }
                [_self showAlertWithTitle:NSLocalizedString(@"cannot_get_data", nil)
                                  message:NSLocalizedString(@"error_api_message", nil)
                                  buttons:@[NSLocalizedString(@"ok_button_title", nil)]
                                  actions:nil
                         messageAlignment:NSTextAlignmentCenter];
                
            }
        });
    }];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [@[self.firstNameText,self.lastNameText,self.answerTextField,self.phoneNumberText] enumerateObjectsUsingBlock:^(UITextField*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(textFieldChanged:)
                                                     name:UITextFieldTextDidChangeNotification
                                                   object:obj];
    }];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    //  Set up Pop pan gesture
    [self setUpCustomizedPanGesturePopRecognizer];
    

    if(self.navigationController.viewControllers.count == 1)
    {
        HomeViewController *homeView = [[HomeViewController alloc] init];
        [self.navigationController setViewControllers:@[homeView,self]];
    }
    
    self.submitButton.hidden = YES;
    //self.cancelButton.hidden = YES;
    self.commitmentLabel.hidden = YES;
    
    [self setTextViewsDefaultBottomBolder];
    [self.view layoutIfNeeded];
    
    //[self setDefaultData];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (isTempUseLocation != isUseLocation) {
        [[SessionData shareSessiondata] setIsUseLocation:isTempUseLocation];
    }
    [@[self.firstNameText,self.lastNameText,self.answerTextField,self.phoneNumberText] enumerateObjectsUsingBlock:^(UITextField*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:obj];
    }];
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}

- (void) onTextFieldChanged:(UITextField *)textfield {
    [textfield setOriginText:textfield.text];
    if (textfield == self.firstNameText) {
        firstNameOnTyping = textfield.text;
    } else if (textfield == self.phoneNumberText) {
        phoneOnTyping = textfield.text;
    } else if (textfield == self.lastNameText) {
        lastNameOnTyping = textfield.text;
    }else if (textfield == self.answerTextField) {
        self.inputNewAnswer = textfield.text;
    }
    [self setButtonStatus:([self checkFieldIsChangeString] || [self isChangeAnswer])];
    if (self.answerTextField.text.length == 0 ||
        [self.answerTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 3) {
        [self.answerTextField setBottomBolderDefaultColor];
    }
}

-(void)textFieldChanged:(NSNotification*)notification {
    UITextField* txt = (UITextField*)[notification object];
    [self onTextFieldChanged:txt];
}

#pragma mark - Setup View

- (void)initView{
    self.phoneNumberText.tag = PHONE_NUMBER_TEXTFIELD_TAG;
    [self setButtonFrame];
    
    self.emailText.textColor = [AppColor placeholderTextColor];
    
    [self.updateButton setBackgroundColorForNormalStatus];
    [self.updateButton setBackgroundColorForTouchingStatus];
    [self.updateButton configureDecorationForButton];
    
    [self.cancelButtonMyProfile setBackgroundColorForNormalStatus];
    [self.cancelButtonMyProfile setBackgroundColorForTouchingStatus];
    [self.cancelButtonMyProfile configureDecorationForButton];
    
    // Text Style For Greet Message
    NSString *message = NSLocalizedString(@"commitment_my_profile_message", nil);
    self.commitmentLabel.attributedText = [UtilStyle setLargeSizeStyleForLabelWithMessage:message];
    
    [self setCheckBoxState:isCheck];
    self.commitmentLabel.text = NSLocalizedString(@"commitment_my_profile_message", nil);
    [self.updateButton setTitle:NSLocalizedString(@"update_button", nil) forState:UIControlStateNormal];
    [self.cancelButtonMyProfile setTitle:NSLocalizedString(@"cancel_button", nil) forState:UIControlStateNormal];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize titleTextSize]],
                                 NSKernAttributeName: @1.4};
    self.updateButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.updateButton.titleLabel.text attributes:attributes];
    self.cancelButtonMyProfile.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.cancelButtonMyProfile.titleLabel.text attributes:attributes];
    
    [self.updateButtonView setBackgroundColor:[AppColor backgroundColor]];
    [self.cancelButtonView setBackgroundColor:[AppColor backgroundColor]];
    
    self.questionDropdown.leadingTitle = 3.0f;
    self.questionDropdown.itemDisplay = 4;
    self.questionDropdown.titleFontSize = [AppSize titleTextSize];
    self.questionDropdown.title = @"Security Question";
    self.questionDropdown.titleColor = [AppColor placeholderTextColor];
    self.questionDropdown.isBreakLine = true;
    self.questionDropdown.delegate = self;
    self.questionDropdown.itemsFont = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.questionDropdown.titleLabel.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    [self.questionDropdown setItemHeight:50];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.answerTextField setBottomBolderDefaultColor];
        [self.questionDropdown setBottomBolderDefaultColor];
    });
}

-(void) setUpData {
    questionArray = [[NSMutableArray alloc] init];
    questionTextArray = [[NSMutableArray alloc] init];
    
    id allKeys = [ModelAspireApiManager getSecurityQuetions];
    
    for (int i=0; i<[allKeys count]; i++) {
        NSDictionary *arrayResult = [allKeys objectAtIndex:i];
        ChallengeQuestionObject *object = [[ChallengeQuestionObject alloc] initFromDict:arrayResult];
        [questionArray addObject:object];
        [questionTextArray addObject:object.questionText];
    }
    
    self.questionDropdown.listItems = questionTextArray;
}

- (void)setButtonStatus:(BOOL)status{
    self.updateButton.enabled = status;
    self.cancelButtonMyProfile.enabled = status;
}


- (void) setButtonFrame{
}

-(void) createAsteriskForTextField:(UITextField *)textField{
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [textField setLeftView:leftView];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    
    asteriskImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"asterisk_icon"]];
    
    asteriskImage.frame = CGRectMake(0.0f, 0.0f, 20.0f, 20.0f);
    asteriskImage.contentMode = UIViewContentModeCenter;
    [textField setRightView:asteriskImage];
    [textField setRightViewMode:UITextFieldViewModeAlways];
}

#pragma mark - Setup Data
- (void)updateSuccessRedirect{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert =nil;
    alert.msgAlert = NSLocalizedString(@"update_profile_success_message", nil);
    alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
    dispatch_async(dispatch_get_main_queue(), ^{
        alert.seconBtn.hidden = YES;
        alert.midView.alpha = 0.0f;;
        [alert.view layoutIfNeeded];
    });
    
    alert.blockFirstBtnAction = ^(void){
        isTempUseLocation = isUseLocation;
        [self getUserInfo];
        [self setTextViewsDefaultBottomBolder];
        [self setButtonStatus:NO];
    };
    
    [self showAlert:alert forNavigation:NO];
}

#pragma mark API PROCESS
- (void)updateProfileWithAPI {
    [self startActivityIndicator];

    UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
    NSMutableDictionary *userDict = [[NSMutableDictionary alloc] init];
    [userDict checkAndSetValue:(self.salutationDropDown.title.length > 1) ? self.salutationDropDown.title : @" " forKey:@"Salutation"];
    [userDict checkAndSetValue:[firstNameOnTyping stringByTrimmingCharactersInSet:
                                [NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"FirstName"];
    [userDict checkAndSetValue:[lastNameOnTyping stringByTrimmingCharactersInSet:
                                [NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"LastName"];
    [userDict checkAndSetValue:[phoneOnTyping stringByReplacingOccurrencesOfString:@"+" withString:@""] forKey:@"MobileNumber"];
    [userDict checkAndSetValue:emailOnTyping forKey:@"Email"];
    [userDict checkAndSetValue:[[SessionData shareSessiondata] BINNumber] forKey:APP_USER_PREFERENCE_Passcode];
    if ([User isValid]) {
        if(userObject.currentCity.length > 0)
            [userDict checkAndSetValue:userObject.currentCity forKey:APP_USER_PREFERENCE_Selected_City];
    }
    [userDict checkAndSetValue:isUseLocation? @"ON": @"OFF" forKey:APP_USER_PREFERENCE_Location];
    [userDict checkAndSetValue:@"USA" forKey:@"homeCountry"];
    [userDict checkAndSetValue:@"USA" forKey:@"Country"];
    [userDict checkAndSetValue:APP_NAME forKey:@"referenceName"];
    [userDict checkAndSetValue:[self getPreferenceByType:DiningPreferenceType].value forKey:@"Dining Preference"];
    [userDict checkAndSetValue:[self getPreferenceByType:HotelPreferenceType].value forKey:@"Hotel Preference"];
    [userDict checkAndSetValue:[self getPreferenceByType:TransportationPreferenceType].value forKey:@"Car Type Preference"];
    [userDict checkAndSetValue:[userObject.currentCity isEqualToString:@""] ? @"Unknown" : userObject.currentCity  forKey:@"City"];
    
    __block NSString*answer = self.inputNewAnswer;
    
    typeof(self) __weak _self = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self startActivityIndicator];
    });
    
    __block BOOL isChangeString = [self checkFieldIsChangeString];
    __block BOOL isChangeAnswer = [answer stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0;
    __block NSError* errorTotal = nil;
    
    dispatch_queue_t queue = dispatch_queue_create("com.s3corp.update", DISPATCH_QUEUE_SERIAL);
    
    // update profile
    dispatch_async(queue, ^{
        if (isChangeString) {
            dispatch_suspend(queue);
            [ModelAspireApiManager updateProfileCurrentUserWithUserInfo:userDict completion:^(NSError *error) {
                errorTotal = error;
                dispatch_resume(queue);
            }];
        }
    });
    
    // change answer
    dispatch_async(queue, ^{
        if(errorTotal == nil && isChangeAnswer) {
            dispatch_suspend(queue);
            [ModelAspireApiManager changeRecoveryQuestionForEmail:[[SessionData shareSessiondata] UserObject].email password:[[AAFPassword alloc] initWithPassword:[AccountAuthentication password]] question:[[AAFRecoveryQuestion alloc] initWithQuestion:selectedChallengeQuestion.questionText answer:answer] completion:^(NSError * _Nullable error) {
                errorTotal = error;
                dispatch_resume(queue);
            }];
        }
    });
    
    // result
    dispatch_barrier_async(queue, ^{
        typeof(self) __self = _self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [_self stopActivityIndicator];
            if (!errorTotal) {
                __self.answerTextField.text = MASKED_ANSWER;
                __self.inputNewAnswer = @"";
                indexQuestionFirstTime = currentIndexQuestion;
                
                [[SessionData shareSessiondata] setUserObject:(UserObject *)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([UserObject class])]];
                [_self updateSuccessRedirect];
                return;
            }
            
            NSString* message = @"";
            switch ([AspireApiError getErrorTypeAPIUpdateProfileFromError:errorTotal]) {
                case NETWORK_UNAVAILABLE_ERR:{
                    [_self showErrorNetwork];
                }
                    break;
                case UNKNOWN_ERR:
                    message = NSLocalizedString(@"error_api_message", nil);
                    break;
                    
                default:
                    message = NSLocalizedString(@"error_api_message", nil);
                    break;
            }
            
            switch ([AspireApiError getErrorTypeAPIChangeRecoveryQuestionOKTAFromError:errorTotal]) {
                case NETWORK_UNAVAILABLE_ERR:{
                    [_self showErrorNetwork];
                }
                    break;
                case UNKNOWN_ERR:
                    message = NSLocalizedString(@"error_api_message", nil);
                    break;
                case ANSWER_ERR:
                    message = NSLocalizedString(@"input_invalid_answer_msg", nil);
                    break;
                case EMAIL_INVALID_ERR:
                    message = NSLocalizedString(@"error_api_message", nil);
                    break;
                case CHANGE_RECOVERY_QUESTION_ERR:
                    message = NSLocalizedString(@"error_api_message", nil);
                    break;
                default:
                    message = NSLocalizedString(@"error_api_message", nil);
                    break;
            }
            
            if (message.length == 0) return;
            AlertViewController *alert = [[AlertViewController alloc] init];
            alert.titleAlert = NSLocalizedString(@"cannot_get_data", nil);
            alert.msgAlert = message;
            alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
            alert.seconBtn.hidden = YES;
            alert.midView.hidden = YES;
            alert.lblAlertMessage.textAlignment = NSTextAlignmentCenter;
            [alert.view layoutIfNeeded];
            [_self showAlert:alert forNavigation:NO];
        });
    });
}


- (void)setDefaultData {
    UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
    firstNameOnTyping = userObject.firstName;
    lastNameOnTyping = userObject.lastName;
    emailOnTyping = userObject.email;
    NSString *tempPhone = userObject.mobileNumber;
    phoneOnTyping = (tempPhone.length < 18) && ![tempPhone containsString:@"+"] ? [NSString stringWithFormat:@"+%@",tempPhone] : tempPhone;
}

- (void)updateProfileButtonStatus{
    [self setButtonStatus:([self checkFieldIsChangeString] || [self isChangeAnswer])];
}

#pragma mark - On Change Data

- (void)changeUseLocation{
    [self setButtonStatus:([self checkFieldIsChangeString] || [self isChangeAnswer])];
}

- (void)changeValueDropDown{
    [self setButtonStatus:([self checkFieldIsChangeString] || [self isChangeAnswer])];
}

- (BOOL) isChangeAnswer {
    return [self.answerTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0 &&
    ![self.answerTextField.text isEqualToString:MASKED_ANSWER];
}

- (BOOL) checkFieldIsChangeString{
    
    if (isUseLocation != isTempUseLocation) {
        return YES;
    }
    if (![[self.salutationDropDown.title lowercaseString] isEqualToString:[oldSalutation lowercaseString]]) {
        return true;
    }
    UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
    if (![self.salutationDropDown.title isEqualToString:userObject.salutation]) {
        return YES;
    }else if (![userObject.firstName isEqualToString:firstNameOnTyping]) {
        return YES;
    }else if (![userObject.lastName isEqualToString:lastNameOnTyping]){
        return YES;
    }
    else if (![userObject.mobileNumber isEqualToString:[phoneOnTyping stringByReplacingOccurrencesOfString:@"+" withString:@""]]){
        return YES;
    }
    return NO;
}

#pragma mark - Action Events
- (IBAction)touchUpdate:(id)sender {
    [self dismissKeyboard];
    [self verifyAccountData:YES];
}



- (IBAction)touchCancel:(id)sender {
    
    [self.phoneNumberText endEditing:YES];
    [self dismissKeyboard];
    [self setDefaultData];
    [self setTextViewsDefaultBottomBolder];
    [self setButtonStatus:NO];
    [[SessionData shareSessiondata] setIsUseLocation:isTempUseLocation];
    [self getUserInfo];
    
    self.inputNewAnswer = @"";
    self.answerTextField.text = MASKED_ANSWER;
    [self.questionDropdown setSelectedIndex:indexQuestionFirstTime];
    selectedChallengeQuestion = [questionArray objectAtIndex:indexQuestionFirstTime];
}

#pragma mark - Dropdown delegate
- (void)didSelectItem:(DropDownView *)dropDownView atIndex:(int)index {
    currentIndexQuestion = index;
    
    if (![dropDownView isEqual:self.questionDropdown]) {
        [self changeValueDropDown];
        return;
    }
    
    if (isFirstLoad) {
        isFirstLoad = false;
        indexQuestionFirstTime = index;
    }else{
        self.inputNewAnswer = @"";
        if (index != indexQuestionFirstTime) {
            self.answerTextField.text = @"";
        }else{
            self.answerTextField.text = MASKED_ANSWER;
        }
        
    }
    
    selectedChallengeQuestion = [questionArray objectAtIndex:index];
    if ([dropDownView.titleLabel.text isEqualToString:@"Security Question"]) {
        dropDownView.titleColor = [AppColor placeholderTextColor];
    } else {
        dropDownView.titleColor = [AppColor textColor];
    }
    
    //[self handleMaskAnswer];
    
    [self updateProfileButtonStatus];
}

- (void)handleMaskAnswer
{
    if (indexQuestionFirstTime == currentIndexQuestion) {
        [self.answerTextField setText: MASKED_ANSWER];
    }
}

-(void)didShow:(DropDownView *)dropDownView {
    [self.view endEditing:true];
}
@end
