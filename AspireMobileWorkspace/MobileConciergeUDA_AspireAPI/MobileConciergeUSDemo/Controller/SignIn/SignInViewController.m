//
//  SignInViewController.m
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 7/14/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "SignInViewController.h"
#import "SWRevealViewController.h"
#import "CreateProfileViewController.h"
#import "UDAForgotPasswordViewController.h"
#import "HomeViewController.h"
#import "WSSignIn.h"
#import "WSB2CGetAccessToken.h"
#import "WSB2CGetUserDetails.h"
#import "WSB2CGetRequestToken.h"
#import "UserRegistrationItem.h"
#import "UtilStyle.h"
#import "UIButton+Extension.h"
#import "NSString+Utis.h"
#import "NSString+AESCrypt.h"
#import "UITextField+Extensions.h"
#import "AppDelegate.h"
#import "MenuViewController.h"
#import "ChangePasswordViewController.h"
#import "PasscodeItem.h"
#import "PasscodeViewController.h"

#import "AppData.h"
#import "PreferenceObject.h"
#import "WSB2CPasscodeVerfication.h"
#import "PasscodeViewController.h"
#import "MenuViewController.h"
#import "WSPreferences.h"
@import AspireApiFramework;
#import "NSDictionary+SBJSONHelper.h"
#import "StoreUserDefaults.h"

// new
@import AspireApiControllers;
#import "ForgotPasswordView.h"

@interface SignInViewController () <DataLoadDelegate, UITextFieldDelegate>{
    
    BOOL isInputEmail, isInputPassword;
    WSB2CGetAccessToken* wsB2CGetAccessToek;
    WSB2CGetRequestToken* wsB2CGetRequestToken;
    WSB2CGetUserDetails* wsGetUser;
    WSPreferences *wsPreferences;
    WSB2CPasscodeVerfication *wsPasscodeVerification;
    AppDelegate* appdelegate;
}

@property (nonatomic, strong) WSSignIn *wsSignIn;

@end

@implementation SignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *tappedOutsideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tappedOutsideKeyboard];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldChanged:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:self.passwordTextField];
    
    [self setUIStyte];
    [self trackingScreenByName:@"Sign in"];
    
    if (self.shouldGotoForgotPasswordImmediately)
        [self ForgetPasswordAction:nil];
    
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //    self.emailTextField.text = @"";
    //    self.passwordTextField.text = @"";
    //    isInputEmail = isInputPassword = NO;
    
//    if (self.email != nil) {
//        [self.emailTextField setText:self.email];
//        //self.email = nil;
//    }
    
    if ([StoreUserDefaults email] != nil) {
        [self.emailTextField setText:[StoreUserDefaults email]];
        isInputEmail = TRUE;
        [StoreUserDefaults saveEmail:nil];
    }
    
    [self setStatusSignInButton];
    [self.navigationController setNavigationBarHidden:YES];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self setTextViewsDefaultBottomBolder];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setTextViewsDefaultBottomBolder {
    [self.emailTextField setBottomBolderDefaultColor];
    [self.passwordTextField setBottomBolderDefaultColor];
}

-(void)resignFirstResponderForAllTextField{
    if(self.emailTextField.isFirstResponder){
        [self.emailTextField resignFirstResponder];
    }
    else if(self.passwordTextField.isFirstResponder){
        [self.passwordTextField resignFirstResponder];
    }
    
}

-(void)dismissKeyboard
{
    [self resignFirstResponderForAllTextField];
}

-(void)verifyData{
    NSString *errorAll = @"All fields are required.";
    NSMutableString *message = [[NSMutableString alloc] init];
    if (self.emailTextField.text.length == 0 && self.passwordTextField.text.length == 0) {
        [message appendString:@"* "];
        [message appendString:errorAll];
        [self.emailTextField setBottomBorderRedColor];
        [self.passwordTextField setBottomBorderRedColor];
        
    }else{
        //        if(self.emailTextField.text.length == 0){
        //            [message appendString:@"* "];
        //            [message appendString:[NSString stringWithFormat:@"%@ \n",errorAll]];
        //            [self.emailTextField setBottomBorderRedColor];
        //        }
        //        if (self.passwordTextField.text.length == 0){
        //            [message appendString:@"* "];
        //            [message appendString:[NSString stringWithFormat:@"%@ \n",errorAll]];
        //            [self.passwordTextField setBottomBorderRedColor];
        //        }
        //        if([self verifyValueForTextField:self.emailTextField].length > 0) {
        //            [message appendString:@"* "];
        //            [message appendString:[self verifyValueForTextField:self.emailTextField]];
        //            [message appendString:@"\n"];
        //        }
        //        if([self verifyValueForTextField:self.passwordTextField].length > 0) {
        //            [message appendString:@"* "];
        //            [message appendString:[self verifyValueForTextField:self.passwordTextField]];
        //            [message appendString:@"\n"];
        //        }
        if (self.emailTextField.text.length == 0 || self.passwordTextField.text.length == 0 || [self verifyValueForTextField:self.emailTextField].length > 0 || [self verifyValueForTextField:self.passwordTextField].length > 0) {
            message = [NSMutableString stringWithString:@"Your User Name or Password is incorrect. Please enter again."];
        }
    }
    
    [self showError:message];
}

-(void)showError:(NSString *)message
{
    if(message.length > 0){
        
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = @"Please confirm that the value entered is correct:";
        alert.msgAlert = message;
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;
            alert.lblAlertMessage.textAlignment = NSTextAlignmentCenter;
            [alert.view layoutIfNeeded];
        });
        
        alert.blockFirstBtnAction = ^(void){
            [self makeBecomeFirstResponderForTextField];
        };
        
        [self showAlert:alert forNavigation:NO];
    }
    else{
        [self resignFirstResponderForAllTextField];
        [self setTextViewsDefaultBottomBolder];
        [self signInWithEmail:self.emailTextField.text withPassword:self.passwordTextField.text];
    }
}

-(void) makeBecomeFirstResponderForTextField
{
    [self resignFirstResponderForAllTextField];
    if(![self.emailTextField.text isValidEmail]){
        [self.emailTextField becomeFirstResponder];
    }
    else if(![self.passwordTextField.text isValidWeakPassword]){
        [self.passwordTextField becomeFirstResponder];
    }
}

-(NSString*)verifyValueForTextField:(UITextField *)textFied{
    
    NSString *errorMsg = @"";
    if(textFied == self.emailTextField && ![(textFied.isFirstResponder ? textFied.text :self.emailTextField.text) isValidEmail]){
        errorMsg = NSLocalizedString(@"input_invalid_email_msg", nil);
        [textFied setBottomBorderRedColor];
    }
    else if(textFied == self.passwordTextField && [(textFied.isFirstResponder ? textFied.text :self.passwordTextField.text) stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0){
        errorMsg = NSLocalizedString(@"input_invalid_password_msg", nil);
        [textFied setBottomBorderRedColor];
    }
    else{
        [textFied setBottomBolderDefaultColor];
    }
    
    return errorMsg;
}
- (void)WSBaseNetworkUnavailable{
    [self stopActivityIndicator];
}

- (void)signInWithEmail:(NSString*)email withPassword:(NSString*)password {
    
    [self startActivityIndicator];
    __weak typeof(self) _self = self;
    [ModelAspireApiManager loginWithUsername:email password:password completion:^(NSError *error) {
        
        if (!error) {
            [ModelAspireApiManager verifyBin:[[User current] getPasscode] completion:^(BOOL isPass, NSString *bin, id  _Nullable error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_self stopActivityIndicator];
                    if (error) {
                        switch ([AspireApiError getErrorTypeAPIVerifyBinFromError:error]) {
                            case BIN_ERR:
                                [_self onBinError];
                                break;
                            case NETWORK_UNAVAILABLE_ERR:
                                [_self showErrorNetwork];
                                break;
                            default:
                                NSLog(@"[RESULT] => VERIFY BIN ERROR: %@",@"OUT OF ENUM AspireApiErrorType");
                                [_self showCommonErrorWithOKAction:^{
                                    [_self okActionOnBinError];
                                }];
                                break;
                        }
                        return;
                    }
                    
                    if(isPass)
                    {
                        // set bin for this account
                        [ModelAspireApiManager registerServiceUsername:AspireApiUsername
                                                              password:AspireApiPassword
                                                               program:AspireApiProgam
                                                                   bin:bin];
                        
                        [[SessionData shareSessiondata] setBINNumber:bin];
                        [_self stopActivityIndicator];
                        [_self updateSuccessRedirect];
                    }
                    else
                    {
                        void(^gotoSignin)(void) = ^{
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            [ModelAspireApiManager logout];
                            //        [self navigationToUDASignInViewController];
                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                            SignInViewController *signinViewController = [storyboard instantiateViewControllerWithIdentifier:@"UDASignInViewController"];
                            UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:signinViewController];
                            AppDelegate *appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
                            appdelegate.window.rootViewController = navigationController;
                            [appdelegate.window makeKeyAndVisible];
                        };
                        
                        [_self showAlertWithTitle:NSLocalizedString(@"alert_error_title", nil)
                                         message:NSLocalizedString(@"check_invalid_passcode_msg", nil)
                                         buttons:@[NSLocalizedString(@"arlet_cancel_button", nil), NSLocalizedString(@"ok_button_title", nil)]
                                         actions:@[gotoSignin, ^{
                            PasscodeViewController *passcodeViewController = [[PasscodeViewController alloc] init];
                            passcodeViewController.needInputNewPasscode = true;
                            appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
                            appdelegate.window.rootViewController = passcodeViewController;
                            [appdelegate.window makeKeyAndVisible];
                        }]
                                messageAlignment:NSTextAlignmentCenter];
                        
                    }
                });
            }];
            
        }else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [_self stopActivityIndicator];
                NSString* message = @"";
                switch ([AspireApiError getErrorTypeAPISignInFromError:error]) {
                    case SIGNIN_INVALID_USER_ERR:
                        message = @"We're sorry. Your username or password do not match what we have in our system. Click OK to try again.";
                        break;
                    case OKTA_LOCKED_ERR:
                        message = @"We're sorry. Please contact us at 877-288-6503 for assistance accessing this account.";
                        break;
                    case UNKNOWN_ERR:
                        break;
                    default:
                        break;
                }
                
                switch ([AspireApiError getErrorTypeAPIGetProfileOKTAFromError:error]) {
                    case OKTA_PROFILE_EXIST_ERR:
                        message = @"We're sorry. Your username or password do not match what we have in our system. Click OK to try again.";
                        break;
                    case UNKNOWN_ERR:
                        break;
                    default:
                        break;
                }
                
                [_self handleAPIErrorWithCode:message];
            });
        }
    }];
    return;
}

- (void) okActionOnBinError {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [ModelAspireApiManager logout];
    PasscodeViewController *passcodeViewController = [[PasscodeViewController alloc] init];
    appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    appdelegate.window.rootViewController = passcodeViewController;
    [appdelegate.window makeKeyAndVisible];
}

- (void) onBinError {
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);
    alert.msgAlert = NSLocalizedString(@"check_invalid_passcode_msg", nil);
    alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
    SignInViewController __weak *weakSelf = self;
    alert.blockFirstBtnAction = ^{
        [weakSelf okActionOnBinError];
    };
    
    [self showAlert:alert forNavigation:NO];
}

- (void) handleAPIErrorWithCode:(NSString*)errorCode{
    [self.emailTextField setBottomBorderRedColor];
    [self.passwordTextField setBottomBorderRedColor];
    if (![errorCode isEqualToString:@""]) {
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);
        alert.msgAlert = errorCode;
        [self addOkAsFirstButtonToAlert:alert];
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;
        });
        
        [self showAlert:alert forNavigation:NO];
        [self.emailTextField setBottomBorderRedColor];
        [self.passwordTextField setBottomBorderRedColor];
    }else{
        [self showApiErrorWithMessage:@""];
    }
    
}

- (void) setUIStyte {
    
    isInputEmail = isInputPassword = NO;
    self.emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    self.passwordTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    self.passwordTextField.secureTextEntry = YES;
    self.emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    self.emailTextField.delegate = self;
    self.passwordTextField.delegate = self;
    
    [self createAsteriskForTextField:self.emailTextField];
    [self createAsteriskForTextField:self.passwordTextField];
    
    self.emailTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.emailTextField.text];
    self.passwordTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.passwordTextField.text];
    self.emailTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.emailTextField.placeholder];
    self.passwordTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.passwordTextField.placeholder];
    
    [self setStatusSignInButton];
    
    [self.backgroundSignInView setBackgroundColor:[AppColor backgroundColor]];
    
    [self.signInButton setBackgroundColorForNormalStatus];
    [self.signInButton setBackgroundColorForTouchingStatus];
    [self.signInButton configureDecorationForButton];
    
    [self.forgotButton configureDecorationForButton];
    [self.signUpButton configureDecorationForButton];
}

- (void)setStatusSignInButton{
    self.signInButton.enabled = (isInputEmail && isInputPassword);
}

-(void) createAsteriskForTextField:(UITextField *)textField
{
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 8, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [textField setLeftView:leftView];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    
}
- (void)getPreference{
    wsPreferences = [[WSPreferences alloc] init];
    wsPreferences.delegate = self;
    [wsPreferences getPreference];
}

#pragma mark TEXT FIELD DELEGATE
-(void)textFieldChanged:(NSNotification*)notification {
    isInputPassword = self.passwordTextField.text.length > 0;
    [self setStatusSignInButton];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *inputString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (textField == self.passwordTextField) {
        if ([string isEqualToString:@" "]) {
            return NO;
        }
        isInputPassword = (inputString.length > 0);
        if (textField.text.length >= 25 && range.length == 0){
            return NO;
        }
    }else if (textField == self.emailTextField){
        isInputEmail = inputString.length > 0;
    }
    [self setStatusSignInButton];
    
    return [self updateTextFiel:textField shouldChangeCharactersInRange:range replacementString:string];
}

-(BOOL)updateTextFiel:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(string.length > 1)
    {
        NSMutableString *newString = [[NSMutableString alloc] initWithString:[string removeRedudantWhiteSpaceInText]];
        NSString *resultText = [textField.text stringByReplacingCharactersInRange:range
                                                                       withString:string];
        resultText = [resultText removeRedudantWhiteSpaceInText];
        
        if(textField == self.emailTextField)
        {
            //textField.text = newString;
            textField.text = (resultText.length > 96)?[resultText substringWithRange:NSMakeRange(0, 96)] : resultText;
        }
        
        return NO;
    }
    
    else if(textField == self.emailTextField)
    {
        if([textField.text occurrenceCountOfCharacter:'@'] == 1 && [string isEqualToString:@"@"]){
            return NO;
        }
        if (textField.text.length == 96 && ![string isEqualToString:@""]){
            return NO;
        }
        
        return YES;
    }
    
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.text = [textField.text removeRedudantWhiteSpaceInText];
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == self.emailTextField)
    {
        self.emailTextField.text = textField.text;
    }
    else if(textField == self.passwordTextField)
    {
        self.passwordTextField.text = textField.text;
    }
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if(textField == self.emailTextField){
        textField.text = self.emailTextField.text;
    }
    else if(textField == self.passwordTextField){
        textField.text = self.passwordTextField.text;
    }
}


#pragma mark - Delegate from API

-(void)loadDataDoneFrom:(WSBase*)ws
{
    if (ws.task == WS_AUTHENTICATION_LOGIN) {
        if (ws.data) {
            [self setUserDefaultInfoWithDict:ws.data[0]];
        }
        //Request token
        wsB2CGetRequestToken = [[WSB2CGetRequestToken alloc] init];
        wsB2CGetRequestToken.delegate = self;
        wsB2CGetRequestToken.task = WS_GET_REQUEST_TOKEN;
        [wsB2CGetRequestToken getRequestToken];
    }
    else if(ws.task == WS_GET_REQUEST_TOKEN){
        wsB2CGetAccessToek = [[WSB2CGetAccessToken alloc] init];
        wsB2CGetAccessToek.delegate = self;
        [wsB2CGetAccessToek requestAccessToken:encodeURLFromString(wsB2CGetRequestToken.requestToken) member:[[SessionData shareSessiondata] OnlineMemberID]];
    }
    else if(ws.task == WS_GET_ACCESS_TOKEN){
        //TODO: get user details
        [[SessionData shareSessiondata] setIsUseLocation: NO];
        [self getPreference];
    }
    else if (ws.task == WS_GET_MY_PREFERENCE){
        
        NSDictionary *profileDetailDict = [[SessionData shareSessiondata] getUserInfo];
        NSString *passcodeText = [profileDetailDict valueForKey:keyPasscode];
        // Logout and invalid passcode => input valid passcode and need update it
        if(![passcodeText isEqualToString:[[SessionData shareSessiondata] BINNumber]])
        {
            [self updatePreferences];
        }
        else
        {
            wsGetUser = [[WSB2CGetUserDetails alloc] init];
            wsGetUser.delegate = self;
            [wsGetUser getUserDetails];
        }
    }
    else if(ws.task == WS_UPDATE_MY_PREFERENCE || ws.task == WS_ADD_MY_PREFERENCE)
    {
        wsGetUser = [[WSB2CGetUserDetails alloc] init];
        wsGetUser.delegate = self;
        [wsGetUser getUserDetails];
    }
    else if(ws.task == WS_B2C_GET_USER_DETAILS)
    {
        [self updateSuccessRedirect];
        [self stopActivityIndicator];
    }
    
}


- (void)updatePreferences{
    
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    NSMutableArray *arraySubDictValues = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *hotelDict = [[NSMutableDictionary alloc] init];
    [hotelDict checkAndSetValue:@"Hotel" forKey:@"Type"];
    
    PreferenceObject *hotelPref = [self getPreferenceByType:HotelPreferenceType];
    NSString *value = ([[SessionData shareSessiondata] isUseLocation])?@"YES":@"NO";
    
    if ([SessionData shareSessiondata].arrayPreferences.count > 0 && hotelPref.preferenceID.length > 0){
        [hotelDict checkAndSetValue:hotelPref.preferenceID forKey:@"MyPreferencesId"];
        [hotelDict checkAndSetValue:hotelPref.value forKey:@"Preferredstarrating"];
        [hotelDict checkAndSetValue:value forKey:@"SmokingPreference"];
        [hotelDict checkAndSetValue:[[SessionData shareSessiondata] BINNumber] forKey:@"RoomPreference"];
        [hotelDict checkAndSetValue:hotelPref.userName forKey:@"BedPreference"];
        [arraySubDictValues addObject:hotelDict];
        [dataDict checkAndSetValue:arraySubDictValues forKey:@"PreferenceDetails"];
        
        wsPreferences = [[WSPreferences alloc] init];
        wsPreferences.delegate = self;
        [wsPreferences updatePreference:dataDict];
    }else{
        [hotelDict checkAndSetValue:@"" forKey:@"Preferredstarrating"];
        [hotelDict checkAndSetValue:value forKey:@"SmokingPreference"];
        [hotelDict checkAndSetValue:[[SessionData shareSessiondata] BINNumber] forKey:@"RoomPreference"];
        [arraySubDictValues addObject:hotelDict];
        [dataDict checkAndSetValue:arraySubDictValues forKey:@"PreferenceDetails"];
        
        wsPreferences = [[WSPreferences alloc] init];
        wsPreferences.delegate = self;
        [wsPreferences addPreference:dataDict];
    }
}


- (void) updateSuccessRedirect{
    
    //    if ([[SessionData shareSessiondata] hasForgotAccount]) {
    //        ChangePasswordViewController *changePasswordVC = [[ChangePasswordViewController alloc] init];
    //        [self presentViewController:changePasswordVC animated:YES completion:nil];
    //    }else{
    appdelegate = appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    [UIView transitionWithView:appdelegate.window
                      duration:0.5
                       options:UIViewAnimationOptionPreferredFramesPerSecond60
                    animations:^{
                        
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        MenuViewController *menuViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
                        UINavigationController *menuNavigationViewController = [[UINavigationController alloc] initWithRootViewController:menuViewController];
                        UIViewController *fontViewController = [[HomeViewController alloc] init];
                        
                        SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:menuNavigationViewController frontViewController:fontViewController];
                        
                        revealController.delegate = appdelegate;
                        revealController.rearViewRevealWidth = SCREEN_WIDTH;
                        revealController.rearViewRevealOverdraw = 0.0f;
                        revealController.rearViewRevealDisplacement = 0.0f;
                        appdelegate.viewController = revealController;
                        appdelegate.window.rootViewController = appdelegate.viewController;
                        [appdelegate.window makeKeyWindow];
                        
                        UIViewController *newFrontController = [[HomeViewController alloc] init];
                        UINavigationController *newNavigationViewController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                        [revealController pushFrontViewController:newNavigationViewController animated:YES];
                    }
                    completion:nil];
    //    }
    [self trackingEventByName:@"Sign in" withAction:ClickActionType withCategory:AuthenticationCategoryType];
    
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [ModelAspireApiManager logout];
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }
}

-(void)loadDataFailFrom:(WSBase *)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message {
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }else{
        AlertViewController *alert = [[AlertViewController alloc] init];
        
        if ([message isEqualToString:@"ENR70-1"]) {
            alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);;
            [self.emailTextField setBottomBorderRedColor];
            [self.passwordTextField setBottomBorderRedColor];
            
            alert.msgAlert = @"Please confirm that the value entered is correct.";
            
        }else{
            alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);;
            alert.msgAlert = message;
        }
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        alert.blockFirstBtnAction = ^{
            [self makeBecomeFirstResponderForTextField];
        };
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;
        });
        
        [self showAlert:alert forNavigation:NO];
    }
}


- (void)setUserDefaultInfoWithDict:(UserRegistrationItem*)item{
    [[SessionData shareSessiondata] setOnlineMemberID:item.OnlineMemberID];
    [[SessionData shareSessiondata] setOnlineMemberDetailIDs:item.OnlineMemberDetailIDs];
}

#pragma mark - Action
- (IBAction)ForgetPasswordAction:(id)sender {
    self.emailTextField.text = @"";
    self.passwordTextField.text = @"";
    isInputEmail = isInputPassword = NO;
    [self.view endEditing:YES];
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    UDAForgotPasswordViewController *forgotPasswordViewController = [storyboard instantiateViewControllerWithIdentifier:@"UDAForgotPasswordViewController"];
    //    [self.navigationController pushViewController:forgotPasswordViewController animated:YES];
    
    ForgotPasswordView* v = [[ForgotPasswordView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    AACForgotPasswordController* vc = [AACForgotPasswordController new];
    vc.subview = v;
    v.controller = vc;
    [self.navigationController pushViewController:vc animated:true];
    
}

- (IBAction)SignInAction:(id)sender {
    [self.view endEditing:true];
    [self verifyData];
}

- (IBAction)SignUpAction:(id)sender {
    [self.view endEditing:true];
    self.emailTextField.text = @"";
    self.passwordTextField.text = @"";
    [self navigationToCreateProfileViewController];
}

-(void)navigationToCreateProfileViewController {
    
    
    PasscodeViewController *passcodeViewController = [[PasscodeViewController alloc] init];
    //    signInViewController.isBINInvalid = false;
    [self.navigationController pushViewController:passcodeViewController animated:true];
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    CreateProfileViewController *createProfileViewController = [storyboard instantiateViewControllerWithIdentifier:@"CreateProfileViewController"];
    //
    //    if ([AppData isCreatedProfile])  {
    //        SWRevealViewController *revealViewController = self.revealViewController;
    //
    //        [revealViewController pushFrontViewController:createProfileViewController animated:YES];
    //    } else
    //        [self.navigationController pushViewController:createProfileViewController animated:YES];
}

@end
