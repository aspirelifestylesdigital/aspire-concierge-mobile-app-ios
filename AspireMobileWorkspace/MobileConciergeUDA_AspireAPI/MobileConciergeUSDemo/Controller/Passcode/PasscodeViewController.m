//
//  PasscodeViewController.m
//  MobileConciergeUSDemo
//
//  Created by Chung Mai on 10/23/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "PasscodeViewController.h"
#import "UIButton+Extension.h"
#import "EnumConstant.h"
#import "WSBase.h"
#import "WSB2CPasscodeVerfication.h"
#import "SWRevealViewController.h"
#import "PasscodeItem.h"
#import "WSB2CGetUserDetails.h"
#import "AppDelegate.h"
#import "HomeViewController.h"
#import "MenuViewController.h"
#import "WSPreferences.h"
#import "PreferenceObject.h"
#import "CreateProfileViewController.h"
#import "AppData.h"
#import "ChangePasswordViewController.h"
#import "SignInViewController.h"
@import AspireApiFramework;
#import "NSDictionary+SBJSONHelper.h"
#import "CheckEmailView.h"
@import AspireApiControllers;

#define LIMITCHARACTERS     25

@interface PasscodeViewController ()<DataLoadDelegate, UITextFieldDelegate>
{
   CGFloat backupBottomConstraint;
    WSPreferences *wsPreferences;
    WSB2CGetUserDetails *wsGetUser;
    WSB2CPasscodeVerfication *wsPasscodeVerification;
}
@end

@implementation PasscodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    // Do any additional setup after loading the view from its nib.
    self.headerLbl.attributedText = [UtilStyle setTextStyleForTitleViewControllerWithMessage:NSLocalizedString(@"concierge", nil)];
    
    UITapGestureRecognizer *dismiss = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:dismiss];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWill:) name:UIKeyboardWillHideNotification object:nil];
    
    [self.submitBtn setTitle:NSLocalizedString(@"submit_button_title", nil) forState:UIControlStateNormal];
    
    [self.submitBtn setBackgroundColorForNormalStatus];
    [self.submitBtn setBackgroundColorForTouchingStatus];
    [self.submitBtn configureDecorationForButton];
    [self.submitBtn setEnabled:NO];
    
    [self.titleLbl setTextColor:[AppColor normalTextColor]];
    self.titleLbl.text = NSLocalizedString(@"passcode_title_msg", @"");
    
    self.passcodeTxtView.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.passcodeTxtView.text];

    self.passcodeTxtView.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:NSLocalizedString(@"passcode_holder_text_msg", @"")];
    [self.passcodeTxtView setDelegate:self];
    
    backupBottomConstraint = self.submitBtnBottomConstraint.constant;
    
    if (self.needInputNewPasscode) {
        [self.btnBack removeFromSuperview];
    }
}

-(void)handleKeyboardWillShow:(NSNotification *)paramNotification
{
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         self.submitBtnBottomConstraint.constant = keyboardRect.size.height + MARGIN_KEYBOARD;
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

-(void)handleKeyboardWill:(NSNotification *)paramNotification
{
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         self.submitBtnBottomConstraint.constant =  backupBottomConstraint;
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.passcodeTxtView) {

        NSString *resultText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        (resultText.length > 0) ? [self.submitBtn setEnabled:YES] :  [self.submitBtn setEnabled:NO];
        
        return !([textField.text length] >= LIMITCHARACTERS && [string length] > range.length);
    }
    return YES;
}

#pragma mark - Delegate from API
- (void)WSBaseNetworkUnavailable{
    [self stopActivityIndicator];
}

- (void)dismissKeyboard{
    if([self.passcodeTxtView becomeFirstResponder])
    {
        [self.passcodeTxtView resignFirstResponder];
    }
}

-(void)loadDataDoneFrom:(WSBase*)ws
{
    if(ws.task == WS_B2C_VERIFY_PASSCODE)
    {
        PasscodeItem *passcodeItem = (PasscodeItem *)ws.data[0];
        if(passcodeItem.valid)
        {
            [[SessionData shareSessiondata] setPasscode:self.passcodeTxtView.text];
            // if existing account, update its preference (killed app and invalid Passcode)
            __weak typeof(self) _self = self;
            if([User isValid])
            {
                UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
                NSMutableDictionary *dictDai = [[NSMutableDictionary alloc] init];
                [dictDai checkAndSetValue:userObject.salutation forKey:@"Salutation"];
                [dictDai checkAndSetValue:userObject.firstName forKey:@"FirstName"];
                [dictDai checkAndSetValue:userObject.lastName forKey:@"LastName"];
                [dictDai checkAndSetValue:[userObject.mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""] forKey:@"MobileNumber"];
                [dictDai checkAndSetValue:userObject.email forKey:@"Email"];
                [dictDai checkAndSetValue:self.passcodeTxtView.text forKey:APP_USER_PREFERENCE_Passcode];
                [dictDai checkAndSetValue:userObject.isUseLocation ? @"ON" : @"OFF" forKey:APP_USER_PREFERENCE_Location];
                if (userObject.currentCity.length > 0)
                    [dictDai checkAndSetValue:userObject.currentCity forKey:APP_USER_PREFERENCE_Selected_City];
                [dictDai checkAndSetValue:[self getPreferenceByType:DiningPreferenceType].value forKey:@"Dining Preference"];
                [dictDai checkAndSetValue:[self getPreferenceByType:HotelPreferenceType].value forKey:@"Hotel Preference"];
                [dictDai checkAndSetValue:[self getPreferenceByType:TransportationPreferenceType].value forKey:@"Car Type Preference"];
                [dictDai checkAndSetValue:[userObject.currentCity isEqualToString:@""] ? @"Unknown" : userObject.currentCity forKey:@"City"];
                [dictDai checkAndSetValue:@"USA" forKey:@"Country"];
                [dictDai checkAndSetValue:@"USA" forKey:@"homeCountry"];
                [dictDai checkAndSetValue:APP_NAME forKey:@"referenceName"];

                [ModelAspireApiManager updateProfileCurrentUserWithUserInfo:dictDai completion:nil];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_self stopActivityIndicator];
                    [_self updateSuccessRedirect];
                });
                
            }else
            {
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
                [[NSUserDefaults standardUserDefaults] synchronize];
//                [ModelAspireApiManager logout];
                //        [self navigationToUDASignInViewController];
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                SignInViewController *signinViewController = [storyboard instantiateViewControllerWithIdentifier:@"UDASignInViewController"];
                UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:signinViewController];
                AppDelegate *appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
                appdelegate.window.rootViewController = navigationController;
                [appdelegate.window makeKeyAndVisible];
                
            }
        }
        else
        {
            [self stopActivityIndicator];
            typeof(self) weakSelf = self;
            [self showAlertForRetryPasscodeWithFirstBlock:^{
                weakSelf.passcodeTxtView.text = @"";
                [weakSelf.passcodeTxtView becomeFirstResponder];
                [weakSelf.submitBtn setEnabled:NO];
            }withSecondBlock:nil];
        }
        
    }
//    else if(ws.task == WS_UPDATE_MY_PREFERENCE || ws.task == WS_ADD_MY_PREFERENCE)
//    {
//        wsGetUser = [[WSB2CGetUserDetails alloc] init];
//        wsGetUser.delegate = self;
//        [wsGetUser getUserDetails];
//    }
//    else if(ws.task == WS_B2C_GET_USER_DETAILS)
//    {
//        NSDictionary *profileDetailDict = [[SessionData shareSessiondata] getUserInfo];
//        if(!profileDetailDict)
//        {
//            profileDetailDict = [[NSMutableDictionary alloc] init];
//        }
//        [profileDetailDict setValue:self.passcodeTxtView.text forKey:keyPasscode];
//        [[SessionData shareSessiondata] setUserObjectWithDict:profileDetailDict];
//
//        [self updateSuccessRedirect];
//        [self stopActivityIndicator];
//    }
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}


-(IBAction)submitPasscode:(id)sender
{
    [self.passcodeTxtView endEditing:YES];
    [self startActivityIndicator];
    typeof(self) __weak _self = self;
    [ModelAspireApiManager verifyBin:self.passcodeTxtView.text completion:^(BOOL isPass, NSString *bin, id  _Nullable error) {
            if (error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_self stopActivityIndicator];
                    NSString* mess = NSLocalizedString(@"api_common_error_message", nil);
                    switch ([AspireApiError getErrorTypeAPIVerifyBinFromError:error]) {
                        case UNKNOWN_ERR:
                            // show general message for update profile
                            NSLog(@"[RESULT] => VERIFY BIN ERROR: %@",@"UNKNOWN_ERR");
                            break;
                        case BIN_ERR:
                            mess = NSLocalizedString(@"check_invalid_passcode_msg", nil);
                            break;
                        case NETWORK_UNAVAILABLE_ERR:
                            mess = @"";
                            [_self showErrorNetwork];
                            break;
                        default:
                            NSLog(@"[RESULT] => VERIFY BIN ERROR: %@",@"OUT OF ENUM AspireApiErrorType");
                            break;
                    }
                    if (mess.length == 0) return;
                    [_self showAlertInvalidBin];
                });
                return;
            }
            
            if(isPass)
            {
                // set bin for this account
                [ModelAspireApiManager registerServiceUsername:AspireApiUsername
                                                      password:AspireApiPassword
                                                       program:AspireApiProgam
                                                           bin:bin];
                
                [[SessionData shareSessiondata] setBINNumber:bin];
                [[SessionData shareSessiondata] updateUserObjectFromAPI];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([User isValid]) {
                        AppDelegate*appdelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
                        
                        [UIView transitionWithView:appdelegate.window
                                          duration:0.5
                                           options:UIViewAnimationOptionPreferredFramesPerSecond60
                                        animations:^{
                                            
                                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                            
                                            MenuViewController *menuViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
                                            UINavigationController *menuNavigationViewController = [[UINavigationController alloc] initWithRootViewController:menuViewController];
                                            UIViewController *fontViewController = [[HomeViewController alloc] init];
                                            
                                            SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:menuNavigationViewController frontViewController:fontViewController];
                                            
                                            revealController.delegate = appdelegate;
                                            revealController.rearViewRevealWidth = SCREEN_WIDTH;
                                            revealController.rearViewRevealOverdraw = 0.0f;
                                            revealController.rearViewRevealDisplacement = 0.0f;
                                            appdelegate.viewController = revealController;
                                            appdelegate.window.rootViewController = appdelegate.viewController;
                                            [appdelegate.window makeKeyWindow];
                                            
                                            UIViewController *newFrontController = [[HomeViewController alloc] init];
                                            UINavigationController *newNavigationViewController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                                            [revealController pushFrontViewController:newNavigationViewController animated:YES];
                                            
                                        }
                                        completion:nil];
                    } else {
                        
                        [_self stopActivityIndicator];
                        CheckEmailView* v = [[CheckEmailView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
                        AACCheckEmailController* vc = [AACCheckEmailController new];
                        vc.subview = v;
                        v.controller = vc;
                        [_self.navigationController pushViewController:vc animated:YES];
                        
                    }
                });
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (!_self) return;
                    [_self showAlertInvalidBin];
                });
            }
    }];
}

- (void) showAlertInvalidBin {
    void(^gotoSignin)(void) = ^{
        [self stopActivityIndicator];
        if (self.navigationController ) {
            [self.navigationController popViewControllerAnimated:true];
            return;
        }
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [ModelAspireApiManager logout];
        //        [self navigationToUDASignInViewController];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SignInViewController *signinViewController = [storyboard instantiateViewControllerWithIdentifier:@"UDASignInViewController"];
        UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:signinViewController];
        AppDelegate *appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
        appdelegate.window.rootViewController = navigationController;
        [appdelegate.window makeKeyAndVisible];
    };
    
    [self stopActivityIndicator];
    [self showAlertWithTitle:NSLocalizedString(@"alert_error_title", nil)
                      message:NSLocalizedString(@"check_invalid_passcode_msg", nil)
                      buttons:@[NSLocalizedString(@"arlet_cancel_button", nil), NSLocalizedString(@"ok_button_title", nil)]
                      actions:@[gotoSignin, ^{
        self.passcodeTxtView.text = @"";
        [self.passcodeTxtView becomeFirstResponder];
        [self.submitBtn setEnabled:NO];
    }]
             messageAlignment:NSTextAlignmentCenter];
}

- (void)updatePreferences{
    
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    NSMutableArray *arraySubDictValues = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *hotelDict = [[NSMutableDictionary alloc] init];
    [hotelDict setValue:@"Hotel" forKey:@"Type"];
    
    PreferenceObject *hotelPref = [self getPreferenceByType:HotelPreferenceType];
    NSString *value = ([[SessionData shareSessiondata] isUseLocation])?@"YES":@"NO";

    if ([SessionData shareSessiondata].arrayPreferences.count > 0 && hotelPref.preferenceID.length > 0){
        [hotelDict setValue:hotelPref.preferenceID forKey:@"MyPreferencesId"];
        [hotelDict setValue:hotelPref.value forKey:@"Preferredstarrating"];
        [hotelDict setValue:value forKey:@"SmokingPreference"];
        [hotelDict setValue:self.passcodeTxtView.text forKey:@"RoomPreference"];
        [hotelDict setValue:hotelPref.userName forKey:@"BedPreference"];
        [arraySubDictValues addObject:hotelDict];
        [dataDict setValue:arraySubDictValues forKey:@"PreferenceDetails"];
        
        wsPreferences = [[WSPreferences alloc] init];
        wsPreferences.delegate = self;
        [wsPreferences updatePreference:dataDict];
    }else{
        [hotelDict setValue:@"" forKey:@"Preferredstarrating"];
        [hotelDict setValue:value forKey:@"SmokingPreference"];
        [hotelDict setValue:self.passcodeTxtView.text forKey:@"RoomPreference"];
        [arraySubDictValues addObject:hotelDict];
        [dataDict setValue:arraySubDictValues forKey:@"PreferenceDetails"];
        
        wsPreferences = [[WSPreferences alloc] init];
        wsPreferences.delegate = self;
        [wsPreferences addPreference:dataDict];
    }
}


- (void) updateSuccessRedirect{
//    if ([[SessionData shareSessiondata] hasForgotAccount]) {
//        ChangePasswordViewController *changePasswordVC = [[ChangePasswordViewController alloc] init];
//        [self presentViewController:changePasswordVC animated:YES completion:nil];
//    }else{
        AppDelegate *appdelegate =(AppDelegate*) [[UIApplication sharedApplication] delegate];
        
        [UIView transitionWithView:appdelegate.window
                          duration:0.5
                           options:UIViewAnimationOptionPreferredFramesPerSecond60
                        animations:^{
                            
                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                            MenuViewController *menuViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
                            
                            UINavigationController *menuNavigationViewController = [[UINavigationController alloc] initWithRootViewController:menuViewController];
                            UIViewController *fontViewController = [[HomeViewController alloc] init];
                            
                            SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:menuNavigationViewController frontViewController:fontViewController];
                            
                            revealController.delegate = appdelegate;
                            revealController.rearViewRevealWidth = SCREEN_WIDTH;
                            revealController.rearViewRevealOverdraw = 0.0f;
                            revealController.rearViewRevealDisplacement = 0.0f;
                            appdelegate.viewController = revealController;
                            appdelegate.window.rootViewController = appdelegate.viewController;
                            [appdelegate.window makeKeyWindow];
                            
                            UIViewController *newFrontController = [[HomeViewController alloc] init];
                            UINavigationController *newNavigationViewController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                            [revealController pushFrontViewController:newNavigationViewController animated:YES];
                        }
                        completion:nil];
//    }
    
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [ModelAspireApiManager logout];
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }
    else
    {
        typeof(self) weakSelf = self;
        [self showAlertForRetryPasscodeWithFirstBlock:^{
            weakSelf.passcodeTxtView.text = @"";
            [weakSelf.passcodeTxtView becomeFirstResponder];
            [weakSelf.submitBtn setEnabled:NO];
        }withSecondBlock:nil];
    }
}

-(void)loadDataFailFrom:(WSBase *)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message {
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }else{
        typeof(self) weakSelf = self;
        [self showAlertForRetryPasscodeWithFirstBlock:^{
            weakSelf.passcodeTxtView.text = @"";
            [weakSelf.passcodeTxtView becomeFirstResponder];
            [weakSelf.submitBtn setEnabled:NO];
        }withSecondBlock:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)setValidPasscode:(id)sender
{
    [[AppData getInstance] setValidPasscode:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
