//
//  PolicyViewController.h
//  MobileConcierge
//
//  Created by Chung Mai on 6/15/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "PrivacyPolicyViewController.h"
#import "EnableSubmitBINButton.h"

@interface PolicyViewController : PrivacyPolicyViewController

@property (weak, nonatomic) id<EnableSubmitBINButton> delegate;

@end
