//
//  MCCommonUILabel.m
//  MobileConcierge
//
//  Created by 😱 on 7/4/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "StyleConstant.h"
#import "MCNoteUILabel.h"

@implementation MCNoteUILabel

- (instancetype) init {
    if (self = [super init]) {
        self.labelType = MCLabelTypeNote;
    }
    return self;
}

- (void) awakeFromNib {
    [super awakeFromNib];
    
    self.labelType = MCLabelTypeNote;
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.labelType = MCLabelTypeNote;
    }
    return self;
}

- (id) initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.labelType = MCLabelTypeNote;
    }
    return self;
}

@end
