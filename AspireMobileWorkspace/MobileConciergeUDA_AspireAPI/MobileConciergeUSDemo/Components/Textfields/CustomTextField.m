//
//  CustomTextField.m
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 9/6/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "CustomTextField.h"
#import "UITextField+Extensions.h"
@import AspireApiControllers;

@implementation CustomTextField

- (void)layoutSubviews {
    [super layoutSubviews];
    [self refreshBorder];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender{
    return NO;
}

- (void) updateAsPhoneNumberWhileChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    UITextRange *selectedRange = [self selectedTextRange];
    NSInteger oldLength = [self.text length];
    NSString* newString = [self.text stringByReplacingCharactersInRange:range withString:string];
    NSString* removedAddChar = [newString stringByReplacingOccurrencesOfString:@"+" withString:@""];
    NSString* readdedAddChar = @"";
    if ([AACStringUtils isEmptyWithString:removedAddChar] == false) {
        readdedAddChar = [NSString stringWithFormat:@"+%@", removedAddChar];
    }
    self.text = readdedAddChar;
    
    NSInteger offset = [self.text length] - oldLength;
    NSInteger length = [self offsetFromPosition:selectedRange.start toPosition:selectedRange.end];
    if (offset < 0 && length > 0) {
        offset = 0;
    }
    UITextPosition *newPosition = [self positionFromPosition:selectedRange.start offset:offset];
    UITextRange *newRange = [self textRangeFromPosition:newPosition toPosition:newPosition];
    [self setSelectedTextRange:newRange];
}

@end
