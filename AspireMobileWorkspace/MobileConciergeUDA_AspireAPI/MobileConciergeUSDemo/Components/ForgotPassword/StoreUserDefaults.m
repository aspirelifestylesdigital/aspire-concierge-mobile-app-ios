//
//  StoreUserDefaults.m
//  MobileConciergeUSDemo
//
//  Created by HieuNguyen on 10/31/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "StoreUserDefaults.h"

@implementation StoreUserDefaults

static NSString * emailKey = @"emailKey";

+ (void) saveEmail :(id ) email {
    if (email == nil) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:emailKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }else  {
        [[NSUserDefaults standardUserDefaults] setObject:email forKey:emailKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}
+ (NSString*) email {
    return  [[NSUserDefaults standardUserDefaults] objectForKey:emailKey];
}
@end
