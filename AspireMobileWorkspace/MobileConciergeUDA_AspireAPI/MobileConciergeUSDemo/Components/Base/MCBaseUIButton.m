//
//  MCBaseUIButton.m
//  MobileConcierge
//
//  Created by 😱 on 7/4/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "MCBaseUIButton.h"

@implementation MCBaseUIButton

#pragma mark - PROPERTIES

#define BUTTON_BG_COLOR_NORMAL_SMALL                @"#FF4000"
#define BUTTON_BG_COLOR_HIGHLIGHT_SMALL             @"#FF8C56"

#define BUTTON_TEXT_COLOR_NORMAL_LARGER              @"#FFFFFF"
#define BUTTON_TEXT_COLOR_HIGHLIGHT_LARGER           @"#FFFFFF"
#define BUTTON_BG_COLOR_NORMAL_LARGER                @"#FF4000"
#define BUTTON_BG_COLOR_HIGHLIGHT_LARGER             @"#FF8C56"

#define FONT_MarkForMC_MED           FONT_MarkForMC_MED
#define BUTTON_FONT_NAME_SMALL              FONT_MarkForMC_MED
#define BUTTON_FONT_NAME_LARGER             FONT_MarkForMC_REGULAR

#define BUTTON_FONT_SIZE_STANDARD           18
#define BUTTON_FONT_SIZE_SMALL              14
#define BUTTON_FONT_SIZE_LARGER             24

// HOME CONTROLLER
#define BUTTON_HOME_FONT_NAME               FONT_MarkForMC_BOLD
#define BUTTON_HOME_FONT_SIZE               16
#define BUTTON_HOME_TEXT_COLOR_NORMAL       @"#272726"
#define BUTTON_HOME_TEXT_COLOR_HIGHLIGHT    @"#FF4000"

- (void) setBtnType:(MCButtonType)btnType{
    
    NSString*bgColorNormal = nil;
    NSString*bgColorHighlight = nil;
    
    NSString*textColorNormal = nil;
    NSString*textColorHighlight = nil;
    
    NSString* fontName = nil;
    CGFloat fontSize = 0;
    
    switch (btnType) {
        case MCButtonTypeLarge:
            
            bgColorNormal = BUTTON_BG_COLOR_NORMAL_LARGER;
            bgColorHighlight = BUTTON_BG_COLOR_HIGHLIGHT_LARGER;
            textColorNormal = BUTTON_TEXT_COLOR_NORMAL_LARGER;
            textColorHighlight = BUTTON_TEXT_COLOR_HIGHLIGHT_LARGER;
            fontName = BUTTON_FONT_NAME_LARGER;
            fontSize = BUTTON_FONT_SIZE_LARGER;
            
            break;
        case MCButtonTypeSmall:
            
            bgColorNormal = BUTTON_BG_COLOR_NORMAL_SMALL;
            bgColorHighlight = BUTTON_BG_COLOR_HIGHLIGHT_SMALL;
            textColorNormal = @"#FFFFFF";
            textColorHighlight = @"#FFFFFF";
            fontName = BUTTON_FONT_NAME_SMALL;
            fontSize = BUTTON_FONT_SIZE_SMALL;
            
            break;
        case MCButtonTypeStandard:
            
            bgColorNormal = @"#FF4000";
            bgColorHighlight = @"#FF8C56";
            textColorNormal = @"#FFFFFF";
            textColorHighlight = @"#FFFFFF";
            fontName = FONT_MarkForMC_MED;
            fontSize = BUTTON_FONT_SIZE_STANDARD;
            
            break;
    }
    
    [self setCustomFontName:fontName withSize:fontSize];
    [self setTextFontHexColor:textColorNormal orColor:nil withState:UIControlStateNormal];
    [self setTextFontHexColor:textColorHighlight orColor:nil withState:UIControlStateHighlighted];
    [self setBackgroundFromHexColor:bgColorNormal orColor:nil withState:UIControlStateNormal];
    [self setBackgroundFromHexColor:bgColorHighlight orColor:nil withState:UIControlStateHighlighted];
}

#pragma mark - PRIVATE
- (void) setCustomFontName:(NSString*)fontName withSize:(CGFloat)fontSize {
    [self.titleLabel setFont:[UIFont fontWithName:fontName size:fontSize]];
}

- (void) setTextFontHexColor:(NSString*)hexColor orColor:(UIColor*)color withState:(UIControlState)state{
    if(color) {
        [self setTitleColor:color forState:state];
    } else {
        if (hexColor.length > 6) {
            [self setTitleColor:colorFromHexString(hexColor) forState:state];
        }
    }
}

- (void) setBackgroundFromHexColor:(NSString*)hexColor orColor:(UIColor*) color withState:(UIControlState)state {
    @autoreleasepool {
        
        UIColor* colorRef = nil;
        if(color) {
            colorRef = color;
        } else {
            if (hexColor.length > 6) {
                colorRef = colorFromHexString(hexColor);
            }
        }
        UIGraphicsBeginImageContext(CGSizeMake(1.0f, 1.0f));
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetFillColorWithColor(context, colorRef.CGColor);
        CGContextFillRect(context, CGRectMake(0, 0, 1, 1));
        UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [self setBackgroundImage:colorImage forState:state];
    }
}

@end
