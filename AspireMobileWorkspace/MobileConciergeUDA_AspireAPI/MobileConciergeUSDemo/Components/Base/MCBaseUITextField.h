//
//  MCBaseUILabel.h
//  MobileConcierge
//
//  Created by 😱 on 7/4/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StyleConstant.h"

@interface MCBaseUITextField : UITextField
@property (assign,nonatomic) MCTextfieldType    TextfieldType;
@end
