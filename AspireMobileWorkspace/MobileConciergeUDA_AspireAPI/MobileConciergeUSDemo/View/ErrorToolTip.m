//
//  ErrorToolTip.m
//  MobileConcierge
//
//  Created by user on 5/18/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "ErrorToolTip.h"
#import "Common.h"
#import "Constant.h"
@implementation ErrorToolTip

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self.view = [[[NSBundle mainBundle] loadNibNamed:@"ErrorToolTip" owner:self options:nil]
                 objectAtIndex:0];
    [self.view setFrame:frame];
    [self addSubview:self.view];
    if (self) {
        // Initialization code
        [self setFrame:frame];
        [self.lbErrorMsg setText:_errorMessage];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSString *className = NSStringFromClass([self class]);
        self.view = [[[NSBundle mainBundle] loadNibNamed:className owner:self options:nil] firstObject];
        [self addSubview:self.view];
        [self.lbErrorMsg setText:_errorMessage];
        return self;
    }
    
    return self;
}

- (IBAction)errorIconClicked:(id)sender {
    if(_delegate!=nil){
        [_delegate clickedOnErrorIcon:self.tag];
    }
    
    [self toggleToolTips:!_btError.isSelected];
}

- (void) toggleToolTips:(Boolean) isShow{
    if(isShow){
        CGSize newSize = getSizeWithFontAndFrame(_errorMessage, [_lbErrorMsg font], SCREEN_WIDTH - 100 * SCREEN_SCALE, 999);
        
        if(newSize.height > 23*SCREEN_SCALE){
            _lbErrorMsg.numberOfLines = 0;
        } else {
            _lbErrorMsg.numberOfLines = 1;
        }
    }
    
    [self.lbErrorMsg setText:_errorMessage];
    [_btError setSelected:isShow];
    _tooltipView.hidden = !_btError.isSelected;
    
}

- (void) setErrorHidden:(BOOL)hidden{
    [self setHidden:hidden];
    _tooltipView.hidden = YES;
}

-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    for (UIView *subView in self.view.subviews) {
        if (!subView.hidden && subView.alpha > 0 && subView.userInteractionEnabled && [subView pointInside:[self convertPoint:point toView:subView] withEvent:event] && [subView isKindOfClass:[UIButton class]])
            return YES;
    }
    return NO;
}


@end
