//
//  ExploreQuestionTableViewCell.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/31/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "ExploreQuestionTableViewCell.h"
#import "Common.h"
#import "UtilStyle.h"

@implementation ExploreQuestionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.nameLabel.textColor = [AppColor normalButtonColor];
    self.addressLabel.textColor = [AppColor linkTextColor];
    self.descriptionLabel.textColor = [AppColor textColor];
    [self.nameLabel setFont:[UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize descriptionTextSize]]];
    [self.addressLabel setFont:[UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize descriptionTextSize]]];
    [self.descriptionLabel setFont:[UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize descriptionTextSize]]];
    [self.contentView setBackgroundColor:[UIColor whiteColor]];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    UIView *view = [self viewWithTag:1000];
    if(!view)
    {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - 1, self.bounds.size.width, 1.0f)];
        [view setBackgroundColor:[AppColor separatingLineColor]];
        view.tag = 1000;
        [self addSubview:view];
    }
    else
    {
        view.frame = CGRectMake(0, self.bounds.size.height - 1, self.bounds.size.width, 1.0f);
    }
    if(self.delegate) {
        if(self.tag == [self.delegate BaseTableViewCellWhatCellSelected:self]) {
            [self.contentView setBackgroundColor:[AppColor selectedViewColor]];
            [view setBackgroundColor:[AppColor separatingLineColor]];
        }
    }
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    UIView *view = [self viewWithTag:1000];
    if(view)
    {
        [view setBackgroundColor:[AppColor separatingLineColor]];
    }
    if(highlighted) {
        [self.contentView setBackgroundColor:[AppColor selectedViewColor]];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    // Configure the view for the selected state
    [super setSelected:selected animated:animated];
    if(selected)
    {
        UIView *view = [self viewWithTag:1000];
        if(view)
        {
            [view setBackgroundColor:[AppColor separatingLineColor]];
        }
        [self.contentView setBackgroundColor:[AppColor selectedViewColor]];
    }
}
- (void)prepareForReuse {
    [super prepareForReuse];
    [self.contentView setBackgroundColor:[UIColor whiteColor]];
}
@end
