//
//  AlertViewController.h
//  MobileConcierge
//
//  Created by Mac OS on 5/31/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AlertViewController;
@protocol AlertViewControllerDelegate <NSObject>

- (void) alertOkAction;
- (void) alertCancelAction;

@end

@interface AlertViewController : UIViewController

@property (nonatomic, strong) id<AlertViewControllerDelegate> alertViewControllerDelegate;
@property (nonatomic, strong) NSString* titleAlert;
@property (nonatomic, strong) NSString* msgAlert;
@property (nonatomic, strong) NSString* firstBtnTitle;
@property (nonatomic, strong) NSString* secondBtnTitle;
@property (nonatomic, strong) void (^blockFirstBtnAction)(void);
@property (nonatomic, strong) void (^blockSecondBtnAction)(void);
@property (weak, nonatomic) IBOutlet UIButton *firstBtn;
@property (weak, nonatomic) IBOutlet UIButton *seconBtn;
@property (weak, nonatomic) IBOutlet UIView *midView;
@property (weak, nonatomic) IBOutlet UIView *viewAlert;
@property (weak, nonatomic) IBOutlet UILabel *lblAlertTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblAlertMessage;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthConstraintMidView;

@end
