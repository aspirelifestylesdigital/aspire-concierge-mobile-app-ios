//
//  TableHeaderView.h
//  MobileConcierge
//
//  Created by Chung Mai on 6/21/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableHeaderView : UIView

@property (nonatomic, weak) IBOutlet UILabel *messageLbl;
@property (nonatomic, weak) IBOutlet UIButton *askConciergeBtn;

-(void)setupViewWithMessage:(NSString*) message;

@end
