//
//  BaseTableViewCell.m
//  MobileConcierge
//
//  Created by Home on 5/9/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "BaseTableViewCell.h"


@interface BaseTableViewCell(){
    BOOL didUpdateLayout;
}

@end

@implementation BaseTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    didUpdateLayout = NO;
    if(!didUpdateLayout){
        didUpdateLayout = YES;
        resetScaleViewBaseOnScreen(self);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)loadData{
    
}
@end
