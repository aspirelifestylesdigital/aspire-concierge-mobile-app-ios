//
//  SearchHeaderView.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/11/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchHeaderView : UIView

@property(nonatomic, weak) IBOutlet UIView      *searchTextView;
@property(nonatomic, weak) IBOutlet UITextField *searchText;
@property(nonatomic, weak) IBOutlet UIButton    *offerClear;
@property(nonatomic, weak) IBOutlet UIButton    *bookOnlineClear;
@property(nonatomic, weak) IBOutlet UILabel     *offerText;
@property(nonatomic, weak) IBOutlet UILabel     *bookOnlineText;
@property(nonatomic, assign) BOOL               isSearchOffer;
@property(nonatomic, assign) BOOL               isSearchBookOnline;

-(void)setupView;
-(CGFloat)calculateHeightView;
-(void)setBottomLine;
@end
