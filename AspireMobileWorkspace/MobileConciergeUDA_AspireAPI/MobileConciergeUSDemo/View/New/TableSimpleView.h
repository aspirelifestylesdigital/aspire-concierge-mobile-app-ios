//
//  TableSimpleView.h
//  TestIOS
//
//  Created by Dai Pham on 7/17/17.
//  Copyright © 2017 Dai Pham. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TableSimpleView;

@protocol TableSimpleViewProtocol <NSObject>

@optional
- (void) TableSimpleView:(TableSimpleView*)view onSelectItem:(id)item;

@end

@interface TableSimpleView : UIView

@property (weak,nonatomic) id<TableSimpleViewProtocol> delegate;

@property (weak, nonatomic) IBOutlet UITableView *tblList;

- (void) reloadWithData:(NSArray*)data;
@end
