//
//  ExploreListView.h
//  Test
//
//  Created by 😱 on 7/14/17.
//  Copyright © 2017 dai.pham.s3corp.com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ExploreListView;

@protocol ExploreListViewProtocol <NSObject>

@optional
- (void) ExploreListView:(ExploreListView*)view onSelectItem:(id)item;
- (void) ExploreListViewOnNavigateToViewSearch:(ExploreListView *)view;
- (void) ExploreListView:(ExploreListView *)view onSelectWithButtonIndex:(NSInteger)index;

@end

@interface ExploreListView : UIView

#pragma mark - 🚸 BLOCK VIEW 🚸

@property (weak, nonatomic) IBOutlet UIStackView *stackView;
@property (weak, nonatomic) IBOutlet UITableView *tblList;

#pragma mark - 🚸 PROPERTIES 🚸
@property (weak,nonatomic) id <ExploreListViewProtocol> delegate;

@end
