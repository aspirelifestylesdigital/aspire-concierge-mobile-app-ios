//
//  ErrorToolTip.h
//  MobileConcierge
//
//  Created by user on 5/18/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ErrorToolTipDelegate ;

@interface ErrorToolTip : UIView
@property (nonatomic) IBInspectable NSString* errorMessage;
@property (unsafe_unretained) id<ErrorToolTipDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIView *view;
@property (strong, nonatomic) IBOutlet UIButton *btError;
@property (strong, nonatomic) IBOutlet UIStackView *tooltipView;
@property (strong, nonatomic) IBOutlet UILabel *lbErrorMsg;
- (void) toggleToolTips:(Boolean) isShow;
- (void) setErrorHidden:(BOOL)hidden;
@end

@protocol ErrorToolTipDelegate <NSObject>

@optional
- (void) clickedOnErrorIcon:(NSInteger) tag;

@end
