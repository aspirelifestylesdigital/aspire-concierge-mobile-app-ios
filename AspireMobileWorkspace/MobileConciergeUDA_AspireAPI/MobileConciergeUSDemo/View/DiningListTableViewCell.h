//
//  DiningListTableViewCell.h
//  MobileConcierge
//
//  Created by Mac OS on 5/30/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
@interface DiningListTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblCity;
@property (weak, nonatomic) IBOutlet UILabel *lblNote;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

- (void) loadDataWithObject:(id)object;

@end
