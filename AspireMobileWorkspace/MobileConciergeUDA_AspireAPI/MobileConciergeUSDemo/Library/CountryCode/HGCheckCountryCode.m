//
//  HGCheckCountryCode.m
//  HungryGoWhere
//
//  Created by Hai NguyenV on 5/9/14.
//  Copyright (c) 2014 Linh Le. All rights reserved.
//

#import "HGCheckCountryCode.h"

@implementation HGCheckCountryCode


- (id)init {
    self = [super init];
    if (self != nil) {
        NSString * plistPath = [[NSBundle mainBundle] pathForResource:@"CountryCode" ofType:@"plist"];
        self.diallingCodesDictionary = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    }
    return self;
}
-(BOOL)checkCountryCode:(NSString*)code{
    if([self.diallingCodesDictionary objectForKey:code]!=nil){
        return YES;
    }
    return NO;
}
@end
