//
//  LocationComponent.h
//  Test
//
//  Created by Dai Pham on 2/24/17.
//  Copyright © 2017 Dai Pham. All rights reserved.
//

#import <Foundation/Foundation.h>

#define LOCATION_COMPONENT_ISAUTHORIZED_NOTIFICATION @"LocationComponent.isAuthorized"
#define LOCATION_COMPONENT_ISAUTHORIZED_NOTIFICATION_FOR_EXPLOREVIEW @"LocationComponent.isAuthorized.exploreView"

@interface LocationComponent : NSObject {
    NSString* getLocation;
    NSMutableArray* listWaitingGetDistances;
    NSMutableArray* againGetDistances;
}

/**
 return current current address
 */
@property (copy) void (^onReturnLocation)(NSString*);

/**
 internal checking get loccation done to perform another action
 */
@property (copy) void (^onGetLocationDone)();

/**
 This block involked after all items got distance. Place this block to view needed handle item's distance
 */
@property (copy) void (^onChecking)();

+ (LocationComponent*) getLocation;


/**
 This Method using init service location for app. Should call at first launch app
 */
+ (void) startRequestLocation;

/**
 Get distance from object address

 @param object object need calculator distance
 @param address from object to current location
 @param onDone block involke after get distance done
 */
+ (void) getDistanceForObject:(id)object fromAddress:(NSString*) address onDone:(void (^)(id obj,double distance, double lng, double lati))onDone;

/**
 Number item need checck before involke block checking

 @param numberItems number of items
 */
+ (void) setNumbersItemCheck:(NSInteger) numberItems;

/**
 Check Location getting distance for items

 @return True Or False
 */
+ (BOOL) isGettingDistance;
@end
