//
//  DiningDetailContentDescriptionTableViewCell.m
//  MobileConcierge
//
//  Created by user on 6/8/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "DiningDetailContentDescriptionTableViewCell.h"
#import "UILabel+Extension.h"
#import "NSString+HTML.h"

@interface DiningDetailContentDescriptionTableViewCell(){
    
    IBOutletCollection(UILabel) NSArray *listLabelPins;
}

@end

@implementation DiningDetailContentDescriptionTableViewCell


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.leadingConstraint.constant = self.leadingConstraint.constant*SCREEN_SCALE;
    
    self.tvContent.delegate  = self;
    
    for (UILabel* label in listLabelPins) {
        label.attributedText = [[NSAttributedString alloc] initWithString:label.text attributes:@{NSKernAttributeName:@1.0}];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)loadData{
    if (![self.tvContent hasText]) {
        
        self.cellDescription = [self.cellDescription removeRedudantNewLineInText];
        NSRange range = [self.cellDescription rangeOfString:@"Click here"];
        if (range.location != NSNotFound) {
            self.cellDescription = [self.cellDescription substringToIndex:range.location];
            self.cellDescription = [NSString stringWithFormat:@"%@ </p>",self.cellDescription];
        }
        range = [self.cellDescription rangeOfString:@"Contact us"];
        if (range.location != NSNotFound) {
            self.cellDescription = [self.cellDescription substringToIndex:range.location];
            self.cellDescription = [NSString stringWithFormat:@"%@ </p>",self.cellDescription];
        }
        while ([self.cellDescription containsString:@"<img"])
        {
            NSRange range2 = [self.cellDescription rangeOfString:@"<img"];
            if(range2.location != NSNotFound)
            {
                // get image link from Description
                NSString *imgString = [self.cellDescription substringFromIndex:range2.location];
                
                NSString* result = [imgString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                if ([result containsString:@"src"] && [result containsString:@"/>"]){
                    result = [result substringToIndex:[result rangeOfString:@"/>"].location + 2];
                }
                
                /// trim image tag from Description
                self.cellDescription = [self.cellDescription stringByReplacingOccurrencesOfString:result withString:@""];
                self.cellDescription = [[self flattenHtml:self.cellDescription] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                self.cellDescription = [self.cellDescription stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
            }
        }
        self.cellDescription = [self.cellDescription removeRedudantNewLineInText];
        self.cellDescription = [self.cellDescription stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; color:#011627;font-size:%fpx;line-height: 22px;}</style>",FONT_MarkForMC_LIGHT, 18.0f*SCREEN_SCALE]];
        NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                initWithData: [self.cellDescription dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil
                                                ];
        self.tvContent.attributedText = attributedString;
    }
}


- (NSString*) getImageTagString:(NSString*)tagString andRange:(NSRange)range{
    if(range.location != NSNotFound)
    {
        return [[tagString substringToIndex:NSMaxRange(range)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    }
    return @"";
}

- (NSString *)flattenHtml: (NSString *) html {
    NSScanner *theScanner;
    NSString *text = nil;
    theScanner = [NSScanner scannerWithString: html];
    while ([theScanner isAtEnd] == NO) {
        
        [theScanner scanUpToString: @"<" intoString: NULL];
        
        [theScanner scanUpToString: @">" intoString: &text];
        
        // Only Replace if you are outside of an html tag
    } // while
    return html;
}
- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange{
    [self.diningDetailContentDescriptionTableViewCellDelegate openDescriptionURL:URL];
    return NO;
}
@end
