//
//  VenueViewController.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/11/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VenueViewController : UIViewController

@property(nonatomic, weak) IBOutlet UITableView *tableView;
@end
