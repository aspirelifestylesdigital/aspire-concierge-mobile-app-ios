//
//  DiningDetailContentPriceTableViewCell.m
//  MobileConcierge
//
//  Created by user on 6/8/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "DiningDetailContentPriceTableViewCell.h"

@interface DiningDetailContentPriceTableViewCell(){
    IBOutletCollection(UILabel) NSArray *listLabelPins;
}

@end

@implementation DiningDetailContentPriceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.leadingConstraint.constant = self.leadingConstraint.constant*SCREEN_SCALE;
    for (UILabel* label in listLabelPins) {
        label.attributedText = [[NSAttributedString alloc] initWithString:label.text attributes:@{NSKernAttributeName:@1.0}];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)loadData{
    NSString *strPriceRange = @"";
    for (int i = 0; i< [self.priceRange integerValue]; i++) {
        strPriceRange = [strPriceRange stringByAppendingString:@"$"];
    }
    self.lblPriceRange.text = strPriceRange;
    self.lblCuisine.text = self.cuisine;
}
@end
