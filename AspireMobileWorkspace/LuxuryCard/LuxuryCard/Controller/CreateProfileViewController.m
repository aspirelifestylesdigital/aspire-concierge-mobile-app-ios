//
//  SignInViewController.m
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/13/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "CreateProfileViewController.h"
#import "MenuViewController.h"
#import "SWRevealViewController.h"
#import "UDASignInViewController.h"
#import "AppDelegate.h"
#import "WSCreateUser.h"
#import "WSB2CGetAccessToken.h"
#import "WSB2CGetUserDetails.h"
#import "WSB2CGetRequestToken.h"
#import "WSPreferences.h"
#import "NSString+Utis.h"
#import "UITextField+Extensions.h"
#import "UIButton+Extension.h"
#import "UIView+Extension.h"
#import "UILabel+Extension.h"
#import "HomeViewController.h"
#import "Constant.h"
#import "MyProfileViewController.h"
#import "AppData.h"
#import "AppDelegate.h"
#import "UserObject.h"
#import "UtilStyle.h"
#import "UserRegistrationItem.h"
#import "LocationComponent.h"
#import "PreferenceObject.h"
#import "Country.h"

#define TEXT_FIELD_FONT_SIZE 18

@interface CreateProfileViewController ()<DataLoadDelegate, UITextFieldDelegate, DropDownViewDelegate, UIGestureRecognizerDelegate>
{
    WSB2CGetAccessToken* wsB2CGetAccessToek;
    WSB2CGetRequestToken* wsB2CGetRequestToken;
    WSB2CGetUserDetails* wsGetUser;
    WSPreferences *wsPreferences;
    AppDelegate* appdelegate;
    UITapGestureRecognizer *tappedOutsideKeyboards;
    NSArray *countries;
}

@property (nonatomic, strong) WSCreateUser *wsCreateUser;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnSC;

@end

@implementation CreateProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
   // isShouldHideKeyboardWhenSwipe = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateStatusLocation:)
                                                 name:LOCATION_COMPONENT_ISAUTHORIZED_NOTIFICATION
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    
    UITapGestureRecognizer *tab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkBox:)];
    self.commitmentLabel.userInteractionEnabled = YES;
    [self.commitmentLabel addGestureRecognizer:tab];
    
    countries = getCountryList();
    [self setUIStyte];

    //TEST
//    self.firstNameText.text = currentFirstName = @"John";
//    self.lastNameText.text = currentLastName = @"Wick";
//    self.emailText.text = currentEmail = @"john.w5@gmail.com";
//    self.passwordText.text = currentPassword = @"Vietdeptrai@1";
//    self.confirmPasswordText.text = currentConfirmPassword = @"Vietdeptrai@1";
//    self.phoneNumberText.text = currentPhone = @"1-232-555-8432";
}

- (void) setUIStyte {
    
    self.phoneNumberText.keyboardType = UIKeyboardTypePhonePad;
    self.phoneNumberText.tag = PHONE_NUMBER_TEXTFIELD_TAG;
    self.emailText.keyboardType = UIKeyboardTypeEmailAddress;
    self.firstNameText.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    self.lastNameText.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    
    self.emailText.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailText.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    self.zipCodeText.autocorrectionType = UITextAutocorrectionTypeNo;
    self.zipCodeText.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    self.numberCardText.keyboardType = UIKeyboardTypeNumberPad;
    self.numberCardText.autocorrectionType = UITextAutocorrectionTypeNo;
    self.numberCardText.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    self.salutationDropDown.leadingTitle = 8.0f;
    self.salutationDropDown.title = @"Please select a salutation.";
    self.salutationDropDown.listItems = @[@"Dr", @"Miss", @"Mr", @"Mrs", @"Ms"];
    self.salutationDropDown.titleColor = colorFromHexString(TEXTFIELD_PLACEHOLDER_COLOR_STANDARD);
    self.salutationDropDown.delegate = self;
    
    self.countryCodeDropDown.type = OtherDropDownType;
    self.countryCodeDropDown.leadingTitle = 8.0f;
    self.countryCodeDropDown.itemLineBreakMode = NSLineBreakByTruncatingMiddle;
    self.countryCodeDropDown.title = @"Country Code";
    
    NSMutableArray *listCountryData = [[NSMutableArray alloc] init];
    for (Country *country in countries) {
        [listCountryData addObject:country.countryDescription];
    }
    self.countryCodeDropDown.listItems = listCountryData;
    self.countryCodeDropDown.itemDisplay = 5;
    self.countryCodeDropDown.titleColor = colorFromHexString(TEXTFIELD_PLACEHOLDER_COLOR_STANDARD);
    self.countryCodeDropDown.delegate = self;
    
    self.firstNameText.delegate = self;
    self.lastNameText.delegate = self;
    self.emailText.delegate = self;
    self.phoneNumberText.delegate = self;
    self.zipCodeText.delegate = self;
    self.numberCardText.delegate = self;
    self.passwordText.delegate = self;
    self.confirmPasswordText.delegate = self;
    
    [self createAsteriskForTextField:self.firstNameText];
    [self createAsteriskForTextField:self.lastNameText];
    [self createAsteriskForTextField:self.emailText];
    [self createAsteriskForTextField:self.phoneNumberText];
    [self createAsteriskForTextField:self.zipCodeText];
    [self createAsteriskForTextField:self.numberCardText];
    [self createAsteriskForTextField:self.passwordText];
    [self createAsteriskForTextField:self.confirmPasswordText];
    
    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
    if (profileDictionary) {
        
        NSString *salutation = [profileDictionary objectForKey:keySalutation];
        if (salutation.length > 1) {
            self.salutationDropDown.titleColor = colorFromHexString(TEXTFIELD_TEXT_COLOR_STANDARD);
            self.salutationDropDown.title = salutation;
        }
        NSString *countryCode = [profileDictionary objectForKey:keyCountryCode];
        if (countryCode.length > 0) {
            Country *currentCountry = [[Country alloc] init];
            NSString *zipCode = [profileDictionary objectForKey:keyZipCode];
            if ([countryCode isEqualToString:@"1"]) {
                if (zipCode.length > 0) {
                    currentCountry = ([[zipCode substringWithRange:NSMakeRange(0, 1)] isNumber]) ? getCountryByCodeName(@"USA") : getCountryByCodeName(@"CAN");
                }else {
                    currentCountry = getCountryByCodeName(@"USA");
                }
            }else{
                currentCountry = getCountryByNumber(countryCode);
            }
            self.countryCodeDropDown.titleColor = colorFromHexString(TEXTFIELD_TEXT_COLOR_STANDARD);
            self.countryCodeDropDown.title = [NSString stringWithFormat:@"%@ (%@)",currentCountry.countryCode, currentCountry.countryNumber];
            self.countryCodeDropDown.titleOther = currentCountry.countryDescription;
        }
        
        self.firstNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:[profileDictionary objectForKey:keyFirstName]];
        self.lastNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:[profileDictionary objectForKey:keyLastName]];
        self.emailText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:[profileDictionary objectForKey:keyEmail]];
        self.phoneNumberText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:[profileDictionary objectForKey:keyMobileNumber]];
        self.zipCodeText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:[profileDictionary objectForKey:keyZipCode]];
        
        isUseLocation = [[SessionData shareSessiondata] isUseLocation];
        [self setImageLocation:isUseLocation];
        
        currentFirstName = [profileDictionary objectForKey:keyFirstName];
        currentEmail = [profileDictionary objectForKey:keyEmail] ;
        currentPhone = [profileDictionary objectForKey:keyMobileNumber];
        currentLastName = [profileDictionary objectForKey:keyLastName];
        currentZipCode = [profileDictionary objectForKey:keyZipCode];
        
        self.firstNameText.text = [currentFirstName createMaskForText:NO];
        self.emailText.text = [currentEmail createMaskForText:YES];
        self.lastNameText.text = [currentLastName createMaskForText:NO];
        //self.phoneNumberText.text = [NSString stringWithFormat:@"*-%@", [[currentPhone substringFromIndex:2] createMaskStringBeforeNumberCharacter:3]];
        self.phoneNumberText.text = (currentPhone.length > 3) ? [currentPhone createMaskStringBeforeNumberCharacter:3]:currentPhone;
        self.emailText.enabled = NO;
        self.zipCodeText.text = currentZipCode;
        
    }else{
        
        currentFirstName = @"";
        currentEmail = @"";
        currentPhone = @"";
        currentLastName = @"";
        currentZipCode = @"";
        currentCardNumber = @"";
        
        self.firstNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.firstNameText.text];
        self.lastNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.lastNameText.text];
        self.emailText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.emailText.text];
        self.phoneNumberText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.phoneNumberText.text];
        self.zipCodeText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.zipCodeText.text];
        self.numberCardText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.numberCardText.text];
        self.passwordText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.passwordText.text];
        self.confirmPasswordText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.confirmPasswordText.text];
        
        self.firstNameText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.firstNameText.placeholder];
        self.lastNameText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.lastNameText.placeholder];
        self.emailText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.emailText.placeholder];
        self.phoneNumberText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.phoneNumberText.placeholder];
        self.zipCodeText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.zipCodeText.placeholder];
        self.numberCardText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.numberCardText.placeholder];
        self.passwordText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.passwordText.placeholder];
        self.confirmPasswordText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.confirmPasswordText.placeholder];
    }
    
    [self.myView setBackgroundColorForView];
    [self.scrollView setBackgroundColorForView];
    [self.signInButton setBackgroundColorForNormalStatus];
    [self.signInButton setBackgroundColorForTouchingStatus];
    [self.signInButton configureDecorationForButton];
    
    //[self backgroundColorForDisableStatusButton];
    [self.submitButton setBackgroundColorForNormalStatus];
    self.submitButton.enabled = NO;
    [self.submitButton setBackgroundColorForTouchingStatus];
    [self.submitButton configureDecorationForButton];
    
    [self.cancelButton setBackgroundColorForNormalStatus];
    [self.cancelButton setBackgroundColorForTouchingStatus];
    [self.cancelButton configureDecorationForButton];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.checkBoxImageTopConstraint.constant = (IPAD) ? 12.0f : 2.0f;
    });

}


- (void)backgroundColorForDisableStatusButton
{
    UIGraphicsBeginImageContext(CGSizeMake(1.0f, 1.0f));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, colorFromHexString(@"#b32d00").CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 1));
    UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.submitButton setBackgroundImage:colorImage forState:UIControlStateNormal];
}

- (void)initView{
    
    // Text Style For Title
    self.titleLable.attributedText = [UtilStyle setTextStyleForTitleViewControllerWithMessage:@"SIGN UP"];
    [self.titleLable setTextColor:colorFromHexString(DEFAULT_HIGHLIGHT_COLOR)];
    
    // Text Style For Greet Message
    NSString *message = NSLocalizedString(@"commitment_profile_message", nil);
    self.commitmentLabel.attributedText = [UtilStyle setLargeSizeStyleForLabelWithMessage:message];
    //[self setCheckBoxState:isCheck];
}


- (void) getUserInfo{
    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
    if (profileDictionary) {
        
        NSString *salutation = [profileDictionary objectForKey:keySalutation];
        self.salutationDropDown.titleColor = (salutation.length > 1)?colorFromHexString(TEXTFIELD_TEXT_COLOR_STANDARD):colorFromHexString(TEXTFIELD_PLACEHOLDER_COLOR_STANDARD);
        self.salutationDropDown.title = (salutation.length > 1)?salutation:@"Please select a salutation.";
        
        NSString *countryCode = [profileDictionary objectForKey:keyCountryCode];
        self.countryCodeDropDown.titleColor = (countryCode.length > 0)?colorFromHexString(TEXTFIELD_TEXT_COLOR_STANDARD):colorFromHexString(TEXTFIELD_PLACEHOLDER_COLOR_STANDARD);
        
        if (countryCode.length > 0) {
            Country *currentCountry = [[Country alloc] init];
            NSString *zipCode = [profileDictionary objectForKey:keyZipCode];
            if ([countryCode isEqualToString:@"1"]) {
                if (zipCode.length > 0) {
                    currentCountry = ([[zipCode substringWithRange:NSMakeRange(0, 1)] isNumber]) ? getCountryByCodeName(@"USA") : getCountryByCodeName(@"CAN");
                }else {
                    currentCountry = getCountryByCodeName(@"USA");
                }
                
            }else{
                currentCountry = getCountryByNumber(countryCode);
            }
            self.countryCodeDropDown.title = (countryCode.length > 0)?[NSString stringWithFormat:@"%@ (%@)",currentCountry.countryCode, currentCountry.countryNumber]:@"Please select a country code.";
            self.countryCodeDropDown.titleOther = (countryCode.length > 0)?currentCountry.countryDescription:@"Please select a country code.";
        }
        currentFirstName = [profileDictionary objectForKey:keyFirstName];
        currentEmail = [profileDictionary objectForKey:keyEmail] ;
        currentPhone = [profileDictionary objectForKey:keyMobileNumber];
        currentLastName = [profileDictionary objectForKey:keyLastName];
        currentZipCode = [profileDictionary objectForKey:keyZipCode];
        
        self.firstNameText.text = [currentFirstName createMaskForText:NO];
        self.emailText.text = [currentEmail createMaskForText:YES];
        self.lastNameText.text = [currentLastName createMaskForText:NO];
        //self.phoneNumberText.text = [NSString stringWithFormat:@"*-%@", [[currentPhone substringFromIndex:2] createMaskStringBeforeNumberCharacter:3]];
        self.phoneNumberText.text = (currentPhone.length > 3) ? [currentPhone createMaskStringBeforeNumberCharacter:3]: currentPhone;
        self.zipCodeText.text = currentZipCode;
        isUseLocation = [[SessionData shareSessiondata] isUseLocation];
        [self setImageLocation:isUseLocation];
    }
    
}

-(void) createAsteriskForTextField:(UITextField *)textField
{
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 8, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [textField setLeftView:leftView];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    
    asteriskImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"asterisk_icon"]];
    
    asteriskImage.frame = CGRectMake(0.0f, 0.0f, 20.0f, 20.0f);
    asteriskImage.contentMode = UIViewContentModeCenter;
    [textField setRightView:asteriskImage];
    [textField setRightViewMode:UITextFieldViewModeAlways];
    
}

- (void) changeUseLocation {
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setTextViewsDefaultBottomBolder];
    tappedOutsideKeyboards = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tappedOutsideKeyboards.delegate = self;
    [self.navigationController.view addGestureRecognizer:tappedOutsideKeyboards];
}


-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.view layoutIfNeeded];
    
}

-(void)setTextViewsDefaultBottomBolder
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.firstNameText setBottomBolderDefaultColor];
        [self.lastNameText setBottomBolderDefaultColor];
        [self.emailText setBottomBolderDefaultColor];
        [self.phoneNumberText setBottomBolderDefaultColor];
        [self.passwordText setBottomBolderDefaultColor];
        [self.confirmPasswordText setBottomBolderDefaultColor];
        [self.salutationDropDown setBottomBolderDefaultColor];
        [self.countryCodeDropDown setBottomBolderDefaultColor];
        [self.zipCodeText setBottomBolderDefaultColor];
        [self.numberCardText setBottomBolderDefaultColor];
    });
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController.view removeGestureRecognizer:tappedOutsideKeyboards];
}


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) setCheckBoxState:(BOOL)check
{
    if (!check) {
        check = NO;
    }
    (check == YES) ? [self.checkBoxButton setImage:[UIImage imageNamed:@"checkbox_check"] forState:UIControlStateNormal] : [self.checkBoxButton setImage:[UIImage imageNamed:@"checkbox_uncheck"] forState:UIControlStateNormal];
}
- (IBAction)checkBox:(id)sender {
    isCheck = !isCheck;
    [self setCheckBoxState:isCheck];
}

- (IBAction)actionSwitchLocation:(id)sender {
    
    isUseLocation = !isUseLocation;
    [self setImageLocation:isUseLocation];
    if (![self isKindOfClass:[MyProfileViewController class]]) {
        [[SessionData shareSessiondata] setIsUseLocation:isUseLocation];
        if (isUseLocation) {
            [LocationComponent startRequestLocation];
        }
    }
    else{
        [[SessionData shareSessiondata] setIsUseLocation:isUseLocation];
        if (isUseLocation) {
            [LocationComponent startRequestLocation];
        }
    }
   [self changeUseLocation];
}

- (void)setImageLocation:(BOOL)isLocation {
    self.locationStatusImage.image = [UIImage imageNamed:(isLocation) ? @"location_on" : @"location_off"];
}

-(void)updateStatusLocation:(NSNotification *)notification {
    
    NSDictionary* dictNoti = notification.userInfo;
    BOOL isLocation = [dictNoti boolForKey:LOCATION_COMPONENT_ISAUTHORIZED_NOTIFICATION];
    if (!isLocation) {
        if ([dictNoti intForKey:@"ERROR_CODE"] == 2) {
            isUseLocation = NO;
            [self setImageLocation:isUseLocation];
            [[SessionData shareSessiondata] setIsUseLocation:isUseLocation];
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
                [self setRequestLocation:YES];
            });
        }else if ([dictNoti intForKey:@"ERROR_CODE"] == 1) {
            if([[NSUserDefaults standardUserDefaults] boolForKey:@"IsRequestLocation"]) {
                [self setRequestLocation:NO];
            }else{
                [self showErrorLocationService];
            }
        }
    }
    [self changeUseLocation];
}

- (void)setRequestLocation:(BOOL)isRequest{
    [[NSUserDefaults standardUserDefaults] setBool:isRequest forKey:@"IsRequestLocation"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)showErrorLocationService {
    
    isUseLocation = NO;
    [self setImageLocation:isUseLocation];
    [[SessionData shareSessiondata] setIsUseLocation:isUseLocation];
    
     UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Location Service Disabled" message:@"To enable, please go to Settings and turn on Location Service for this app." preferredStyle:UIAlertControllerStyleAlert];
     
     UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction*  action) {
     
     }];
     
     UIAlertAction *settingAction = [UIAlertAction actionWithTitle:@"Setting" style:UIAlertActionStyleDefault handler:^(UIAlertAction  *action) {
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
     }];
     
     [alertController addAction:okAction];
     [alertController addAction:settingAction];
     alertController.preferredAction = settingAction;
     
     [self presentViewController:alertController animated: YES completion: nil];
}

- (void)updateProfileButtonStatus{

}

-(void) enableSignInButton:(UIButton *)button
{
    [button setHighlighted:NO];
    self.checkBoxButton.selected = !(button.selected);
    [button setBackgroundColor:[UIColor clearColor]];
    [self.submitButton setEnabled:button.selected];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)CheckboxAction:(id)sender {
    if (isCheck) {
        self.checkBoxImage.image = [UIImage imageNamed:@"checkbox_uncheck"];
        isCheck = NO;
    }else{
        self.checkBoxImage.image = [UIImage imageNamed:@"checkbox_check"];
        isCheck = YES;
    }
    [self enableSubmitButton:isCheck];
}

- (void)enableSubmitButton:(BOOL)isEnable {
    self.submitButton.enabled = isEnable;
}

- (IBAction)SubmitAction:(id)sender {
    [self verifyAccountData:NO];
}

- (IBAction)CancelAction:(id)sender {
    if ([AppData isCreatedProfile]) {
    SWRevealViewController *revealViewController = self.revealViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UDASignInViewController *signInViewController = [storyboard instantiateViewControllerWithIdentifier:@"UDASignInViewController"];
    [revealViewController pushFrontViewController:signInViewController animated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)updateProfileWithAPI{
    [self startActivityIndicator];
    NSMutableDictionary *userDict = [[NSMutableDictionary alloc] init];
    [userDict setValue:B2C_ConsumerKey forKey:@"ConsumerKey"];
    [userDict setValue:_isUpdateProfile ? @"Registration" : @"Registration" forKey:@"Functionality"];
    [userDict setValue:(self.salutationDropDown.title.length > 1) ? self.salutationDropDown.title : @" " forKey:@"Salutation"];
    [userDict setValue:currentFirstName forKey:@"FirstName"];
    [userDict setValue:currentLastName forKey:@"LastName"];
    
    NSString *phoneNumberWithCode = @"";
    if (self.countryCodeDropDown.selectedIndex == -1) {
        phoneNumberWithCode = [NSString stringWithFormat:@"%@",[currentPhone stringByReplacingOccurrencesOfString:@"-" withString:@""]];
    }else {
        Country *country = [countries objectAtIndex:self.countryCodeDropDown.selectedIndex];
        phoneNumberWithCode = [NSString stringWithFormat:@"%@%@",country.countryNumber,[currentPhone stringByReplacingOccurrencesOfString:@"-" withString:@""]];
    }

    [userDict setValue:phoneNumberWithCode forKey:@"MobileNumber"];
    [userDict setValue:currentEmail forKey:@"Email"];
    [userDict setValue:currentZipCode forKey:@"ZipCode"];
    [userDict setValue:currentPassword forKey:@"Password"];
    [userDict setValue:B2C_DeviceId forKey:@"MemberDeviceID"];
    [[SessionData shareSessiondata] setIsUseLocation:isUseLocation];
    
    self.wsCreateUser = [[WSCreateUser alloc] init];
    self.wsCreateUser.delegate = self;
    if (!_isUpdateProfile) {
        [self.wsCreateUser createUser:userDict];
    }else{
        [self.wsCreateUser updateUser:userDict];
    }
    
}

-(void)loadDataDoneFrom:(WSBase*)ws {
    
    if (ws.task == WS_CREATE_USER) {
        if (ws.data) {
            [self setUserDefaultInfoWithDict:ws.data[0]];
        }
//        [self performSelector:@selector(setUserObjectWithDict:) withObject:((WSCreateUser*)ws).user afterDelay:1];
        
//        if (((WSCreateUser*)ws).user) {
//            [self setUserObjectWithDict:((WSCreateUser*)ws).user];
//        }
        [AppData setCreatedProfileSuccessful];
        
        [self addPreferences];
        
    } else if (ws.task == WS_GET_MY_PREFERENCE){
        [self stopActivityIndicator];
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = nil;
        alert.msgAlert = @"Profile created successfully.";
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
            alert.lblAlertMessage.textAlignment = NSTextAlignmentCenter;
            [alert.view layoutIfNeeded];
        });
        
        alert.blockFirstBtnAction = ^(void){
            [self updateSuccessRedirect];
        };
        
        [self showAlert:alert forNavigation:NO];
    }else if(ws.task == WS_ADD_MY_PREFERENCE || ws.task == WS_UPDATE_MY_PREFERENCE){

        if (![self isKindOfClass:[MyProfileViewController class]]) {
            [self getPreference];
        }else{
            [self stopActivityIndicator];
            getPreferencesFromAPI(NO, self);
            [self updateSuccessRedirect];
        }
    }
    
}

- (void)addPreferences{
    
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    NSMutableArray *arraySubDictValues = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *hotelDict = [[NSMutableDictionary alloc] init];
    [hotelDict setValue:@"Hotel" forKey:@"Type"];
    
    PreferenceObject *hotelPref = [self getPreferenceByType:HotelPreferenceType];
    NSString *value = ([[SessionData shareSessiondata] isUseLocation])?@"YES":@"NO";
    if ([SessionData shareSessiondata].arrayPreferences.count > 0 && hotelPref.preferenceID.length > 0){
        [hotelDict setValue:hotelPref.preferenceID forKey:@"MyPreferencesId"];
        [hotelDict setValue:hotelPref.value forKey:@"Preferredstarrating"];
        [hotelDict setValue:value forKey:@"SmokingPreference"];
        [arraySubDictValues addObject:hotelDict];
        [dataDict setValue:arraySubDictValues forKey:@"PreferenceDetails"];
        
        wsPreferences = [[WSPreferences alloc] init];
        wsPreferences.delegate = self;
        [wsPreferences updatePreference:dataDict];
        
    }else{
        [hotelDict setValue:@"" forKey:@"Preferredstarrating"];
        [hotelDict setValue:value forKey:@"SmokingPreference"];
        [arraySubDictValues addObject:hotelDict];
        
//        //Begin: support for android.
//        NSMutableDictionary *diningDict = [[NSMutableDictionary alloc] init];
//        [diningDict setValue:@"Dining" forKey:@"Type"];
//        [diningDict setValue:@"" forKey:@"CuisinePreferences"];
//        [arraySubDictValues addObject:diningDict];
//        
//        NSMutableDictionary *transportationDict = [[NSMutableDictionary alloc] init];
//        [transportationDict setValue:@"Car Rental" forKey:@"Type"];
//        [transportationDict setValue:@"" forKey:@"PreferredRentalVehicle"];
//        [arraySubDictValues addObject:transportationDict];
//        //End: support for android.
        
        [dataDict setValue:arraySubDictValues forKey:@"PreferenceDetails"];
        wsPreferences = [[WSPreferences alloc] init];
        wsPreferences.delegate = self;
        [wsPreferences addPreference:dataDict];
    }
    
}
-(PreferenceObject*)getPreferenceByType:(PreferenceType)type{
    if ([SessionData shareSessiondata].arrayPreferences.count > 0) {
        for (PreferenceObject *preference in [SessionData shareSessiondata].arrayPreferences) {
            if (preference.type == type) {
                return preference;
            }
        }
    }
    return nil;
}

- (void)getPreference{
    
    [self startActivityIndicator];
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    [dataDict setValue:B2C_ConsumerKey forKey:@"ConsumerKey"];
    [dataDict setValue:@"GetPreference" forKey:@"Functionality"];
    [dataDict setValue:[[SessionData shareSessiondata] OnlineMemberID] forKey:@"OnlineMemberId"];
    
    wsPreferences = [[WSPreferences alloc] init];
    wsPreferences.delegate = self;
    [wsPreferences getPreference];
    
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Password"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }
}

-(void)loadDataFailFrom:(WSBase *)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message
{
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }else{
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);
        if ([message isEqualToString:@"ENR68-2"]) {
            alert.msgAlert =  @"An account with this email address already exists. Click Cancel to Sign In.";
        }else if ([message isEqualToString:@"ENR61-1"]) {
            alert.msgAlert =  @"Please confirm that the value entered is correct.";
        }else {
            alert.msgAlert = message;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.lblAlertMessage.textAlignment = NSTextAlignmentLeft;
            [alert.view layoutIfNeeded];
        });
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
        });
        
        [self showAlert:alert forNavigation:NO];
    }
}

- (void)setUserObjectWithDict:(NSDictionary*)dict{
    [[SessionData shareSessiondata] setUserObjectWithDict:dict];
}

- (void)setUserDefaultInfoWithDict:(UserRegistrationItem*)item{
    [[SessionData shareSessiondata] setOnlineMemberID:item.OnlineMemberID];
    [[SessionData shareSessiondata] setOnlineMemberDetailIDs:item.OnlineMemberDetailIDs];
}

#pragma mark KEYBOARD PROCESS
-(void)handleKeyboardWillShow:(NSNotification *)paramNotification
{
    [self.salutationDropDown didTapBackground];
    [self.countryCodeDropDown didTapBackground];
    
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         keyboardHeight = keyboardRect.size.height;
                         [self updateEdgeInsetForShowKeyboard];
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

-(void)handleKeyboardWillHide:(NSNotification *)paramNotification
{
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
//    isAnimatingHideKeyboard = NO;
//    if(isShouldHideKeyboardWhenSwipe)
//        [UIView setAnimationsEnabled:NO];
    
    [self updateEdgeInsetForHideKeyboard];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:0
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL isFinished){
//                         isAnimatingHideKeyboard = !isFinished;
                     }];
    
    
}


-(void)updateEdgeInsetForShowKeyboard
{
    
//    float heightInset = 0.0;
//    heightInset = ([self isKindOfClass:[MyProfileViewController class]])?80.0f:120.0f; //80.0f:60.0f
//    
//    self.scrollView.contentInset = UIEdgeInsetsMake(- heightInset,  0.0f, 0.0f, 0.0f);
//    self.scrollBottom.constant = 150.0f + heightInset;
//    if (!IPAD) {
//        self.viewActionBottomConstraint.constant = (keyboardHeight + MARGIN_KEYBOARD);
//    }
//    [self.view layoutIfNeeded];
    _btnSC.constant =  keyboardHeight + 80;
    
}


-(void)updateEdgeInsetForHideKeyboard
{
    _btnSC.constant = 80;
//    self.scrollView.contentInset = UIEdgeInsetsMake(0.0f,  0.0f, 0.0f, 0.0f);
    [self resignFirstResponderForAllTextField];
//    self.scrollBottom.constant = 0.0f;
//    self.viewActionBottomConstraint.constant = backupBottomConstraint;
//    [self.view layoutIfNeeded];
}

- (void)hidenKeyboardWhenSwipe{
//    [self.view.layer removeAllAnimations];
    [self updateEdgeInsetForHideKeyboard];
    [UIView setAnimationsEnabled:YES];
}


-(void)resignFirstResponderForAllTextField
{
    if(self.firstNameText.isFirstResponder)
    {
        [self.firstNameText resignFirstResponder];
    }
    else if(self.lastNameText.isFirstResponder)
    {
        [self.lastNameText resignFirstResponder];
    }
    else if(self.emailText.isFirstResponder)
    {
        [self.emailText resignFirstResponder];
    }
    else if(self.phoneNumberText.isFirstResponder)
    {
        [self.phoneNumberText resignFirstResponder];
    }
    else if(self.passwordText.isFirstResponder)
    {
        [self.passwordText resignFirstResponder];
    }
    else if(self.confirmPasswordText.isFirstResponder)
    {
        [self.confirmPasswordText resignFirstResponder];
    }
    else if (self.zipCodeText.isFirstResponder)
    {
        [self.zipCodeText resignFirstResponder];
    }
    else if (self.numberCardText.isFirstResponder)
    {
        [self.numberCardText resignFirstResponder];
    }
}

-(void)dismissKeyboard
{
    [self updateEdgeInsetForHideKeyboard];
    [self.salutationDropDown didTapBackground];
    [self.countryCodeDropDown didTapBackground];
}

-(BOOL)verifyValueForTextField:(UITextField *)textFied andMessageError:(NSMutableString *)message
{
    NSString *currentText = nil;
    NSString *errorMsg = nil;
    BOOL isValid = NO;
    if(textFied == self.firstNameText)
    {
        currentText = currentFirstName;
        
        NSMutableString *tempError = [[NSMutableString alloc] init];
        if (![(textFied.isFirstResponder ? [textFied.text removeRedudantWhiteSpaceInText] :currentText) isValidName]) {
            isValid = NO;
            if (tempError.length > 0) {
                [tempError appendString:@"\n* "];
            }
            [tempError appendString:NSLocalizedString(@"input_invalid_first_name_msg", nil)];
        }else if (![(textFied.isFirstResponder ? textFied.text :currentText) isValidNameWithSpecialCharacter]) {
            isValid = NO;
            if (tempError.length > 0) {
                [tempError appendString:@"\n* "];
            }
            [tempError appendString:@"Please enter a valid first name. Acceptable special characters are - and space."]; //Please enter a valid first name.
        }else{
            isValid = YES;
        }
        
        errorMsg = tempError;
        //errorMsg = NSLocalizedString(@"input_invalid_first_name_msg", nil);
        //isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidName];
    }
    else if(textFied == self.lastNameText)
    {
        currentText = currentLastName;
        
        NSMutableString *tempError = [[NSMutableString alloc] init];
        if (![(textFied.isFirstResponder ? [textFied.text removeRedudantWhiteSpaceInText] :currentText) isValidName]) {
            isValid = NO;
            if (tempError.length > 0) {
                [tempError appendString:@"\n* "];
            }
            [tempError appendString:NSLocalizedString(@"input_invalid_last_name_msg", nil)];
        }else if (![(textFied.isFirstResponder ? textFied.text :currentText) isValidNameWithSpecialCharacter]) {
            isValid = NO;
            if (tempError.length > 0) {
                [tempError appendString:@"\n* "];
            }
            [tempError appendString:@"Please enter a valid last name. Acceptable special characters are - and space."]; //Please enter a valid last name.
        }else{
            isValid = YES;
        }
        
        errorMsg = tempError;
        //errorMsg = NSLocalizedString(@"input_invalid_last_name_msg", nil);
        //isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidName];
    }
    else if(textFied == self.emailText)
    {
        currentText = currentEmail;
        errorMsg = NSLocalizedString(@"input_invalid_email_msg", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidEmail];
    }
    else if(textFied == self.phoneNumberText)
    {
        currentText = currentPhone;
        errorMsg = NSLocalizedString(@"input_invalid_phone_number_msg", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidPhoneNumber];
    }
    else if (textFied == self.zipCodeText)
    {
        currentText = currentZipCode;
        errorMsg = @"Enter a valid zip code.";
        isValid = [(textFied.isFirstResponder ? [textFied.text removeRedudantWhiteSpaceInText] : currentText) isValidZipCode];
    }
    else if (textFied == self.numberCardText)
    {
        currentText = currentCardNumber;
        errorMsg = @"Enter a valid first six digits of your credit card number.";
        isValid = [(textFied.isFirstResponder ? textFied.text : currentText) isValidBinNumber];
    }
    else if(textFied == self.passwordText)
    {
        currentText = currentPassword;
        errorMsg = NSLocalizedString(@"input_invalid_password_msg", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidPassword];//isValidStrongPassword
    }
    else if(textFied == self.confirmPasswordText)
    {
        currentText = currentConfirmPassword;
        errorMsg = NSLocalizedString(@"input_invalid_confirm_password_msg", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isEqualToString:currentPassword];
    }
    
    if((textFied.isFirstResponder ? textFied.text :currentText).length > 0)
    {
        if(!isValid)
        {
            (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
            [message appendString:errorMsg];
            [textFied setBottomBorderRedColor];
        }
        else
        {
            [textFied setBottomBolderDefaultColor];
        }
    }
    else
    {
        [textFied setBottomBorderRedColor];
    }
    
    return isValid;
}
- (BOOL)verifyValueForDropdown:(DropDownView*)dropdown andMessageError:(NSMutableString *)message{
    
    BOOL isValid = NO;
    
    if (dropdown == self.salutationDropDown) {
        if (self.salutationDropDown.valueStatus == DefaultValueStatus) {
            (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
            [message appendString:@"Select a salutation."]; //All fields are required.
            isValid = NO;
            [self.salutationDropDown setBottomBorderRedColor];
        }else {
            isValid = YES;
            [self.salutationDropDown setBottomBolderDefaultColor];
        }
        /*
        if ((self.firstNameText.text.length > 0 && self.lastNameText.text.length > 0 && self.emailText.text.length > 0 && self.phoneNumberText.text.length > 0 && self.zipCodeText.text.length > 0 && self.numberCardText.text.length > 0 && self.passwordText.text.length > 0 && self.confirmPasswordText.text.length > 0)) {
            if (self.salutationDropDown.valueStatus == DefaultValueStatus) {
                (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
                [message appendString:@"Select a salutation."]; //All fields are required.
                isValid = NO;
                [self.salutationDropDown setBottomBorderRedColor];
            }
        }else{
            if (self.salutationDropDown.valueStatus == DefaultValueStatus) {
                isValid = NO;
                [self.salutationDropDown setBottomBorderRedColor];
            }else{
                isValid = YES;
                [self.salutationDropDown setBottomBolderDefaultColor];
            }
        }
         */
    }
    else if (dropdown == self.countryCodeDropDown) {
        if (self.countryCodeDropDown.valueStatus == DefaultValueStatus) {
            (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
            [message appendString:@"Select a country code."];
            isValid = NO;
            [self.countryCodeDropDown setBottomBorderRedColor];
        }else {
            isValid = YES;
            [self.countryCodeDropDown setBottomBolderDefaultColor];
        }
        /*
        if ((self.firstNameText.text.length > 0 && self.lastNameText.text.length > 0 && self.emailText.text.length > 0 && self.phoneNumberText.text.length > 0 && self.zipCodeText.text.length > 0 && self.numberCardText.text.length > 0 && self.passwordText.text.length > 0 && self.confirmPasswordText.text.length > 0)) {
            if (self.countryCodeDropDown.valueStatus == DefaultValueStatus) {
                (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
                [message appendString:@"Select a country code."];
                isValid = NO;
                [self.countryCodeDropDown setBottomBorderRedColor];
            }
        }else{
            if (self.countryCodeDropDown.valueStatus == DefaultValueStatus) {
                isValid = NO;
                [self.countryCodeDropDown setBottomBorderRedColor];
            }else{
                isValid = YES;
                [self.countryCodeDropDown setBottomBolderDefaultColor];
            }
        }
         */
    }
    return isValid;
}
#pragma mark LOCAL PROCESS
-(void)verifyAccountData:(BOOL)isUpdate
{
    NSMutableString *message = [[NSMutableString alloc] init];
    if (isUpdate) {
        if(self.firstNameText.text.length == 0 || self.lastNameText.text.length == 0 || self.emailText.text.length == 0 || self.phoneNumberText.text.length == 0 || self.zipCodeText.text.length == 0)
        {
            [message appendString:@"* "];
            [message appendString:@"All fields are required."];
            if (self.salutationDropDown.title.length > 10) {
                [self verifyValueForDropdown:self.salutationDropDown andMessageError:message];
            }
            [self verifyValueForTextField:self.firstNameText andMessageError:message];
            [self verifyValueForTextField:self.lastNameText andMessageError:message];
            [self verifyValueForTextField:self.emailText andMessageError:message];
            if (self.countryCodeDropDown.title.length > 11) {
                [self verifyValueForDropdown:self.countryCodeDropDown andMessageError:message];
            }
            [self verifyValueForTextField:self.phoneNumberText andMessageError:message];
            [self verifyValueForTextField:self.zipCodeText andMessageError:message];
            
            [self createProfileInforWithMessage:message isCreatedSuccessful:NO];
        }
        else{
            if (self.salutationDropDown.title.length > 10) {
                [self verifyValueForDropdown:self.salutationDropDown andMessageError:message];
            }
            [self verifyValueForTextField:self.firstNameText andMessageError:message];
            [self verifyValueForTextField:self.lastNameText andMessageError:message];
            [self verifyValueForTextField:self.emailText andMessageError:message];
            if (self.countryCodeDropDown.title.length > 11) {
                [self verifyValueForDropdown:self.countryCodeDropDown andMessageError:message];
            }
            [self verifyValueForTextField:self.phoneNumberText andMessageError:message];
            [self verifyValueForTextField:self.zipCodeText andMessageError:message];
            
            [self createProfileInforWithMessage:message isCreatedSuccessful:(message.length == 0)];
        }
    }else{
        if(self.firstNameText.text.length == 0 || self.lastNameText.text.length == 0 || self.emailText.text.length == 0 || self.phoneNumberText.text.length == 0 || self.zipCodeText.text.length == 0 || self.numberCardText.text.length == 0 || self.passwordText.text.length == 0 || self.confirmPasswordText.text.length == 0)
        {
            [message appendString:@"* "];
            [message appendString:@"All fields are required."];
            
            [self verifyValueForDropdown:self.salutationDropDown andMessageError:message];
            
            [self verifyValueForTextField:self.firstNameText andMessageError:message];
            [self verifyValueForTextField:self.lastNameText andMessageError:message];
            [self verifyValueForTextField:self.emailText andMessageError:message];
            
            [self verifyValueForDropdown:self.countryCodeDropDown andMessageError:message];
            
            [self verifyValueForTextField:self.phoneNumberText andMessageError:message];
            [self verifyValueForTextField:self.zipCodeText andMessageError:message];
            [self verifyValueForTextField:self.numberCardText andMessageError:message];
            [self verifyValueForTextField:self.passwordText andMessageError:message];
            [self verifyValueForTextField:self.confirmPasswordText andMessageError:message];
            
            [self createProfileInforWithMessage:message isCreatedSuccessful:NO];
        }
        else{
            
            [self verifyValueForDropdown:self.salutationDropDown andMessageError:message];

            [self verifyValueForTextField:self.firstNameText andMessageError:message];
            [self verifyValueForTextField:self.lastNameText andMessageError:message];
            [self verifyValueForTextField:self.emailText andMessageError:message];
            
            [self verifyValueForDropdown:self.countryCodeDropDown andMessageError:message];
            
            [self verifyValueForTextField:self.phoneNumberText andMessageError:message];
            [self verifyValueForTextField:self.zipCodeText andMessageError:message];
            [self verifyValueForTextField:self.numberCardText andMessageError:message];
            [self verifyValueForTextField:self.passwordText andMessageError:message];
            [self verifyValueForTextField:self.confirmPasswordText andMessageError:message];
            
            [self createProfileInforWithMessage:message isCreatedSuccessful:(message.length == 0)];
        }
    }
}

-(void)createProfileInforWithMessage:(NSString *)message isCreatedSuccessful:(BOOL)isSuccessful
{
    if(isSuccessful)
    {
        [self updateEdgeInsetForHideKeyboard];
        [self setTextViewsDefaultBottomBolder];
        
//      save profile is create
        [self updateProfileWithAPI];
        
    }
    else
    {
        NSMutableString *newMessage = [[NSMutableString alloc] init];
        [newMessage appendString:@"\n"];
        [newMessage appendString:message];
        [newMessage appendString:@"\n"];
        
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = @"Please confirm that the value entered is correct:";
        alert.msgAlert = newMessage;
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
            alert.lblAlertMessage.textAlignment = NSTextAlignmentLeft;
            [alert.view layoutIfNeeded];
        });
        
        alert.blockFirstBtnAction = ^(void){
            [self makeBecomeFirstResponderForTextField];
        };
        
        [self showAlert:alert forNavigation:NO];
         
    }
}


- (void) updateSuccessRedirect{
    
    appdelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
   
    [UIView transitionWithView:appdelegate.window
                      duration:0.5
                       options:UIViewAnimationOptionPreferredFramesPerSecond60
                    animations:^{
                        
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        
                        MenuViewController *menuViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
                        UINavigationController *menuNavigationViewController = [[UINavigationController alloc] initWithRootViewController:menuViewController];
                        UIViewController *fontViewController = [[HomeViewController alloc] init];
                        
                        SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:menuNavigationViewController frontViewController:fontViewController];
                        
                        revealController.delegate = appdelegate;
                        revealController.rearViewRevealWidth = SCREEN_WIDTH;
                        revealController.rearViewRevealOverdraw = 0.0f;
                        revealController.rearViewRevealDisplacement = 0.0f;
                        appdelegate.viewController = revealController;
                        appdelegate.window.rootViewController = appdelegate.viewController;
                        [appdelegate.window makeKeyWindow];
                        
                        UIViewController *newFrontController = [[HomeViewController alloc] init];
                        UINavigationController *newNavigationViewController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                        [revealController pushFrontViewController:newNavigationViewController animated:YES];
                        
                    }
                    completion:nil];
}
-(void) makeBecomeFirstResponderForTextField
{
    [self resignFirstResponderForAllTextField];
    if(![currentFirstName isValidName])
    {
        [self.firstNameText becomeFirstResponder];
    }
    else if(![currentLastName isValidName])
    {
        [self.lastNameText becomeFirstResponder];
    }
    else if(![currentEmail isValidEmail])
    {
        [self.emailText becomeFirstResponder];
    }
    else if(![currentPhone isValidPhoneNumber])
    {
        [self.phoneNumberText becomeFirstResponder];
    }
    else if(![self.passwordText.text isValidPassword])
    {
        [self.passwordText becomeFirstResponder];
    }
    else if(![self.confirmPasswordText.text isEqualToString:currentPassword])
    {
        [self.confirmPasswordText becomeFirstResponder];
    }
    else if (![self.zipCodeText.text isValidZipCode])
    {
        [self.zipCodeText becomeFirstResponder];
    }
    else if (![self.numberCardText.text isValidBinNumber])
    {
        [self.numberCardText becomeFirstResponder];
    }
}

- (void) handleAsterickIcon{
    
}
#pragma mark TEXT FIELD DELEGATE

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.text = [textField.text removeRedudantWhiteSpaceInText];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == self.firstNameText)
    {
        currentFirstName = textField.text;
        textField.text = [textField.text createMaskForText:NO];

    }
    else if(textField == self.lastNameText)
    {
        currentLastName = textField.text;
        textField.text =  [textField.text createMaskForText:NO];

    }
    else if(textField == self.phoneNumberText)
    {
//        if(textField.text.length == 2 && [[textField.text substringToIndex:2] isEqualToString:@"1-"]) {
//            textField.text = nil;
//        }
        currentPhone = textField.text;
        
        if (textField.text.length > 3) {
//            NSString *subString = [textField.text substringFromIndex:2];
//            textField.text = [NSString stringWithFormat:@"**%@", [subString createMaskStringBeforeNumberCharacter:(subString.length > 2) ? 3 : 2]];
            textField.text = (textField.text.length > 3) ? [textField.text createMaskStringBeforeNumberCharacter:3]: textField.text;
        }

    }
    else if(textField == self.emailText)
    {
        currentEmail = textField.text;
        if([textField.text containsString:@"@"])
        {
            textField.text = [textField.text createMaskForText:YES];
        }
    }
    else if (textField == self.zipCodeText)
    {
        currentZipCode = textField.text;
        //textField.text = [textField.text createMaskForText:NO];
    }
    else if (textField == self.numberCardText)
    {
        currentCardNumber = textField.text;
        //textField.text = [textField.text createMaskForText:NO];
    }
    else if(textField == self.passwordText)
    {
        currentPassword = textField.text;
    }
    else if(textField == self.confirmPasswordText)
    {
        currentConfirmPassword = textField.text;
    }
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == self.firstNameText)
    {
        textField.text = currentFirstName;
    }
    
    else if(textField == self.lastNameText)
    {
        textField.text = currentLastName;
    }
    
    else if(textField == self.phoneNumberText)
    {
        textField.text = currentPhone;
        
//        if(textField.text.length > 0)
//        {
//            if(![[textField.text substringWithRange:NSMakeRange(0, 2)] isEqualToString:@"1-"])
//            {
//                NSMutableString *insertCountryCode = [[NSMutableString alloc] initWithString:textField.text];
//                [insertCountryCode insertString:@"1-" atIndex:0];
//                textField.text = insertCountryCode;
//            }
//        }
//        else
//        {
//            textField.text = @"1-";
//        }
    }
    else if(textField == self.emailText)
    {
        textField.text = currentEmail;
    }
    else if(textField == self.zipCodeText)
    {
        textField.text = currentZipCode;
    }
    else if(textField == self.numberCardText)
    {
        textField.text = currentCardNumber;
    }
    else if(textField == self.passwordText)
    {
        textField.text = currentPassword;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([self isKindOfClass:[MyProfileViewController class]]) {
        if (textField == self.emailText) {
            return NO;
        }
    }
    if (textField == self.passwordText || textField == self.confirmPasswordText) {
        if ([string isEqualToString:@" "]) {
            return NO;
        }
    }
    
    NSString *inputString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (textField == self.firstNameText || textField == self.lastNameText) {
        if (inputString.length <= 19) {
            [self checkTextFieldIsChange:textField shouldChangeCharactersInRange:range replacementString:string];
        }
    }else if (textField == self.zipCodeText) {
        if (inputString.length <= 10) {
            [self checkTextFieldIsChange:textField shouldChangeCharactersInRange:range replacementString:string];
        }
    }else{
        [self checkTextFieldIsChange:textField shouldChangeCharactersInRange:range replacementString:string];
    }
    
    return [self updateTextFiel:textField shouldChangeCharactersInRange:range replacementString:string];
    
}

-(void)checkTextFieldIsChange:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
}

-(BOOL)updateTextFiel:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(string.length > 1)
    {

        NSMutableString *newString = [[NSMutableString alloc] initWithString:[string removeRedudantWhiteSpaceInText]];
        
        NSString *resultText = [textField.text stringByReplacingCharactersInRange:range
                                                                       withString:string];
        resultText = [resultText removeRedudantWhiteSpaceInText];
        
        if(textField == self.firstNameText || textField == self.lastNameText)
        {
//            if([newString isValidName])
//            {
//                textField.text = newString;
//            }
//            else if(string.length > 19)
//            {
//                textField.text = [newString substringToIndex:19];
//            }
//            else
//            {
//                textField.text = newString;
//            }
            textField.text = (resultText.length > 19) ? [resultText substringWithRange:NSMakeRange(0, 19)] : resultText;
        }
        if(textField == self.passwordText || textField == self.confirmPasswordText)
        {
            if([newString isValidPassword]) //isValidStrongPassword
            {
                textField.text = newString;
            }
            else if(string.length > 25)
            {
                textField.text = [newString substringToIndex:25];
            }
            else
            {
                textField.text = newString;
            }
        }
        if(textField == self.phoneNumberText)
        {
            if([newString isValidPhoneNumber])
            {
                textField.text = newString;
            }
            /*
            else{
                if(newString.length <= 10)
                {
                    [newString insertString:@"1-" atIndex:0];
                }
                else{
                    if(![[newString substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"-"])
                    {
                        [newString insertString:@"-" atIndex:1];
                    }
                }
                
                if(newString.length > 5 && ![[newString substringWithRange:NSMakeRange(5, 1)] isEqualToString:@"-"])
                {
                    [newString insertString:@"-" atIndex:5];
                }
                
                if(newString.length > 9 && ![[newString substringWithRange:NSMakeRange(9, 1)] isEqualToString:@"-"])
                {
                    [newString insertString:@"-" atIndex:9];
                }
                
                textField.text = (newString.length > 14) ? [newString substringWithRange:NSMakeRange(0, 14)] : newString;
            }
            */
        }
        
        if(textField == self.emailText)
        {
            textField.text = (resultText.length > 100)?[resultText substringWithRange:NSMakeRange(0, 100)] : resultText;
        }
        if(textField == self.zipCodeText)
        {
            textField.text = (resultText.length > 10)?[resultText substringWithRange:NSMakeRange(0, 10)] : resultText;
        }
        
        return NO;
    }
    
    if(textField == self.firstNameText || textField == self.lastNameText)
    {
        if(textField.text.length == 19 && ![string isEqualToString:@""])
        {
            return NO;
        }
        return YES;
    }
    if(textField == self.passwordText || textField == self.confirmPasswordText)
    {
        if(textField.text.length == 25 && ![string isEqualToString:@""])
        {
            return NO;
        }
        return YES;
    }
    if (textField == self.numberCardText) {
        if (textField.text.length > 5 && ![string isEqualToString:@""]) {
            return NO;
        }
        return YES;
    }
    if (textField == self.zipCodeText) {
        if (textField.text.length > 9 && ![string isEqualToString:@""]) {
            return NO;
        }
        return YES;
    }
    else if(textField == self.phoneNumberText)
    {
        if (textField.text.length > 20 && ![string isEqualToString:@""]) {
            return NO;
        }
        /*
        if(textField.text.length == 14 && ![string isEqualToString:@""])
        {
            return NO;
        }
        
        if(range.location < 2 && [string isEqualToString:@""])
        {
            return NO;
        }
        
        if(range.location < textField.text.length)
        {
            textField.text = [self reformatForText:textField WithNewString:string WithRange:range];
            NSRange newRange;
            if([string isEqualToString:@""])
            {
                newRange = (textField.text.length < range.location) ? NSMakeRange(textField.text.length, 0) : NSMakeRange(range.location, 0);
            }
            else
            {
                newRange = (range.location == 5 || range.location == 9) ? NSMakeRange(range.location + 2, 0) : NSMakeRange(range.location + 1, 0);
                
            }
            [textField updateCursorPositionAtRange:newRange];
            return NO;
        }
        
        if(textField.text.length == 5 && ![string isEqualToString:@""])
        {
            textField.text = [NSString stringWithFormat:@"%@-%@",textField.text,string];
            return NO;
        }
        
        if(textField.text.length == 7 && [string isEqualToString:@""])
        {
            textField.text = [textField.text substringToIndex:5];
            return NO;
        }
        
        if(textField.text.length == 9 && ![string isEqualToString:@""])
        {
            textField.text = [NSString stringWithFormat:@"%@-%@",textField.text,string];
            return NO;
        }
        
        if(textField.text.length == 11 && [string isEqualToString:@""])
        {
            textField.text = [textField.text substringToIndex:9];
            return NO;
        }
         */
        
        return YES;
    }
    else if(textField == self.emailText)
    {
        if(textField.text.length == 100 && ![string isEqualToString:@""])
        {
            return NO;
        }
        
        if([textField.text occurrenceCountOfCharacter:'@'] == 1 && [string isEqualToString:@"@"]){
            return NO;
        }
        
        return YES;
    }
    
    return YES;
}
- (void)WSBaseNetworkUnavailable{
    [self stopActivityIndicator];
}

#pragma mark LOGICAL FUNCTION

-(NSString *)reformatForText:(UITextField *)textField WithNewString:(NSString *)newString WithRange:(NSRange)range
{
    NSMutableString *mutableString = [[NSMutableString alloc] initWithString:textField.text];
    NSRange newRange = range;
    if([newString isEqualToString:@""] && (range.location == 5 ||  range.location == 9))
    {
        newRange = NSMakeRange(range.location - 1, 1);
    }
    [mutableString replaceCharactersInRange:newRange withString:newString];
    mutableString = [[NSMutableString alloc] initWithString:[mutableString stringByReplacingOccurrencesOfString:@"-" withString:@""]];
    
    if(mutableString.length >= 1 && textField.tag == PHONE_NUMBER_TEXTFIELD_TAG)
    {
        [mutableString insertString:@"-" atIndex:1];
    }
    
    if(mutableString.length > 5)
    {
        [mutableString insertString:@"-" atIndex:5];
    }
    
    if(mutableString.length > 9 && textField.tag == PHONE_NUMBER_TEXTFIELD_TAG)
    {
        [mutableString insertString:@"-" atIndex:9];
    }
    
    return mutableString;
}
#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{

    CGPoint touchPoint = [touch locationInView:touch.view];
    
    UIView *viewTouch = touch.view.superview;
    if ([viewTouch isKindOfClass:[UITableViewCell class]]) {
        return NO;
    }else {
        if ([self isKindOfClass:[MyProfileViewController class]]) {
            for (UIView* view in touch.view.subviews) {
                if ((UITextField*)view == self.emailText && CGRectContainsPoint(self.emailText.frame, touchPoint)) {
                    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo]; 
                    if (profileDictionary) {
                        self.emailText.text = [profileDictionary objectForKey:keyEmail];
                        break;
                    }
                }else{
                    if([self.emailText.text containsString:@"@"])
                    {
                        self.emailText.text = [self.emailText.text createMaskForText:YES];
                    }
                }
            }
        }
        
    }
    
    return YES;
    
}

#pragma mark - DropDownViewDelegate
- (void)didSelectItem:(DropDownView *)dropDownView atIndex:(int)index{
    if (dropDownView == self.countryCodeDropDown) {
        Country *country = [countries objectAtIndex:index];
        self.countryCodeDropDown.title = [NSString stringWithFormat:@"%@ (%@)",country.countryCode, country.countryNumber];
        self.countryCodeDropDown.titleOther = country.countryDescription;
    }
    [self changeValueDropDown];
}
- (void)didShow:(DropDownView *)dropDownView {
    [self.view endEditing:YES];
    if (dropDownView == self.salutationDropDown) {
        [self.countryCodeDropDown didTapBackground];
    }else if (dropDownView == self.countryCodeDropDown) {
        [self.salutationDropDown didTapBackground];
    }
}

- (void)didHiden:(DropDownView *)dropDownView {

}

- (void)changeValueDropDown{
    
}
@end
