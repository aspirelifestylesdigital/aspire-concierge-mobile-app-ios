//
//  GalleryDetailViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/8/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "GalleryDetailViewController.h"
#import "Common.h"
#import "Constant.h"
#import "UIView+Extension.h"
#import "SWRevealViewController.h"
#import "NSString+Utis.h"
#import "UIImageView+AFNetworking.h"
#import "UILabel+Extension.h"

@interface GalleryDetailViewController ()

@end

@implementation GalleryDetailViewController

- (void)viewDidLoad {
    isNotChangeNavigationBarColor = YES;
    [super viewDidLoad];
    
    self.imageTopConstraint.constant = self.imageTopConstraint.constant * SCREEN_SCALE_BY_HEIGHT;
    self.titleLabelTopConstraint.constant = self.titleLabelTopConstraint.constant * SCREEN_SCALE;
    self.descriptionLabelTopConstraint.constant = self.descriptionLabelTopConstraint.constant * SCREEN_SCALE;
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(displayFrontView:)];
    [self.panToMenuView addGestureRecognizer:panGesture];
    [self.panToMenuView setBackgroundColor:[UIColor clearColor]];

}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


-(void)initView
{
    self.galleryDescription.textColor = colorFromHexString(@"#99A1A8");
    [self.galleryDescription setFont:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:16.0 * SCREEN_SCALE]];
    
    NSDictionary *attributes = @{NSForegroundColorAttributeName: colorFromHexString(@"#272726"),
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_BOLD size:(16.0 * SCREEN_SCALE)],
                                 NSKernAttributeName: @1.8};
    self.galleryTitle.attributedText = [[NSAttributedString alloc] initWithString:self.galleryTitle.text.uppercaseString attributes:attributes];
    self.galleryTitle.numberOfLines = 0;
    self.galleryTitle.textAlignment = NSTextAlignmentCenter;
    [self.view setBackgroundColor:colorFromHexString(self.colorHex)];
    [self.galleryDescription setLineSpacing:2.7];
}


-(void)initData
{
    self.galleryTitle.text = [self.titleText uppercaseString];
    self.galleryDescription.text = self.descriptionText;
    [self.galleryImageView setImageWithURL:[NSURL URLWithString:[self.imageURL removeBlankFromURL]] placeholderImage:[UIImage imageNamed:@"gallery_placeholder"]];
    [self.galleryDescription setLineSpacing:2.7];
}


-(void)displayFrontView:(UIPanGestureRecognizer*)panGesture

{
    if([self.delegate respondsToSelector:@selector(getMenuView:)])
    {
        [self.delegate getMenuView:panGesture];
    }
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if([self.delegate respondsToSelector:@selector(setColorForPage:)])
    {
        [self.delegate setColorForPage:self.index];
    }
    
    if(self.index == 0 && [self.delegate respondsToSelector:@selector(updatePageControlPosition:)])
    {
        [self.view layoutIfNeeded];
        [self.delegate updatePageControlPosition:self.dummyViewForPageControl.frame.origin.y];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
