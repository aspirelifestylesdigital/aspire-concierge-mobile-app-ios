//
//  VenueViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/11/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "VenueViewController.h"
#import "VenueTableViewCell.h"
#import "Constant.h"
#import "UIButton+Extension.h"

@interface VenueViewController ()<UITableViewDelegate, UITableViewDataSource>

@end

@implementation VenueViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.tableView registerNib:[UINib nibWithNibName:@"VenueTableViewCell" bundle:nil] forCellReuseIdentifier:@"VenueTableViewCell"];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100.f;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VenueTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VenueTableViewCell"];
    
    cell.venueImg.image = [UIImage imageNamed:@"venue_image_sample"];
    cell.nameLbl.text = @"21 Club";
    cell.addressLbl.text = @"21 W. 52nd St. New York, NY 10019";    
    [cell.toWebBnt setUnderlineForTextWithDefaultColorForNormalStatus:@"www.21club.com"];
    [cell.toWebBnt setUnderlineForTextWithCustomizedColorForTouchingStatus:@"www.21club.com"];
    [cell.seeMapBtn setUnderlineForTextWithDefaultColorForNormalStatus:@"see map"];
    [cell.seeMapBtn setUnderlineForTextWithCustomizedColorForTouchingStatus:@"see map"];
    
    cell.offerTextLbl.text = @"";
    cell.venueDescriptionLbl.text = @"The world famous Orient Express in the heart of Manhattan offers a superb seasonal American menu and an award-winning wine list. A-listers, dignitaries, and world leaders can be seen rubbing elbows at either of hte two inhouse restuarants: the famous Bar Room and the romantic upstairs 21. The world famous Orient Express in the heart of Manhattan offers a superb seasonal American menu and an award-winning wine list. A-listers, dignitaries, and world leaders can be seen rubbing elbows at either of hte two inhouse restuarants: the famous Bar Room and the romantic upstairs 21.";
    cell.personalRecommendLbl.text = @"Jean, can I recommend a great place for a craft cocktail near here?";
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
