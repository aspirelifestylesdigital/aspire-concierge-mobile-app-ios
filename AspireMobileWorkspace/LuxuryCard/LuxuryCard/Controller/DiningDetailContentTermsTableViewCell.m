//
//  DiningDetailContentTermsTableViewCell.m
//  MobileConcierge
//
//  Created by user on 6/8/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "DiningDetailContentTermsTableViewCell.h"
#import "UILabel+Extension.h"
#import "NSString+Utis.h"

@implementation DiningDetailContentTermsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.leadingConstraint.constant = self.leadingConstraint.constant*SCREEN_SCALE;
    self.webView.scrollView.scrollEnabled = NO;
    self.webView.scrollView.contentInset = UIEdgeInsetsMake(0, -8, 0, 0);
    self.webView.opaque = NO;
     self.webView.tintColor = colorFromHexString(@"#011627");
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)loadData{
    if (!self.webView.delegate ) {
        
        self.webView.delegate = self;
        
        self.cellDescription = [self.cellDescription removeRedudantNewLineInText];
        if(self.cellDescription.length > 0)
        {
            self.cellDescription = [self.cellDescription stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; color:#011627;font-size:%fpx;line-height: 22px;}</style>",FONT_MarkForMC_LIGHT, 18.0f*SCREEN_SCALE]];
        }
        
        [self.webView loadHTMLString:self.cellDescription baseURL:nil];
    }
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if (navigationType == UIWebViewNavigationTypeLinkClicked ) {
        [self.diningDetailContentTermsTableViewCellDelegate gotoTermsAndConditionsWebBrowser:webView shouldStartLoadWithRequest:request navigationType:navigationType];
        return NO;
    }
    
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if(self.cellDescription.length > 0)
    {
        CGRect frame = self.webView.frame;
        frame.size.height = 1;
        self.webView.frame = frame;
        CGSize fittingSize = [self.webView sizeThatFits:CGSizeZero];
        frame.size = fittingSize;
        self.webView.frame = frame;
        
        self.heightWebView.constant = fittingSize.height;
        [self.diningDetailContentTermsTableViewCellDelegate reloadTermsCell:self andHeight:fittingSize.height];
        
    }
    else{
        [self.lineView setBackgroundColor:[UIColor clearColor]];
        self.titleLbl.hidden = YES;
        self.heightTitleLbl.constant = 0.01f;
        self.titleTopConstrain.constant = 0.01f;
        
        CGRect frame = self.webView.frame;
        frame.size.height = 1;
        self.webView.frame = frame;
        
        self.heightWebView.constant = 1;
        [self.diningDetailContentTermsTableViewCellDelegate reloadTermsCell:self andHeight:1];
    }
}

@end
