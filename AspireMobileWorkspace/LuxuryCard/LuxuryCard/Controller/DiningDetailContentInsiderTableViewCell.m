//
//  DiningDetailContentInsiderTableViewCell.m
//  MobileConcierge
//
//  Created by user on 6/8/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "DiningDetailContentInsiderTableViewCell.h"
#import "UILabel+Extension.h"

@implementation DiningDetailContentInsiderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.leadingConstraint.constant = self.leadingConstraint.constant*SCREEN_SCALE;
    [self.lblInsiderTips setFont:[UIFont fontWithName:FONT_MarkForMC_MED size:18]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)loadData{
    if ([self.insiderTip containsString:@"<"]) {
        self.lblInsiderTips.attributedText = [self.lblInsiderTips handleHTMLString:self.insiderTip];
    }else{
        self.lblInsiderTips.text = self.insiderTip;
    }
    
}

@end
