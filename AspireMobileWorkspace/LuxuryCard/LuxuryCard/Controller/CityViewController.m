//
//  CityViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "CityViewController.h"
#import "CityItem.h"
#import "CityTableViewCell.h"
#import "AppData.h"
#import "SWRevealViewController.h"
#import "UtilStyle.h"
#import "HomeViewController.h"
#import "ExploreViewController.h"

#define CENTER_TAG 1
#define LEFT_PANEL_TAG 2
#define RIGHT_PANEL_TAG 3

#define CORNER_RADIUS 4

#define SLIDE_TIMING .25
#define PANEL_WIDTH 60

@interface CityViewController ()<UITableViewDelegate, UITableViewDataSource,SWRevealViewControllerDelegate>
{
    BOOL isFirstSelectCity;
}

@end

@implementation CityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.separatorColor = [UtilStyle colorForSeparateLine];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"city_title", nil)];
    

    NSInteger index = self.currentCity ?  [self.cityLst indexOfObject:self.currentCity] : 0;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    if(index > self.cityLst.count - 3){
        [self.view layoutIfNeeded];
        CGPoint offset = CGPointMake(0, self.tableView.contentSize.height);
        [self.tableView setContentOffset:offset animated:YES];
    }
    else{
        [self.tableView scrollToRowAtIndexPath:indexPath
                              atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
}


-(void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    // Stop being the navigation controller's delegate
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //  Set up Pop pan gesture
//    [self setUpCustomizedPanGesturePopRecognizer];
    if(self.navigationController.viewControllers.count == 1)
    {
        isFirstSelectCity = YES;
        HomeViewController *homeView = [[HomeViewController alloc] init];
        [self.navigationController setViewControllers:@[homeView,self]];
    }
    else{
        UIViewController *vc = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count - 2];
        if(![vc isKindOfClass:[ExploreViewController class]])
        {
            isFirstSelectCity = YES;
        }
    }
}


-(void)initView
{
    [self.tableView registerNib:[UINib nibWithNibName:@"CityTableViewCell" bundle:nil] forCellReuseIdentifier:@"CityTableViewCell"];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100.f;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self backNavigationItem];
    self.navigationItem.title = NSLocalizedString(@"city_title", nil);
}

-(void)initData
{
    self.cityLst = [AppData getSelectionCityList];
}

#pragma mark TABLEVIEW DELEGATE
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.cityLst.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CityTableViewCell"];
    [cell initCellWithData:[self.cityLst objectAtIndex:indexPath.row]];
//    if()
        [cell setHiddenBottomLine:indexPath.row == self.cityLst.count-1];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CityTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setBackgroundColor:[UtilStyle colorForSeparateLine]];
    CityItem *currentCity = [self.cityLst objectAtIndex:indexPath.row];
    
    if(isFirstSelectCity)
    {
        ExploreViewController *exploreViewController = [[ExploreViewController alloc] init];
        exploreViewController.currentCity = currentCity;
        [AppData setCurrentCityWithCode:currentCity.name];
         [self.navigationController setViewControllers:@[exploreViewController,self]];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    if(self.delegate)
        [self.delegate getDataBaseOnCity:currentCity];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}
@end
