//
//  AskConciergeConfirmationViewController.m
//  MobileConcierge
//
//  Created by Mac OS on 5/29/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "AskConciergeConfirmationViewController.h"
#import "UIButton+Extension.h"
#import "Constant.h"
#import "HomeViewController.h"
#import "SWRevealViewController.h"
#import "ExploreViewController.h"
#import "AppData.h"
#import "CityViewController.h"
#import "Common.h"
#import "MyRequestPagerViewController.h"


@interface AskConciergeConfirmationViewController ()

@property UIViewController *askConciergeVC;

@end

@implementation AskConciergeConfirmationViewController

- (void)viewDidLoad {
    isNotAskConciergeBarButton = YES;
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   
}

- (void)viewDidAppear:(BOOL)animated{
     [self setUpCustomizedPanGesturePopRecognizer];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}

-(void)initView{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadMyRequest" object:nil];
    self.askConciergeVC = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    if (self.isUpdatedRequest) {
        [navigationArray removeObjectAtIndex: self.navigationController.viewControllers.count-2];
        [navigationArray removeObjectAtIndex:self.navigationController.viewControllers.count-3];
    } else {
        [navigationArray removeObjectAtIndex: self.navigationController.viewControllers.count-2];  // You can pass your index here
    }
    self.navigationController.viewControllers = navigationArray;
    
    
    
    
    NSDictionary *attributes = @{NSForegroundColorAttributeName:
                                     colorFromHexString(@"#011627"),
                                 
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_BOLD size:(18.0f * SCREEN_SCALE)],
                                 
                                 NSKernAttributeName: @1.8};
    
    NSDictionary *attributesSelected = @{NSForegroundColorAttributeName:
                                             colorFromHexString(BUTTON_BG_COLOR_HIGHLIGHT),
                                         
                                         NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_BOLD size:(18.0f * SCREEN_SCALE)],
                                         
                                         NSKernAttributeName: @1.8};
    
    [self.btnExplore setAttributedTitle:[[NSAttributedString alloc] initWithString:@"CONTINUE TO EXPLORE" attributes:attributes]  forState:UIControlStateNormal];
    [self.btnExplore setAttributedTitle:[[NSAttributedString alloc] initWithString:@"CONTINUE TO EXPLORE" attributes:attributesSelected]  forState:UIControlStateHighlighted];
    
    
    [self.btnAnotherRequest setAttributedTitle:[[NSAttributedString alloc] initWithString:@"PLACE ANOTHER REQUEST" attributes:attributes]  forState:UIControlStateNormal];
    [self.btnAnotherRequest setAttributedTitle:[[NSAttributedString alloc] initWithString:@"PLACE ANOTHER REQUEST" attributes:attributesSelected]  forState:UIControlStateHighlighted];
    
    [self.btnExplore addTarget:self action:@selector(touchExplore:) forControlEvents:UIControlEventTouchUpOutside];
    [self.btnAnotherRequest addTarget:self action:@selector(touchAnotherRequest:) forControlEvents:UIControlEventTouchUpOutside];
    
    self.topConstraintHeaderView.constant = self.topConstraintHeaderView.constant*SCREEN_SCALE;
    self.topConstraintThanksView.constant = self.topConstraintThanksView.constant*SCREEN_SCALE;
    
    self.bottomConstraintExploreView.constant = self.bottomConstraintExploreView.constant*SCREEN_SCALE;
    self.heightConstraintHeaderView.constant = self.heightConstraintHeaderView.constant*SCREEN_SCALE;
    
    [self createBackBarButton];
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"request_confirmation_title",nil)];
    
    
    NSString *greetMsg = NSLocalizedString(@"request_confirmation_message", nil);
    NSRange range = NSMakeRange(0, greetMsg.length);
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:greetMsg];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:20.0*SCREEN_SCALE] range:range];
    [attributeString addAttribute:NSForegroundColorAttributeName value:colorFromHexString(@"#011627") range:range];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init] ;
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    paragraphStyle.lineSpacing = 1.4;
    [attributeString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    
    self.lblMessage.attributedText = attributeString;
}

- (void)touchBack{
//     [self.navigationController popViewControllerAnimated:YES];
    UIViewController *View = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    [self.navigationController popToViewController:View animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)touchAnotherRequest:(id)sender {
    self.viewOverlay.alpha = 0.0f;
    
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    [navigationArray insertObject:_askConciergeVC atIndex:self.navigationController.viewControllers.count-1];
    if (self.isUpdatedRequest) {
        UIViewController *vc = [navigationArray objectAtIndex:1];
        if ([vc isKindOfClass:[MyRequestPagerViewController class]]) {
            MyRequestPagerViewController * requestVC =(MyRequestPagerViewController*)vc;
            requestVC.isReaload = true;
        }
    }
    self.navigationController.viewControllers = navigationArray;
    
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)touchExplore:(id)sender {
    self.viewOverlay.alpha = 0.0f;
    UIViewController *newFrontController = nil;
    CityItem *selectedCity = [AppData getSelectedCity];
    if(selectedCity)
    {
        ExploreViewController *exploreViewController = [[ExploreViewController alloc] init];
        exploreViewController.currentCity = selectedCity;
        newFrontController = exploreViewController;
//        int a = 0;
//        for (UIViewController *View in self.navigationController.viewControllers) {
//            if([View isKindOfClass:[ExploreViewController class]]) {
//                [self.navigationController popToViewController:View animated:YES];
//                break;
//            }
//            a++;
//        }
    }
    else
    {
        newFrontController = [[CityViewController alloc] init];
    }
    [self.navigationController pushViewController:newFrontController animated:YES];
}
- (void)WSBaseNetworkUnavailable{
    [self stopActivityIndicator];
}

@end
