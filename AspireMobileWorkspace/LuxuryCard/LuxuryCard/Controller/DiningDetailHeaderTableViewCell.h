//
//  DiningDetailHeaderTableViewCell.h
//  MobileConcierge
//
//  Created by user on 6/7/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "BaseTableViewCell.h"

@class DiningDetailHeaderTableViewCell;
@protocol DiningDetailHeaderTableViewCellDelegate <NSObject>

- (void) shareUrl:(NSArray *)data;
- (void) touchAskConcierge;

@end
@interface DiningDetailHeaderTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImg;

- (IBAction)touchAskConcierge:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnAskConcierge;

@property (weak, nonatomic) IBOutlet UIButton *btnShare;
- (IBAction)touchShare:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblQuestion;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topButtonsConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topQuestionConstraint;
@property (strong, nonatomic) id<DiningDetailHeaderTableViewCellDelegate> diningDetailHeaderTableViewCellDelegate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightImageView;

// Input data
@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic, strong) NSString *cityName;
@property (nonatomic, strong) NSString *questionName;
@property (nonatomic, strong) NSString *shareLink;


@end
