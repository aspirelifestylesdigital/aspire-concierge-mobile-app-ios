//
//  DiningDetailContentHoursTableViewCell.m
//  MobileConcierge
//
//  Created by user on 6/8/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "DiningDetailContentHoursTableViewCell.h"
#import "UILabel+Extension.h"

@interface DiningDetailContentHoursTableViewCell(){
    
    IBOutletCollection(UILabel) NSArray *listLabelPins;
}

@end

@implementation DiningDetailContentHoursTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.leadingConstraint.constant = self.leadingConstraint.constant*SCREEN_SCALE;
    [self.lblHour setFont:[UIFont fontWithName:FONT_MarkForMC_LIGHT size:18]];
    
    for (UILabel* label in listLabelPins) {
        label.attributedText = [[NSAttributedString alloc] initWithString:label.text attributes:@{NSKernAttributeName:@1.0}];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)loadData{
    if ([self.lblHour.text isEqualToString:@""]) {
        self.lblHour.attributedText = [self.lblHour handleHTMLString:self.workingHour];
    }
}

@end
