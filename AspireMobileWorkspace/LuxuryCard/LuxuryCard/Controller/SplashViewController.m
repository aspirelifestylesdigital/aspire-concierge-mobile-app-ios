//
//  SplashViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/5/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "SplashViewController.h"
#import "SWRevealViewController.h"
#import "SignInViewController.h"
#import "Constant.h"
#import "EnumConstant.h"
#import "HomeViewController.h"
#import "UIView+Extension.h"
#import "AppData.h"
#import "PrivacyPolicyViewController.h"
#import "CreateProfileViewController.h"
#import "Common.h"
#import "WSB2CGetRequestToken.h"
#import "WSB2CGetAccessToken.h"
#import "WSB2CGetUserDetails.h"
#import "WSB2CGetPolicyInfo.h"
#import "WSB2CVerifyBIN.h"
#import "PolicyInfoItem.h"
#import "BINItem.h"

@interface SplashViewController () <DataLoadDelegate>

@end

@implementation SplashViewController
{
    NSInteger currentTask;
    BOOL isLoadPolicy;
    WSB2CGetRequestToken *wsRequestToken;
    WSB2CGetAccessToken *wsAccessToken;
    WSB2CGetUserDetails *wsGetUser;
    WSB2CGetPolicyInfo *wsPolicy;
    
    NSNumber *currentPolicy;
    dispatch_group_t group;
    dispatch_queue_t queue;
    float policyVersion;
}


- (void)viewDidLoad {
    isIgnoreScaleView = YES;
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.view layoutIfNeeded];
    
    [self CheckBINNumber];
    [self getUUID];
    
    group = dispatch_group_create();
    queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    dispatch_group_notify(group, queue, ^{
        NSLog(@"All tasks done");
    });
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getUUID{
    if (![[SessionData shareSessiondata] UUID]) {
        NSString *uuidString = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [[SessionData shareSessiondata] setUUID:uuidString];
    }
}

- (void)appWillEnterForeground:(NSNotification*) noti{
//    [self CheckBINNumber];
}

- (void) CheckBINNumber{
    
    if(!isNetworkAvailable())
    {
        [self showNetWorkingStatusArlet];
    }
    else
    {
        if (![AppData isCreatedProfile]) {
            [self navigationToCreateProfileViewController];
        }else {
            [self navigationToHomeViewController];
        }
        // VietVo: 12072017
        /*
        if(![[NSUserDefaults standardUserDefaults] stringForKey:VERIFIED_BIN] || ![AppData isCreatedProfile])
        {
            [self navigationToInitialBinViewController:NO];
        }
        else{
            WSB2CVerifyBIN *ws = [[WSB2CVerifyBIN alloc] init];
            ws.bin = [[[NSUserDefaults standardUserDefaults] stringForKey:VERIFIED_BIN] stringByReplacingOccurrencesOfString:@"-" withString:@""];
            ws.delegate = self;
            ws.task = WS_B2C_VERIFY_BIN;
            [ws verifyBIN];
        }*/
    }
}

- (void) loadPolicy{
    // load policy privacy
    wsPolicy = [[WSB2CGetPolicyInfo alloc] init];
    wsPolicy.delegate = self;
    isLoadPolicy = YES;
    [wsPolicy retrieveDataFromServer];
}

- (void) getRequestToken{
    wsRequestToken = [[WSB2CGetRequestToken alloc] init];
    wsRequestToken.delegate = self;
    currentTask = WS_GET_REQUEST_TOKEN;
    [wsRequestToken getRequestToken];
}


- (void)loadDataDoneFrom:(id<WSBaseProtocol>)ws{
    /*
    if([ws isKindOfClass:[WSB2CVerifyBIN class]])
    {
        WSB2CVerifyBIN *wsVerifyBIN = (WSB2CVerifyBIN *)ws;
        if(wsVerifyBIN.data.count > 0)
        {
            BINItem *binItem = (BINItem *)[wsVerifyBIN.data objectAtIndex:0];
            if(binItem.valid){
                [self getRequestToken];
                [self loadPolicy];
            }
            else{
                [self navigationToInitialBinViewController:YES];
            }
        }
    }
    else if (![ws isKindOfClass:[WSB2CGetPolicyInfo class]]) {
        if (currentTask == WS_GET_REQUEST_TOKEN) {
            wsAccessToken = [[WSB2CGetAccessToken alloc] init];
            wsAccessToken.delegate = self;
            [[SessionData shareSessiondata] setRequestToken:wsRequestToken.requestToken];
            currentTask = WS_GET_ACCESS_TOKEN;
            [wsAccessToken requestAccessToken:wsRequestToken.requestToken member:[[SessionData shareSessiondata] OnlineMemberID]];
            
        }else if(currentTask == WS_GET_ACCESS_TOKEN){
            // get user detail
            wsGetUser = [[WSB2CGetUserDetails alloc] init];
            wsGetUser.delegate = self;
            currentTask = WS_UPDATE_USER;
            [wsGetUser getUserDetails];
        }else if (currentTask == WS_UPDATE_USER){
            NSLog(@"Done Update User: CurrentPolicyVersion %@", [[SessionData shareSessiondata] CurrentPolicyVersion]);
            if (![((PolicyInfoItem *)[wsPolicy.data objectAtIndex:0]).CurrentVersion isEqualToString:[[SessionData shareSessiondata] CurrentPolicyVersion]]) {
                [self navigationToPrivacyPolicyViewController];
            }else{
                [self navigationToHomeViewController];
            }
        }
    }*/
}

- (void)loadDataFailFrom:(id<BaseResponseObjectProtocol>)result withErrorCode:(NSInteger)errorCode{
    NSLog(@"faillllllllll lllll");
}

- (void)loadDataFailFrom:(id<WSBaseProtocol>)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);;
    alert.msgAlert = message;
    alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        alert.seconBtn.hidden = YES;
        alert.midView.alpha = 0.0f;;
    });
    
    [self showAlert:alert forNavigation:NO];
}


- (void) showNetWorkingStatusArlet{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = nil;
    alert.msgAlert = NSLocalizedString(@"no_network_connection", nil);
    alert.firstBtnTitle = NSLocalizedString(@"arlet_retry_button", nil);
    alert.secondBtnTitle = NSLocalizedString(@"arlet_cancel_button", nil);
    
    alert.secondBtnTitle = NSLocalizedString(@"home_button_title", nil);
    alert.blockFirstBtnAction = ^(void){
       [self CheckBINNumber];
    };
    
    [self showAlert:alert forNavigation:NO];
}

-(void)navigationToInitialBinViewController:(BOOL)invalidBIN
{
    SWRevealViewController *revealViewController = self.revealViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SignInViewController *signInViewController = [storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];
    signInViewController.isBINInvalid = invalidBIN;
    UINavigationController *fontViewController = [[UINavigationController alloc] initWithRootViewController:signInViewController];
    [revealViewController pushFrontViewController:fontViewController animated:NO];
}

-(void)navigationToHomeViewController
{
    SWRevealViewController *revealViewController = self.revealViewController;
    HomeViewController *homeViewConroller = [[HomeViewController alloc] init];
    UINavigationController *fontViewController = [[UINavigationController alloc] initWithRootViewController:homeViewConroller];
    [revealViewController pushFrontViewController:fontViewController animated:YES];
    
}

-(void)navigationToPrivacyPolicyViewController
{
    SWRevealViewController *revealViewController = self.revealViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PrivacyPolicyViewController *privacyPolicyViewController = [storyboard instantiateViewControllerWithIdentifier:@"PrivacyPolicyViewController"];
    privacyPolicyViewController.isRecheckPolicy = YES;
    NSString *str = ((PolicyInfoItem *)[wsPolicy.data objectAtIndex:0]).textInfo;
    privacyPolicyViewController.policyText = str;
    NSLog(@"return policy version: %@", ((PolicyInfoItem *)[wsPolicy.data objectAtIndex:0]).CurrentVersion);
    privacyPolicyViewController.policyVersion = ((PolicyInfoItem *)[wsPolicy.data objectAtIndex:0]).CurrentVersion;
    [revealViewController pushFrontViewController:privacyPolicyViewController animated:YES];
}

-(void)navigationToCreateProfileViewController
{
    SWRevealViewController *revealViewController = self.revealViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CreateProfileViewController *createProfileViewController = [storyboard instantiateViewControllerWithIdentifier:@"CreateProfileViewController"];
    [revealViewController pushFrontViewController:createProfileViewController animated:YES];
}
- (void) crashApp{
    //[self performSelector:@selector(die_die)];
}



@end
