//
//  TermsOfUseViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/16/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "TermsOfUseViewController.h"
#import "WSB2CGetTermsOfUse.h"
#import "TermsOfUseInfoItem.h"

#import "Constant.h"

@interface TermsOfUseViewController () <DataLoadDelegate, UIScrollViewDelegate, UIWebViewDelegate>
{
    UIBarButtonItem *backButtonItem;
    NSString *content;
    float textHeight;
}
@end

@implementation TermsOfUseViewController

- (void)viewDidLoad {
    isNotAskConciergeBarButton = YES;
    [super viewDidLoad];
    [self createBackBarButtonWithIconName:@"clear_icon"];
    [self setNavigationBarWithDefaultColorAndTitle:@"Terms Of Use"];
    // Do any additional setup after loading the view from its nib.
    
    self.webView.delegate = self;
    self.webView.scrollView.delegate = self;
    [self.webView setBackgroundColor:[UIColor clearColor]];
    [self.webView setOpaque:NO];

    backButtonItem = self.navigationItem.leftBarButtonItem;
    backButtonItem.enabled = YES;
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //  Set up Pop pan gesture
    [self setUpCustomizedPanGesturePopRecognizer];
}

- (void)handlePopRecognizer:(UIPanGestureRecognizer *)recognizer{
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchBack
{
    /*
    if([self.delegate respondsToSelector:@selector(enableSubmitBINButton:)])
    {
        [self.delegate enableSubmitBINButton:@"TermsOfUseViewController"];
    }
     */
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)initData
{
    [self startActivityIndicator];
    WSB2CGetTermsOfUse *ws = [[WSB2CGetTermsOfUse alloc] init];
    ws.delegate = self;
    [ws retrieveDataFromServer];
}

- (void)getContentHeight{
    UITextView *tempTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, self.webView.frame.size.width, self.webView.frame.size.height)];
    
    tempTextView.attributedText = [[NSAttributedString alloc]
                                   initWithData: [content dataUsingEncoding:NSUnicodeStringEncoding]
                                   options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                   documentAttributes: nil
                                   error: nil
                                   ];
    textHeight = tempTextView.contentSize.height;
}


#pragma mark DATALOADDELEGATE


-(void)loadDataDoneFrom:(WSB2CGetTermsOfUse *)wsTermOfUse
{
    if(wsTermOfUse.data.count > 0)
    {
        
        content = [self replaceWebLinkInString:((TermsOfUseInfoItem *)[wsTermOfUse.data objectAtIndex:0]).textInfo];
        content = [content stringByReplacingOccurrencesOfString:@"http://mailto:" withString:@"mailto:"];
        [self getContentHeight];
        
        NSRange bodyHtmlRange = [content rangeOfString:@"font-family"];
        if (bodyHtmlRange.location == NSNotFound) {
            [self.webView loadHTMLString:centerHTMLString(content, FONT_MarkForMC_REGULAR, (16.0 * SCREEN_SCALE), @"#FFFFFF") baseURL:nil];
        }else{
            [self.webView loadHTMLString:content baseURL:nil];
        }
    }
    else{
        [self stopActivityIndicator];
    }
}

- (NSString*) replaceWebLinkInString:(NSString*)string{
    
    NSArray *tempArr = getMatchesFromString(string,@"href+=+\"(?!#+)(.*?)+\"",NSRegularExpressionCaseInsensitive);
    
    NSMutableArray* vvv = [NSMutableArray new];
    for (NSString* test in tempArr) {
        NSString* t = strimStringFrom(test,@[@"href=|\"?"]);
        if([t containsString:@"http"]) {
            [vvv addObject:@{test:[[@" style=\"word-break:break-all !important; white-space:pre-line\" href=" stringByAppendingString:t] stringByAppendingString:@"\""]}];
        }
    }
    NSMutableString* tttt = [NSMutableString stringWithString:string];
    for (NSDictionary* t in vvv) {
        if([string containsString:[[t allKeys] firstObject]]) {
            string = [tttt stringByReplacingOccurrencesOfString:[[t allKeys] firstObject] withString:[t objectForKey:[[t allKeys] firstObject]]];
        }
    }
    return string;
}

-(void)loadDataFailFrom:(id<BaseResponseObjectProtocol>) result withErrorCode:(NSInteger)errorCode
{
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }
}

-(void)loadDataFailFrom:(id<WSBaseProtocol>)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message
{
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }else{
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);;
        alert.msgAlert = message;
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
        });
        
        [self showAlert:alert forNavigation:YES];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height)
    {
        backButtonItem.enabled = YES;
    }
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self stopActivityIndicator];
 
    
    if(textHeight < self.webView.bounds.size.height)
    {
        backButtonItem.enabled = YES;
    }
    
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
#ifdef DEBUG
    NSLog(@"%@", request.URL);
#endif
    if (navigationType==UIWebViewNavigationTypeLinkClicked) {
        if ([request.URL.absoluteString containsString:@"http"]) {
            
            AlertViewController *alert = [[AlertViewController alloc] init];
            alert.titleAlert = NSLocalizedString(@"alert_title", nil);
            alert.msgAlert = NSLocalizedString(@"category_more_info_msg", nil);
            alert.firstBtnTitle = NSLocalizedString(@"yes_button", nil);
            alert.blockFirstBtnAction = ^(void){
                [[UIApplication sharedApplication] openURL:request.URL];
            };
            
            alert.blockSecondBtnAction  = ^(void){};
            [self showAlert:alert forNavigation:YES];
            // It was a link
            
            return NO;
        }
    }
    
    return YES;
}

- (void)WSBaseNetworkUnavailable{
    [self stopActivityIndicator];
}

@end
