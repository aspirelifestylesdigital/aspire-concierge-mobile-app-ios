//
//  DiningDetailContentInfoTableViewCell.m
//  MobileConcierge
//
//  Created by user on 6/8/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "DiningDetailContentInfoTableViewCell.h"
#import "Common.h"
#import "UILabel+Extension.h"

@implementation DiningDetailContentInfoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.leadingConstraint.constant = self.leadingConstraint.constant*SCREEN_SCALE;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)touchSeeMap:(id)sender {
    [self.diningDetailContentInfoTableViewCellDelegate seeDetailMapWithAddress:self.googleAddress];
}
- (IBAction)touchWebLink:(id)sender {
    [self.diningDetailContentInfoTableViewCellDelegate gotoWebBrowser];
}

- (void) loadData{
//    self.lblAddress1.text = self.address1;
    self.lblAddress1.attributedText = [self.lblAddress1 handleHTMLString:self.address1];
    self.lblAddress3.attributedText = [self.lblAddress3 handleHTMLString:self.address3];
    
    if ([self.webLink containsString:@"http"]) {
        // normal state
        [self.btnWebLink setAttributedTitle:[self attributeStringUnderlineWithColor:@"#011627" andText:self.webLink] forState:UIControlStateNormal];
        
        // selected state
        [self.btnWebLink setAttributedTitle:[self attributeStringUnderlineWithColor:@"#C54B53" andText:self.webLink] forState:UIControlStateHighlighted];
        
        self.btnWebLink.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.btnWebLink.titleLabel.numberOfLines = 2;//if you want unlimited number of lines put 0
    }else{
        self.btnWebLink.hidden = YES;
    }
    
    [self.btnSeeMap setAttributedTitle:[self attributeStringUnderlineWithColor:@"#011627" andText:@"See map"] forState:UIControlStateNormal];
    [self.btnSeeMap setAttributedTitle:[self attributeStringUnderlineWithColor:@"#C54B53" andText:@"See map"] forState:UIControlStateHighlighted];
}

- (NSAttributedString*)attributeStringUnderlineWithColor:(NSString*)hexColorString andText:(NSString*)text{
    
    NSArray * objects = [[NSArray alloc] initWithObjects:colorFromHexString(hexColorString), [NSNumber numberWithInt:NSUnderlineStyleSingle], nil];
    NSArray * keys = [[NSArray alloc] initWithObjects:NSForegroundColorAttributeName, NSUnderlineStyleAttributeName, nil];
    
    NSDictionary * linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    return  [[NSAttributedString alloc] initWithString:text attributes:linkAttributes];
}
@end
