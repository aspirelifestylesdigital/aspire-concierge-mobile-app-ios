//
//  MyRequestPagerViewController.h
//  LuxuryCard
//
//  Created by Nghia Dinh on 9/13/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "BaseViewController.h"
#import "EnumConstant.h"

@interface MyRequestPagerViewController : BaseViewController

@property BOOL isFullItem;
@property BOOL isLoadMore;
@property BOOL isReaload;


-(void)loadMoreData;

@end
