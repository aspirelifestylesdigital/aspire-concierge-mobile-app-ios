//
//  MyRequestDetailViewController.m
//  LuxuryCard
//
//  Created by Nghia Dinh on 9/8/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "MyRequestDetailViewController.h"
#import "WSRequestConcierge.h"
#import "UIButton+Extension.h"
#import "ConciergeDetailObject.h"
#import "AskConciergeRequest.h"
#import "AskConciergeViewController.h"
#import "WSB2CCreateConciergeCase.h"
#import "MyRequestPagerViewController.h"

@interface MyRequestDetailViewController () <DataLoadDelegate> {
    IBOutlet UITextView *contentTextView;
    IBOutlet UIButton *createButton;
    IBOutlet UIButton *changeButton;
    IBOutlet UIButton *cancelButton;
    IBOutlet UIView *createButtonView;
    IBOutlet UIView *changeButtonView;
    IBOutlet UIView *cancelButtonView;
    
    __weak IBOutlet UILabel *lbCloseDate;
    __weak IBOutlet UILabel *lbStatus;
    __weak IBOutlet UILabel *lbRequestName;
    __weak IBOutlet UILabel *lblClose;
    __weak IBOutlet UILabel *lbStatusTitle;
    
    WSRequestConcierge *wsRequestConcierge;
    ConciergeDetailObject *conciergeDetail ;
    WSB2CCreateConciergeCase *wSB2CCreateConciergeCase;
}

@end

@implementation MyRequestDetailViewController

- (void)viewDidLoad {
    isNotAskConciergeBarButton = YES;
    [super viewDidLoad];
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"my_request",nil)];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    [self backNavigationItem];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setUpCustomizedPanGesturePopRecognizer];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated{
    //[self setUpCustomizedPanGesturePopRecognizer];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    //[self removeSetupForCustomizedPanGesturePopRecognizer];
}


-(void)initView {
    contentTextView.textContainerInset = UIEdgeInsetsMake(18, 18, 18, 18);
    
    [createButton setBackgroundColorForNormalStatus];
    [createButton setBackgroundColorForTouchingStatus];
    [createButton configureDecorationForButton];
    
    [changeButton setBackgroundColorForNormalStatus];
    [changeButton setBackgroundColorForTouchingStatus];
    [changeButton configureDecorationForButton];
    
    [cancelButton setBackgroundColorForNormalStatus];
    [cancelButton setBackgroundColorForTouchingStatus];
    [cancelButton configureDecorationForButton];

    
    [createButton setTitle:NSLocalizedString(@"create_button", nil) forState:UIControlStateNormal];
    [changeButton setTitle:NSLocalizedString(@"change_button", nil) forState:UIControlStateNormal];
    [cancelButton setTitle:NSLocalizedString(@"cancel_button", nil) forState:UIControlStateNormal];
    
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:BUTTON_FONT_NAME_STANDARD size:(BUTTON_FONT_SIZE_STANDARD * SCREEN_SCALE)],
                                 NSKernAttributeName: @1.4};
    createButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:changeButton.titleLabel.text attributes:attributes];
    changeButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:changeButton.titleLabel.text attributes:attributes];
    cancelButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:cancelButton.titleLabel.text attributes:attributes];
    
    [createButtonView setBackgroundColor:colorFromHexString(DEFAULT_BACKGROUND_COLOR)];
    [changeButtonView setBackgroundColor:colorFromHexString(DEFAULT_BACKGROUND_COLOR)];
    [cancelButtonView setBackgroundColor:colorFromHexString(DEFAULT_BACKGROUND_COLOR)];
    
    

}

-(void)initData {

    wsRequestConcierge = [[WSRequestConcierge alloc] init];
    wsRequestConcierge.delegate = self;
    wsRequestConcierge.task = WS_GET_DETAIL_CONCIERGE;
    
    wSB2CCreateConciergeCase = [[WSB2CCreateConciergeCase alloc] init];
    wSB2CCreateConciergeCase.delegate = self;
    [self getData];
    
}

-(void)getData {
    if (self.transactionID) {
        [self startActivityIndicator];
        NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
//        [param setValue:self.epcCaseID forKey:@"EPCCaseID"];
        [param setValue:self.transactionID forKey:@"TransactionID"];
        [wsRequestConcierge getConciergeDetail:param];
    }
    
}

- (NSMutableAttributedString*) textViewLineSpacing:(NSString*)string{
    
    NSString *textViewText = string;
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:textViewText];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 10;
    NSDictionary *dict = @{NSParagraphStyleAttributeName : paragraphStyle , NSFontAttributeName : [UIFont fontWithName:FONT_MarkForMC_REGULAR size:(18.0f * SCREEN_SCALE)]};
    [attributedString addAttributes:dict range:NSMakeRange(0, [textViewText length])];
    return attributedString;
}
-(void) bindingDataAfterRequest{
    contentTextView.attributedText = [self textViewLineSpacing:[AskConciergeRequest buildAskConciergeTextWithConciergeData:conciergeDetail]];
    
    lbRequestName.text = conciergeDetail.requestTypeMapping;
    lbStatusTitle.text = NSLocalizedString(@"Status:", nil);
    
    if ([conciergeDetail.requestStatus isEqualToString:@"Close"] || [conciergeDetail.requestMode isEqualToString:@"CANCEL"]) {
        
        lbCloseDate.text = conciergeDetail.createdDate;
        createButtonView.hidden = YES;
        changeButtonView.hidden = YES;
        cancelButtonView.hidden = YES;
        lbStatus.text = @"Closed";
        lblClose.text = NSLocalizedString(@"Closed:", nil);
    }else{
        lbCloseDate.text = conciergeDetail.createdDate;
        createButtonView.hidden = NO;
        changeButtonView.hidden = NO;
        cancelButtonView.hidden = NO;
        lbStatus.text = conciergeDetail.requestStatus;
        lblClose.text = NSLocalizedString(@"Last Changed:", nil);
    }
}

-(void)reloadMyRequest {
//    UIViewController *vc = [self.navigationController.viewControllers objectAtIndex:1];
//    if ([vc isKindOfClass:[MyRequestPagerViewController class]]) {
//        MyRequestPagerViewController *requestVC = (MyRequestPagerViewController*)vc;
//        requestVC.isReaload = true;
//    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadMyRequest" object:nil];
    [self.navigationController popViewControllerAnimated:true];
}

#pragma mark - Data Load Delegate
-(void)loadDataDoneFrom:(WSBase*)ws {
    [self stopActivityIndicator];
    if (ws.task == WS_CANCEL_CONCIERGE) {
        [self reloadMyRequest];
    }else {
        if ([ws isKindOfClass:[WSB2CCreateConciergeCase class]]) {
            [self reloadMyRequest];
        }else{
            conciergeDetail = ws.data[0];
            [self bindingDataAfterRequest];
        }
    }
}
- (void)loadDataFailFrom:(id<BaseResponseObjectProtocol>)result withErrorCode:(NSInteger)errorCode{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self stopActivityIndicatorWithoutMask];
        if (errorCode == 1005 || errorCode == -1009) {
            [self showErrorNetwork];
        }else{
            AlertViewController *alert = [[AlertViewController alloc] init];
            alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);;
            alert.msgAlert = @"Failed to send request";
            alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                alert.seconBtn.hidden = YES;
                alert.midView.alpha = 0.0f;;
            });
            
            [self showAlert:alert forNavigation:YES];
        }
    });
}


- (void)loadDataFailFrom:(id<WSBaseProtocol>)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message{
    [self stopActivityIndicatorWithoutMask];
    if (errorCode == 1005 || errorCode == -1009) {
        [self showErrorNetwork];
    } else {
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);;
        alert.msgAlert = message;
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
        });
        [self showAlert:alert forNavigation:YES];
    }
}


- (void) showErrorNetwork {
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"cannot_get_data", nil);
    alert.msgAlert = NSLocalizedString(@"no_network_connection", nil);
    alert.firstBtnTitle = NSLocalizedString(@"settings_button", nil);
    alert.blockFirstBtnAction = ^(void){
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"prefs:root"]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root"]];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root"]];
        }
    };
    alert.secondBtnTitle = NSLocalizedString(@"alert_ok_button", nil);
    alert.providesPresentationContextTransitionStyle = YES;
    alert.definesPresentationContext = YES;
    [alert setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)cancelRequestAction:(id)sender {
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.msgAlert = NSLocalizedString(@"cancel_request_confirmation", nil);
    alert.firstBtnTitle = NSLocalizedString(@"yes_button_lowercase", nil);
    alert.blockFirstBtnAction = ^(void){
        [self startActivityIndicator];
        NSDictionary *dict = @{@"RequestDetails": conciergeDetail.requestDetail,
                               @"TransactionID":(conciergeDetail.transactionId ? conciergeDetail.transactionId : @""),
                               //@"requestType":self.categoryName,
                               //@"requestCity":self.cityName,
                               @"phone":@"false",
                               @"mail":@"true",
                               @"editType":CANCEL_EDIT_TYPE};
        [wSB2CCreateConciergeCase askConciergeWithMessage:dict];
    };
    alert.secondBtnTitle = NSLocalizedString(@"no_button_lowercase", nil);
    [self showAlert:alert forNavigation:YES];
}


-(IBAction)changeRequestAction:(id)sender
{
    AskConciergeViewController *askConciergeVC = [[AskConciergeViewController alloc] init];
    askConciergeVC.conciergeDetail = conciergeDetail;
    [self.navigationController pushViewController:askConciergeVC animated:NO];
}

-(IBAction)createRequestAction:(id)sender
{
    AskConciergeViewController *askConciergeVC = [[AskConciergeViewController alloc] init];
    ConciergeDetailObject *copyConconciergeDetail = [conciergeDetail copy];
    
    copyConconciergeDetail.transactionId = @"";
    copyConconciergeDetail.isCreated = NO;
    copyConconciergeDetail.createdDate = @"";
    copyConconciergeDetail.eventDate = @"";
    copyConconciergeDetail.pickUpDate = @"";
    copyConconciergeDetail.startDate = @"";
    copyConconciergeDetail.endDate = @"";
    copyConconciergeDetail.startDateWithoutTime = @"";
    copyConconciergeDetail.endDateWithoutTime = @"";
    askConciergeVC.conciergeDetail = copyConconciergeDetail;
    
    [self.navigationController pushViewController:askConciergeVC animated:NO];
}
@end
