//
//  MyRequestViewController.m
//  LuxuryCard
//
//  Created by Nghia Dinh on 9/8/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "MyRequestViewController.h"
#import "MyRequestTableViewCell.h"
#import "WSRequestConcierge.h"
#import "MyRequestDetailViewController.h"
#import "UITableView+Extensions.h"
#import "MenuListView.h"
#import "Common.h"



@interface MyRequestViewController () <UITableViewDelegate, UITableViewDataSource, MenuListViewProtocol> {
    
}
@property (weak, nonatomic) IBOutlet UITableView *myRequestTableView;



@end



@implementation MyRequestViewController
@synthesize requestType, requestArray, parrentVC;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTableView];
}




-(void)setupTableView {
    [self.myRequestTableView registerNib:[UINib nibWithNibName:@"MyRequestTableViewCell" bundle:nil] forCellReuseIdentifier:@"MyRequestTableViewCell"];
    self.myRequestTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.myRequestTableView.rowHeight = UITableViewAutomaticDimension;
    self.myRequestTableView.estimatedRowHeight = 100.f;
}

-(void)initData {
//    isFullItem = false;
//    requestPage = 0;
//    requestPerPage = 20;
//    isLoading = false;
//    openRequestArray = [[NSMutableArray alloc] init];
//    closedRequestArray = [[NSMutableArray alloc] init];
//    //requestType = OpenRequest;
//    wsRequestConcierge = [[WSRequestConcierge alloc] init];
//    wsRequestConcierge.delegate = self;
//    wsRequestConcierge.task = WS_GET_RECENT_CONCIERGE;
    //[self getData];
    
    requestArray = [[NSMutableArray alloc] init];
    
}


-(void)reloadData {
    NSLog(@"reloadData");
    [_myRequestTableView reloadData];
}


#pragma mark - UITableView DataSource & Delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  requestArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MyRequestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyRequestTableViewCell" forIndexPath:indexPath];
    [cell setupDataForCell:[requestArray objectAtIndex:indexPath.row]];
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(!isNetworkAvailable()) {
        [self showErrorNetwork];
    }else{
        ConciergeObject *object = [requestArray objectAtIndex:indexPath.row];
        NSDate *date = dateFromISO8601Time(object.createdDate);
        NSDate *currentDate = [[NSDate alloc] init];
        NSTimeInterval distanceBetweenDates = [currentDate timeIntervalSinceDate:date];
        if (distanceBetweenDates <= 5 * 60) {
            [tableView deselectRowAtIndexPath:indexPath animated:FALSE];
            if (requestType == OpenRequest) {
                return;
            }
        }
        self.parrentVC.navigationController.delegate = nil;
        MyRequestDetailViewController *vc = [[MyRequestDetailViewController alloc] init];
        vc.epcCaseID = object.ePCCaseID;
        vc.transactionID = object.transactionID;
        
        [self.navigationController pushViewController:vc animated:true];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!parrentVC.isFullItem) {
        if([self isLastRowVisible])
        {
            if(parrentVC.isLoadMore)
            {
                [self loadMoreIndicatorIcon:parrentVC.isLoadMore];
                return;
            } else {
                [parrentVC loadMoreData];
                //isLoading = true;
                //requestPage += 1;
                //[self getData];
            }
        }
        NSInteger lastRowIndex = [tableView numberOfRowsInSection:0] - 1;
        if ((indexPath.row == lastRowIndex))
        {
            [self loadMoreIndicatorIcon:isLoading];
        }
    }
}

-(void)loadMoreIndicatorIcon:(BOOL)loading
{
    if(loading)
    {
        [self.myRequestTableView startLoadMoreIndicator];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        self.myRequestTableView.contentInset = UIEdgeInsetsMake(0, 0, 30, 0);
        [UIView commitAnimations];;
    } else {
        [self.myRequestTableView removeLoadMoreIndicator];
        self.myRequestTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    }
}


-(BOOL)isLastRowVisible {
    NSArray *indexes = [self.myRequestTableView indexPathsForVisibleRows];
    // ignore this case
    if(indexes.count < 3)
        return YES;
    
    for (NSIndexPath *index in indexes) {
        if (index.row == requestArray.count - 1) {
            return YES;
        }
    }
    return NO;
}


@end
