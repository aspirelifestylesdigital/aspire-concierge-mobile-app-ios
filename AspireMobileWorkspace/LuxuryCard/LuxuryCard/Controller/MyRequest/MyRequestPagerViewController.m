//
//  MyRequestPagerViewController.m
//  LuxuryCard
//
//  Created by Nghia Dinh on 9/13/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "MyRequestPagerViewController.h"
#import "LJViewPager.h"
#import "MyRequestViewController.h"
#import "MenuListView.h"
#import "WSRequestConcierge.h"
#import "MyRequestDetailViewController.h"
#import "ConciergeObject.h"
#import "Constant.h"

@interface MyRequestPagerViewController () <LJViewPagerDelegate, LJViewPagerDataSource, MenuListViewProtocol, DataLoadDelegate> {
    MyRequestViewController *openRequestVC;
    MyRequestViewController *closeRequestVC;
    __weak IBOutlet UIView *pagerView;
    MenuListView *menuListView;
    __weak IBOutlet UIStackView *stackView;
    
    NSMutableArray *allRequestArray;
    int requestPage;
    int requestPerPage;
    
    WSRequestConcierge *wsRequestConcierge;
    MyRequestType currentRequestType;
    
    LJViewPager *viewPager;
}

@end

@implementation MyRequestPagerViewController
@synthesize isLoadMore, isFullItem;

- (void)viewDidLoad {
    isNotAskConciergeBarButton = NO;
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    [super viewDidLoad];
    [self backNavigationItem];
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"my_request",nil)];
    //[self setUpCustomizedPanGesturePopRecognizer];
    openRequestVC = [[MyRequestViewController alloc] init];
    openRequestVC.parrentVC = self;
    openRequestVC.requestType = OpenRequest;
    closeRequestVC = [[MyRequestViewController alloc] init];
    closeRequestVC.parrentVC = self;
    closeRequestVC.requestType = ClosedRequest;
    
    currentRequestType = OpenRequest;
    
    menuListView = [MenuListView new];
    [stackView insertArrangedSubview:menuListView atIndex:0];
    menuListView.delegate = self;
    [menuListView reload];
    [self setupViewPage];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadMyRequest)
                                                 name:@"ReloadMyRequest"
                                               object:nil];
    
    //[self setupViewPage];
    // Do any additional setup after loading the view from its nib.
}



- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setUpCustomizedPanGesturePopRecognizer];
    if (self.isReaload == true) {
        self.isReaload = false;
        openRequestVC.requestArray = [[NSMutableArray alloc] init];
        closeRequestVC.requestArray = [[NSMutableArray alloc] init];
        [openRequestVC reloadData];
        [closeRequestVC reloadData];
        [self initData];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)reloadMyRequest {
    self.isReaload = true;
}


-(void)initData {
    self.isReaload = false;
    isFullItem = false;
    requestPage = 0;
    requestPerPage = 20;
    isLoading = false;
    // openRequestArray = [[NSMutableArray alloc] init];
    //closedRequestArray = [[NSMutableArray alloc] init];
    //requestType = OpenRequest;
    wsRequestConcierge = [[WSRequestConcierge alloc] init];
    wsRequestConcierge.delegate = self;
    wsRequestConcierge.task = WS_GET_RECENT_CONCIERGE;
    [self getData];
    
}

-(void)getData {
    if(!isNetworkAvailable()) {
        [self showErrorNetwork];
    }else{
        if (!isLoadMore) {
            [self startActivityIndicator];
        } else {
            if ((currentRequestType == OpenRequest && openRequestVC.requestArray.count == 0) || (currentRequestType == ClosedRequest && closeRequestVC.requestArray.count == 0)) {
                [self startActivityIndicator];
            }
        }
        NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
        [param setValue:[NSString stringWithFormat:@"%d", requestPage * requestPerPage + 1] forKey:@"RowStart"];
        [param setValue:[NSString stringWithFormat:@"%d", (requestPage + 1) * requestPerPage] forKey:@"RowEnd"];
        [wsRequestConcierge getRecentConcierge:param];
    }
}



-(void)setupViewPage {
    viewPager = [[LJViewPager alloc] initWithFrame:CGRectZero];
    viewPager.viewPagerDateSource = self;
    viewPager.viewPagerDelegate = self;
    [pagerView addSubview:viewPager];
    viewPager.translatesAutoresizingMaskIntoConstraints = false;
    
    [viewPager.topAnchor constraintEqualToAnchor:pagerView.topAnchor constant:0].active = true;
    [viewPager.bottomAnchor constraintEqualToAnchor:pagerView.bottomAnchor constant:0].active = true;
    [viewPager.trailingAnchor constraintEqualToAnchor:pagerView.trailingAnchor constant:0].active = true;
    [viewPager.leadingAnchor constraintEqualToAnchor:pagerView.leadingAnchor constant:0].active = true;
}



#pragma mark - Pager View Delegate

- (UIViewController *)viewPagerInViewController {
    return self;
}

- (NSInteger)numbersOfPage {
    return 2;
}

- (UIViewController *)viewPager:(LJViewPager *)viewPager controllerAtPage:(NSInteger)page {
    switch (page) {
        case 0:
            return openRequestVC;
            break;
        case 1:
            return closeRequestVC;
            break;
        default:
            return nil;
            break;
    }
}

-(void)viewPager:(LJViewPager *)viewPager didScrollToPage:(NSInteger)page {
    [self cancelAllRequest];
    switch (page) {
        case 0:
            currentRequestType = OpenRequest;
            [openRequestVC reloadData];
            break;
        case 1:
            currentRequestType = ClosedRequest;
            [closeRequestVC reloadData];
            break;
        default:
            break;
    }
    [menuListView setDefaultIndex:page];
    
    if ((currentRequestType == OpenRequest && isFullItem == false && openRequestVC.requestArray.count == 0) || (currentRequestType == ClosedRequest && isFullItem == false && closeRequestVC.requestArray.count == 0))  {
        if (!isLoadMore) {
            [self getData];
        } else {
            [self startActivityIndicator];
        }
    }
}


#pragma mark - Menu List View Delegate

- (NSArray*) MenuListViewListItemsForView:(MenuListView*) view {
    return  @[@"Open",@"Closed"];
}


-(void)loadMoreData {
    isLoadMore =  true;
    [self getData];
}


- (void) MenuListView:(MenuListView*)view didSelectedItem:(id) item atIndex:(NSInteger) index {
    [self cancelAllRequest];
    switch (index) {
        case 0:
            currentRequestType = OpenRequest;
            [openRequestVC reloadData];
            break;
        case 1:
            currentRequestType = ClosedRequest;
            [closeRequestVC reloadData];
            break;
        default:
            break;
    }
    [viewPager scrollToPage:index];
    
    
    if ((currentRequestType == OpenRequest && isFullItem == false && openRequestVC.requestArray.count == 0) || (currentRequestType == ClosedRequest && isFullItem == false && closeRequestVC.requestArray.count == 0))  {
        if (!isLoadMore) {
            [self getData];
        } else {
            [self startActivityIndicator];
        }
    }
}

-(void)cancelAllRequest {
    //[wsRequestConcierge cancelRequest];
    //isLoadMore = false;
    [openRequestVC loadMoreIndicatorIcon:false];
    [closeRequestVC loadMoreIndicatorIcon:false];
    
}

-(void)dealloc {
    [wsRequestConcierge cancelRequest];
}


#pragma mark - Data Load Delegate
-(void)loadDataDoneFrom:(WSRequestConcierge*)ws {
    [self stopActivityIndicator];
    requestPage += 1;
    if (ws.data.count == 0){
        isFullItem = true;
        isLoadMore = false;
        [openRequestVC loadMoreIndicatorIcon:false];
        [closeRequestVC loadMoreIndicatorIcon:false];
        //[_myRequestTableView reloadData];
    }
    if (ws.data.count > 0) {
        if (ws.data.count < requestPerPage) {
            isFullItem = true;
        }
        NSArray *filteredOpenRequestArray = [ws.data filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
            ConciergeObject *obj = (ConciergeObject*)evaluatedObject;
            return [obj.requestStatus isEqualToString:OPEN_STATUS] && ![obj.requestMode isEqualToString:CANCEL_MODE];
        }]];
        NSArray *filteredClosedRequestArray = [ws.data filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
            ConciergeObject *obj = (ConciergeObject*)evaluatedObject;
            return [obj.requestStatus isEqualToString:CLOSE_STATUS] || [obj.requestMode isEqualToString:CANCEL_MODE];
        }]];
        
        
        [openRequestVC.requestArray addObjectsFromArray:filteredOpenRequestArray];
        [closeRequestVC.requestArray addObjectsFromArray:filteredClosedRequestArray];
        
        
        if ((currentRequestType == OpenRequest && isFullItem == false && filteredOpenRequestArray.count == 0) || (currentRequestType == ClosedRequest && isFullItem == false && filteredClosedRequestArray.count == 0))  {
            [self getData];
        } else {
            //[self stopActivityIndicator];
            isLoadMore = false;
            [openRequestVC loadMoreIndicatorIcon:false];
            [closeRequestVC loadMoreIndicatorIcon:false];
            [openRequestVC reloadData];
            [closeRequestVC reloadData];
            
            //[self loadMoreIndicatorIcon:false];
            //[_myRequestTableView removeLoadMoreIndicator];
            //[_myRequestTableView reloadData];
        }
    }
}
- (void)loadDataFailFrom:(id<BaseResponseObjectProtocol>)result withErrorCode:(NSInteger)errorCode{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self stopActivityIndicatorWithoutMask];
        if (errorCode == 1005 || errorCode == -1009) {
            [self showErrorNetwork];
        }else{
            AlertViewController *alert = [[AlertViewController alloc] init];
            alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);;
            alert.msgAlert = @"Failed to send request";
            alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                alert.seconBtn.hidden = YES;
                alert.midView.alpha = 0.0f;;
            });
            [self showAlert:alert forNavigation:YES];
        }
        
    });
}


- (void)loadDataFailFrom:(id<WSBaseProtocol>)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message{
    [self stopActivityIndicatorWithoutMask];
    if (errorCode == 1005 || errorCode == -1009) {
        [self showErrorNetwork];
    } else {
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);;
        alert.msgAlert = message;
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
        });
        [self showAlert:alert forNavigation:YES];
    }
}


- (void) showErrorNetwork {
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"cannot_get_data", nil);
    alert.msgAlert = NSLocalizedString(@"no_network_connection", nil);
    alert.firstBtnTitle = NSLocalizedString(@"settings_button", nil);
    alert.blockFirstBtnAction = ^(void){
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"prefs:root"]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root"]];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root"]];
        }
    };
    alert.secondBtnTitle = NSLocalizedString(@"alert_ok_button", nil);
    alert.providesPresentationContextTransitionStyle = YES;
    alert.definesPresentationContext = YES;
    [alert setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)WSBaseNetworkUnavailable {
    [self stopActivityIndicator];
}


@end
