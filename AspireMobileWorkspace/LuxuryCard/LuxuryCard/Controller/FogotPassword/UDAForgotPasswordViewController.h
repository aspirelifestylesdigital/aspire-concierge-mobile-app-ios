//
//  UDAForgotPasswordViewController.h
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 7/19/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "CustomTextField.h"

@interface UDAForgotPasswordViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet CustomTextField *emailTextField;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *buttonBackgroundView;

// Constraints
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topLayoutConstraint;

@end
