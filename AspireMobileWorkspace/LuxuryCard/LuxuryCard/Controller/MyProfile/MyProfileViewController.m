//
//  MyProfileViewController.m
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/13/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "MyProfileViewController.h"
#import "ImageUtilities.h"
#import "NSString+Utis.h"
#import "Constant.h"
#import "SWRevealViewController.h"
#import "UIButton+Extension.h"
#import "HomeViewController.h"

#import "AppDelegate.h"
#import "UserObject.h"
#import "UITextField+Extensions.h"
#import "UtilStyle.h"
@interface MyProfileViewController ()


@end

@implementation MyProfileViewController
{
    NSString *firstNameOnTyping;
    NSString *lastNameOnTyping;
    NSString *emailOnTyping;
    NSString *phoneOnTyping;
    NSString *zipCodeTyping;
    NSString *passwordOnTyping;
    NSString *confirmPasswordOnTyping;
    BOOL isTempUseLocation;
    IBOutlet NSLayoutConstraint *btSC;
    
}
- (void)viewDidLoad
{
    isTempUseLocation = [[SessionData shareSessiondata] isUseLocation];
    isNotAskConciergeBarButton = YES;
    self.isUpdateProfile = YES;
    [super viewDidLoad];
    [self createBackBarButton];
    [self setNavigationBarWithDefaultColorAndTitle:@"Contact Info"];
    backupBottomConstraint = self.viewActionBottomConstraint.constant;
    [self setButtonStatus:NO];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //  Set up Pop pan gesture
    [self setUpCustomizedPanGesturePopRecognizer];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    //  Set up Pop pan gesture
    //[self setUpCustomizedPanGesturePopRecognizer];
    
    if(self.navigationController.viewControllers.count == 1)
    {
        HomeViewController *homeView = [[HomeViewController alloc] init];
        [self.navigationController setViewControllers:@[homeView,self]];
    }
    
    self.submitButton.hidden = YES;
    //self.cancelButton.hidden = YES;
    self.commitmentLabel.hidden = YES;
    
    [self setTextViewsDefaultBottomBolder];
    [self.view layoutIfNeeded];
    
    [self setDefaultData];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (isTempUseLocation != isUseLocation) {
        [[SessionData shareSessiondata] setIsUseLocation:isTempUseLocation];
    }
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}

- (void)setDefaultData {
    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
    if (profileDictionary) {
        firstNameOnTyping = [profileDictionary objectForKey:keyFirstName];
        lastNameOnTyping = [profileDictionary objectForKey:keyLastName];
        emailOnTyping = [profileDictionary objectForKey:keyEmail];
        phoneOnTyping = [profileDictionary objectForKey:keyMobileNumber];
        passwordOnTyping = [profileDictionary objectForKey:keyPassword];
        zipCodeTyping = [profileDictionary objectForKey:keyZipCode];
    }
}

- (void)initView{
    self.phoneNumberText.tag = PHONE_NUMBER_TEXTFIELD_TAG;
    [self setButtonFrame];
    
    self.emailText.textColor = colorFromHexString(TEXTFIELD_PLACEHOLDER_COLOR_STANDARD);
    
    [self.updateButton setBackgroundColorForNormalStatus];
    [self.updateButton setBackgroundColorForTouchingStatus];
    [self.updateButton configureDecorationForButton];
    
    [self.cancelButtonMyProfile setBackgroundColorForNormalStatus];
    [self.cancelButtonMyProfile setBackgroundColorForTouchingStatus];
    [self.cancelButtonMyProfile configureDecorationForButton];
    
    // Text Style For Greet Message
    NSString *message = NSLocalizedString(@"commitment_my_profile_message", nil);
    self.commitmentLabel.attributedText = [UtilStyle setLargeSizeStyleForLabelWithMessage:message];
    
    [self setCheckBoxState:isCheck];
    self.commitmentLabel.text = NSLocalizedString(@"commitment_my_profile_message", nil);
    [self.updateButton setTitle:NSLocalizedString(@"update_button", nil) forState:UIControlStateNormal];
    [self.cancelButtonMyProfile setTitle:NSLocalizedString(@"cancel_button", nil) forState:UIControlStateNormal];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:BUTTON_FONT_NAME_STANDARD size:(BUTTON_FONT_SIZE_STANDARD * SCREEN_SCALE)],
                                 NSKernAttributeName: @1.4};
    self.updateButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.updateButton.titleLabel.text attributes:attributes];
    self.cancelButtonMyProfile.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.cancelButtonMyProfile.titleLabel.text attributes:attributes];
    
    [self.updateButtonView setBackgroundColor:colorFromHexString(DEFAULT_BACKGROUND_COLOR)];
    [self.cancelButtonView setBackgroundColor:colorFromHexString(DEFAULT_BACKGROUND_COLOR)];
    
}

- (void) setButtonFrame{
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)handleKeyboardWillHide:(NSNotification *)paramNotification {
    btSC.constant = 80;
//    self.viewActionBottomConstraint.constant = backupBottomConstraint;
}

- (void)handleKeyboardWillShow:(NSNotification *)paramNotification {
    
    [self.salutationDropDown didTapBackground];
    [self.countryCodeDropDown didTapBackground];
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    keyboardHeight = keyboardRect.size.height;
    btSC.constant =  keyboardHeight + 80;
//    self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight + 250, 0);
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

-(void) createAsteriskForTextField:(UITextField *)textField{
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [textField setLeftView:leftView];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    
    asteriskImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"asterisk_icon"]];
    
    asteriskImage.frame = CGRectMake(0.0f, 0.0f, 20.0f, 20.0f);
    asteriskImage.contentMode = UIViewContentModeCenter;
    [textField setRightView:asteriskImage];
    [textField setRightViewMode:UITextFieldViewModeAlways];
}

- (void)changeUseLocation{
    [self setButtonStatus:[self checkFieldIsChangeString]];
}

- (void)updateSuccessRedirect{    
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert =nil;
    alert.msgAlert = NSLocalizedString(@"update_profile_success_message", nil);
    alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
    dispatch_async(dispatch_get_main_queue(), ^{
        alert.seconBtn.hidden = YES;
        alert.midView.alpha = 0.0f;;
        [alert.view layoutIfNeeded];
    });
    
    alert.blockFirstBtnAction = ^(void){
        isTempUseLocation = isUseLocation;
        [self getUserInfo];
        [self setTextViewsDefaultBottomBolder];
        [self setButtonStatus:NO];
    };
    
    [self showAlert:alert forNavigation:NO];
}


- (IBAction)touchUpdate:(id)sender {
    [self.view endEditing:YES];
    [self verifyAccountData:YES];
}

- (IBAction)touchCancel:(id)sender {
    [self.phoneNumberText endEditing:YES];
    [self dismissKeyboard];
    [self setDefaultData];
    [self setTextViewsDefaultBottomBolder];
    [self setButtonStatus:NO];
    [[SessionData shareSessiondata] setIsUseLocation:isTempUseLocation];
    [self getUserInfo];
}

- (void)updateProfileButtonStatus{
    [self setButtonStatus:[self checkFieldIsChangeString]];
}

- (void)checkTextFieldIsChange:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *resultText = [textField.text stringByReplacingCharactersInRange:range
                                                                   withString:string];
    
    if (textField == self.firstNameText) {
        firstNameOnTyping = resultText;
    }else if (textField == self.lastNameText){
        lastNameOnTyping = resultText;
    }else if (textField == self.emailText){
        emailOnTyping = resultText;
    }else if (textField == self.phoneNumberText){
        phoneOnTyping = resultText;
    }else if (textField == self.passwordText){
        passwordOnTyping = resultText;
    }else if (textField == self.zipCodeText){
        zipCodeTyping = resultText;
    }
    [self setButtonStatus:[self checkFieldIsChangeString]];
}

- (void)changeValueDropDown{
    [self setButtonStatus:[self checkFieldIsChangeString]];
}

- (BOOL)checkFieldIsChangeString{
    
    if (isUseLocation != isTempUseLocation) {
        return YES;
    }
    if (firstNameOnTyping.length == 0 && lastNameOnTyping.length == 0 && emailOnTyping.length == 0 && phoneOnTyping.length == 0 && self.salutationDropDown.valueStatus == DefaultValueStatus && zipCodeTyping.length == 0 && self.countryCodeDropDown.valueStatus == DefaultValueStatus) {
        return NO;
    }
    
    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo]; 
    if (profileDictionary) {
        Country *currentCountry = [[Country alloc] init];
        NSString *zipCode = [profileDictionary objectForKey:keyZipCode];
        NSString *countryCode = [profileDictionary objectForKey:keyCountryCode];
        if ([countryCode isEqualToString:@"1"]) {
            if (zipCode.length > 0) {
                currentCountry = ([[zipCode substringWithRange:NSMakeRange(0, 1)] isNumber]) ? getCountryByCodeName(@"USA") : getCountryByCodeName(@"CAN");
            }else {
                currentCountry = getCountryByCodeName(@"USA");
            }
        }else{
            currentCountry = getCountryByNumber(countryCode);
        }
        if (![self.salutationDropDown.title isEqualToString:[profileDictionary objectForKey:keySalutation]]) {
            return YES;
        }else if (![[profileDictionary objectForKey:keyFirstName] isEqualToString:firstNameOnTyping]) {
            return YES;
        }else if (![[profileDictionary objectForKey:keyLastName] isEqualToString:lastNameOnTyping]){
            return YES;
        }
        else if (![[profileDictionary objectForKey:keyEmail] isEqualToString:emailOnTyping]){
            return YES;
        }
        else if (![[profileDictionary objectForKey:keyMobileNumber] isEqualToString:phoneOnTyping]){
            return YES;
        }
        else if (![[profileDictionary objectForKey:keyZipCode] isEqualToString:zipCodeTyping]){
            return YES;
        }
        else if (![self.countryCodeDropDown.title isEqualToString:[NSString stringWithFormat:@"%@ (%@)",currentCountry.countryCode, currentCountry.countryNumber]]){
            return YES;
        }
    }

    return NO;
}

- (void)setButtonStatus:(BOOL)status{
    self.updateButton.enabled = status;
    self.cancelButtonMyProfile.enabled = status;
}

@end
