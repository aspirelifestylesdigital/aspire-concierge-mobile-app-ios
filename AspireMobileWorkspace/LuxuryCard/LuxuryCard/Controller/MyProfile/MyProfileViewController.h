//
//  MyProfileViewController.h
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/13/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "BaseViewController.h"
#import "CreateProfileViewController.h"

@interface MyProfileViewController : CreateProfileViewController

@property (weak, nonatomic) IBOutlet UIView *viewAction;

@property (weak, nonatomic) IBOutlet UIButton *updateButton;

@property (weak, nonatomic) IBOutlet UIButton *cancelButtonMyProfile;
@property (weak, nonatomic) IBOutlet UIView *updateButtonView;
@property (weak, nonatomic) IBOutlet UIView *cancelButtonView;


- (IBAction)touchUpdate:(id)sender;
- (IBAction)touchCancel:(id)sender;
@end

