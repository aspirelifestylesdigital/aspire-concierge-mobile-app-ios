//
//  DiningDetailHeaderTableViewCell.m
//  MobileConcierge
//
//  Created by user on 6/7/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "DiningDetailHeaderTableViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "Common.h"

@implementation DiningDetailHeaderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
//    [self.lblQuestion setFont:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:36]];
    
    self.leadingConstraint.constant = self.leadingConstraint.constant*SCREEN_SCALE;
    self.topButtonsConstraint.constant = self.topButtonsConstraint.constant*SCREEN_SCALE;
    self.topQuestionConstraint.constant = self.topQuestionConstraint.constant*SCREEN_SCALE;
    
    [self.btnShare setBackgroundImage:[UIImage imageNamed:@"share_icon"] forState:UIControlStateNormal];
    [self.btnShare setBackgroundImage:[UIImage imageNamed:@"share_icon_interact"] forState:UIControlStateHighlighted];
    
    [self.btnAskConcierge setBackgroundImage:[UIImage imageNamed:@"concierge_icon"] forState:UIControlStateNormal];
    [self.btnAskConcierge setBackgroundImage:[UIImage imageNamed:@"concierge_icon_interaction"] forState:UIControlStateHighlighted];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)touchAskConcierge:(id)sender {
    [self.diningDetailHeaderTableViewCellDelegate touchAskConcierge];
}
- (IBAction)touchShare:(id)sender {
    NSMutableArray *data = [[NSMutableArray alloc] init];
    if(self.lblQuestion.text.length > 0)
    {
        [data addObject:[NSString stringWithFormat:@"%@\n",self.lblQuestion.text]];
    }
    
    [data addObject:[NSURL URLWithString:MASTERCARD_SHARING]];
    
    if(self.headImg.image)
    {
        [data addObject:self.headImg.image];
    }

    [self.diningDetailHeaderTableViewCellDelegate shareUrl:data];
}

- (void)loadData{
    self.lblQuestion.text = self.questionName;
    [self.lblQuestion sizeToFit];
}
@end
