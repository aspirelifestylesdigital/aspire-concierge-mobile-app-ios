//
//  DiningDetailContentTermsTableViewCell.h
//  MobileConcierge
//
//  Created by user on 6/8/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "BaseTableViewCell.h"
@class DiningDetailContentTermsTableViewCell;
@protocol DiningDetailContentTermsTableViewCellDelegate <NSObject>

- (void) gotoTermsAndConditionsWebBrowser:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType;

- (void) reloadTermsCell:(DiningDetailContentTermsTableViewCell*)cell andHeight:(float)height;

@end
@interface DiningDetailContentTermsTableViewCell : BaseTableViewCell <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConstraint;
@property (strong, nonatomic) NSString *cellDescription;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightWebView;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTitleLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleTopConstrain;

@property (strong, nonatomic) id<DiningDetailContentTermsTableViewCellDelegate> diningDetailContentTermsTableViewCellDelegate;
@end
