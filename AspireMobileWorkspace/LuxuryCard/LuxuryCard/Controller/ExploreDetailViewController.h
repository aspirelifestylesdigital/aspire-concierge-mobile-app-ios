//
//  DiningDetailViewController.h
//  MobileConcierge
//
//  Created by user on 6/7/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "BaseViewController.h"
#import "BaseItem.h"

typedef NS_ENUM( NSInteger, DetailType){
    DetailType_Dining,
    DetailType_CityGuide,
    DetailType_Other
};


@interface ExploreDetailViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) BaseItem *item;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (assign, nonatomic) BOOL itemCCA;
@property (strong, nonatomic) NSString *cityName;
@property (nonatomic, assign) NSInteger detailType;

@property (nonatomic, strong) NSString *contentID;
@end
