//
//  DiningDetailViewController.m
//  MobileConcierge
//
//  Created by user on 6/7/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "ExploreDetailViewController.h"
#import "AskConciergeViewController.h"
// Utils
#import "Constant.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
// cell
#import "DiningDetailHeaderTableViewCell.h"
#import "DiningDetailContentInfoTableViewCell.h"
#import "DiningDetailContentInsiderTableViewCell.h"
#import "DiningDetailContentBenefitsTableViewCell.h"
#import "DiningDetailContentPriceTableViewCell.h"
#import "DiningDetailContentDescriptionTableViewCell.h"
#import "DiningDetailContentTermsTableViewCell.h"
#import "DiningDetailContentHoursTableViewCell.h"
// item
#import "AnswerItem.h"
#import "WSB2CGetContentFullFromCCA.h"
#import "ContentFullCCAResponseObject.h"
#import "ContentFullCCAItem.h"
#import "TileItem.h"
#import "SearchItem.h"
#import "WSB2CGetQuestionAndAnswers.h"
#import "AppData.h"
#import "CityItem.h"
#import "CategoryItem.h"
#import "UIImageView+AFNetworking.h"
#import "MCActivityItemSource.h"
#import "AlertViewController.h"
#import "MCImageActivityItemSource.h"
#import "MCTitleActivityItemSource.h"
#import "ImageUtilities.h"


@interface ExploreDetailViewController () <DiningDetailHeaderTableViewCellDelegate, DiningDetailContentInfoTableViewCellDelegate, DiningDetailContentTermsTableViewCellDelegate, DiningDetailContentBenefitsTableViewCellDelegate, DataLoadDelegate, DiningDetailContentDescriptionTableViewCellDelegate>

@end

@implementation ExploreDetailViewController
{
    WSB2CGetContentFullFromCCA *wSB2CGetContentFullFromCCA;
    WSB2CGetQuestionAndAnswers *wsAnswer;
    ContentFullCCAItem *contentFullCCAItem;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self registTableView];
    [self setNavigationBarWithDefaultColorAndTitle:self.item.name.uppercaseString];
    [self backNavigationItem];
    [self createConciergeBarButton];
    [self setupDataForView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setUpCustomizedPanGesturePopRecognizer];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark REGISTER TABLE CELL
- (void) registTableView{
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 1.0f;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"DiningDetailHeaderTableViewCell" bundle:nil] forCellReuseIdentifier:@"DiningDetailHeaderTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"DiningDetailContentInfoTableViewCell" bundle:nil] forCellReuseIdentifier:@"DiningDetailContentInfoTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"DiningDetailContentInsiderTableViewCell" bundle:nil] forCellReuseIdentifier:@"DiningDetailContentInsiderTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"DiningDetailContentBenefitsTableViewCell" bundle:nil] forCellReuseIdentifier:@"DiningDetailContentBenefitsTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"DiningDetailContentPriceTableViewCell" bundle:nil] forCellReuseIdentifier:@"DiningDetailContentPriceTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"DiningDetailContentDescriptionTableViewCell" bundle:nil] forCellReuseIdentifier:@"DiningDetailContentDescriptionTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"DiningDetailContentTermsTableViewCell" bundle:nil] forCellReuseIdentifier:@"DiningDetailContentTermsTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"DiningDetailContentHoursTableViewCell" bundle:nil] forCellReuseIdentifier:@"DiningDetailContentHoursTableViewCell"];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.estimatedRowHeight = 150;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

#pragma mark PROCESS VIEW LOGIC

-(UIImage *)getImageForCityGuideItem:(SearchItem *)searchItem
{
    NSArray *citySelections = [AppData getSelectionCityList];
    for(CityItem *cityItem  in citySelections)
    {
        for(CategoryItem *categoryItem in cityItem.subCategories)
        {
            if([searchItem.secondaryID isEqualToString:categoryItem.ID])
            {
                return categoryItem.categoryImgDetail;
            }
        }
    }
    
    return nil;
}
-(void)setupDataForView
{
    if([self.item isKindOfClass:[TileItem class]])
    {
        self.detailType = DetailType_Other;
        [self loadDataFromCCAServerWithItemId:self.item.ID];
    }
    else if([self.item isKindOfClass:[AnswerItem class]] && [self.item.categoryCode isEqualToString:@"dining"])
    {
        self.detailType = DetailType_Dining;
    }
    else if([self.item isKindOfClass:[AnswerItem class]] && [self.item.categoryCode isEqualToString:@"cityguide"])
    {
        self.detailType = DetailType_CityGuide;
    }
    else if([self.item isKindOfClass:[SearchItem class]])
    {
        SearchItem *searchItem = (SearchItem *)self.item;
        UIImage *headImage = [self getImageForCityGuideItem:searchItem];
        
        if([searchItem.product isEqualToString:@"CCA"])
        {
            self.detailType = DetailType_Other;
            [self loadDataFromCCAServerWithItemId:searchItem.ID];
        }
        else if([searchItem.product isEqualToString:@"IA"])
        {
         
            self.item.image = headImage;
            self.detailType = DetailType_Other;
            [self loadDataFromIAServerWithSearchItem:searchItem];
        }
    }

}

-(void) loadDataFromIAServerWithSearchItem:(SearchItem *)item;
{
    [self startActivityIndicatorWithoutMask];
    
    wsAnswer = [[WSB2CGetQuestionAndAnswers alloc] init];
    wsAnswer.delegate = self;
    wsAnswer.categoryId = item.secondaryID;
    wsAnswer.questionId = item.ID;
    wsAnswer.task = WS_B2C_GET_SEARCH_DETAIL;
    [wsAnswer retrieveDataFromServer];
}

- (void) loadDataFromCCAServerWithItemId:(NSString *)itemID
{
    [self startActivityIndicatorWithoutMask];
    
    wSB2CGetContentFullFromCCA = [[WSB2CGetContentFullFromCCA alloc] init];
    wSB2CGetContentFullFromCCA.delegate = self;
    wSB2CGetContentFullFromCCA.itemID = itemID;
    [wSB2CGetContentFullFromCCA loadDataFromServer];
}

-(void)shareUrl:(NSArray *)data{
    MCImageActivityItemSource *imageItemSource = [[MCImageActivityItemSource alloc] init];
    imageItemSource.data = data;
    
    MCTitleActivityItemSource *titleItemSource = [[MCTitleActivityItemSource alloc] init];
    imageItemSource.data = data;
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:@[titleItemSource,[data objectAtIndex:1],imageItemSource] applicationActivities:nil];
    /*
    [activityVC setCompletionWithItemsHandler:^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
        if(activityType == UIActivityTypePostToFacebook)
        {
            
            if(!completed){
                AlertViewController *alert = [[AlertViewController alloc] init];
                alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);;
                alert.msgAlert = @"Please login Facebook and share again!";
                alert.firstBtnTitle = NSLocalizedString(@"arlet_ok_button", nil);
                alert.secondBtnTitle = NSLocalizedString(@"arlet_cancel_button", nil);
                
                alert.blockFirstBtnAction =^(){
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"fb://profile/355356557838717"]];
                };
                
                [self showAlert:alert forNavigation:YES];
            }
        }
    }];
     */
    [self presentViewController:activityVC animated:YES completion:^{
    }];
}

- (void) touchAskConcierge{
    AskConciergeViewController *vc = [[AskConciergeViewController alloc] init];
    if (self.detailType != DetailType_Other) {
        vc.itemName = self.item.name;
        vc.categoryName = self.item.categoryName;

        if (self.detailType == DetailType_CityGuide) {
            vc.cityName = self.cityName;
        }
    }else{
        vc.itemName = contentFullCCAItem.title;
        vc.categoryName = contentFullCCAItem.category;
    }
    
    [self.navigationController pushViewController:vc animated:YES];
}


- (void) gotoAppleMapWith:(NSString*)address{
    // Check for iOS 6
    Class mapItemClass = [MKMapItem class];
    if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)])
    {
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder geocodeAddressString:address
                     completionHandler:^(NSArray *placemarks, NSError *error) {
                         
                         // Convert the CLPlacemark to an MKPlacemark
                         // Note: There's no error checking for a failed geocode
                         CLPlacemark *geocodedPlacemark = [placemarks objectAtIndex:0];
                         MKPlacemark *placemark = [[MKPlacemark alloc]
                                                   initWithCoordinate:geocodedPlacemark.location.coordinate
                                                   addressDictionary:geocodedPlacemark.addressDictionary];
                         
                         // Create a map item for the geocoded address to pass to Maps app
                         MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
                         [mapItem setName:((BaseItem*)self.item).name];
                         [MKMapItem openMapsWithItems:@[mapItem] launchOptions:nil];
                         
                     }];
    }
}

- (void) seeDetailMapWithAddress:(NSString *)address{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"alert_title", nil);
    alert.msgAlert = NSLocalizedString(@"category_more_info_msg", nil);
    alert.firstBtnTitle = NSLocalizedString(@"yes_button", nil);
    alert.blockFirstBtnAction = ^(void){
        [self gotoAppleMapWith:address];
    };
    
    alert.blockSecondBtnAction  = ^(void){};
    [self showAlert:alert forNavigation:YES];
}

- (void) gotoWebBrowser{
    
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"alert_title", nil);
    alert.msgAlert = NSLocalizedString(@"category_more_info_msg", nil);
    alert.firstBtnTitle = NSLocalizedString(@"yes_button", nil);
    alert.blockFirstBtnAction = ^(void){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:((AnswerItem*)self.item).URL]];
    };
    
    alert.blockSecondBtnAction  = ^(void){};
    [self showAlert:alert forNavigation:YES];
}

- (void)gotoTermsAndConditionsWebBrowser:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    if ([[[request URL] absoluteString] containsString:@"http"]) {
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"alert_title", nil);
        alert.msgAlert = NSLocalizedString(@"category_more_info_msg", nil);
        alert.firstBtnTitle = NSLocalizedString(@"yes_button", nil);
        alert.blockFirstBtnAction = ^(void){
            [[UIApplication sharedApplication] openURL:[request URL]];
        };
        
        alert.blockSecondBtnAction  = ^(void){};
        [self showAlert:alert forNavigation:YES];
    }
}

- (void)gotoBenefitsWebBrowser:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    if ([[[request URL] absoluteString] containsString:@"http"]) {
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"alert_title", nil);
        alert.msgAlert = NSLocalizedString(@"category_more_info_msg", nil);
        alert.firstBtnTitle = NSLocalizedString(@"yes_button", nil);
        alert.blockFirstBtnAction = ^(void){
            [[UIApplication sharedApplication] openURL:[request URL]];
        };
        
        alert.blockSecondBtnAction  = ^(void){};
        [self showAlert:alert forNavigation:YES];
    }
}
- (void)openDescriptionURL:(NSURL *)url{
    if ([[url absoluteString] containsString:@"http"]) {
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"alert_title", nil);
        alert.msgAlert = NSLocalizedString(@"category_more_info_msg", nil);
        alert.firstBtnTitle = NSLocalizedString(@"yes_button", nil);
        alert.blockFirstBtnAction = ^(void){
            [[UIApplication sharedApplication] openURL:url];
        };
        
        alert.blockSecondBtnAction  = ^(void){};
        [self showAlert:alert forNavigation:YES];
    }
}



#pragma mark TABLE DELEGATE


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    if (self.detailType == DetailType_Dining) {
        switch (indexPath.row) {
            case 0:
                cell = [self cellHeader:self.item];
                break;
            case 1:
                cell = [self cellInfo:self.item];
                break;
            case 2:
                cell = [self cellInsider:self.item];
                break;
            case 3:
                cell = [self cellBenefits:self.item];
                break;
            case 4:
                cell = [self cellPrice:self.item];
                break;
            case 5:
                cell = [self cellDescription:self.item];
                break;
            case 6:
                cell = [self cellHours:self.item];
                break;
            case 7:
                cell = [self cellTerm:self.item];
                break;
                
            default:
                break;
        }
    }else if (self.detailType == DetailType_CityGuide){
        switch (indexPath.row) {
            case 0:
                cell = [self cellHeader:self.item];
                break;
            case 1:
                cell = [self cellInfo:self.item];
                break;
            case 2:
                cell = [self cellInsider:self.item];
                break;
            case 3:
                cell = [self cellDescription:self.item];
                break;                
            default:
                break;
        }
    }else if (self.detailType == DetailType_Other){
        switch (indexPath.row) {
            case 0:
                cell = [self cellHeader:self.item];
                break;
            case 1:
                cell = [self cellBenefits:self.item];
                
                break;
            case 2:
                cell = [self cellDescription:self.item];
                break;
            case 3:
                cell = [self cellTerm:self.item];
                break;
            default:
                break;
        }
    }
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (self.detailType == DetailType_Dining) {
        return 8;
    }
    else if (self.detailType == DetailType_CityGuide){
        return 4;
    }
    else{
        return contentFullCCAItem ? 4 : 0;
    }
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.detailType == DetailType_Dining) {
        AnswerItem *answerItem = (AnswerItem *)self.item;
        switch (indexPath.row) {
            case 0:
                return UITableViewAutomaticDimension;
                break;
            case 1:
                return UITableViewAutomaticDimension;
                break;
            case 2:
                if(answerItem.insiderTip.length == 0)
                {
                    return 0.01f;
                }
                return UITableViewAutomaticDimension;
                break;
            case 3:
                return UITableViewAutomaticDimension;
                break;
            case 4:
                if(answerItem.userDefined1.length == 0){
                    return 0.01f;
                }
                return UITableViewAutomaticDimension;
                break;
            case 5:
                return UITableViewAutomaticDimension;
                break;
            case 6:
                if(answerItem.hoursOfOperation.length == 0){
                    return 0.01f;
                }
                return UITableViewAutomaticDimension;
                break;
            case 7:
                return UITableViewAutomaticDimension;
                break;
                
            default:
                break;
        }
    }
    else if (self.detailType == DetailType_CityGuide){
        AnswerItem *anserItem = (AnswerItem *)self.item;
        switch (indexPath.row) {
            case 0:
            case 1:
                return UITableViewAutomaticDimension;
                break;
            case 2:
                if(anserItem.insiderTip.length == 0){
                    return 0.01f;
                }
                return UITableViewAutomaticDimension;
                break;
            case 3:
                return UITableViewAutomaticDimension;
                break;
            default:
                break;
        }
    }
    
    return UITableViewAutomaticDimension;
}


#pragma mark TABLE CELL VIEW
- (UITableViewCell *)cellHeader:(BaseItem *)item{
    DiningDetailHeaderTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"DiningDetailHeaderTableViewCell"];
    if (self.detailType != DetailType_Other) {
        cell.questionName = item.name;
        
        if(self.detailType == DetailType_CityGuide){
            cell.headImg.image = item.image;
        }
        else{
            cell.imageURL = item.imageURL;
        }
        
        cell.diningDetailHeaderTableViewCellDelegate = self;
    }else{
        cell.questionName = contentFullCCAItem.title;
        cell.imageURL = [contentFullCCAItem.imgURL isEqualToString:@""] ? item.imageURL : contentFullCCAItem.imgURL;
        cell.shareLink = @"";
        cell.diningDetailHeaderTableViewCellDelegate = self;
    }
    
    if(cell.imageURL)
    {
        __weak typeof(cell) weakCell = cell;
        __weak typeof(self.tableView) weakTable = self.tableView;
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[weakCell.imageURL removeBlankFromURL]]] ;
        
        [cell.headImg setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"placehoder_explore_detail"] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
            
            float imgWidth = image.size.width;
            image = [ImageUtilities imageWithImage:image convertToSize:CGSizeMake(SCREEN_WIDTH, (SCREEN_WIDTH/imgWidth)*image.size.height)];
            NSLog(@"%f  %f", image.size.width, image.size.height);
            
            weakCell.headImg.image = image;
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            if(([weakTable.visibleCells containsObject:weakCell]))
            {
                [weakTable reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            }
            
        } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
        }];
    }
    
    [cell loadData];
    [cell.contentView layoutIfNeeded];
    [self.view layoutIfNeeded];
    return cell;
}


- (UITableViewCell *)cellInfo:(id)item{
    DiningDetailContentInfoTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"DiningDetailContentInfoTableViewCell"];
    if (self.detailType == DetailType_CityGuide) {
        cell.address1 = [NSString stringWithFormat:@"%@ %@, %@ %@ %@ %@",((AnswerItem*)item).address1, ((AnswerItem*)item).cityName, ((AnswerItem*)item).state, ((AnswerItem*)item).zipCode, ((AnswerItem*)item).address3, ((AnswerItem*)item).address2];
//        cell.address3 = ((AnswerItem*)item).address2;
    }else{
        cell.address1 = [NSString stringWithFormat:@"%@, %@, %@, %@ %@",((AnswerItem*)item).address1, ((AnswerItem*)item).cityName, ((AnswerItem*)item).state, ((AnswerItem*)item).zipCode, ((AnswerItem*)item).address3];
//        cell.address3 = ((AnswerItem*)item).address3;
    }
    cell.googleAddress = [NSString stringWithFormat:@"%@, %@, %@",((AnswerItem*)item).address1, ((AnswerItem*)item).cityName, ((AnswerItem*)item).state];
    cell.webLink = ((AnswerItem*)item).URL;
    cell.diningDetailContentInfoTableViewCellDelegate = self;
    [cell loadData];
    return cell;
}

- (UITableViewCell *)cellInsider:(id)item{
    DiningDetailContentInsiderTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"DiningDetailContentInsiderTableViewCell"];
    cell.insiderTip = ((AnswerItem*)item).insiderTip;
    if (((AnswerItem*)item).insiderTip.length == 0) {
        [cell.subviews setValue:@YES forKeyPath:@"hidden"];
    }else{
        [cell.subviews setValue:@NO forKeyPath:@"hidden"];
    }
    [cell loadData];
    return cell;
}
 

- (UITableViewCell *)cellHours:(id)item{
    DiningDetailContentHoursTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"DiningDetailContentHoursTableViewCell"];
    cell.workingHour = ((AnswerItem*)item).hoursOfOperation;
    [cell loadData];
    return cell;
}

- (UITableViewCell *)cellPrice:(id)item{
    DiningDetailContentPriceTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"DiningDetailContentPriceTableViewCell"];
    cell.priceRange = [NSString stringWithFormat:@"%.2f",((AnswerItem*)item).price];
    cell.cuisine = ((AnswerItem*)item).userDefined1;
    [cell loadData];
    return cell;
}

//- (UITableViewCell *)cellBenefits:(id)item{
//    DiningDetailContentTermsTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"DiningDetailContentTermsTableViewCell"];
//    if (self.detailType != DetailType_Other) {
//        cell.cellDescription = ((AnswerItem*)item).offer2;
//        
//    }else{
//        cell.cellDescription = contentFullCCAItem.offerText;
//    }
////    cell.diningDetailContentTermsTableViewCellDelegate = self;
//    
//    /*
//    if ([cell.benefits isEqualToString:@""]) {
//        [cell.subviews setValue:@YES forKeyPath:@"hidden"];
//    }else{
//        [cell.subviews setValue:@NO forKeyPath:@"hidden"];
//    }
//     */
//    
//    
//    [cell loadData];
//    return cell;
//}

- (UITableViewCell *)cellBenefits:(id)item{
    DiningDetailContentBenefitsTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"DiningDetailContentBenefitsTableViewCell"];
    if (self.detailType != DetailType_Other) {
        cell.benefits = ((AnswerItem*)item).offer2;
        
    }else{
        cell.benefits = contentFullCCAItem.offerText;
    }
    cell.isOffer = ((AnswerItem*)item).isOffer;
    cell.diningDetailContentBenefitsTableViewCellDelegate = self;
    [cell loadData];
    return cell;
}


- (UITableViewCell *)cellDescription:(id)item{
    DiningDetailContentDescriptionTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"DiningDetailContentDescriptionTableViewCell"];
    cell.titleLbl.text = @"DESCRIPTION";
    if (self.detailType != DetailType_Other) {
        cell.cellDescription = ((AnswerItem*)item).answerDescription;
        if (self.detailType == DetailType_Dining) {
//            cell.titleLbl.hidden = YES;
            cell.heightTitleLbl.constant = 0.01f;
            cell.titleTopConstrain.constant = 0.01f;
        }
    }else{
        cell.cellDescription = contentFullCCAItem.descriptionContent;
    }
    
    if (self.detailType == DetailType_CityGuide) {
        cell.topLine.hidden = YES;
    }else{
        cell.topLine.hidden = NO;
    }
    cell.diningDetailContentDescriptionTableViewCellDelegate = self;
    [cell loadData];
    return cell;
}
 /*
- (UITableViewCell *)cellDescription:(id)item{
    DiningDetailContentDescriptionTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"DiningDetailContentDescriptionTableViewCell"];
    if (self.detailType != DetailType_Other) {
        cell.descriptions = ((AnswerItem*)item).answerText;
        if (self.detailType == DetailType_Dining) {
            cell.lblTitle.hidden = YES;
        }
    }else{
        cell.descriptions = contentFullCCAItem.descriptionContent;
    }
    
    [cell loadData];
    [cell layoutIfNeeded];
    return cell;
}


 */

- (UITableViewCell *)cellTerm:(id)item{
    DiningDetailContentTermsTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"DiningDetailContentTermsTableViewCell"];
    cell.diningDetailContentTermsTableViewCellDelegate = self;
    
    if(self.detailType == DetailType_Dining){
        AnswerItem *answerIten = (AnswerItem*)item;
        cell.titleLbl.text = NSLocalizedString(@"terms_and_conditions_explore_detail", nil);
        cell.cellDescription = answerIten.offer2.length > 0 ? NSLocalizedString(@"terms_and_conditions", nil) : @"";
    }
    else if (self.detailType == DetailType_CityGuide){
        cell.titleLbl.text = NSLocalizedString(@"terms_and_conditions_explore_detail", nil);
        cell.cellDescription = NSLocalizedString(@"terms_and_conditions", nil);
    }
    else{
        cell.titleLbl.text = NSLocalizedString(@"terms_of_use_explore_detail", nil);
        cell.cellDescription = [NSString stringWithFormat:@"%@\n%@",contentFullCCAItem.termsOfUse ? contentFullCCAItem.termsOfUse : @"", NSLocalizedString(@"terms_and_conditions", nil)];;
    }
    cell.titleLbl.attributedText = [[NSAttributedString alloc] initWithString:cell.titleLbl.text attributes:@{NSKernAttributeName:@1.0}];
    [cell loadData];
    return cell;
}

- (void)reloadBenefitsCell:(DiningDetailContentBenefitsTableViewCell *)cell andHeight:(float)height{
    
    [UIView transitionWithView: self.tableView
                      duration: 0.1f
                       options: UIViewAnimationOptionTransitionCrossDissolve
                    animations: ^(void)
     {
         [self.tableView reloadData];
     }
                    completion: nil];
}

- (void)reloadTermsCell:(DiningDetailContentTermsTableViewCell *)cell andHeight:(float)height{
    [UIView transitionWithView: self.tableView
                      duration: 0.1f
                       options: UIViewAnimationOptionTransitionCrossDissolve
                    animations: ^(void)
     {
         [self.tableView reloadData];
     }
                    completion: nil];
}


#pragma mark DATA LOAD DELEGATE


- (void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_B2C_GET_SEARCH_DETAIL && ws.data.count > 0)
    {
        UIImage *headImage = self.item.image;
        self.item = [ws.data objectAtIndex:0];
        
        if(headImage){
            self.detailType = DetailType_CityGuide;
            self.item.image = headImage;
        }
        else{
            self.detailType = DetailType_Dining;
        }
    }
    else if (ws.data.count > 0) {
        contentFullCCAItem = (ContentFullCCAItem*)[ws.data objectAtIndex:0];
    }
    [self.tableView reloadData];
    [self stopActivityIndicatorWithoutMask];
}


- (void)loadDataFailFrom:(id<BaseResponseObjectProtocol>)result withErrorCode:(NSInteger)errorCode{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self stopActivityIndicatorWithoutMask];
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);;
        alert.msgAlert = @"Failed to send request";
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
        });
        
        [self showAlert:alert forNavigation:YES];
    });
}


- (void)loadDataFailFrom:(id<WSBaseProtocol>)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message{
    [self stopActivityIndicatorWithoutMask];
    
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);;
    alert.msgAlert = message;
    alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        alert.seconBtn.hidden = YES;
        alert.midView.alpha = 0.0f;;
    });
    
    [self showAlert:alert forNavigation:YES];
}
@end
