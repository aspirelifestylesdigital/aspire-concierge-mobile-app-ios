//
//  SubCategoryViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/26/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "SubCategoryViewController.h"
#import "AppData.h"
#import "CategoryCollectionViewCell.h"
#import "SelectingCategoryDelegate.h"
#import "CategoryCityGuideViewController.h"
#import "WSB2CGetSubCategory.h"
#import "BaseResponseObject.h"

@interface SubCategoryViewController ()

@end

@implementation SubCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //  Set up Pop pan gesture
    [self setUpCustomizedPanGesturePopRecognizer];
}


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initData
{
    self.categoryLst = [AppData getCityGuideList];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryCollectionViewCell *cell = (CategoryCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
    cell.name.textColor = [UIColor redColor];
    [cell.maskView setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.8]];
    
    CategoryItem *item = [self.categoryLst objectAtIndex:indexPath.row];
    
    if(indexPath.row == 0)
    {
        CategoryCityGuideViewController *categoryCityGuide = [[CategoryCityGuideViewController alloc] init];
        categoryCityGuide.delegate = self.delegate;
        [self.navigationController pushViewController:categoryCityGuide animated:NO];
        [cell setCellToDefault];
    }
    else
    {
        if(self.delegate)
            [self.delegate Explore:self onSelectCategory:item];
    }
}


@end
