//
//  MenuProfileViewController.m
//  LuxuryCard
//
//  Created by Viet Vo on 9/7/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "MenuProfileViewController.h"

#import "UtilStyle.h"

#import "SWRevealViewController.h"
#import "MyProfileViewController.h"
#import "PreferencesViewController.h"
#import "ChangePasswordViewController.h"

@interface MenuProfileViewController () <UITableViewDelegate, UITableViewDataSource> {
    NSArray *contentList;
}

@end

@implementation MenuProfileViewController

- (void)viewDidLoad {
    isNotAskConciergeBarButton = NO;
    [super viewDidLoad];
    [self createMenuBarButton];
    [self setNavigationBarWithDefaultColorAndTitle:@"MY PROFILE"];
    [self setupTableView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.contentTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup UI
- (void)setupTableView{
    contentList = @[@"CONTACT INFO",@"PREFERENCES", @"CHANGE PASSWORD"];
    self.contentTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.contentTableView.alwaysBounceVertical = NO;
    [self.contentTableView setDelegate:self];
    [self.contentTableView setDataSource:self];
}


#pragma mark - UITableView DataSource & Delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0f * SCREEN_SCALE;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return contentList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"MenuProfileCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (nil == cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.text = [contentList objectAtIndex:indexPath.row];
    cell.textLabel.textColor = [UIColor grayColor];
    cell.textLabel.attributedText = [[NSAttributedString alloc] initWithString:[contentList objectAtIndex:indexPath.row] attributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_MarkForMC_MED size:18.0f * SCREEN_SCALE],NSKernAttributeName:@(1.2)}];
    
//    UIView *bgColor = [[UIView alloc] init];
//    bgColor.backgroundColor = [UtilStyle colorForSeparateLine];
//    [cell setSelectedBackgroundView:bgColor];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 80,(44.0f * SCREEN_SCALE - 30)/2, 30, 30)];
        imageView.image=[UIImage imageNamed:@"right_arrow_black"];
        [cell.contentView addSubview:imageView];
    });
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.textColor = [UIColor grayColor];
    cell.textLabel.textColor = colorFromHexString(BUTTON_BG_COLOR_HIGHLIGHT);
    
    for (UIView *view in cell.contentView.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            UIImageView *imageView = (UIImageView*)view;
            imageView.image = [UIImage imageNamed:@"right_arrow_red"];
        }
    }
    
    switch (indexPath.row) {
        case 0:
        {
            MyProfileViewController *myProfile = [[MyProfileViewController alloc] init];
            [self.navigationController pushViewController:myProfile animated:YES];
        }
            break;
        case 1:
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            PreferencesViewController *preferenceViewController = [storyboard instantiateViewControllerWithIdentifier:@"PreferencesViewController"];
            [self.navigationController pushViewController:preferenceViewController animated:YES];
            
        }
            break;
        case 2:
        {
            ChangePasswordViewController *changePassword = [[ChangePasswordViewController alloc] init];
            [self.navigationController pushViewController:changePassword animated:YES];
        }
            break;
        default:
            break;
    }

}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.textColor = [UIColor grayColor];
    cell.textLabel.textColor = colorFromHexString(BUTTON_BG_COLOR_HIGHLIGHT);
    
    for (UIView *view in cell.contentView.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            UIImageView *imageView = (UIImageView*)view;
            imageView.image = [UIImage imageNamed:@"right_arrow_red"];
        }
    }
}

- (void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.textColor = [UIColor grayColor];
    
    for (UIView *view in cell.contentView.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            UIImageView *imageView = (UIImageView*)view;
            imageView.image = [UIImage imageNamed:@"right_arrow_black"];
        }
    }
}

@end
