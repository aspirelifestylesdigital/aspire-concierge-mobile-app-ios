//
//  MenuProfileViewController.h
//  LuxuryCard
//
//  Created by Viet Vo on 9/7/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "BaseViewController.h"

@interface MenuProfileViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITableView *contentTableView;

@end
