//
//  ExploreDetailHeaderImageView.m
//  TestLink
//
//  Created by Nghia Dinh on 7/18/17.
//  Copyright © 2017 Nghia Dinh. All rights reserved.
//

#import "ExploreDetailHeaderImageView.h"
#import "UIImageView+AFNetworking.h"
#import "Common.h"
#import "Constant.h"
#import "NSString+Utis.h"
#import "ImageUtilities.h"
#import "LinkLabel.h"

@interface ExploreDetailHeaderImageView() <TTTAttributedLabelDelegate>
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpaceButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSpaceButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomLabelNameConstraint;
@property (weak, nonatomic) IBOutlet UIButton *seeMapButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintButton;
@property (weak, nonatomic) IBOutlet LinkLabel *webLinkLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightImageConstraint;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet LinkLabel *addressLabel;
@property (weak, nonatomic) IBOutlet LinkLabel *seeMapLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topImageConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomLabelConstraint;
@property NSString *address;

@end

@implementation ExploreDetailHeaderImageView


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self customInit];
    }
    return self;
}


-(void)customInit {
    [[NSBundle mainBundle] loadNibNamed:@"ExploreDetailHeaderImageView" owner:self options:nil];
    [self addSubview:_contentView];
    self.contentView.frame = self.frame;
    
    
    [self.nameLabel setFont:[UIFont fontWithName:@"Georgia" size:36.0*SCREEN_SCALE]];
    
    //self.leadingConstraint.constant = self.leadingConstraint.constant*SCREEN_SCALE;
    //self.topButtonsConstraint.constant = self.topButtonsConstraint.constant*SCREEN_SCALE;
    //self.topQuestionConstraint.constant = self.topQuestionConstraint.constant*SCREEN_SCALE;
    
//    [self.btnShare setBackgroundImage:[UIImage imageNamed:@"share_icon"] forState:UIControlStateNormal];
//    [self.btnShare setBackgroundImage:[UIImage imageNamed:@"share_icon_interact"] forState:UIControlStateHighlighted];
    
    [self.btnAskConcierge setBackgroundImage:[UIImage imageNamed:@"concierge_icon"] forState:UIControlStateNormal];
    [self.btnAskConcierge setBackgroundImage:[UIImage imageNamed:@"concierge_icon_interaction"] forState:UIControlStateHighlighted];
    [self.btnAskConcierge setBackgroundImage:[UIImage imageNamed:@"concierge_icon_interaction"] forState:UIControlStateSelected];
    
    
    
    
    UIFont *font = [UIFont fontWithName:FONT_MarkForMC_LIGHT size:18*SCREEN_SCALE];
    
    UIColor *textColor = colorFromHexString(@"#011627");
   
    
    
    
    NSDictionary *attrs = @{ NSForegroundColorAttributeName : textColor,
                             NSFontAttributeName: font,
                             NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)
                             };
    NSAttributedString *buttonString = [[NSAttributedString alloc] initWithString:@"See Map" attributes:attrs];
    self.heightConstraintButton.constant = 30 * SCREEN_SCALE;

    self.seeMapButton.titleLabel.attributedText = buttonString;
    
    self.webLinkLabel.font = font;
    
    
    _webLinkLabel.delegate = self;
    _addressLabel.delegate = self;
    
    if (IPAD) {
        _topSpaceButton.constant = 10;
        _bottomSpaceButton.constant = 10;
    } else {
        _topSpaceButton.constant = 5;
        _bottomSpaceButton.constant = 5;
    }
    [self layoutIfNeeded];
}


- (IBAction)touchAskConcierge:(id)sender {
    self.btnAskConcierge.highlighted = YES;
    self.btnAskConcierge.selected = YES;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.btnAskConcierge.highlighted = NO;
        self.btnAskConcierge.selected = NO;
        if ([self.delegate respondsToSelector:@selector(touchAskConcierge)]){
            [self.delegate touchAskConcierge];
        }
    });
}
- (IBAction)touchShare:(id)sender {
    NSMutableArray *data = [[NSMutableArray alloc] init];
    if(self.nameLabel.text.length > 0) {
        [data addObject:[NSString stringWithFormat:@"%@\n",self.nameLabel.text]];
    }
    [data addObject:[NSURL URLWithString:MASTERCARD_SHARING]];
    if(self.headImg.image) {
        [data addObject:self.headImg.image];
    }
    if ([self.delegate respondsToSelector:@selector(shareUrl:)]){
        [self.delegate shareUrl:data];
    }
}

- (void)loadData{
    if(self.imageURL)
    {
        __weak typeof(self) weakCell = self;
        //__weak typeof(self.tableView) weakTable = self.tableView;
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[self.imageURL removeBlankFromURL]]] ;
        [weakCell removeConstraint:_topImageConstraint];
        [weakCell removeConstraint:_bottomLabelConstraint];
        [weakCell.headImg.topAnchor constraintEqualToAnchor:weakCell.topAnchor constant:0].active = YES;
        [weakCell.webLinkLabel.bottomAnchor constraintEqualToAnchor:weakCell.bottomAnchor constant:-5].active = YES;
        [self.headImg setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"placehoder_explore_detail"] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
            float imgWidth = image.size.width;
            image = [ImageUtilities imageWithImage:image convertToSize:CGSizeMake(SCREEN_WIDTH, (SCREEN_WIDTH/imgWidth)*image.size.height)];
            weakCell.heightImageConstraint.constant = image.size.height;
                [UIView animateWithDuration:0.1 animations:^{
                    [self.superview.superview.superview layoutIfNeeded];
                } completion:^(BOOL finished) {
                    weakCell.headImg.image = image;
                }];            
        } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
        }];
    }
    self.nameLabel.text = self.questionName;
    //[self.lblQuestion sizeToFit];
}

-(void)setupDataAddress:(BaseItem*)item withType:(DetailType)type withContentFullCA:(ContentFullCCAItem*)content; {
    
    if (type != DetailType_Other) {
        self.questionName = item.name;
        
        if(type == DetailType_CityGuide){
            float imgWidth = item.image.size.width;
            UIImage *image = [ImageUtilities imageWithImage:item.image convertToSize:CGSizeMake(SCREEN_WIDTH, (SCREEN_WIDTH/imgWidth)*item.image.size.height)];
            self.heightImageConstraint.constant = image.size.height;
            //dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:0.1 animations:^{
                [self.superview.superview.superview layoutIfNeeded];
            } completion:^(BOOL finished) {
                self.headImg.image = image;
            }];
        }
        else{
            self.imageURL = item.imageURL;
        }
        
        //headerView.diningDetailHeaderTableViewCellDelegate = self;
    }else{
        self.questionName = content.title;
        self.imageURL = [content.imgURL isEqualToString:@""] ? item.imageURL : content.imgURL;
        self.shareLink = @"";
        //headerView.diningDetailHeaderTableViewCellDelegate = self;
    }
    [self loadData];
    
    if (type == DetailType_CityGuide || type == DetailType_Dining) {
        NSString *address1;
        if (type == DetailType_CityGuide) {
            address1 = [NSString stringWithFormat:@"%@ %@, %@ %@ %@ %@",((AnswerItem*)item).address1, ((AnswerItem*)item).cityName, ((AnswerItem*)item).state, ((AnswerItem*)item).zipCode, ((AnswerItem*)item).address3, ((AnswerItem*)item).address2];
            //        cell.address3 = ((AnswerItem*)item).address2;
        }else{
            address1 = [NSString stringWithFormat:@"%@, %@, %@, %@ %@",((AnswerItem*)item).address1, ((AnswerItem*)item).cityName, ((AnswerItem*)item).state, ((AnswerItem*)item).zipCode, ((AnswerItem*)item).address3];
            //        cell.address3 = ((AnswerItem*)item).address3;
        }
        
        address1 = [address1 stringByReplacingOccurrencesOfString:@", ," withString:@","];
        if ([address1 hasSuffix:@","]) {
            address1 = [address1 substringToIndex:[address1 length]-1];
        }
        address1 = [NSString stringWithFormat:@"%@", address1];
        _address = address1;
        
        NSString *webLink = ((AnswerItem*)item).URL;
        NSMutableParagraphStyle *paragraphLineStyle = [[NSMutableParagraphStyle alloc] init];
        //paragraphStyle.lineSpacing = 4;
        UIColor *textColor = colorFromHexString(@"#011627");
        UIFont *font = [UIFont fontWithName:FONT_MarkForMC_LIGHT size:18.0*SCREEN_SCALE];
        if (IPAD) {
            [paragraphLineStyle setLineHeightMultiple:0.5];
        } else {
            [paragraphLineStyle setLineHeightMultiple:0.5 * SCREEN_SCALE];
        }
        NSDictionary *lineAttrs = @{
                                    NSForegroundColorAttributeName : textColor,
                                     NSFontAttributeName: font,
                                 NSParagraphStyleAttributeName:paragraphLineStyle
                                 };
         NSMutableAttributedString *attLineText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n\n"] attributes:lineAttrs];
        
        
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        //paragraphStyle.lineSpacing = 4;
        
        NSMutableAttributedString *contentText = [[NSMutableAttributedString alloc] initWithString:@""];
        if (IPAD) {
            [paragraphStyle setLineHeightMultiple:1.0];
        } else {
            [paragraphStyle setLineHeightMultiple:1.1 * SCREEN_SCALE];
        }
        NSDictionary *attrs = @{ NSForegroundColorAttributeName : textColor,
                                 NSFontAttributeName: font,
                                 NSParagraphStyleAttributeName:paragraphStyle
                                 };
        NSMutableAttributedString *attText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",address1] attributes:attrs];
        [contentText appendAttributedString:attLineText];
        [contentText appendAttributedString:attText];
        [contentText appendAttributedString:attLineText];
        
        NSMutableParagraphStyle *mapParagraphStyle = [[NSMutableParagraphStyle alloc] init];
        if (IPAD) {
            [paragraphStyle setLineHeightMultiple:1.0];
        } else {
            [mapParagraphStyle setLineHeightMultiple:1.1 * SCREEN_SCALE];
        }
        NSDictionary *mapAttrs = @{ NSForegroundColorAttributeName : textColor,
                                 NSFontAttributeName: font,
                                 NSParagraphStyleAttributeName:mapParagraphStyle
                                 };
        NSMutableAttributedString *attMapText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",webLink] attributes:mapAttrs];

        _addressLabel.text = contentText;
        _webLinkLabel.isCheckHTTPLink = YES;
        _webLinkLabel.enabledTextCheckingTypes = NSTextCheckingTypeLink;
        _webLinkLabel.text =  attMapText;
        
    } else {
        _addressLabel.text = nil;
        _webLinkLabel.text = nil;
        [_seeMapButton removeFromSuperview];
        [_webLinkLabel removeFromSuperview];
        [_addressLabel removeFromSuperview];
        [_nameLabel.bottomAnchor constraintEqualToAnchor:self.bottomAnchor constant:-5.0].active = true;
    }
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    if ([self.delegate respondsToSelector:@selector(gotoWebBrowser:)]){
         [self.delegate gotoWebBrowser:url];
    }
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithTransitInformation:(NSDictionary *)components {
    NSString *googleAdress = [components valueForKey:@"GoogleAddress"];
    if (googleAdress.length > 0) {
         if ([self.delegate respondsToSelector:@selector(seeDetailMapWithAddress:)]){
             [self.delegate seeDetailMapWithAddress:googleAdress];
         }
    }
}

-(void)attributedLabel:(TTTAttributedLabel *)label didLongPressLinkWithURL:(NSURL *)url atPoint:(CGPoint)point {
    if ([self.delegate respondsToSelector:@selector(gotoWebBrowser:)]){
        [self.delegate gotoWebBrowser:url];
    }
}
-(void)attributedLabel:(TTTAttributedLabel *)label didLongPressLinkWithTransitInformation:(NSDictionary *)components atPoint:(CGPoint)point {
    NSString *googleAdress = [components valueForKey:@"GoogleAddress"];
    if (googleAdress.length > 0) {
        if ([self.delegate respondsToSelector:@selector(seeDetailMapWithAddress:)]){
            [self.delegate seeDetailMapWithAddress:googleAdress];
        }
    }
}
-(IBAction)seeMapButtonTapped:(id)sender {
    if ([self.delegate respondsToSelector:@selector(seeDetailMapWithAddress:)]){
        [self.delegate seeDetailMapWithAddress:_address];
    }
}







@end
