//
//  ExploreDetailDescriptionView.h
//  TestLink
//
//  Created by Nghia Dinh on 7/18/17.
//  Copyright © 2017 Nghia Dinh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseItem.h"
#import "ExploreDetailViewController.h"
#import "ContentFullCCAItem.h"

typedef NS_ENUM( NSInteger, TypeDescriptionView){
    ViewOperatorHours,
    ViewDescription,
    ViewTerm,
    ViewDefaultTerm
};

@protocol  ExploreDetailDescriptionDelegate <NSObject>
-(void)didSelectURL:(NSURL*)url;
@end

@interface ExploreDetailDescriptionView : UIView
@property (weak, nonatomic) id<ExploreDetailDescriptionDelegate> delegate;
-(void)setupData:(BaseItem*)item withDetailType:(DetailType)detailType withFullContent:(ContentFullCCAItem*)content withType:(TypeDescriptionView)type;

@end
