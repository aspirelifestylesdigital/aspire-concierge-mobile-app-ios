//
//  ExploreDetailHeaderImageView.h
//  TestLink
//
//  Created by Nghia Dinh on 7/18/17.
//  Copyright © 2017 Nghia Dinh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseItem.h"
#import "AnswerItem.h"
#import "ExploreDetailViewController.h"
#import "ContentFullCCAItem.h"


@protocol ExploreDetailHeaderImageViewDelegate <NSObject>
-(void)shareUrl:(NSArray *)data;
-(void)touchAskConcierge;
-(void)seeDetailMapWithAddress:(NSString*)address;
-(void)gotoWebBrowser:(NSURL*)url;
@end

@interface ExploreDetailHeaderImageView : UIView

-(void)setupDataAddress:(BaseItem*)item withType:(DetailType)type withContentFullCA:(ContentFullCCAItem*)content;


@property (weak, nonatomic) IBOutlet UIImageView *headImg;

- (IBAction)touchAskConcierge:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnAskConcierge;

@property (weak, nonatomic) IBOutlet UIButton *btnShare;
- (IBAction)touchShare:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConstraint;
@property (weak, nonatomic) id<ExploreDetailHeaderImageViewDelegate> delegate;


// Input data
@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic, strong) NSString *cityName;
@property (nonatomic, strong) NSString *questionName;
@property (nonatomic, strong) NSString *shareLink;



@end
