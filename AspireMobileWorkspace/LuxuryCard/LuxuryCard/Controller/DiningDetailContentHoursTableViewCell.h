//
//  DiningDetailContentHoursTableViewCell.h
//  MobileConcierge
//
//  Created by user on 6/8/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface DiningDetailContentHoursTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblHour;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConstraint;
@property (strong, nonatomic) NSString *workingHour;
@end
