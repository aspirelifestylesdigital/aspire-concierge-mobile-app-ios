//
//  DiningDetailContentBenefitsTableViewCell.m
//  MobileConcierge
//
//  Created by user on 6/8/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "DiningDetailContentBenefitsTableViewCell.h"
#import "UILabel+Extension.h"
#import "NSString+Utis.h"

@implementation DiningDetailContentBenefitsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.leadingConstraint.constant = self.leadingConstraint.constant*SCREEN_SCALE;
    self.webView.scrollView.scrollEnabled = NO;
    self.webView.scrollView.contentInset = UIEdgeInsetsMake(0, -8, 0, 0);
    self.webView.opaque = NO;
     self.webView.tintColor = colorFromHexString(@"#011627");
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)loadData{    
    if (!self.webView.delegate) {
        self.webView.delegate = self;
        self.benefits = [self.benefits removeRedudantNewLineInText];
        NSRange range = [self.benefits rangeOfString:@"Click here"];
        if (range.location != NSNotFound) {
            self.benefits = [self.benefits substringToIndex:range.location];
            self.benefits = [NSString stringWithFormat:@"%@ </p>",self.benefits];
        }
        range = [self.benefits rangeOfString:@"Contact us"];
        if (range.location != NSNotFound) {
            self.benefits = [self.benefits substringToIndex:range.location];
            self.benefits = [NSString stringWithFormat:@"%@ </p>",self.benefits];
        }
        range = [self.benefits rangeOfString:@"Contact&nbsp;us"];
        if (range.location != NSNotFound) {
            self.benefits = [self.benefits substringToIndex:range.location];
            self.benefits = [NSString stringWithFormat:@"%@ </p>",self.benefits];
        }
        if(self.benefits.length > 0)
        {
            self.benefits = [self.benefits stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; color:#011627;font-size:%fpx;line-height: 22px;}</style>",FONT_MarkForMC_LIGHT, 18.0f*SCREEN_SCALE]];
        }

        [self.webView loadHTMLString:[NSString stringWithFormat:@"%@",self.benefits] baseURL:nil];
        [self layoutIfNeeded];
    }
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if (navigationType == UIWebViewNavigationTypeLinkClicked ) {
        [self.diningDetailContentBenefitsTableViewCellDelegate gotoBenefitsWebBrowser:webView shouldStartLoadWithRequest:request navigationType:navigationType];
        return NO;
    }
    
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if(self.benefits.length > 0)
    {
        CGRect frame = self.webView.frame;
        frame.size.height = 1;
        self.webView.frame = frame;
        CGSize fittingSize = [self.webView sizeThatFits:CGSizeZero];
        frame.size = fittingSize;
        self.webView.frame = frame;
        
        self.heightWebView.constant = fittingSize.height;
        [self.diningDetailContentBenefitsTableViewCellDelegate reloadBenefitsCell:self andHeight:fittingSize.height];
    }
    else{
        if(!self.isOffer)
        {
            self.lblBenefits.hidden = YES;
            self.heightLblBenefits.constant = 0.01f;
            self.heightWebView.constant = 0.01f;
            self.heightTopConstraint.constant = 0.01f;
            self.decoverImageView.hidden = YES;
        }
        
        CGRect frame = self.webView.frame;
        frame.size.height = 1;
        self.webView.frame = frame;
        
        
        self.heightWebView.constant = 1;
        [self.diningDetailContentBenefitsTableViewCellDelegate reloadBenefitsCell:self andHeight:1];
    }
}

@end
