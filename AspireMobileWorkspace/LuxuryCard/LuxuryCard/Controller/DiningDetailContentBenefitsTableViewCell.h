//
//  DiningDetailContentBenefitsTableViewCell.h
//  MobileConcierge
//
//  Created by user on 6/8/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "BaseTableViewCell.h"
@class DiningDetailContentBenefitsTableViewCell;
@protocol DiningDetailContentBenefitsTableViewCellDelegate <NSObject>

- (void) gotoBenefitsWebBrowser:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType;
- (void) reloadBenefitsCell:(DiningDetailContentBenefitsTableViewCell*)cell andHeight:(float)height;

@end
@interface DiningDetailContentBenefitsTableViewCell : BaseTableViewCell <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConstraint;
@property (weak, nonatomic) IBOutlet UILabel *lblBenefits;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightWebView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightLblBenefits;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTopConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *decoverImageView;
@property (assign, nonatomic) BOOL isOffer;

@property (strong, nonatomic) NSString *benefits;
@property (strong, nonatomic) id<DiningDetailContentBenefitsTableViewCellDelegate> diningDetailContentBenefitsTableViewCellDelegate;
@end
