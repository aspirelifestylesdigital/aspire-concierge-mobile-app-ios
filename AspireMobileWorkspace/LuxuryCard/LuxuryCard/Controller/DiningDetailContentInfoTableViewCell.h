//
//  DiningDetailContentInfoTableViewCell.h
//  MobileConcierge
//
//  Created by user on 6/8/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "BaseTableViewCell.h"

@class DiningDetailContentInfoTableViewCell;
@protocol DiningDetailContentInfoTableViewCellDelegate <NSObject>

- (void) seeDetailMapWithAddress:(NSString*)address;
- (void) gotoWebBrowser;

@end
@interface DiningDetailContentInfoTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblAddress1;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress3;
@property (weak, nonatomic) IBOutlet UIButton *btnSeeMap;
- (IBAction)touchSeeMap:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnWebLink;
- (IBAction)touchWebLink:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConstraint;

@property (nonatomic, strong) NSString *address1;
@property (nonatomic, strong) NSString *address3;
@property (nonatomic, strong) NSString *googleAddress;
@property (nonatomic, strong) NSString *webLink;
@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic, strong) NSString *questionName;
@property (nonatomic, strong) id<DiningDetailContentInfoTableViewCellDelegate> diningDetailContentInfoTableViewCellDelegate;
@end
