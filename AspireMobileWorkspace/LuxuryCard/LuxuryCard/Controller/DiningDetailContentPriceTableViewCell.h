//
//  DiningDetailContentPriceTableViewCell.h
//  MobileConcierge
//
//  Created by user on 6/8/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface DiningDetailContentPriceTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConstraint;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceRange;
@property (weak, nonatomic) IBOutlet UILabel *lblCuisine;

@property (nonatomic, strong) NSString *priceRange;
@property (nonatomic, strong) NSString *cuisine;
@end
