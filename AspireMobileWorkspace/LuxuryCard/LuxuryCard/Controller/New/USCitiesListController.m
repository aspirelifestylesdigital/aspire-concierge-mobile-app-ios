//
//  USCitiesListController.m
//  TestIOS
//
//  Created by Dai Pham on 7/17/17.
//  Copyright © 2017 Dai Pham. All rights reserved.
//

// Components
#import "CitiesManager.h"

//Model
#import "AppData.h"

// views
#import "TableSimpleView.h"

// controllers
#import "USCitiesListController.h"
#import "HomeViewController.h"
#import "USExploreListController.h"

@interface USCitiesListController ()<TableSimpleViewProtocol> {
    TableSimpleView* viewSimple;
}

@end

@implementation USCitiesListController

#pragma mark - 🚸 INIT 🚸
- (instancetype)init {
    if(self = [super init]) {
        viewSimple = [TableSimpleView new];
        viewSimple.delegate = self;
        self.view = viewSimple;
        // !!!: TEST
//        [viewSimple reloadWithData:[[CitiesManager shared] getListCitiesForDining]];
        [viewSimple reloadWithData:[AppData getSelectionCityList]];
        
        [self viewDidLoad];
    }
    
    return self;
}

- (void) viewDidLoad {
    [super viewDidLoad];
    
    [self backNavigationItem];
    self.navigationItem.title = NSLocalizedString(@"city_title", nil);
    
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"city_title", nil)];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //  Set up Pop pan gesture
    [self setUpCustomizedPanGesturePopRecognizer];
//    if(self.navigationController.viewControllers.count == 1)
//    {
//        HomeViewController *homeView = [[HomeViewController alloc] init];
//        [self.navigationController setViewControllers:@[homeView,self]];
//    }
//    else{
//        UIViewController *vc = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count - 2];
//        if(![vc isKindOfClass:[ExploreViewController class]])
//        {
//            isFirstSelectCity = YES;
//        }
//    }
}

-(void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    // Stop being the navigation controller's delegate
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}

#pragma mark - 🚸 CITIES LIST VIEW DELEGATE 🚸
- (void)TableSimpleView:(TableSimpleView *)view onSelectItem:(id)item {
    printf("%s",[[NSString stringWithFormat:@"\n%s\n",__PRETTY_FUNCTION__] UTF8String]);
}

@end
