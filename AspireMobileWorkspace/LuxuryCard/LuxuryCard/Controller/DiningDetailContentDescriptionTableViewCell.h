//
//  DiningDetailContentDescriptionTableViewCell.h
//  MobileConcierge
//
//  Created by user on 6/8/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "BaseTableViewCell.h"
@class DiningDetailContentDescriptionTableViewCell;
@protocol DiningDetailContentDescriptionTableViewCellDelegate <NSObject>

- (void) openDescriptionURL:(NSURL*)url;

@end
@interface DiningDetailContentDescriptionTableViewCell : BaseTableViewCell<UIWebViewDelegate, UITextViewDelegate>
/*
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConstraint;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (strong, nonatomic) NSString *descriptions;
 */

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConstraint;
@property (strong, nonatomic) NSString *cellDescription;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightWebView;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTitleLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleTopConstrain;
@property (weak, nonatomic) IBOutlet UIView *topLine;
@property (weak, nonatomic) IBOutlet UITextView *tvContent;

@property (weak, nonatomic) id<DiningDetailContentDescriptionTableViewCellDelegate>
diningDetailContentDescriptionTableViewCellDelegate;

@end
