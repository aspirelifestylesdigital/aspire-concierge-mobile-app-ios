//
//  BaseViewController.m
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/12/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "BaseViewController.h"
#import "SWRevealViewController.h"
#import "Constant.h"
#import "Common.h"
#import "UIView+Extension.h"
#import "AskConciergeViewController.h"
#import "CustomPopTransition.h"
#import "CategoryViewController.h"
#import "AlertViewController.h"
#import "ExploreDetailViewController.h"
#import "ExploreVenueDetailViewController.h"

// views
#import "ViewLog.h"

// library
#import "NSTimer+Block.h"

@interface BaseViewController () <AlertViewControllerDelegate>
{
    BOOL didUpdateLayout;
    
    ViewLog* vwLog;
}
@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    didUpdateLayout = NO;
    if(!didUpdateLayout && !isIgnoreScaleView){
        didUpdateLayout = YES;
        resetScaleViewBaseOnScreen(self.view);
    }
    
    if(!isNotAskConciergeBarButton)
    {
        [self createConciergeBarButton];
    }
    
    [self.view setBackgroundColorForView];
    if(!isNotChangeNavigationBarColor)
    {
       [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"concierge", nil)];
    }
    [self initData];
    [self initView];
   
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];

}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) touchDown:(UIGestureRecognizer *)gesture {
    [self hidenKeyboardWhenSwipe];
}
-(void)hidenKeyboardWhenSwipe{

}

- (UIImageView*) createIndicator{
    if (!indicatorImageView) {
        indicatorImageView = [[UIImageView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - SPINNER_BACKGROUND_WIDTH)/2.0,
                                                                           (SCREEN_HEIGHT - SPINNER_BACKGROUND_HEIGHT-NAVIGATION_HEIGHT)/2.0,
                                                                           SPINNER_BACKGROUND_WIDTH*SCREEN_SCALE, SPINNER_BACKGROUND_HEIGHT*SCREEN_SCALE)];
        
        [indicatorImageView setImage:[UIImage imageNamed:@"activity_indicator_icon_1"]];
        //Add more images which will be used for the animation
        indicatorImageView.animationImages =  [NSArray arrayWithObjects:
                                               [UIImage imageNamed:@"activity_indicator_icon_1"],
                                               [UIImage imageNamed:@"activity_indicator_icon_2"],
                                               [UIImage imageNamed:@"activity_indicator_icon_3"],
                                               [UIImage imageNamed:@"activity_indicator_icon_4"],
                                               [UIImage imageNamed:@"activity_indicator_icon_5"],
                                               [UIImage imageNamed:@"activity_indicator_icon_6"],
                                               [UIImage imageNamed:@"activity_indicator_icon_7"],
                                               [UIImage imageNamed:@"activity_indicator_icon_8"],
                                               [UIImage imageNamed:@"activity_indicator_icon_9"],
                                               [UIImage imageNamed:@"activity_indicator_icon_10"],
                                               [UIImage imageNamed:@"activity_indicator_icon_11"],
                                               [UIImage imageNamed:@"activity_indicator_icon_12"],
                                               nil];
        indicatorImageView.animationDuration = 1.0f;
    }
    
    return indicatorImageView;
}

//- (void)appWillEnterForeground:(NSNotification*) noti{
//    [self checkNetworkStatus];
//}
//
//- (void) checkNetworkStatus{
//    if(!isNetworkAvailable())
//    {
//        [self showNetWorkingStatusArlet];
//    }
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initData{
}

- (void)initView{
}
- (void)changePositionForView{
    
}
-(void)dealloc
{
    #ifdef DEBUG
    NSLog(@"dealloc view controller: %@",NSStringFromClass([self class]));
    #endif
}

-(void)createMenuBarButton
{
    SWRevealViewController *revealViewController = [self revealViewController];
    [revealViewController tapGestureRecognizer];
    [revealViewController panGestureRecognizer];
    
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [menuButton addTarget:self action:@selector(showMenu) forControlEvents:UIControlEventTouchUpInside];
    [menuButton setBackgroundImage:[UIImage imageNamed:@"menu_white_icon"] forState:UIControlStateNormal];
     [menuButton setBackgroundImage:[UIImage imageNamed:@"menu_interaction_icon"] forState:UIControlStateHighlighted];
    UIBarButtonItem *menuButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    self.navigationItem.leftBarButtonItem = menuButtonItem;
}

- (void)showMenu{
    
    SWRevealViewController *revealViewController = [self revealViewController];
    [revealViewController revealToggle:self];
}

-(void) createConciergeBarButton
{
    UIButton *conciergeButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 30.0f, 30.0f)];
    [conciergeButton addTarget:self action:@selector(askConciergeAction:) forControlEvents:UIControlEventTouchUpInside];
    [conciergeButton setBackgroundImage:[UIImage imageNamed:@"bell_white_icon"] forState:UIControlStateNormal];
    [conciergeButton setBackgroundImage:[UIImage imageNamed:@"bell_interaction_icon"] forState:UIControlStateHighlighted];

    UIBarButtonItem *conciergeBarItem = [[UIBarButtonItem alloc] initWithCustomView:conciergeButton];
    self.navigationItem.rightBarButtonItem = conciergeBarItem;
}

-(void)createBackBarButton
{
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
    [menuButton addTarget:self action:@selector(touchBack) forControlEvents:UIControlEventTouchUpInside];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:(14.0f * SCREEN_SCALE)],
                                 NSKernAttributeName: @1.6};
    [menuButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"BACK" attributes:attributes] forState:UIControlStateNormal];
    
    NSDictionary *attributesHighlight = @{NSForegroundColorAttributeName: colorFromHexString(DEFAULT_HIGHLIGHT_COLOR),
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:(14.0f * SCREEN_SCALE)],
                                 NSKernAttributeName: @1.6};
    [menuButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"BACK" attributes:attributesHighlight] forState:UIControlStateHighlighted];

    /*
    [menuButton setBackgroundImage:[UIImage imageNamed:@"back_white_icon"] forState:UIControlStateNormal];
    [menuButton setBackgroundImage:[UIImage imageNamed:@"back_interaction_icon"] forState:UIControlStateHighlighted];
     */
    UIBarButtonItem *menuButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    self.navigationItem.leftBarButtonItem = menuButtonItem;
    
    //  Set up Pop pan gesture
    [self setUpCustomizedPanGesturePopRecognizer];
}

-(void)createBackBarButtonWithIconName:(NSString*)iconName
{
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [menuButton addTarget:self action:@selector(touchBack) forControlEvents:UIControlEventTouchUpInside];
    [menuButton setBackgroundImage:[UIImage imageNamed:iconName] forState:UIControlStateNormal];
    [menuButton setBackgroundImage:[UIImage imageNamed:iconName] forState:UIControlStateHighlighted];
    UIBarButtonItem *menuButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    self.navigationItem.leftBarButtonItem = menuButtonItem;
    
    //  Set up Pop pan gesture
    [self setUpCustomizedPanGesturePopRecognizer];
}

- (void) touchBack{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)askConciergeAction:(id)sender
{
    AskConciergeViewController *vc = [[AskConciergeViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)askConciergeActionFromNavigation {
    // Implement from sub class.
}

//-(void)revealToggle:(id)sender
//{
//    SWRevealViewController *revealController = [self revealViewController];
//    [revealController setFrontViewPosition:FrontViewPositionRightMostRemoved animated:YES];
//}

-(void)decorateForButton:(UIButton *)button
{
    [button.layer setBorderWidth:1.0f];
    [UIColor colorWithRed:1 green:1 blue:1 alpha:0];
}

- (void)rotateLayerInfinite:(CALayer *)layer
{
    CABasicAnimation *rotation;
    rotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    rotation.fromValue = [NSNumber numberWithFloat:0];
    rotation.toValue = [NSNumber numberWithFloat:(2 * M_PI)];
    rotation.duration = 0.7f; // Speed
    rotation.repeatCount = HUGE_VALF; // Repeat forever. Can be a finite number.
    [layer removeAllAnimations];
    [layer addAnimation:rotation forKey:@"Spin"];
}

#pragma mark LOADING
- (void) checkCreateMaskIndicator {
    
    BOOL isExistMask = [self.view.subviews containsObject:maskViewForSpinner];
    
    if(!maskViewForSpinner) {
        UIView *maskView = [[UIView alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT)];
        maskView.backgroundColor = [UIColor clearColor];
        maskView.alpha = 1;
        maskViewForSpinner = maskView;
    }
    
    if(!isExistMask)
        [self.view addSubview:maskViewForSpinner];
    
    BOOL isExist = [self.view.subviews containsObject:indicatorImageView];
    
    if(!isExist) {
        [self.view addSubview:[self createIndicator]];
        indicatorImageView.translatesAutoresizingMaskIntoConstraints = NO;
        [indicatorImageView.superview addConstraint:[NSLayoutConstraint constraintWithItem:indicatorImageView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:indicatorImageView.superview attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
        [indicatorImageView.superview addConstraint:[NSLayoutConstraint constraintWithItem:indicatorImageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:indicatorImageView.superview attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        [indicatorImageView addConstraint:[NSLayoutConstraint constraintWithItem:indicatorImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:SPINNER_BACKGROUND_WIDTH*SCREEN_SCALE]];
        [indicatorImageView addConstraint:[NSLayoutConstraint constraintWithItem:indicatorImageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:SPINNER_BACKGROUND_HEIGHT*SCREEN_SCALE]];
    }
    
    [self.view bringSubviewToFront:maskViewForSpinner];
    [self.view bringSubviewToFront:indicatorImageView];
}

- (void)startActivityIndicator
{
    printf("%s",[[NSString stringWithFormat:@"%s\n",__PRETTY_FUNCTION__] UTF8String]);
    dispatch_async(dispatch_get_main_queue(), ^{
        if (showingSpinner) {
            return;
        }
        
        [self checkCreateMaskIndicator];
        
        [maskViewForSpinner setHidden:NO];
        [indicatorImageView setHidden:NO];
        
        //Start the animation
        [indicatorImageView startAnimating];

        showingSpinner = YES;
    });
}

- (void)startActivityIndicatorWithoutMask
{
    printf("%s",[[NSString stringWithFormat:@"%s\n",__PRETTY_FUNCTION__] UTF8String]);
    dispatch_async(dispatch_get_main_queue(), ^{
        if (showingSpinner) {
            return;
        }
        
        [self checkCreateMaskIndicator];
        
        [maskViewForSpinner setHidden:YES];
        [indicatorImageView setHidden:NO];
        
        [indicatorImageView startAnimating];
        showingSpinner = YES;
    });
}

- (void)stopActivityIndicatorWithoutMask
{

    [self stopActivityIndicator];
}

- (void)stopActivityIndicator
{
    printf("%s",[[NSString stringWithFormat:@"%s\n",__PRETTY_FUNCTION__] UTF8String]);
    dispatch_async(dispatch_get_main_queue(), ^{
//        [indicatorImageView.layer removeAllAnimations];
        [maskViewForSpinner setHidden:YES];
        [indicatorImageView setHidden:YES];
        showingSpinner = NO;
    });
    
}

#pragma mark NAVIGATION
-(void) setNavigationBarWithDefaultColorAndTitle:(NSString*)title{
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBarTintColor:colorFromHexString(DEFAULT_BACKGROUND_COLOR)];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.navigationItem.titleView.frame.size.width, 50)];
    
    NSDictionary *attributes = @{NSForegroundColorAttributeName:colorFromHexString(DEFAULT_HIGHLIGHT_COLOR),
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:(14.0f * SCREEN_SCALE)],
                                 NSKernAttributeName: @1.6};
    titleLabel.attributedText = [[NSAttributedString alloc] initWithString:title.uppercaseString attributes:attributes];
    titleLabel.numberOfLines = 0;
//    titleLabel.adjustsFontSizeToFitWidth = YES;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    self.navigationItem.titleView = titleLabel;
}


-(void)setNavigationBarColor
{
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:1.0f/255.0f green:22.0f/255.0f blue:39.0f/255.0f alpha:1.0f]];
    [self.navigationController.navigationBar setTranslucent:NO];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName:[UIFont fontWithName:FONT_MarkForMC_Nrw_BOLD size:(18.0f * SCREEN_SCALE)]};
}

-(void)setNavigationBarWithColor:(UIColor *)color
{
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBarTintColor:color];
    [self.navigationController.navigationBar setTranslucent:NO];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : colorFromHexString(DEFAULT_HIGHLIGHT_COLOR), NSFontAttributeName:[UIFont fontWithName:FONT_MarkForMC_Nrw_BOLD size:(18.0f * SCREEN_SCALE)]};
}

-(void)backNavigationItem
{
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
    [menuButton addTarget:self action:@selector(backNavigationAction:) forControlEvents:UIControlEventTouchUpInside];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:(14.0f * SCREEN_SCALE)],
                                 NSKernAttributeName: @1.6};
    [menuButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"BACK" attributes:attributes] forState:UIControlStateNormal];
    
    NSDictionary *attributesHighlight = @{NSForegroundColorAttributeName:colorFromHexString(DEFAULT_HIGHLIGHT_COLOR),
                                          NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:(14.0f * SCREEN_SCALE)],
                                          NSKernAttributeName: @1.6};
    [menuButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"BACK" attributes:attributesHighlight] forState:UIControlStateHighlighted];
    /*
    [menuButton setBackgroundImage:[UIImage imageNamed:@"back_white_icon"] forState:UIControlStateNormal];
    [menuButton setBackgroundImage:[UIImage imageNamed:@"back_interaction_icon"] forState:UIControlStateHighlighted];
     */
    UIBarButtonItem *menuButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = menuButtonItem;
    
    //  Set up Pop pan gesture
    [self setUpCustomizedPanGesturePopRecognizer];
}

-(void)backNavigationAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)alterWithMessage:(NSString *)message
{
    UIAlertController *alterController = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok_button_title", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    [alterController addAction:cancel];
    [self presentViewController:alterController animated:YES completion:^{}];
}

- (void) showAlert:(AlertViewController *)alert forNavigation:(BOOL)isNavigationView
{
    alert.providesPresentationContextTransitionStyle = YES;
    alert.definesPresentationContext = YES;
    [alert setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    alert.alertViewControllerDelegate = self;
    if(self.navigationController.presentedViewController == nil)
    (isNavigationView)?[self.navigationController presentViewController:alert animated:NO completion:nil] : [self presentViewController:alert animated:NO completion:^{}];
}

- (void) showAlertMCStyleWithTitle:(NSString*)title andMessage:(NSString*)message andOkButtonTitle:(NSString *)okTitle
{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.providesPresentationContextTransitionStyle = YES;
    alert.definesPresentationContext = YES;
    [alert setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    //    [self.navigationController presentViewController:alert animated:YES completion:nil];
    alert.titleAlert = title;
    alert.msgAlert = message;
    alert.firstBtnTitle = okTitle;
    
    alert.alertViewControllerDelegate = self;
    //    [self presentModalViewController:alert animated:YES];
    [self.navigationController presentViewController:alert animated:NO completion:nil];
}

- (void)alertOkAction{
    
}

- (void)alertCancelAction{
    
}


//- (void) showNetWorkingStatusArlet{
//    UIAlertController *alterController = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"no_network_connection", nil) preferredStyle:UIAlertControllerStyleAlert];
//    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"arlet_cancel_button", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//    }];
//    UIAlertAction *retry = [UIAlertAction actionWithTitle:NSLocalizedString(@"arlet_retry_button", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        [self checkNetworkStatus];
//    }];
//    
//    [alterController addAction:cancel];
//    [alterController addAction:retry];
//    [self presentViewController:alterController animated:YES completion:^{}];
//}

#pragma mark CHANGE VIEW WHEN SCROLLVIEW
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if( [scrollView.panGestureRecognizer translationInView:self.view].y  < translationPointY) {
        [self changePositionForView:YES];
        translationPointY = [scrollView.panGestureRecognizer translationInView:self.view].y;
    } else if ([scrollView.panGestureRecognizer translationInView:self.view].y  > translationPointY) {
        [self changePositionForView:NO];
        translationPointY = [scrollView.panGestureRecognizer translationInView:self.view].y;
    }
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    translationPointY = [scrollView.panGestureRecognizer translationInView:self.view].y;
}

-(void)changePositionForView:(BOOL)isHide
{
}


#pragma mark CUSTOMIZED POP ACTION
-(void)setUpCustomizedPanGesturePopRecognizer
{
    self.navigationController.delegate = self;
    
//    if(isShouldHideKeyboardWhenSwipe) {
//        touchdown = [[TouchDownGestureRecognizer alloc] initWithTarget:self action:@selector(touchDown:)];
//        touchdown.delegate = self;
//        [self.view addGestureRecognizer:touchdown];
//    }
//
    if(!popRecognizer) {
        popRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePopRecognizer:)];
        //popRecognizer.delegate = self;
        [self.view addGestureRecognizer:popRecognizer];
    }
    
    if(!popNavigationBarRecognizer) {
        popNavigationBarRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePopRecognizer:)];
        //popNavigationBarRecognizer.delegate = self;
        [self.navigationController.navigationBar addGestureRecognizer:popNavigationBarRecognizer];
    }
}

-(void)removeSetupForCustomizedPanGesturePopRecognizer
{
    if (self.navigationController.delegate == self) {
        self.navigationController.delegate = nil;
    }
}


- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC {
    
    // Check if we're transitioning from this view controller to a DSLSecondViewController
    if (fromVC == self) //&& [toVC isKindOfClass:[ExploreViewController class]])
    {
        return [[CustomPopTransition alloc] init];
    }
    else {
        return nil;
    }
}

- (id<UIViewControllerInteractiveTransitioning>)navigationController:(UINavigationController *)navigationController interactionControllerForAnimationController:(id<UIViewControllerAnimatedTransitioning>)animationController {
    
    // Check if this is for our custom transition
    if ([animationController isKindOfClass:[CustomPopTransition class]]) {
        return self.interactivePopTransition;
    }
    else {
        return nil;
    }
}

- (void) returnNormalState{
}

- (void)handlePopRecognizer:(UIPanGestureRecognizer*)recognizer {
    
//    if(isAnimatingHideKeyboard) {
//        [self hidenKeyboardWhenSwipe];
//    }
    
    [self hidenKeyboardWhenSwipe];
    
    CGPoint velocity = [recognizer translationInView:self.view];
    if(velocity.x < 0)
    {
        [self returnNormalState];
        if(self.interactivePopTransition)
        {
            [self.interactivePopTransition cancelInteractiveTransition];
            self.interactivePopTransition = nil;
        }
        
        return;
    }
    
#ifdef DEBUG
    NSLog(@"Location x: %f",velocity.x);
#endif
    // Calculate how far the user has dragged across the view
    CGFloat progress = [recognizer translationInView:self.view].x / (self.view.bounds.size.width * 1.0);
    progress = MIN(1.0, MAX(0.0, progress));
    
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        // Create a interactive transition and pop the view controller
//        [self hidenKeyboardWhenSwipe];
        self.interactivePopTransition = [[UIPercentDrivenInteractiveTransition alloc] init];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (recognizer.state == UIGestureRecognizerStateChanged) {
        // Update the interactive transition's progress
        [self returnNormalState];
#ifdef DEBUG
        NSLog(@"UIGestureRecognizerStateChanged :%f",progress);
#endif
        [self.interactivePopTransition updateInteractiveTransition:progress];
    }
    else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
        // Finish or cancel the interactive transition
        if (progress > 0.5) {
            [self returnNormalState];
#ifdef DEBUG
            NSLog(@"finishInteractiveTransition");
#endif
            
            [self.interactivePopTransition finishInteractiveTransition];
            
            
        }
        else {
            [self returnNormalState];
#ifdef DEBUG
            NSLog(@"cancelInteractiveTransition");
#endif
            [self.interactivePopTransition cancelInteractiveTransition];
            
        }
        self.interactivePopTransition = nil;
    }
}

#pragma mark - Show error network
- (void)showErrorNetwork{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"cannot_get_data", nil);
    alert.msgAlert = NSLocalizedString(@"no_network_connection", nil);
    alert.firstBtnTitle = NSLocalizedString(@"settings_button", nil);
    alert.blockFirstBtnAction = ^(void){
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"prefs:root"]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root"]];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root"]];
        }
    };
    alert.secondBtnTitle = NSLocalizedString(@"alert_ok_button", nil);
    
    alert.blockSecondBtnAction = ^(void){
        [self stopActivityIndicator];
    };
    alert.providesPresentationContextTransitionStyle = YES;
    alert.definesPresentationContext = YES;
    [alert setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self presentViewController:alert animated:YES completion:nil];

}

- (void) forceRelease {
    
}
@end
