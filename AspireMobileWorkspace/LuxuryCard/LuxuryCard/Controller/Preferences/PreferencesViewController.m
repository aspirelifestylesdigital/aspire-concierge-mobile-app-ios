//
//  PreferencesViewController.m
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 7/24/17.
//  Copyright © 2017 Sunrixe Software Solutions Corporation. All rights reserved.
//

#import "PreferencesViewController.h"
#import "AppData.h"
#import "UtilStyle.h"
#import "UIButton+Extension.h"
#import "ControlStyleHeader.h"

#import "WSPreferences.h"
#import "PreferenceObject.h"

#define DEFAULT_STRING_PLACEHOLDER_DINING          @"Please select your preferred cuisine."
#define DEFAULT_STRING_PLACEHOLDER_HOTEL           @"Please select a rating."
#define DEFAULT_STRING_PLACEHOLDER_TRASPORTATION   @"Please select a preferred vehicle type."


@interface PreferencesViewController () <DropDownViewDelegate, DataLoadDelegate> {
    NSMutableArray *arrayPreferences, *listToAdd, *listToUpdate;
    BOOL isAddingPreference;
}

@property (nonatomic, strong) WSPreferences *wsPreferences;

@end

@implementation PreferencesViewController

- (void)viewDidLoad {
    isNotAskConciergeBarButton = YES;
    [super viewDidLoad];
    [self getPreferenceData];
    [self createBackBarButton];
    [self setNavigationBarWithDefaultColorAndTitle:@"PREFERENCES"];
    [self setupView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Process Data

- (void)getPreferenceData {
    
    isAddingPreference = NO;
    arrayPreferences = [[NSMutableArray alloc] init];
    arrayPreferences = [SessionData shareSessiondata].arrayPreferences;
    [self setPreferenceData];
}

- (void)setPreferenceData{
    
    self.diningDropDownView.titleColor = self.hotelDropDownView.titleColor = self.transportationDropDownView.titleColor = colorFromHexString(TEXTFIELD_PLACEHOLDER_COLOR_STANDARD);
    
    self.diningDropDownView.title = DEFAULT_STRING_PLACEHOLDER_DINING;
    self.hotelDropDownView.title = DEFAULT_STRING_PLACEHOLDER_HOTEL;
    self.transportationDropDownView.title = DEFAULT_STRING_PLACEHOLDER_TRASPORTATION;
    
    if (arrayPreferences.count > 0) {
        for (PreferenceObject *preferece in arrayPreferences) {
            switch (preferece.type) {
                case DiningPreferenceType:
                {
                    if (preferece.value.length > 0 && ![preferece.value isEqualToString:@"na"]) {
                        self.diningDropDownView.title = preferece.value;
                        self.diningDropDownView.titleColor = colorFromHexString(TEXTFIELD_TEXT_COLOR_STANDARD);
                        //[self.diningDropDownView layoutSubviews];
                    }
                }
                    break;
                case HotelPreferenceType:
                {
                    if (preferece.value.length > 0 && ![preferece.value isEqualToString:@"na"]) {
                        self.hotelDropDownView.title = preferece.value;
                        self.hotelDropDownView.titleColor = colorFromHexString(TEXTFIELD_TEXT_COLOR_STANDARD);
                        //[self.hotelDropDownView layoutSubviews];
                    }
                }
                    break;
                case TransportationPreferenceType:
                {
                    if (preferece.value.length > 0 && ![preferece.value isEqualToString:@"na"]) {
                        self.transportationDropDownView.title = preferece.value;
                        self.transportationDropDownView.titleColor = colorFromHexString(TEXTFIELD_TEXT_COLOR_STANDARD);
                        //[self.transportationDropDownView layoutSubviews];
                    }
                }
                    break;
                default:
                    break;
            }
        }
    }
    self.diningDropDownView.valueStatus = DefaultValueStatus;
    self.hotelDropDownView.valueStatus = DefaultValueStatus;
    self.transportationDropDownView.valueStatus = DefaultValueStatus;
}

#pragma mark - Setup
- (void)setupView {

    UITapGestureRecognizer *tappedBackground = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapBackground)];
    tappedBackground.delegate = self;
    [self.view addGestureRecognizer:tappedBackground];
    
    self.line1View.backgroundColor = [UtilStyle colorForSeparateLine];
    self.line2View.backgroundColor = [UtilStyle colorForSeparateLine];
    
    self.diningDropDownView.listItems = [[AppData getPreferencesItems] objectForKey:@"DINING"];
    self.diningDropDownView.delegate = self;
    
    self.hotelDropDownView.listItems = [[AppData getPreferencesItems] objectForKey:@"HOTEL"];
    self.hotelDropDownView.delegate = self;
    
    self.transportationDropDownView.listItems = [[AppData getPreferencesItems] objectForKey:@"TRANSPORTATION"];
    self.transportationDropDownView.delegate = self;
    
    [self.updateButton setBackgroundColorForNormalStatus];
    [self.updateButton setBackgroundColorForTouchingStatus];
    [self.updateButton configureDecorationForButton];
    
    [self.cancelButton setBackgroundColorForNormalStatus];
    [self.cancelButton setBackgroundColorForTouchingStatus];
    [self.cancelButton configureDecorationForButton];
    
    [self.updateButton setTitle:NSLocalizedString(@"update_button", nil) forState:UIControlStateNormal];
    [self.cancelButton setTitle:NSLocalizedString(@"cancel_button", nil) forState:UIControlStateNormal];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:BUTTON_FONT_NAME_STANDARD size:(BUTTON_FONT_SIZE_STANDARD * SCREEN_SCALE)],
                                 NSKernAttributeName: @1.4};
    self.updateButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.updateButton.titleLabel.text attributes:attributes];
    self.cancelButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.cancelButton.titleLabel.text attributes:attributes];
    
    [self setButtonStatus:NO];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.topLayoutConstraintDiningView.constant = (IPAD) ? 10.f : 30.0f;
    });

}
- (void)tapBackground {
    [self.diningDropDownView didTapBackground];
    [self.hotelDropDownView didTapBackground];
    [self.transportationDropDownView didTapBackground];
}

- (void)setButtonStatus:(BOOL)status {
    self.updateButton.enabled = status;
    self.cancelButton.enabled = status;
}

- (BOOL)isChangeData{
    if (arrayPreferences > 0) {
        for (PreferenceObject *preference in arrayPreferences) {
            switch (preference.type) {
                case DiningPreferenceType:
                {
                    NSString *valueTitle = ([self.diningDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_DINING]) ? @"" : self.diningDropDownView.title;
                    if ([valueTitle.uppercaseString isEqualToString:preference.value.uppercaseString]) {
                        self.diningDropDownView.valueStatus = DefaultValueStatus;
                    }else{
                        self.diningDropDownView.valueStatus = UpdateValueStatus;
                    }
                }
                    break;
                case HotelPreferenceType:
                {
                    NSString *valueTitle = ([self.hotelDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_HOTEL]) ? @"" : self.hotelDropDownView.title;
                    if ([valueTitle.uppercaseString isEqualToString:preference.value.uppercaseString]) {
                        self.hotelDropDownView.valueStatus = DefaultValueStatus;
                    }else{
                        self.hotelDropDownView.valueStatus = UpdateValueStatus;
                    }
                }
                    break;
                case TransportationPreferenceType:
                {
                    NSString *valueTitle = ([self.transportationDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_TRASPORTATION]) ? @"" : self.transportationDropDownView.title;
                    if ([valueTitle.uppercaseString isEqualToString:preference.value.uppercaseString]) {
                        self.transportationDropDownView.valueStatus = DefaultValueStatus;
                    }else{
                        self.transportationDropDownView.valueStatus = UpdateValueStatus;
                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
    }
    return (self.diningDropDownView.valueStatus != DefaultValueStatus || self.hotelDropDownView.valueStatus != DefaultValueStatus || self.transportationDropDownView.valueStatus != DefaultValueStatus);
}

#pragma mark - DropDownView Delegate
- (void)didSelectItem:(DropDownView *)dropDownView atIndex:(int)index{
    
    NSString *valueSelected = [dropDownView.listItems objectAtIndex:index];
    if ([valueSelected.uppercaseString isEqualToString:@"NONE"]) {
        
        if (dropDownView == self.diningDropDownView) {
            self.diningDropDownView.titleColor = colorFromHexString(TEXTFIELD_PLACEHOLDER_COLOR_STANDARD);
            self.diningDropDownView.title = DEFAULT_STRING_PLACEHOLDER_DINING;
            self.diningDropDownView.valueStatus = DefaultValueStatus;
        }else if (dropDownView == self.hotelDropDownView) {
            self.hotelDropDownView.titleColor = colorFromHexString(TEXTFIELD_PLACEHOLDER_COLOR_STANDARD);
            self.hotelDropDownView.title = DEFAULT_STRING_PLACEHOLDER_HOTEL;
            self.hotelDropDownView.valueStatus = DefaultValueStatus;
        }else if (dropDownView == self.transportationDropDownView) {
            self.transportationDropDownView.titleColor = colorFromHexString(TEXTFIELD_PLACEHOLDER_COLOR_STANDARD);
            self.transportationDropDownView.title = DEFAULT_STRING_PLACEHOLDER_TRASPORTATION;
            self.transportationDropDownView.valueStatus = DefaultValueStatus;
        }
        
    }
    [self setButtonStatus:[self isChangeData]];
}
- (void)didShow:(DropDownView *)dropDownView {
    if (dropDownView == self.diningDropDownView) {
        [self.hotelDropDownView didTapBackground];
        [self.transportationDropDownView didTapBackground];
    }else if (dropDownView == self.hotelDropDownView){
        [self.diningDropDownView didTapBackground];
        [self.transportationDropDownView didTapBackground];
    }else if (dropDownView == self.transportationDropDownView) {
        [self.diningDropDownView didTapBackground];
        [self.hotelDropDownView didTapBackground];
    }
        
}
#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    
    UIView *viewTouch = touch.view.superview;
    if ([viewTouch isKindOfClass:[UITableViewCell class]]) {
        return NO;
    }
    
    return YES;
    
}

#pragma mark - Actions Button
- (IBAction)UpdateAction:(id)sender {
    
    if(!isNetworkAvailable()) {
        [self showErrorNetwork];
        
    }else{
        [self setButtonStatus:NO];
        [self startActivityIndicator];
        
        listToAdd = [[NSMutableArray alloc] init];
        listToUpdate = [[NSMutableArray alloc] init];
        
        for (UIView *item in self.view.subviews) {
            if ([item isKindOfClass:[DropDownView class]]) {
                if (((DropDownView*)item).valueStatus == NewValueStatus) {
                    [listToAdd addObject:item];
                }else if(((DropDownView*)item).valueStatus == UpdateValueStatus){
                    [listToUpdate addObject:item];
                }
            }
        }
        if (listToAdd.count > 0 && listToUpdate.count > 0) {
            isAddingPreference = YES;
            [self addPreference];
        }else{
            if (listToAdd.count > 0) {
                [self addPreference];
            }else if (listToUpdate.count > 0){
                [self updatePreference];
            }
        }
    }

}

- (IBAction)CancelAction:(id)sender {
    [self setPreferenceData];
    [self setButtonStatus:NO];
}
#pragma mark - Private Methods
- (void)addPreference{
    
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    NSMutableArray *arraySubDictValues = [[NSMutableArray alloc] init];
    for (DropDownView *item in listToAdd) {
        if (item == self.diningDropDownView) {
            NSMutableDictionary *diningDict = [[NSMutableDictionary alloc] init];
            [diningDict setValue:@"Dining" forKey:@"Type"];
            [diningDict setValue:([self.diningDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_DINING])?@"":self.diningDropDownView.title forKey:@"CuisinePreferences"];
            [arraySubDictValues addObject:diningDict];
            
        }else if (item == self.hotelDropDownView) {
            NSMutableDictionary *hotelDict = [[NSMutableDictionary alloc] init];
            [hotelDict setValue:@"Hotel" forKey:@"Type"];
            [hotelDict setValue:([self.hotelDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_HOTEL])?@"":self.hotelDropDownView.title forKey:@"Preferredstarrating"];
            [arraySubDictValues addObject:hotelDict];
        }else if (item == self.transportationDropDownView) {
            NSMutableDictionary *transportationDict = [[NSMutableDictionary alloc] init];
            [transportationDict setValue:@"Car Rental" forKey:@"Type"];
            [transportationDict setValue:([self.transportationDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_TRASPORTATION])?@"":self.transportationDropDownView.title forKey:@"PreferredRentalVehicle"];
            [arraySubDictValues addObject:transportationDict];
        }
    }
    
    [dataDict setValue:arraySubDictValues forKey:@"PreferenceDetails"];
    self.wsPreferences = [[WSPreferences alloc] init];
    self.wsPreferences.delegate = self;
    [self.wsPreferences addPreference:dataDict];
    
}

- (void)updatePreference{
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    NSMutableArray *arraySubDictValues = [[NSMutableArray alloc] init];
    for (DropDownView *item in listToUpdate) {
        if (item == self.diningDropDownView) {
            NSMutableDictionary *diningDict = [[NSMutableDictionary alloc] init];
            [diningDict setValue:[self getPreferenceIDByType:DiningPreferenceType] forKey:@"MyPreferencesId"];
            [diningDict setValue:@"Dining" forKey:@"Type"];
            [diningDict setValue:([self.diningDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_DINING])?@"":self.diningDropDownView.title forKey:@"CuisinePreferences"];
            [arraySubDictValues addObject:diningDict];
            
        }else if (item == self.hotelDropDownView) {
            NSMutableDictionary *hotelDict = [[NSMutableDictionary alloc] init];
            [hotelDict setValue:[self getPreferenceIDByType:HotelPreferenceType] forKey:@"MyPreferencesId"];
            [hotelDict setValue:@"Hotel" forKey:@"Type"];
            [hotelDict setValue:([self.hotelDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_HOTEL])?@"":self.hotelDropDownView.title forKey:@"Preferredstarrating"];
            [arraySubDictValues addObject:hotelDict];
        }else if (item == self.transportationDropDownView) {
            NSMutableDictionary *transportationDict = [[NSMutableDictionary alloc] init];
            [transportationDict setValue:[self getPreferenceIDByType:TransportationPreferenceType] forKey:@"MyPreferencesId"];
            [transportationDict setValue:@"Car Rental" forKey:@"Type"];
            [transportationDict setValue:([self.transportationDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_TRASPORTATION])?@"":self.transportationDropDownView.title forKey:@"PreferredRentalVehicle"];
            [arraySubDictValues addObject:transportationDict];
        }
    }
    [dataDict setValue:arraySubDictValues forKey:@"PreferenceDetails"];
    
    self.wsPreferences = [[WSPreferences alloc] init];
    self.wsPreferences.delegate = self;
    [self.wsPreferences updatePreference:dataDict];
}

- (void)getPreference{
    [self startActivityIndicator];
    self.wsPreferences = [[WSPreferences alloc] init];
    self.wsPreferences.delegate = self;
    [self.wsPreferences getPreference];
    
}
-(NSString*)getPreferenceIDByType:(PreferenceType)type{
    for (PreferenceObject *preference in arrayPreferences) {
        if (preference.type == type) {
            return preference.preferenceID;
        }
    }
    return @"";
}

#pragma mark - Delegate from API
-(void)loadDataDoneFrom:(WSBase*)ws{
    if (ws.task == WS_GET_MY_PREFERENCE) {
        arrayPreferences = [SessionData shareSessiondata].arrayPreferences;
        [self setPreferenceData];
        [self stopActivityIndicator];
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert =nil;
        alert.msgAlert = @"You have successfully updated your preference.";
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
            [alert.view layoutIfNeeded];
        });
        alert.blockFirstBtnAction = ^(void){
        };
        
        [self showAlert:alert forNavigation:NO];
    }else if (ws.task == WS_ADD_MY_PREFERENCE){
        if (isAddingPreference) {
            [self updatePreference];
            isAddingPreference = NO;
        }else{
            [self getPreference];
        }
    }else if (ws.task == WS_UPDATE_MY_PREFERENCE){
        [self getPreference];
    }
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode {
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }
}

-(void)loadDataFailFrom:(WSBase *)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message {
    
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
         [self showErrorNetwork];
    }else{
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);;
        alert.msgAlert = message;
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        alert.blockFirstBtnAction = ^{
            [self setButtonStatus:YES];
        };
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
        });
        
        [self showAlert:alert forNavigation:NO];
    }
}

@end
