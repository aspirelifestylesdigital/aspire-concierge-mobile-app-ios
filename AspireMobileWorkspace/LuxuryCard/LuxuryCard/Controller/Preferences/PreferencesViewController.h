//
//  PreferencesViewController.h
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 7/24/17.
//  Copyright © 2017 Sunrixe Software Solutions Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"
#import "BaseViewController.h"

@interface PreferencesViewController : BaseViewController

@property (weak, nonatomic) IBOutlet DropDownView *diningDropDownView;
@property (weak, nonatomic) IBOutlet DropDownView *hotelDropDownView;
@property (weak, nonatomic) IBOutlet DropDownView *transportationDropDownView;

@property (weak, nonatomic) IBOutlet UIView *line1View;
@property (weak, nonatomic) IBOutlet UIView *line2View;
@property (weak, nonatomic) IBOutlet UIButton *updateButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topLayoutConstraintDiningView;

@end
