//
//  SplashViewController.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/5/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface SplashViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UILabel *nameApp;

@end
