//
//  MenuSearch.m
//  Test
//
//  Created by 😱 on 7/14/17.
//  Copyright © 2017 dai.pham.s3corp.com.vn. All rights reserved.
//

#import "MenuSearch.h"

#import "StyleConstant.h"

#import "UIView+Extension.h"

#pragma mark - 🚸 UILabelOptionSearch 🚸
@interface UILabelOptionSearch:UILabel@end
@implementation UILabelOptionSearch
- (void) awakeFromNib {
    [super awakeFromNib];
    
//    !!!: Uncommon
    self.font = [UIFont fontWithName:FONT_MarkForMC_MED size:18 * SCREEN_SCALE];
    self.textColor = colorFromHexString(@"#011627");
}

@end

#pragma mark - 🚸 UITextFieldMenuSearch 🚸
@interface UITextFieldMenuSearch:UITextField@end
@implementation UITextFieldMenuSearch
- (void) awakeFromNib {
    [super awakeFromNib];
    
//    !!!: Uncomment
//    self.font = [UIFont fontWithName:TEXTFIELD_FONT_NAME_SEARCH size:TEXTFIELD_FONT_SIZE_SEARCH * SCREEN_SCALE];
//    self.textColor = colorFromHexString(TEXTFIELD_TEXT_COLOR_SEARCH);
    
    UIButton *clearButton = [self valueForKey:@"_clearButton"];
    if(clearButton) {
        [clearButton setImage:[UIImage imageNamed:@"clear_icon"] forState:UIControlStateNormal];
        [clearButton setImage:[UIImage imageNamed:@"clear_icon"] forState:UIControlStateHighlighted];
    }
    
    UIImageView *searchIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"search_icon"]];
    searchIcon.frame = CGRectMake(0.0f, 0.0f, 30.0f, 30.0f);
    self.leftView = searchIcon;
    self.leftViewMode = UITextFieldViewModeAlways;
    
    self.clearButtonMode = UITextFieldViewModeAlways;
    self.returnKeyType = UIReturnKeySearch;
    
    self.layer.cornerRadius = 3.0;
    
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search" attributes:@{NSForegroundColorAttributeName:colorFromHexString(@"#99A1A8")}];
    [self setFont:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:18]];
}

@end

#pragma mark - 🚸 Declare OptionSearch Interface 🚸
@class OptionSearch;
@protocol OptionSearchProtocol <NSObject>
@optional
- (void) OptionSearch:(OptionSearch*)view onTouchWithObject:(id)object;
@end

@interface OptionSearch:UIView {
    id object;
    BOOL isChecked;
}
@property (strong, nonatomic) IBOutlet UIButton *btnAction;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (assign,nonatomic,setter = setType:) OptionSearchType type;

@property (weak,nonatomic) id<OptionSearchProtocol> delegate;

- (void) setType:(OptionSearchType) type;
- (void) presentObject:(id) obj;

@end


#pragma mark - 🚸 MenuSearch 🚸
@interface MenuSearch()<OptionSearchProtocol, UITextFieldDelegate> {
    NSLayoutConstraint* heightC;
    __weak IBOutlet UIView *vwSearchfield;
    __weak IBOutlet UITextFieldMenuSearch *txtSearch;
    OptionSearchType optionSearchType;
    
    
}
@end
@implementation MenuSearch

#pragma mark - INIT
- (id)init
{
    self = [super init];
    
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
    }
    return self;
}

- (void) awakeFromNib {
    [super awakeFromNib];
    
    txtSearch.delegate = self;
    [txtSearch addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [self setOptionSearchType:OptionSearchTypeClear];
    //    !!!: Uncomment
    [vwSearchfield setbackgroundColorForLineMenu];
}

#pragma mark - INTERFACE
- (void) setAttributeTextSearch:(NSAttributedString *)attributeText withListOptionSearchs:(NSArray *)optionSearchs{
    if(attributeText.length > 0 && attributeText != nil)
        txtSearch.attributedText = attributeText;
    
    [self handleListOptionSearchs:optionSearchs];
}

- (void) setOptionSearchType:(OptionSearchType)type {
    optionSearchType = type;
    if(type == OptionSearchTypeCheckbox)
        _stackView.spacing = 10;
    else
        _stackView.spacing = 5;
}

#pragma mark - OptionSearch Protocol
- (void)OptionSearch:(OptionSearch *)view onTouchWithObject:(id)object {
    if(self.delegate)
        [self.delegate MenuSearch:self onClearOptionSearch:object];
}

#pragma mark - TextField Protocol
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    
    if(self.delegate)
        return [self.delegate MenuSearchIsTextFieldShouldBeginEditing:self];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(MenuSearchIsTextFieldShouldBeginEditing:)])
        return [self.delegate MenuSearchIsTextFieldShouldBeginEditing:self];
    return YES;
}

- (void)textFieldDidChange :(UITextField *)textField{
    BOOL isShouldSendData = YES;
    if(self.delegate && [self.delegate respondsToSelector:@selector(MenuSearchIsTextFieldShouldBeginEditing:)])
        isShouldSendData = [self.delegate MenuSearchIsTextFieldShouldBeginEditing:self];
    
    if(self.delegate && isShouldSendData) {
        [self.delegate MenuSearch:self onTextSearchDidChange:textField];
    }
    
}

#pragma mark - PRIVATE
- (void) handleListOptionSearchs:(NSArray*) optionSearchs {
    // clear all old option search
    for (UIView* view in [_stackView.arrangedSubviews reverseObjectEnumerator]) {
        if([view isEqual:vwSearchfield]) {
            continue;
        } else {
            [_stackView removeArrangedSubview:view];
        }
    }
    
    for (id object in optionSearchs) {
        OptionSearch* oS = [OptionSearch new];
        [oS setType:optionSearchType];
        oS.delegate = self;
        [oS presentObject:object];
        [_stackView insertArrangedSubview:oS atIndex:_stackView.arrangedSubviews.count];
    }
}

@end

#pragma mark - OptionSearch
@implementation OptionSearch

- (id)init
{
    self = [super init];
    
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
    }
    return self;
}

- (void) awakeFromNib {
    [super awakeFromNib];
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:22]];
}

- (void) presentObject:(id)obj {
    object = obj;
    self.lblTitle.text = @"test";
}

- (void) setType:(OptionSearchType)type {
    _type = type;
    [self configView];
}

- (void) configView {
    switch (_type) {
        case OptionSearchTypeCheckbox:
            [self.btnAction setImage:[UIImage imageNamed:@"checkbox_uncheck"] forState:UIControlStateNormal];
            [self.btnAction setImage:[UIImage imageNamed:@"checkbox_check"] forState:UIControlStateSelected];
            break;
        default:
            [self.btnAction setImage:[UIImage imageNamed:@"clear_icon"] forState:UIControlStateNormal];
            break;
    }
}

- (IBAction)actionHandle:(UIButton*)sender {
    if(self.delegate && _type == OptionSearchTypeClear)
        [self.delegate OptionSearch:self onTouchWithObject:object];
    else {
        [self.btnAction setSelected:!self.btnAction.isSelected];
    }
}
@end
