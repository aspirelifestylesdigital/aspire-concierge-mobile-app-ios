//
//  BaseTableViewCell.h
//  MobileConcierge
//
//  Created by Home on 5/9/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Common.h"
#import "Constant.h"
#import "NSString+Utis.h"

@class BaseTableViewCell;

@protocol BaseTableViewCellDelegate <NSObject>
- (CGFloat) BaseTableViewCellWhatCellSelected:(BaseTableViewCell*) cell;
@end

@interface BaseTableViewCell : UITableViewCell
- (void) loadData;
@property (weak,nonatomic) id <BaseTableViewCellDelegate> delegate;
@end
