//
//  ExploreTableViewCell.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/9/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "ExploreTableViewCell.h"
#import "Constant.h"
#import "UIImageView+AFNetworking.h"
#import "NSString+Utis.h"
#import "Common.h"
#import "THLabel.h"
#import <QuartzCore/QuartzCore.h>
#import "UtilStyle.h"
#import "StyleConstant.h"

@interface ExploreTableViewCell() {
    
}

@end

@implementation ExploreTableViewCell
@synthesize testLbael;
- (void)awakeFromNib {
    [super awakeFromNib];
    self.nameLbl.textColor = colorFromHexString(BUTTON_BG_COLOR_NORMAL);
    self.addressLbl.textColor = colorFromHexString(@"#253847");
    self.offerLbl.textColor = colorFromHexString(@"#011627");
    [self.nameLbl setFont:[UIFont fontWithName:FONT_MarkForMC_MED size:16.0 * SCREEN_SCALE]];
    [self.addressLbl setFont:[UIFont fontWithName:FONT_MarkForMC_MED size:16.0 * SCREEN_SCALE]];
     [self.offerLbl setFont:[UIFont fontWithName:FONT_MarkForMC_MED size:16.0 * SCREEN_SCALE]];
    self.exploreImg.contentMode = UIViewContentModeScaleAspectFill;
    self.exploreImg.alignLeft = YES;
    [self.nameLbl sizeToFit];
    [self.addressLbl sizeToFit];
    [self.offerLbl sizeToFit];
    
    [self.maskImageView.layer setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.4].CGColor];
    [self.categoryName setFont:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:16.0 * SCREEN_SCALE]];
    self.categoryName.shadowColor = [UIColor blackColor];
    self.categoryName.shadowOffset = CGSizeMake(0,4.0);
    self.categoryName.shadowBlur = 5.0f;
    [self.contentView setBackgroundColor:[UIColor whiteColor]];
    
//    [testLbael setFont:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:16.0 * SCREEN_SCALE]];
//    testLbael.textColor = [UIColor whiteColor];
//    testLbael.adjustsFontSizeToFitWidth = YES;
//    testLbael.layer.shadowOpacity = 1;
//    testLbael.layer.shadowOffset = CGSizeMake(0, 4);
//    testLbael.layer.shadowRadius = 3;
    
    [self.contentView setBackgroundColor:[UIColor whiteColor]];
}

- (void)initCellWithData:(BaseItem *)item {
    
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    UIView *view = [self viewWithTag:1000];
    if(!view)
    {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - 1, self.bounds.size.width, 1.0f)];
        [view setBackgroundColor:[UtilStyle colorForSeparateLine]];
        view.tag = 1000;
        [self addSubview:view];
    }
    else
    {
        view.frame = CGRectMake(0, self.bounds.size.height - 1, self.bounds.size.width, 1.0f);
    }
    
    for (UILabel* label in @[self.nameLbl,self.addressLbl,self.offerLbl]) {
        label.adjustsFontSizeToFitWidth = NO;
        label.lineBreakMode = NSLineBreakByTruncatingTail;
    }
    
    
    self.addressLbl.hidden = self.addressLbl.text.length == 0;
    if(self.delegate) {
        if(self.tag == [self.delegate BaseTableViewCellWhatCellSelected:self]) {
            [self.contentView setBackgroundColor:colorFromHexString(@"#E5EBEE")];
            [view setBackgroundColor:[UtilStyle colorForSeparateLine]];
        }
    }
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    [self.maskImageView.layer setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.4].CGColor];
     UIView *view = [self viewWithTag:1000];
    if(view)
    {
        [view setBackgroundColor:[UtilStyle colorForSeparateLine]];
    }
    if(highlighted) {
        [self.contentView setBackgroundColor:colorFromHexString(@"#E5EBEE")];
    } else {
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    // Configure the view for the selected state
    [super setSelected:selected animated:animated];
    if(selected)
    {
        [self.maskImageView.layer setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.4].CGColor];
        UIView *view = [self viewWithTag:1000];
        if(view)
        {
            [view setBackgroundColor:[UtilStyle colorForSeparateLine]];
        }
        [self.contentView setBackgroundColor:colorFromHexString(@"#E5EBEE")];
    } else {
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
    }
}

- (void)prepareForReuse {
    [super prepareForReuse];
    [self.contentView setBackgroundColor:[UIColor whiteColor]];
}
@end
