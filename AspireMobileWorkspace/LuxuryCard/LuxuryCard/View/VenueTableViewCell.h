//
//  VenueTableViewCell.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/11/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"

@interface VenueTableViewCell : BaseTableViewCell

@property(nonatomic, weak) IBOutlet UIImageView *venueImg;
@property(nonatomic, weak) IBOutlet UIButton *heartBtn;
@property(nonatomic, weak) IBOutlet UIButton *calendarBtn;
@property(nonatomic, weak) IBOutlet UIButton *conciergeBtn;
@property(nonatomic, weak) IBOutlet UIButton *shareBtn;
@property(nonatomic, weak) IBOutlet UILabel *nameLbl;
@property(nonatomic, weak) IBOutlet UILabel *addressLbl;
@property(nonatomic, weak) IBOutlet UIButton *seeMapBtn;
@property(nonatomic, weak) IBOutlet UIButton *toWebBnt;
@property(nonatomic, weak) IBOutlet UIButton *offerIconBtn;
@property(nonatomic, weak) IBOutlet UILabel *offerTextLbl;
@property(nonatomic, weak) IBOutlet UILabel *venueDescriptionLbl;
@property(nonatomic, weak) IBOutlet UILabel *personalRecommendLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *seeMapHeightConstraint;

@end
