//
//  TouchDownGestureRecognizer.h
//  LuxuryCard
//
//  Created by Dai Pham on 9/7/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TouchDownGestureRecognizer : UIGestureRecognizer

@end
