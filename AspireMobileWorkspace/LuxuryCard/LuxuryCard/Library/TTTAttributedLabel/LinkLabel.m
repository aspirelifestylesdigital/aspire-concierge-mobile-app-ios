//
//  LinkLabel.m
//  TestLink
//
//  Created by Nghia Dinh on 7/18/17.
//  Copyright © 2017 Nghia Dinh. All rights reserved.
//

#import "LinkLabel.h"
#import "Common.h"
#import "Constant.h"


@interface LinkLabel() <TTTAttributedLabelDelegate>
@property (nonatomic) BOOL isExpand;
@end

@implementation LinkLabel
@synthesize delegate;

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self customInit];
    }
    return self;
}

-(void)customInit {
    self.numberOfLines = 0;
    //UIColor *activeColor = [UIColor blackColor];
    //NSDictionary *attrsActive = @{ NSForegroundColorAttributeName : activeColor };
    
    //UIColor *highlightColor = colorFromHexString(@"C54B53");
    //NSDictionary *attrsHighlight = @{ NSForegroundColorAttributeName : highlightColor };
    //self.activeLinkAttributes = attrsHighlight;
    self.linkAttributes = @{ (id)kCTForegroundColorAttributeName: colorFromHexString(@"#011627"),
                             (id)kCTUnderlineStyleAttributeName : [NSNumber numberWithInt:NSUnderlineStyleSingle] };
    /*
    self.activeLinkAttributes = @{ (id)kCTForegroundColorAttributeName: [UIColor colorWithRed:197.0/255.0 green:75.0/255.0 blue:83.0/255.0 alpha:1.0],
                                   (id)kCTUnderlineStyleAttributeName : [NSNumber numberWithInt:NSUnderlineStyleSingle] };*/
    
    self.activeLinkAttributes = @{ (id)kCTForegroundColorAttributeName:colorFromHexString(@"#e1cb78") ,
                                   (id)kCTUnderlineStyleAttributeName : [NSNumber numberWithInt:NSUnderlineStyleSingle] };
    
    
}

-(void)setHTMLString:(NSString *)html {
    
    self.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    if (IPAD) {
        self.textInsets = UIEdgeInsetsMake(0, 0, 8, 0);
    }
    self.text = [self formatHTMLString:html];
}

-(NSMutableAttributedString*)formatHTMLString:(NSString*)html {
    
    
    
    
    //    html =[html stringByReplacingOccurrencesOfString:@"<p>" withString:@"<open p tag>"];
//    html =[html stringByReplacingOccurrencesOfString:@"</p>" withString:@"<close p tag>"];
    html = [html stringByReplacingOccurrencesOfString:@"\r\n" withString:@""];
    html = [html stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    
    NSRegularExpression *regexHyperLink = [NSRegularExpression regularExpressionWithPattern:@"<a href=(?!\"http+)(.*?)>.*?</a>" options:NSRegularExpressionCaseInsensitive error:NULL];
    NSArray *regexLinkArray = [regexHyperLink matchesInString:html options:0 range:NSMakeRange(0, html.length)];
    
    for (NSTextCheckingResult *result in regexLinkArray) {
        NSString *subString = [html substringWithRange:result.range];
        html = [html stringByReplacingOccurrencesOfString:subString withString:@""];
    }
    
    html = [html stringByReplacingOccurrencesOfString:@"<br />" withString:@"<new line>"];
    html =[html stringByReplacingOccurrencesOfString:@"<br>" withString:@"<new line>"];

    
    
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"<ul>(.*?)</ul>" options:NSRegularExpressionCaseInsensitive error:NULL];
    NSArray *regexArray = [regex matchesInString:html options:0 range:NSMakeRange(0, html.length)];
    int count = (int)[regexArray count];
    
    int countForLoop = count - 1;
    while (countForLoop >= 0) {
        NSTextCheckingResult *result = [regexArray objectAtIndex:countForLoop];
        NSString *subString = [html substringWithRange:result.range];
        if ([subString containsString:@"<new line>"]) {
            NSString *space = [NSString stringWithFormat:@"<li style=\"color: red\">"];
            subString = [subString stringByReplacingOccurrencesOfString:@"<new line>" withString:space];
            html = [html stringByReplacingCharactersInRange:result.range withString:subString];
        }
        countForLoop--;
    }
    //html = [html stringByReplacingOccurrencesOfString:@"<new line>" withString:@"<br />"];
    //html =[html stringByReplacingOccurrencesOfString:@"<open p tag>" withString:@"<p>"];
   // html =[html stringByReplacingOccurrencesOfString:@"<close p tag>" withString:@"</p>"];
    
    
    
    NSMutableAttributedString *htmlString = [[NSMutableAttributedString alloc] initWithData:[html dataUsingEncoding:NSUTF8StringEncoding]                                                                            options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                                                                                                                                                               NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                                         documentAttributes:nil error:nil];
    
    
    CGFloat myComponentArray[] = { 1, 0, 0, 1 };
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    CGColorRef aColor = CGColorCreate(colorspace, myComponentArray);
    UIColor *redColor = [UIColor colorWithCGColor:aColor];
    
    
    
    
    
    
    NSMutableArray *changeColorToWhite = [[NSMutableArray alloc] init];
    NSMutableArray *changeColorToDefault = [[NSMutableArray alloc] init];
    
    NSMutableAttributedString *res = [htmlString mutableCopy];
    
    [res enumerateAttributesInRange:NSMakeRange(0, htmlString.length) options:0 usingBlock:^(NSDictionary<NSString *,id> * _Nonnull attrs, NSRange range, BOOL * _Nonnull stop) {
        
//        NSMutableParagraphStyle *paragraphStyle  = [attrs valueForKey:NSParagraphStyleAttributeName];
        UIColor *textColor = [attrs valueForKey:NSForegroundColorAttributeName];
        
        const CGFloat *comTextColor = CGColorGetComponents(textColor.CGColor);
        const CGFloat *comRedColor = CGColorGetComponents(redColor.CGColor);
        
        if (comTextColor[0] == comRedColor[0] && comTextColor[1] == comRedColor[1] &&comTextColor[2] == comRedColor[2] && CGColorGetAlpha(textColor.CGColor) == CGColorGetAlpha(redColor.CGColor)) {
            
            
            NSAttributedString *tempString = [res attributedSubstringFromRange:range];
            NSString *trimString = [tempString.string stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            NSArray* words = [trimString componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            NSString* nospacestring = [words componentsJoinedByString:@""];
            if ([nospacestring isEqualToString:@"•"]) {
                [changeColorToWhite addObject:[NSValue valueWithRange:range]];
            } else {
                [changeColorToDefault addObject:[NSValue valueWithRange:range]];
            }
        }
    }];
    
    
    for (NSValue *value in changeColorToWhite) {
        NSRange range = value.rangeValue;
        [htmlString addAttribute:NSForegroundColorAttributeName value:[UIColor clearColor] range:range];
    }
    for (NSValue *value in changeColorToDefault) {
        NSRange range = value.rangeValue;
        [htmlString addAttribute:NSForegroundColorAttributeName value:colorFromHexString(@"#011627") range:range];
    }
    
    CGColorRelease(aColor);
    CGColorSpaceRelease(colorspace);

    
    
    return htmlString;

}



-(void)expandLabel:(BOOL)isExpand {
    self.isExpand = isExpand;
    if (self.isExpand) {
        self.numberOfLines = 5;
    } else {
        self.numberOfLines = 0;
    }
}


@end
