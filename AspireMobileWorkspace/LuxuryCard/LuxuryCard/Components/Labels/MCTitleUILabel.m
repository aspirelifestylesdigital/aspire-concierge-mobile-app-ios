//
//  MCCommonUILabel.m
//  MobileConcierge
//
//  Created by 😱 on 7/4/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "StyleConstant.h"
#import "MCTitleUILabel.h"

@implementation MCTitleUILabel

- (instancetype) init {
    if (self = [super init]) {
        self.labelType = MCLabelTypeTitle;
    }
    return self;
}

- (void) awakeFromNib {
    [super awakeFromNib];
    
    self.labelType = MCLabelTypeTitle;
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.labelType = MCLabelTypeTitle;
    }
    return self;
}

- (id) initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.labelType = MCLabelTypeTitle;
    }
    return self;
}

@end
