//
//  MCBaseUILabel.m
//  MobileConcierge
//
//  Created by 😱 on 7/4/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "MCBaseUITextField.h"

@interface MCBaseUITextField() {
    NSString* placeholderColor;
}

@end

@implementation MCBaseUITextField

#pragma mark - PROPERTIES

- (void) setTextfieldType:(MCTextfieldType)TextfieldType {
    
    NSString* colorString = nil;
    placeholderColor = nil;
    NSString* fontName = nil;
    CGFloat   fontSize = LABEL_FONT_SIZE_DETAIL;
    
    switch (TextfieldType) {
        case MCTextfieldTypeStandard:
            colorString = TEXTFIELD_TEXT_COLOR_STANDARD;
            fontName = TEXTFIELD_FONT_NAME_STANDARD;
            fontSize = TEXTFIELD_FONT_SIZE_STANDARD;
            placeholderColor = TEXTFIELD_PLACEHOLDER_COLOR_STANDARD;
            break;
    }
    
    if(colorString)
        [self setTextFontHexColor:colorString orColor:nil];
    
    if(fontName)
        [self setCustomFontName:fontName withSize:fontSize];
}

- (void) setAttributedPlaceholder:(NSAttributedString *)attributedPlaceholder {
    if(placeholderColor != nil && attributedPlaceholder.length > 0) {
        attributedPlaceholder = [[NSAttributedString alloc] initWithString:attributedPlaceholder.string attributes:@{NSForegroundColorAttributeName:colorFromHexString(placeholderColor)}];
        placeholderColor = nil;
        [self setAttributedPlaceholder:attributedPlaceholder];
    } else {
        [super setAttributedPlaceholder:attributedPlaceholder];
    }
}


#pragma mark - PRIVATE
- (void) setCustomFontName:(NSString*)fontName withSize:(CGFloat)fontSize {
    [self setFont:[UIFont fontWithName:fontName size:fontSize]];
}

- (void) setTextFontHexColor:(NSString*)hexColor orColor:(UIColor*)color {
    if(color) {
        [self setTextColor:color];
    } else {
        if (hexColor.length > 6) {
            [self setTextColor:colorFromHexString(hexColor)];
        }
    }
}

- (void) setBackgroundFromHexColor:(NSString*)hexColor orColor:(UIColor*) color {
    if(color) {
        [self setBackgroundColor:color];
    } else {
        if (hexColor.length > 6) {
            [self setBackgroundColor:colorFromHexString(hexColor)];
        }
    }
}
@end
