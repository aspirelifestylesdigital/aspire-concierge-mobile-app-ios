//
//  StyleLabel.h
//  MobileConcierge
//
//  Created by 😱 on 7/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "Common.h"

// - COMMON -
#define LABEL_COLOR_TITLE               @"#FFFFFF"
#define LABEL_COLOR_DESCRIPTION         @"#011627"
#define LABEL_COLOR_DETAIL              @"#FFFFFF"
#define LABEL_COLOR_DETAIL_WITH_LINK    @"#141413"
#define LABEL_COLOR_NOTE                @"#FFFFFF"
#define LABEL_COLOR_TITLE_HIGHLIGHT     @"#f7eab9"//@"#C54B53"
#define LABEL_COLOR_TITLE_SEARCH        @"#272726"

#define LABEL_FONT_NAME_TITLE           FONT_MarkForMC_MED
#define LABEL_FONT_NAME_DESCRIPTION     FONT_MarkForMC_REGULAR
#define LABEL_FONT_NAME_DETAIL          FONT_MarkForMC_REGULAR
#define LABEL_FONT_NAME_NOTE            FONT_MarkForMC_REGULAR

#define LABEL_FONT_SIZE_TITLE           18
#define LABEL_FONT_SIZE_DESCRIPTION     16
#define LABEL_FONT_SIZE_DETAIL          16
#define LABEL_FONT_SIZE_NOTE            16

// - CITY CELL AND GALLERY AND GALLERY CONTROLLER
#define LABEL_COLOR_CITY_NAME                @"#272726"
#define LABEL_COLOR_CITY_ADDRESS             @"#011627"

// - EXPLORE <Question> CELL AND GALLERY CONTROLLER | CategoryCollectionViewCell
#define LABEL_COLOR_EXPLORE_NAME             @"#FF4000"
#define LABEL_COLOR_EXPLORE_DESCRIPTION      @"#272726" //(apply for Offer label)
#define LABEL_COLOR_EXPLORE_ADDRESS          @"#011627"

#define LABEL_FONT_NAME_EXPLORE_NAME    FONT_MarkForMC_BOLD
#define LABEL_FONT_SIZE_EXPLORE_NAME    16

#define LABEL_FONT_NAME_EXPLORE_CATEGORY        FONT_MarkForMC_BOLD
#define LABEL_FONT_SIZE_EXPLORE_CATEGORY        14
#define LABEL_SHADOW_COLOR_EXPLORE_CATEGORY     [UIColor blackColor]
#define LABEL_SHADOW_BLUR_EXPLORE_CATEGORY      5.0f

// - HOME CONTROLLER
#define LABEL_COLOR_HOME_WELCOME                            @"#FFFFFF"
#define LABEL_COLOR_HOME_WELCOME_MESSAGE                    @"#FFFFFF"
#define LABEL_COLOR_HOME_ASK_CONCIERGE_MESSAGE              @"#011627"

#define LABEL_FONT_NAME_HOME_WELCOME                        FONT_MarkForMC_REGULAR
#define LABEL_FONT_NAME_HOME_WELCOME_MESSAGE                FONT_MarkForMC_REGULAR
#define LABEL_FONT_NAME_HOME_ASK_CONCIERGE_MESSAGE          FONT_MarkForMC_REGULAR

#define LABEL_FONT_SIZE_HOME_WELCOME                        44
#define LABEL_FONT_SIZE_HOME_WELCOME_MESSAGE                20
#define LABEL_FONT_SIZE_HOME_ASK_CONCIERGE_MESSAGE          16

// - TableHeaderView
#define LABEL_COLOR_HEADERVIEW              @"#011627"

#define LABEL_FONT_NAME_HEADERVIEW          FONT_MarkForMC_REGULAR

#define LABEL_FONT_SIZE_HEADERVIEW          20

// - AskConciergeTextViewPlaceHolder
#define LABEL_COLOR_AskConciergeTextView              @"#011627"

#define LABEL_FONT_NAME_AskConciergeTextView          FONT_MarkForMC_REGULAR_It

#define LABEL_FONT_SIZE_AskConciergeTextView          24
