//
//  CCAService.h
//  MobileConciergeUSDemo
//
//  Created by Dai Pham on 8/5/17.
//  Copyright © 2017 Sunrixe Software Solutions Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCAService : NSObject

#pragma mark - SERVICE
+ (CCAService*) startService;
+ (CCAService*) service;


#pragma mark - PROPERTIES
@property (assign,nonatomic) BOOL isGetCCADone;

#pragma mark INTERFACE
+ (id) responseFromCCA;
@end
