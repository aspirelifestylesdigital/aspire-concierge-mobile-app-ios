//
//  BINResponseObject.m
//  MobileConcierge
//
//  Created by Chung Mai on 7/5/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "BINResponseObject.h"
#import "NSDictionary+SBJSONHelper.h"
#import "BINItem.h"

@implementation BINResponseObject
-(id)initFromDict:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        dict = [dict dictionaryForKey:@"CheckMasterCardBINResult"];
        [self parseCommonResponse:dict];
        self.data = [self parseJson:dict toObject:NSStringFromClass([BINItem class])];
    }
    return self;
}

-(id)parseJson:(NSDictionary *)dict toObject:(NSString *)className
{
    NSMutableArray *datas = [[NSMutableArray alloc] init];
    BINItem *item = [[BINItem alloc] init];
    item.valid = [dict boolForKey:@"Valid"];
    item.message = [dict stringForKey:@"Status"];
    [datas addObject:item];
    return datas;
}

-(void)parseCommonResponse:(NSDictionary *)dict
{
    self.status = [dict boolForKey:@"Success"];
    NSArray* mesArr = [dict arrayForKey:@"message"];
    if(mesArr && mesArr.count > 0){
        NSDictionary * error = [mesArr objectAtIndex:0];
        self.message = [error stringForKey:@"message"];
        self.errorCode = [error stringForKey:@"code"];
    }
    else
    {
        self.message = [dict objectForKey:@"message"];
    }
}
@end
