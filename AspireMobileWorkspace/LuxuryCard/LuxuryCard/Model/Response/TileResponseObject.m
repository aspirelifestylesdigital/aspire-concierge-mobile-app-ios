//
//  TileResponseObject.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/6/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "TileResponseObject.h"
#import "TileItem.h"
#import "NSDictionary+SBJSONHelper.h"
#import "AppData.h"
#import "CategoryItem.h"
#import "CCAMapCategories.h"

@interface TileResponseObject()
{
    NSArray *categoryIDsForCurrentCategory;
    NSArray *categoriesForCCAData;
}
@end

@implementation TileResponseObject

-(id)initFromDict:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        if([self.categoryCode isEqualToString:@"all"] )
        {
            categoriesForCCAData = [self getCategoriesForCCAData];
        } else if(![self.categoryCode isEqualToString:@"appgallery"]) {
            categoryIDsForCurrentCategory = [self getIDsOfCurrentCategory];
        }
        
        dict = [dict dictionaryForKey:@"GetTilesResult"];
        self.data = [dict arrayForKey:@"Tiles" withProcessing:self toObject:NSStringFromClass([TileItem class])];
        [self parseCommonResponse:dict];
    }
    return self;
}

-(id)parseJson:(NSDictionary *)dict toObject:(NSString *)className
{
    if([className isEqualToString:NSStringFromClass([TileItem class])])
    {
        self.totalItem += 1;
        TileItem *item = [self parseJsonForQuestionItem:dict];
        
        if(self.searchText && ![self verifyDataForSearch:item])
        {
            return nil;
        }
        
        
        self.totalSearchItem += 1;
        return item;
    }
    
    
    return nil;
}
-(BOOL)verifyDataForSearch:(TileItem *)item
{
    if([item.name containsString:@"Valley"]) {
        NSLog(@"");
    }
    NSMutableString *itemString = [[NSMutableString alloc] init];
    if(item.title.length > 0)
        [itemString appendString:item.title];
    [itemString appendString:@" "];
    if(item.tileText.length > 0)
    [itemString appendString:item.tileText];
    [itemString appendString:@" "];
    if(item.shortDescription.length > 0)
    [itemString appendString:item.shortDescription];
    
    return [[itemString lowercaseString] containsString:[self.searchText lowercaseString]];
}

-(TileItem *)parseJsonForQuestionItem:(NSDictionary *)dict
{
    TileItem *item = [[TileItem alloc] init];
    item.ID = [dict stringForKey:@"ID"];
    item.title = [dict stringForKey:@"Title"];
    item.tileText = [dict stringForKey:@"TileText"];
    item.tileImage = [dict stringForKey:@"TileImage"];
    item.tileLink = [dict stringForKey:@"TileLink"];
    item.shortDescription = [dict stringForKey:@"ShortDescription"];
    item.GeographicRegion = [dict stringForKey:@"GeographicRegion"];
    item.SubCategory = [dict stringForKey:@"SubCategory"];
    
    if([self.categoryCode isEqualToString:@"appgallery"])
    {
        item.shortDescription = [self colorForGalleryPage:item.shortDescription];
    }
    
    return [self mappingItemForCategory:item];
}

-(void)parseCommonResponse:(NSDictionary *)dict
{
    self.status = [dict boolForKey:@"Success"];
    NSArray* mesArr = [dict arrayForKey:@"message"];
    if(mesArr && mesArr.count > 0){
        NSDictionary * error = [mesArr objectAtIndex:0];
        self.message = [error stringForKey:@"message"];
        self.errorCode = [error stringForKey:@"code"];
    }
    
    self.message = [dict objectForKey:@"message"];
}

-(NSString *)colorForGalleryPage:(NSString *)text
{
    NSRange rangeForColorText = NSMakeRange(NSNotFound, 0);
    if(text.length > 0)
        rangeForColorText = [text rangeOfString:@"#"];
    rangeForColorText.length = 7;
    return [text substringWithRange:rangeForColorText];
}

- (TileItem *)mappingItemForCategory:(TileItem *)item
{
    
    if([self.categoryCode isEqualToString:@"all"])
    {
        for(CategoryItem *cateItem in categoriesForCCAData)
        {
            if([cateItem.categoryIDs containsObject:item.ID])
            {
                item.categoryName = cateItem.categoryName;
                break;
            }
        }
        
        // remove item Custom / Speacia
        if([item.ID isEqualToString:@"36"])
            return nil;
        
    } else if(![self.categoryCode isEqualToString:@"appgallery"]) {
        if(![categoryIDsForCurrentCategory containsObject:item.ID])
        {
            return nil;
        }
    }
    
    // filter Hotels, Cruise, VP for all category
    if([self.currentCategory.code isEqualToString:@"all"] &&
       ![item.SubCategory isEqualToString:@"Global Partners"] && item.SubCategory.length > 0) {
        NSArray* listHasGeo = [CCAMapCategories getCategoriesIDsForCategoryHasGeographic];
        if([listHasGeo containsObject:item.ID]) {
            NSArray* geographic = [self.currentCity.GeographicRegion componentsSeparatedByString:@"|"];
            NSArray* subcate = [self.currentCity.SubCategory componentsSeparatedByString:@"|"];
            BOOL isOK = NO;
            
            for (int i = 0; i < geographic.count; i++) {
                NSString* geo = [geographic objectAtIndex:i];
                NSString* sub = [subcate objectAtIndex:i];
                if([geo isEqualToString:@"all"]) {
                    if([sub isEqualToString:item.SubCategory]) {
//                        NSLog(@"%s: %@ - %@",__PRETTY_FUNCTION__,item.SubCategory,item.GeographicRegion);
                        isOK = YES;
                        break;
                    }
                } else {
                    if([geo isEqualToString:item.GeographicRegion] && [sub isEqualToString:item.SubCategory]) {
//                        NSLog(@"%s: %@ - %@",__PRETTY_FUNCTION__,item.SubCategory,item.GeographicRegion);
                        isOK = YES;
                        break;
                    }
                }
            }
            
            if(!isOK) {
                return nil;
            }
        }
    }
    
    
    // allow item geo is null and same sub category
//    if([item.SubCategory stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0 && [item.GeographicRegion stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0) {
//        NSLog(@"Warning: this item has GeographicRegion null ==> %@",item.name);
//    }
    
    // if city dont have GeographicRegion && item not is Global Parntners
    if(self.currentCategory.isCheckGeographicRegion &&
       self.currentCity.GeographicRegion.length == 0 &&
       ![item.SubCategory isEqualToString:@"Global Partners"] &&
       item.SubCategory.length > 0)
        return nil;
    
    // !!!: Feature for Hotels, Cruise, VP
    if(self.currentCategory.isCheckGeographicRegion &&
       ![item.SubCategory isEqualToString:@"Global Partners"] &&
       [item.GeographicRegion stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0 &&
       [item.SubCategory stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0)
    {
        NSArray* geographic = [self.currentCity.GeographicRegion componentsSeparatedByString:@"|"];
        NSArray* subcate = [self.currentCity.SubCategory componentsSeparatedByString:@"|"];
        BOOL isOK = NO;
        
        for (int i = 0; i < geographic.count; i++) {
            NSString* geo = [geographic objectAtIndex:i];
            NSString* sub = [subcate objectAtIndex:i];
            if([geo isEqualToString:@"all"]) {
                if([sub isEqualToString:item.SubCategory]) {
//                    NSLog(@"%s: %@ - %@",__PRETTY_FUNCTION__,item.SubCategory,item.GeographicRegion);
                    isOK = YES;
                    break;
                }
            } else {
                if([geo isEqualToString:item.GeographicRegion] && [sub isEqualToString:item.SubCategory]) {
//                    NSLog(@"%s: %@ - %@",__PRETTY_FUNCTION__,item.SubCategory,item.GeographicRegion);
                    isOK = YES;
                    break;
                }
            }
        }
        
        if(!isOK) {
            return nil;
        }
    }

    if([item.name containsString:@"Valley"]) {
       NSLog(@"");
    }

    return item;
}



-(NSArray *)getIDsOfCurrentCategory
{
    NSArray *categories = [AppData getSelectionCategoryList];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"code == %@",self.categoryCode];
    NSArray *tempArray = [categories filteredArrayUsingPredicate:predicate];
    if(tempArray.count > 0)
    {
        return ((CategoryItem *)[tempArray objectAtIndex:0]).categoryIDs;
    }
    return nil;
}

-(NSArray *)getCategoriesForCCAData
{
    NSArray *categories = [AppData getSelectionCategoryList];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"categoryIDs.@count > 0"];
    return [categories filteredArrayUsingPredicate:predicate];
    
}
@end
