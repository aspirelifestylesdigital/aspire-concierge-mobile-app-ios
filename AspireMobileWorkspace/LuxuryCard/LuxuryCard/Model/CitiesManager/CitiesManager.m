//
//  CitiesManager.m
//  TestIOS
//
//  Created by Dai Pham on 7/12/17.
//  Copyright © 2017 Dai Pham. All rights reserved.
//

#import "CitiesManager.h"

#pragma mark -
@implementation CityObject
- (void) setIsValid:(BOOL)isValid {
    _isValid = isValid;
}

- (void) setName:(NSString *)name {
    _name = name;
}

- (void) setGroup:(NSString *)group {
    _group = group;
}

- (void) setIsRegion:(BOOL)isRegion {
    _isRegion = isRegion;
}

- (void) setIsDisplayForDining:(BOOL)isDisplayForDining {
    _isDisplayForDining = isDisplayForDining;
}

- (void) setIsDisplayForHotel:(BOOL)isDisplayForHotel {
    _isDisplayForHotel = isDisplayForHotel;
}

- (void) setIsDisplayForOther:(BOOL)isDisplayForOther {
    _isDisplayForOther = isDisplayForOther;
}
@end

#pragma mark -
@implementation CitiesManager
static CitiesManager *_shared;

+ (CitiesManager*) shared {
    static dispatch_once_t onceInstance;
    if(!_shared) {
        dispatch_once(&onceInstance, ^{
            _shared = [CitiesManager new];
        });
    }
    return _shared;
}

- (instancetype) init {
    if(self = [super init]) {
        [self getData];
    }
    return self;
}

#pragma mark - INTERFACE
- (NSArray*) getListCitiesForHotel {
    if(citiesForHotel.count == 0) {
        NSMutableArray* temp = [NSMutableArray new];
        for (CityObject* city in allCities) {
            if(city.isDisplayForHotel && city.isValid) {
                [temp addObject:city];
            }
        }
        citiesForHotel = temp;
        temp = nil;
    }
    
    citiesForHotel = [self sortCitiesFromArray:citiesForHotel];
    
    return citiesForHotel;
}

- (NSArray *)getListCitiesForOther {
    if(citiesForOther.count == 0) {
        NSMutableArray* temp = [NSMutableArray new];
        for (CityObject* city in allCities) {
            if(city.isDisplayForOther && city.isValid) {
                [temp addObject:city];
            }
        }
        citiesForOther = temp;
        temp = nil;
    }
    
    citiesForOther = [self sortCitiesFromArray:citiesForOther];
    
    return citiesForOther;
}

- (NSArray *)getListCitiesForDining {
    if(citiesForDining.count == 0) {
        NSMutableArray* temp = [NSMutableArray new];
        for (CityObject* city in allCities) {
            if(city.isDisplayForDining && city.isValid) {
                [temp addObject:city];
            }
        }
        citiesForDining = temp;
        temp = nil;
    }
    
    citiesForDining = [self sortCitiesFromArray:citiesForDining];
    
    return citiesForDining;
}

#pragma mark - PRIVATE
- (NSArray*) sortCitiesFromArray:(NSArray*) array {
    NSSortDescriptor* descriptorGroup = [NSSortDescriptor sortDescriptorWithKey:@"group" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    NSSortDescriptor* descriptorName = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    return [array sortedArrayUsingDescriptors:@[descriptorGroup,descriptorName]];
}

- (void) getData {
    NSString*  filepath = [[NSBundle mainBundle] pathForResource:@"Cities" ofType:@"json"];
    NSError* error;
    NSString*myJson = [[NSString alloc] initWithContentsOfFile:filepath encoding:NSUTF8StringEncoding error:&error];
    if(error != nil) {
        return;
    }
    
    NSArray* data = [NSJSONSerialization JSONObjectWithData:[myJson dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    
    if(error != nil) {
        return;
    }
    
    [self convertDataToCitiObject:data];
}

- (void) convertDataToCitiObject:(NSArray*)data {
    if(data.count == 0)
        return;
    NSMutableArray* temp = [NSMutableArray new];
    for (NSDictionary* obj in data) {
        CityObject* city = [CityObject new];
        if([[obj allKeys] containsObject:@"name"])
            [city setName:[obj objectForKey:@"name"]];
        if([[obj allKeys] containsObject:@"group"])
            [city setGroup:[obj objectForKey:@"group"]];
        if([[obj allKeys] containsObject:@"isValid"])
            [city setIsValid:[[obj objectForKey:@"isValid"] boolValue]];
        if([[obj allKeys] containsObject:@"isRegion"])
            [city setIsRegion:[[obj objectForKey:@"isRegion"] boolValue]];
        if([[obj allKeys] containsObject:@"isDisplayForDining"])
            [city setIsDisplayForDining:[[obj objectForKey:@"isDisplayForDining"] boolValue]];
        if([[obj allKeys] containsObject:@"isDisplayForHotel"])
            [city setIsDisplayForHotel:[[obj objectForKey:@"isDisplayForHotel"] boolValue]];
        if([[obj allKeys] containsObject:@"isDisplayForOther"])
            [city setIsDisplayForOther:[[obj objectForKey:@"isDisplayForOther"] boolValue]];
        if (city.name.length > 0)
            [temp addObject:city];
    }
    allCities = temp;
}
@end
