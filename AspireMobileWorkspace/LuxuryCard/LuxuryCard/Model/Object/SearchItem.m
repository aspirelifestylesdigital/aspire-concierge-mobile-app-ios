//
//  SearchItem.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/21/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "SearchItem.h"
#import "NSString+Utis.h"

@implementation SearchItem

-(NSString *)name
{
    return self.title;
}

-(NSString *)offer
{
    return [self.searchDescription removeRedudantNewLineInText];
}

-(BOOL)isOffer
{
    return self.hasOffer;
}
@end
