//
//  TileItem.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/6/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "TileItem.h"
#import "CCAMapCategories.h"

@implementation TileItem

-(NSString *)name
{
    return self.title;
}

-(NSString *)offer
{
    return self.tileText;
}

-(NSString *)imageURL
{
    return self.tileImage;
}

-(NSString *)address
{
    NSString* add = @" ";
    if([[CCAMapCategories getCategoriesIDsMutilCities] containsObject:self.ID]) {
        add = @"Multiple Cities";
    }
    return add;
}

-(BOOL) isOffer
{
    return YES;
}
@end
