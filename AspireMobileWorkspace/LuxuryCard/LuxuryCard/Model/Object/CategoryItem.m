//
//  CategoryItem.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "CategoryItem.h"

@implementation CategoryItem

- (CategoryItem *)copyWithZone:(NSZone *)zone
{
    CategoryItem *copy = [[[self class] alloc] init];
    
    if (copy) {
        copy.ID = self.ID;
        copy.supperCategoryID = self.supperCategoryID;
        copy.categoryName = self.categoryName;
        copy.subCategoryName = self.subCategoryName;
        copy.imageURL = self.imageURL;
        copy.categoryImg = self.categoryImg;
        copy.code = self.code;
        copy.categoriesForService = self.categoriesForService;
    }
    
    return copy;
}

@end
