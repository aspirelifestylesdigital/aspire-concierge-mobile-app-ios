//
//  SearchItem.h
//  MobileConcierge
//
//  Created by Chung Mai on 6/21/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseItem.h"

@interface SearchItem : BaseItem

@property(nonatomic, strong) NSString * searchDescription;
@property(nonatomic, strong) NSString * product;
@property(nonatomic, strong) NSString * score;
@property(nonatomic, strong) NSString * secondaryID;
@property(nonatomic, strong) NSString * title;
@property(nonatomic, strong) NSString *GeographicRegion;
@property(nonatomic, strong) NSString *SubCategory;

@property(nonatomic, assign) BOOL hasOffer;
@end
