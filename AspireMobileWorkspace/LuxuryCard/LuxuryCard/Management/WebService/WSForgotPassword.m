//
//  WSForgotPassword.m
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 7/19/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSForgotPassword.h"

@implementation WSForgotPassword

-(void)submitForgotPassword:(NSMutableDictionary*) data {
    _dataDict = data;
    _isSuccesful = NO;
    [self startAPIAfterAuthenticateWithAPI:PostForgotPasswordUrl ];
}

-(void)startAPIAfterAuthenticateWithAPI:(NSString*)apiStr{
    self.task = WS_AUTHENTICATION_LOGIN;
    self.subTask = WS_AUTHENTICATION;
    
    NSString* url = [MCD_API_URL stringByAppendingString:apiStr];
    [self POST:url withParams:[self buildRequestParams]];
    
}
-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    
    if(jsonResult) {
        if(self.delegate !=nil && [jsonResult boolForKey:@"success"]){
            _isSuccesful = YES;
            [self.delegate loadDataDoneFrom:self];
        }else{
            [self.delegate loadDataFailFrom:nil withErrorCode:400 errorMessage:@"Enter a valid email address."];
        }
    }
    else if(self.delegate != nil) {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}

-(void)processDataResultWithError:(NSError *)error
{
    [self.delegate loadDataFailFrom:nil withErrorCode:error.code];
}

#pragma mark - DataLoadDelegate

-(void)loadDataDoneFrom:(WSBase*)ws{
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
}

-(NSMutableDictionary*) buildRequestParams{
    
    NSMutableDictionary* dictKeyValues = [[NSMutableDictionary alloc] init];
    [dictKeyValues setObject:[_dataDict objectForKey:@"ConsumerKey"] forKey:@"ConsumerKey"];
    [dictKeyValues setObject:[_dataDict objectForKey:@"Email"] forKey:@"Email2"];
    [dictKeyValues setObject:[_dataDict objectForKey:@"Functionality"] forKey:@"Functionality"];
    
    return dictKeyValues;
}

@end
