//
//  WSRequestConcierge.m
//  LuxuryCard
//
//  Created by Viet Vo on 8/29/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "WSRequestConcierge.h"

#import "ConciergeObject.h"
#import "ConciergeDetailObject.h"

#import "WSB2CGetAccessToken.h"
#import "WSB2CGetRequestToken.h"

@interface WSRequestConcierge(){
    
    WSB2CGetAccessToken* wsB2CGetAccessToken;
    WSB2CGetRequestToken* wsB2CGetRequestToken;
}
@end

@implementation WSRequestConcierge

- (void)updateConcierge:(NSMutableDictionary*)data {
    _dataDict = data;
    self.task = WS_UPDATE_CONCIERGE;
    self.subTask = WS_ST_NONE;
    [self getRequestToken];
}
- (void)getRecentConcierge:(NSMutableDictionary*)data {
    _dataDict = data;
    self.task = WS_GET_RECENT_CONCIERGE;
    self.subTask = WS_ST_NONE;
    [self getRequestToken];
}
- (void)getConciergeDetail:(NSMutableDictionary*)data {
    _dataDict = data;
    self.task = WS_GET_DETAIL_CONCIERGE;
    self.subTask = WS_ST_NONE;
    [self getRequestToken];
}

- (void)getRequestToken {
    wsB2CGetRequestToken = [[WSB2CGetRequestToken alloc] init];
    wsB2CGetRequestToken.delegate = self;
    [wsB2CGetRequestToken getRequestToken];
}

-(void)startAPIAfterAuthenticateWithAPI:(NSString*)apiString{
    
    NSString* url = [MCD_API_URL stringByAppendingString:apiString];
    [self POST:url withParams:[self buildRequestParams]];
    
}
-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat {
    
    if(jsonResult) {
        switch (task) {
            case WS_UPDATE_CONCIERGE:
            {
                BOOL status = [jsonResult boolForKey:@"success"];
                if (status == NO) {
                    NSArray* mesArr = [jsonResult arrayForKey:@"message"];
                    if(mesArr && mesArr.count > 0){
                        NSDictionary * error = [mesArr objectAtIndex:0];
                        [self.delegate loadDataFailFrom:nil withErrorCode:400 errorMessage:[error objectForKey:@"message"]];
                    }else{
                        [self.delegate loadDataFailFrom:nil withErrorCode:400 errorMessage:@"Unknown error"];
                    }
                }else{
                    NSString *transactionID = [jsonResult objectForKey:@"transactionId"];
                    if (transactionID.length > 0) {
                        [[SessionData shareSessiondata] setTransactionID:transactionID];
                    }
                    [self.delegate loadDataDoneFrom:self];
                }
            }
                break;
            case WS_GET_RECENT_CONCIERGE:
            {
                if ([jsonResult isKindOfClass:[NSArray class]]) {
                    NSArray *arrayData = (NSArray*)jsonResult;
                    NSMutableArray *arrayConcierges = [[NSMutableArray alloc] init];
                    for (NSDictionary *item in arrayData) {
                        ConciergeObject *iConcierge = [[ConciergeObject alloc] initFromDict:item];
    
                        [arrayConcierges addObject:iConcierge];
                    }
                    self.data = arrayConcierges;
                    [self.delegate loadDataDoneFrom:self];
                }else{
                    [self.delegate loadDataFailFrom:nil withErrorCode:400 errorMessage:@"Unknown error"];
                }
            }
                break;
            case WS_GET_DETAIL_CONCIERGE:
            {
                if ([jsonResult isKindOfClass:[NSDictionary class]]) {
                    ConciergeDetailObject *detailConcierge = [[ConciergeDetailObject alloc] initFromDict:jsonResult];
                    NSLog(@"%@",detailConcierge);
                    if (![jsonResult[@"TransactionID"] isKindOfClass:[NSNull class]]) {
//                        NSMutableArray *arr = [[NSMutableArray alloc] init];
//                        [arr addObject:detailConcierge];
                        self.data = @[detailConcierge];
                        [self.delegate loadDataDoneFrom:self];
                    }else{
                       [self.delegate loadDataFailFrom:nil withErrorCode:400 errorMessage:@"Unknown error"];
                    }
                }else{
                    [self.delegate loadDataFailFrom:nil withErrorCode:400 errorMessage:@"Unknown error"];
                }
            }
                break;
                
            default:
                break;
        }
    }else{
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}

-(void)processDataResultWithError:(NSError *)error {
    [self.delegate loadDataFailFrom:nil withErrorCode:error.code];
}

#pragma mark - DataLoadDelegate
-(void)loadDataDoneFrom:(WSBase*)ws {
    if(ws.task == WS_GET_REQUEST_TOKEN) {
        wsB2CGetAccessToken = [[WSB2CGetAccessToken alloc] init];
        wsB2CGetAccessToken.delegate = self;
        [wsB2CGetAccessToken requestAccessToken:encodeURLFromString(wsB2CGetRequestToken.requestToken) member:[[SessionData shareSessiondata] OnlineMemberID]];
    } else if(ws.task == WS_GET_ACCESS_TOKEN){
        
        switch (self.task) {
            case WS_UPDATE_CONCIERGE:
            {
                [self startAPIAfterAuthenticateWithAPI:UpdateConciergeRequest];
            }
                break;
            case WS_GET_RECENT_CONCIERGE:
            {
                [self startAPIAfterAuthenticateWithAPI:GetRecentConciergeRequest];
            }
                break;
            case WS_GET_DETAIL_CONCIERGE:
            {
                [self startAPIAfterAuthenticateWithAPI:GetDetailConciergeRequest];
            }
                break;
            default:
                break;
        }
    }
}

- (void)loadDataFailFrom:(id<WSBaseProtocol>)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message{
    
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{

}

-(NSMutableDictionary*) buildRequestParams{
    
    NSMutableDictionary* dictKeyValues = [[NSMutableDictionary alloc] init];
    
    switch (self.task) {
        case WS_UPDATE_CONCIERGE:
        {
            [dictKeyValues setObject:[[SessionData shareSessiondata] AccessToken] forKey:@"AccessToken"];
            [dictKeyValues setObject:[_dataDict objectForKey:@"AttachmentPath"] forKey:@"AttachmentPath"];
            [dictKeyValues setObject:[_dataDict objectForKey:@"TransactionID"] forKey:@"TransactionID"];
            [dictKeyValues setObject:B2C_ConsumerKey forKey:@"ConsumerKey"];
            [dictKeyValues setObject:B2C_EPCClientCode forKey:@"EPCClientCode"];
            [dictKeyValues setObject:B2C_EPCProgramCode forKey:@"EPCProgramCode"];
            [dictKeyValues setObject:@"AMEND" forKey:@"EditType"];
            [dictKeyValues setObject:@"Mobile App" forKey:@"Source"];
            [dictKeyValues setObject:B2C_VERIFICATION_CODE forKey:@"VeriCode"];
            [dictKeyValues setObject:@"Other Request" forKey:@"Functionality"];
            [dictKeyValues setObject:@"O Client Specific" forKey:@"RequestType"];
            
            [dictKeyValues setObject:[_dataDict objectForKey:@"Salutation"] forKey:@"Salutation"];
            [dictKeyValues setObject:[_dataDict objectForKey:@"FirstName"] forKey:@"FirstName"];
            [dictKeyValues setObject:[_dataDict objectForKey:@"LastName"] forKey:@"LastName"];
            [dictKeyValues setObject:[_dataDict objectForKey:@"PrefResponse"] forKey:@"PrefResponse"];
            [dictKeyValues setObject:[_dataDict objectForKey:@"EmailAddress1"] forKey:@"EmailAddress1"];
            [dictKeyValues setObject:[_dataDict objectForKey:@"PhoneNumber"] forKey:@"PhoneNumber"];
            [dictKeyValues setObject:[[SessionData shareSessiondata] OnlineMemberID]  forKey:@"OnlineMemberId"];
            [dictKeyValues setObject:[_dataDict objectForKey:@"RequestDetails"] forKey:@"RequestDetails"];
        }
            break;
        case WS_GET_RECENT_CONCIERGE:
        {
            [dictKeyValues setObject:[[SessionData shareSessiondata] AccessToken] forKey:@"AccessToken"];
            [dictKeyValues setObject:B2C_ConsumerKey forKey:@"ConsumerKey"];
            [dictKeyValues setObject:@"GetRecentRequests" forKey:@"Functionality"];
            [dictKeyValues setObject:[[SessionData shareSessiondata] OnlineMemberID]  forKey:@"OnlineMemberId"];
            [dictKeyValues setObject:[_dataDict objectForKey:@"RowEnd"] forKey:@"RowEnd"];
            [dictKeyValues setObject:[_dataDict objectForKey:@"RowStart"] forKey:@"RowStart"];
        }
            break;
        case WS_GET_DETAIL_CONCIERGE:
        {
            [dictKeyValues setObject:[[SessionData shareSessiondata] AccessToken] forKey:@"AccessToken"];
            [dictKeyValues setObject:B2C_ConsumerKey forKey:@"ConsumerKey"];
//            [dictKeyValues setObject:@" " forKey:@"EPCCaseID"];
            [dictKeyValues setObject:@"GetRecentRequests" forKey:@"Functionality"];
            [dictKeyValues setObject:[[SessionData shareSessiondata] OnlineMemberID] forKey:@"OnlineMemberId"];
            [dictKeyValues setObject:[_dataDict objectForKey:@"TransactionID"] forKey:@"TransactionID"];
        }
            break;
            
        default:
            break;
    }
    
    return dictKeyValues;
}
@end
