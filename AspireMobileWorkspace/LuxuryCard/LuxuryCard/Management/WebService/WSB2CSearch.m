//
//  WSB2CSearch.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/21/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSB2CSearch.h"
#import "SearchResponseObject.h"

#import "NSTimer+Block.h"

@implementation WSB2CSearch

-(void)retrieveDataFromServer
{
    
    self.pageSize = 20;
    NSString* url = [B2C_IA_API_URL stringByAppendingString:GetDataBasedOnSearchText];
    [self POST:url withParams:[self buildRequestParams]];
}

-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    
    // cause of security scanning, not allow get and set password directly
    NSString* userPass = B2C_PW;
    [dictKeyValues setObject:B2C_SUBDOMAIN forKey:@"subDomain"];
    [dictKeyValues setObject:userPass forKey:@"password"];
    [dictKeyValues setObject:self.searchText forKey:@"searchString"];
    
    if(self.cities.count > 0)
    {
        NSData *json = [NSJSONSerialization dataWithJSONObject:self.cities options:0 error:nil];
        //assign back
        NSArray *array = [NSJSONSerialization JSONObjectWithData:json options:0 error:nil];
        [dictKeyValues setObject:array forKey:@"cities"];
    }
    
    if(self.pageIndex == 0)
    {
        self.pageIndex = 1;
    }
    
    [dictKeyValues setObject:[NSNumber numberWithInteger:self.pageSize] forKey:@"pageSize"];
    [dictKeyValues setObject:[NSNumber numberWithInteger:self.pageIndex] forKey:@"pageNumber"];
    return dictKeyValues;
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        SearchResponseObject *response = [[SearchResponseObject alloc] init];
        response.task = self.task;
        response.hasOffer = self.hasOffer;
        response.currentCity = self.currentCity;
        response.categoryCode = self.categoryCode;
        response.searchText = self.searchText;
        response.currentCategory = self.currentCategory;
        response.categories = self.currentCity.subCategories;
        
        response = [response initFromDict:jsonResult];
        
        if(self.delegate != nil && response.isSuccess){
            self.data = [response data];
            
            if(self.hasOffer)
            {
//                if(response.totalItem == self.pageSize && response.offerNumber == 0)
//                {
//                    [NSTimer timerWithTimeout:1 andBlock:^(NSTimer* timer){
//                        self.pageIndex += 1;
//                        [self retrieveDataFromServer];
//                    }];
//                }
//                else
                {
                    self.isHasNextPage = response.totalResultItemDontFilter == 0 ? NO : YES;//(self.pageSize > response.totalItem) ? NO : YES;
                    
                        [self.delegate loadDataDoneFrom:self];
                }
            }
            else
            {
                self.isHasNextPage = response.totalResultItemDontFilter == 0 ? NO : YES;//(self.pageSize > response.totalItem)  ? NO : YES;
                
                    [self.delegate loadDataDoneFrom:self];
            }
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.isSuccess];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}

-(void)processDataResultWithError:(NSError *)error
{
    [self.delegate loadDataFailFrom:nil withErrorCode:error.code errorMessage:error.localizedDescription];
}



@end
