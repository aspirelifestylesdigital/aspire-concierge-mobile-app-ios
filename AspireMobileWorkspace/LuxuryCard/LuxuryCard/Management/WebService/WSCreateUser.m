//
//  WSCreateUser.m
//  ALC
//
//  Created by Anh Tran on 8/29/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.

//  '- Step1: 2.2.1 Registration (API Endpoint: Registration)
//     Input:
//        All user information,
//        a ConsumerKey is stored on app,
//        VerificationCode: from B2C api definition
//  - Step2: Endpoint: AuthoriseRequestToken
//  - Step3: Endpoint: GetAccessToken
//  - Step 4: Endpoint: GetUserDetails
//

#import "WSCreateUser.h"
#import "UserRegistrationResponseObject.h"
#import "UserRegistrationItem.h"
#import "WSB2CGetAccessToken.h"
#import "WSB2CGetRequestToken.h"
#import "WSB2CGetUserDetails.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "NSDictionary+SBJSONHelper.h"
#import "EnumConstant.h"

#define USER_SECRET @"Password"

@interface WSCreateUser (){
    NSString *onlineMemberId;
    WSB2CGetAccessToken* wsB2CGetAccessToek;
    WSB2CGetRequestToken* wsB2CGetRequestToken;
    WSB2CGetUserDetails* wsGetUser;
    BOOL isUpdateProfile; // YES = UpdateProfile ; NO = CreateProfile
}

@end

@implementation WSCreateUser

-(void)createUser:(NSMutableDictionary*) userDict{
    _user = userDict;
    isUpdateProfile = NO;
    [self startAPIAfterAuthenticateWithAPI:PostRegistration ];
}

-(void)updateUser:(NSMutableDictionary*)userDict{
    _user = userDict;
    isUpdateProfile = YES;
    [self startAPIAfterAuthenticateWithAPI:PostUpdateRegistration];
}

-(void)startAPIAfterAuthenticateWithAPI:(NSString*)apiStr{
    self.task = WS_CREATE_USER;
    self.subTask = WS_ST_NONE;
    
    
    NSString* url = [MCD_API_URL stringByAppendingString:apiStr];
    [self POST:url withParams:[self buildRequestParams]]; 
    
}
-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    
    if(jsonResult){
        UserRegistrationResponseObject *result = [[UserRegistrationResponseObject alloc] initFromDict:jsonResult];
        result.task = task;
        if(self.delegate !=nil && result.isSuccess){
            
            self.data = [result data];
            [self setUserDefaultInfoWithDict:self.data[0]];
            //Request token
            wsB2CGetRequestToken = [[WSB2CGetRequestToken alloc] init];
            wsB2CGetRequestToken.delegate = self;
            [wsB2CGetRequestToken getRequestToken];
            
        } else if(self.delegate != nil){

            [self.delegate loadDataFailFrom:nil withErrorCode:400 errorMessage:([result.errorCode isEqualToString:@"ENR68-2"] || [result.errorCode isEqualToString:@"ENR61-1"]) ? result.errorCode : result.message];
        }

    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
    
}
- (void)setUserDefaultInfoWithDict:(UserRegistrationItem*)item{
    [[SessionData shareSessiondata] setOnlineMemberID:item.OnlineMemberID];
    [[SessionData shareSessiondata] setOnlineMemberDetailIDs:item.OnlineMemberDetailIDs];
}

-(void)processDataResultWithError:(NSError *)error
{
    [self.delegate loadDataFailFrom:nil withErrorCode:error.code];
}

#pragma mark - DataLoadDelegate

-(void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_GET_REQUEST_TOKEN){
        wsB2CGetAccessToek = [[WSB2CGetAccessToken alloc] init];
        wsB2CGetAccessToek.delegate = self;
        [wsB2CGetAccessToek requestAccessToken:encodeURLFromString(wsB2CGetRequestToken.requestToken) member:[[SessionData shareSessiondata] OnlineMemberID]];
    } else if(ws.task == WS_GET_ACCESS_TOKEN){
        wsGetUser = [[WSB2CGetUserDetails alloc] init];
        wsGetUser.delegate = self;
        [wsGetUser getUserDetails];
    } else if(ws.task == WS_B2C_GET_USER_DETAILS){
        if(wsGetUser.userDetails != nil){
            self.userDetails = wsGetUser.userDetails;
            [self.delegate loadDataDoneFrom:self];
        } else {
            BaseResponseObject *result = [[BaseResponseObject alloc] init];
            result.task = self.task;
            result.status = 400;
            [self.delegate loadDataFailFrom:result withErrorCode:result.status];
        }
    }
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    result.task = self.task;
    [self.delegate loadDataFailFrom:result withErrorCode:errorCode];
}

-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    
    // cause of security scanning, not allow get and set password directly
    
    NSMutableDictionary* memberDic = [[NSMutableDictionary alloc] init];
    [memberDic setObject:[_user objectForKey:@"ConsumerKey"] forKey:@"ConsumerKey"];
    [memberDic setObject:[_user objectForKey:@"Email"] forKey:@"Email"];
    [memberDic setObject:[_user objectForKey:@"FirstName"] forKey:@"FirstName"];
    [memberDic setObject:[_user objectForKey:@"LastName"] forKey:@"LastName"];
    [memberDic setObject:[_user objectForKey:@"Functionality"] forKey:@"Functionality"];
    [memberDic setObject:[_user objectForKey:@"MemberDeviceID"] forKey:@"MemberDeviceID"];
    [memberDic setObject:[_user objectForKey:@"MobileNumber"] forKey:@"MobileNumber"];
    [memberDic setObject:[_user objectForKey:@"Salutation"] forKey:@"Salutation"];
    [memberDic setObject:[_user objectForKey:@"ZipCode"] forKey:@"PostalCode"];
    
    if (isUpdateProfile == YES) {
        [memberDic setObject:[[SessionData shareSessiondata] OnlineMemberID] forKey:@"OnlineMemberID"];
    }else{
        [memberDic setObject:[_user objectForKey:@"Password"] forKey:@"Password"];
    }
    
    [dictKeyValues setObject:memberDic forKey:@"Member"];
    
    NSMutableDictionary *verificationDict = [[NSMutableDictionary alloc] init];

    [verificationDict setObject:B2C_VERIFICATION_CODE
                             forKey:@"VeriCode"];

    if (isUpdateProfile == YES) {
        [verificationDict setObject:[[SessionData shareSessiondata] OnlineMemberDetailIDs]
                             forKey:@"OnlineMemberDetailIDs"];
    }
    NSMutableArray *memberDetails = [[NSMutableArray alloc] init];
    [memberDetails addObject:verificationDict];
    
    [dictKeyValues setObject:memberDetails forKey:@"MemberDetails"];
    return dictKeyValues;
}

@end
