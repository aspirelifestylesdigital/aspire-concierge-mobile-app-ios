//
//  Common.h
//  Reservations
//
//  Created by Linh Le on 10/22/12.
//  Copyright (c) 2012 Linh Le. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Country.h"

typedef enum
{
    THREE_DOT_FIVE_INCH,
    FOUR_INCH,
    UNDEFINED
}ScreenSize;


@interface Common : NSObject


extern int NUMBER_ROW_UPDATED;

extern ScreenSize screenSize;
extern ScreenSize getScreenSize();
extern NSString* urlencode(NSString* url);
extern NSString* getUniqueIdentifier();
extern NSString* getPhotoUrlBySizeAndSecret(NSString * oldUrl, int width, int height, NSString *secret);
extern NSString* getPhotoUrlByIdAndSize(NSInteger photoId, NSString *photoBaseUrl, int width, int height);
extern NSString* reverseString(NSString *inputString);
extern NSString* formatDateForBookingDisplay(NSDate *date);

// flattent html
extern NSString* flattenHTML(NSString *html);
UIImage* scaleImageWithSize(UIImage *imagetemp ,CGSize newSize);

// format date
NSString* formatDateToString(NSDate* date, NSString*format);
extern NSString* formatFullDate(NSDate* date,BOOL isInAMPM);
extern NSString* formatFullDateForReview(NSDate* date,BOOL isInAMPM);
extern NSString* formatTimeSlot(NSInteger timeSlotId);
extern NSString* getDistanceDay(NSDate* starDay);
extern NSString* formatDateForBooking(NSDate *date);
extern NSString* dateToTimeStampForBooking(NSDate *date);
extern NSString* formatDateForContent(NSDate *date);

extern NSString* base64forData(NSData* theData);
extern NSMutableAttributedString* letterSpacing(float spacing, NSString* string);
extern NSString* getDistance(float item);
extern NSDate* convertUnixTimeToDate(double timestamp);
extern float iosVersion();

extern NSString *formatHTMLString(NSString *htmlString, NSString * tag, int width, int height);
BOOL isSameDay(NSDate* date1, NSDate*date2);
extern UIImageView* createCircleMask(UIImageView *imageView,int radiusConner);
extern NSDate* countDateNextMonth(int month);
extern NSString* formatTimeForSearch(NSDate *date);
extern NSDate* countDateNextHour(int hour);
extern NSString * removeZeroTrail(NSString * stringSource);
extern NSString* getMonthName(NSDate *date);
extern NSString* getMonthNamePromotion(NSDate *date);
extern NSString* getDayOfWeek(NSDate *date);
extern NSString* getDateOfMonth(NSDate *date);
extern NSString* getHourOfDate(NSDate *date);
extern CGSize getSizeWithFont(NSString *string,UIFont *font);
extern CGSize getSizeWithFontAndFrame(NSString *string,UIFont *font,int width,int height);
extern NSDate* countDateNextMinute(int minute);
extern NSString* formatDate(NSDate* date, NSString *format);
NSDate* adjustDateToFirstTime(NSDate* date);
extern NSString* stringByStrippingHTML(NSString* data);
extern void resetScaleViewBaseOnScreen(UIView* view);
extern void resetScaleIndividualViewBaseOnScreenSize(UIView *view);
extern NSDate* convertStringToDate(NSString* dateString, NSString* dateFormat);
extern BOOL isValidDateTimeWithin24h(NSDate* date, NSDate* time);;
extern NSDate* convertStringToDateWithTimeZone(NSString* dateString, NSString* dateFormat, NSString* timezone);
extern BOOL isWithin24h(NSDate* date1, NSDate* date2);
extern BOOL isMoreDateh(NSDate* date1, NSDate* date2);

extern UIColor* colorFromHexString(NSString *hexString);
extern UIColor* colorFromHexStringWithAlpha(NSString *hexString, CGFloat alpha);
extern NSAttributedString* handleHTMLString(NSString*str, NSString *fontName, CGFloat pointSize, NSString *textColor);
//haven't review
extern NSString* stringFromObject(id object);
extern NSString* encodeURLFromString(NSString *string);
extern CGPoint getPositionView(UIView* view);
extern void getPreferencesFromAPI(BOOL isDelegate, id fromVC);
#pragma mark - Get Top Controller

extern NSString* platformType(NSString *platform);
extern BOOL isNetworkAvailable();
extern NSString* removeSpecialCharacters(NSString* value);
extern NSMutableDictionary* removeSpecialCharactersfromDict(NSMutableDictionary* dict);
extern BOOL isNotIncludedSpecialCharacters(NSString* text);
extern void trackGAICategory(NSString* category, NSString* action, NSString* label,NSNumber* value);
NSString* justifyHTMLString(NSString*text, NSString *fontName, CGFloat pointSize, NSString *textColor);
NSString* centerHTMLString(NSString*text, NSString *fontName, CGFloat pointSize, NSString *textColor);

extern BOOL isJailbroken();

extern NSString* strimStringFrom(NSString* from, NSArray* pattern);
extern NSArray* getMatchesFromString(NSString* string, NSString* pattern, NSUInteger expression);
extern NSArray* getCountryList();
extern Country* getCountryByNumber(NSString *number);
extern Country* getCountryByCodeName(NSString *codeName);

extern NSDate* dateFromISO8601Time(NSString *dateString);
extern NSString* stringDateWithFormat(NSString *format, NSDate *date);
extern NSDate* dateFromStringWithFormat(NSString *format, NSString *dateString);
extern NSDate* dateFromTime(NSString *dateString);
@end
