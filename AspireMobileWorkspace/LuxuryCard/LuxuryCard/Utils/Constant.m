//
//  Constant.m
//  ALC
//
//  Created by Anh Tran on 8/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "Constant.h"

/***
 /// Define Font
 ***/
NSString *const FONT_MarkForMC_BOLD = @"ProximaNova-Bold";
NSString *const FONT_MarkForMC_MED = @"ProximaNova-Semibold";
NSString *const FONT_MarkForMC_LIGHT = @"ProximaNova-Light";
NSString *const FONT_MarkForMC_REGULAR = @"ProximaNova-Regular";
NSString *const FONT_MarkForMC_REGULAR_It = @"ProximaNova-RegularIt";
NSString *const FONT_MarkForMC_Nrw_BOLD = @"ProximaNovaAExCn-Bold";
NSString *const DEFAULT_BACKGROUND_COLOR = @"#000000";//@"#011627";
NSString *const DEFAULT_PLACEHOLDER_COLOR = @"#99A1A8";
NSString *const DEFAULT_HIGHLIGHT_COLOR = @"#e1cb78";


@implementation Constant

@end
