//
//  ImageUtilities.h
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/14/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImageUtilities : NSObject

+(void) setImageViewCircularFrame:(UIImageView *)imageView;
+ (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size;
@end
