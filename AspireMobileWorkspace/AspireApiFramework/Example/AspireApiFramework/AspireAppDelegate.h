//
//  AspireAppDelegate.h
//  AspireApiFramework
//
//  Created by vuongtrancong on 10/26/2018.
//  Copyright (c) 2018 vuongtrancong. All rights reserved.
//

@import UIKit;

@interface AspireAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
