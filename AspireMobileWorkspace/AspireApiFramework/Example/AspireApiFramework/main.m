//
//  main.m
//  AspireApiFramework
//
//  Created by vuongtrancong on 10/26/2018.
//  Copyright (c) 2018 vuongtrancong. All rights reserved.
//

@import UIKit;
#import "AspireAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AspireAppDelegate class]));
    }
}
