//
//  MSTests.m
//  AspireApiFramework_Tests
//
//  Created by VuongTC on 11/2/18.
//  Copyright © 2018 vuongtrancong. All rights reserved.
//

#import "MSTests.h"
@import AspireApiFramework;

@implementation MSTests

- (void)setUp {
}

- (void)testNormalSignIn {
    //Register sevice as MS project.
    [ModelAspireApiManager registerServiceUsername:@"hms-qa@aspire.org"
                                          password:@"P@ssw0rd1234"
                                           program:@"MORGAN STANLEY PERSONAL ASSISTANTS"
                                               bin:@"2881234"];
    [ModelAspireApiManager registerClient:@"HMS" xAppId:@"893517d3ad8740659a27009189b28368"];
    
    NSString* email = @"anhanh102@mailinator.com";
    NSString* pasword = @"Admin123&&&";

//    NSString* email = @"tcvuong22@gmail.com";
//    NSString* pasword = @"Zz1234!@#$";

    XCTestExpectation* expected = [self expectationWithDescription:@"api"];
    [ModelAspireApiManager loginWithUsername:email password:pasword completion:^(NSError *error) {
        XCTAssert(error == nil);
        [expected fulfill];
    }];
    [self waitForExpectationsWithTimeout:15.0 handler:nil];
}


@end
