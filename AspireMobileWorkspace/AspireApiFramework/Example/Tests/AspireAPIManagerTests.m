//
//  AspireAPIManagerTests.m
//  AspireApiFramework_Tests
//
//  Created by VuongTC on 10/29/18.
//  Copyright © 2018 vuongtrancong. All rights reserved.
//

@import XCTest;
@import AspireApiFramework;

@interface AspireAPIManagerTests : XCTestCase

@end

@implementation AspireAPIManagerTests

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testGetSecurityQuetions {
    NSArray* question = [ModelAspireApiManager getSecurityQuetions];
    XCTAssert(question.count > 0, @"The list cannot be empty.");
}

- (void)testLogging {
    [ModelAspireApiManager enableLogging];
    [[ModelAspireApiManager shared] stopRequest];
}

@end
