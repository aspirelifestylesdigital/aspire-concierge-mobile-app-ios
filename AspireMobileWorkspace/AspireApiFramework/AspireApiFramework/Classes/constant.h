//
//  constant.h
//  AspireApiFramework
//
//  Created by dai on 8/22/18.
//  Copyright © 2018 com.aspirelifestyles.ios.mobileconcierge.UnitTest. All rights reserved.
//

#ifndef constant_h
#define constant_h

#define DMA_SERVICE_ACCOUNT                     @"dma-qa@aspire.org"
#define DMA_SERVICE_PASSWORD                    @"P@ssw0rd123"

#define DMA_AUTH_ACC                            @"0oaeanvkzevwMjXAY0h7"
#define DMA_AUTH_PAS                            @"4eNOLghFGDCDHBm2SdcE48wcwGfPtKYmoLX8pI7F"

#define PMA_SERIVCE_ACCOUNT                     @"svc_PMA_OKTA@isosdev.com"
#define PMA_SERVICE_PASSWORD                    @"P@ssword123"

#define PMA_AUTH_ACC                            @"0oa1p9xvdrsLwU0MS0i7"
#define PMA_AUTH_PAS                            @"wvzrh2eWnnHlA28fPuwC3o9zKbhsTm0dsVpYpbxb"

#define ASPIRE_API_PASSWORD_TOKEN_TYPE          @"SSWS"
#define ASPIRE_API_PASSWORD_TOKEN               @"00jwhHpyTm9tHaIDbeHC6A91vzLbgsx7KjCI8dW4pg"

#define VERIFICATIONMETADATA                    @{@"bin":@(6445785)}

#define PROGRAM_REQUEST                         @"DEMO SINGAPORE"

// AspireAPI
#define ASPIREAPI_CREATE_CUSTOMER_URL           @"https://ace-digital-exp-customers-qa.au.cloudhub.io/customers"
#define ASPIREAPI_PROFILE_URL                   @"https://ace-digital-exp-reg-qa.au.cloudhub.io/profiles?="
#define ASPIREAPI_REQUEST_URL                   @"https://ace-digital-exp-unr-qa.au.cloudhub.io/concierge/cases"
#define ASPIREAPI_VERIFY_BIN_URL                @"https://ace-digital-exp-general-qa.au.cloudhub.io/customers/verifications"

// OKTA
#define ASPIREAPI_REVOKE_TOKEN_URL              @"https://aspire.oktapreview.com/oauth2/v1/revoke"
#define ASPIREAPI_TOKEN_URL                     @"https://aspire.oktapreview.com/oauth2/v1/token"
#define ASPIREAPI_CHANGE_PASSWORD_URL           @"https://aspire.oktapreview.com/api/v1/users/[@email]/credentials/change_password"
#define ASPIREAPI_RESET_PASSWORD_URL            @"https://aspire.oktapreview.com/api/v1/users/[@email]/lifecycle/reset_password?sendEmail=true"
#define ASPIREAPI_FORGOT_PASSWORD_URL           @"https://aspire.oktapreview.com/api/v1/users/[@email]/credentials/forgot_password?sendEmail=true"
#define ASPIREAPI_PROFILE_OKTA_URL              @"https://aspire.oktapreview.com/api/v1/users/[@email]"
#define ASPIREAPI_CHANGE_RECOVERY               @"https://aspire.oktapreview.com/api/v1/users/[@id]/credentials/change_recovery_question"

// PMA
#define PMA_SEARCH_PARTY_PERSON_URL             @"https://party-master-exp-qa.au.cloudhub.io/api/v1/parties/search?applicationCode=SSePC&parentOrgCode=[@clientID]&"
#define PMA_GET_TOKEN_URL                       @"https://intlsos-test.okta-emea.com/oauth2/v1/token"
#define PMA_GET_PARTY_PERSON                    @"https://party-master-exp-qa.au.cloudhub.io/api/v1/parties/[@partyID]?applicationCode=SSePC&contacts=true&locations=true&parentOrgCode=[@clientID]"


#endif /* constant_h */
