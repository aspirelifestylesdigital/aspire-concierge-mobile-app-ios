//
//  ModelAspireApiManager.h
//  MobileConciergeUSDemo
//
//  Created by 💀 on 5/28/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASPIREAPI_AccessToken.h"
#import "GroupObject.h"
#import "AspireApiError.h"
#import "OKTAUserProfile.h"
#import "PMAUser.h"

#define APP_USER_PREFERENCE_Location        @"User Location Status"
#define APP_USER_PREFERENCE_Newsletter      @"Newsletter Status"
#define APP_USER_PREFERENCE_Passcode        @"Passcode"
#define APP_USER_PREFERENCE_Selected_City   @"Selected City"
#define APP_USER_PREFERENCE_Policy_Version  @"Policy Version"
#define APP_USER_PREFERENCE_DeviceOS        @"Device OS"
#define APP_USER_PREFERENCE_Newsletter_Date @"Newsletter Date"

#define LIST_APP_USER_REFERENCES @[APP_USER_PREFERENCE_Location,APP_USER_PREFERENCE_Newsletter,APP_USER_PREFERENCE_Passcode,APP_USER_PREFERENCE_Selected_City,APP_USER_PREFERENCE_Policy_Version,APP_USER_PREFERENCE_DeviceOS,APP_USER_PREFERENCE_Newsletter_Date]

@interface ModelAspireApiManager : NSObject

#pragma mark - check Network before request

/**
 Block to get result state internet connection
 */
@property (nonatomic, copy) BOOL (^checkingNetwork)(void);

/**
 Block involke when the username or password wrong
 */
@property (nonatomic, copy) void (^errorAuthorized)(void);

/**
 Show all request/response log
 */
@property BOOL showAllLog;

/**
 Get a singleton of ModelAspireApiManager object, you can access properties

 @return singleton of ModelAspireApiManager object
 */
+ (ModelAspireApiManager*) shared;

/**
 continue list api requests if have. Used when app is active status
 */
- (void) continueRequest;

/**
 suspend next request if have, if you want continue should call contine request. Used when app is resign active
 */
- (void) stopRequest;

/**
 Did you known ModelAspireApiManager is requesting api?

 @return if requesting return YES else return NO
 */
- (BOOL) isRequesting;

#pragma mark - Handle data stored
/**
 Register AspireFramework lot of important information to get service access token.
 
How it used:
 
  1. Should call this function didFinishLaunchingWithOptions in AppDelete.m before used this framework.
 
  2. If app used with dynamic bin number. After verifybin success, should call it again and fill with bin success.

 @param username account service
 @param password password serivce
 @param program used to request service ask the concierge
 @param bin used to update profile.
 */
+ (void) registerServiceUsername:(nullable NSString*)username
                        password:(nullable NSString*) password
                         program:(nullable NSString*) program
                             bin:(nullable NSString*) bin;

/**
 Register client, xappId. every request to pma server them will fill to the header
 
 Note: access link to known: https://docs.google.com/spreadsheets/d/16soUU_M52t2WJoBYCw0ymTSiBRPziXqT9TRVGwztQ_c/edit#gid=67828823
 
 @param client DMA,MCD, vvvvv
 @param xAppId id for client
 */
+ (void) registerClient:(nullable NSString*)client
                        xAppId:(nullable NSString*) xAppId;

/**
 Clean every data user, access token stored at local.
 
 How it used: Should call when you sign out an account.
 */
+ (void) logout;

#pragma mark - Profiles
/**
 terminate current access token.
 
 How it used: when sign out or app enter background.
 */
+ (void) revokeToken;

/**
 Login to pma api with the username and password.
 
 Steps:
 
 1. Get profile OKTA from the username. If the user exists then check current status. (LOCKED_OUT) return error
 
 2. Get access token from username & password, store access token at local
 
 3. Use access token from the step above to retrieve profile. store profile at local

 @param username email
 @param password password
 @param completion error != nil -> login success, else use AspireApiError to get type error from error returned
 */
+ (void) loginWithUsername:(NSString*)username password:(NSString*)password completion:(void(^)(NSError*error)) completion;

/**
 Retrieve the profile of the current user

 Steps:
 
 1. Check the token is valid, if invalid, request access token again and store new access token  at local.
 
 2. Use access token from the step above to retrieve profile. store profile at local
 
 @param completion callback a block with User object and NSError
 */
+ (void) retrieveProfileCurrentUser:(void(^)(User* user,NSError*error)) completion;

/**
 Update profile with new information

 Steps:
 
 1. Check the token is valid, if invalid, request access token again and store new access token  at local.
 
 2. Use access token from the step above to update profile.
 
 3. Use access token from the step above to retrieve profile. store profile at local
 
 @param userinfo a dictionary store key & value about new value of the user
 @param completion return a error if fail else error is nil
 */
+ (void) updateProfileCurrentUserWithUserInfo:(NSDictionary*)userinfo completion:(void(^)(NSError*error)) completion;

/**
 Create profile with information

 Steps:
 
 1. Check the token is valid, if invalid, request access token again and store new access token  at local.
 
 2. Use access token from the step above to create profile.
 
 3. Use access token from the step above to retrieve profile. store profile at local
 
 @param userinfo userinfo a dictionary store key & value about values of the user
 @param completion return a error & User object if fail else error is nil
 */
+ (void) createProfileWithUserInfo:(NSDictionary*)userinfo completion:(void(^)(User* user,NSError*error)) completion;

#pragma mark - Request

/**
 Create request ask concierge

 @param userinfo information request
 @param completion return a error, if fail else error is nil
 */
+ (void) createRequestWithUserInfo:(NSDictionary*) userinfo completion:(void(^)(NSError*error)) completion;

/**
 Update request ask concierge
 
 Steps:
 
 1. Check the token is valid, if invalid, request access token again and store new access token  at local.
 
 2. Use access token from the step above to update request. store profile at local
 
 @param userinfo information request
 @param completion return a error, if fail else error is nil
 */
+ (void) updateRequestWithUserInfo:(NSDictionary*) userinfo completion:(void(^)(NSError*error)) completion;

/**
 Delete request ask concierge
 
 Steps:
 
 1. Check the token is valid, if invalid, request access token again and store new access token  at local.
 
 2. Use access token from the step above to delete request. store profile at local
 
 @param userinfo information request
 @param completion return a error, if fail else error is nil
 */
+ (void) deleteRequestWithUserInfo:(NSDictionary*) userinfo completion:(void(^)(NSError*error)) completion;

#pragma mark - OKTA User

/**
 Change password

 @param newPassword new password
 @param oldPassword old password
 @param completion if error != nil -> fail else success
 */
+ (void) changePasswordWithNewPassword:(NSString*) newPassword andOldPassword:(NSString*)oldPassword completion:(void(^)(NSError*error)) completion;

/**
 Reset password

 @param email email need reset
 @param completion if error != nil -> fail else success
 */
+ (void) resetPasswordForEmail:(NSString*)email completion:(void(^)(NSError*error)) completion;

/**
 Forgot password

 Steps:
 
 1. retreive profile OKTA check valid status, check password constain fisrt name, last name
 
 2. involke request forgot password
 
 @param email email
 @param question AAFRecoveryQuestion object
 @param newPassword AAFPassword object
 @param completion if error != nil -> fail else success
 */
+ (void) forgotPasswordForEmail:(NSString*)email withQuestion:(AAFRecoveryQuestion *) question newPassword:(AAFPassword*) newPassword completion:(void(^)(NSError*error)) completion;

/**
 Verify bin number
 
 Steps:
 
 1. get service token
 
 2. involke request verify bin number
 
 @param bin passcode from user input
 @param completion if error != nil -> fail else success
 */
+ (void) verifyBin:(NSString*)bin completion:(void(^)(BOOL isPass, NSString*  bin,_Nullable id error))completion;

/**
 Get profile OKTA from email

 @param email email from user input
 @param completion return OKTAUserProfile object, User object, error
 */
+ (void) getProfileFromEmail:(NSString*)email completion:(void(^)(OKTAUserProfile* _Nullable  OKTAuser,User* _Nullable  user,NSError*error)) completion;

/**
 Change recovery question

 @param email email fro user input
 @param aafPassword AAFPassword object
 @param question AAFRecoveryQuestion object
 @param completion if error != nil -> fail else success
 */
+ (void) changeRecoveryQuestionForEmail:(NSString*)email password:(AAFPassword*) aafPassword question:(AAFRecoveryQuestion*) question completion:(void(^)(NSError* _Nullable error)) completion;

#pragma mark - PMA
/**
 Get profile party PMA

 Steps:
 
 1. get service token PMA
 
 2. search party id
 
 3. get profile pma by partyid got from step 2
 
 @param email email from user input
 @param completion return PMAUser object, error
 */
+ (void) getProfilePartyPMA:(NSString*) email completion:(void(^)(PMAUser* _Nullable pmaUser, NSError* _Nullable error)) completion;

#pragma mark - Local data

/**
 Get list secure questions

 @return return list questions
 */
+ (nullable NSArray*) getSecurityQuetions;
+ (nullable NSArray*) getSecurityQuetionsTest;

+ (void) enableLogging;
+ (BOOL) isLogging;
@end


#pragma mark - Error
#define ERR_SERVICE_TOKEN   -1020
#define ERR_USER_TOKEN      -1021
#define ERR_NETWORK         -1022
