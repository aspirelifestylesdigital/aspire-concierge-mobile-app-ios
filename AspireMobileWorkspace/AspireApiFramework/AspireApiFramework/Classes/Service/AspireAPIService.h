//
//  AspireAPIService.h
//  AspireAPI
//
//  Created by 💀 on 6/15/18.
//  Copyright © 2018 com.aspirelifestyles.ios.mobileconcierge.UnitTest. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AspireAPIValidate.h"

@interface AspireAPIService : NSObject <NSURLSessionDelegate>

+ (NSString*) getClient;
+ (NSString*) getXAppId;

#pragma mark AspireAPI
+ (void) revokeToken:(NSString*)token;
+ (void) revokeTokenNotCache:(NSString*)token;

+ (void) requestAspireTokenWithUsername:(NSString*)username
                               password:(NSString*)password
                             completion:(void(^)(_Nullable id,_Nullable id))completion;
    
+ (void) retrieveCurrentUserFromAccessToken:(NSString*)token
                                  tokenType:(NSString*)tokenType
                                 completion:(void(^)(_Nullable id,_Nullable id))completion;
    
+ (void) createUserFromAccessToken:(NSString*)token
                         tokenType:(NSString*)tokenType
                      withUserInfo:(NSDictionary*)userInfo
                        completion:(void(^)(_Nullable id))completion;
    
+ (void) updateUserFromAccessToken:(NSString*)token
                         tokenType:(NSString*)tokenType
                      withUserInfo:(NSDictionary*)userInfo
                        completion:(void(^)(_Nullable id))completion;
    
+ (void) createRequestFromAccessToken:(NSString*)token
                            tokenType:(NSString*)tokenType
                         withUserInfo:(NSDictionary*)userInfo
                           completion:(void(^)(_Nullable id))completion;

+ (void) updateRequestFromAccessToken:(NSString*)token
                            tokenType:(NSString*)tokenType
                         withUserInfo:(NSDictionary*)userInfo
                           completion:(void(^)(_Nullable id))completion;

+ (void) deleteRequestFromAccessToken:(NSString*)token
                            tokenType:(NSString*)tokenType
                         withUserInfo:(NSDictionary*)userInfo
                           completion:(void(^)(_Nullable id))completion;
    
+ (void) changePasswordWithToken:(NSString*)token
                       tokenType:(NSString*)tokenType
                           email:(NSString*)email
                        userInfo:(NSDictionary*) userinfo
                      completion:(void(^)(_Nullable id))completion;
    
+ (void) resetPasswordWithToken:(NSString*)token
                      tokenType:(NSString*)tokenType
                          email:(NSString*)email
                     completion:(void(^)(_Nullable id))completion;

+ (void) forgotPasswordWithUrl:(NSString*)url
                        method:(NSString*)mehtod
                         token:(NSString*)token
                      tokenType:(NSString*)tokenType
                          email:(NSString*)email
                          params:(NSDictionary*)params
                     completion:(void(^)(_Nullable id))completion;

+ (void) verifyBinWithToken:(NSString*)token
                       tokenType:(NSString*)tokenType
                           bin:(NSInteger)bin
                      completion:(void(^)(BOOL,_Nullable id))completion;

+ (void) getProfileWithToken:(NSString*)token
                   tokenType:(NSString*)tokenType
                       email:(NSString*)email
                  completion:(void(^)(_Nullable id,_Nullable id))completion;

+ (void) changeRecoveryQuestionWithUrl:(NSString*)url
                                method:(NSString*)method
                                 token:(NSString*)token
                   tokenType:(NSString*)tokenType
                                   withOKTAId:(NSString*)oktaId
                                  params:(NSDictionary*) param
                  completion:(void(^)(_Nullable id,_Nullable id))completion;

#pragma mark - PMA
+ (void) requestPMATokenWithUsername:(NSString*)username
                               password:(NSString*)password
                             completion:(void(^)(_Nullable id,_Nullable id))completion;

+ (void) findPartyPMAWithToken:(NSString*)token
                     tokenType:(NSString*)tokenType
                         email:(NSString*)email
                    completion:(void(^)(_Nullable id,_Nullable id))completion;

+ (void) getPartyPMAWithToken:(NSString*)token
                     tokenType:(NSString*)tokenType
                         partyId:(NSString*)partyId
                    completion:(void(^)(_Nullable id,_Nullable id))completion;

@end
