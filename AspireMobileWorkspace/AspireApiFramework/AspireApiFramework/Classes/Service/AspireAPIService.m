//
//  AspireAPIService.m
//  AspireAPI
//
//  Created by 💀 on 6/15/18.
//  Copyright © 2018 com.aspirelifestyles.ios.mobileconcierge.UnitTest. All rights reserved.
//

#import "AspireAPIService.h"
#import "ModelAspireApiManager.h"
#import "constant.h"
#import "AspireLogging.h"

@implementation AspireAPIService

#define SHOULD_LOG true

#pragma mark - API
+ (void)revokeToken:(NSString *)token {
    NSMutableData *postData = [[NSMutableData alloc] initWithData:[[NSString stringWithFormat:@"token=%@",token] dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&grant_type=password" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", DMA_AUTH_ACC, DMA_AUTH_PAS];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString* endcode = [authData base64EncodedStringWithOptions:0];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", endcode];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/x-www-form-urlencoded"};
    
#if DEBUG
    if (SHOULD_LOG) {
        printf("\n====POST revoke token ====\n%s\n=============\n",[[[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding] UTF8String]);
    }
#endif
    
    [AspireAPIService requestWithMethod:@"POST" urlString:ASPIREAPI_REVOKE_TOKEN_URL data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
#if DEBUG
            NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
        } else {
//            NSError* err;
//            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
//                                                                 options:kNilOptions
//                                                                   error:&err];
#if DEBUG
            if (SHOULD_LOG) {
                printf("\n==== response revoke token ====\n%s\n=============\n",[[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] UTF8String]);
            }
#endif
        }
    }];
}

+ (void)revokeTokenNotCache:(NSString *)token {
    NSMutableData *postData = [[NSMutableData alloc] initWithData:[[NSString stringWithFormat:@"token=%@",token] dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&grant_type=password" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", DMA_AUTH_ACC, DMA_AUTH_PAS];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString* endcode = [authData base64EncodedStringWithOptions:0];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", endcode];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/x-www-form-urlencoded"};
    
#if DEBUG
    if (SHOULD_LOG) {
        printf("\n====POST revoke token ====\n%s\n=============\n",[[[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding] UTF8String]);
    }
#endif
    
    [AspireAPIService requestNotCacheWithMethod:@"POST" urlString:ASPIREAPI_REVOKE_TOKEN_URL data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
#if DEBUG
            NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
        } else {
            //            NSError* err;
            //            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
            //                                                                 options:kNilOptions
            //                                                                   error:&err];
#if DEBUG
            if (SHOULD_LOG) {
                printf("\n==== response revoke token ====\n%s\n=============\n",[[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] UTF8String]);
            }
#endif
        }
    }];
}

+ (void)requestAspireTokenWithUsername:(NSString *)username password:(NSString *)password completion:(void (^)(id, id))completion {
    
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion(nil,[NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    if ([username componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].count == 0 ||
        [password componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].count == 0) {
        if (completion)
        completion(nil,[NSError errorWithDomain:@"com.error" code:-1011 userInfo:@{@"message":@"username or password is invalid"}]);
        return;
    }
    
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:
                              @"password", @"grant_type",
                              @"openid", @"scope",
                              username, @"username",
                              password, @"password",
                              nil];
    NSData *postData = [self encodeDictionary:postDict];
    
//    NSMutableData *postData = [[NSMutableData alloc] initWithData:[@"grant_type=password" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&scope=openid" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[[NSString stringWithFormat:@"&username=%@",username] dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[[NSString stringWithFormat:@"&password=%@",password] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", DMA_AUTH_ACC, DMA_AUTH_PAS];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString* endcode = [authData base64EncodedStringWithOptions:0];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", endcode];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/x-www-form-urlencoded"};
    
#if DEBUG
    if (SHOULD_LOG) {
        NSString *str = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];

        printf("\n====POST request accesstoken ====\n%s\n=============\n",[[[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding] UTF8String]);
        printf("Post data to nsstring: %s", [str UTF8String]);
    }
#endif
    
    [AspireAPIService requestWithMethod:@"POST" urlString:ASPIREAPI_TOKEN_URL data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
#if DEBUG
            NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
            if(completion) {
                completion(nil,error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
#if DEBUG
            if (SHOULD_LOG) {
                printf("\n==== response accesstoken ====\n%s\n=============\n",[[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] UTF8String]);
            }
#endif
            if (err == nil) {
                if(completion) {
                    completion(json,[AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(nil,error);
                }
            }
        }
    }];
}
    
+ (void) retrieveCurrentUserFromAccessToken:(NSString *)token tokenType:(NSString*)tokenType completion:(void (^)(id _Nullable, id _Nullable))completion {
    
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion(nil,[NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json",
                               @"X-App-Id": [AspireAPIService getXAppId],
                               @"X-Organization": [AspireAPIService getClient]
                               };
    [AspireAPIService requestWithMethod:@"GET" urlString:ASPIREAPI_PROFILE_URL data:nil header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            if(completion) {
                completion(nil,error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
            if (err == nil) {
                if(completion) {
                    completion(json,[AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1021 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(nil,error);
                }
            }
        }
    }];
}
    
+ (void)createUserFromAccessToken:(NSString *)token tokenType:(NSString *)tokenType withUserInfo:(NSDictionary *)userInfo completion:(void (^)(id _Nullable))completion {
    
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion([NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:userInfo options:0 error:nil];

    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json",
                               @"X-App-Id": [AspireAPIService getXAppId],
                               @"X-Organization": [AspireAPIService getClient]
                               };
    
    [AspireAPIService requestWithMethod:@"POST" urlString:ASPIREAPI_CREATE_CUSTOMER_URL data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            if(completion) {
                completion(error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
            if (err == nil) {
                if(completion) {
                    completion([AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1021 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(error);
                }
            }
        }
    }];
}
    
+ (void)updateUserFromAccessToken:(NSString *)token tokenType:(NSString *)tokenType withUserInfo:(NSDictionary *)userInfo completion:(void (^)(id _Nullable))completion {
    
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion([NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:userInfo options:0 error:nil];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json",
                               @"X-App-Id": [AspireAPIService getXAppId],
                               @"X-Organization": [AspireAPIService getClient]
                               };
    
    [AspireAPIService requestWithMethod:@"PUT" urlString:ASPIREAPI_PROFILE_URL data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            if(completion) {
                completion(error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
            if (err == nil) {
                if(completion) {
                    completion([AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(error);
                }
            }
        }
    }];
}
    
+ (void)createRequestFromAccessToken:(NSString *)token tokenType:(NSString *)tokenType withUserInfo:(NSDictionary *)userInfo completion:(void (^)(id _Nullable))completion {
    
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion([NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:userInfo options:0 error:nil];

    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json",
                               @"X-App-Id": [AspireAPIService getXAppId],
                               @"X-Organization": [AspireAPIService getClient]
                               };
    
    [AspireAPIService requestWithMethod:@"POST" urlString:ASPIREAPI_REQUEST_URL data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            if(completion) {
                completion(error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
            if (err == nil) {
                if(completion) {
                    completion([AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(error);
                }
            }
        }
    }];
}

+ (void)updateRequestFromAccessToken:(NSString *)token tokenType:(NSString *)tokenType withUserInfo:(NSDictionary *)userInfo completion:(void (^)(id _Nullable))completion {
    
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion([NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:userInfo options:0 error:nil];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json"};
    
    [AspireAPIService requestWithMethod:@"PUT" urlString:ASPIREAPI_REQUEST_URL data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            if(completion) {
                completion(error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
            if (err == nil) {
                if(completion) {
                    completion([AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(error);
                }
            }
        }
    }];
}

+ (void)deleteRequestFromAccessToken:(NSString *)token tokenType:(NSString *)tokenType withUserInfo:(NSDictionary *)userInfo completion:(void (^)(id _Nullable))completion {
    
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion([NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:userInfo options:0 error:nil];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json"};
    
    [AspireAPIService requestWithMethod:@"DELETE" urlString:ASPIREAPI_REQUEST_URL data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            if(completion) {
                completion(error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
            
            if (err == nil) {
                if(completion) {
                    completion([AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(error);
                }
            }
        }
    }];
}

+ (void)changePasswordWithToken:(NSString *)token tokenType:(NSString*)tokenType email:(NSString*)email userInfo:(NSDictionary *)userinfo completion:(void (^)(id _Nullable)) completion {
    
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion([NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:userinfo options:0 error:nil];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json"};
    
    
    [AspireAPIService requestWithMethod:@"POST" urlString:[ASPIREAPI_CHANGE_PASSWORD_URL stringByReplacingOccurrencesOfString:@"[@email]" withString:email] data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            if(completion) {
                completion(error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
            if (err == nil) {
                if(completion) {
                    completion([AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(error);
                }
            }
        }
    }];
}
    
+ (void)resetPasswordWithToken:(NSString *)token tokenType:(NSString *)tokenType email:(NSString *)email completion:(void (^)(id _Nullable))completion {
    
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion([NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json"};
    [AspireAPIService requestWithMethod:@"POST" urlString:[ASPIREAPI_RESET_PASSWORD_URL stringByReplacingOccurrencesOfString:@"[@email]" withString:email] data:nil header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            if(completion) {
                completion(error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
            if (err == nil) {
                if(completion) {
                    completion([AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(error);
                }
            }
        }
    }];
}

+ (void)forgotPasswordWithUrl:(NSString*)url method:(NSString*)method token:(NSString *)token tokenType:(NSString *)tokenType email:(NSString *)email params:(NSDictionary *)params completion:(void (^)(id _Nullable))completion {
    
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion([NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    if(url == nil) {
        url = [ASPIREAPI_FORGOT_PASSWORD_URL stringByReplacingOccurrencesOfString:@"[@email]" withString:email];
    }
    if (method == nil) {
        method = @"POST";
    }
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json"};
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
    
    [AspireAPIService requestWithMethod:method urlString:url data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            if(completion) {
                completion(error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
            if (err == nil) {
                if(completion) {
                    completion([AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(error);
                }
            }
        }
    }];
}

+ (void)verifyBinWithToken:(NSString *)token tokenType:(NSString *)tokenType bin:(NSInteger)bin completion:(void (^)(BOOL, id _Nullable))completion {
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion(false,[NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json",
                               @"X-App-Id": [AspireAPIService getXAppId],
                               @"X-Organization": [AspireAPIService getClient]
                               };
    
    NSDictionary *parameters = @{ @"verificationMetadata": @{ @"bin": @(bin) } };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    [AspireAPIService requestWithMethod:@"POST" urlString:ASPIREAPI_VERIFY_BIN_URL data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            if(completion) {
                completion(false,error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
            if (err == nil) {
                if(completion) {
                    BOOL isSuccess = false;
                    if ([[json allKeys] containsObject:@"result"]) {
                        isSuccess = [[json objectForKey:@"result"] isEqualToString:@"success"];
                    }
                    completion(isSuccess,nil);
                }
            } else {
                if(completion) {
                    completion(false,[AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            }
        }
    }];
}

+ (void) getProfileWithToken:(NSString *)token tokenType:(NSString *)tokenType email:(NSString *)email completion:(void (^)(id _Nullable,id _Nullable))completion {
    
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion(nil,[NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json"};
    [AspireAPIService requestWithMethod:@"GET" urlString:[ASPIREAPI_PROFILE_OKTA_URL stringByReplacingOccurrencesOfString:@"[@email]" withString:email] data:nil header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            if(completion) {
                completion(nil,error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
            if (err == nil) {
                if(completion) {
                    completion(json,[AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(nil,error);
                }
            }
        }
    }];
}

+ (void)changeRecoveryQuestionWithUrl:(NSString*)url method:(NSString*)method token:(NSString *)token tokenType:(NSString *)tokenType withOKTAId:(NSString *)oktaId params:(NSDictionary *)params completion:(void (^)(id _Nullable, id _Nullable))completion {
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion(nil,[NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    if(url == nil) {
        url = [ASPIREAPI_CHANGE_RECOVERY stringByReplacingOccurrencesOfString:@"[@id]" withString:oktaId];
    }
    if (method == nil) {
        method = @"POST";
    }
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json"};
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
    [AspireAPIService requestWithMethod:method urlString:url data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            if(completion) {
                completion(nil,error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
            if (err == nil) {
                if(completion) {
                    completion(json,[AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(nil,error);
                }
            }
        }
    }];
}

+ (NSData*)encodeDictionary:(NSDictionary*)dictionary {
    NSMutableArray *parts = [[NSMutableArray alloc] init];
    NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"];

    for (NSString *key in dictionary) {
        NSString* value = [dictionary objectForKey:key];
        NSString *encodedKey = [key stringByAddingPercentEncodingWithAllowedCharacters:set];
        NSString *encodedValue = [value stringByAddingPercentEncodingWithAllowedCharacters:set];
//        NSString *encodedKey = [key stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
//        NSString *encodedValue = [value stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSString *part = [NSString stringWithFormat: @"%@=%@", encodedKey, encodedValue];
        [parts addObject:part];
    }
    NSString *encodedDictionary = [parts componentsJoinedByString:@"&"];
    return [encodedDictionary dataUsingEncoding:NSUTF8StringEncoding];
}

#pragma mark - PMA
+ (void)requestPMATokenWithUsername:(NSString *)username password:(NSString *)password completion:(void (^)(id _Nullable, id _Nullable))completion {
    
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion(nil,[NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    if ([username componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].count == 0 ||
        [password componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].count == 0) {
        if (completion)
            completion(nil,[NSError errorWithDomain:@"com.error" code:-1011 userInfo:@{@"message":@"username or password is invalid"}]);
        return;
    }
    
    NSDictionary *postDict = [NSDictionary dictionaryWithObjectsAndKeys:
                              @"password", @"grant_type",
                              @"openid", @"scope",
                              username, @"username",
                              password, @"password",
                              nil];
    NSData *postData = [self encodeDictionary:postDict];
    
//    NSMutableData *postData = [[NSMutableData alloc] initWithData:[@"grant_type=password" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&scope=openid" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[[NSString stringWithFormat:@"&username=%@",username] dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[[NSString stringWithFormat:@"&password=%@",password] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", PMA_AUTH_ACC, PMA_AUTH_PAS];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString* endcode = [authData base64EncodedStringWithOptions:0];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", endcode];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/x-www-form-urlencoded"};
    [AspireAPIService requestWithMethod:@"POST" urlString:PMA_GET_TOKEN_URL data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            if(completion) {
                completion(nil,error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
            if (err == nil) {
                if(completion) {
                    completion(json,[AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(nil,error);
                }
            }
        }
    }];
}

+ (void)findPartyPMAWithToken:(NSString *)token tokenType:(NSString *)tokenType email:(NSString *)email completion:(void (^)(id _Nullable, id _Nullable))completion {
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion(nil,[NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSDictionary *headers = @{ @"Authorization": authValue};
    [AspireAPIService requestWithMethod:@"GET" urlString:[[PMA_SEARCH_PARTY_PERSON_URL stringByReplacingOccurrencesOfString:@"[@clientID]" withString:[AspireAPIService getClient]] stringByAppendingFormat:@"emailAddress=%@",email] data:nil header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            if(completion) {
                completion(nil,error);
            }
        } else {
            NSError* err;
            id json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
            
            if (err == nil) {
                if(completion) {
                    completion(json,[AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(nil,error);
                }
            }
        }
    }];
}

+ (void)getPartyPMAWithToken:(NSString *)token tokenType:(NSString *)tokenType partyId:(NSString *)partyId completion:(void (^)(id _Nullable, id _Nullable))completion {
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion(nil,[NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSString* url = [[PMA_GET_PARTY_PERSON stringByReplacingOccurrencesOfString:@"[@partyID]" withString:partyId] stringByReplacingOccurrencesOfString:@"[@clientID]" withString:[AspireAPIService getClient]];
    
    NSDictionary *headers = @{ @"Authorization": authValue};

    [AspireAPIService requestWithMethod:@"GET" urlString:url data:nil header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            if(completion) {
                completion(nil,error);
            }
        } else {
            NSError* err;
            id json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
            
            if (err == nil) {
                if(completion) {
                    completion(json,[AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(nil,error);
                }
            }
        }
    }];
}

#pragma mark - Private
+ (void) requestWithMethod:(NSString*)method urlString:(NSString*) urlString data:(nullable NSData*)data header:(NSDictionary*) headers completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error)) completion {

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    
    [request setHTTPMethod:method];
    [request setAllHTTPHeaderFields:headers];
    
    if (data != nil) {
         [request setHTTPBody:data];
    }
    [AspireAPIService logRequest:request];
    NSURLSession *session = [NSURLSession sharedSession];
#if DEBUG
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AspireAPIService* test = [[AspireAPIService alloc] init];
    session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:test delegateQueue:Nil];
#endif
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (completion)
                                                        completion(data,response,error);
                                                    [AspireAPIService logResponse:response data:data error:error];
                                                }];
    [dataTask resume];
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler {
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]){
        NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
        completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
    }
}

+ (void) logRequest:(NSMutableURLRequest *) request {
    if ([ModelAspireApiManager isLogging]) {
        NSString* paramString = [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding];
        
        NSString* removeSpaceHeader = [NSString stringWithFormat:@"%@", request.allHTTPHeaderFields];
        removeSpaceHeader = [removeSpaceHeader stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSString* fullRequest = request.URL.description;
        NSString* logMessage = [NSString stringWithFormat:@"REQUEST: %@ %@", request.HTTPMethod, fullRequest];
        if (![request.HTTPMethod isEqualToString:@"GET"]) {
            logMessage = [NSString stringWithFormat:@"\nREQUEST: %@ %@ \nparams: %@", request.HTTPMethod, [request.URL absoluteString], paramString];
        }
        logMessage = [NSString stringWithFormat:@"%@\n%@", logMessage, self];
        logMessage = [NSString stringWithFormat:@"%@\n", logMessage];
        
        NSLogAAM(logMessage);
    }
}

+ (void) logResponse: (NSURLResponse *) response data:(NSData *)data error: (NSError *) error {
    if ([ModelAspireApiManager isLogging]) {
        if (data != nil) {
            NSString* responsedString = @"";
            if (data != nil) {
                NSError* parseError;
                NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&parseError];
                NSData *jsonDataResponse = [NSJSONSerialization dataWithJSONObject:jsonData
                                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                                             error:nil];
                NSString* string = [[NSString alloc] initWithData:jsonDataResponse encoding:NSUTF8StringEncoding];
                responsedString = [NSString stringWithFormat:@"RESPONSE: %@ %@ \n", string, self];
            }
            NSLogAAM(@"%@ %@", responsedString, self);
        }
        if (error != nil) {
            NSLogAAM(@"RESPONSE ERROR: %@ %@", error.localizedDescription, self);
        }
    }
}

+ (void) requestNotCacheWithMethod:(NSString*)method urlString:(NSString*) urlString data:(nullable NSData*)data header:(NSDictionary*) headers completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error)) completion {
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    
    [request setHTTPMethod:method];
    [request setAllHTTPHeaderFields:headers];
    
    if (data != nil) {
        [request setHTTPBody:data];
    }
    NSURLSessionConfiguration* sessionConfig = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (completion)
                                                        completion(data,response,error);
                                                    [AspireAPIService logResponse:response data:data error:error];
                                                }];
    [dataTask resume];
}

+ (BOOL) isJsonError:(id)json {
    if ([json isKindOfClass:[NSArray class]]) return false;
    return ([[json allKeys] containsObject:@"error"] ||
            [[json allKeys] containsObject:@"errorCode"]);
}

#define FILE_SERVICE_OKTA  @"data2"
+ (NSString*) getClient {
    
    id dictTemp = [AspireAPIValidate getDataFromFileName:FILE_SERVICE_OKTA];
    if (![dictTemp isKindOfClass:[NSDictionary class]]) {return @"";}
    
    NSDictionary* dict = (NSDictionary*)dictTemp;
    if([[dict allKeys] containsObject:@"client"]) {
        return [dict valueForKey:@"client"];
    }
    
    return @"";
}

+ (NSString*) getXAppId {
    
    id dictTemp = [AspireAPIValidate getDataFromFileName:FILE_SERVICE_OKTA];
    if (![dictTemp isKindOfClass:[NSDictionary class]]) {return @"";}
    
    NSDictionary* dict = (NSDictionary*)dictTemp;
    if([[dict allKeys] containsObject:@"xAppId"]) {
        return [dict valueForKey:@"xAppId"];
    }
    
    return @"";
}
@end
