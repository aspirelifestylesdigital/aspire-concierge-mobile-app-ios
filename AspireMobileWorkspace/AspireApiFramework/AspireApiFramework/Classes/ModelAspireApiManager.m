//
//  ModelAspireApiManager.m
//  MobileConciergeUSDemo
//
//  Created by 💀 on 5/28/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "ModelAspireApiManager.h"
#import "AspireAPIValidate.h"
#import "AspireAPIService.h"
#import "constant.h"
#import "AsyncBlockOperation.h"
#import "AspireLogging.h"
static ModelAspireApiManager *shared = nil;

@interface ModelAspireApiManager() {
    __block NSOperationQueue* queues;
}

@end

@implementation ModelAspireApiManager

#pragma mark - Handle data stored
#define FILE_SERVICE_OKTA  @"data2"
+ (void)registerServiceUsername:(nullable NSString *)username
                       password:(nullable NSString *)password
                        program:(nullable NSString *)program
                            bin:(nullable NSString *)bin {
    
    NSMutableDictionary* dict = [NSMutableDictionary new];
    
    id info = [AspireAPIValidate getDataFromFileName:FILE_SERVICE_OKTA];
    NSDictionary* dictTemp = (NSDictionary*)info;
    if (dictTemp.count >0) {
        [dict addEntriesFromDictionary:dictTemp];
    }
    
    if (username) {
        [dict addEntriesFromDictionary:@{@"username":username}];
    }
    if (password) {
        [dict addEntriesFromDictionary:@{@"password":password}];
    }
    if (program) {
        [dict addEntriesFromDictionary:@{@"program":program}];
    }
    if (bin) {
        [dict addEntriesFromDictionary:@{@"bin":bin}];
    }
    if (dict.count > 0) {
        [AspireAPIValidate storedData:dict toFileName:FILE_SERVICE_OKTA];
    }
}

+ (void)registerClient:(NSString *)client xAppId:(NSString *)xAppId {
    NSMutableDictionary* dict = [NSMutableDictionary new];
    
    id info = [AspireAPIValidate getDataFromFileName:FILE_SERVICE_OKTA];
    NSDictionary* dictTemp = (NSDictionary*)info;
    if (dictTemp.count >0) {
        [dict addEntriesFromDictionary:dictTemp];
    }
    
    if (client) {
        [dict addEntriesFromDictionary:@{@"client":client}];
    }
    if (xAppId) {
        [dict addEntriesFromDictionary:@{@"xAppId":xAppId}];
    }
    
    if (dict.count > 0) {
        [AspireAPIValidate storedData:dict toFileName:FILE_SERVICE_OKTA];
    }
}

+ (void) logout {
    
    [ModelAspireApiManager revokeToken];
    [User reset];
    [AccountAuthentication reset];
    [ASPIREAPI_AccessToken resetUser];
    NSLogAAM(@"%s",__PRETTY_FUNCTION__);
}

#pragma mark - Profiles API
+ (void)revokeToken {
    if ([ASPIREAPI_AccessToken user].accessToken) {
        [AspireAPIService revokeTokenNotCache:[ASPIREAPI_AccessToken user].accessToken];
        [ASPIREAPI_AccessToken removeToken];
    }
}

+ (void)loginWithUsername:(NSString *)username password:(NSString *)password completion:(void (^)(NSError *))completion {
    
    __block NSError* errorTotal = nil;
    
    ModelAspireApiManager* _self = [ModelAspireApiManager shared];
    __block NSOperationQueue* queues = [[NSOperationQueue alloc] init];
    _self->queues = queues;
    
    // init operations
    __block AsyncBlockOperation* checkStatusOKTA =     [AsyncBlockOperation new];
    __block AsyncBlockOperation* getUserAccesstoken =  [AsyncBlockOperation new];
    __block AsyncBlockOperation* retrieveProfile =     [AsyncBlockOperation new];
    
    // add dependency
    [getUserAccesstoken addDependency:checkStatusOKTA];
    [retrieveProfile addDependency:getUserAccesstoken];
    
    typeof(AsyncBlockOperation) __weak *_getUserAccesstoken = getUserAccesstoken;
    typeof(AsyncBlockOperation) __weak *_checkStatusOKTA = checkStatusOKTA;
    typeof(AsyncBlockOperation) __weak *_retrieveProfile = retrieveProfile;
    
    // init block action for Operation
    void(^blockGetAccessToken)(dispatch_block_t completionHandler) = ^(dispatch_block_t completionHandler) {
        typeof(AsyncBlockOperation) *_s = _getUserAccesstoken;
        if (_s.isCancelled) {
            completionHandler();
            return;
        }
        [ModelAspireApiManager getUserAccessTokenFromUsername:[[AspireAPIService getClient] stringByAppendingFormat:@"_%@",username] password:password completion:^(ASPIREAPI_AccessToken *sToken, NSError *error) {
            errorTotal = error;
            if (errorTotal) {
                [queues cancelAllOperations];
            }
            completionHandler();
        }];
    };
    
    void(^blockRetrieveProfile)(dispatch_block_t completionHandler) = ^(dispatch_block_t  _Nonnull completionHandler) {
        typeof(AsyncBlockOperation) *_s = _retrieveProfile;
        if (_s.isCancelled) {
            completionHandler();
            return;
        }
        
        [AspireAPIService retrieveCurrentUserFromAccessToken:[ASPIREAPI_AccessToken user].accessToken  tokenType:[ASPIREAPI_AccessToken user].tokenType completion:^(id _Nullable result, NSError* _Nullable error) {
            errorTotal = error;
            if (result) {
                if ([result isKindOfClass:[NSDictionary class]]) {
                    if (![[result allKeys] containsObject:@"errorCode"])
                        [User fromJSON:[ModelAspireApiManager jsonStringFromDictionary:result WithPrettyPrint:true] encoding:NSUTF8StringEncoding error:&errorTotal];
                    else
                        errorTotal = [NSError errorWithDomain:@"com.s3corp.parseUser" code:-1011 userInfo:@{@"errorCode":@"test"}];
                }
            }
            if (errorTotal) {
                [queues cancelAllOperations];
            }
            completionHandler();
        }];
    };
    
    void(^blockCheckStatusOKTAProfile)(dispatch_block_t completionHandler) = ^(dispatch_block_t  _Nonnull completionHandler) {
        typeof(AsyncBlockOperation) *_s = _checkStatusOKTA;
        if (_s.isCancelled) {
            completionHandler();
            return;
        }
        [ModelAspireApiManager getProfileFromEmail:username
                                        completion:^(OKTAUserProfile * _Nullable OKTAuser, User * _Nullable user, NSError *error) {
                                            errorTotal = error;
                                            if (OKTAuser) {
                                                if ([OKTAuser.status isEqualToString:@"LOCKED_OUT"]) {
                                                    errorTotal = [NSError errorWithDomain:@"com.s3corp.locked" code:-1011 userInfo:@{@"errorCode":@"OKTA_LOCKED_ERR"}];
                                                }
                                            }
                                            if (errorTotal) {
                                                [queues cancelAllOperations];
                                            }
                                            completionHandler();
                                        }];
    };
    
    // set block action for Operation
    [checkStatusOKTA addAsyncBlock:blockCheckStatusOKTAProfile];
    [getUserAccesstoken addAsyncBlock:blockGetAccessToken];
    [retrieveProfile addAsyncBlock:blockRetrieveProfile];
    
    // add operations to queue
    [queues addOperations:@[retrieveProfile,getUserAccesstoken,checkStatusOKTA] waitUntilFinished:false];

    // detect the last run block to return result
    retrieveProfile.completionBlock = ^{
        _self->queues = nil;
        if (errorTotal.code == 53) { // in this case, app in background enter foreground, should request again
            [ModelAspireApiManager loginWithUsername:username password:password completion:completion];
            return;
        }
        if (completion) {
            completion(errorTotal);
        }
    };
}

+ (void) retrieveProfileCurrentUser:(void (^)(User *, NSError *))completion {
    
    if (![User isValid] || ![AccountAuthentication username]) {
        if ([ModelAspireApiManager shared].errorAuthorized) {
            [ModelAspireApiManager shared].errorAuthorized();
            return;
        }
        
        if (completion) {
            completion(nil,[NSError errorWithDomain:@"com.aspirelifestyles.ios.mobileconcierge.retrieveprofile" code:ERR_USER_TOKEN userInfo:@{@"message":@"User not login yet"}]);
        }
        return;
    }
    
    // init operations
    __block AsyncBlockOperation* checkTokenValid =     [AsyncBlockOperation new];
    __block AsyncBlockOperation* retrieveProfile =     [AsyncBlockOperation new];
    
    __block ModelAspireApiManager* _self = [ModelAspireApiManager shared];
    __block NSOperationQueue* queues = [[NSOperationQueue alloc] init];
    _self->queues = queues;
    
    __block User* user = nil;
    __block NSError* errorTotal = nil;
    
    
    typeof(AsyncBlockOperation) __weak *_checkTokenValid = checkTokenValid;
    typeof(AsyncBlockOperation) __weak *_retrieveProfile = retrieveProfile;
    
    // init block action for Operation
    __block void(^blockCheckTokenValid)(dispatch_block_t completionHandler) = ^(dispatch_block_t completionHandler) {
        typeof(AsyncBlockOperation) *_s = _checkTokenValid;
        if (_s.isCancelled) {
            completionHandler();
            return;
        }
        
        if (![ASPIREAPI_AccessToken isValid]) {
            [ModelAspireApiManager getUserAccessTokenFromUsername:[AccountAuthentication username] password:[AccountAuthentication password] completion:^(ASPIREAPI_AccessToken *sToken, NSError *error) {
                errorTotal = error;
                if (errorTotal) {
                    [queues cancelAllOperations];
                }
                completionHandler();
            }];
        } else {
            completionHandler();
        }
    };
    
    __block void(^blockRetrieveProfile)(dispatch_block_t completionHandler) = ^(dispatch_block_t  _Nonnull completionHandler) {
        typeof(AsyncBlockOperation) *_s = _retrieveProfile;
        if (_s.isCancelled) {
            completionHandler();
            return;
        }
        
        if (![ASPIREAPI_AccessToken isValid]) {
            completionHandler();
            return;
        }
        
        [AspireAPIService retrieveCurrentUserFromAccessToken:[ASPIREAPI_AccessToken user].accessToken  tokenType:[ASPIREAPI_AccessToken user].tokenType completion:^(id _Nullable result, NSError* _Nullable error) {
            errorTotal = error;
            if (result) {
                if ([result isKindOfClass:[NSDictionary class]]) {
                    if (![[result allKeys] containsObject:@"errorCode"])
                        user = [User fromJSON:[ModelAspireApiManager jsonStringFromDictionary:result WithPrettyPrint:true] encoding:NSUTF8StringEncoding error:&errorTotal];
                    else
                        errorTotal = [NSError errorWithDomain:@"com.s3corp.parseUser" code:-1011 userInfo:@{@"errorCode":@"test"}];
                }
            }
            if (errorTotal) {
                [queues cancelAllOperations];
            }
            completionHandler();
        }];
    };
    
    __block void(^action)(void) = ^{
        // add dependency
        [retrieveProfile addDependency:checkTokenValid];
        
        [checkTokenValid addAsyncBlock:blockCheckTokenValid];
        [retrieveProfile addAsyncBlock:blockRetrieveProfile];
        
        // add operations to queue
        [queues addOperations:@[retrieveProfile,checkTokenValid] waitUntilFinished:false];
    };
    
    action();
    
    // detect the last run block to return result
    retrieveProfile.completionBlock = ^{
        [_self->queues cancelAllOperations];
        _self->queues = nil;
        queues = _self->queues;
        if ((errorTotal.code == 53 || ![ASPIREAPI_AccessToken isValid]) && ![AspireApiError isNotAuthorizedFromError:errorTotal]) { // in this case, app in background enter foreground, should request again
            action();
            return;
        } else
        if (errorTotal != nil) {
            if ([[errorTotal.userInfo allKeys] containsObject:@"errorCode"]) {
                if ([[errorTotal.userInfo objectForKey:@"errorCode"] isEqualToString:@"ACE_UNAUTHORISED_001"]) {
                    [ASPIREAPI_AccessToken removeToken];
                    NSLogAAM(@"%s: get user access token again",__PRETTY_FUNCTION__);
                    [ModelAspireApiManager retrieveProfileCurrentUser:completion];
                    return;
                }
            } else if ([AspireApiError isNotAuthorizedFromError:errorTotal] && [User isValid]) {
                if ([ModelAspireApiManager shared].errorAuthorized) {
                    [ModelAspireApiManager shared].errorAuthorized();
                    return;
                }
            }
        }
        if (completion) {
#if DEBUG
            NSLogAAM(@"%s => %@ - test response",__PRETTY_FUNCTION__,errorTotal);
#endif
            completion(user,errorTotal);
        }
        
    };
}

+ (void)updateProfileCurrentUserWithUserInfo:(NSDictionary *)userinfo completion:(void (^)(NSError *))completion {
    
    if (![User isValid]|| ![AccountAuthentication username]) {
        if ([ModelAspireApiManager shared].errorAuthorized) {
            [ModelAspireApiManager shared].errorAuthorized();
            return;
        }
        if (completion) {
            completion([NSError errorWithDomain:@"com.aspirelifestyles.ios.mobileconcierge.retrieveprofile" code:ERR_USER_TOKEN userInfo:@{@"message":@"Invalid Params input or User not login yet"}]);
        }
        return;
    }
    
    NSMutableDictionary* root = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                @"gender": @"Unknown",
                                                                                @"maritalStatus": @"Unknown",
                                                                                @"homeCountry": @"USA",
                                                                                @"vip": @"Y"}];
    if ([User current].partyId.length > 0) {
        [root addEntriesFromDictionary:@{@"partyId":[User current].partyId}];
    }
    NSMutableDictionary* membership = [NSMutableDictionary new];
    NSMutableDictionary* location = [NSMutableDictionary dictionaryWithDictionary:@{@"locationType":@"Original",
                                                                                    @"address":@{@"addressLine1": @"Unknown"}
                                                                                    }];
    if ([[User current] getLocationIdFromType:@"Original"].length > 0) {
        [location setObject:[[User current] getLocationIdFromType:@"Original"] forKey:@"locationId"];
    }
    NSMutableArray* preferences = [NSMutableArray new];
    NSMutableArray* userAppPreferences = [NSMutableArray new];
    
    if ([[User current] getAppUserPreferenceIdFromKey:APP_USER_PREFERENCE_DeviceOS].length > 0) {
        [userAppPreferences addObject:@{@"preferenceKey":APP_USER_PREFERENCE_DeviceOS,
                                        @"preferenceValue": @"iOS",
                                        @"appUserPreferenceId":[[User current] getAppUserPreferenceIdFromKey:APP_USER_PREFERENCE_DeviceOS]
                                        }];
    } else {
        [userAppPreferences addObject:@{@"preferenceKey":APP_USER_PREFERENCE_DeviceOS,
                                        @"preferenceValue": @"iOS"
                                        }];
    }
    
    NSMutableArray* emails = [NSMutableArray new];
    NSMutableArray* phones = [NSMutableArray new];
    
//    if ([[userinfo allKeys] containsObject:@"FirstName"] && [[userinfo allKeys] containsObject:@"LastName"]) {
//        [root setObject:[NSString stringWithFormat:@"%@ %@",[userinfo objectForKey:@"FirstName"],[userinfo objectForKey:@"LastName"]] forKey:@"fullName"];
//        [root setObject:[NSString stringWithFormat:@"%@ %@",[userinfo objectForKey:@"FirstName"],[userinfo objectForKey:@"LastName"]] forKey:@"displayName"];
//        [root setObject:[NSString stringWithFormat:@"%@ %@",[userinfo objectForKey:@"FirstName"],[userinfo objectForKey:@"LastName"]] forKey:@"aliasName"];
//    }
    
    [[userinfo allKeys] enumerateObjectsUsingBlock:^(NSString*  _Nonnull key, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([key isEqualToString:@"FirstName"]) {
            [root setObject:[userinfo objectForKey:key] forKey:@"firstName"];
        } else
            if ([key isEqualToString:@"homeCountry"]) {
                [root setObject:[userinfo objectForKey:key] forKey:@"homeCountry"];
            } else
        if ([key isEqualToString:@"LastName"]) {
            [root setObject:[userinfo objectForKey:key] forKey:@"lastName"];
        } else
        if ([key isEqualToString:@"Car Type Preference"] ||
            [key isEqualToString:@"Dining Preference"] ||
            [key isEqualToString:@"Hotel Preference"] ||
            [key isEqualToString:@"Other Preferences"]) {
            if ([[User current] getPreferencesIdFromType:key].length > 0) {
                [preferences addObject:@{@"preferenceType":key,
                                         @"preferenceValue":[userinfo objectForKey:key],
                                         @"preferenceId":[[User current] getPreferencesIdFromType:key]
                                         }];
            } else {
                [preferences addObject:@{@"preferenceType":key,
                                         @"preferenceValue":[userinfo objectForKey:key]
                                         }];
            }
        } else
        if ([key isEqualToString:@"City"] || [key isEqualToString:@"Country"]) {
            NSMutableDictionary* address = [NSMutableDictionary dictionaryWithDictionary:[location objectForKey:@"address"]];
            [address setObject:[userinfo objectForKey:key] forKey:[key lowercaseString]];
            [location setObject:address forKey:@"address"];
        } else
        
        if ([key isEqualToString:@"Email"]) {
            if ([[User current] getEmailIdFromType:@"Primary"].length > 0) {
                [emails addObject:@{@"emailType":@"Primary",
                                    @"emailAddress": [userinfo objectForKey:key],
                                    @"emailId":[[User current] getEmailIdFromType:@"Primary"]}];
            } else {
                [emails addObject:@{@"emailType":@"Primary",
                                    @"emailAddress": [userinfo objectForKey:key]}];
            }
            
        } else
        if ([key isEqualToString:@"MobileNumber"]) {
            if ([[User current] getPhoneIdFromType:@"Mobile"].length > 0) {
                [phones addObject:@{@"phoneType":@"Mobile",
                                    @"phoneNumber": [userinfo objectForKey:key],
                                    @"phoneId":[[User current] getPhoneIdFromType:@"Mobile"]
                                    }];
            } else {
                [phones addObject:@{@"phoneType":@"Mobile",
                                    @"phoneNumber": [userinfo objectForKey:key]}];
            }
        } else
        if ([key isEqualToString:@"Salutation"]) {
            [root setObject:[[userinfo objectForKey:key] convertSalutationToAspireAPI] forKey:@"salutation"];
        } else
            if ([key isEqualToString:@"ZipCode"]) {
                NSMutableDictionary* address = [NSMutableDictionary dictionaryWithDictionary:[location objectForKey:@"address"]];
                [address setObject:[userinfo objectForKey:key] forKey:@"zipCode"];
                [location setObject:address forKey:@"address"];
            } else
        if ([key isEqualToString:@"addressLine5"]) {
            NSMutableDictionary* address = [NSMutableDictionary dictionaryWithDictionary:[location objectForKey:@"address"]];
            [address setObject:[userinfo objectForKey:key] forKey:key];
            [location setObject:address forKey:@"address"];
        } else
        if ([key isEqualToString:@"referenceName"]) {
            [membership addEntriesFromDictionary:@{
                @"membershipCategory": @"DMA",
                @"referenceName": [userinfo valueForKey:key],
                @"referenceNumber": @"",
                @"externalReferenceNumber": @"",
                @"uniqueIdentifier": @""}];
        } else
            if ([LIST_APP_USER_REFERENCES containsObject:key]) {
                if ([[User current] getAppUserPreferenceIdFromKey:key].length > 0) {
                    [userAppPreferences addObject:@{@"preferenceKey":key,
                                                    @"preferenceValue": [userinfo objectForKey:key],
                                                    @"appUserPreferenceId":[[User current] getAppUserPreferenceIdFromKey:key]
                                        }];
                } else {
                    [userAppPreferences addObject:@{@"preferenceKey":key,
                                                    @"preferenceValue": [userinfo objectForKey:key]
                                        }];
                }
            }
    }];
    
    [root setObject:preferences forKey:@"preferences"];
    [root setObject:@[location] forKey:@"locations"];
    
    // avoid update failed for old account has memberships
    if ([User current].memberships.count > 0) {
        UserMembership* a = [User current].memberships.firstObject;
        NSMutableDictionary* temp = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                    @"membershipCategory": a.membershipCategory,
                                                                                    @"referenceName": a.referenceName,
                                                                                    @"referenceNumber": a.referenceNumber,
                                                                                    @"externalReferenceNumber": a.externalReferenceNumber,
                                                                                    @"uniqueIdentifier": a.uniqueIdentifier}];
        if (a.membershipId.length > 0) {
            [temp setObject:a.membershipId forKey:@"membershipId"];
        }
        [root setObject:@[temp] forKey:@"memberships"];
    }
    
//    [root setObject:@[membership] forKey:@"memberships"];
    [root setObject:emails forKey:@"emails"];
    [root setObject:phones forKey:@"phones"];
    if (userAppPreferences.count > 0) {
        [root setObject:userAppPreferences forKey:@"appUserPreferences"];
    }
    [root setObject:[ModelAspireApiManager getVerificationmetaData]  forKey:@"verificationMetadata"];
    
    __block NSError* errorTotal = nil;
    
    dispatch_queue_t queue = dispatch_queue_create("com.aspirelifestyles.ios.mobileconcierge.retrieveprofile", DISPATCH_QUEUE_SERIAL);
    
    // check token valid
    if (![ASPIREAPI_AccessToken isValid]) {
        dispatch_async(queue, ^{
            NSLogAAM(@"%s: get user access token",__PRETTY_FUNCTION__);
            dispatch_suspend(queue);
            [ModelAspireApiManager getUserAccessTokenFromUsername:[AccountAuthentication username] password:[AccountAuthentication password] completion:^(ASPIREAPI_AccessToken *sToken, NSError *error) {
                errorTotal = error;
                dispatch_resume(queue);
            }];
        });
    }
    
    // update profile
    dispatch_async(queue, ^{
        if (errorTotal == nil) {
            NSLogAAM(@"%s: retrieve profile",__PRETTY_FUNCTION__);
            dispatch_suspend(queue);
            [AspireAPIService updateUserFromAccessToken:[ASPIREAPI_AccessToken user].accessToken  tokenType:[ASPIREAPI_AccessToken user].tokenType withUserInfo:root completion:^(NSError* _Nullable error) {
                errorTotal = error;
                dispatch_resume(queue);
            }];
        }
    });
    
    // retrieve profile
    dispatch_async(queue, ^{
        if (errorTotal == nil) {
            NSLogAAM(@"%s: retrieve profile",__PRETTY_FUNCTION__);
            dispatch_suspend(queue);
            [AspireAPIService retrieveCurrentUserFromAccessToken:[ASPIREAPI_AccessToken user].accessToken  tokenType:[ASPIREAPI_AccessToken user].tokenType completion:^(id _Nullable result, NSError* _Nullable error) {
                errorTotal = error;
                if (result) {
                    if ([result isKindOfClass:[NSDictionary class]]) {
                        if (![[result allKeys] containsObject:@"errorCode"])
                            [User fromJSON:[ModelAspireApiManager jsonStringFromDictionary:result WithPrettyPrint:true] encoding:NSUTF8StringEncoding error:&errorTotal];
                    }
                }
                dispatch_resume(queue);
            }];
        }
    });
    
    dispatch_barrier_async(queue, ^{
        NSLogAAM(@"%s: return with error: %@",__PRETTY_FUNCTION__,errorTotal.description);
        if (errorTotal != nil) {
            if ([[errorTotal.userInfo allKeys] containsObject:@"errorCode"]) {
                if ([[errorTotal.userInfo objectForKey:@"errorCode"] isEqualToString:@"ACE_UNAUTHORISED_001"]) {
                    [ASPIREAPI_AccessToken removeToken];
                    NSLogAAM(@"%s: get user access token again",__PRETTY_FUNCTION__);
                    [ModelAspireApiManager updateProfileCurrentUserWithUserInfo:userinfo completion:completion];
                    return;
                }
            } else if ([AspireApiError isNotAuthorizedFromError:errorTotal] && [User isValid]) {
                if ([ModelAspireApiManager shared].errorAuthorized) {
                    [ModelAspireApiManager shared].errorAuthorized();
                    return;
                }
            }
        }
        if (completion) {
            completion(errorTotal);
        }
    });
}

+ (void)createProfileWithUserInfo:(NSDictionary *)userinfo completion:(void (^)(User*user,NSError *error))completion {
    if (userinfo.count == 0) {
        if (completion) {
            completion(nil,[NSError errorWithDomain:@"com.aspirelifestyles.ios.mobileconcierge.retrieveprofile" code:-1011 userInfo:@{@"message":@"Invalid Params input"}]);
        }
        return;
    }
    
    NSMutableDictionary* root = [NSMutableDictionary new];
    NSMutableDictionary* profile = [[NSMutableDictionary alloc] initWithDictionary:@{
                                                                                     @"gender": @"Unknown",
                                                                                     /*@"maritalStatus": @"Unknown",*/
                                                                                     @"homeCountry": @"USA",
                                                                                     @"vip": @"Y"}];
    NSMutableDictionary* account = [NSMutableDictionary dictionaryWithDictionary:@{@"activate":[NSNumber numberWithBool:true]}];
    NSMutableDictionary* membership = [NSMutableDictionary new];
    NSMutableDictionary* location = [NSMutableDictionary dictionaryWithDictionary:@{@"locationType":@"Original",
                                                                                    @"address":@{@"addressLine1": @"Unknown"}
                                                                                    }];
    NSMutableArray* preferences = [NSMutableArray new];
    NSMutableArray* userAppPreferences = [NSMutableArray new];
    
    [userAppPreferences addObject:@{@"preferenceKey":APP_USER_PREFERENCE_DeviceOS,
                                        @"preferenceValue": @"iOS"
                                        }];
    
    NSMutableArray* emails = [NSMutableArray new];
    NSMutableArray* phones = [NSMutableArray new];
    
//    if ([[userinfo allKeys] containsObject:@"FirstName"] && [[userinfo allKeys] containsObject:@"LastName"]) {
//        [profile setObject:[NSString stringWithFormat:@"%@ %@",[userinfo objectForKey:@"FirstName"],[userinfo objectForKey:@"LastName"]] forKey:@"fullName"];
//        [profile setObject:[NSString stringWithFormat:@"%@ %@",[userinfo objectForKey:@"FirstName"],[userinfo objectForKey:@"LastName"]] forKey:@"displayName"];
//        [profile setObject:[NSString stringWithFormat:@"%@ %@",[userinfo objectForKey:@"FirstName"],[userinfo objectForKey:@"LastName"]] forKey:@"aliasName"];
//    }
    
    [[userinfo allKeys] enumerateObjectsUsingBlock:^(NSString*  _Nonnull key, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([key isEqualToString:@"FirstName"]) {
            [profile setObject:[userinfo objectForKey:key] forKey:@"firstName"];
        } else
            if ([key isEqualToString:@"homeCountry"]) {
                [profile setObject:[userinfo objectForKey:key] forKey:@"homeCountry"];
            } else
        if ([key isEqualToString:@"LastName"]) {
            [profile setObject:[userinfo objectForKey:key] forKey:@"lastName"];
        } else
        if ([key isEqualToString:@"Car Type Preference"] ||
            [key isEqualToString:@"Dining Preference"] ||
            [key isEqualToString:@"Hotel Preference"] ||
            [key isEqualToString:@"Other Preferences"]) {
            
            [preferences addObject:@{@"preferenceType":key,
                                     @"preferenceValue":[userinfo objectForKey:key]
                                     }];
        } else
        if ([key isEqualToString:@"City"] || [key isEqualToString:@"Country"]) {
            NSMutableDictionary* address = [NSMutableDictionary dictionaryWithDictionary:[location objectForKey:@"address"]];
            [address setObject:[userinfo objectForKey:key] forKey:[key lowercaseString]];
            [location setObject:address forKey:@"address"];
        } else
        
        if ([key isEqualToString:@"Email"]) {
            [emails addObject:@{@"emailType":@"Primary",
                                @"emailAddress": [userinfo objectForKey:key]}];
            [account setObject:[[AspireAPIService getClient] stringByAppendingFormat:@"_%@",[userinfo objectForKey:key]] forKey:@"username"];
            [account setObject:[[AspireAPIService getClient] stringByAppendingFormat:@"_%@",[userinfo objectForKey:key]] forKey:@"email"];
        } else
        if ([key isEqualToString:@"MobileNumber"]) {
            [phones addObject:@{@"phoneType":@"Mobile",
                                @"phoneNumber": [userinfo objectForKey:key]}];
        } else
        if ([key isEqualToString:@"Password"]) {
            [account setObject:[userinfo objectForKey:key] forKey:@"password"];
        } else
        if ([key isEqualToString:@"Salutation"]) {
            [profile setObject:[[userinfo objectForKey:key] convertSalutationToAspireAPI] forKey:@"salutation"];
        } else
            if ([key isEqualToString:@"ZipCode"]) {
                NSMutableDictionary* address = [NSMutableDictionary dictionaryWithDictionary:[location objectForKey:@"address"]];
                [address setObject:[userinfo objectForKey:key] forKey:@"zipCode"];
                [location setObject:address forKey:@"address"];
            } else
        if ([key isEqualToString:@"addressLine5"]) {
            NSMutableDictionary* address = [NSMutableDictionary dictionaryWithDictionary:[location objectForKey:@"address"]];
            [address setObject:[userinfo objectForKey:key] forKey:key];
            [location setObject:address forKey:@"address"];
        } else
        if ([key isEqualToString:@"referenceName"]) {
            [membership addEntriesFromDictionary:@{
                                                   @"membershipCategory": @"DMA",
                                                   @"referenceName": [userinfo valueForKey:key],
                                                   @"referenceNumber": @"",
                                                   @"externalReferenceNumber": @"",
                                                   @"uniqueIdentifier": @""}];
        } else
            if ([LIST_APP_USER_REFERENCES containsObject:key]) {
                [userAppPreferences addObject:@{@"preferenceKey":key,
                                                @"preferenceValue": [userinfo objectForKey:key]
                                                }];
        } else
                if ([key isEqualToString:@"partyId"]) {
                    [root setObject:[userinfo objectForKey:key] forKey:@"partyId"];
                }
    }];
    
    [profile setObject:preferences forKey:@"preferences"];
    [profile setObject:@[location] forKey:@"locations"];
//    [profile setObject:@[membership] forKey:@"memberships"];
    [profile setObject:emails forKey:@"emails"];
    [profile setObject:phones forKey:@"phones"];
    
    if (userAppPreferences.count > 0) {
        [profile setObject:userAppPreferences forKey:@"appUserPreferences"];
    }
    
    [root setObject:[ModelAspireApiManager getVerificationmetaData]  forKey:@"verificationMetadata"];
    [root setObject:account forKey:@"account"];
    [root setObject:profile forKey:@"profile"];
    
    __block NSError* errorTotal = nil;
    __block User* user = nil;
    
    dispatch_queue_t queue = dispatch_queue_create("com.aspirelifestyles.ios.mobileconcierge.retrieveprofile", DISPATCH_QUEUE_SERIAL);
    
    // get service token
    dispatch_async(queue, ^{
        NSLogAAM(@"%s: get service access token",__PRETTY_FUNCTION__);
        dispatch_suspend(queue);
        [ModelAspireApiManager getServiceAccessTokenFromUsername:[ModelAspireApiManager getUsernameService] password:[ModelAspireApiManager getPasswordService] completion:^(ASPIREAPI_AccessToken *sToken, NSError *error) {
            errorTotal = error;
            dispatch_resume(queue);
        }];
    });
    
    
    // create profile
    dispatch_async(queue, ^{
        if (errorTotal == nil) {
            NSLogAAM(@"%s: create profile",__PRETTY_FUNCTION__);
            dispatch_suspend(queue);
            [AspireAPIService createUserFromAccessToken:[ASPIREAPI_AccessToken service].accessToken tokenType:[ASPIREAPI_AccessToken service].tokenType withUserInfo:root completion:^(NSError* _Nullable error) {
                errorTotal = error;
                dispatch_resume(queue);
            }];
        }
    });
    
    // get UserAccessToken for user
    dispatch_async(queue, ^{
        if (errorTotal == nil) {
            [AccountAuthentication storedWithDictionary:account];
            NSLogAAM(@"%s: get user access token",__PRETTY_FUNCTION__);
            dispatch_suspend(queue);
            [ModelAspireApiManager getUserAccessTokenFromUsername:[account valueForKey:@"username"] password:[account valueForKey:@"password"] completion:^(ASPIREAPI_AccessToken *sToken, NSError *error) {
                errorTotal = error;
                dispatch_resume(queue);
            }];
        }
    });
    
    // retrieve profile
    dispatch_async(queue, ^{
        if (errorTotal == nil) {
            NSLogAAM(@"%s: retrieve profile",__PRETTY_FUNCTION__);
            dispatch_suspend(queue);
            [AspireAPIService retrieveCurrentUserFromAccessToken:[ASPIREAPI_AccessToken user].accessToken  tokenType:[ASPIREAPI_AccessToken user].tokenType completion:^(id _Nullable result, NSError* _Nullable error) {
                errorTotal = error;
                if (result) {
                    if ([result isKindOfClass:[NSDictionary class]]) {
                        if (![[result allKeys] containsObject:@"errorCode"])
                            user = [User fromJSON:[ModelAspireApiManager jsonStringFromDictionary:result WithPrettyPrint:true] encoding:NSUTF8StringEncoding error:&errorTotal];
                    }
                }
                dispatch_resume(queue);
            }];
        }
    });
    
    // done all queues
    dispatch_barrier_async(queue, ^{
        if (user != nil)
            NSLogAAM(@"%s: Create user jobs success",__PRETTY_FUNCTION__);
        else
            NSLogAAM(@"%s: Create user jobs fail",__PRETTY_FUNCTION__);
        if (completion) {
            completion(user,errorTotal);
        }
    });
}

#pragma mark - Request
+ (void)createRequestWithUserInfo:(NSDictionary *)userinfo completion:(void (^)(NSError *))completion {
    
    if (![User isValid] || ![AccountAuthentication username]) {
        if ([ModelAspireApiManager shared].errorAuthorized) {
            [ModelAspireApiManager shared].errorAuthorized();
            return;
        }
        if (completion) {
            completion([NSError errorWithDomain:@"com.aspirelifestyles.ios.mobileconcierge.retrieveprofile" code:ERR_USER_TOKEN userInfo:@{@"message":@"Invalid Params input or User not login yet"}]);
        }
        return;
    }
    
    NSMutableDictionary* root = [NSMutableDictionary dictionaryWithDictionary:@{@"program":[ModelAspireApiManager getProgram],
                                                                                @"preferredResponse":@"Email"/*,
                                                                                @"currentLocation":@"Unknown",
                                                                                @"currentPhone":@"",
                                                                                @"contactSource":@""*/}];
    
    NSMutableDictionary* profile = [NSMutableDictionary new];
    
    [profile addEntriesFromDictionary:@{@"emailAddress1":[User current].emails.firstObject.emailAddress,
                                        @"firstName":[User current].firstName,
                                        @"lastName":[User current].lastName,
                                        @"phoneNumber":[User current].phones.firstObject.phoneNumber
                                        }];
    if ([User current].salutation.length > 0) {
        [profile addEntriesFromDictionary:@{@"salutation":[[User current].salutation convertSalutationToAspireAPI]}];
    }
    NSMutableDictionary* request = [NSMutableDictionary new];//[NSMutableDictionary dictionaryWithDictionary:@{@"requestType":@"V AIRPORT TRANSFER",@"airportName":@"SIN",@"airline":@"SQ"}];

    [[userinfo allKeys] enumerateObjectsUsingBlock:^(NSString*  _Nonnull key, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([key isEqualToString:@"emailAddress1"]) {
            [profile setObject:[userinfo objectForKey:key] forKey:@"emailAddress1"];
        } else if ([key isEqualToString:@"salutation"]) {
            [profile setObject:[[userinfo objectForKey:key] convertSalutationToAspireAPI] forKey:@"salutation"];
        } else if ([key isEqualToString:@"firstName"]) {
            [profile setObject:[userinfo objectForKey:key] forKey:@"firstName"];
        } else if ([key isEqualToString:@"lastName"]) {
                [profile setObject:[userinfo objectForKey:key] forKey:@"lastName"];
        } else if ([key isEqualToString:@"phoneNumber"]) {
            [profile setObject:[userinfo objectForKey:key] forKey:@"phoneNumber"];
        } else if ([key isEqualToString:@"requestDetails"]) {
            [request setObject:[userinfo objectForKey:key] forKey:@"requestDetails"];
        } else if ([key isEqualToString:@"requestType"]) {
            [request setObject:[userinfo objectForKey:key] forKey:@"requestType"];
        } else if ([key isEqualToString:@"requestCity"]) {
            [request setObject:[userinfo objectForKey:key] forKey:@"requestCity"];
        } else if ([key isEqualToString:@"preferredResponse"]) {
            [root setValue:[userinfo objectForKey:key] forKey:@"preferredResponse"];
        }
    }];
    
    [root setObject:[ModelAspireApiManager getVerificationmetaData]  forKey:@"verificationMetadata"];
    [root setObject:profile  forKey:@"profile"];
    [root setObject:request  forKey:@"request"];
    
    __block NSError* errorTotal = nil;
    
    dispatch_queue_t queue = dispatch_queue_create("com.aspirelifestyles.ios.mobileconcierge.retrieveprofile", DISPATCH_QUEUE_SERIAL);
    
    // check token valid
    if (![ASPIREAPI_AccessToken isValid]) {
        dispatch_async(queue, ^{
            NSLogAAM(@"%s: get user access token",__PRETTY_FUNCTION__);
            dispatch_suspend(queue);
            [ModelAspireApiManager getUserAccessTokenFromUsername:[AccountAuthentication username] password:[AccountAuthentication password] completion:^(ASPIREAPI_AccessToken *sToken, NSError *error) {
                errorTotal = error;
                dispatch_resume(queue);
            }];
        });
    }
    
    // create request
    dispatch_async(queue, ^{
        if (errorTotal == nil) {
            NSLogAAM(@"%s: create request",__PRETTY_FUNCTION__);
            dispatch_suspend(queue);
            [AspireAPIService createRequestFromAccessToken:[ASPIREAPI_AccessToken user].accessToken  tokenType:[ASPIREAPI_AccessToken user].tokenType withUserInfo:root completion:^(NSError* _Nullable error) {
                errorTotal = error;
                dispatch_resume(queue);
            }];
        }
    });
    
    dispatch_barrier_async(queue, ^{
        NSLogAAM(@"%s: return with error: %@",__PRETTY_FUNCTION__,errorTotal.description);
        if (errorTotal != nil) {
            if ([[errorTotal.userInfo allKeys] containsObject:@"errorCode"]) {
                if ([[errorTotal.userInfo objectForKey:@"errorCode"] isEqualToString:@"ACE_UNAUTHORISED_001"]) {
                    [ASPIREAPI_AccessToken removeToken];
                    NSLogAAM(@"%s: get user access token again",__PRETTY_FUNCTION__);
                    [ModelAspireApiManager createRequestWithUserInfo:userinfo completion:completion];
                    return;
                }
            } else if ([AspireApiError isNotAuthorizedFromError:errorTotal]&& [User isValid]) {
                if ([ModelAspireApiManager shared].errorAuthorized) {
                    [ModelAspireApiManager shared].errorAuthorized();
                    return;
                }
            }
        }
        if (completion) {
            completion(errorTotal);
        }
    });
}

+ (void)updateRequestWithUserInfo:(NSDictionary *)userinfo completion:(void (^)(NSError *))completion {
    
    if (![User isValid] || ![AccountAuthentication username]) {
        if ([ModelAspireApiManager shared].errorAuthorized) {
            [ModelAspireApiManager shared].errorAuthorized();
            return;
        }
        if (completion) {
            completion([NSError errorWithDomain:@"com.aspirelifestyles.ios.mobileconcierge.retrieveprofile" code:ERR_USER_TOKEN userInfo:@{@"message":@"Invalid Params input or User not login yet"}]);
        }
        return;
    }
    
    NSMutableDictionary* root = [NSMutableDictionary dictionaryWithDictionary:@{@"program":[ModelAspireApiManager getProgram],
                                                                                @"preferredResponse":@"Email"/*,
                                                                                                              @"currentLocation":@"Unknown",
                                                                                                              @"currentPhone":@"",
                                                                                                              @"contactSource":@""*/}];
    
    NSMutableDictionary* profile = [NSMutableDictionary new];
    
    [profile addEntriesFromDictionary:@{@"emailAddress1":[User current].emails.firstObject.emailAddress,
                                        @"firstName":[User current].firstName,
                                        @"lastName":[User current].lastName,
                                        @"phoneNumber":[User current].phones.firstObject.phoneNumber
                                        }];
    if ([User current].salutation.length > 0) {
        [profile addEntriesFromDictionary:@{@"salutation":[[User current].salutation convertSalutationToAspireAPI]}];
    }
    NSMutableDictionary* request = [NSMutableDictionary new];//[NSMutableDictionary dictionaryWithDictionary:@{@"requestType":@"V AIRPORT TRANSFER",@"airportName":@"SIN",@"airline":@"SQ"}];
    
    [[userinfo allKeys] enumerateObjectsUsingBlock:^(NSString*  _Nonnull key, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([key isEqualToString:@"emailAddress1"]) {
            [profile setObject:[userinfo objectForKey:key] forKey:@"emailAddress1"];
        } else if ([key isEqualToString:@"salutation"]) {
            [profile setObject:[[userinfo objectForKey:key] convertSalutationToAspireAPI] forKey:@"salutation"];
        } else if ([key isEqualToString:@"firstName"]) {
            [profile setObject:[userinfo objectForKey:key] forKey:@"firstName"];
        } else if ([key isEqualToString:@"lastName"]) {
            [profile setObject:[userinfo objectForKey:key] forKey:@"lastName"];
        } else if ([key isEqualToString:@"phoneNumber"]) {
            [profile setObject:[userinfo objectForKey:key] forKey:@"phoneNumber"];
        } else if ([key isEqualToString:@"requestDetails"]) {
            [request setObject:[userinfo objectForKey:key] forKey:@"requestDetails"];
        } else if ([key isEqualToString:@"requestType"]) {
            [request setObject:[userinfo objectForKey:key] forKey:@"requestType"];
        } else if ([key isEqualToString:@"requestCity"]) {
            [request setObject:[userinfo objectForKey:key] forKey:@"requestCity"];
        } else if ([key isEqualToString:@"preferredResponse"]) {
            [root setValue:[userinfo objectForKey:key] forKey:@"preferredResponse"];
        }
    }];
    
    [root setObject:[ModelAspireApiManager getVerificationmetaData]  forKey:@"verificationMetadata"];
    [root setObject:profile  forKey:@"profile"];
    [root setObject:request  forKey:@"request"];
    
    __block NSError* errorTotal = nil;
    
    dispatch_queue_t queue = dispatch_queue_create("com.aspirelifestyles.ios.mobileconcierge.retrieveprofile", DISPATCH_QUEUE_SERIAL);
    
    // check token valid
    if (![ASPIREAPI_AccessToken isValid]) {
        dispatch_async(queue, ^{
            NSLogAAM(@"%s: get user access token",__PRETTY_FUNCTION__);
            dispatch_suspend(queue);
            [ModelAspireApiManager getUserAccessTokenFromUsername:[AccountAuthentication username] password:[AccountAuthentication password] completion:^(ASPIREAPI_AccessToken *sToken, NSError *error) {
                errorTotal = error;
                dispatch_resume(queue);
            }];
        });
    }
    
    // create request
    dispatch_async(queue, ^{
        if (errorTotal == nil) {
            NSLogAAM(@"%s: update request",__PRETTY_FUNCTION__);
            dispatch_suspend(queue);
            [AspireAPIService updateRequestFromAccessToken:[ASPIREAPI_AccessToken user].accessToken  tokenType:[ASPIREAPI_AccessToken user].tokenType withUserInfo:root completion:^(NSError* _Nullable error) {
                errorTotal = error;
                dispatch_resume(queue);
            }];
        }
    });
    
    dispatch_barrier_async(queue, ^{
        NSLogAAM(@"%s: return with error: %@",__PRETTY_FUNCTION__,errorTotal.description);
        if (errorTotal != nil) {
            if ([[errorTotal.userInfo allKeys] containsObject:@"errorCode"]) {
                if ([[errorTotal.userInfo objectForKey:@"errorCode"] isEqualToString:@"ACE_UNAUTHORISED_001"]) {
                    [ASPIREAPI_AccessToken removeToken];
                    NSLogAAM(@"%s: get user access token again",__PRETTY_FUNCTION__);
                    [ModelAspireApiManager updateProfileCurrentUserWithUserInfo:userinfo completion:completion];
                    return;
                }
            } else if ([AspireApiError isNotAuthorizedFromError:errorTotal]) {
                if ([ModelAspireApiManager shared].errorAuthorized) {
                    [ModelAspireApiManager shared].errorAuthorized();
                    return;
                }
            }
        }
        if (completion) {
            completion(errorTotal);
        }
    });
}

+ (void)deleteRequestWithUserInfo:(NSDictionary *)userinfo completion:(void (^)(NSError *))completion {
    
    if (![User isValid] || ![AccountAuthentication username]) {
        if ([ModelAspireApiManager shared].errorAuthorized) {
            [ModelAspireApiManager shared].errorAuthorized();
            return;
        }
        if (completion) {
            completion([NSError errorWithDomain:@"com.aspirelifestyles.ios.mobileconcierge.retrieveprofile" code:ERR_USER_TOKEN userInfo:@{@"message":@"Invalid Params input or User not login yet"}]);
        }
        return;
    }
    
    NSMutableDictionary* root = [NSMutableDictionary dictionaryWithDictionary:@{@"program":[ModelAspireApiManager getProgram],
                                                                                @"preferredResponse":@"Email"/*,
                                                                                                              @"currentLocation":@"Unknown",
                                                                                                              @"currentPhone":@"",
                                                                                                              @"contactSource":@""*/}];
    
    NSMutableDictionary* profile = [NSMutableDictionary new];
    
    [profile addEntriesFromDictionary:@{@"emailAddress1":[User current].emails.firstObject.emailAddress,
                                        @"firstName":[User current].firstName,
                                        @"lastName":[User current].lastName,
                                        @"phoneNumber":[User current].phones.firstObject.phoneNumber
                                        }];
    if ([User current].salutation.length > 0) {
        [profile addEntriesFromDictionary:@{@"salutation":[[User current].salutation convertSalutationToAspireAPI]}];
    }
    NSMutableDictionary* request = [NSMutableDictionary new];//[NSMutableDictionary dictionaryWithDictionary:@{@"requestType":@"V AIRPORT TRANSFER",@"airportName":@"SIN",@"airline":@"SQ"}];
    
    [[userinfo allKeys] enumerateObjectsUsingBlock:^(NSString*  _Nonnull key, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([key isEqualToString:@"emailAddress1"]) {
            [profile setObject:[userinfo objectForKey:key] forKey:@"emailAddress1"];
        } else if ([key isEqualToString:@"salutation"]) {
            [profile setObject:[[userinfo objectForKey:key] convertSalutationToAspireAPI] forKey:@"salutation"];
        } else if ([key isEqualToString:@"firstName"]) {
            [profile setObject:[userinfo objectForKey:key] forKey:@"firstName"];
        } else if ([key isEqualToString:@"lastName"]) {
            [profile setObject:[userinfo objectForKey:key] forKey:@"lastName"];
        } else if ([key isEqualToString:@"phoneNumber"]) {
            [profile setObject:[userinfo objectForKey:key] forKey:@"phoneNumber"];
        } else if ([key isEqualToString:@"requestDetails"]) {
            [request setObject:[userinfo objectForKey:key] forKey:@"requestDetails"];
        } else if ([key isEqualToString:@"requestType"]) {
            [request setObject:[userinfo objectForKey:key] forKey:@"requestType"];
        } else if ([key isEqualToString:@"requestCity"]) {
            [request setObject:[userinfo objectForKey:key] forKey:@"requestCity"];
        } else if ([key isEqualToString:@"preferredResponse"]) {
            [root setValue:[userinfo objectForKey:key] forKey:@"preferredResponse"];
        }
    }];
    
    [root setObject:[ModelAspireApiManager getVerificationmetaData]  forKey:@"verificationMetadata"];
    [root setObject:profile  forKey:@"profile"];
    [root setObject:request  forKey:@"request"];
    
    __block NSError* errorTotal = nil;
    
    dispatch_queue_t queue = dispatch_queue_create("com.aspirelifestyles.ios.mobileconcierge.retrieveprofile", DISPATCH_QUEUE_SERIAL);
    
    // check token valid
    if (![ASPIREAPI_AccessToken isValid]) {
        dispatch_async(queue, ^{
            NSLogAAM(@"%s: get user access token",__PRETTY_FUNCTION__);
            dispatch_suspend(queue);
            [ModelAspireApiManager getUserAccessTokenFromUsername:[AccountAuthentication username] password:[AccountAuthentication password] completion:^(ASPIREAPI_AccessToken *sToken, NSError *error) {
                errorTotal = error;
                dispatch_resume(queue);
            }];
        });
    }
    
    // delete request
    dispatch_async(queue, ^{
        if (errorTotal == nil) {
            NSLogAAM(@"%s: delete request",__PRETTY_FUNCTION__);
            dispatch_suspend(queue);
            [AspireAPIService deleteRequestFromAccessToken:[ASPIREAPI_AccessToken user].accessToken  tokenType:[ASPIREAPI_AccessToken user].tokenType withUserInfo:root completion:^(NSError* _Nullable error) {
                errorTotal = error;
                dispatch_resume(queue);
            }];
        }
    });
    
    dispatch_barrier_async(queue, ^{
        NSLogAAM(@"%s: return with error: %@",__PRETTY_FUNCTION__,errorTotal.description);
        if (errorTotal != nil) {
            if ([[errorTotal.userInfo allKeys] containsObject:@"errorCode"]) {
                if ([[errorTotal.userInfo objectForKey:@"errorCode"] isEqualToString:@"ACE_UNAUTHORISED_001"]) {
                    [ASPIREAPI_AccessToken removeToken];
                    NSLogAAM(@"%s: get user access token again",__PRETTY_FUNCTION__);
                    [ModelAspireApiManager deleteRequestWithUserInfo:userinfo completion:completion];
                    return;
                }
            } else if ([AspireApiError isNotAuthorizedFromError:errorTotal]) {
                if ([ModelAspireApiManager shared].errorAuthorized) {
                    [ModelAspireApiManager shared].errorAuthorized();
                    return;
                }
            }
        }
        if (completion) {
            completion(errorTotal);
        }
    });
}

#pragma mark - OKTA User
+ (void)changePasswordWithNewPassword:(NSString *)newPassword andOldPassword:(NSString *)oldPassword completion:(void (^)(NSError *))completion {
    
    if (newPassword.length == 0 || oldPassword.length == 0) {
        if(completion) {
            completion([NSError errorWithDomain:@"com.s3corp.usertoken" code:-1011 userInfo:@{@"message":@"Password invalid"}]);
        }
        return;
    }
    
    // change password
    NSLogAAM(@"%s: change password",__PRETTY_FUNCTION__);
    [AspireAPIService changePasswordWithToken:ASPIRE_API_PASSWORD_TOKEN tokenType:ASPIRE_API_PASSWORD_TOKEN_TYPE email:[AccountAuthentication username] userInfo:@{@"oldPassword": @{ @"value": oldPassword },@"newPassword": @{ @"value": newPassword }} completion:^(NSError* _Nullable error) {
        if(error == nil) {
            [AccountAuthentication storedWithDictionary:@{@"username":[AccountAuthentication username],
                                                          @"email":[AccountAuthentication username],
                                                          @"password":newPassword
                                                          }];
        }
        if (completion) {
            completion(error);
        }
    }];
    
    
}

+ (void)resetPasswordForEmail:(NSString *)email completion:(void (^)(NSError *))completion {
    
    if (email.length == 0) {
        if(completion) {
            completion([NSError errorWithDomain:@"com.s3corp.usertoken" code:-1011 userInfo:@{@"message":@"Email invalid"}]);
        }
        return;
    }
    
    NSLogAAM(@"%s: reset password",__PRETTY_FUNCTION__);
    [AspireAPIService resetPasswordWithToken:ASPIRE_API_PASSWORD_TOKEN tokenType:ASPIRE_API_PASSWORD_TOKEN_TYPE email:[[AspireAPIService getClient] stringByAppendingFormat:@"_%@",email] completion:^(NSError* _Nullable error) {
        if (completion)
            completion(error);
     }];
    
}

+ (void)forgotPasswordForEmail:(NSString *)email withQuestion:(AAFRecoveryQuestion *) question newPassword:(AAFPassword *)password completion:(void (^)(NSError *))completion {
    
    if (email.length == 0 ||
        question.answer.length == 0 ||
        password.value.length == 0 ||
        question.question.length == 0) {
        if(completion) {
            completion([NSError errorWithDomain:@"com.s3corp.usertoken" code:-1011 userInfo:@{@"message":@"Email invalid"}]);
        }
        return;
    }
    
    __block NSDictionary* params = @{
                             @"password":@{@"value":password.value},
                             @"recovery_question":@{
                                     @"answer":question.answer
                                     }
                             };
    
    __block NSError* errorTotal = nil;
    __block NSString* urlForgot = nil;
    __block NSString* method = nil;
    
    dispatch_queue_t queue = dispatch_queue_create("com.aspirelifestyles.ios.mobileconcierge.forgetpassword", DISPATCH_QUEUE_SERIAL);
    
    // get profile okta
    dispatch_async(queue, ^{
        dispatch_suspend(queue);
        [AspireAPIService getProfileWithToken:ASPIRE_API_PASSWORD_TOKEN
                                    tokenType:ASPIRE_API_PASSWORD_TOKEN_TYPE
                                        email:[[AspireAPIService getClient] stringByAppendingFormat:@"_%@",email]
                                   completion:^(id json,NSError* _Nullable error) {
                                       errorTotal = error;
                                       OKTAUserProfile* data = nil;
                                       if (errorTotal == nil) {
                                           data = [OKTAUserProfile fromJSON:[ModelAspireApiManager jsonStringFromDictionary:json WithPrettyPrint:true] encoding:NSUTF8StringEncoding error:nil];
                                           if ([data.status isEqualToString:@"LOCKED_OUT"]) {
                                               errorTotal = [NSError errorWithDomain:@"com.s3corp.locked" code:-1011 userInfo:@{@"errorCode":@"OKTA_LOCKED_ERR"}];
                                           } else
                                           if ([password.value containsString:data.profile.firstName] ||
                                               [password.value containsString:data.profile.lastName]) {
                                               errorTotal = [NSError errorWithDomain:@"com.s3corp.wrongpassword" code:-1011 userInfo:@{@"message":@"password constrain first name or last name",@"errorCode":@"PASSWORDERROR"}];
                                           } else
                                           if (![data.credentials.recoveryQuestion.question isEqualToString:question.question]) {
                                               errorTotal = [NSError errorWithDomain:@"com.s3corp.wrongrecoveryquestions" code:-1011 userInfo:@{@"message":@"wrong recovery question",@"errorCode":@"RECOVERYQUESTIONWRONG"}];
                                           }
                                           if (data._links.forgotPassword.href.length > 0) {
                                               urlForgot = data._links.forgotPassword.href;
                                               method = data._links.forgotPassword.method;
                                           }
                                       }
                                       dispatch_resume(queue);
                                   }];
    });
    
    dispatch_async(queue, ^{
        if (errorTotal == nil) {
            dispatch_suspend(queue);
            [AspireAPIService forgotPasswordWithUrl:urlForgot
                                             method:method
                                              token:ASPIRE_API_PASSWORD_TOKEN
                                          tokenType:ASPIRE_API_PASSWORD_TOKEN_TYPE
                                              email:[[AspireAPIService getClient] stringByAppendingFormat:@"_%@",email]
                                             params:params
                                         completion:^(NSError* _Nullable error) {
                errorTotal = error;
                dispatch_resume(queue);
            }];
        }
    });
    
    dispatch_barrier_async(queue, ^{
        NSLogAAM(@"%s: return with error: %@",__PRETTY_FUNCTION__,errorTotal.description);
        if (completion) {
            completion(errorTotal);
        }
    });
}

+ (void)verifyBin:(NSString *)bin completion:(void (^)(BOOL, NSString*, id _Nullable))completion {
    NSInteger binNumber = [bin integerValue];
    if (binNumber <=0 || [bin stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]].length > 0) {
        if(completion) {
            completion(false,bin,[NSError errorWithDomain:@"com.s3corp.usertoken" code:-1011 userInfo:@{@"errorCode":@"ACE_BADREQUEST_007",@"errorMessage":@"Bin is invalid"}]);
        }
        return;
    }
    
    __block NSError* errorTotal = nil;
    __block BOOL isSuccess = false;
    
    dispatch_queue_t queue = dispatch_queue_create("com.aspirelifestyles.ios.mobileconcierge.verifybin", DISPATCH_QUEUE_SERIAL);
    
    // get service token
    dispatch_async(queue, ^{
        NSLogAAM(@"%s: get service access token",__PRETTY_FUNCTION__);
        dispatch_suspend(queue);
        [ModelAspireApiManager getServiceAccessTokenFromUsername:[ModelAspireApiManager getUsernameService] password:[ModelAspireApiManager getPasswordService] completion:^(ASPIREAPI_AccessToken *sToken, NSError *error) {
            errorTotal = error;
            dispatch_resume(queue);
        }];
    });
    
    // verify bin
    dispatch_async(queue, ^{
        if (errorTotal == nil) {
            NSLogAAM(@"%s: verify bin",__PRETTY_FUNCTION__);
            dispatch_suspend(queue);
            [AspireAPIService verifyBinWithToken:[ASPIREAPI_AccessToken service].accessToken tokenType:[ASPIREAPI_AccessToken service].tokenType bin:binNumber completion:^(BOOL success, NSError* error) {
                errorTotal = error;
                isSuccess = success;
                dispatch_resume(queue);
            }];
        }
    });
    
    dispatch_barrier_async(queue, ^{
        NSLogAAM(@"%s: return with error: %@",__PRETTY_FUNCTION__,errorTotal.description);
        if (errorTotal != nil) {
            if ([[errorTotal.userInfo allKeys] containsObject:@"errorCode"]) {
                if ([[errorTotal.userInfo objectForKey:@"errorCode"] isEqualToString:@"ACE_UNAUTHORISED_001"]) {
                    [ASPIREAPI_AccessToken removeToken];
                    NSLogAAM(@"%s: get user access token again",__PRETTY_FUNCTION__);
                    [ModelAspireApiManager verifyBin:bin completion:completion];
                    return;
                }
            }
        }
        if (completion) {
            completion(isSuccess,bin,errorTotal);
        }
    });
}

+ (void)getProfileFromEmail:(NSString *)email completion:(void (^)( OKTAUserProfile* _Nullable  OKTAuser, User* _Nullable  user,NSError *))completion {
    if (email.length == 0) {
        if(completion) {
            completion(nil,nil,[NSError errorWithDomain:@"com.s3corp.usertoken" code:-1011 userInfo:@{@"message":@"Email invalid"}]);
        }
        return;
    }
    
    NSLogAAM(@"%s: get profile",__PRETTY_FUNCTION__);
    [AspireAPIService getProfileWithToken:ASPIRE_API_PASSWORD_TOKEN
                                tokenType:ASPIRE_API_PASSWORD_TOKEN_TYPE
                                    email:[[AspireAPIService getClient] stringByAppendingFormat:@"_%@",email]
                               completion:^(id json,NSError* _Nullable error) {
                                   OKTAUserProfile* data = nil;
                                   User* user = nil;
                                   if (json) {
                                       data = [OKTAUserProfile fromJSON:[ModelAspireApiManager jsonStringFromDictionary:json WithPrettyPrint:true] encoding:NSUTF8StringEncoding error:nil];
                                       if (data) {
                                           user = [User convertFromOKTAUser:data];
                                       }
                                   }
        if (completion)
            completion(data,user,error);
    }];
}

+ (void)changeRecoveryQuestionForEmail:(NSString *)email password:(AAFPassword *)aafPassword question:(AAFRecoveryQuestion *)question completion:(void (^)(NSError * _Nullable))completion {
    if (email.length == 0 ||
        aafPassword.value.length == 0 ||
        question.answer.length == 0 ||
        question.question.length == 0) {
        if(completion) {
            completion([NSError errorWithDomain:@"com.s3corp.changerecoveryquestion" code:-1011 userInfo:@{@"message":@"Params invalid"}]);
        }
        return;
    }
    
    __block NSDictionary* params = @{
                                     @"password":@{@"value":aafPassword.value},
                                     @"recovery_question":@{
                                             @"question":question.question,
                                             @"answer":question.answer
                                             }
                                     };
    
    __block NSError* errorTotal = nil;
    __block NSString* oktaUserId = nil;
    __block NSString* urlChangeRecoveryQuestion = nil;
    __block NSString* method = nil;
    
    dispatch_queue_t queue = dispatch_queue_create("com.aspirelifestyles.ios.mobileconcierge.recoveryquestion", DISPATCH_QUEUE_SERIAL);
    
    // get profile okta
    dispatch_async(queue, ^{
        NSLogAAM(@"%s: get profile okta",__PRETTY_FUNCTION__);
        dispatch_suspend(queue);
        [AspireAPIService getProfileWithToken:ASPIRE_API_PASSWORD_TOKEN
                                    tokenType:ASPIRE_API_PASSWORD_TOKEN_TYPE
                                        email:[[AspireAPIService getClient] stringByAppendingFormat:@"_%@",email]
                                   completion:^(id json,NSError* _Nullable error) {
                                       errorTotal = error;
                                       OKTAUserProfile* data = nil;
                                       if (errorTotal == nil) {
                                           data = [OKTAUserProfile fromJSON:[ModelAspireApiManager jsonStringFromDictionary:json WithPrettyPrint:true] encoding:NSUTF8StringEncoding error:nil];
                                           if (data) {
                                               oktaUserId = data.id;
                                           }
                                           if (data._links.changeRecoveryQuestion.href.length > 0) {
                                               urlChangeRecoveryQuestion = data._links.changeRecoveryQuestion.href;
                                               method = data._links.changeRecoveryQuestion.method;
                                           }
                                       }
                                       dispatch_resume(queue);
        }];
    });
    
    // change recovery question
    dispatch_async(queue, ^{
        if (errorTotal == nil && oktaUserId != nil) {
            NSLogAAM(@"%s: change recovery question",__PRETTY_FUNCTION__);
            dispatch_suspend(queue);
            [AspireAPIService changeRecoveryQuestionWithUrl:urlChangeRecoveryQuestion
                                                     method:method
                                                      token:ASPIRE_API_PASSWORD_TOKEN
                                                    tokenType:ASPIRE_API_PASSWORD_TOKEN_TYPE
                                                   withOKTAId:oktaUserId
                                                       params:params
                                                   completion:^(id json, NSError* error) {
                                                       errorTotal = error;
                                                       dispatch_resume(queue);
                                                   }];
        }
    });
    
    dispatch_barrier_async(queue, ^{
        NSLogAAM(@"%s: return with error: %@",__PRETTY_FUNCTION__,errorTotal.description);
        if (completion) {
            completion(errorTotal);
        }
    });
}

#pragma mark - PMA
+ (void)getProfilePartyPMA:(NSString *)email completion:(void (^)(PMAUser* _Nullable, NSError * _Nullable))completion {
    if (email.length == 0) {
        if(completion) {
            completion(nil,[NSError errorWithDomain:@"com.s3corp.changerecoveryquestion" code:-1011 userInfo:@{@"message":@"Params invalid"}]);
        }
        return;
    }
    
    __block NSError* errorTotal = nil;
    __block NSString* token = nil;
    __block NSString* token_type = nil;
    __block PMAUser* pmaUser = nil;
    __block NSString* partyID = nil;
    
    dispatch_queue_t queue = dispatch_queue_create("com.aspirelifestyles.ios.mobileconcierge.getProfilePartyPMA", DISPATCH_QUEUE_SERIAL);
    
    // get service token
    dispatch_async(queue, ^{
        if (errorTotal == nil) {
            NSLogAAM(@"%s: get service access token PMA",__PRETTY_FUNCTION__);
            dispatch_suspend(queue);
            [AspireAPIService requestPMATokenWithUsername:PMA_SERIVCE_ACCOUNT password:PMA_SERVICE_PASSWORD completion:^(id json, NSError* error) {
                errorTotal = error;
                if (json) {
                    NSDictionary* dict = (NSDictionary*)json;
                    if ([[dict allKeys] containsObject:@"access_token"] && [[dict allKeys] containsObject:@"token_type"]) {
                        token = [dict objectForKey:@"access_token"];
                        token_type = [dict objectForKey:@"token_type"];
                    } else {
                        errorTotal = [NSError errorWithDomain:@"com.s3corp.findPartyFromPMA" code:-1011 userInfo:@{@"message":@"Cant get access token"}];
                    }
                }
                dispatch_resume(queue);
                
            }];
        }
    });
    
    // search party
    dispatch_async(queue, ^{
        if (errorTotal == nil ||
            token != nil ||
            token_type != nil) {
            NSLogAAM(@"%s: verify bin",__PRETTY_FUNCTION__);
            dispatch_suspend(queue);
            [AspireAPIService findPartyPMAWithToken:token tokenType:token_type email:email completion:^(id json, NSError* error) {
                errorTotal = error;
                if (json) {
                    if ([json isKindOfClass:[NSArray class]]) {
                        NSArray* list = (NSArray*)json;
                        if (list.count > 0 && [list.firstObject isKindOfClass:[NSDictionary class]]) {
                            partyID = [list.firstObject objectForKey:@"partyId"];
                        }
                    }
                }
                dispatch_resume(queue);
            }];
        }
    });
             
    // get profile PMA
    dispatch_async(queue, ^{
        if (errorTotal == nil && partyID != nil) {
            NSLogAAM(@"%s: get profile PMA",__PRETTY_FUNCTION__);
            dispatch_suspend(queue);
            [AspireAPIService getPartyPMAWithToken:token tokenType:token_type partyId:partyID completion:^(id _Nullable json, NSError* _Nullable error) {
                errorTotal = error;
                if ([[json allKeys] containsObject:@"partyId"]) {
                    pmaUser = [[PMAUser alloc] initWithJSONDictionary:json];
                }
                dispatch_resume(queue);
            }];
        }
    });
    
    dispatch_barrier_async(queue, ^{
        NSLogAAM(@"%s: return with error: %@",__PRETTY_FUNCTION__,errorTotal.description);
        if (completion) {
            completion(pmaUser,errorTotal);
        }
    });
}

#pragma mark - PRIVATE
+ (void) getServiceAccessTokenFromUsername:(NSString *)username password:(NSString *)password completion:(void (^)(ASPIREAPI_AccessToken *, NSError *))completion {
    
    if (username.length == 0 || password.length == 0) {
        if(completion) {
            completion(nil,[NSError errorWithDomain:@"com.s3corp.usertoken" code:ERR_SERVICE_TOKEN userInfo:@{@"message":@"username or password invalid"}]);
        }
        return;
    }
    
    [AspireAPIService requestAspireTokenWithUsername:username password:password completion:^(id _Nullable respone, NSError* _Nullable error) {
        NSError* err = nil;
        ASPIREAPI_AccessToken* userToken = nil;
        if (!error) {
            userToken = [[ASPIREAPI_AccessToken service] initWithJSONDictionary:respone];
        } else {
            err = error;
//            err = [NSError errorWithDomain:@"com.s3corp.usertoken" code:ERR_SERVICE_TOKEN userInfo:error.userInfo];
        }
        
        if(completion) {
            completion(userToken, err);
        }
    }];
}

+ (void) getUserAccessTokenFromUsername:(NSString *)username password:(NSString *)password completion:(void (^)(ASPIREAPI_AccessToken *, NSError *))completion {
    
    if (username.length == 0 || password.length == 0) {
        if(completion) {
            completion(nil,[NSError errorWithDomain:@"com.s3corp.usertoken" code:ERR_USER_TOKEN userInfo:@{@"message":@"username or password invalid"}]);
        }
        return;
    }
    
    [AspireAPIService requestAspireTokenWithUsername:username password:password completion:^(id _Nullable respone, NSError* _Nullable error) {
        
        NSError* err = nil;
        ASPIREAPI_AccessToken* userToken = nil;
        if (!error) {
            if ([[respone allKeys] containsObject:@"error"]) {
                err = [NSError errorWithDomain:@"com.s3corp.usertoken" code:ERR_USER_TOKEN userInfo:error.userInfo];
            } else {
                
                [ASPIREAPI_AccessToken saveJSONUserDictionary:respone];
                userToken = [ASPIREAPI_AccessToken user];
                [AccountAuthentication storedWithDictionary:@{@"username":username,
                                                              @"email":username,
                                                              @"password":password
                                                              }];
            }
        } else {
            if (error.code == 53) {
                err = error;
            } else
                err = [NSError errorWithDomain:@"com.s3corp.usertoken" code:ERR_USER_TOKEN userInfo:error.userInfo];
        }
        
        if(completion) {
            completion(userToken,err);
        }
    }];
}

+ (NSArray *)getSecurityQuetions {
//    return [self getSecurityQuetionsTest];
    return [self getSecurityQuetionsWithFile:@"ChallengeQuestion"];
}

+ (NSArray *)getSecurityQuetionsTest {
    return [self getSecurityQuetionsWithFile:@"ChallengeQuestionTest"];
}

+ (NSArray *)getSecurityQuetionsWithFile:(NSString *) fileName {
    __block NSArray* list = @[];
    
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_enter(group);
    [AspireAPIValidate getListFromFileName:fileName completion:^(NSArray * _Nullable data) {
        if (data != nil) {
            list = data;
        }
        dispatch_group_leave(group);
    }];
    dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
    
    return list;
}

+ (NSString*) getUsernameService {
    
    id dictTemp = [AspireAPIValidate getDataFromFileName:FILE_SERVICE_OKTA];
    if (![dictTemp isKindOfClass:[NSDictionary class]]) {return DMA_SERVICE_ACCOUNT;}
    
    NSDictionary* dict = (NSDictionary*)dictTemp;
    if([[dict allKeys] containsObject:@"username"]) {
        return [dict valueForKey:@"username"];
    }
    
    return DMA_SERVICE_ACCOUNT;
}

+ (NSString*) getPasswordService {
    
    id dictTemp = [AspireAPIValidate getDataFromFileName:FILE_SERVICE_OKTA];
    if (![dictTemp isKindOfClass:[NSDictionary class]]) {return DMA_SERVICE_PASSWORD;}
    
    NSDictionary* dict = (NSDictionary*)dictTemp;
    if([[dict allKeys] containsObject:@"password"]) {
        return [dict valueForKey:@"password"];
    }
    
    return DMA_SERVICE_PASSWORD;
}

+ (NSString*) getProgram {
    
    id dictTemp = [AspireAPIValidate getDataFromFileName:FILE_SERVICE_OKTA];
    if (![dictTemp isKindOfClass:[NSDictionary class]]) {return PROGRAM_REQUEST;}
    
    NSDictionary* dict = (NSDictionary*)dictTemp;
    if([[dict allKeys] containsObject:@"program"]) {
        return [dict valueForKey:@"program"];
    }
    
    return PROGRAM_REQUEST;
}

+ (NSDictionary*) getVerificationmetaData {
    
    id dictTemp = [AspireAPIValidate getDataFromFileName:FILE_SERVICE_OKTA];
    if (![dictTemp isKindOfClass:[NSDictionary class]]) {return VERIFICATIONMETADATA;}
    
    NSDictionary* dict = (NSDictionary*)dictTemp;
    if([[dict allKeys] containsObject:@"bin"]) {
        return @{@"bin":@([[dict valueForKey:@"bin"] integerValue])};
    }
    
    return VERIFICATIONMETADATA;
}

+ (NSString*) jsonStringFromDictionary:(NSDictionary*) dict WithPrettyPrint:(BOOL) prettyPrint {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                       options:(NSJSONWritingOptions)    (prettyPrint ? NSJSONWritingPrettyPrinted : 0)
                                                         error:&error];
    
    if (! jsonData) {
        NSLogAAM(@"%s: error: %@", __func__, error.localizedDescription);
        return @"{}";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}

#pragma mark - INIT
+ (ModelAspireApiManager *)shared {
    static dispatch_once_t onceToken;
    @synchronized([ModelAspireApiManager class]) {
        if (!shared)
            dispatch_once(&onceToken, ^{
                shared = [[ModelAspireApiManager alloc] init];
            });
        return shared;
    }
    return nil;
}

- (id)init
{
    if (!shared) {
        shared = [super init];
    }
    return shared;
}

- (void)continueRequest {
    if (queues) {
        [queues setSuspended:false];
    }
}

- (void)stopRequest {
    if (queues) {
        [queues setSuspended:true];
    }
    NSLogAAM(@"Stop all requests");
}

- (BOOL) isRequesting {
    return queues != nil;
}

+ (void) enableLogging {
    [ModelAspireApiManager shared].showAllLog = true;
}
+ (BOOL) isLogging {
    return [ModelAspireApiManager shared].showAllLog;
}

@end
