//
//  OKTAUserProfile.h
//  AspireApiFramework
//
//  Created by Dai on 7/24/18.
//  Copyright © 2018 com.aspirelifestyles.ios.mobileconcierge.UnitTest. All rights reserved.
//

#import <Foundation/Foundation.h>

@class OKTAUserProfile;
@class OKTAUserCredentials;
@class OKTAUserPassword;
@class OKTAUserProvider;
@class OKTAUserLinks;
@class OKTAUserDeactivate;
@class OKTAUserSelf;
@class OKTAUserProfileClass;
@class OKTARecoveryQuestion;


NS_ASSUME_NONNULL_BEGIN

#pragma mark - Object interfaces

@interface OKTAUserProfile : NSObject
@property (nonatomic, copy)           NSString *id;
@property (nonatomic, copy)           NSString *status;
@property (nonatomic, copy)           NSString *created;
@property (nonatomic, copy)           NSString *activated;
@property (nonatomic, copy)           NSString *statusChanged;
@property (nonatomic, nullable, copy) id lastLogin;
@property (nonatomic, copy)           NSString *lastUpdated;
@property (nonatomic, copy)           NSString *passwordChanged;
@property (nonatomic, strong)         OKTAUserProfileClass *profile;
@property (nonatomic, strong)         OKTAUserCredentials *credentials;
@property (nonatomic, strong)         OKTAUserLinks *_links;

+ (_Nullable instancetype)fromJSON:(NSString *)json encoding:(NSStringEncoding)encoding error:(NSError *_Nullable *)error;
+ (_Nullable instancetype)fromData:(NSData *)data error:(NSError *_Nullable *)error;
- (NSString *_Nullable)toJSON:(NSStringEncoding)encoding error:(NSError *_Nullable *)error;
- (NSData *_Nullable)toData:(NSError *_Nullable *)error;
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
@end

@interface OKTAUserCredentials : NSObject
@property (nonatomic, strong) OKTAUserPassword *password;
@property (nonatomic, strong) OKTAUserProvider *provider;
@property (nonatomic, strong) OKTARecoveryQuestion *recoveryQuestion;
@end

@interface OKTAUserPassword : NSObject
@end

@interface OKTAUserProvider : NSObject
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *name;
@end

@interface OKTARecoveryQuestion : NSObject
@property (nonatomic, copy) NSString *question;
@end

@interface OKTAUserLinks : NSObject
@property (nonatomic, strong) OKTAUserDeactivate *suspend;
@property (nonatomic, strong) OKTAUserDeactivate *resetPassword;
@property (nonatomic, strong) OKTAUserDeactivate *expirePassword;
@property (nonatomic, strong) OKTAUserDeactivate *changePassword;
@property (nonatomic, strong) OKTAUserDeactivate *forgotPassword;
@property (nonatomic, strong) OKTAUserDeactivate *changeRecoveryQuestion;
@property (nonatomic, strong) OKTAUserDeactivate *deactive;
@property (nonatomic, strong) OKTAUserSelf *self;
@property (nonatomic, strong) OKTAUserDeactivate *deactivate;
@end

@interface OKTAUserDeactivate : NSObject
@property (nonatomic, copy) NSString *href;
@property (nonatomic, copy) NSString *method;
@end

@interface OKTAUserSelf : NSObject
@property (nonatomic, copy) NSString *href;
@end

@interface OKTAUserProfileClass : NSObject
@property (nonatomic, copy)           NSString *firstName;
@property (nonatomic, copy)           NSString *lastName;
@property (nonatomic, nullable, copy) id mobilePhone;
@property (nonatomic, copy)           NSString *organization;
@property (nonatomic, nullable, copy) id secondEmail;
@property (nonatomic, copy)           NSString *userType;
@property (nonatomic, copy)           NSString *partyId;
@property (nonatomic, copy)           NSString *login;
@property (nonatomic, copy)           NSString *email;
@end

NS_ASSUME_NONNULL_END
