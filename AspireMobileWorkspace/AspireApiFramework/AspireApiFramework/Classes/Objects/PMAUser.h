#import <Foundation/Foundation.h>

@class PMAUser;
@class PMAContacts;
@class PMAEmail;
@class PMAPhone;
@class PMALocation;
@class PMAParentParty;
@class PMAPartyPerson;

NS_ASSUME_NONNULL_BEGIN

#pragma mark - Object interfaces

@interface PMAUser : NSObject
@property (nonatomic, copy)           NSString *partyID;
@property (nonatomic, strong)         PMAPartyPerson *partyPerson;
@property (nonatomic, assign)         NSInteger active;
@property (nonatomic, copy)           NSArray<PMAParentParty *> *parentParties;
@property (nonatomic, copy)           NSArray *names;
@property (nonatomic, copy)           NSArray *additionalInformations;
@property (nonatomic, copy)           NSArray *rewards;
@property (nonatomic, copy)           NSArray *partyEntitlements;
@property (nonatomic, copy)           NSArray *consents;
@property (nonatomic, copy)           NSArray *identifications;
@property (nonatomic, copy)           NSArray *partyVerifications;
@property (nonatomic, copy)           NSArray *personalAttributes;
@property (nonatomic, assign)         BOOL pdiRecord;
@property (nonatomic, copy)           NSString *partyType;
@property (nonatomic, copy)           NSString *partyStatusCode;
@property (nonatomic, nullable, copy) id partyName;
@property (nonatomic, nullable, copy) id recordEffectiveFrom;
@property (nonatomic, nullable, copy) id recordEffectiveTo;
@property (nonatomic, nullable, copy) id importanceLevel;
@property (nonatomic, copy)           NSArray<PMALocation *> *locations;
@property (nonatomic, strong)         PMAContacts *contacts;

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict;
@end

@interface PMAContacts : NSObject
@property (nonatomic, copy) NSArray<PMAEmail *> *emails;
@property (nonatomic, copy) NSArray *faxes;
@property (nonatomic, copy) NSArray<PMAPhone *> *phones;
@property (nonatomic, copy) NSArray *websites;
@property (nonatomic, copy) NSArray *socialMedias;
@end

@interface PMAEmail : NSObject
@property (nonatomic, copy)           NSString *emailID;
@property (nonatomic, copy)           NSString *emailTypeDescription;
@property (nonatomic, copy)           NSString *contactTypeDescription;
@property (nonatomic, assign)         BOOL pdiRecord;
@property (nonatomic, copy)           NSString *emailAddress;
@property (nonatomic, nullable, copy) id recordEffectiveFrom;
@property (nonatomic, nullable, copy) id recordEffectiveTo;
@property (nonatomic, copy)           NSString *emailTypeID;
@property (nonatomic, copy)           NSString *emailTypeCode;
@property (nonatomic, nullable, copy) id theDescription;
@property (nonatomic, copy)           NSString *contactTypeID;
@property (nonatomic, copy)           NSString *contactTypeCode;
@end

@interface PMAPhone : NSObject
@property (nonatomic, copy)           NSString *phoneID;
@property (nonatomic, copy)           NSString *phoneTypeDescription;
@property (nonatomic, copy)           NSString *contactTypeDescription;
@property (nonatomic, assign)         BOOL pdiRecord;
@property (nonatomic, nullable, copy) id phoneCountryCode;
@property (nonatomic, nullable, copy) id phoneAreaCode;
@property (nonatomic, copy)           NSString *phoneNumber;
@property (nonatomic, nullable, copy) id phoneExtension;
@property (nonatomic, nullable, copy) id recordEffectiveFrom;
@property (nonatomic, nullable, copy) id recordEffectiveTo;
@property (nonatomic, copy)           NSString *phoneTypeID;
@property (nonatomic, copy)           NSString *phoneTypeCode;
@property (nonatomic, nullable, copy) id theDescription;
@property (nonatomic, copy)           NSString *contactTypeID;
@property (nonatomic, copy)           NSString *contactTypeCode;
@end

@interface PMALocation : NSObject
@property (nonatomic, copy)           NSString *locationID;
@property (nonatomic, copy)           NSString *locationTypeDescription;
@property (nonatomic, copy)           NSString *contactTypeDescription;
@property (nonatomic, assign)         BOOL pdiRecord;
@property (nonatomic, nullable, copy) id recordEffectiveFrom;
@property (nonatomic, nullable, copy) id recordEffectiveTo;
@property (nonatomic, nullable, copy) id additionalInformation;
@property (nonatomic, copy)           NSString *locationTypeID;
@property (nonatomic, copy)           NSString *locationTypeCode;
@property (nonatomic, copy)           NSDictionary<NSString *, NSString *> *address;
@property (nonatomic, nullable, copy) id siteDetail;
@property (nonatomic, nullable, copy) id theDescription;
@property (nonatomic, copy)           NSString *contactTypeID;
@property (nonatomic, copy)           NSString *contactTypeCode;
@end

@interface PMAParentParty : NSObject
@property (nonatomic, copy)           NSString *partyID;
@property (nonatomic, assign)         NSInteger active;
@property (nonatomic, assign)         BOOL pdiRecord;
@property (nonatomic, copy)           NSString *partyType;
@property (nonatomic, nullable, copy) id partyStatusCode;
@property (nonatomic, nullable, copy) id partyName;
@property (nonatomic, nullable, copy) id recordEffectiveFrom;
@property (nonatomic, nullable, copy) id recordEffectiveTo;
@property (nonatomic, nullable, copy) id importanceLevel;
@end

@interface PMAPartyPerson : NSObject
@property (nonatomic, copy)           NSString *employee;
@property (nonatomic, copy)           NSString *fullName;
@property (nonatomic, copy)           NSString *firstName;
@property (nonatomic, nullable, copy) id middleName;
@property (nonatomic, copy)           NSString *lastName;
@property (nonatomic, copy)           NSString *displayName;
@property (nonatomic, copy)           NSString *aliasName;
@property (nonatomic, copy)           NSString *prefix;
@property (nonatomic, copy)           NSString *suffix;
@property (nonatomic, copy)           NSString *salutation;
@property (nonatomic, copy)           NSString *gender;
@property (nonatomic, copy)           NSString *maritalStatus;
@property (nonatomic, copy)           NSString *recordEffectiveFrom;
@property (nonatomic, copy)           NSString *recordEffectiveTo;
@property (nonatomic, copy)           NSString *dateOfBirth;
@property (nonatomic, copy)           NSString *residentCountry;
@property (nonatomic, copy)           NSString *homeCountry;
@property (nonatomic, copy)           NSString *nationality;
@property (nonatomic, copy)           NSString *signature;
@property (nonatomic, copy)           NSString *profilePic;
@property (nonatomic, copy)           NSString *disabilityStatus;
@property (nonatomic, nullable, copy) id placeOfBirth;
@property (nonatomic, copy)           NSString *vip;
@property (nonatomic, copy)           NSString *personType;
@property (nonatomic, nullable, copy) id employmentStatus;
@property (nonatomic, assign)         NSInteger statusTest;
@end

NS_ASSUME_NONNULL_END
