//
//  AspireApiError.m
//  MobileConciergeUSDemo
//
//  Created by Dai Pham on 6/5/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "AspireApiError.h"
#import "ModelAspireApiManager.h"

@implementation AspireApiError
+ (NSString *)getMessageAPIChangePasswordFromError:(NSError *)error {
    
    // errorCode = E0000014 => error common from cause: Password has been used too recently | Old Password is not correct | Password cannot be your current password
    
    for (NSString*key in [error.userInfo allKeys]) {
        if ([key isEqualToString:@"errorCauses"]) {
            NSArray* causes = [error.userInfo objectForKey:key];
            NSMutableString* temp = [NSMutableString stringWithString:@""];
            [causes enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [temp appendString:idx == 0 ? [NSString stringWithFormat:@"%@",[[[obj objectForKey:@"errorSummary"] componentsSeparatedByString:@":"] lastObject]] : [NSString stringWithFormat:@"\n%@",[[[obj objectForKey:@"errorSummary"] componentsSeparatedByString:@":"] lastObject]]];
            }];
            return temp;
        }
    }
    
    if (error.code == ERR_USER_TOKEN && [[error.userInfo allKeys] containsObject:@"error_description"]) {
        return [error.userInfo valueForKey:@"error_description"];
    }
    
    return @"";
}

+ (NSString *)getMessageAPISignupFromError:(NSError *)error {
    // errorCode = ACE_BADREQUEST_008 => email has used
    for (NSString*key in [error.userInfo allKeys]) {
        if ([key isEqualToString:@"message"]) {
            if ([[error.userInfo objectForKey:key] containsString:@"An object with this field already exists in the current organization"] ||
                [[error.userInfo objectForKey:key] containsString:@"Matching party found"]) {
                return @"An account with this email address already exists. Click Cancel to Sign In.";
            }
            return @"";
        }
    }
    return @"";
}

+ (NSString *)getMessageAPISignInFromError:(NSError *)error {
    
    // error: invalid_grant
    // error_description: The credentials provided were invalid.
    
    for (NSString*key in [error.userInfo allKeys]) {
        if ([key isEqualToString:@"error"]) {
            if ([[error.userInfo objectForKey:key] isEqualToString:@"invalid_grant"]) {
                return @"Please confirm that the value entered is correct.";
            }else{
                return @"";
            }
        }
    }
    return @"";
}

+ (NSString *)getMessageAPIResetPasswordFromError:(NSError *)error {
    
    // errorCode = E0000007
    // errorSummary = "Not found: Resource not found: dai.pham.198100@gmail1.com (User)"
    
    for (NSString*key in [error.userInfo allKeys]) {
        if ([key isEqualToString:@"errorCode"]) {
            if ([[error.userInfo objectForKey:key] isEqualToString:@"E0000007"]) {
                return @"* Enter a valid email address.";
            }else{
                return @"";
            }
            
        }
    }
    return @"";
}

+ (NSString *)getMessageAPICreateRequestFromError:(NSError *)error {
    
    // errorCode = ACE_BADREQUEST_000
    // wrong params of required field
    
    for (NSString*key in [error.userInfo allKeys]) {
        if ([key isEqualToString:@"message"]) {
            return [error.userInfo objectForKey:key];
        }
    }
    return @"";
}

#pragma mark - get error type
+ (AspireApiErrorType)getErrorTypeAPISignupFromError:(NSError *)error {
    
    if (error.code == ERR_NETWORK) {
        return NETWORK_UNAVAILABLE_ERR;
    }
    
    for (NSString*key in [error.userInfo allKeys]) {
        if ([key isEqualToString:@"errorCode"]) {
            
            if (([[error.userInfo objectForKey:key] containsString:@"ACE_BADREQUEST_000"] ||
                 [[error.userInfo objectForKey:key] containsString:@"ACE_BADREQUEST_008"]) && [[error.userInfo objectForKey:@"message"] containsString:@"password"]) {
                return PW_ERR;
            }
            
            if ([[error.userInfo objectForKey:key] containsString:@"ACE_BADREQUEST_016"] ||
                [[error.userInfo objectForKey:key] containsString:@"ACE_BADREQUEST_008"]) {
                return SIGNUP_EXIST_ERR;
            }
        }
    }
    
    return UNKNOWN_ERR;
}

+ (AspireApiErrorType)getErrorTypeAPISignInFromError:(NSError *)error {
    
    if (error.code == ERR_NETWORK) {
        return NETWORK_UNAVAILABLE_ERR;
    }
    
    for (NSString*key in [error.userInfo allKeys]) {
        if ([key isEqualToString:@"error"]) {
            return SIGNIN_INVALID_USER_ERR;
        } else if ([key isEqualToString:@"errorCode"]) {
            if ([[error.userInfo objectForKey:@"errorCode"] isEqualToString:@"OKTA_LOCKED_ERR"])
                return OKTA_LOCKED_ERR;
        }
    }
    
    return UNKNOWN_ERR;
}

+ (AspireApiErrorType) getErrorTypeAPICreateRequestFromError:(NSError *)error {
    
    if (error.code == ERR_USER_TOKEN) {
        return ACCESSTOKEN_INVALID_ERR;
    } else if (error.code == ERR_NETWORK) {
        return NETWORK_UNAVAILABLE_ERR;
    } else if ([AspireApiError isNotAuthorizedFromError:error]) {
        return INVALID_GRANT;
    }
    
    
    return UNKNOWN_ERR;
}

+ (AspireApiErrorType) getErrorTypeAPIRequestFromError:(NSError *)error {
    
    if (error.code == ERR_USER_TOKEN) {
        return ACCESSTOKEN_INVALID_ERR;
    } else if (error.code == ERR_NETWORK) {
        return NETWORK_UNAVAILABLE_ERR;
    } else if ([AspireApiError isNotAuthorizedFromError:error]) {
        return INVALID_GRANT;
    }
    
    return UNKNOWN_ERR;
}

+ (AspireApiErrorType)getErrorTypeAPIResetPasswordFromError:(NSError *)error {

    if (error.code == ERR_NETWORK) {
        return NETWORK_UNAVAILABLE_ERR;
    }
    
    for (NSString*key in [error.userInfo allKeys]) {
        if ([key isEqualToString:@"errorCode"]) {
            if ([[error.userInfo objectForKey:key] containsString:@"E0000007"]) {
                return RESETPW_ERR;
            }
        }
    }
    
    if ([AspireApiError isNotAuthorizedFromError:error]) {
        return INVALID_GRANT;
    }
    
    return UNKNOWN_ERR;
}

+ (AspireApiErrorType)getErrorTypeAPIForgetPasswordFromError:(NSError *)error {
    if (error.code == ERR_NETWORK) {
        return NETWORK_UNAVAILABLE_ERR;
    }
    
    if ([AspireApiError isNotAuthorizedFromError:error]) {
        return INVALID_GRANT;
    }
    
    for (NSString*key in [error.userInfo allKeys]) {
        if ([key isEqualToString:@"errorCode"]) {
            if ([[error.userInfo objectForKey:key] containsString:@"E0000001"] && [[[error.userInfo objectForKey:@"errorMessage"] lowercaseString] containsString:@"password"]) {
                return PW_ERR;
            } else if ([[error.userInfo objectForKey:key] containsString:@"E0000001"] && [[[error.userInfo objectForKey:@"errorMessage"] lowercaseString] containsString:@"answer"]) {
                return ANSWERQUESTION_ERR;
            } else if ([[error.userInfo objectForKey:key] containsString:@"E0000007"]) {
                return EMAIL_INVALID_ERR;
            } else if ([[error.userInfo objectForKey:key] containsString:@"E0000014"]) {
                return ANSWERQUESTION_ERR;
            } else if ([[error.userInfo objectForKey:key] containsString:@"E0000034"]) {
                return OKTA_LOCKED_ERR;//FORGETPW_USER_NOT_ALLOWED_ERR;
            } else if ([[error.userInfo objectForKey:key] containsString:@"PASSWORDERROR"]) {
                return PW_ERR;
            } else if ([[error.userInfo objectForKey:key] containsString:@"RECOVERYQUESTIONWRONG"]) {
                return RECOVERYQUESTION_ERR;
            } else if ([[error.userInfo objectForKey:key] containsString:@"E0000001"]){
                return FORGETPW_ERR;
            } else if ([[error.userInfo objectForKey:key] containsString:@"OKTA_LOCKED_ERR"]) {
                return OKTA_LOCKED_ERR;
            }
        }
    }
    
    return UNKNOWN_ERR;
}

+ (AspireApiErrorType)getErrorTypeAPIChangePasswordFromError:(NSError *)error {
    
    if ([AspireApiError isNotAuthorizedFromError:error]) {
        return INVALID_GRANT;
    }
    
    if (error.code == ERR_NETWORK) {
        return NETWORK_UNAVAILABLE_ERR;
    }
    BOOL shouldCheckDetail = false;
    BOOL oldPasswordWrong = false;
    for (NSString*key in [error.userInfo allKeys]) {
        if ([key isEqualToString:@"errorCode"]) {
            if ([[error.userInfo objectForKey:key] containsString:@"E0000014"]) {
                shouldCheckDetail = true;
            }
        }
        if ([key isEqualToString:@"errorCauses"]) {
            NSArray* list = (NSArray*)[error.userInfo objectForKey:key];
            for (NSDictionary* obj in list) {
                NSString* msg = [obj objectForKey:@"errorSummary"];
                if ([msg containsString:@"Old Password is not correct"]) {
                    oldPasswordWrong = true;
                }
            }
        }
    }
    if (shouldCheckDetail && oldPasswordWrong) {
        return OLDPW_ERR;
    } else if(shouldCheckDetail && !oldPasswordWrong) {
        return CHANGEPW_ERR;
    }
    return UNKNOWN_ERR;
}

+ (AspireApiErrorType)getErrorTypeAPIRetrieveProfileFromError:(NSError *)error {
    
    if (error.code == ERR_USER_TOKEN) {
        return ACCESSTOKEN_INVALID_ERR;
    } else if (error.code == ERR_NETWORK) {
        return NETWORK_UNAVAILABLE_ERR;
    }
    
    if ([AspireApiError isNotAuthorizedFromError:error]) {
        return INVALID_GRANT;
    }
    
    return UNKNOWN_ERR;
}

+ (AspireApiErrorType)getErrorTypeAPIUpdateProfileFromError:(NSError *)error {
    
    if (error.code == ERR_USER_TOKEN) {
        return ACCESSTOKEN_INVALID_ERR;
    } else if (error.code == ERR_NETWORK) {
        return NETWORK_UNAVAILABLE_ERR;
    }
    
    if ([AspireApiError isNotAuthorizedFromError:error]) {
        return INVALID_GRANT;
    }
    
    return UNKNOWN_ERR;
}

+ (AspireApiErrorType)getErrorTypeAPIVerifyBinFromError:(NSError *)error {
    if (error.code == ERR_NETWORK) {
        return NETWORK_UNAVAILABLE_ERR;
    }
    
    for (NSString*key in [error.userInfo allKeys]) {
        if ([key isEqualToString:@"errorCode"]) {
            if ([[error.userInfo objectForKey:key] containsString:@"ACE_BADREQUEST_007"]) {
                return BIN_ERR;
            }
        }
    }
    
    if ([AspireApiError isNotAuthorizedFromError:error]) {
        return INVALID_GRANT;
    }
    
    return UNKNOWN_ERR;
}

+ (AspireApiErrorType)getErrorTypeAPIGetProfileOKTAFromError:(NSError *)error {
    if (error.code == ERR_NETWORK) {
        return NETWORK_UNAVAILABLE_ERR;
    }
    
    for (NSString*key in [error.userInfo allKeys]) {
        if ([key isEqualToString:@"errorCode"]) {
            if ([[error.userInfo objectForKey:key] containsString:@"E0000007"]) {
                return OKTA_PROFILE_EXIST_ERR;
            } else if ([[error.userInfo objectForKey:key] containsString:@"OKTA_LOCKED_ERR"]) {
                return OKTA_LOCKED_ERR;
            }
        }
    }
    
    return UNKNOWN_ERR;
}

+ (AspireApiErrorType)getErrorTypeAPIChangeRecoveryQuestionOKTAFromError:(NSError *)error {
    if (error.code == ERR_NETWORK) {
        return NETWORK_UNAVAILABLE_ERR;
    }

    for (NSString*key in [error.userInfo allKeys]) {
        if ([key isEqualToString:@"errorCode"]) {
            if ([[error.userInfo objectForKey:key] containsString:@"E0000014"]) {
                return CHANGE_RECOVERY_QUESTION_ERR;
            } else if ([[error.userInfo objectForKey:key] containsString:@"E0000001"]) {
                 return NEWPASSWORD_ERR | ANSWER_ERR;
            } else if ([[error.userInfo objectForKey:key] containsString:@"E0000007"]) {
                return EMAIL_INVALID_ERR;
            }
        }
    }
    
    return UNKNOWN_ERR;
}

+ (BOOL)isNotAuthorizedFromError:(NSError *)error {
    if ([[error.userInfo allKeys] containsObject:@"error"]) {
        if ([[error.userInfo objectForKey:@"error"] isEqualToString:@"invalid_grant"]) {
            return true;
        }
    }
    return false;
}
@end
