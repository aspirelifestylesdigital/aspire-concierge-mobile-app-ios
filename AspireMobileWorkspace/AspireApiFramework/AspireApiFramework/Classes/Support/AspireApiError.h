//
//  AspireApiError.h
//  MobileConciergeUSDemo
//
//  Created by Dai Pham on 6/5/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    /*sign up*/
    SIGNUP_EXIST_ERR,           // user exist
    
    /*get profile okta*/
    OKTA_PROFILE_EXIST_ERR,     // email already used
    
    /*sign in*/
    SIGNIN_INVALID_USER_ERR,    // user not exist, or wrong username/password
    
    /*change password*/
    CHANGEPW_ERR,    // Old Password is not correct, Password has been used too recently, Password cannot be your current password
    OLDPW_ERR,
    
    /* reset password */
    RESETPW_ERR,    // Email not found
    
    /* forget password */
    FORGETPW_ERR,    // Password cannot be your current password, Password has been changed too recently
    RECOVERYQUESTION_ERR, // recovery question is not match
    ANSWERQUESTION_ERR, // The credentials provided were incorrect. (answer is wrong)
    FORGETPW_USER_NOT_ALLOWED_ERR, // Forgot password not allowed on specified user.
    
    /* verify bin */
    BIN_ERR,    // Invalid verification metadata
    
    /* change recovery question */
    CHANGE_RECOVERY_QUESTION_ERR,    // The security question answer is too weak, answer must not be part of the question
    NEWPASSWORD_ERR, //
    ANSWER_ERR,
    
    /* change recovery & forget password */
    EMAIL_INVALID_ERR,
    PW_ERR,  // password error
    
    /* sign in & fotgot password */
    OKTA_LOCKED_ERR,            // account locked
    
    /* common */
    UNKNOWN_ERR,                // error common for apis
    ACCESSTOKEN_INVALID_ERR,    // access token current user expired
    INVALID_GRANT,              // wrong email, password
    
    /* network unavailable */
    NETWORK_UNAVAILABLE_ERR,  // network unavailable
    
    /* Not a error */
    NONE,  // Not a error
    
} AspireApiErrorType;

@interface AspireApiError : NSObject
//+ (NSString*) getMessageAPIChangePasswordFromError:(NSError*)error;
//+ (NSString*) getMessageAPISignupFromError:(NSError*)error;
//+ (NSString*) getMessageAPISignInFromError:(NSError*)error;
//+ (NSString *)getMessageAPIResetPasswordFromError:(NSError *)error;
//+ (NSString *)getMessageAPICreateRequestFromError:(NSError *)error;

+ (AspireApiErrorType) getErrorTypeAPIChangePasswordFromError:(NSError*)error;
+ (AspireApiErrorType) getErrorTypeAPISignupFromError:(NSError*)error;
+ (AspireApiErrorType) getErrorTypeAPISignInFromError:(NSError*)error;
+ (AspireApiErrorType) getErrorTypeAPIResetPasswordFromError:(NSError *)error;
+ (AspireApiErrorType) getErrorTypeAPIForgetPasswordFromError:(NSError *)error;
+ (AspireApiErrorType) getErrorTypeAPICreateRequestFromError:(NSError *)error;
+ (AspireApiErrorType) getErrorTypeAPIRetrieveProfileFromError:(NSError *)error;
+ (AspireApiErrorType) getErrorTypeAPIUpdateProfileFromError:(NSError *)error;
+ (AspireApiErrorType) getErrorTypeAPIVerifyBinFromError:(NSError *)error;
+ (AspireApiErrorType) getErrorTypeAPIRequestFromError:(NSError *)error;
+ (AspireApiErrorType) getErrorTypeAPIGetProfileOKTAFromError:(NSError *)error;
+ (AspireApiErrorType) getErrorTypeAPIChangeRecoveryQuestionOKTAFromError:(NSError *)error;
+ (BOOL) isNotAuthorizedFromError:(NSError *)error;
@end
