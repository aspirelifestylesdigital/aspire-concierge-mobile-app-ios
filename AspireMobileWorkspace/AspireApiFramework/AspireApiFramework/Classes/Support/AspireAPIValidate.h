//
//  AspireAPIValidate.h
//  MobileConciergeUSDemo
//
//  Created by 💀 on 5/31/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (AspireAPI_validate)

- ( NSString* _Nullable ) convertSalutationToAspireAPI;
- ( NSString* _Nullable ) convertSalutationFromAspireAPI;
- (BOOL) isValidSalutation;

- (BOOL) isValidCountry;
- (BOOL) isValidGender;
- (BOOL) isValidLocationType;
- (BOOL) isValidStateProvince;
@end

@interface AspireAPIValidate : NSObject
+ (void) getDataFromFileName:(NSString*_Nullable) fileName completion:(void(^_Nullable)(NSDictionary* _Nullable)) completion;
+ (void) getListFromFileName:(NSString*_Nullable) fileName completion:(void(^_Nullable)(NSArray* _Nullable)) completion;
+ (void) storedData:(id)data toFileName:(NSString*)fileName;
+ (nullable id) getDataFromFileName:(NSString*)fileName;
@end
