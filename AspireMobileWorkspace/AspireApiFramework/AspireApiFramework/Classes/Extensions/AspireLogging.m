//
//  AspireLogging.m
//  AspireApiFramework
//
//  Created by VuongTC on 10/31/18.
//

#import "AspireLogging.h"
#import "ModelAspireApiManager.h"

void NSLogAAM(NSString *format, ...) {
    if ([ModelAspireApiManager isLogging] == false) {
        return;
    }
    va_list args;
    va_start(args, format);
    
    NSString* message = [[NSString alloc] initWithFormat:format arguments:args];
    fprintf(stderr,"%s\n", [message UTF8String]);
    va_end(args);
}
