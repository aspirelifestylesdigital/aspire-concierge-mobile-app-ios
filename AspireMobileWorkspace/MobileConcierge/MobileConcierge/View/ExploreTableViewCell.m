//
//  ExploreTableViewCell.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/9/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "ExploreTableViewCell.h"
#import "Constant.h"
#import "UIImageView+AFNetworking.h"
#import "NSString+Utis.h"
#import "Common.h"
#import "THLabel.h"
#import <QuartzCore/QuartzCore.h>
#import "UtilStyle.h"

@implementation ExploreTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.nameLbl.textColor = [AppColor activeButtonColor];
    self.addressLbl.textColor = [AppColor descriptionTextColor];
    self.offerLbl.textColor = [AppColor textColor];
    [self.nameLbl setFont:[UIFont fontWithName:FONT_MarkForMC_BOLD size:[AppSize descriptionTextSize]]];
    [self.addressLbl setFont:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionTextSize]]];
     [self.offerLbl setFont:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionTextSize]]];
    self.exploreImg.contentMode = UIViewContentModeScaleAspectFill;
    self.exploreImg.alignLeft = YES;
      
//    [self.nameLbl sizeToFit];
//    [self.addressLbl sizeToFit];
//    [self.offerLbl sizeToFit];
    
    [self.maskImageView.layer setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.4].CGColor];
    [self.categoryName setFont:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionTextSize]]];
    self.categoryName.shadowColor = [UIColor blackColor];
    self.categoryName.shadowOffset = CGSizeMake(0,4.0);
    self.categoryName.shadowBlur = 5.0f;
    [self.contentView setBackgroundColor:[UIColor whiteColor]];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    UIView *view = [self viewWithTag:1000];
    if(!view)
    {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - 1, self.bounds.size.width, 1.0f)];
        [view setBackgroundColor:[UtilStyle colorForSeparateLine]];
        view.tag = 1000;
        [self addSubview:view];
    }
    else
    {
        view.frame = CGRectMake(0, self.bounds.size.height - 1, self.bounds.size.width, 1.0f);
    }
    for (UILabel* label in @[self.nameLbl,self.addressLbl,self.offerLbl]) {
        label.adjustsFontSizeToFitWidth = NO;
        label.lineBreakMode = NSLineBreakByTruncatingTail;
    }
    
    self.addressLbl.hidden = self.addressLbl.text.length == 0;
}

- (BOOL)isTextTruncated:(UILabel*)label
{
    CGRect testBounds = self.bounds;
    testBounds.size.height = NSIntegerMax;
    CGRect limitActual = [label textRectForBounds:[label bounds] limitedToNumberOfLines:label.numberOfLines];
    CGRect limitTest = [label textRectForBounds:testBounds limitedToNumberOfLines:label.numberOfLines + 1];
    return limitTest.size.height<limitActual.size.height;
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    [self.maskImageView.layer setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.4].CGColor];
     UIView *view = [self viewWithTag:1000];
    if(view)
    {
        [view setBackgroundColor:[UtilStyle colorForSeparateLine]];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    // Configure the view for the selected state
    [super setSelected:selected animated:animated];
    if(selected)
    {
        [self.maskImageView.layer setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.4].CGColor];
        UIView *view = [self viewWithTag:1000];
        if(view)
        {
            [view setBackgroundColor:[UtilStyle colorForSeparateLine]];
        }
        [self.contentView setBackgroundColor:[AppColor selectedViewBackgroundColor]];
    }
}
@end
