//
//  ExploreDetailTableCel.m
//  MobileConcierge
//
//  Created by Chung Mai on 7/18/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "ExploreDetailTableCell.h"
#import "Common.h"
#import "UILabel+Extension.h"
#import "Constant.h"
#import "NSString+Utis.h"
#import "AlertViewController.h"
#import "TTTAttributedLabel.h"
#import "AppColor.h"
#import "AppSize.h"


@interface ExploreDetailTableCell () <TTTAttributedLabelDelegate>

@end

@implementation ExploreDetailTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self setStyleForView];
}

-(void)setStyleForView
{
    self.leadingConstraint.constant = 30.0f * SCREEN_SCALE;
    UIColor *colorText = [AppColor descriptionTextColor];
    UIColor *detailColorText = [AppColor textColor];
    //Header Info
    [self.btnShare setImage:[UIImage imageNamed:@"share_icon"] forState:UIControlStateNormal];
    [self.btnShare setImage:[UIImage imageNamed:@"share_icon_interact"] forState:UIControlStateHighlighted];
    [self.btnShare setImage:[UIImage imageNamed:@"share_icon_interact"] forState:UIControlStateSelected];
    
    [self.btnAskConcierge setImage:[UIImage imageNamed:@"concierge_icon"] forState:UIControlStateNormal];
    [self.btnAskConcierge setImage:[UIImage imageNamed:@"concierge_icon_interaction"] forState:UIControlStateHighlighted];
    [self.btnAskConcierge setImage:[UIImage imageNamed:@"concierge_icon_interaction"] forState:UIControlStateSelected];
    
    self.lblQuestion.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize largeTitleTextSize]];
    self.lblQuestion.textColor = [AppColor placeholderTextColor];
    
    //Address Info
    UIFont *detailFont = [UIFont fontWithName:FONT_MarkForMC_LIGHT size:[AppSize titleTextSize]];
    self.lblAddress1.font = detailFont;
    self.lblAddress1.textColor = detailColorText;
    self.lblAddress3.font = detailFont;
    self.lblAddress3.textColor = detailColorText;
    self.btnSeeMap.titleLabel.font = detailFont;
    self.btnWebLink.titleLabel.font = detailFont;
    
    //Insider Tip Info
    UIFont *titleFont = [UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize descriptionTextSize]];
    self.lblInsiderTipsTitle.font = titleFont;
    self.lblInsiderTipsTitle.textColor = [AppColor activeButtonColor];
    [self.lblInsiderTipsTitle sizeToFit];
    self.lblInsiderTipsDetail.font = detailFont;
    self.lblInsiderTipsDetail.textColor = detailColorText;
    [self.lblInsiderTipsDetail sizeToFit];
    self.lblInsiderTipsDetail.textInsets = UIEdgeInsetsMake(0, 18, 0, 0);
    
    
    //Benefits Info
    self.lblBenefitsTitle.font = titleFont;
    self.lblBenefitsTitle.textColor = [AppColor activeButtonColor];
    [self.lblBenefitsTitle sizeToFit];
    self.lblBenefitsDetail.textInsets = UIEdgeInsetsMake(0, 20, 0, 0);
    
    //Price Info
    UIFont *priceTitleFont = [UIFont fontWithName:FONT_MarkForMC_Nrw_MED size:[AppSize moreInfoTextSize]];
    self.lblPriceRangeTitle.font = priceTitleFont;
    self.lblPriceRangeTitle.textColor = colorText;
    
    self.lblCuisineTitle.font = priceTitleFont;
    self.lblCuisineTitle.textColor = colorText;
    
    self.lblPriceRange.font = detailFont;
    self.lblPriceRange.textColor = detailColorText;
    [self.lblPriceRange sizeToFit];
    
    self.lblCuisine.font = detailFont;
    self.lblCuisine.textColor = detailColorText;
    [self.lblCuisine sizeToFit];
    
    //Description Info
    self.lblDecriptionTitle.font = priceTitleFont;
    self.lblDecriptionTitle.textColor = colorText;
    
    //Term Info
    self.lblTermTitle.font = priceTitleFont;
    self.lblTermTitle.textColor = colorText;
    
    //Hours Info
    self.lblHoursTitle.font = priceTitleFont;
    self.lblHoursTitle.textColor = colorText;
    self.lblHoursDetail.font = detailFont;
    self.lblHoursDetail.textColor = detailColorText;
}

-(NSString *)setStyleTextForHTMLString:(NSString *)htmlString
{
    htmlString = [htmlString stringByAppendingString:[NSString stringWithFormat:@"<style>i{font-family: '%@'; color:#272726;font-size:%fpx;}</style>",FONT_MarkForMC_LIGHTIT, [AppSize titleTextSize]]];
    htmlString = [htmlString stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; color:#272726;font-size:%fpx;}</style>",FONT_MarkForMC_LIGHT, [AppSize titleTextSize]]];
    return htmlString;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


#pragma mark HEADER INFO
- (void)loadDataForHeader{
    self.lblQuestion.text = self.questionName;
    [self.lblQuestion sizeToFit];
}


- (IBAction)touchAskConcierge:(id)sender {
    self.btnAskConcierge.highlighted = YES;
    self.btnAskConcierge.selected = YES;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.btnAskConcierge.highlighted = NO;
        self.btnAskConcierge.selected = NO;
        [self.delegate touchAskConcierge];
    });
    
}
- (IBAction)touchShare:(id)sender {
    NSMutableArray *data = [[NSMutableArray alloc] init];
    if(self.lblQuestion.text.length > 0)
    {
        [data addObject:[NSString stringWithFormat:@"%@\n",self.lblQuestion.text]];
    }
    
    [data addObject:[NSURL URLWithString:MASTERCARD_SHARING]];
    
    if(self.headImg.image)
    {
        [data addObject:self.headImg.image];
    }
    self.btnShare.highlighted = YES;
    self.btnShare.selected = YES;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.btnShare.highlighted = NO;
        self.btnShare.selected = NO;
        [self.delegate shareUrl:data];
    });
    
}

#pragma mark ADDRESS INFO

- (void) loadDataForInfo{
    self.lblAddress1.text = self.address1;
    self.lblAddress3.text = self.address3;
    if ([self.webLink containsString:@"http"]) {
        // normal state
        [self.btnWebLink setAttributedTitle:[self attributeStringUnderlineWithColor:[AppColor textColor] andText:self.webLink] forState:UIControlStateNormal];
        
        // selected state
        [self.btnWebLink setAttributedTitle:[self attributeStringUnderlineWithColor:[AppColor childViewBackgroundColor] andText:self.webLink] forState:UIControlStateHighlighted];
        
        self.btnWebLink.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.btnWebLink.titleLabel.numberOfLines = 2;//if you want unlimited number of lines put 0
    }else{
        self.btnWebLink.hidden = YES;
    }
    
    if(self.googleAddress.length > 0)
    {
        [self.btnSeeMap setAttributedTitle:[self attributeStringUnderlineWithColor:[AppColor textColor] andText:@"See map"] forState:UIControlStateNormal];
        [self.btnSeeMap setAttributedTitle:[self attributeStringUnderlineWithColor:[AppColor childViewBackgroundColor] andText:@"See map"] forState:UIControlStateHighlighted];
    }
    else{
        self.btnSeeMap.hidden = YES;
    }
}

- (NSAttributedString*)attributeStringUnderlineWithColor:(UIColor*)color andText:(NSString*)text{
    
    NSArray * objects = [[NSArray alloc] initWithObjects:color, [NSNumber numberWithInt:NSUnderlineStyleSingle], nil];
    NSArray * keys = [[NSArray alloc] initWithObjects:NSForegroundColorAttributeName, NSUnderlineStyleAttributeName, nil];
    
    NSDictionary * linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    return  [[NSAttributedString alloc] initWithString:text attributes:linkAttributes];
}

- (IBAction)touchSeeMap:(id)sender {
    [self.delegate seeDetailMapWithAddress:self.googleAddress];
}

- (IBAction)touchWebLink:(id)sender {
    [self.delegate gotoWebBrowser];
}


#pragma mark INSIDER TIP INFO

- (void)loadDataForInsiderTip{
    if(self.insiderTip.length > 0)
    {
        if ([self.insiderTip containsString:@"<"]) {
            self.insiderTip = [self setStyleTextForHTMLString:self.insiderTip];
            NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                    initWithData: [self.insiderTip dataUsingEncoding:NSUnicodeStringEncoding]
                                                    options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                NSFontAttributeName: [UIFont fontWithName:self.font.fontName size:self.font.pointSize]}
                                                    documentAttributes: nil
                                                    error: nil
                                                    ];
            self.lblInsiderTipsDetail.attributedText = attributedString;
        }else{
            self.lblInsiderTipsDetail.text = self.insiderTip;
        }
        
        self.addressBottomMargin.hidden = YES;
    }
    else{
        self.insiderTipsTopMargin.hidden = YES;
        self.insiderTipsMiddleMargin.hidden = YES;
        self.insiderTipsBottomMargin.hidden = YES;
        self.insiderTipsHeaderView.hidden = YES;
        self.lblInsiderTipsDetail.text = nil;
    }
}

#pragma mark BENEFITS INFO

- (void)loadDataForBenefits{
    if(self.benefits.length > 0)
    {
        if([self.itemID isEqualToString:@"27"])
        {
            self.benefits = [self.benefits stringByReplacingOccurrencesOfString:@"Early access/pre-order of new products before available<br />\r\n\tto the public" withString:@"Early access pre-order of new products before available to the public"];
        }
        
        NSRange range = [self.benefits rangeOfString:@"Click here"];
        if (range.location != NSNotFound) {
            self.benefits = [self.benefits substringToIndex:range.location];
            self.benefits = [NSString stringWithFormat:@"%@ </p>",self.benefits];
        }
        range = [self.benefits rangeOfString:@"Contact us"];
        if (range.location != NSNotFound) {
            self.benefits = [self.benefits substringToIndex:range.location];
            self.benefits = [NSString stringWithFormat:@"%@ </p>",self.benefits];
        }
        range = [self.benefits rangeOfString:@"Contact&nbsp;us"];
        if (range.location != NSNotFound) {
            self.benefits = [self.benefits substringToIndex:range.location];
            self.benefits = [NSString stringWithFormat:@"%@ </p>",self.benefits];
        }
        
        self.benefits = [self.benefits removeRedudantNewLineInText];
        
        if(self.benefits.length == 0){
            self.benefitTopMargin.hidden = YES;
            self.benefitBottomMargin.hidden = YES;
            self.benefitHeaderView.hidden = YES;
            self.lblBenefitsDetail.hidden = YES;
            return;
        }
            
        self.benefits = [self setStyleTextForHTMLString:self.benefits];
        
        NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                initWithData: [self.benefits dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil
                                                ];
        self.lblBenefitsDetail.linkAttributes = @{
          NSForegroundColorAttributeName: [AppColor descriptionTextColor],NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_LIGHT size:[AppSize titleTextSize]],NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)
          };
        
        self.lblBenefitsDetail.activeLinkAttributes = @{
                                                           NSForegroundColorAttributeName: [AppColor childViewBackgroundColor],NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_LIGHT size:[AppSize titleTextSize]],NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)
                                                           };
        
        if(self.benefits.length > 0)
        {
            self.lblBenefitsDetail.delegate = self;
            self.lblBenefitsDetail.enabledTextCheckingTypes = NSTextCheckingTypeLink;
            self.lblBenefitsDetail.text = attributedString;
            self.addressBottomMargin.hidden = YES;
        }
    }
    else{
        self.benefitTopMargin.hidden = YES;
        self.benefitBottomMargin.hidden = YES;
        self.benefitHeaderView.hidden = YES;
        self.lblBenefitsDetail.hidden = YES;
    }
}


#pragma mark HOURS INFO

- (void)loadDataForHours{
    if(self.workingHour.length > 0)
    {
        self.workingHour = [self setStyleTextForHTMLString:self.workingHour];
        NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                initWithData: [self.workingHour dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                            NSFontAttributeName: [UIFont fontWithName:self.font.fontName size:self.font.pointSize]}
                                                documentAttributes: nil
                                                error: nil
                                                ];
      self.lblHoursDetail.attributedText = attributedString;
    }
    else{
        self.hoursSeparateLine.hidden = YES;
        self.lblHoursTitle.hidden = YES;
        self.lblHoursDetail.text = nil;
        self.hoursTopMargin.hidden = YES;
        self.hoursBottomMargin.hidden = YES;
    }
}

#pragma mark DESCRIPTION INFO

-(void)loadDataForDescription{

    if(self.cellDescription.length > 0)
    {
        self.cellDescription = [self strimStringFrom:self.cellDescription withPattens:@[@"<table .*?>",@"</table>",@"</tbody>",@"<tbody>",@"</tr>",@"<tr>",@"</td>",@"<td>",@"</tr>"]];
        self.cellDescription = [self.cellDescription removeRedudantNewLineInText];
        NSRange range = [self.cellDescription rangeOfString:@"Click here"];
        if (range.location != NSNotFound) {
            self.cellDescription = [self.cellDescription substringToIndex:range.location];
            self.cellDescription = [NSString stringWithFormat:@"%@ </p>",self.cellDescription];
        }
        range = [self.cellDescription rangeOfString:@"Contact us"];
        if (range.location != NSNotFound) {
            self.cellDescription = [self.cellDescription substringToIndex:range.location];
            self.cellDescription = [NSString stringWithFormat:@"%@ </p>",self.cellDescription];
        }
        while ([self.cellDescription containsString:@"<img"])
        {
            NSRange range2 = [self.cellDescription rangeOfString:@"<img"];
            if(range2.location != NSNotFound)
            {
                // get image link from Description
                NSString *imgString = [self.cellDescription substringFromIndex:range2.location];
                
                NSString* result = [imgString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                if ([result containsString:@"src"] && [result containsString:@"/>"]){
                    result = [result substringToIndex:[result rangeOfString:@"/>"].location + 2];
                }
                
                /// trim image tag from Description
                self.cellDescription = [self.cellDescription stringByReplacingOccurrencesOfString:result withString:@""];
                self.cellDescription = [[self flattenHtml:self.cellDescription] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                self.cellDescription = [self.cellDescription stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
            }
        }
        self.cellDescription = [self.cellDescription removeRedudantNewLineInText];
        self.cellDescription = [self setStyleTextForHTMLString:self.cellDescription];
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]
                                                initWithData: [self.cellDescription dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil
                                                ];

        
        
        self.lblDescriptionDetail.linkAttributes = @{
                                                     NSForegroundColorAttributeName: [AppColor descriptionTextColor],NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_LIGHT size:[AppSize titleTextSize]],NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)
                                                     };
        self.lblDescriptionDetail.activeLinkAttributes = @{
                                                     NSForegroundColorAttributeName: [AppColor childViewBackgroundColor],NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_LIGHT size:[AppSize titleTextSize]],NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)
                                                     };

        
        if(self.cellDescription.length > 0)
        {
            self.lblDescriptionDetail.delegate = self;
            self.lblDescriptionDetail.enabledTextCheckingTypes = NSTextCheckingTypeLink;
            self.lblDescriptionDetail.text = attributedString;
            self.lblDescriptionDetail.text = attributedString;
        }
    }
    else
    {
        self.descriptionSeparateLine.hidden = YES;
        self.lblDecriptionTitle.hidden = YES;
        self.lblDescriptionDetail.hidden = YES;
        self.descriptionTopMargin.hidden = YES;
        self.descriptionBottomMargin.hidden = YES;
    }
}

- (NSString *)flattenHtml: (NSString *) html {
    NSScanner *theScanner;
    NSString *text = nil;
    theScanner = [NSScanner scannerWithString: html];
    while ([theScanner isAtEnd] == NO) {
        
        [theScanner scanUpToString: @"<" intoString: NULL];
        
        [theScanner scanUpToString: @">" intoString: &text];
        
        // Only Replace if you are outside of an html tag
    } // while
    return html;
}

#pragma mark TERM INFO

-(void)loadDataForTermOfCondition
{
    self.termDetail = [self.termDetail removeRedudantNewLineInText];
    if(self.termDetail.length > 0)
    {
        self.lblTermDetail.delegate = self;
        self.termDetail = [self setStyleTextForHTMLString:self.termDetail];
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]
                                                initWithData: [self.termDetail dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil
                                                ];
        
        self.lblTermDetail.linkAttributes =  @{
                                               NSForegroundColorAttributeName: [AppColor descriptionTextColor],NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_LIGHT size:[AppSize titleTextSize]],NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)
                                               };
        self.lblTermDetail.activeLinkAttributes = @{
                                                           NSForegroundColorAttributeName: [AppColor childViewBackgroundColor],NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_LIGHT size:[AppSize titleTextSize]],NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)
                                                           };
        self.lblTermDetail.text = attributedString;
    }
    else
    {
        self.termBottomMargin.hidden = YES;
        self.termSeparateLine.hidden = YES;
        self.termTopMargin.hidden = YES;
        self.lblTermTitle.hidden = YES;
        self.lblTermDetail.hidden = YES;
    }

}


#pragma mark PRICE INFO

- (void)loadDataForPrice{
    if(self.cuisine.length > 0)
    {
        NSString *strPriceRange = @"";
        for (int i = 0; i< [self.priceRange integerValue]; i++) {
            strPriceRange = [strPriceRange stringByAppendingString:@"$"];
        }
        self.lblPriceRange.text = strPriceRange;
        self.lblCuisine.text = self.cuisine;
    }
    else{
        self.priceSeparateLine.hidden = YES;
        self.priceMainView.hidden = YES;
        self.priceTopMargin.hidden = YES;
    }
    
}


#pragma mark TAP URL
- (void)attributedLabel:(__unused TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url
{
    [self.delegate openURL:url];
}

- (NSString*) strimStringFrom:(NSString*) from withPattens:(NSArray*)pattern {
    NSString* temp = from;
    for (NSString* p in pattern) {
        temp = [temp stringByReplacingOccurrencesOfString:p withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0,temp.length)];
    }
    return temp;
}
@end
