//
//  ExploreTableViewCell.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/9/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseItem.h"
#import "BaseTableViewCell.h"
#import "THLabel.h"
#import "UIImageViewAligned.h"

@interface ExploreTableViewCell : BaseTableViewCell

@property(nonatomic, weak) IBOutlet UIImageViewAligned *exploreImg;
@property(nonatomic, weak) IBOutlet UILabel *nameLbl;
@property(nonatomic, weak) IBOutlet UILabel *addressLbl;
@property(nonatomic, weak) IBOutlet UILabel *offerLbl;
@property(nonatomic, weak) IBOutlet UIButton *favoritBtn;
@property (nonatomic, assign) BOOL isFavorite;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageWidthConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *starImage;
@property (weak, nonatomic) IBOutlet UIView *maskImageView;
@property (weak, nonatomic) IBOutlet THLabel *categoryName;

-(void) initCellWithData:(BaseItem *)item;
@end
