//
//  SearchHeaderView.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/11/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "SearchHeaderView.h"
#import "Common.h"
#import "Constant.h"
#import "AppColor.h"
#import "AppSize.h"

@implementation SearchHeaderView

-(void)setupView
{
    resetScaleViewBaseOnScreen(self);
    [self.offerClear setImage:[UIImage imageNamed:@"clear_icon"] forState:UIControlStateNormal];
    [self.bookOnlineClear setImage:[UIImage imageNamed:@"clear_icon"] forState:UIControlStateNormal];
    
    [self.bookOnlineText setText:NSLocalizedString(@"can_book_search", nil)];
    [self.offerText setText:NSLocalizedString(@"offer_search", nil)];
    
    UIColor *color = [AppColor textColor];
    self.offerText.font = [UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize titleTextSize]];
    self.offerText.textColor = color;
    [self.searchTextView setBackgroundColor:[AppColor childViewBackgroundColor]];
    
    UIButton *clearButton = [self.searchText valueForKey:@"_clearButton"];
    [clearButton setImage:[UIImage imageNamed:@"clear_icon"] forState:UIControlStateNormal];
    [clearButton setImage:[UIImage imageNamed:@"clear_icon"] forState:UIControlStateHighlighted];
    
    self.searchText.clearButtonMode = UITextFieldViewModeAlways;
    self.searchText.returnKeyType = UIReturnKeySearch;
    
    if(!self.isSearchOffer && !self.isSearchBookOnline)
    {
        self.offerClear.hidden = YES;
        self.offerText.hidden = YES;
        self.bookOnlineClear.hidden = YES;
        self.bookOnlineText.hidden = YES;
    }
    else if(self.isSearchOffer && !self.isSearchBookOnline)
    {
        self.bookOnlineClear.hidden = YES;
        self.bookOnlineText.hidden = YES;
    }
    else if(!self.isSearchOffer && self.isSearchBookOnline)
    {
        self.offerText.text = self.bookOnlineText.text;
        self.bookOnlineClear.hidden = YES;
        self.bookOnlineText.hidden = YES;
    }
}

-(CGFloat)calculateHeightView
{
    if(self.isSearchOffer && self.isSearchBookOnline)
    {
        return (self.searchTextView.frame.size.height + self.bookOnlineClear.frame.size.height + self.offerClear.frame.size.height) * SCREEN_SCALE + 30.0f;
    }
    else if(self.isSearchOffer && !self.isSearchBookOnline)
    {
        return (self.searchTextView.frame.size.height + self.offerClear.frame.size.height) * SCREEN_SCALE  + 20.0f;
    }
    else if(!self.isSearchOffer && self.isSearchBookOnline)
    {
        return (self.searchTextView.frame.size.height + self.bookOnlineClear.frame.size.height ) * SCREEN_SCALE + 20.0f;
    }
    else{
        return self.searchTextView.frame.size.height * SCREEN_SCALE;
    }
}

-(void)setBottomLine
{
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0.0f, self.frame.size.height - 1, SCREEN_WIDTH, 1.0f)];
    [line setBackgroundColor:[UIColor colorWithRed:0.8f green:0.8f blue:0.8f alpha:1.f]];
    [self addSubview:line];
}

@end
