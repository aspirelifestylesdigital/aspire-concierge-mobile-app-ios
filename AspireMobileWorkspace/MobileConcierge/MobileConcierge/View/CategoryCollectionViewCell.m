//
//  CategoryCollectionViewCell.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "CategoryCollectionViewCell.h"
#import "CategoryItem.h"
#import "Common.h"
#import <QuartzCore/QuartzCore.h>
#import "THLabel.h"

@implementation CategoryCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    resetScaleViewBaseOnScreen(self.contentView);
    [self.name setFont:[UIFont fontWithName:FONT_MarkForMC_BOLD size:[AppSize headerTextSize]]];
    self.name.shadowColor = [UIColor blackColor];
    self.name.shadowOffset = CGSizeMake(0,4.0);
    self.name.shadowBlur = 5.0f;
}


-(void) initCellWithData:(CategoryItem *)item withDisableItem:(BOOL)isDisable;
{
    self.categoryImg.image = item.categoryImg;
    self.name.text = [item.code isEqualToString:@"experiences"] ? @"CONCIERGE \nEXPERIENCES" : [item.categoryName uppercaseString];
//    [self.name sizeToFit];
    
    if(isDisable){
        [self.maskView setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.8]];
    }
    else{
        [self.maskView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.4]];
    }
}

-(void)setCellToDefault
{
    self.name.textColor = [UIColor whiteColor];
    [self.maskView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.4]];
}
@end
