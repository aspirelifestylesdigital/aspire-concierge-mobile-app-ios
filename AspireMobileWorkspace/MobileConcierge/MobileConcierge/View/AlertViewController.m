//
//  AlertViewController.m
//  MobileConcierge
//
//  Created by Mac OS on 5/31/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "AlertViewController.h"
#import "Common.h"
#import "AppSize.h"
#import "Constant.h"

@interface AlertViewController () <UIScrollViewDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate>

- (IBAction)touchOk:(id)sender;
- (IBAction)touchCancel:(id)sender;

@end

@implementation AlertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.viewAlert.layer.cornerRadius = 8;
    self.lblAlertTitle.text = self.titleAlert;
    self.lblAlertMessage.text = self.msgAlert;
    [self.firstBtn setTitle:self.firstBtnTitle forState:UIControlStateNormal];
    if(self.secondBtnTitle)
    {
        [self.seconBtn setTitle:self.secondBtnTitle forState:UIControlStateNormal];
    }
    [self setUpCustomizedPanGesturePopRecognizer];
    
    [self.seconBtn.titleLabel setAdjustsFontSizeToFitWidth:true];
    [self.firstBtn.titleLabel setAdjustsFontSizeToFitWidth:true];
    
    [self.lblAlertMessage setFont:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]]];
    [self.lblAlertTitle setFont:[UIFont fontWithName:FONT_MarkForMC_BOLD size:[AppSize titleTextSize]]];
//    resetScaleViewBaseOnScreen(self.view);
    
    [self.seconBtn addObserver:self forKeyPath:@"hidden" options:(0) context:NULL];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"hidden"]) {
        [self.seconBtn removeObserver:self forKeyPath:@"hidden"];
        if (self.seconBtn.isHidden) [_midView removeFromSuperview];
    }
}

- (void)dealloc {
    if (self.seconBtn)
        [self.seconBtn removeObserver:self forKeyPath:@"hidden"];
}

-(void)setUpCustomizedPanGesturePopRecognizer
{
    self.navigationController.delegate = self;
    UIPanGestureRecognizer *popRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePopRecognizer:)];
    [self.view addGestureRecognizer:popRecognizer];
    
    UIPanGestureRecognizer *popNavigationBarRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePopRecognizer:)];
    [self.navigationController.navigationBar addGestureRecognizer:popNavigationBarRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)handlePopRecognizer:(UIPanGestureRecognizer*)recognizer{
    
}

- (IBAction)touchOk:(id)sender {
    if(self.blockFirstBtnAction){
        self.blockFirstBtnAction();
    }
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)touchCancel:(id)sender {
    if(self.blockSecondBtnAction){
        self.blockSecondBtnAction();
    }
    [self dismissViewControllerAnimated:NO completion:nil];
}
@end
