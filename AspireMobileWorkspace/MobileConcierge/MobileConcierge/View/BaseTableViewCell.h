//
//  BaseTableViewCell.h
//  MobileConcierge
//
//  Created by Home on 5/9/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Common.h"
#import "Constant.h"
#import "NSString+Utis.h"
#import "AppColor.h"
#import "AppSize.h"
#import "UtilStyle.h"

@interface BaseTableViewCell : UITableViewCell
- (void) loadData;
@end
