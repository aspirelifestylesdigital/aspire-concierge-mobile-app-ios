//
//  ExploreDetailTableCel.h
//  MobileConcierge
//
//  Created by Chung Mai on 7/18/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"

@protocol ExploreDetailTableCellDelegate <NSObject>

- (void) shareUrl:(NSArray *)data;
- (void) touchAskConcierge;
- (void) seeDetailMapWithAddress:(NSString*)address;
- (void) gotoWebBrowser;
- (void)openURL:(NSURL *)url;

@end

@interface ExploreDetailTableCell : UITableViewCell
@property (weak, nonatomic) id<ExploreDetailTableCellDelegate> delegate;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *leadingConstraint;
@property (nonatomic, strong) NSString *itemID;

//Heard Info
@property (weak, nonatomic) IBOutlet UIImageView *headImg;
@property (weak, nonatomic) IBOutlet UIButton *btnAskConcierge;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (weak, nonatomic) IBOutlet UILabel *lblQuestion;

// Input data
@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic, strong) NSString *cityName;
@property (nonatomic, strong) NSString *questionName;
@property (nonatomic, strong) NSString *shareLink;

- (IBAction)touchAskConcierge:(id)sender;
- (IBAction)touchShare:(id)sender;
- (void)loadDataForHeader;

//Address Info
@property (weak, nonatomic) IBOutlet UILabel *lblAddress1;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress3;
@property (weak, nonatomic) IBOutlet UIButton *btnSeeMap;
@property (weak, nonatomic) IBOutlet UIButton *btnWebLink;
@property (weak, nonatomic) IBOutlet UIView *addressBottomMargin;

@property (nonatomic, strong) NSString *address1;
@property (nonatomic, strong) NSString *address3;
@property (nonatomic, strong) NSString *googleAddress;
@property (nonatomic, strong) NSString *webLink;

- (IBAction)touchSeeMap:(id)sender;
- (IBAction)touchWebLink:(id)sender;
- (void) loadDataForInfo;


//Insider Tips Info
@property (weak, nonatomic) IBOutlet UIView *insiderTipsTopMargin;
@property (weak, nonatomic) IBOutlet UIView *insiderTipsBottomMargin;
@property (weak, nonatomic) IBOutlet UIView *insiderTipsMiddleMargin;
@property (weak, nonatomic) IBOutlet UIView *insiderTipsHeaderView;
@property (weak, nonatomic) IBOutlet UILabel *lblInsiderTipsTitle;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblInsiderTipsDetail;
@property (strong, nonatomic) NSString *insiderTip;

- (void)loadDataForInsiderTip;

//Benefits Info
@property (weak, nonatomic) IBOutlet UIView *benefitTopMargin;
@property (weak, nonatomic) IBOutlet UIView *benefitBottomMargin;
@property (weak, nonatomic) IBOutlet UIView *benefitHeaderView;
@property (weak, nonatomic) IBOutlet UIImageView *imgBenefit;
@property (weak, nonatomic) IBOutlet UILabel *lblBenefitsTitle;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblBenefitsDetail;
@property (strong, nonatomic) NSString *benefits;

- (void)loadDataForBenefits;

//Price Info
@property (weak, nonatomic) IBOutlet UIView  *priceSeparateLine;
@property (weak, nonatomic) IBOutlet UIView  *priceTopMargin;
@property (weak, nonatomic) IBOutlet UIView  *priceBottomMargin;
@property (weak, nonatomic) IBOutlet UIView  *priceMainView;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceRangeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblCuisineTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceRange;
@property (weak, nonatomic) IBOutlet UILabel *lblCuisine;

@property (nonatomic, strong) NSString *priceRange;
@property (nonatomic, strong) NSString *cuisine;

- (void)loadDataForPrice;


//Description Info
@property (weak, nonatomic) IBOutlet UIView  *descriptionSeparateLine;
@property (weak, nonatomic) IBOutlet UIView  *descriptionTopMargin;
@property (weak, nonatomic) IBOutlet UIView  *descriptionBottomMargin;
@property (weak, nonatomic) IBOutlet UILabel *lblDecriptionTitle;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblDescriptionDetail;
@property (strong, nonatomic) NSString *cellDescription;

-(void)loadDataForDescription;

//Hours Info
@property (weak, nonatomic) IBOutlet UIView  *hoursSeparateLine;
@property (weak, nonatomic) IBOutlet UIView  *hoursTopMargin;
@property (weak, nonatomic) IBOutlet UIView  *hoursBottomMargin;
@property (weak, nonatomic) IBOutlet UILabel *lblHoursTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblHoursDetail;
@property (strong, nonatomic) NSString *workingHour;

- (void)loadDataForHours;


//Term Info
@property (weak, nonatomic) IBOutlet UIView  *termSeparateLine;
@property (weak, nonatomic) IBOutlet UIView  *termTopMargin;
@property (weak, nonatomic) IBOutlet UIView  *termBottomMargin;
@property (weak, nonatomic) IBOutlet UILabel *lblTermTitle;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblTermDetail;
@property (strong, nonatomic) NSString *termDetail;

-(void)loadDataForTermOfCondition;
@end
