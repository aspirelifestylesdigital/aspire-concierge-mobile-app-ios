//
//  CategoryCollectionViewCell.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
#import "THLabel.h"
@class CategoryItem;

@interface CategoryCollectionViewCell : UICollectionViewCell

@property(nonatomic, weak) IBOutlet UIImageView *categoryImg;
@property(nonatomic, weak) IBOutlet THLabel *name;
@property(nonatomic, weak) IBOutlet UIView *maskView;


-(void) initCellWithData:(CategoryItem *)item withDisableItem:(BOOL)isDisable;
-(void) setCellToDefault;

@end
