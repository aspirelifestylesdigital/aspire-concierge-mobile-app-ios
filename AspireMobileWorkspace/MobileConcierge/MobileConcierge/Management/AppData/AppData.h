//
//  AppData.h
//  MobileConcierge
//
//  Created by Home on 5/7/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CityItem.h"

@interface AppData : NSObject
+(NSDictionary *)getMenuItems;
+(void)storedTimeAcceptedPolicy;
+(NSTimeInterval)getTimeAcceptedPolicy;
+(BOOL)isAcceptedPolicyOverSixMonths;
+(void)setCreatedProfileSuccessful;
+(BOOL)isCreatedProfile;
+ (AppData *)getInstance;
-(NSArray *)getSelectionCityList;
-(NSArray *)getSelectionCategoryList;
-(NSArray *)getCityGuideList;
-(NSArray *)getSubCategoryItemList;
-(NSArray *)getDefaultAnswers;
-(BOOL) isCurrentCity;
-(CityItem *) getSelectedCity;
-(void)setCurrentCityWithCode:(NSString *)cityName;
-(BOOL) isUpdatingProfile;
@end
