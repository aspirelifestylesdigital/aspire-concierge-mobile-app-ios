//
//  AppData.m
//  MobileConcierge
//
//  Created by Home on 5/7/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "AppData.h"
#import <Foundation/Foundation.h>
#import "Constant.h"
#import "CityItem.h"
#import "CategoryItem.h"
#import "AnswerItem.h"
#import "CCAGeographicManager.h"
//#import <AspireApiFramework/AspireApiFramework.h>
@import AspireApiFramework;

@interface AppData()
{
    NSArray *cityItemList;
    NSArray *categoryItemList;
    NSArray *cityGuideList;
    NSArray *subCategoryItemList;
    NSArray *defaultAnswers;
    BOOL isUpdatingProfile;
    NSTimer* checkIsUpdatingData;
    NSString* lastSelectedCity;
}

@end
@implementation AppData

+ (AppData *)getInstance
{
    
    static AppData *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

-(NSArray *)getDefaultAnswers
{
    if(!defaultAnswers)
    {
        [self createDefaultAnswers];
    }
    
    return defaultAnswers;
}

-(void)createDefaultAnswers
{
    NSMutableArray *mutableDefaultAnswers = [[NSMutableArray alloc] init];
    AnswerItem *item = [[AnswerItem alloc] init];
    item.answerName = @"Charlie Palmer Steak (Midtown)";
    item.address3 = @"Midtown";
    item.offer2 = @"Complimentary Glass of Sparkling Wine for each guest";
    item.imageURL = @"https://assets.vipdesk.com/universal/images/diningnetwork/newyork/NYC_CHARLIE_PALMER_STEAK_FINAL.JPG";
    
    [mutableDefaultAnswers addObject:item];
    
    item = [[AnswerItem alloc] init];
    item.answerName = @"Megu (Chelsea)";
    item.address3 = @"Chelsea";
    item.offer2 = @"A Complimentary Appetizer or Dessert for the Table";
    item.imageURL = @"https://assets.vipdesk.com/universal/images/diningnetwork/newyork/NYC_MEGU_FINAL.JPG";
    
    [mutableDefaultAnswers addObject:item];
    
    defaultAnswers = mutableDefaultAnswers;
}

-(NSArray *)getSubCategoryItemList
{
    if(!subCategoryItemList)
    {
        [self createSubCategoryItemList];
    }
    
    return subCategoryItemList;
}

-(void)createSubCategoryItemList
{
    NSMutableArray *subCategories = [[NSMutableArray alloc] init];
    CategoryItem *category = [[CategoryItem alloc] init];
    category.categoryName = @"ACCOMMODATIONS";
    category.code = @"accommodations";
    category.categoryImg = [UIImage imageNamed:@"asset_cityguide"];
    category.categoryImgDetail = [UIImage imageNamed:@"asset_cityguide_detail"];
    [subCategories addObject:category];
    
    category = [[CategoryItem alloc] init];
    category.categoryName = @"BARS/CLUBS";
    category.code = @"bars/clubs";
    category.categoryImg = [UIImage imageNamed:@"bar_club_cityguide"];
    category.categoryImgDetail = [UIImage imageNamed:@"bar_club_cityguide_detail"];
    [subCategories addObject:category];
    
    category = [[CategoryItem alloc] init];
    category.categoryName = @"CULTURE";
    category.code = @"culture";
    category.categoryImg = [UIImage imageNamed:@"culture_cityguide"];
    category.categoryImgDetail = [UIImage imageNamed:@"culture_cityguide_detail"];
    [subCategories addObject:category];
    
    category = [[CategoryItem alloc] init];
    category.categoryName = @"DINING";
    category.code = @"dining";
    category.categoryImg = [UIImage imageNamed:@"dining_cityguide"];
    category.categoryImgDetail = [UIImage imageNamed:@"dining_cityguide_detail"];
    [subCategories addObject:category];
    
    category = [[CategoryItem alloc] init];
    category.categoryName = @"SHOPPING";
    category.code = @"shopping";
    category.categoryImg = [UIImage imageNamed:@"shopping_cityguide"];
    category.categoryImgDetail = [UIImage imageNamed:@"shopping_cityguide_detail"];
    [subCategories addObject:category];
    
    category = [[CategoryItem alloc] init];
    category.categoryName = @"SPAS";
    category.code = @"spas";
    category.categoryImg = [UIImage imageNamed:@"spa_wellness_cityguide"];
    category.categoryImgDetail = [UIImage imageNamed:@"spa_wellness_cityguide_detail"];
    [subCategories addObject:category];
    
    subCategoryItemList = subCategories;
}

-(NSArray *)getSelectionCityList
{
    if(!cityItemList)
    {
        [self createSelectionCityList];
        if(!cityGuideList)
        {
            [self createCityGuideList];
        }
        
        for(CityItem *item in cityItemList)
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cityCode == %@",item.name];
            NSArray *tempArray = [cityGuideList filteredArrayUsingPredicate:predicate];
            
            if(tempArray.count > 0)
            {
                item.ID = ((CityItem *)[tempArray objectAtIndex:0]).ID;
            }
            
        }
    }
    return cityItemList;
}

-(NSArray *)getSelectionCategoryList
{
    if(!categoryItemList)
    {
        [self createSelectionCategoryList];
    }
    return categoryItemList;
}

-(NSArray *)getCityGuideList
{
    if(!cityGuideList)
    {
        [self createCityGuideList];
    }
    return cityGuideList;
}


-(void)createCityGuideList
{
    NSMutableArray *cityGuides = [[NSMutableArray alloc] init];
//    CategoryItem *category = [[CategoryItem alloc] init];
//    category.ID = @"";
//    category.categoryName = @"Atlanta";
//    category.code = @"Atlanta";
//    category.categoryImg = [UIImage imageNamed:@"atlanta"];
//    [cityGuides addObject:category];
    
    CityItem *city = [[CityItem alloc] init];
    city.ID = @"6311";
    city.name = @"Boston";
    city.cityCode = @"Boston";
    city.image = [UIImage imageNamed:@"boston"];
    [cityGuides addObject:city];
    
    city = [[CityItem alloc] init];
    city.ID = @"6308";
    city.name = @"Chicago";
    city.cityCode = @"Chicago";
    city.image = [UIImage imageNamed:@"chicago"];
    [cityGuides addObject:city];
    
    city = [[CityItem alloc] init];
    city.ID = @"6445";
    city.name = @"Dallas";
    city.cityCode = @"Dallas";
    city.image = [UIImage imageNamed:@"dallas"];
    [cityGuides addObject:city];
    
    city = [[CityItem alloc] init];
    city.ID = @"6310";
    city.name = @"Las Vegas";
    city.cityCode = @"Las Vegas";
    city.image = [UIImage imageNamed:@"lasvegas"];
    [cityGuides addObject:city];
    
    city = [[CityItem alloc] init];
    city.ID = @"6314";
    city.name = @"London";
    city.cityCode = @"London";
    city.image = [UIImage imageNamed:@"london"];
    [cityGuides addObject:city];
    
    city = [[CityItem alloc] init];
    city.ID = @"6312";
    city.name = @"Los Angeles";
    city.cityCode = @"Los Angeles";
    city.image = [UIImage imageNamed:@"losangeles"];
    [cityGuides addObject:city];
    
    city = [[CityItem alloc] init];
    city.ID = @"6307";
    city.name = @"Miami";
    city.cityCode = @"Miami";
    city.image = [UIImage imageNamed:@"miami"];
    [cityGuides addObject:city];
    
    city = [[CityItem alloc] init];
    city.ID = @"6429";
    city.name = @"Montreal";
    city.cityCode = @"Montreal";
    city.image = [UIImage imageNamed:@"montreal"];
    [cityGuides addObject:city];
    
    city = [[CityItem alloc] init];
    city.ID = @"6306";
    city.name = @"New York";
    city.cityCode = @"New York";
    city.image = [UIImage imageNamed:@"new_york"];
    [cityGuides addObject:city];
    
    city = [[CityItem alloc] init];
    city.ID = @"6389";
    city.name = @"Orlando";
    city.cityCode = @"Orlando";
    city.image = [UIImage imageNamed:@"orlando"];
    [cityGuides addObject:city];
    
    city = [[CityItem alloc] init];
    city.ID = @"6315";
    city.name = @"Paris";
    city.cityCode = @"Paris";
    city.image = [UIImage imageNamed:@"paris"];
    [cityGuides addObject:city];
    
    city = [[CityItem alloc] init];
    city.ID = @"6458";
    city.name = @"Rome";
    city.cityCode = @"Rome";
    city.image = [UIImage imageNamed:@"rome"];
    [cityGuides addObject:city];
    
//    category = [[CategoryItem alloc] init];
//    category.ID = @"";
//    category.categoryName = @"San Diego";
//    category.code = @"San Diego";
//    category.categoryImg = [UIImage imageNamed:@"sandiego"];
//    [cityGuides addObject:category];
    
    city = [[CityItem alloc] init];
    city.ID = @"6313";
    city.name = @"San Francisco";
    city.cityCode = @"San Francisco";
    city.image = [UIImage imageNamed:@"sanfrancisco"];
    [cityGuides addObject:city];
    
    city = [[CityItem alloc] init];
    city.ID = @"6444";
    city.name = @"Seattle";
    city.cityCode = @"Seattle";
    city.image = [UIImage imageNamed:@"seattle"];
    [cityGuides addObject:city];
    
    city = [[CityItem alloc] init];
    city.ID = @"6428";
    city.name = @"Toronto";
    city.cityCode = @"Toronto";
    city.image = [UIImage imageNamed:@"toronto"];
    [cityGuides addObject:city];
    
    city = [[CityItem alloc] init];
    city.ID = @"6459";
    city.name = @"Vancouver";
    city.cityCode = @"Vancouver";
    city.image = [UIImage imageNamed:@"vancouver"];
    [cityGuides addObject:city];
    
    city = [[CityItem alloc] init];
    city.ID = @"6309";
    city.name = @"Washington, D.C.";
    city.cityCode = @"Washington, DC";
    city.image = [UIImage imageNamed:@"washington"];
    [cityGuides addObject:city];
    
    cityGuideList = cityGuides;
}

-(void)createSelectionCategoryList
{
    NSMutableArray *categories = [[NSMutableArray alloc] init];
    
    CategoryItem *category = [[CategoryItem alloc] init];
    category.categoryName = @"";
    category.code = @"all";
    category.categoryImg = [UIImage imageNamed:@"all_category"];
    category.categoriesForService = @[@"Flowers",@"Golf Merchandise",@"Wine",@"VIP Travel Services",@"Tickets", @"Specialty Travel"];
    [categories addObject:category];
    
    category = [[CategoryItem alloc] init];
    category.ID = @"2";
    category.categoryName = @"DINING";
    category.code = @"dining";
    category.categoryImg = [UIImage imageNamed:@"dining"];
    [categories addObject:category];
    
    category = [[CategoryItem alloc] init];
    category.ID = @"3";
    category.categoryName = @"Mastercard Travel & Lifestyles Services";
    category.code = @"mastercard travel";
    category.categoryImg = [UIImage imageNamed:@"mastercard_travel"];
    [categories addObject:category];
    
    category = [[CategoryItem alloc] init];
    category.categoryName = @"FLOWERS";
    category.code = @"flowers";
    category.categoryImg = [UIImage imageNamed:@"artboard"];
    category.categoriesForService = @[@"Flowers"];
    category.categoryIDs = @[@"15",@"934"];
    [categories addObject:category];
    
    
    category = [[CategoryItem alloc] init];
    category.categoryName = @"ARTS + \nCULTURE";
    category.code = @"art culture";
    category.categoryImg = [UIImage imageNamed:@"art_culture"];
    category.categoriesForService = @[@"Tickets"];
    [categories addObject:category];
    /*
    category = [[CategoryItem alloc] init];
    category.categoryName = @"HOTELS";
    category.code = @"hotels";
    category.categoryImg = [UIImage imageNamed:@"hotel"];
    [categories addObject:category];
    */
    
    category = [[CategoryItem alloc] init];
    category.categoryName = @"SHOPPING";
    category.code = @"shopping";
    category.categoryImg = [UIImage imageNamed:@"shopping"];
    category.categoriesForService = @[@"Golf Merchandise",@"Flowers",@"Wine"];
    category.categoryIDs = @[@"14",@"27",@"1247"];
    [categories addObject:category];
    
    category = [[CategoryItem alloc] init];
    category.categoryName = @"GOLF";
    category.code = @"golf";
    category.ID = @"7";
    category.categoryImg = [UIImage imageNamed:@"golf"];
    [categories addObject:category];
    
    category = [[CategoryItem alloc] init];
    category.categoryName = @"SPORTS";
    category.code = @"sports";
    category.categoryImg = [UIImage imageNamed:@"sport"];
    category.categoriesForService = @[@"Tickets"];
    category.categoryIDs = @[@"28"];
    [categories addObject:category];
    
    /*
    category = [[CategoryItem alloc] init];
    category.categoryName = @"LEISURE";
    category.code = @"leisure";
    category.categoryImg = [UIImage imageNamed:@"leisure"];
    [categories addObject:category];
    */
    
    category = [[CategoryItem alloc] init];
    category.ID = @"12";
    category.categoryName = @"PRICELESS \nCITIES";
    category.code = @"priceless city";
    category.categoryImg = [UIImage imageNamed:@"priceless"];
    [categories addObject:category];
    
    category = [[CategoryItem alloc] init];
    category.categoryName = @"TRAVEL";
    category.code = @"travel";
    category.categoryImg = [UIImage imageNamed:@"travel"];
    category.categoriesForService = @[@"VIP Travel Services"];
    category.categoryIDs = @[@"13",@"23",@"1246",@"1250"];
    [categories addObject:category];
    
    
    
    category = [[CategoryItem alloc] init];
    category.categoryName = @"EXPERIENCES";
    category.code = @"experiences";
    category.categoryImg = [UIImage imageNamed:@"experiences"];
    category.categoriesForService = @[@"Specialty Travel"];
    category.categoryIDs = @[@"1023",@"1138",@"1139",@"1244",@"1245"];
    [categories addObject:category];
    /*
    category = [[CategoryItem alloc] init];
    category.categoryName = @"TOURS";
    category.code = @"tours";
    category.categoryImg = [UIImage imageNamed:@"tour"];
    category.categoriesForService = @[@"VIP Travel Services"];
    [categories addObject:category];
     */
    
    category = [[CategoryItem alloc] init];
    category.ID = @"5";
    category.categoryName = @"CITY GUIDE";
    category.code = @"city guide";
    category.categoryImg = [UIImage imageNamed:@"city_guide"];
    [categories addObject:category];
    
    /*
    category = [[CategoryItem alloc] init];
    category.categoryName = @"NIGHTLIFE";
    category.code = @"nightlife";
    category.categoryImg = [UIImage imageNamed:@"nightlife"];
//    category.categoriesForService = @[@"Specialty Travel"];
    [categories addObject:category];
     */
    
    categoryItemList = categories;
}

-(NSArray *)createSubCategoriesWithIDs:(NSArray *)categoryIDs
{
    NSMutableArray *subCategories = [[NSMutableArray alloc] init];
    for(NSInteger i = 0; i< categoryIDs.count; i++)
    {
        CategoryItem *item = [[CategoryItem alloc] init];
        item.ID = [categoryIDs objectAtIndex:i];
        item.supperCategoryID = @"80";
        switch (i) {
                /*
            case 0:
                item.categoryName = @"ACCOMMODATIONS";
                item.code = @"accommodation";
                item.categoryImg =  [UIImage imageNamed:@"asset_cityguide"];
                item.categoryImgDetail =  [UIImage imageNamed:@"asset_cityguide_detail"];
                break;
                 */
            case 0:
                item.categoryName = @"DINING";
                item.code = @"dining";
                item.categoryImg =  [UIImage imageNamed:@"dining_cityguide"];
                item.categoryImgDetail =  [UIImage imageNamed:@"dining_cityguide_detail"];
                break;
            case 1:
                item.categoryName = @"BARS/CLUBS";
                item.code = @"bars";
                item.categoryImg =  [UIImage imageNamed:@"bar_club_cityguide"];
                item.categoryImgDetail =  [UIImage imageNamed:@"bar_club_cityguide_detail"];
                break;
            case 2:
                item.categoryName = @"SPAS";
                item.code = @"spas";
                item.categoryImg =  [UIImage imageNamed:@"spa_wellness_cityguide"];
                item.categoryImgDetail =  [UIImage imageNamed:@"spa_wellness_cityguide_detail"];
                break;
            case 3:
                item.categoryName = @"SHOPPING";
                item.code = @"shopping";
                item.categoryImg =  [UIImage imageNamed:@"shopping_cityguide"];
                item.categoryImgDetail =  [UIImage imageNamed:@"shopping_cityguide_detail"];
                break;
            case 4:
                item.categoryName = @"CULTURE";
                item.code = @"culture";
                item.categoryImg =  [UIImage imageNamed:@"culture_cityguide"];
                item.categoryImgDetail =  [UIImage imageNamed:@"culture_cityguide_detail"];
                break;
            
                
            default:
                break;
        }
        [subCategories addObject:item];
        
    }
    return subCategories;
}

-(void) createSelectionCityList
{
    NSMutableArray *cityLst = [[NSMutableArray alloc] init];
    /*
    CityItem *city = [[CityItem alloc] init];
    city.name = @"Africa";
    city.regionCode = @"Africa";
    city.image = [UIImage imageNamed:@"africa"];
    [cityLst addObject:city];
     */
    
    CityItem *city = [[CityItem alloc] init];
    city.name = @"Asia Pacific";
    city.regionCode = @"Asia Pacific";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"asia_pacific"];
    city.diningID = @"6523";
    [cityLst addObject:city];
    
    city = [[CityItem alloc] init];
    city.name = @"Atlanta";
    city.cityCode = @"Atlanta";
    city.stateCode = @"Georgia";
    city.countryCode = @"USA";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"atlanta"];
    city.diningID = @"6514";
    [cityLst addObject:city];
    
    city = [[CityItem alloc] init];
    city.name = @"Boston";
    city.cityCode = @"Boston";
    city.stateCode = @"Massachusetts";
    city.countryCode = @"USA";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"boston"];
    city.diningID = @"6422";
    NSArray *subCategoryId = @[@"6357",@"6358",@"6359",@"6360",@"6362"];
    city.subCategories = [self createSubCategoriesWithIDs:subCategoryId];
    [cityLst addObject:city];
    
    city = [[CityItem alloc] init];
    city.name = @"Cancun";
    city.cityCode = @"Cancun";
    city.countryCode = @"Mexico";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"cancun"];
    city.diningID = @"6586";
    [cityLst addObject:city];
    
    /*
    city = [[CityItem alloc] init];
    city.name = @"Caribbean";
    city.regionCode = @"Caribbean ";
    city.image = [UIImage imageNamed:@"caribbean"];
    [cityLst addObject:city];
     */
    
    city = [[CityItem alloc] init];
    city.name = @"Chicago";
    city.cityCode = @"Chicago";
    city.stateCode = @"Illinois";
    city.countryCode = @"USA";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"chicago"];
    city.diningID = @"6317";
    subCategoryId = @[@"6339",@"6340",@"6341",@"6342",@"6344"];
    city.subCategories = [self createSubCategoriesWithIDs:subCategoryId];

    [cityLst addObject:city];
    
    
    city = [[CityItem alloc] init];
    city.name = @"Cleveland";
    city.cityCode = @"Cleveland";
    city.stateCode = @"Ohio";
    city.countryCode = @"USA";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"cleveland"];
    city.diningID = @"6318";
    [cityLst addObject:city];
    
    city = [[CityItem alloc] init];
    city.name = @"Dallas";
    city.cityCode = @"Dallas";
    city.stateCode = @"Texas";
    city.countryCode = @"USA";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"dallas"];
    city.diningID = @"6423";
    subCategoryId = @[@"6452",@"6453",@"6454",@"6455",@"6457"];
    city.subCategories = [self createSubCategoriesWithIDs:subCategoryId];
    [cityLst addObject:city];
    
    /*
    city = [[CityItem alloc] init];
    city.name = @"Europe";
    city.regionCode = @"Europe";
    city.image = [UIImage imageNamed:@"europe"];
    [cityLst addObject:city];
     */
    
    city = [[CityItem alloc] init];
    city.name = @"Hawaii";
    city.cityCode = @"Honolulu";
    city.countryCode = @"USA";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"hawai"];
    city.diningID = @"6473";
    [cityLst addObject:city];

    /*
    city = [[CityItem alloc] init];
    city.name = @"Latin American";
    city.regionCode = @"Latin America";
    city.image = [UIImage imageNamed:@"latin_america"];
    [cityLst addObject:city];
     */
    
    city = [[CityItem alloc] init];
    city.name = @"Las Vegas";
    city.cityCode = @"Las Vegas";
    city.stateCode = @"Nevada";
    city.countryCode = @"USA";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"lasvegas"];
    city.diningID = @"6319";
    subCategoryId = @[@"6351",@"6352",@"6353",@"6354",@"6356"];
    city.subCategories = [self createSubCategoriesWithIDs:subCategoryId];
    [cityLst addObject:city];
    
    
    city = [[CityItem alloc] init];
    city.name = @"London";
    city.cityCode = @"London";
    city.countryCode = @"United Kingdom";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"london"];
    city.diningID = @"6443";
    subCategoryId = @[@"6375",@"6376",@"6377",@"6378",@"6380"];
    city.subCategories = [self createSubCategoriesWithIDs:subCategoryId];
    [cityLst addObject:city];
    
    city = [[CityItem alloc] init];
    city.name = @"Los Angeles";
    city.cityCode = @"Los Angeles";
    city.stateCode = @"California";
    city.countryCode = @"USA";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"losangeles"];
    city.diningID = @"6320";
    subCategoryId = @[@"6363",@"6364",@"6365",@"6366",@"6368"];
    city.subCategories = [self createSubCategoriesWithIDs:subCategoryId];
    [cityLst addObject:city];
    
    city = [[CityItem alloc] init];
    city.name = @"Miami";
    city.cityCode = @"Miami";
    city.stateCode = @"Florida";
    city.countryCode = @"USA";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"miami"];
    city.diningID = @"6321";
    subCategoryId = @[@"6333",@"6334",@"6335",@"6336",@"6338"];
    city.subCategories = [self createSubCategoriesWithIDs:subCategoryId];
    [cityLst addObject:city];
    
    /*
    city = [[CityItem alloc] init];
    city.name = @"Middle East";
    city.regionCode = @"Middle East";
    city.image = [UIImage imageNamed:@"middle_east"];
    [cityLst addObject:city];
     */
    
    city = [[CityItem alloc] init];
    city.name = @"Milwaukee";
    city.cityCode = @"Milwaukee";
    city.stateCode = @"Wisconsin";
    city.countryCode = @"USA";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"milwaukee"];
    city.diningID = @"6322";
    [cityLst addObject:city];
    
    city = [[CityItem alloc] init];
    city.name = @"Montreal";
    city.cityCode = @"Montreal";
    city.stateCode = @"Quebec";
    city.countryCode = @"Canada";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"montreal"];
    city.diningID = @"6424";
    subCategoryId = @[@"6436",@"6438",@"6439",@"6440",@"6437"];
    city.subCategories = [self createSubCategoriesWithIDs:subCategoryId];
    [cityLst addObject:city];
    
    city = [[CityItem alloc] init];
    city.name = @"Napa/Sonoma";
    city.stateCode = @"California";
    city.countryCode = @"USA";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"napa_sonoma"];
    city.diningID = @"6483";
    [cityLst addObject:city];
    
    city = [[CityItem alloc] init];
    city.name = @"New Orleans";
    city.cityCode = @"New Orleans";
    city.stateCode = @"Louisiana";
    city.countryCode = @"USA";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"new_orlean"];
    city.diningID = @"6515";
    [cityLst addObject:city];
    
    
    city = [[CityItem alloc] init];
    city.name = @"New York";
    city.cityCode = @"New York";
    city.stateCode = @"New York";
    city.countryCode = @"USA";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"new_york"];
    city.diningID = @"6323";
    subCategoryId = @[@"6326",@"6327",@"6328",@"6329",@"6331"];
    city.subCategories = [self createSubCategoriesWithIDs:subCategoryId];
    [cityLst addObject:city];
    
    city = [[CityItem alloc] init];
    city.name = @"Orlando";
    city.cityCode = @"Orlando";
    city.stateCode = @"Florida";
    city.countryCode = @"USA";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"orlando"];
    city.diningID = @"6477";
    subCategoryId = @[@"6395",@"6398",@"6405",@"6408",@"6418"];
    city.subCategories = [self createSubCategoriesWithIDs:subCategoryId];
    [cityLst addObject:city];
    
    city = [[CityItem alloc] init];
    city.name = @"Paris";
    city.cityCode = @"Paris";
    city.countryCode = @"France";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"paris"];
    city.diningID = @"6476";
    subCategoryId = @[@"6381",@"6382",@"6383",@"6384",@"6386"];
    city.subCategories = [self createSubCategoriesWithIDs:subCategoryId];
    [cityLst addObject:city];
    
    city = [[CityItem alloc] init];
    city.name = @"Philadelphia";
    city.cityCode = @"Philadelphia";
    city.stateCode = @"Pennsylvania";
    city.countryCode = @"USA";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"philadelphia"];
    city.diningID = @"6425";
    [cityLst addObject:city];
    
    city = [[CityItem alloc] init];
    city.name = @"Rome";
    city.cityCode = @"Rome";
    city.countryCode = @"Italy";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"rome"];
    city.diningID = @"6475";
    subCategoryId = @[@"6460",@"6461",@"6462",@"6471",@"6472"];
    city.subCategories = [self createSubCategoriesWithIDs:subCategoryId];
    [cityLst addObject:city];
    
    /*
    city = [[CityItem alloc] init];
    city.name = @"San Diego";
    city.cityCode = @"San Diego";
    city.stateCode = @"California";
    city.countryCode = @"USA";
    city.image = [UIImage imageNamed:@"sandiego"];
    [city generateNewDataForCity];
    [cityLst addObject:city];
     */
    
    city = [[CityItem alloc] init];
    city.name = @"San Francisco";
    city.cityCode = @"San Francisco";
    city.stateCode = @"California";
    city.countryCode = @"USA";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"sanfrancisco"];
    city.diningID = @"6324";
    subCategoryId = @[@"6369",@"6370",@"6371",@"6372",@"6374"];
    city.subCategories = [self createSubCategoriesWithIDs:subCategoryId];
    [cityLst addObject:city];
    
    city = [[CityItem alloc] init];
    city.name = @"Seattle";
    city.cityCode = @"Seattle";
    city.stateCode = @"Washington";
    city.countryCode = @"USA";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"seattle"];
    city.diningID = @"6426";
    subCategoryId = @[@"6446",@"6447",@"6448",@"6449",@"6451"];
    city.subCategories = [self createSubCategoriesWithIDs:subCategoryId];
    [cityLst addObject:city];
    
    city = [[CityItem alloc] init];
    city.name = @"Toronto";
    city.cityCode = @"Toronto";
    city.stateCode = @"Ontario";
    city.countryCode = @"Canada";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"toronto"];
    city.diningID = @"6427";
    subCategoryId = @[@"6430",@"6432",@"6433",@"6434",@"6431"];
    city.subCategories = [self createSubCategoriesWithIDs:subCategoryId];
    [cityLst addObject:city];
    
    city = [[CityItem alloc] init];
    city.name = @"Vancouver";
    city.cityCode = @"Montreal";
    city.stateCode = @"British Columbia";
    city.countryCode = @"Canada";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"vancouver"];
    city.diningID = @"6442";
    subCategoryId = @[@"6464",@"6468",@"6470",@"6465",@"6469"];
    city.subCategories = [self createSubCategoriesWithIDs:subCategoryId];
    [cityLst addObject:city];
    
    city = [[CityItem alloc] init];
    city.name = @"Washington, D.C.";
    city.cityCode = @"Washington DC";
    city.countryCode = @"USA";
    city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
    city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
    city.image = [UIImage imageNamed:@"washington"];
    city.diningID = @"6325";
    subCategoryId = @[@"6345",@"6346",@"6347",@"6348",@"6350"];
    city.subCategories = [self createSubCategoriesWithIDs:subCategoryId];
    [cityLst addObject:city];
    
    NSArray *sortedArray = [cityLst sortedArrayUsingComparator:^NSComparisonResult(CityItem *obj1, CityItem *obj2){ return [obj1.name compare:obj2.name];}];
    
//    CityItem *allCity = [[CityItem alloc] init];
//    allCity.name = @"All";
//    allCity.regionCode = @"all";
//    allCity.image = [UIImage imageNamed:@"all_cityselection"];
    
//    NSMutableArray *allItems = [[NSMutableArray alloc] init];
//    [allItems addObject:allCity];
//    [allItems addObjectsFromArray:sortedArray];
    
    cityItemList = sortedArray;
}
+(NSDictionary *)getMenuItems
{
    return @{@"0":@[NSLocalizedString(@"home_menu_title", nil),
                    NSLocalizedString(@"ask_concierge_menu_title", nil),
                    NSLocalizedString(@"explore_menu_title", nil)],
             @"1":@[NSLocalizedString(@"my_profile_menu_title", nil),
                    NSLocalizedString(@"password_menu_title", nil)
//                    NSLocalizedString(@"my_favorites_menu_title", nil),
//                    NSLocalizedString(@"my_requests_menu_title", nil)
                    ],
             @"2":@[NSLocalizedString(@"about_this_app_menu_title", nil),
                    NSLocalizedString(@"privacy_policy_menu_title", nil),
                    NSLocalizedString(@"terms_of_use_menu_title", nil),
                    NSLocalizedString(@"sign_out", nil)]
             };
    
}

+(void)storedTimeAcceptedPolicy
{
    [[NSUserDefaults standardUserDefaults] setDouble:[[NSDate date] timeIntervalSince1970] forKey:@"TimeAcceptedPolicy"];
    [[NSUserDefaults standardUserDefaults]  synchronize];
}

+(void)setCreatedProfileSuccessful
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ProfileCreatedSuccessful"];
    [[NSUserDefaults standardUserDefaults]  synchronize];
}


+(BOOL)isCreatedProfile
{
    return [User isValid];
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"ProfileCreatedSuccessful"];
}

+(NSTimeInterval)getTimeAcceptedPolicy
{
    return [[NSUserDefaults standardUserDefaults] doubleForKey:@"TimeAcceptedPolicy"];
}

+(BOOL)isAcceptedPolicyOverSixMonths
{
    return (([[NSDate date] timeIntervalSince1970] - [AppData getTimeAcceptedPolicy]) > SIX_MONTH_SECONDS_TIME);
}


-(void)setCurrentCityWithCode:(NSString *)cityName
{
    
    if([[self getSelectedCity].name isEqualToString:cityName]) return;
    
    [[NSUserDefaults standardUserDefaults] setValue:cityName forKey:CURRENT_CITY_CODE];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    lastSelectedCity = cityName;
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:NOTIFICATION_NAME_ISUPDATINGPROFILE]) {
        [checkIsUpdatingData invalidate];
        checkIsUpdatingData = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(queueWaitUpdateLastSelectedCity) userInfo:nil repeats:true];
        return;
    }
    
    [checkIsUpdatingData invalidate];
    
    NSMutableDictionary *userDict = [[NSMutableDictionary alloc] init];
    UserObject* temp = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([UserObject class])];
    [userDict setValue:B2C_ConsumerKey forKey:@"referenceName"];
    [userDict setValue:temp.firstName forKey:@"FirstName"];
    [userDict setValue:temp.lastName forKey:@"LastName"];
    [userDict setValue:temp.mobileNumber forKey:@"MobileNumber"];
    [userDict setValue:temp.email forKey:@"Email"];
    [userDict setValue:[[SessionData shareSessiondata] BINNumber] forKey:APP_USER_PREFERENCE_Passcode];
    [userDict setValue:temp.zipCode forKey:@"ZipCode"];
    [userDict setValue:@"Unknown" forKey:@"City"];
    [userDict setValue:@"USA" forKey:@"Country"];
    [userDict setObject:[[User current] isGetNewsletter] ? @"ON" : @"OFF" forKey:APP_USER_PREFERENCE_Newsletter];
    if([[User current] isGetNewsletterDate].length > 0)
        [userDict setObject:[[User current] isGetNewsletterDate] forKey:APP_USER_PREFERENCE_Newsletter_Date];
    [userDict setValue:cityName forKey:APP_USER_PREFERENCE_Selected_City];
    [userDict setObject:[[SessionData shareSessiondata] CurrentPolicyVersion] forKey:APP_USER_PREFERENCE_Policy_Version];
    
    isUpdatingProfile = true;
    typeof(self) __weak _self = self;
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NAME_ISUPDATINGPROFILE object:nil userInfo:@{@"isUpdating":@true}];
    [ModelAspireApiManager updateProfileCurrentUserWithUserInfo:userDict completion:^(NSError *error) {
        if (!_self) return;
        typeof(self) __self = _self;
        __self->isUpdatingProfile = false;
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NAME_ISUPDATINGPROFILE object:nil userInfo:@{@"isUpdating":@false}];
        [[SessionData shareSessiondata] setUserObject:(UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([UserObject class])]];
    }];
}

- (void) queueWaitUpdateLastSelectedCity {
    #if DEBUG
    NSLog(@"%s => wait %@",__PRETTY_FUNCTION__,lastSelectedCity);
#endif
    [self setCurrentCityWithCode:lastSelectedCity];
}

- (BOOL)isUpdatingProfile {
    return isUpdatingProfile;
}

-(BOOL)isCurrentCity
{
    if([[NSUserDefaults standardUserDefaults] valueForKey:CURRENT_CITY_CODE])
    {
        return YES;
    }
    return NO;
}

-(CityItem *)getSelectedCity
{
    BOOL shouldUseSelectedCityFromUser = false;
    if([[[[NSUserDefaults standardUserDefaults] dictionaryRepresentation] allKeys] containsObject:NOTIFICATION_NAME_ISUPDATINGPROFILE]) {
        shouldUseSelectedCityFromUser = ![[NSUserDefaults standardUserDefaults] boolForKey:NOTIFICATION_NAME_ISUPDATINGPROFILE];
    }
    NSString *currentCityCode = [[NSUserDefaults standardUserDefaults] valueForKey:CURRENT_CITY_CODE];
    if (!isUpdatingProfile && [User isValid] && shouldUseSelectedCityFromUser) {
        UserObject* temp = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([UserObject class])];
        currentCityCode = temp.selectedCity;
    }
    if(currentCityCode)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@",currentCityCode];
        NSArray *cities = [self getSelectionCityList];
        NSArray *tempArray = [cities filteredArrayUsingPredicate:predicate];
        if(tempArray.count > 0)
        {
            return [tempArray objectAtIndex:0];
        }
    }
    return nil;
}
@end
