//
//  WSB2CGetRequestToken.m
//  ALC
//
//  Created by Anh Tran on 11/1/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CGetRequestToken.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "BaseResponseObject.h"
#import "Common.h"
#import "NSDictionary+SBJSONHelper.h"
#import "AppDelegate.h"
#import "UserObject.h"
#import "EnumConstant.h"
#import "RequestBase.h"

@interface WSB2CGetRequestToken (){

}

@end

@implementation WSB2CGetRequestToken
-(void) getRequestToken{
    self.subTask = WS_ST_NONE;
    self.task = WS_GET_REQUEST_TOKEN;
    if(!isNetworkAvailable())
    {
        request = [[RequestBase alloc] init];
        request.requestURL = [MCD_API_URL stringByAppendingString:GetRequestToken];;
        request.parameterDict = [self buildRequestParams];
        request.requestMethod = POST;
        [self showNoNetWorkAlertForRetryWithData:request];
    }else{
        [self processRequestToken];
    }
}

-(void)processRequestToken
{
    NSString* url = [MCD_API_URL stringByAppendingString:GetRequestToken];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [manager.requestSerializer setValue:B2C_ConsumerKey forHTTPHeaderField:@"ConsumerKey"];
    [manager.requestSerializer setValue:B2C_ConsumerSecret forHTTPHeaderField:@"ConsumerSecret"];
     manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [self setCertificationForRequest:manager withUrlString:url];
    __weak typeof(self) weakSelf = self;
    
    self.servicetask = [manager GET:url parameters:[weakSelf buildRequestParams] progress:nil success:^(NSURLSessionDataTask * _Nonnull aftask, id  _Nullable responseObject) {
        [weakSelf processDataResults:responseObject forTask:weakSelf.task forSubTask:weakSelf.subTask returnFormat:400];
        [manager invalidateSessionCancelingTasks:YES];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [weakSelf processDataResultWithError:error];
        [manager invalidateSessionCancelingTasks:YES];
    }];
}
-(void) processDataResults:(NSDictionary*)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat
{
    
    if(jsonResult){
        NSString *token = [jsonResult stringForKey:@"RequestToken"];
        if(token!=nil && token.length > 0){
            _requestToken = token;
//            _onlineMemberId = [jsonResult stringForKey:@"OnlineMemberId"];
            if(self.delegate)
                [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:nil withErrorCode:400 errorMessage:@""];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}

-(void)processDataResultWithError:(NSError *)error
{
    [self.delegate loadDataFailFrom:nil withErrorCode:error.code];
}

-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    [dictKeyValues setObject:[[SessionData shareSessiondata] OnlineMemberID] forKey:@"OnlineMemberId"];
    [dictKeyValues setObject:B2C_Callback_Url forKey:@"CallbackURL"];
    [dictKeyValues setObject:[[SessionData shareSessiondata] UUID] forKey:@"MemberDeviceId"];
    
    return dictKeyValues;
}

-(void)timerCheckNetwork:(NSTimer *)timer
{
    timeCounter += 1;
    RequestBase *requestTimer = (RequestBase*)timer.userInfo;
    if(ISLOGINGINFO)
    {
        NSLog(@"%@", timer.userInfo);
    }
    
    if(timeCounter == 16)
    {
        [timer invalidate];
        timeCounter = 0;
        [self showNoNetWorkAlertForRetryWithData:requestTimer];
    }
    else{
        if(isNetworkAvailable())
        {
            [timer invalidate];
            timeCounter = 0;
            [self processRequestToken];
        }
    }
}
@end
