//
//  WSB2CGetTiles.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/6/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSB2CGetTiles.h"
#import "TileResponseObject.h"

@implementation WSB2CGetTiles

-(void) loadTilesForCategory
{
    // Logic for check no searching result
    if(self.searchText)
    {
        self.pageIndex = 1;
    }
    NSString* url = [B2C_API_URL stringByAppendingString:GetTiles];
    [self POST:url withParams:[self buildRequestParams]];
}


-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    
    // cause of security scanning, not allow get and set password directly
    NSString* userPass = B2C_PW;
    [dictKeyValues setObject:B2C_SUBDOMAIN forKey:@"subDomain"];
    [dictKeyValues setObject:userPass forKey:@"password"];
    NSData *json = [NSJSONSerialization dataWithJSONObject:self.categories options:0 error:nil];
    NSArray *array = [NSJSONSerialization JSONObjectWithData:json options:0 error:nil];
    [dictKeyValues setObject:array forKey:@"categories"];
    return dictKeyValues;
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        TileResponseObject *response = [[TileResponseObject alloc] init];
        response.categoryCode = self.categoryCode;
        response.categoryName = self.categoryName;
        response.searchText = self.searchText;
        response.currentCategory = self.currentCategory;
        response.hasOffer = self.hasOffer;
        response.task = self.task;
        response = [response initFromDict:jsonResult];
        if(self.delegate != nil && response.isSuccess){
            self.data = [response data];
            self.isHasNextPage = NO;//CCA always get full data at one time
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:nil withErrorCode:400 errorMessage:response.message];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}

-(void)processDataResultWithError:(NSError *)error
{
    [self.delegate loadDataFailFrom:nil withErrorCode:error.code];
}

@end
