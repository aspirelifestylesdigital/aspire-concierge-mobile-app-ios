//
//  WSB2CBase.h
//  ALC
//
//  Created by Anh Tran on 11/1/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSBase.h"
#import "CategoryItem.h"
#import "AFHTTPSessionManager.h"
#import "RequestBase.h"

@interface WSB2CBase : WSBase <DataLoadDelegate>
{
    NSInteger timeCounter;
    RequestBase *request;
}

@property (nonatomic, strong) NSString *categoryName;
@property(strong, nonatomic) NSString *categoryCode;
@property(strong, nonatomic) CategoryItem *currentCategory;

+ (AFHTTPSessionManager*) manager;

-(void)timerCheckNetwork:(NSTimer *)timer;
- (void) showNoNetWorkAlertForRetryWithData:(RequestBase *)currentRequest;
-(void)setCertificationForRequest:(AFHTTPSessionManager *)manager withUrlString:(NSString *)url;
@end
