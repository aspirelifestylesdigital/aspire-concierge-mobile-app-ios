//
//  WSB2CCreateNewConciergeCase.m
//  MobileConcierge
//
//  Created by user on 5/15/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSB2CCreateConciergeCase.h"
#import "CreateConciergeCaseResponseObject.h"
#import "UserObject.h"

@implementation WSB2CCreateConciergeCase
-(void) askConciergeWithMessage:(NSDictionary*)dict
{
    NSString* url = [B2C_API_SERVICES stringByAppendingString:POSTCreateNewConciergeCase];
    
    [self POST:url withParams:[self buildRequestParamsWithMessage:dict]];
    
    // return response
}
-(NSMutableDictionary*) buildRequestParamsWithMessage:(NSDictionary*)dict{
    NSMutableDictionary *contentDict = [[NSMutableDictionary alloc] init];
    
    
    [contentDict setObject:[self configurationAccessTokenDict] forKey:@"forThisExampleImplementationOnly_ConfigurationAccessToken"];
    [contentDict setObject:[self newCaseRequestDictWithMessage:dict] forKey:@"newCaseRequest"];
    return contentDict;
}

- (NSMutableDictionary *)newCaseRequestDictWithMessage:(NSDictionary*)dict{
    UserObject *userInfo =[[SessionData shareSessiondata] UserObject];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[self configurationAccessTokenDict] forKey:@"accessToken"];
    
    [params setValue:[NSNull null] forKey:@"programId"];
    [params setValue:[[SessionData shareSessiondata] OnlineMemberID] forKey:@"memberId"];
    [params setValue:userInfo.firstName forKey:@"firstName"];
    [params setValue:userInfo.lastName forKey:@"lastName"];
    [params setValue:userInfo.mobileNumber forKey:@"phoneNumber"];
    [params setValue:userInfo.email forKey:@"emailAddress"];
    [params setValue:dict[@"requestCity" ] forKey:@"requestCity"];
    [params setValue:dict[@"requestType"] forKey:@"requestType"];
    
    NSString *requestDetail = @"";

    if ([dict[@"phone"] boolValue] == YES && [dict[@"mail"] boolValue] == YES) {
        requestDetail = [requestDetail stringByAppendingString:[NSString stringWithFormat:@"Respond by %@ or Respond by %@\n\n",userInfo.mobileNumber,userInfo.email]];
    } else {
        if ([dict[@"phone"] boolValue] == YES) {
            requestDetail = [requestDetail stringByAppendingString:[NSString stringWithFormat:@"Respond by %@\n\n",userInfo.mobileNumber]];
        }else if ([dict[@"mail"] boolValue] == YES) {
            requestDetail = [requestDetail stringByAppendingString:[NSString stringWithFormat:@"Respond by %@\n\n",userInfo.email]];
        }
    }
    requestDetail = [requestDetail stringByAppendingString:dict[@"message"]];
    [params setValue:requestDetail forKey:@"requestDetails"];

    return params;
}
- (NSMutableDictionary*)configurationAccessTokenDict{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    NSDictionary *keyDict = @{@"key":@"4ef83516-67fb-4bad-8ed4-879f575661de"};
    NSString* userPass = B2C_PW;
    [dict setObject:keyDict forKey:@"licenseKey"];
    [dict setValue:B2C_SUBDOMAIN forKey:@"applicationName"];
    [dict setValue:userPass forKey:@"programPassword"];
    
    return dict;
}

- (NSString*) getJSONStringFromArray:(NSMutableArray*)array{
    NSError* error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:&error];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}
-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        CreateConciergeCaseResponseObject *response = [[CreateConciergeCaseResponseObject alloc] initFromDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && response.isSuccess){
            self.data = [response data];
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:nil withErrorCode:400 errorMessage:response.message];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}

-(void)processDataResultWithError:(NSError *)error
{
    [self.delegate loadDataFailFrom:nil withErrorCode:error.code];
}
@end
