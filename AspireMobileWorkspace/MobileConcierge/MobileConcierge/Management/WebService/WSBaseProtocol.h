//
//  WSBaseProtocol.h
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/19/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DataLoadDelegate;
@protocol WSBaseProtocol <NSObject>

@property (atomic, strong)  NSURLSessionDataTask *servicetask;
@property (nonatomic, weak) id<DataLoadDelegate>delegate;
@property (nonatomic, assign) enum WSTASK task;
@property (nonatomic, assign) enum WS_SUB_TASK subTask;

// Dispatch message to server
-(void)POST:(NSString*) url withParams:(NSMutableDictionary*)params;
-(void)GET:(NSString*) url withParams:(NSMutableDictionary*)params;
-(void)cancelRequest;

// Reload view
-(void) processDataResults:(NSData*)data forTask:(NSInteger)task forSubTask:(NSInteger)subTask returnFormat:(NSInteger)returnFormat;
-(void) processDataResultWithError:(NSError *)errorCode;

// Authentication
-(void)startAPIAfterAuthenticate;
@end
