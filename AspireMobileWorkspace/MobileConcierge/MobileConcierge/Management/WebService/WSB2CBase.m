//
//  WSB2CBase.m
//  ALC
//
//  Created by Anh Tran on 11/1/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"
#import "WSB2CRefreshToken.h"
#import "WSB2CGetRequestToken.h"
#import "WSB2CGetAccessToken.h"
#import "AppDelegate.h"
#import "UserObject.h"
#import "BaseResponseObject.h"
#import "Common.h"
#import "SBJson.h"
#import "Constant.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "Common.h"
#import "AlertViewController.h"
#import "EnumConstant.h"
#import "RequestBase.h"

@interface WSB2CBase(){
    WSB2CRefreshToken *wsRefreshToken;
    WSB2CGetRequestToken *wsRequestToken;
    WSB2CGetAccessToken *wsGetAccessToken;
    UserObject *loggedInUser;
    
    NSTimer *retryTimer;
}

@end

@implementation WSB2CBase

+ (AFHTTPSessionManager*) manager
{
    static dispatch_once_t onceToken;
    static AFHTTPSessionManager *manager = nil;
    dispatch_once(&onceToken, ^{
        manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        manager.requestSerializer =  [AFJSONRequestSerializer serializer];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        AFJSONResponseSerializer *serializer = [AFJSONResponseSerializer serializer];
        [serializer setRemovesKeysWithNullValues:YES];
        [manager setResponseSerializer:serializer];
    });
    
    return manager;
}


-(void)refreshTokenAPI:(enum WSTASK)aTask
{
    wsRefreshToken = [[WSB2CRefreshToken alloc] init];
    wsRefreshToken.delegate = self;
    [wsRefreshToken refreshAccessToken];
}

-(void)requestToken
{
    if(loggedInUser){
        wsRequestToken = [[WSB2CGetRequestToken alloc] init];
        wsRequestToken.delegate = self;
        [wsRequestToken getRequestToken];
    } else {
        [self userHaventLoggedIn];
    }
}

-(void)requestAccessToken:(NSString*) token{
    if(loggedInUser){
        wsGetAccessToken = [[WSB2CGetAccessToken alloc] init];
        wsGetAccessToken.delegate = self;
        [wsGetAccessToken requestAccessToken:token member:loggedInUser.userId];
    } else {
        [self userHaventLoggedIn];
     }
}

-(void) userHaventLoggedIn{
    NSError* err = [NSError errorWithDomain:@"Error" code:401 userInfo:nil];
    [self processDataResultWithError:err];

}

-(void) nextPage{
    if([self hasNextItem]){
        self.pageIndex++;
    } else {
        [self.delegate loadDataDoneFrom:self];
    }
}

-(BOOL) hasNextItem{
    return self.isHasNextPage;
}


-(void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_GET_REFRESH_ACCESS_TOKEN){
        // after refresh token, call next request
        [self startAPIAfterAuthenticate];
    }else if(ws.task == WS_GET_REQUEST_TOKEN){
        [self requestAccessToken:wsRequestToken.requestToken];
    } else if(ws.task == WS_GET_ACCESS_TOKEN){
        [self startAPIAfterAuthenticate];
    }
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    NSError* err = [NSError errorWithDomain:@"Error" code:errorCode userInfo:nil];
    if(result && [result.errorCode isEqualToString:B2C_INVALID_ACCESS_TOKEN]){
        // access token is invalid, have to request new token, then request access token again
        [self requestToken];
    } else {
        [self processDataResultWithError:err];
    }
}

-(void)POST:(NSString*) url withParams:(NSMutableDictionary*)params{
    if(ISLOGINGINFO)
    {
       NSLog(@"Request:%@\n%@", url, params);
    }
    
    if(!isNetworkAvailable())
    {
        request = [[RequestBase alloc] init];
        request.requestURL = url;
        request.parameterDict = params;
        request.requestMethod = POST;
        [self showNoNetWorkAlertForRetryWithData:request];
    }else{
        AFHTTPSessionManager *manager = [WSB2CBase manager];
        [self setCertificationForRequest:manager withUrlString:url];
        
        self.servicetask = [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull aftask, id  _Nullable responseObject) {
            // handling response
            if(ISLOGINGINFO)
            {
                NSLog(@"Response:%@%@",url,responseObject);
            }
            
            [self processDataResults:responseObject forTask:self.task forSubTask:self.subTask returnFormat:400];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            if(ISLOGINGINFO)
            {
                NSLog(@"Response:%@ error",error.localizedDescription);
            }
 
            [self processDataResultWithError:error];
        }];
    }
}

-(void)setCertificationForRequest:(AFHTTPSessionManager *)manager withUrlString:(NSString *)url
{
#ifndef DEBUG
    NSData *localCertificate = [[NSData alloc] init];
    
    /*
    if([url containsString:@"apiservice-stg.aspirelifestyles.com"])
    {
        NSString *pathToCert = [[NSBundle mainBundle]pathForResource:@"apiservice-stg.aspirelifestyles.com" ofType:@"cer"];
        localCertificate = [NSData dataWithContentsOfFile:pathToCert];
    }
    else
    {
        NSString *pathToCert = [[NSBundle mainBundle]pathForResource:@"tools.vipdesk.com" ofType:@"cer"];
        localCertificate = [NSData dataWithContentsOfFile:pathToCert];
    }
    */
    
    if([url containsString:@"tools.vipdesk.com"])
    {
        NSString *pathToCert = [[NSBundle mainBundle]pathForResource:@"tools.vipdesk.com" ofType:@"cer"];
        localCertificate = [NSData dataWithContentsOfFile:pathToCert];
    }
    else if([url containsString:@"api.vipdesk.com"])
    {
        NSString *pathToCert = [[NSBundle mainBundle]pathForResource:@"api.vipdesk.com" ofType:@"cer"];
        localCertificate = [NSData dataWithContentsOfFile:pathToCert];
    }
    else if([url containsString:@"apiservice.vipdesk.com"])
    {
        NSString *pathToCert = [[NSBundle mainBundle]pathForResource:@"apiservice.vipdesk.com" ofType:@"cer"];
        localCertificate = [NSData dataWithContentsOfFile:pathToCert];
    }
    else if([url containsString:@"apiservice-stg.aspirelifestyles.com"])
    {
        NSString *pathToCert = [[NSBundle mainBundle]pathForResource:@"apiservice-stg.aspirelifestyles.com" ofType:@"cer"];
        localCertificate = [NSData dataWithContentsOfFile:pathToCert];
    }
    
    if(localCertificate)
    {
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
        manager.securityPolicy = policy;
        manager.securityPolicy.pinnedCertificates = [NSSet setWithObject:localCertificate];
    }
#endif
}

-(void)GET:(NSString*) url withParams:(NSMutableDictionary*)params{
    if(ISLOGINGINFO)
    {
        NSLog(@"Request:%@%@",url,params);
    }
    
    if(!isNetworkAvailable())
    {
        request = [[RequestBase alloc] init];
        request.requestURL = url;
        request.parameterDict = params;
        request.requestMethod = GET;
        [self showNoNetWorkAlertForRetryWithData:request];
    }else{
        AFHTTPSessionManager *manager = [WSB2CBase manager];
        [self setCertificationForRequest:manager withUrlString:url];
        self.servicetask = [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull aftask, id  _Nullable responseObject) {
            if(ISLOGINGINFO)
            {
                NSLog(@"Response:%@%@",url,responseObject);
            }
            
            if([self respondsToSelector:@selector(processDataResults:forTask:forSubTask:returnFormat:)]) {
                [self processDataResults:responseObject forTask:self.task forSubTask:self.subTask returnFormat:400];
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            if(ISLOGINGINFO)
            {
                NSLog(@"Response:%@ error",error.localizedDescription);
            }
           
            [self processDataResultWithError:error];
        }];
    }
}

- (void) showNoNetWorkAlertForRetryWithData:(RequestBase *)currentRequest{
    [self.delegate stopIndicator];
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"cannot_get_data", nil);
    alert.msgAlert = NSLocalizedString(@"no_network_connection", nil);
    alert.secondBtnTitle = NSLocalizedString(@"settings_button", nil);
    alert.blockSecondBtnAction = ^(void){
        openWifiSettings();
    };
    alert.firstBtnTitle = NSLocalizedString(@"alert_ok_button", nil);
    
    alert.providesPresentationContextTransitionStyle = YES;
    alert.definesPresentationContext = YES;
    [alert setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    
    id rootViewController = [UIApplication sharedApplication].delegate.window.rootViewController;
    if([rootViewController isKindOfClass:[UINavigationController class]])
    {
        rootViewController = ((UINavigationController *)rootViewController).viewControllers.firstObject;
    }
    [rootViewController presentViewController:alert animated:YES completion:nil];
}

-(void)timerCheckNetwork:(NSTimer *)timer
{
    timeCounter += 1;
    RequestBase *requestTimer = (RequestBase*)timer.userInfo;
    if(ISLOGINGINFO)
    {
       NSLog(@"%@", timer.userInfo);
    }
    
    if(timeCounter == 16)
    {
        [timer invalidate];
        timeCounter = 0;
        [self showNoNetWorkAlertForRetryWithData:requestTimer];
    }
    else{
        if(isNetworkAvailable())
        {
            [timer invalidate];
            timeCounter = 0;
            if(requestTimer.requestMethod == POST){
                [self POST:requestTimer.requestURL withParams:[requestTimer.parameterDict mutableCopy]];
                
            }
            else{
                [self GET:requestTimer.requestURL withParams:[requestTimer.parameterDict mutableCopy]];
            }
        }
    }
    
}

-(void)cancelRequest{
    
    if(wsRequestToken){
        [wsRequestToken cancelRequest];
    }
    
    if(wsGetAccessToken){
        [wsGetAccessToken cancelRequest];
        
    }
    
    if(wsRefreshToken){
        [wsRefreshToken cancelRequest];
    }
    
    [super cancelRequest];
}

- (void)dealloc
{
    if(wsRequestToken){
        [wsRequestToken cancelRequest];
    }
    
    if(wsGetAccessToken){
        [wsGetAccessToken cancelRequest];
        
    }
    
    if(wsRefreshToken){
        [wsRefreshToken cancelRequest];
    }
    
    [super cancelRequest];
}


@end
