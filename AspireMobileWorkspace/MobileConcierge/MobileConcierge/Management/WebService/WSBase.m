//
//  WSBase.m
//  MobileMap
//
//  Created by Huy Tran on 5/10/12.
//  Copyright (c) 2012 S3Corp. All rights reserved.
//

#import "WSBase.h"
#import "BaseResponseObject.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "AppDelegate.h"
#import "Common.h"

@interface WSBase()

@end

@implementation WSBase


-(id) init
{
    self = [super init];
    // for new API
    jsonParser = [[SBJsonParser alloc] init];
    // end for new API
    return self;
}
-(void)startAPIAfterAuthenticate{
}


-(void)POST:(NSString*) url withParams:(NSMutableDictionary*)params{

}

-(void)GET:(NSString*) url withParams:(NSMutableDictionary*)params{

}

-(void) processDataResults:(NSDictionary*)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat
{
    
}


-(void)processDataResultWithError:(NSError *)errorCode
{
    
}

-(void) dealloc
{
    self.delegate = nil;
    if(self.servicetask && self.servicetask.state == NSURLSessionTaskStateRunning){
        [self.servicetask cancel];
        self.servicetask = nil;
    }
}

-(void)cancelRequest
{
    if(self.servicetask && self.servicetask.state == NSURLSessionTaskStateRunning){
        [self.servicetask cancel];
        self.servicetask = nil;
    }
}

-(void) nextPage
{
    
}
-(BOOL) hasNextItem
{
    return self.isHasNextPage;
}


@end
