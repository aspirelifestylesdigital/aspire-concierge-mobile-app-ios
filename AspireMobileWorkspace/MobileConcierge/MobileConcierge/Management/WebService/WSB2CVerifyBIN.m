//
//  WSB2CVerifyBIN.m
//  MobileConcierge
//
//  Created by Chung Mai on 7/5/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSB2CVerifyBIN.h"
#import "BaseResponseObject.h"
#import "BINResponseObject.h"

@implementation WSB2CVerifyBIN

-(void)verifyBIN
{
    NSString* url = [B2C_API_URL stringByAppendingString:checkBIN];
    [self POST:url withParams:[self buildRequestParams]];

}


-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    
    // cause of security scanning, not allow get and set password directly
    NSString* userPass = B2C_PW;
    [dictKeyValues setObject:B2C_SUBDOMAIN forKey:@"subDomain"];
    [dictKeyValues setObject:userPass forKey:@"password"];
    [dictKeyValues setObject:self.bin forKey:@"bin"];
    
    return dictKeyValues;
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        BINResponseObject *response = [[BINResponseObject alloc] initFromDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil ){
            self.data = response.data;
            [self.delegate loadDataDoneFrom:self];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}

-(void)processDataResultWithError:(NSError *)error
{
    [self.delegate loadDataFailFrom:nil withErrorCode:error.code];
}


@end
