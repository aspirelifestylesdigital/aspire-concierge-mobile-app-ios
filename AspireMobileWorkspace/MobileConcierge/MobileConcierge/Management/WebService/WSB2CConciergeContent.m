//
//  WSB2CConciergeContent.m
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/21/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSB2CConciergeContent.h"
#import "ConciergeContentResponseObject.h"

@implementation WSB2CConciergeContent

-(void) loadConciergeContentDataAdmin
{
    NSString* url = [@"https://tools.vipdesk.com/b2cwebservices/" stringByAppendingString:@"conciergecontent/GetAllContent"];
    [self POST:url withParams:[self buildRequestParams]];
    
    // return response
}

-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    
    // cause of security scanning, not allow get and set password directly
    NSString* userPass = @"9110D145-939E-46F5-A4B8-9354C525041B";
    [dictKeyValues setObject:B2C_SUBDOMAIN forKey:@"subDomain"];
    [dictKeyValues setObject:userPass forKey:@"password"];
    return dictKeyValues;
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        ConciergeContentResponseObject *response = [[ConciergeContentResponseObject alloc] initFromDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && response.isSuccess){
            self.data = [response data];
//            maybeHaveNextItems = pageSize*pageIndex < response.totalRecord;
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:nil withErrorCode:400 errorMessage:response.message];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}

-(void)processDataResultWithError:(NSError *)error
{
    [self.delegate loadDataFailFrom:nil withErrorCode:error.code];
}

@end
