//
//  RequestBase.h
//  MobileConcierge
//
//  Created by Chung Mai on 7/27/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RequestBase : NSObject

@property(nonatomic, strong) NSString *requestURL;
@property(nonatomic, strong) NSDictionary *parameterDict;
@property(nonatomic, assign) NSInteger requestMethod;

@end
