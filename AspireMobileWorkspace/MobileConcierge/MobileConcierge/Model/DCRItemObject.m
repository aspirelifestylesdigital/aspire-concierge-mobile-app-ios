//
//  DCRItemObject.m
//  ALC
//
//  Created by Anh Tran on 3/10/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "DCRItemObject.h"
#import "DCRImageObject.h"
#import "DCRVendorObject.h"
#import "DCRRelatedItemObject.h"
#import "NSDictionary+SBJSONHelper.h"
#import "Constant.h"
#import "Common.h"
#import "DCRCommonObject.h"

@implementation DCRItemObject


-(id) initFromDict:(NSDictionary*) dict{
    self = [super init];
    self.itemId = [dict stringForKey:@"id"];
    self.itemName = [dict stringForKey:@"name"];
    self.itemType = [[DCRCommonObject alloc] initFromDict:[dict dictionaryForKey:@"item_type"]];
    self.paredItemType = [self parseItemType:self.itemType.Name];
    self.itemSummary = [dict stringForKey:@"summary"];
    self.latitude = [dict stringForKey:@"latitude"];
    self.longitude = [dict stringForKey:@"longitude"];
    self.status = [[DCRCommonObject alloc] initFromDict:[dict dictionaryForKey:@"status"]];
    self.addressLine1 = [dict stringForKey:@"address_line_1"];
    self.addressLine2 = [dict stringForKey:@"address_line_2"];
    self.addressMetropolitan = [dict stringForKey:@"address_metropolitan"];
    self.addressNeighbourhood = [dict stringForKey:@"address_neighbourhood"];
    self.addressPostalCode = [dict stringForKey:@"address_postal_code"];
    self.addressCity = [[DCRCommonObject alloc] initFromDict:[dict dictionaryForKey:@"address_city"]];
    self.addressState = [[DCRCommonObject alloc] initFromDict:[dict dictionaryForKey:@"address_state"]];
    self.addressCountry = [[DCRCommonObject alloc] initFromDict:[dict dictionaryForKey:@"address_country"]];
    self.images = [dict arrayForKey:@"images" withObjectClass:[DCRImageObject class]];
    
    self.priceRange = [[DCRCommonObject alloc] initFromDict:[dict dictionaryForKey:@"price_range"]];
    self.priceCurrency = [[DCRCommonObject alloc] initFromDict:[dict dictionaryForKey:@"price_currency"]];
    self.relatedItems = [dict arrayForKey:@"related_items" withObjectClass:[DCRRelatedItemObject class]];
    self.sourceName = [dict stringForKey:@"source_name"];
    
    self.cuisines = [dict arrayForKey:@"cuisines" withObjectClass:[DCRCommonObject class]];// for Dining
    self.internationalStartRating = [[DCRCommonObject alloc] initFromDict:[dict dictionaryForKey:@"international_star_rating"]]; // for SightSeeing
    self.vacationTypes = [dict arrayForKey:@"types" withObjectClass:[DCRCommonObject class]];// for Vacation
    
    //Event
    self.category = [[DCRCommonObject alloc] initFromDict:[dict dictionaryForKey:@"category"]];
    self.startDate = convertStringToDateWithTimeZone([dict stringForKey:@"start_date"],DCR_DATE_FORMAT, UTC_TIMEZONE);
    self.endDate = convertStringToDateWithTimeZone([dict stringForKey:@"end_date"],DCR_DATE_FORMAT, UTC_TIMEZONE);
    
    //Privileges
    self.effectionDate = convertStringToDateWithTimeZone([dict stringForKey:@"effective_date"],DCR_DATE_FORMAT, UTC_TIMEZONE);
    self.expiredDate = convertStringToDateWithTimeZone([dict stringForKey:@"expiry_date"],DCR_DATE_FORMAT, UTC_TIMEZONE);
    self.vendors = [dict arrayForKey:@"vendors" withObjectClass:[DCRVendorObject class]];
    return self;
}


- (enum DCR_ITEM_TYPE) parseItemType:(NSString *) iType{
    if(isEqualIgnoreCase(iType, @"accommodation")){
        return ACCOMMODATION_ITEM;
    } else if(isEqualIgnoreCase(iType, @"dining")){
        return DINING_ITEM;
    } else if(isEqualIgnoreCase(iType, @"event")){
        return EVENT_ITEM;
    } else if(isEqualIgnoreCase(iType, @"privilege")){
        return PRIVILEGE_ITEM;
    } else if(isEqualIgnoreCase(iType, @"sightseeing_activity")){
        return SIGHTSEEING_ITEM;
    } else if(isEqualIgnoreCase(iType, @"vacation_package")){
        return ACCOMMODATION_ITEM;
    }
    
    return UNKNOWN_TYPE;
    
}

- (NSString*) getFirstImageUrl{
    if(_images && _images.count > 0){
       return ((DCRImageObject*)[_images objectAtIndex:0]).url;
    }
    return nil;
}
@end
