//
//  DCRItemObject.h
//  ALC
//
//  Created by Anh Tran on 3/10/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DCRCommonObject.h"


@interface DCRItemObject : NSObject
@property (nonatomic, strong) NSString* itemId;
@property (nonatomic, strong) DCRCommonObject* itemType;
@property (nonatomic) enum DCR_ITEM_TYPE paredItemType;
@property (nonatomic, strong) NSString* itemName;
@property (nonatomic, strong) NSString* itemSummary;
@property (nonatomic, strong) NSString* latitude;
@property (nonatomic, strong) NSString* longitude;
@property (nonatomic, strong) DCRCommonObject* status;
@property (nonatomic, strong) NSString* addressLine1;
@property (nonatomic, strong) NSString* addressLine2;
@property (nonatomic, strong) NSString* addressMetropolitan;
@property (nonatomic, strong) NSString* addressNeighbourhood;
@property (nonatomic, strong) NSString* addressPostalCode;
@property (nonatomic, strong) DCRCommonObject* addressCity;
@property (nonatomic, strong) DCRCommonObject* addressState;
@property (nonatomic, strong) DCRCommonObject* addressCountry;
@property (nonatomic, strong) NSArray* images;
@property (nonatomic, strong) DCRCommonObject* priceRange;
@property (nonatomic, strong) DCRCommonObject* priceCurrency;
@property (nonatomic, strong) NSArray* relatedItems;
@property (nonatomic, strong) NSString* sourceName;

@property (nonatomic, strong) NSArray* cuisines; // for Dining
@property (nonatomic, strong) DCRCommonObject* internationalStartRating; // for SightSeeing
@property (nonatomic, strong) NSArray* vacationTypes; // for Vacation

//Event
@property (nonatomic, strong) DCRCommonObject* category;
@property (nonatomic, strong) NSDate* startDate;
@property (nonatomic, strong) NSDate* endDate;

//Privileges
@property (nonatomic, strong) NSDate* effectionDate;
@property (nonatomic, strong) NSDate* expiredDate;
@property (nonatomic, strong) NSArray* vendors;

-(id) initFromDict:(NSDictionary*) dict;
- (NSString*) getFirstImageUrl;
@end
