//
//  CitiesManager.h
//  TestIOS
//
//  Created by Dai Pham on 7/12/17.
//  Copyright © 2017 Dai Pham. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCAGeographicObject: NSObject
@property (strong,nonatomic) NSString* City;
@property (strong,nonatomic) NSString* GeographicRegion;
@property (strong,nonatomic) NSString* Subcategory;

@end

@interface CCAGeographicManager : NSObject {
    NSArray* data;
}

@property (copy) void (^onDone)();

+ (CCAGeographicManager*) startService;

+ (NSString*) getSubcategoryForCity:(NSString*)cityName;
+ (NSString*) getGeographicForCity:(NSString*)cityName;
@end
