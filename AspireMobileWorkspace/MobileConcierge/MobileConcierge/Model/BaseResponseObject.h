//
//  BaseResponseObject.h
//  ALC
//
//  Created by Anh Tran on 8/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseResponseObjectProtocol.h"
#import "Category+CoreDataClass.h"
#import "Category+CoreDataProperties.h"
#import "City+CoreDataClass.h"
#import "ConciergeContent+CoreDataClass.h"
#import "CityItem.h"
#import "CategoryItem.h"

@interface BaseResponseObject : NSObject<BaseResponseObjectProtocol>

@property(nonatomic, assign) NSInteger totalItem;
@property (nonatomic, assign) NSInteger task;
@property (nonatomic, assign) NSInteger status;
@property (nonatomic, strong) NSString* message;
@property (nonatomic, strong) NSArray* data;
@property (nonatomic, strong) NSArray* errors;
@property (nonatomic, copy)   NSString* errorCode;

@property(nonatomic, copy) NSString *categoryCode;
@property(nonatomic, copy) NSString *categoryName;

@property(nonatomic, strong) CategoryItem *currentCategory;
@property(nonatomic, strong) CityItem *currentCity;

-(id) initFromDict:(NSDictionary*) dict;
-(id) parseJson:(NSDictionary *)dict toObject:(NSString *)className;
-(void) parseCommonResponse:(NSDictionary *)dict;
-(BOOL) isSuccess;


@end
