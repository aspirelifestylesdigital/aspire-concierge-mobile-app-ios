//
//  UserObject.m
//  ALC
//
//  Created by Anh Tran on 8/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "UserObject.h"
#import "HGCheckCountryCode.h"
#import "NSDictionary+SBJSONHelper.h"


@implementation UserObject
@synthesize userId, password, avatarUrl,salutation, lastName, firstName, cardExpirydDate, email, mobileNumber, nameOnCard, currentCity, currentCountry, countryOfBirth, dateOfBirth, zipCode, memberCardId,fullName, shortMobileNumber, countryCode,ONLINEMEMBERDETAILID,VERIFICATIONCODE, optStatus;
-(Boolean) isLoggedIn{
    if(userId!=nil && userId.length>0){
        return true;
    }
    return false;
}

- (id) initFromDict:(NSDictionary*) dict{
    self = [[UserObject alloc] init];
    userId = [dict stringForKey:@"ONLINEMEMBERID"];
    email = [dict stringForKey:@"EMAIL"];
    password = [dict stringForKey:@"Password"];
    avatarUrl = [dict stringForKey:@"AvatarURL"];
    salutation = [dict stringForKey:@"SALUTATION"];
    firstName = [dict stringForKey:@"FIRSTNAME"];
    lastName = [dict stringForKey:@"LASTNAME"];
    fullName = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
    dateOfBirth = [dict stringForKey:@"DateOfBirth"];
    zipCode = [dict stringForKey:@"POSTALCODE"];
    nameOnCard = [dict stringForKey:@"NameOnCard"];
    mobileNumber = [dict stringForKey:@"MOBILENUMBER"];
    memberCardId = [dict stringForKey:@"MemberCardID"];
    optStatus = [dict boolForKey:@"optStatus"];
//    [self isValidMobileNumber:mobileNumber];
    return self;
}

//- (BOOL)isOptStatus {
//    if ([[[[NSUserDefaults standardUserDefaults] dictionaryRepresentation] allKeys] containsObject:@"App:isOptStatus"]) {
//        optStatus = [[NSUserDefaults standardUserDefaults] boolForKey:@"App:isOptStatus"];
//    }
//    return optStatus;
//}

-(BOOL)isValidMobileNumber:(NSString*)checkString
{
    checkString = [checkString stringByReplacingOccurrencesOfString:@"+" withString:@""];
    /*
    if ((checkString == nil) || (checkString.length < 8)) {
        return NO;
    }
    */
    NSString* mobile= checkString;
    
    HGCheckCountryCode* checkCode= [[HGCheckCountryCode alloc] init];
    
    NSString* tempCode = @"";
    NSString* countrycode =@"";
    
    for (int i =1; i <5; i++) {
        NSString* temp = [mobile substringToIndex:i];
        if([temp isEqualToString:@"1"]){
            NSString* code = [mobile substringToIndex:4];
            if([checkCode checkCountryCode:code]){
                countrycode = [NSString stringWithFormat:@"%@",code];
            }else{
                countrycode = [NSString stringWithFormat:@"1"];
            }
            break;
        }else{
            tempCode = [NSString stringWithFormat:@"%@",temp];
            if([checkCode checkCountryCode:tempCode]){
                countrycode = [NSString stringWithFormat:@"%@",tempCode];
                break;
            }
        }
    }
    NSString* checkPhone = [mobile substringFromIndex:countrycode.length];
    shortMobileNumber = checkPhone;
    countryCode = countrycode;
    
    return YES;
}
@end
