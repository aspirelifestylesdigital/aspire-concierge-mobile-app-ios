//
//  PolicyInfoResponseObject.h
//  MobileConcierge
//
//  Created by Chung Mai on 6/19/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseResponseObject.h"

@interface PolicyInfoResponseObject : BaseResponseObject

@end
