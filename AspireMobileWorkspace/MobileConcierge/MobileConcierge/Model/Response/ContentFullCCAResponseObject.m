//
//  ContentFullCCAResponseObject.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/5/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "ContentFullCCAResponseObject.h"
#import "ContentFullCCAItem.h"
#import "NSDictionary+SBJSONHelper.h"
#import "NSString+HTML.h"
#import "NSString+Utis.h"

@implementation ContentFullCCAResponseObject

-(id)initFromDict:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        dict = [dict dictionaryForKey:@"GetContentFullResult"];
        self.data = [self parseJson:[dict dictionaryForKey:@"ContentFull"] toObject:NSStringFromClass([ContentFullCCAItem class])];
        [self parseCommonResponse:dict];
    }
    return self;
}

-(id)parseJson:(NSDictionary *)dict toObject:(NSString *)className
{
    if([className isEqualToString:NSStringFromClass([ContentFullCCAItem class])])
    {
        return [NSArray  arrayWithObject:[self parseJsonForQuestionItem:dict]];
    }
    
    
    return nil;
}

-(ContentFullCCAItem *)parseJsonForQuestionItem:(NSDictionary *)dict
{
    ContentFullCCAItem *item = [[ContentFullCCAItem alloc] init];
    item.ID = [dict stringForKey:@"ID"];
    item.benefitInd = [dict boolForKey:@"BenefitInd"];
    item.category = [dict stringForKey:@"Category"];
    item.descriptionContent = dict[@"Description"] ? dict[@"Description"] : @"";
    item.descriptionContent = [item.descriptionContent removeRedudantNewLineInText];
    if ([item.descriptionContent containsString:@"<img"]){
        NSRange range2 = [item.descriptionContent rangeOfString:@"</p>"];
        if(range2.location != NSNotFound)
        {
            // get image link from Description
            NSString *imgString = [self getImageTagString:item.descriptionContent andRange:range2];
            
            NSString* result = [imgString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if ([result containsString:@"src"] && [result containsString:@"style"]){
                result = [result substringFromIndex:[result rangeOfString:@"src="].location+[@"src=" length]+1];
                result = [result substringToIndex:[result rangeOfString:@"style="].location-2];
            }
            item.imgURL = result;
            
            /// trim image tag from Description
            item.descriptionContent = [item.descriptionContent stringByReplacingOccurrencesOfString:imgString withString:@""];
            item.descriptionContent = [[self flattenHtml:item.descriptionContent] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            item.descriptionContent = [item.descriptionContent stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        }
        
    }else{
        item.imgURL = @"";
    }

    item.geographicRegion = [dict stringForKey:@"GeographicRegion"];
    item.hotInd = [dict boolForKey:@"HotInd"];
    item.offerInd = [dict boolForKey:@"OfferInd"];
    item.offerText = dict[@"OfferText"] ? dict[@"OfferText"] : @"";
    if ([item.offerText containsString:@"<img"]){
        NSRange endImageRange = [item.offerText rangeOfString:@"/>"];
        if(endImageRange.location != NSNotFound)
        {
            // get image link from Description
            NSRange beginImageRange = [item.offerText rangeOfString:@"<img"];
            NSString *imgString = [item.offerText substringWithRange:NSMakeRange(beginImageRange.location, NSMaxRange(endImageRange) - beginImageRange.location)];
            
            item.offerText = [item.offerText stringByReplacingOccurrencesOfString:imgString withString:@""];
            item.offerText = [item.offerText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        }
    }
//    item.offerText = [item.offerText stringWithNewLinesAsPs];
    item.parentCategory = [dict stringForKey:@"ParentCategory"];
    item.subCategory = [dict stringForKey:@"SubCategory"];
    item.termsOfUse = dict[@"TermsOfUse"] ? dict[@"TermsOfUse"] : @"";
    item.title = [dict stringForKey:@"Title"];
    
    return item;
}

- (NSString*) getImageTagString:(NSString*)tagString andRange:(NSRange)range{
    if(range.location != NSNotFound)
    {
        return [[tagString substringToIndex:NSMaxRange(range)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    }
    return @"";
}

-(void)parseCommonResponse:(NSDictionary *)dict
{
    self.status = [dict boolForKey:@"Success"];
    NSArray* mesArr = [dict arrayForKey:@"message"];
    if(mesArr && mesArr.count > 0){
        NSDictionary * error = [mesArr objectAtIndex:0];
        self.message = [error stringForKey:@"message"];
        self.errorCode = [error stringForKey:@"code"];
    }
    
    self.message = [dict objectForKey:@"Status"];
}

-(NSString *) stringByStrippingHTMLWithString:(NSString*)str {
    NSRange r;
    NSString *s = [str copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

- (NSString *)flattenHtml: (NSString *) html {
    NSScanner *theScanner;
    NSString *text = nil;
    theScanner = [NSScanner scannerWithString: html];
    while ([theScanner isAtEnd] == NO) {
        
        [theScanner scanUpToString: @"<" intoString: NULL];
        
        [theScanner scanUpToString: @">" intoString: &text];
        
        // Only Replace if you are outside of an html tag
    } // while
    return html;
}

@end
