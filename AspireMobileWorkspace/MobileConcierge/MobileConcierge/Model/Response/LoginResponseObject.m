//
//  LoginResponseObject.m
//  MobileConcierge
//
//  Created by Chung Mai on 4/26/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "LoginResponseObject.h"
#import "NSDictionary+SBJSONHelper.h"

@implementation LoginResponseObject

-(id)initFromDict:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        [self parseCommonResponse:dict];
    }
    return self;
}
@end
