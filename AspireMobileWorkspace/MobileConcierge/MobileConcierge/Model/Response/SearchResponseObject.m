//
//  SearchResponseObject.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/21/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "SearchResponseObject.h"
#import "SearchItem.h"
#import "NSDictionary+SBJSONHelper.h"
#import "CategoryItem.h"

@implementation SearchResponseObject

-(id)initFromDict:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        dict = [dict dictionaryForKey:@"SearchIAAndCCAResult"];
        self.data = [dict arrayForKey:@"SearchResults" withProcessing:self toObject:NSStringFromClass([SearchItem class])];
        [self parseCommonResponse:dict];
    }
    return self;
}

-(id)parseJson:(NSDictionary *)dict toObject:(NSString *)className
{
    if([className isEqualToString:NSStringFromClass([SearchItem class])])
    {
        self.totalItem += 1;
        SearchItem *item = [[SearchItem alloc] init];
        item.ID = [dict stringForKey:@"ID"];
        item.title = [dict stringForKey:@"Title"];
        item.searchDescription = [dict stringForKey:@"Description"];
        item.product = [dict stringForKey:@"Product"];
        item.score = [dict stringForKey:@"Score"];
        item.secondaryID = [dict stringForKey:@"SecondaryID"];
        item.hasOffer = [dict boolForKey:@"HasOffer"];
        item.address1 = [dict stringForKey:@"Address1"];
        item.address2 = [dict stringForKey:@"Address2"];
        item.address3 = [dict stringForKey:@"Address3"];
        item.cityName = [dict stringForKey:@"City"];
        item.countryName = [dict stringForKey:@"Country"];
        item.GeographicRegion = [dict stringForKey:@"GeographicRegion"];
        item.searchSubCategory = [dict stringForKey:@"SubCategory"];
        item.searchCategory = [dict stringForKey:@"Category"];
        item.currentCategoryCode = self.categoryCode;
        
        // remove item Custom / Speacial
        if([self.categoryCode isEqualToString:@"all"])
        {
            if([item.categoryCode isEqualToString:@""]){
                return nil;
            }
        }
        else if([self.categoryCode compare:item.categoryCode options:NSCaseInsensitiveSearch] != NSOrderedSame){
            return nil;
        }
        
        if(self.hasOffer && !item.hasOffer)
        {
            return nil;
        }
        
        if([item.product isEqualToString:@"CCA"] && [item.searchDescription isEqualToString:@"..."]) {
            return nil;
        }
        
        if(![item.searchSubCategory isEqualToString:@"Global Partners"])
        {
            NSArray* geographic = [self.currentCity.GeographicRegion componentsSeparatedByString:@"|"];
            NSArray* subcate = [self.currentCity.SubCategory componentsSeparatedByString:@"|"];
            BOOL isOK = NO;
            // filter Hotels, Cruise, VP for all category
            if([item.categoryCode isEqualToString:@"hotels"] || [item.categoryCode isEqualToString:@"vacation packages"] || [item.categoryCode isEqualToString:@"cruise"])
            {
                if([item.GeographicRegion stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0 &&
                   [item.searchSubCategory stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0)
                {
                    isOK = YES;
                }
                else
                {
                    for (int i = 0; i < geographic.count; i++) {
                        NSString* geo = [geographic objectAtIndex:i];
                        NSString* sub = [subcate objectAtIndex:i];
                        if([geo isEqualToString:@"all"]) {
                            if([sub isEqualToString:item.searchSubCategory]) {
                                isOK = YES;
                                break;
                            }
                        }
                        else if([item.GeographicRegion isEqualToString:@""])
                        {
                            if([sub isEqualToString:item.searchSubCategory]) {
                                isOK = YES;
                                break;
                            }
                        }
                        else {
                            if([geo isEqualToString:item.GeographicRegion] && [sub isEqualToString:item.searchSubCategory]) {
                                isOK = YES;
                                break;
                            }
                        }
                    }
                }
                
                if(!isOK) {
                    return nil;
                }
            }
            else
            {
                if([item.GeographicRegion stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0){
                    return item;
                }
                
                for (int i = 0; i < geographic.count; i++) {
                    NSString* geo = [geographic objectAtIndex:i];
                    if([geo isEqualToString:item.GeographicRegion]){
                        isOK = YES;
                        break;
                    }
                }
                
                if(!isOK) {
                    return nil;
                }
            }
        }
        
        self.offerNumber += 1;
        self.searchItemNumber += 1;
        return item;
    }
    
    return nil;
}

-(void)parseCommonResponse:(NSDictionary *)dict
{
    self.status = [dict boolForKey:@"Success"];
    NSArray* mesArr = [dict arrayForKey:@"message"];
    if(mesArr && mesArr.count > 0){
        NSDictionary * error = [mesArr objectAtIndex:0];
        self.message = [error stringForKey:@"message"];
        self.errorCode = [error stringForKey:@"code"];
    }
    
    self.message = [dict objectForKey:@"message"];
}

@end
