//
//  SearchItem.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/21/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "SearchItem.h"
#import "NSString+Utis.h"

@implementation SearchItem

-(NSString *)name
{
    return self.title;
}

-(NSString *)address
{
    if([self.product isEqualToString:@"CCA"])
    {
        return [self addressForItem];
    }
    
    return self.address3;
}
-(NSString *)offer
{
    return [self.searchDescription removeRedudantNewLineInText];
}

-(BOOL)isOffer
{
    return self.hasOffer;
}

-(NSString *)categoryName
{
    if([self.product isEqualToString:@"IA"])
    {
        return @"DINING";
    }
    else if(([self.currentCategoryCode isEqualToString:@"flowers"] || [self.currentCategoryCode isEqualToString:@"all"])
       && ([self.searchCategory compare:@"Flowers" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
       return @"FLOWERS";
    }
    else if([self.searchCategory compare:@"Specialty Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame &&
            ([self.searchSubCategory compare:@"Entertainment Experiences" options:NSCaseInsensitiveSearch] == NSOrderedSame
             || [self.searchSubCategory compare:@"Major Sports Events" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return @"EXPERIENCES";
    }
    else if(([self.currentCategoryCode isEqualToString:@"art culture"] || [self.currentCategoryCode isEqualToString:@"all"])&& [self.searchCategory compare:@"Tickets" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"ARTS + \nCULTURE";
    }
    else if([self.currentCategoryCode isEqualToString:@"sports"] && [self.searchCategory compare:@"Tickets" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"SPORTS";
    }
    else  if([self.searchCategory compare:@"VIP Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.searchSubCategory compare:@"Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"TRAVEL";
    }
    else if(([self.currentCategoryCode isEqualToString:@"shopping"] || [self.currentCategoryCode isEqualToString:@"all"])
            && ([self.searchCategory compare:@"Golf Merchandise" options:NSCaseInsensitiveSearch] == NSOrderedSame
                || [self.searchCategory compare:@"Flowers" options:NSCaseInsensitiveSearch] == NSOrderedSame
                || [self.searchCategory compare:@"Wine" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return @"SHOPPING";
    }
    else
    {
        return @"";
    } 
}


-(NSString *)categoryCode
{
    if([self.product isEqualToString:@"IA"])
    {
        return @"dining";
    }
    else if(([self.currentCategoryCode isEqualToString:@"flowers"] || [self.currentCategoryCode isEqualToString:@"all"])
       && ([self.searchCategory compare:@"Flowers" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return @"flowers";
    }
    else if([self.searchCategory compare:@"Specialty Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame &&
            ([self.searchSubCategory compare:@"Entertainment Experiences" options:NSCaseInsensitiveSearch] == NSOrderedSame
             || [self.searchSubCategory compare:@"Major Sports Events" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return @"experiences";
    }
    else if(([self.currentCategoryCode isEqualToString:@"art culture"] || [self.currentCategoryCode isEqualToString:@"all"])&& [self.searchCategory compare:@"Tickets" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"art culture";
    }
    else if([self.currentCategoryCode isEqualToString:@"sports"] && [self.searchCategory compare:@"Tickets" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"sports";
    }
    else  if([self.searchCategory compare:@"VIP Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.searchSubCategory compare:@"Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"travel";
    }
    else if(([self.currentCategoryCode isEqualToString:@"shopping"] || [self.currentCategoryCode isEqualToString:@"all"])
            && ([self.searchCategory compare:@"Golf Merchandise" options:NSCaseInsensitiveSearch] == NSOrderedSame
                || [self.searchCategory compare:@"Flowers" options:NSCaseInsensitiveSearch] == NSOrderedSame
                || [self.searchCategory compare:@"Wine" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return @"shopping";
    }
    else
    {
        return @"";
    }
}

@end
