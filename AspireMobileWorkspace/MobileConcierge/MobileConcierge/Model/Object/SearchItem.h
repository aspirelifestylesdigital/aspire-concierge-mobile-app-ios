//
//  SearchItem.h
//  MobileConcierge
//
//  Created by Chung Mai on 6/21/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseItem.h"

@interface SearchItem : BaseItem

@property(nonatomic, strong) NSString * searchDescription;
@property(nonatomic, strong) NSString * product;
@property(nonatomic, strong) NSString * score;
@property(nonatomic, strong) NSString * secondaryID;
@property(nonatomic, strong) NSString * title;
@property(nonatomic, assign) BOOL hasOffer;
//@property(nonatomic, strong) NSString * address1;
//@property(nonatomic, strong) NSString * address2;
//@property(nonatomic, strong) NSString * address3;
//@property(nonatomic, strong) NSString * cityName;
//@property(nonatomic, strong) NSString * countryName;
@property(nonatomic, strong) NSString *GeographicRegion;
@property(nonatomic, strong) NSString *searchSubCategory;
@property(nonatomic, strong) NSString *searchCategory;
@property(nonatomic, strong) NSString *currentCategoryCode;

@end
