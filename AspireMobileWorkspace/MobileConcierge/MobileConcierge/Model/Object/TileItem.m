//
//  TileItem.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/6/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "TileItem.h"

@implementation TileItem

-(NSString *)name
{
    return self.title;
}

-(NSString *)offer
{
    return self.tileText;
}

-(NSString *)imageURL
{
    return self.tileImage;
}

-(NSString *)address
{
    return [self addressForItem];
}

-(BOOL) isOffer
{
    return YES;
}

-(NSString *)categoryName
{
    if(([self.currentCategoryCode isEqualToString:@"flowers"] || [self.currentCategoryCode isEqualToString:@"all"])
       && ([self.tileCategory compare:@"Flowers" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return @"FLOWERS";
    }
    else if([self.tileCategory compare:@"Specialty Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame &&
            ([self.tileSubCategory compare:@"Entertainment Experiences" options:NSCaseInsensitiveSearch] == NSOrderedSame
             || [self.tileSubCategory compare:@"Major Sports Events" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return @"EXPERIENCES";
    }
    else if(([self.currentCategoryCode isEqualToString:@"art culture"] || [self.currentCategoryCode isEqualToString:@"all"])&& [self.tileCategory compare:@"Tickets" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"ARTS + \nCULTURE";
    }
    else if([self.currentCategoryCode isEqualToString:@"sports"] && [self.tileCategory compare:@"Tickets" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"SPORTS";
    }
    else  if([self.tileCategory compare:@"VIP Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.tileSubCategory compare:@"Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"TRAVEL";
    }
    else if(([self.currentCategoryCode isEqualToString:@"shopping"] || [self.currentCategoryCode isEqualToString:@"all"])
            && ([self.tileCategory compare:@"Golf Merchandise" options:NSCaseInsensitiveSearch] == NSOrderedSame
                || [self.tileCategory compare:@"Flowers" options:NSCaseInsensitiveSearch] == NSOrderedSame
                || [self.tileCategory compare:@"Wine" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return @"SHOPPING";
    }
    else
    {
        return @"";
    }
}

-(NSString *)categoryCode
{
    /*
    if([self.tileCategory compare:@"Dining" options:NSCaseInsensitiveSearch] == NSOrderedSame || ([self.tileCategory compare:@"Specialty Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.tileSubCategory compare:@"Culinary Experiences" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return @"dining";
    }
    else
     */
    if(([self.currentCategoryCode isEqualToString:@"flowers"] || [self.currentCategoryCode isEqualToString:@"all"])
            && ([self.tileCategory compare:@"Flowers" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return @"flowers";
    }
    else if([self.tileCategory compare:@"Specialty Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame &&
                ([self.tileSubCategory compare:@"Entertainment Experiences" options:NSCaseInsensitiveSearch] == NSOrderedSame
                 || [self.tileSubCategory compare:@"Major Sports Events" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return @"experiences";
    }
    else if(([self.currentCategoryCode isEqualToString:@"art culture"] || [self.currentCategoryCode isEqualToString:@"all"])&& [self.tileCategory compare:@"Tickets" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"art culture";
    }
    else if([self.currentCategoryCode isEqualToString:@"sports"] && [self.tileCategory compare:@"Tickets" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"sports";
    }
    else  if([self.tileCategory compare:@"VIP Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.tileSubCategory compare:@"Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"travel";
    }
    else if(([self.currentCategoryCode isEqualToString:@"shopping"] || [self.currentCategoryCode isEqualToString:@"all"])
            && ([self.tileCategory compare:@"Golf Merchandise" options:NSCaseInsensitiveSearch] == NSOrderedSame
                || [self.tileCategory compare:@"Flowers" options:NSCaseInsensitiveSearch] == NSOrderedSame
                || [self.tileCategory compare:@"Wine" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return @"shopping";
    }
    else
    {
        return @"";
    }
}
@end
