//
//  ExploreItem.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/9/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "BaseItem.h"


@implementation BaseItem


-(NSString *)addressForItem
{
    // Experience only take place in a specific location
    NSArray *array = @[@"1023",@"1138",@"1139",@"1244",@"1245"];
    if([array containsObject:self.ID])
    {
        return @" ";
    }
    
    return @"Multiple Cities";
}
@end
