//
//  AnswerItem.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/1/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "AnswerItem.h"
#import "BaseItem.h"

@implementation AnswerItem

-(NSString *)name
{
    return self.answerName;
}

-(NSString *)offer
{
    if([self.categoryCode isEqualToString:@"dining"])
    {
      return self.offer2;
    }
    
    return self.insiderTip;
}


-(NSString *)address
{
    return (self.address3.length > 0) ? self.address3 : @" ";
}

-(BOOL)isOffer
{
    return (self.offer2.length > 0);
}


-(UIImage *)image
{
    if([self.categoryCode isEqualToString:@"cityguide"])
    {
        if([self.categoryName isEqualToString:@"ACCOMMODATIONS"])
        {
            return [UIImage imageNamed:@"asset_cityguide_detail"];
        }
        
        if([self.categoryName isEqualToString:@"BARS/CLUBS"])
        {
            return [UIImage imageNamed:@"bar_club_cityguide_detail"];
        }
        
        if([self.categoryName isEqualToString:@"CULTURE"])
        {
            return [UIImage imageNamed:@"culture_cityguide_detail"];
        }
        
        if([self.categoryName isEqualToString:@"DINING"])
        {
            return [UIImage imageNamed:@"dining_cityguide_detail"];
        }
        
        if([self.categoryName isEqualToString:@"SHOPPING"])
        {
            return [UIImage imageNamed:@"shopping_cityguide_detail"];
        }
        
        if([self.categoryName isEqualToString:@"SPAS"])
        {
            return [UIImage imageNamed:@"spa_wellness_cityguide_detail"];
        }
    }
    return super.image;
}

@end
