//
//  SubCategoryItem.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/30/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "SubCategoryItem.h"

@implementation SubCategoryItem

-(void)setMappingDataForSubCategory
{
    if([self.name isEqualToString:@"Fabulous Dining"] )
    {
        self.image =  [UIImage imageNamed:@"dining_cityguide"];
        self.name = @"DINING";
    }
    else if([self.name isEqualToString:@"Cool for Cocktails"])
    {
        self.image =  [UIImage imageNamed:@"bar_club_cityguide"];
        self.name = @"BARS/CLUBS";
    }
    else if([self.name isEqualToString:@"Beauty/Grooming/Spa/Fitness"])
    {
        self.image =  [UIImage imageNamed:@"spa_wellness_cityguide"];
        self.name = @"SPAS";
    }
    else if([self.name isEqualToString:@"Shops & Boutiques"])
    {
        self.image =  [UIImage imageNamed:@"shopping_cityguide"];
        self.name = @"SHOPPING";
    }
    else if([self.name isEqualToString:@"Shh... Local Knowledge"])
    {
        self.image =  [UIImage imageNamed:@"culture_cityguide"];
        self.name = @"CULTURE";
    }
    else
    {
        self.image =  [UIImage imageNamed:@"asset_cityguide"];
        self.name = @"ACCOMMODATIONS";
    }
    
}
@end
