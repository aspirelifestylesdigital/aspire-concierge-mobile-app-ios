//
//  NSString+Utis.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/5/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utis)
-(BOOL)isValidEmail;
-(BOOL)isValidPhoneNumber;
-(BOOL)isValidZipCode;
-(BOOL)isValidBinNumber;
-(BOOL)isValidStrongPassword;
-(NSString *)createMaskStringBeforeNumberCharacter:(NSInteger)number;
-(NSString *)createMaskForText:(BOOL)isEmail;
-(NSString *)createMaskForPhone;
-(NSString *)MD5;
-(BOOL)isValidName;
-(NSString *)removeBlankFromURL;
- (NSUInteger)occurrenceCountOfCharacter:(UniChar)character;
-(NSString *)removeRedudantWhiteSpaceInText;
-(NSString *)removeRedudantNewLineInText;
-(NSString *)replaceWhiteSpacingTag;
@end
