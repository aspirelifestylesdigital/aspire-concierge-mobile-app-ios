//
//  Constant.m
//  ALC
//
//  Created by Anh Tran on 8/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "Constant.h"

/***
 /// Define Font
 ***/
NSString *const FONT_MarkForMC_BOLD = @"MarkOffcForMC-Bold";
NSString *const FONT_MarkForMC_MED = @"MarkOffcForMC-Medium";
NSString *const FONT_MarkForMC_LIGHT = @"MarkOffcForMC-Light";
NSString *const FONT_MarkForMC_LIGHTIT = @"MarkOffcForMC-LightItalic";
NSString *const FONT_MarkForMC_REGULAR = @"MarkOffcForMC";
NSString *const FONT_MarkForMC_REGULAR_It = @"MarkOffcForMC-Italic";
NSString *const FONT_MarkForMC_Nrw_BOLD = @"MarkForMCNrw-Bold";
NSString *const FONT_MarkForMC_Nrw_MED = @"MarkForMCNrw-Medium";
//NSString *const DEFAULT_BACKGROUND_COLOR = @"#272726";
//NSString *const DEFAULT_PLACEHOLDER_COLOR = @"#434342";


@implementation Constant

@end
