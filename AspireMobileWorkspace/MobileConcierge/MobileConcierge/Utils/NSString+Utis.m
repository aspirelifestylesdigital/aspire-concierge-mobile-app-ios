//
//  NSString+Utis.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/5/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "NSString+Utis.h"
#import "Constant.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (Utis)
-(BOOL)isValidEmail
{
    if(self.length > 100)
    {
        return NO;
    }
    
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@{1}([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

-(BOOL)isValidStrongPassword{
    //NSString *passwordRegex = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[d$@$!%*?&#])[A-Za-z\\dd$@$!%*?&#]{10,}"; //"'()+,-./:;<=>?@[\]^_`{|}~"
    NSString *passwordRegex = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[d$!@#$%^&*\\[\\]\\{\\}\"\\\\])[A-Za-z\\dd$!@#$%^&*\\[\\]\\{\\}\"\\\\]{10,25}";
    NSPredicate *passwordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", passwordRegex];
    
    return [passwordPredicate evaluateWithObject:self];
}

-(NSString *)createMaskStringBeforeNumberCharacter:(NSInteger)number
{
    NSMutableString *asterisks = [[NSMutableString alloc] init];
    for(int i = 0; i < self.length - number; i++)
    {
        [asterisks appendString:@"*"];
    }
    
    NSRange range = NSMakeRange(0,self.length - number);
    return [self stringByReplacingCharactersInRange:range withString:asterisks];
}

-(NSString *)createMaskForText:(BOOL)isEmail
{
    NSRange rangeForAtSign = [self rangeOfString:@"@" options:NSBackwardsSearch];
    NSString *beforeSign = isEmail ? [self substringToIndex:rangeForAtSign.location] : self;
    NSString *afterSign = isEmail ? [self substringFromIndex:rangeForAtSign.location+1] : @"";
    NSString *atSign = isEmail ? @"@" : @"";
    
    NSMutableString *asterisks = [[NSMutableString alloc] init];
    NSInteger length = 0;
    
    if(beforeSign.length == 1)
    {
        [asterisks appendString:@"*"];
        length = 0;
    }
    else if(beforeSign.length == 2)
    {
        [asterisks appendString:@"*"];
        length = 1;
    }
    else if(beforeSign.length == 3)
    {
        [asterisks appendString:@"**"];
        length = 1;
    }
    else if(beforeSign.length > 3)
    {
        for(int i = 0; i < beforeSign.length - 3; i++)
        {
            [asterisks appendString:@"*"];
        }
        length = 3;
    }
    
    NSRange range = NSMakeRange(0,beforeSign.length - length);
    NSString *newString =  [beforeSign stringByReplacingCharactersInRange:range withString:asterisks];
    return [NSString stringWithFormat:@"%@%@%@",newString,atSign,afterSign];
}

-(NSString *)createMaskForPhone {
    
    NSArray* temp = [self componentsSeparatedByString:@"-"];
    NSString *tempString = [[temp subarrayWithRange:NSMakeRange(1, temp.count-1)] componentsJoinedByString:@""];
    if (tempString == nil) return self;
    
    NSInteger asteriskToIndex = 0;
    if(tempString.length == 1)
        asteriskToIndex = 0;
    else if(tempString.length == 2 || tempString.length == 3)
        asteriskToIndex = 1;
    else if(tempString.length > 3)
        asteriskToIndex = 3;
    
    NSMutableString *asterisks = [NSMutableString stringWithString: temp.count > 2 ? @"*-" : @"1-"];
    for (int i = 0; i < temp.count - 2; i++) {
        [asterisks appendString:@"*"];
    }
    for (int i=0; i< tempString.length; i++) {
        if (i < tempString.length - asteriskToIndex)
            [asterisks appendString:@"*"];
        else
            [asterisks appendString:[tempString substringWithRange:NSMakeRange(i, 1)]];
    }

    return asterisks;
}

- (NSString*)MD5
{
    // Create pointer to the string as UTF8
    const char *ptr = [self UTF8String];
    
    // Create byte array of unsigned chars
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    
    // Create 16 bytes MD5 hash value, store in buffer
    CC_MD5(ptr, strlen(ptr), md5Buffer);
    
    // Convert unsigned char buffer to NSString of hex values
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x",md5Buffer[i]];
    
    return output;
}


-(BOOL)isValidPhoneNumber
{
    NSString *phoneRegex = @"^(1-)*[0-9]{3}(-[0-9]{3})(-[0-9]{4})$";
    NSPredicate *phonePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phonePredicate evaluateWithObject:self];
}

-(BOOL)isValidZipCode
{
    NSString *zipRegex = @"^(\\d{5}(-\\d{4})*)$";
    NSPredicate *zipPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", zipRegex];
    
    return [zipPredicate evaluateWithObject:self];
}

-(BOOL)isValidBinNumber
{

//    NSString *binRegex = @"(^(5)[0-9]{3}(-[0-9]{2})$)";
    NSString *binRegex = @"(^[0-9]{4}(-[0-9]{2})$)";
    NSPredicate *binPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", binRegex];
    return [binPredicate evaluateWithObject:self];
}

-(BOOL)isValidName
{
    return ([self length] > 1 && [self length] <= MAX_FIRSTNAME_TEXT_LENGTH);
}

-(NSString *)removeBlankFromURL
{
    return [self stringByReplacingOccurrencesOfString:@"\\s" withString:@"%20"
                                              options:NSRegularExpressionSearch
                                                range:NSMakeRange(0, [self length])];
}

- (NSUInteger)occurrenceCountOfCharacter:(UniChar)character
{
    CFStringRef selfAsCFStr = (__bridge CFStringRef)self;
    
    CFStringInlineBuffer inlineBuffer;
    CFIndex length = CFStringGetLength(selfAsCFStr);
    CFStringInitInlineBuffer(selfAsCFStr, &inlineBuffer, CFRangeMake(0, length));
    
    NSUInteger counter = 0;
    
    for (CFIndex i = 0; i < length; i++) {
        UniChar c = CFStringGetCharacterFromInlineBuffer(&inlineBuffer, i);
        if (c == character) counter += 1;
    }
    
    return counter;
}

-(NSString *)removeRedudantWhiteSpaceInText
{
    NSMutableString *removeWhiteSpace = [[NSMutableString alloc] initWithString:[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    return [removeWhiteSpace stringByReplacingOccurrencesOfString:@"\\s+" withString:@" " options: NSRegularExpressionSearch range: NSMakeRange(0, removeWhiteSpace.length)];
}

-(NSString *)removeRedudantNewLineInText
{
    NSString *newString = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    newString = [newString stringByReplacingOccurrencesOfString:@"\r"
                                                     withString:@""
                                                        options:NSRegularExpressionSearch
                                                          range:NSMakeRange(0, newString.length)];
    
    
    newString = [newString stringByReplacingOccurrencesOfString:@"\n\n"
                                                     withString:@"\n"
                                                        options:NSRegularExpressionSearch
                                                          range:NSMakeRange(0, newString.length)];
    newString = [newString stringByReplacingOccurrencesOfString:@">&nbsp; &nbsp;"
                                                     withString:@"&nbsp;"
                                                        options:NSRegularExpressionSearch
                                                          range:NSMakeRange(0, newString.length)];
    
    newString = [newString stringByReplacingOccurrencesOfString:@"<p>&nbsp;</p>"
                                                     withString:@"\n"
                                                        options:NSRegularExpressionSearch
                                                          range:NSMakeRange(0, newString.length)];
    
    newString = [newString stringByReplacingOccurrencesOfString:@"<p>\n</p>"
                                                     withString:@"\n"
                                                        options:NSRegularExpressionSearch
                                                          range:NSMakeRange(0, newString.length)];
    
    
    newString = [newString stringByReplacingOccurrencesOfString:@"\n<p>"
                                                     withString:@"<p>"
                                                        options:NSRegularExpressionSearch
                                                          range:NSMakeRange(0, newString.length)];
    newString = [newString stringByReplacingOccurrencesOfString:@"<br/><br/>"
                                                     withString:@""
                                                        options:NSRegularExpressionSearch
                                                          range:NSMakeRange(0, newString.length)];
    newString = [newString stringByReplacingOccurrencesOfString:@"<p><b> </p>"
                                                     withString:@""
                                                        options:NSRegularExpressionSearch
                                                          range:NSMakeRange(0, newString.length)];
    newString = [newString stringByReplacingOccurrencesOfString:@"<p style=\"margin-left:6.0pt\">&nbsp;</p>"
                                                     withString:@""
                                                        options:NSRegularExpressionSearch
                                                          range:NSMakeRange(0, newString.length)];
    
    
    newString = [newString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return newString;
}

-(NSString *)replaceWhiteSpacingTag
{
   NSString *newString = [self stringByReplacingOccurrencesOfString:@"&nbsp;"
                                                     withString:@" "
                                                        options:NSRegularExpressionSearch
                                                          range:NSMakeRange(0, self.length)];
    return newString;
}

//- (NSString*)removeSpecifiedSentencesOfHTMLText{
//    NSString *newString = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//    newString = [newString stringByReplacingOccurrencesOfString:@"Click here OR"
//                                                     withString:@""
//                                                        options:NSRegularExpressionSearch
//                                                          range:NSMakeRange(0, newString.length)];
//    
//    newString = [newString stringByReplacingOccurrencesOfString:@"Click here OR"
//                                                     withString:@""
//                                                        options:NSRegularExpressionSearch
//                                                          range:NSMakeRange(0, newString.length)];
//    
//    newString = [newString stringByReplacingOccurrencesOfString:@"Browse and Book Sightseeing & Tours Worldwide"
//                                                     withString:@""
//                                                        options:NSRegularExpressionSearch
//                                                          range:NSMakeRange(0, newString.length)];
//    
//    newString = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//    return newString;
//}
@end
