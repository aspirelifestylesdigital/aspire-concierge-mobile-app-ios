//
//  UITextField+Extensions.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/4/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "UITextField+Extensions.h"
#import "Common.h"
#import "Constant.h"
#import "AppColor.h"
#import "NSString+Utis.h"
#import <objc/runtime.h>

static char* const BORDER = "border_for_textfield";
static char* const ORIGIN_TEXT = "origin_text_textfield";
static char* const IS_TEXT_MASKED = "IS_TEXT_MASKED";

@implementation UITextField (Extensions)

- ( void)setBottomBorderRedColor
{
    [self clearBorder];
    CALayer *borderLayer = [CALayer layer];
    [borderLayer setName:@"red"];
    CGFloat borderWidth = 1.0f;
    borderLayer.borderColor = [UIColor redColor].CGColor;
    borderLayer.frame = CGRectMake(0.0f, self.frame.size.height - borderWidth, self.frame.size.width, 1.0f);
    borderLayer.borderWidth = borderWidth;
    [self.layer addSublayer:borderLayer];
    self.layer.masksToBounds = YES;
    objc_setAssociatedObject(self, BORDER, borderLayer, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


- (void)setBottomBolderDefaultColor
{
    [self clearBorder];
    CALayer *borderLayer = [CALayer layer];
    [borderLayer setName:@"default"];
    CGFloat borderWidth = 1.0f;
    borderLayer.borderColor = [AppColor childViewBackgroundColor].CGColor;
    borderLayer.frame = CGRectMake(0.0f, self.frame.size.height - borderWidth, self.frame.size.width, 1.0f);
    borderLayer.borderWidth = borderWidth;
    [self.layer addSublayer:borderLayer];
    self.layer.masksToBounds = YES;
    objc_setAssociatedObject(self, BORDER, borderLayer, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void) refreshBorder {
    if (objc_getAssociatedObject(self, BORDER)) {
        CALayer* border = (CALayer*)objc_getAssociatedObject(self, BORDER);
        if ([border.name isEqualToString:@"red"]) {
            [self setBottomBorderRedColor];
        } else {
            [self setBottomBolderDefaultColor];
        }
    }
}

- (void) clearBorder {
    for (CALayer* ca in self.layer.sublayers.reverseObjectEnumerator) {
        if ([ca.name isEqualToString:@"red"] || [ca.name isEqualToString:@"default"]) {
            [ca removeFromSuperlayer];
        }
    }
}

-(void)updateCursorPositionAtRange:(NSRange)range {
    UITextPosition *start = [self positionFromPosition:[self beginningOfDocument]
                                                 offset:range.location];
    UITextPosition *end = [self positionFromPosition:start
                                               offset:range.length];
    [self setSelectedTextRange:[self textRangeFromPosition:start toPosition:end]];
}

-(void)setTextStyleForText:(NSString *)text WithFontName:(NSString *)fontName WithColor:(UIColor *)color WithTextSize:(float)textSize WithTextCenter:(BOOL)isCenter WithSpacing:(float)spacing ForPlaceHolder:(BOOL)isPlaceHolder;
{
    NSRange range = NSMakeRange(0, text.length);
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:text];
    [attributeString addAttribute:NSKernAttributeName value:@(spacing) range:range];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont fontWithName:fontName size:textSize*SCREEN_SCALE] range:range];
    [attributeString addAttribute:NSForegroundColorAttributeName value:color range:range];
    
    if(isCenter)
    {
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init] ;
        [paragraphStyle setAlignment:NSTextAlignmentCenter];
        [attributeString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    }
    
    if(isPlaceHolder){
       self.attributedPlaceholder = attributeString;
    }
    else{
        self.attributedText = attributeString;
    }
}

- (void)showAsteriskWithoutMargin:(CGFloat)margin {
    if (self.leftView) self.leftView = nil;
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, margin, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [self setLeftView:leftView];
    [self setLeftViewMode:UITextFieldViewModeAlways];
}

-(void) setMarginLeftRight:(CGFloat)margin {
    
    if (self.leftView) self.leftView = nil;
    if (self.rightView) self.rightView = nil;
    
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, margin, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    UIView *rView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, margin, 30)];
    [rView setBackgroundColor:[UIColor clearColor]];
    [self setLeftView:leftView];
    [self setLeftViewMode:UITextFieldViewModeAlways];
    [self setRightView:rView];
    [self setRightViewMode:UITextFieldViewModeAlways];
}

-(void) showAsterisk:(CGFloat)margin {
    
    if (self.leftView) self.leftView = nil;
    if (self.rightView) self.rightView = nil;
    
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, margin, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [self setLeftView:leftView];
    [self setLeftViewMode:UITextFieldViewModeAlways];
    UIImageView* asteriskImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"asterisk_icon"]];
    asteriskImage.frame = CGRectMake(0.0f, 0.0f, 20.0f, 20.0f);
    asteriskImage.contentMode = UIViewContentModeCenter;
    [self setRightView:asteriskImage];
    [self setRightViewMode:UITextFieldViewModeAlways];
}

- (void)showTextMask:(BOOL)isEmail {
//    if ([self isTextMasked]) return;
    if (self.text.length > 0) {
        self.text = [self.text createMaskForText:isEmail];
//        objc_setAssociatedObject(self, IS_TEXT_MASKED, @(true), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
}

- (void)showTextMaskPhone {
//    if ([self isTextMasked]) return;
    if (self.text.length > 0) {
        self.text = [self.text createMaskForPhone];
//        objc_setAssociatedObject(self, IS_TEXT_MASKED, @(true), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
}

- (NSString *) originText {
    if (objc_getAssociatedObject(self, ORIGIN_TEXT)) {
        return objc_getAssociatedObject(self, ORIGIN_TEXT);
    }
    return self.text;
}

- (void) setOriginText:(NSString *)text {
#if DEBUG
    NSLog(@"%s => %@",__PRETTY_FUNCTION__,text);
#endif
    objc_setAssociatedObject(self, ORIGIN_TEXT, text, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)unMasked {
    objc_setAssociatedObject(self, IS_TEXT_MASKED, @(false), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL) isTextMasked {
    if (objc_getAssociatedObject(self, IS_TEXT_MASKED))
        return [objc_getAssociatedObject(self, IS_TEXT_MASKED) boolValue];
    return false;
}
@end
