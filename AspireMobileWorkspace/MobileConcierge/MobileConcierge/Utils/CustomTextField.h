//
//  CustomTextField.h
//  LuxuryCard
//
//  Created by Viet Vo on 9/6/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTextField : UITextField

- (void) mask;
- (void) maskFixedStars;

@end
