//
//  CustomTextField.m
//  LuxuryCard
//
//  Created by Viet Vo on 9/6/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "CustomTextField.h"
#import "UITextField+Extensions.h"
#import "Constant.h"
@import AspireApiControllers;

@implementation CustomTextField

- (void)layoutSubviews {
    [super layoutSubviews];
    [self refreshBorder];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender{
    return NO;
}

- (void) mask {
    if ([AACStringUtils isEmptyWithString:self.text] == false) {
        NSString* stars = @"";
        stars = [stars stringByPaddingToLength:self.text.length withString:@"*" startingAtIndex:0];
        self.text = stars;
    }
}

- (void) maskFixedStars {
    self.text = MASKED_ANSWER;

}
@end
