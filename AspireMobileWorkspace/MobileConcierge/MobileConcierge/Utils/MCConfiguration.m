//
//  MCConfiguration.m
//  MobileConcierge
//
//  Created by VuongTC on 10/30/18.
//  Copyright © 2018 s3Corp. All rights reserved.
//

#import "MCConfiguration.h"

@implementation MCConfiguration
+ (NSDictionary*) getConfigurations {
    return kConfigurations;
}
@end
