//
//  Common.m
//  Reservations
//
//  Created by Linh Le on 10/22/12.
//  Copyright (c) 2012 Linh Le. All rights reserved.
//

#import "Common.h"
#import "NSString+Utis.h"
#import "Constant.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "NSString+HTML.h"
#import "AppDelegate.h"
#import <sys/sysctl.h>
#import "Reachability.h"
#import "UserObject.h"
#import "Constant.h"

#define SPECIAL_CHARACTER @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%&*()-+_=[]:;\",.?/ "

@implementation Common

int NUMBER_ROW_UPDATED = 20;

static NSArray *SHORT_MONTH;
ScreenSize screenSize = UNDEFINED;

ScreenSize getScreenSize()
{
    if (screenSize == UNDEFINED) {
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        if (screenBounds.size.height == 568) {
            // code for 4-inch screen
            screenSize = FOUR_INCH;
        }
        else if(screenBounds.size.height == 480) {
            // code for 3.5-inch screen
            screenSize = THREE_DOT_FIVE_INCH;
        }        
    }
    
    return screenSize;
}

NSString* getDistance(float item)
{
    // Get distance
    NSString *result;
    
    if (item >= 1000)
    {
        result = [[NSString alloc] initWithFormat:@"%.1f km", item /1000 ] ;
    }
    else
    {
        int intDistance = roundf(item);
        result = [[NSString alloc] initWithFormat:@"%d m", intDistance] ;
        
    }

    return result;
}

NSString* urlencode(NSString* url)
{
    NSMutableString * output = [NSMutableString string];
    const unsigned char * source = (const unsigned char *)[url UTF8String];
    int sourceLen = (int)strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}

NSString* getUniqueIdentifier()
{
    NSString *uDID = [[NSUserDefaults standardUserDefaults] objectForKey:@"UDID"];
    if (uDID == nil) {
        // create UDID
        CFUUIDRef theUUID = CFUUIDCreate(NULL);
        CFStringRef string = CFUUIDCreateString(NULL, theUUID);
        CFRelease(theUUID);
        uDID = (__bridge  NSString *)string;
        // store to user default
        [[NSUserDefaults standardUserDefaults] setObject:uDID forKey:@"UDID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    return uDID;
}

NSString* getPhotoUrlBySizeAndSecret(NSString * oldUrl, int width, int height, NSString *secret)
{
    if (!oldUrl || !secret) {
        return nil;
    }
    
    NSString *baseUrl = [oldUrl stringByDeletingLastPathComponent];
    NSString *photoName = [oldUrl lastPathComponent];
    NSRange dotRange = [photoName rangeOfString:@"."];
    NSString *photoBaseName = [photoName substringToIndex:dotRange.location];
    NSString *photoExt = [photoName substringFromIndex:dotRange.location+1];

    NSString *photoCombineInfo = [NSString stringWithFormat:@"%@_%dx%d.%@%@", photoBaseName, width, height, photoExt, secret];
    NSString *sig = [[photoCombineInfo MD5] substringToIndex:10];
    
    return [NSString stringWithFormat:@"%@/%@_%dx%d_%@.%@", baseUrl, photoBaseName, width, height, sig, photoExt];
}


NSString* reverseString(NSString *inputString)
{
    int len =(int) [inputString length];
    NSMutableString *reverseName = [[NSMutableString alloc] initWithCapacity:len];
    for(int i=len-1;i>=0;i = i - 2)
    {
        [reverseName appendString:[NSString stringWithFormat:@"%c%c",[inputString characterAtIndex:i - 1], [inputString characterAtIndex:i]]];
    }
    return reverseName;
}

// flatent html
NSString* flattenHTML(NSString *html)
{
	
    NSScanner *theScanner;
    NSString *text = nil;
	
    theScanner = [NSScanner scannerWithString:html];
	
    while ([theScanner isAtEnd] == NO) {
		
        // find start of tag
        [theScanner scanUpToString:@"<" intoString:NULL] ;
		
        // find end of tag
        [theScanner scanUpToString:@">" intoString:&text] ;
		
        // replace the found tag with a space
        //(you can filter multi-spaces out later if you wish)
        
        
        html = [html stringByReplacingOccurrencesOfString:
				[ NSString stringWithFormat:@"%@>", text]
											   withString:@" "];
        
        
		
    }
    
    return html;
}

//scale image
UIImage* scaleImageWithSize(UIImage *imagetemp ,CGSize newSize)
{
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [imagetemp drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

NSString* formatDateToString(NSDate* date, NSString*format)
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    
    return [dateFormatter stringFromDate:date];
}


NSString* formatFullDate(NSDate* date,BOOL isInAMPM)
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if (isInAMPM) {
        [dateFormatter setDateFormat:@"dd MMMM yyyy hh:mm a"];
    }
    else
    {
        [dateFormatter setDateFormat:@"dd MMMM yyyy HH:mm"];
    }
    return [dateFormatter stringFromDate:date];
}

NSString* formatFullDateForReview(NSDate* date,BOOL isInAMPM)
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if (isInAMPM) {
        [dateFormatter setDateFormat:@"dd MMMM yyyy@%@hh:mm a"];
    }
    else
    {
        [dateFormatter setDateFormat:@"dd MMMM yyyy@%@HH:mm"];
    }
    NSString* returString =[dateFormatter stringFromDate:date];
    returString = [returString stringByReplacingOccurrencesOfString:@"am" withString:@"AM"];
    returString = [returString stringByReplacingOccurrencesOfString:@"pm" withString:@"PM"];
    return returString;
}

NSString* formatDateForBooking(NSDate *date){
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    return [dateFormatter stringFromDate:date];
}

extern NSString* formatDateForContent(NSDate *date)
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMMM yyyy"];
    //[dateFormatter setDateStyle:NSDateFormatterLongStyle]; // day, Full month and year
    //[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    return [dateFormatter stringFromDate:date];
}

NSString* dateToTimeStampForBooking(NSDate *date)
{
    if (date != nil) {
        NSTimeZone *timeZone = [NSTimeZone localTimeZone];
        return [NSString stringWithFormat:@"%.0f",[date timeIntervalSince1970] + [timeZone secondsFromGMTForDate:date]];
    }
    return nil;
}

NSString* formatDateForBookingDisplay(NSDate *date){
    SHORT_MONTH = [NSArray arrayWithObjects:@"Jan",@"Feb",@"Mar",@"Apr",@"May", @"Jun", @"Jul", @"Aug",@"Sep", @"Oct",@"Nov", @"Dec", nil];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    NSDateFormatter *d = [[NSDateFormatter alloc] init];
    [d setDateFormat:@"EEE"];
    NSString *day = [d stringFromDate:date];
    [d setDateFormat:@"dd"];
    NSString *dateInMonth = [d stringFromDate:date];
    return [NSString stringWithFormat:@"%@, %@ %@", day, [SHORT_MONTH objectAtIndex:[components month]-1], dateInMonth];
}

NSString* formatDate(NSDate* date, NSString *format){
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    return [dateFormatter stringFromDate:date];

}

NSDate* adjustDateToFirstTime(NSDate* date){
    NSDate *adjustedDate;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMMM yyyy"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    
    [dateFormatter setFormatterBehavior:NSDateFormatterBehaviorDefault];
    [dateFormatter setDateFormat:@"dd MMMM yyyy HH:mm"];
    adjustedDate = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@ %@", formattedDate, @"08:00"]];
    
    return adjustedDate;
}

NSString* formatTimeForSearch(NSDate *date){
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    NSString *time = [formatter stringFromDate:date];
    if(time.length > 0){
        NSString *firstChar = [time substringToIndex:1];
        if([firstChar isEqualToString:@"0"]){
            time = [time substringFromIndex:1];
        }
    }
    return time;
}

NSString* formatTimeSlot(NSInteger timeSlotId)
{
    if(timeSlotId == 0){
        return @"00:00";
    }
    
    NSMutableString *timeSlot = [NSMutableString stringWithFormat:@"%ld", (long)timeSlotId];
    if([timeSlot length] <=2 ){
        [timeSlot insertString:@"0:" atIndex:0];
    }
    else if ([timeSlot length] == 3) {
        [timeSlot insertString:@":" atIndex:1];
    }
    else
    {
        [timeSlot insertString:@":" atIndex:2];
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehaviorDefault];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSDate *intime = [dateFormatter dateFromString:timeSlot];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *time = [[dateFormatter stringFromDate:intime] uppercaseString];

    if(time.length > 0){
        NSString *firstChar = [time substringToIndex:1];
        if([firstChar isEqualToString:@"0"]){
            time = [time substringFromIndex:1];
        }
    }
    return time;
}

// return distance day
NSString* getDistanceDay(NSDate* starDay)
{
    int interval = [[NSDate date] timeIntervalSinceDate:starDay];
    
    NSString *timeUnit;
    int timeValue = 0;
    if(timeValue<0){
        timeValue = interval;
        timeUnit = @"just now";
    }else  if (interval < 60)
    {
        timeValue = interval;
        timeUnit = @"seconds";
    }
    else if (interval< 3600)
    {
        timeValue = round(interval / 60);
        
        if (timeValue == 1) {
            timeUnit = @"minute";
        } else {
            timeUnit = @"minutes";
        }
    }
    else if (interval< 86400) {
        timeValue = round(interval / 60 / 60);
        
        if (timeValue == 1) {
            timeUnit = @"hour";
            
        } else {
            timeUnit = @"hours";
        }
    }
    else if (interval < 604800)
    {
        int diff = round(interval / 60 / 60 / 24);
        if (diff == 1)
        {
            timeUnit = @"yesterday";
        }
        else if (diff == 7)
        {
            timeUnit = @"last week";
        }
        else
        {
            timeValue = diff;
            timeUnit = @"days";
        }
    }
    else
    {
        int days = round(interval / 60 / 60 / 24);
        
        if (days < 7) {
            
            timeValue = days;
            
            if (timeValue == 1) {
                timeUnit = @"day";
            } else {
                timeUnit = @"days";
            }
            
        } else if (days < 30) {
            int weeks = days / 7;
            
            timeValue = weeks;
            
            if (timeValue == 1) {
                timeUnit = @"week";
            } else {
                timeUnit = @"weeks";
            }
            
        } else if (days < 365) {
            
            int months = days / 30;
            timeValue = months;
            
            if (timeValue == 1) {
                timeUnit = @"month";
            } else {
                timeUnit = @"months";
            }
            
        } else if (days < 30000)
        {
            // this is roughly 82 years. After that, we'll say 'forever'
            int years = days / 365;
            timeValue = years;
            
            if (timeValue == 1) {
                timeUnit = @"year";
            } else {
                timeUnit = @"years";
            }
            
        } else {
            return @"forever ago";
        }
    }
    
    if(timeValue == 0)
    {
        return [NSString stringWithFormat:@"%@", timeUnit];
    }
    return [NSString stringWithFormat:@"%d %@ ago", timeValue, timeUnit];
}

// return 64 base encode
NSString* base64forData(NSData* theData)
{
    
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

NSMutableAttributedString* letterSpacing(float spacing, NSString* string){
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    [attributedString addAttribute:NSKernAttributeName
                             value:@(spacing)
                             range:NSMakeRange(0, [string length] - 1)];
    
    return attributedString;
}


NSDate* convertUnixTimeToDate(double timestamp)
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];
    return date;
}

float iosVersion()
{
    NSString *ver = [[UIDevice currentDevice] systemVersion];
    float ver_float = [ver floatValue];
    return ver_float;
}

BOOL isSameDay(NSDate* date1, NSDate*date2)
{
    if (date1 == nil || date2 == nil) {
        return NO;
    }
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}


NSString *formatHTMLString(NSString *htmlString, NSString * tag, int width, int height){
    
    NSRegularExpression *regularExpression = [NSRegularExpression regularExpressionWithPattern:[NSString stringWithFormat:@"<%@%@",tag,@"[^>]+(/>|>){1}"] options:0 error:nil];
    NSArray *matchResult = [regularExpression matchesInString:htmlString options:0 range:NSMakeRange(0, [htmlString length])];
    if(matchResult && (matchResult.count>0)){
        NSRegularExpression *regularWidth = [NSRegularExpression regularExpressionWithPattern:@"width=\"[0-9]+" options:0 error:nil];
        NSRegularExpression *regularHeight = [NSRegularExpression regularExpressionWithPattern:@"height=\"[0-9]+" options:0 error:nil];

        NSString *matchNumber;
        NSString *matchedItem;
        NSTextCheckingResult *tempRange;
        NSMutableArray *matchedStringResults = [NSMutableArray array];
        int number;
        for (NSTextCheckingResult *textRange in matchResult) {
//            textRange = [matchResult objectAtIndex:i];
            matchedItem = [htmlString substringWithRange:textRange.range];
            // find width to replace values. If havent got these attributes, add them in
            //find Width
            tempRange = [regularWidth firstMatchInString:matchedItem options:0 range:NSMakeRange(0, matchedItem.length)];
            matchNumber = [matchedItem substringWithRange:tempRange.range];
            if (matchNumber && (matchNumber.length > 0)) {
                number  = [[matchNumber stringByReplacingOccurrencesOfString:@"width=\"" withString:@""] intValue];
                if (number > width) {
                    matchedItem = [matchedItem stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"width=\"%d", number] withString:[NSString stringWithFormat:@"width=\"%d", width]];
                }
            } else {
                matchedItem = [matchedItem stringByReplacingOccurrencesOfString:tag withString:[NSString stringWithFormat:@"%@ width=\"%d\"", tag, width]];
            }
            
            //find height
            if(height > 0){
                tempRange = [regularHeight firstMatchInString:matchedItem options:0 range:NSMakeRange(0, matchedItem.length)];
                matchNumber = [matchedItem substringWithRange:tempRange.range];
                if (matchNumber && (matchNumber.length > 0)) {
                    number  = [[matchNumber stringByReplacingOccurrencesOfString:@"height=\"" withString:@""] intValue];
                    if (number > height) {
                        matchedItem = [matchedItem stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"height=\"%d", number] withString:[NSString stringWithFormat:@"height=\"%d", height]];
                    }
                } else {
                    matchedItem = [matchedItem stringByReplacingOccurrencesOfString:tag withString:[NSString stringWithFormat:@"%@ height=\"%d\"", tag, height]];
                }
                
            }
            [matchedStringResults addObject:[NSArray arrayWithObjects:[htmlString substringWithRange:textRange.range], matchedItem, nil]];
        }                  

        for (int i=0; i<matchedStringResults.count;i++){
            htmlString = [htmlString stringByReplacingOccurrencesOfString:[(NSArray*)[matchedStringResults objectAtIndex:i] objectAtIndex:0] withString:[(NSArray*)[matchedStringResults objectAtIndex:i] objectAtIndex:1]];
        }

    }
    return htmlString;
}

UIImageView* createCricleMask(UIImageView *img,int radiusConner)
{
    img.layer.cornerRadius = radiusConner;
    img.layer.masksToBounds = YES;
    return img;
}

NSDate* countDateNextMonth(int month){
    NSCalendar *cal = [[NSCalendar alloc ] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSTimeZone *zone = [NSTimeZone timeZoneWithName:@"GMT"];
    [cal setTimeZone:zone];
    NSDateComponents *components = [[NSDateComponents alloc] init] ;
    // Update the start date
    [components setMonth:month];
    return [cal dateByAddingComponents:components toDate:[NSDate date] options:0];
}

NSDate* countDateNextHour(int hour){
    NSCalendar *cal = [[NSCalendar alloc ] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSTimeZone *zone = [NSTimeZone timeZoneWithName:@"GMT"];
    [cal setTimeZone:zone];
    NSDateComponents *components = [[NSDateComponents alloc] init] ;
    // Update the start date
    [components setHour:hour];
    return [cal dateByAddingComponents:components toDate:[NSDate date] options:0];
}

NSDate* countDateNextMinute(int minute){
    NSCalendar *cal = [[NSCalendar alloc ] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSTimeZone *zone = [NSTimeZone timeZoneWithName:@"GMT"];
    [cal setTimeZone:zone];
    NSDateComponents *components = [[NSDateComponents alloc] init] ;

    [components setMinute:minute];
    return [cal dateByAddingComponents:components toDate:[NSDate date] options:0];
}

NSString * removeZeroTrail(NSString * stringSource)
{
    // This function use to remove zero trail in latitude and longitude to prevent error signature encode
    int lenStringSource = [stringSource length];
    NSString *lastChar = [stringSource substringFromIndex:lenStringSource -1];
    if ([lastChar isEqualToString:@"0"])
    {
        return removeZeroTrail([stringSource substringToIndex:lenStringSource -1]);
    }
    else
    {
        return stringSource;
    }
}
NSString* getMonthName(NSDate *date){
    SHORT_MONTH = [NSArray arrayWithObjects:@"JANUARY",@"FEBRUARY",@"MARCH",@"APRIL",@"MAY", @"JUNE", @"JULY", @"AUGUST",@"SEPTEMBER", @"OCTOBER",@"NOVEMBER", @"DECEMBER", nil];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    return [NSString stringWithFormat:@"%@", [SHORT_MONTH objectAtIndex:[components month]-1]];
    
    
}
NSString* getMonthNamePromotion(NSDate *date){
    SHORT_MONTH = [NSArray arrayWithObjects:@"January",@"February",@"March",@"April",@"May", @"June", @"July", @"August",@"September", @"October",@"November", @"December", nil];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    return [NSString stringWithFormat:@"%@", [SHORT_MONTH objectAtIndex:[components month]-1]];
    
    
}
NSString* getDayOfWeek(NSDate *date)
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat:@"EEEE"];
    return [dateFormatter stringFromDate:date];
}

NSString* getDateOfMonth(NSDate *date)
{
     NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    switch ([components day]) {
        case 1:
//        case 11:
        case 21:
        case 31:
            return @"ST";
            break;
        case 2:
//        case 12:
        case 22:
            return @"ND";
            break;
        case 3:
//        case 13:
        case 23:
            return @"RD";
        default:
            break;
    }
    return @"TH";
}

NSString* getHourOfDate(NSDate *date)
{
     NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:date];
    int hour =(int) [components hour];
    if(hour > 12)
    {
        return [NSString stringWithFormat:@"%liPM",[components hour] - 12];
    }
    return [NSString stringWithFormat:@"%liAM",(long)[components hour]];
}
CGSize getSizeWithFont(NSString *string,UIFont *font)
{
    CGSize contentSize;
    if(iosVersion() < 7.0)
    {
        contentSize=[string sizeWithFont:font constrainedToSize:CGSizeMake(320, 999) lineBreakMode:NSLineBreakByWordWrapping];
    }
    else
    {
        
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    font,NSFontAttributeName,NSLineBreakByWordWrapping,NSParagraphStyleAttributeName, nil];
        
        CGRect rect = [string boundingRectWithSize:CGSizeMake(320, 999) options:NSStringDrawingUsesLineFragmentOrigin  attributes:attributes context:nil ];
        contentSize =rect.size;
        
    }
    return contentSize;
}

CGSize getSizeWithFontAndFrame(NSString *string,UIFont *font,int width,int height)
{
    CGSize contentSize;
    if(iosVersion() < 7.0)
    {
        contentSize=[string sizeWithFont:font constrainedToSize:CGSizeMake(width, height) lineBreakMode:NSLineBreakByWordWrapping];
    }
    else
    {
        
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    font,NSFontAttributeName,NSLineBreakByWordWrapping,NSParagraphStyleAttributeName, nil];
        
        CGRect rect = [string boundingRectWithSize:CGSizeMake(width, height) options:NSStringDrawingUsesLineFragmentOrigin  attributes:attributes context:nil ];
        contentSize =rect.size;
        
    }
    return contentSize;
}



NSString* stringByStrippingHTML(NSString* data) {
    NSRange r;
    NSString *s = data;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound){
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    }
    
    s = [s stringByDecodingHTMLEntities];
    return s;
}


void resetScaleViewBaseOnScreen(UIView* view){
    if(view == nil){
        return;
    }
    
    if([view isKindOfClass:[UITextField class]]){
        NSLayoutConstraint *heightConstraint = getHeightConstranint(view);
        if(heightConstraint!=nil){
            heightConstraint.constant = heightConstraint.constant*SCREEN_SCALE;
        }
        
        NSLayoutConstraint *widthConstraint = getWidthConstraint(view);
        if(widthConstraint)
        {
            widthConstraint.constant = widthConstraint.constant * SCREEN_SCALE;
        }
        
        UIFont* font = ((UITextField*) view).font;
        [((UITextField*) view) setFont:[UIFont fontWithName:font.fontName size:font.pointSize*SCREEN_SCALE]];
    } else if([view isKindOfClass:[UITextView class]]){
        NSLayoutConstraint *heightConstraint = getHeightConstranint(view);
        if(heightConstraint!=nil){
            heightConstraint.constant = heightConstraint.constant*SCREEN_SCALE;
        }
        UIFont* font = ((UITextView*) view).font;
        [((UITextView*) view) setFont:[UIFont fontWithName:font.fontName size:font.pointSize*SCREEN_SCALE]];
    } else if([view isKindOfClass:[UIButton class]]){
        NSLayoutConstraint *heightConstraint = getHeightConstranint(view);
        if(heightConstraint!=nil){
            heightConstraint.constant = heightConstraint.constant*SCREEN_SCALE;
        }
        
        NSLayoutConstraint *widthConstraint = getWidthConstraint(view);
        if(widthConstraint)
        {
            widthConstraint.constant = widthConstraint.constant * SCREEN_SCALE;
        }
        
        UIFont* font = ((UIButton*) view).titleLabel.font;
        [((UIButton*) view).titleLabel setFont:[UIFont fontWithName:font.fontName size:font.pointSize*SCREEN_SCALE]];
    } else if([view isKindOfClass:[UILabel class]]){
        NSLayoutConstraint *heightConstraint = getHeightConstranint(view);
        if(heightConstraint!=nil){
            heightConstraint.constant = heightConstraint.constant*SCREEN_SCALE;
        }
        
        NSLayoutConstraint *widthConstraint = getWidthConstraint(view);
        if(widthConstraint)
        {
            widthConstraint.constant = widthConstraint.constant * SCREEN_SCALE;
        }
        UIFont* font = ((UILabel*) view).font;
        [((UILabel*) view) setFont:[UIFont fontWithName:font.fontName size:font.pointSize*SCREEN_SCALE]];
    }
    else if([view isKindOfClass:[UIImageView class]]){
        NSLayoutConstraint *heightConstraint = getHeightConstranint(view);
        if(heightConstraint!=nil){
            heightConstraint.constant = heightConstraint.constant*SCREEN_SCALE;
        }
        
        NSLayoutConstraint *widthConstraint = getWidthConstraint(view);
        if(widthConstraint)
        {
            widthConstraint.constant = widthConstraint.constant * SCREEN_SCALE;
        }
    }
    
    else if(([view isKindOfClass:[UIView class]] || [view isKindOfClass:[UIScrollView class]] || [view isKindOfClass:[UIStackView class]])){
        NSLayoutConstraint *heightConstraint = getHeightConstranint(view);
        if(heightConstraint!=nil){
            heightConstraint.constant = heightConstraint.constant*SCREEN_SCALE;
        }
        
        NSLayoutConstraint *widthConstraint = getWidthConstraint(view);
        if(widthConstraint)
        {
            widthConstraint.constant = widthConstraint.constant * SCREEN_SCALE;
        }
        for (UIView* sub in [view subviews]) {
            resetScaleViewBaseOnScreen(sub);
        }
    }
}

void resetScaleIndividualViewBaseOnScreenSize(UIView *view)
{
    NSLayoutConstraint *heightConstraint = getHeightConstranint(view);
    if(heightConstraint!=nil){
        heightConstraint.constant = heightConstraint.constant*SCREEN_SCALE;
    }
    
    NSLayoutConstraint *widthConstraint = getWidthConstraint(view);
    if(widthConstraint)
    {
        widthConstraint.constant = widthConstraint.constant * SCREEN_SCALE;
    }
    
    NSLayoutConstraint *topConstraint = getTopConstraint(view);
    if(topConstraint)
    {
        topConstraint.constant = topConstraint.constant * SCREEN_SCALE;
    }
    
    NSLayoutConstraint *bottomConstraint = getBottomConstraint(view);
    if(bottomConstraint)
    {
        bottomConstraint.constant = bottomConstraint.constant * SCREEN_SCALE;
    }
}


NSLayoutConstraint* getHeightConstranint(UIView* view){
    NSLayoutConstraint *heightConstraint;
    for (NSLayoutConstraint *constraint in view.constraints) {
        if (constraint.firstAttribute == NSLayoutAttributeHeight) {
            return constraint;
        }
    }
    return heightConstraint;
}

NSLayoutConstraint* getWidthConstraint(UIView *view)
{
    NSLayoutConstraint *widthConstraint;
    for (NSLayoutConstraint *constraint in view.constraints) {
        if (constraint.firstAttribute == NSLayoutAttributeWidth) {
            return constraint;
        }
    }
    return widthConstraint;
}

NSLayoutConstraint* getTopConstraint(UIView *view)
{
    NSLayoutConstraint *topConstraint;
    for (NSLayoutConstraint *constraint in view.superview.constraints) {
        if ((constraint.firstItem == view && constraint.firstAttribute == NSLayoutAttributeTop) || (constraint.secondItem == view && constraint.secondAttribute == NSLayoutAttributeTop)) {
            return constraint;
        }
    }
    return topConstraint;
}

NSLayoutConstraint* getBottomConstraint(UIView *view)
{
    NSLayoutConstraint *bottomConstraint;
    for (NSLayoutConstraint *constraint in view.constraints) {
        if ((constraint.firstItem == view && constraint.firstAttribute == NSLayoutAttributeBottom) || (constraint.secondItem == view && constraint.secondAttribute == NSLayoutAttributeBottom)) {
            return constraint;
        }
    }
    return bottomConstraint;
}

NSDate* convertStringToDate(NSString* dateString, NSString* dateFormat){
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    return [dateFormatter dateFromString:dateString];
}

NSDate* convertStringToDateWithTimeZone(NSString* dateString, NSString* dateFormat, NSString* timezone){
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:timezone];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:dateFormat];
    return [dateFormatter dateFromString:dateString];
}


BOOL isValidDateTimeWithin24h(NSDate* date, NSDate* time){
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components1 = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:time];
    NSInteger hour = [components1 hour];
    NSInteger minute = [components1 minute];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
    NSDateComponents *components = [gregorian components: NSUIntegerMax fromDate: date];
    [components setHour: hour];
    [components setMinute: minute];
    [components setSecond: 0];
    NSDate *fullReservation = [gregorian dateFromComponents: components];
    
    if([fullReservation compare:[[NSDate date] dateByAddingTimeInterval:60*60*24*1]] == NSOrderedAscending){
        return NO;
    }
    return YES;
}

BOOL isWithin24h(NSDate* date1, NSDate* date2){
    if([date1 compare:[date2 dateByAddingTimeInterval:60*60*24*1]] == NSOrderedAscending){
        return YES;
    }
    return NO;
}
BOOL isMoreDateh(NSDate* date1, NSDate* date2){
    if([date1 compare:date2] == NSOrderedAscending){
        return YES;
    }
    return NO;
}


UIColor* colorFromHexString(NSString *hexString)
{
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

UIColor* colorFromHexStringWithAlpha(NSString *hexString, CGFloat alpha)
{
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:alpha];
}

NSAttributedString* handleHTMLString(NSString*str, NSString *fontName, CGFloat pointSize, NSString *textColor){
    //    str = [self stringByStrippingHTMLWithString:str];
    str = [str stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; text-align:justify;font-size:%fpx; color: %@}</style>",fontName,pointSize, textColor]];
    
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [str dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                        NSFontAttributeName: [UIFont fontWithName:fontName size:pointSize]}
                                            documentAttributes: nil
                                            error: nil
                                            ];
    return attributedString;
}

NSString* justifyHTMLString(NSString*text, NSString *fontName, CGFloat pointSize, NSString *textColor)
{
    return [NSString stringWithFormat:@"<html><head></head><body style=\"font-family: '%@';text-align:justify; font-size:%fpx; color: %@\"> %@ </body></html>",fontName,pointSize, textColor, text];
}

NSString* centerHTMLString(NSString*text, NSString *fontName, CGFloat pointSize, NSString *textColor)
{
    return [NSString stringWithFormat:@"<html><head></head><body style=\"font-family: '%@';text-align:center; font-size:%fpx; color: %@\"> %@ </body></html>",fontName,pointSize, textColor, text];
}
//haven't review

NSString* stringFromObject(id object)
{
    if (object) {
        if ([object isKindOfClass:[NSString class]]) {
            return object;
        } else if ([object isKindOfClass:[NSNull class]]) {
            return @"";
        }
        else if ([object isKindOfClass:[NSNumber class]]) {
            return [object stringValue];
        }
        return [object description];
    }
    return @"";
}


#pragma mark - Get Top Controller


BOOL isNetworkAvailable()
{
    Reachability *reachability = [Reachability reachabilityWithHostname:@"www.google.com"];
    [reachability startNotifier];
    
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if(status == NotReachable)
    {
        //No internet
        [reachability stopNotifier];
        
        return NO;
    }
    else if (status == ReachableViaWiFi)
    {
        //WiFi
        [reachability stopNotifier];
        
        return YES;
    }
    else if (status == ReachableViaWWAN)
    {
        //3G
        [reachability stopNotifier];
        
        return YES;
    }
    [reachability stopNotifier];
    
    return YES;
}


#pragma mark - Check Device

NSString* platformType(NSString *platform)
{
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"iPhone 4 (Verizon)";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5 (GSM)";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM)";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s (GSM)";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    if ([platform isEqualToString:@"iPhone8,1"])    return @"iPhone 6s";
    if ([platform isEqualToString:@"iPhone8,2"])    return @"iPhone 6s Plus";
    if ([platform isEqualToString:@"iPhone8,4"])    return @"iPhone SE";
    if ([platform isEqualToString:@"iPhone9,1"])    return @"iPhone 7";
    if ([platform isEqualToString:@"iPhone9,3"])    return @"iPhone 7";
    if ([platform isEqualToString:@"iPhone9,2"])    return @"iPhone 7 Plus";
    if ([platform isEqualToString:@"iPhone9,4"])    return @"iPhone 7 Plus";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    if ([platform isEqualToString:@"iPod6,1"])      return @"iPod Touch 6G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad 1G";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (Wi-Fi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2 (Wi-Fi)";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini (Wi-Fi)";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini (GSM)";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (Wi-Fi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3 (GSM)";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4 (Wi-Fi)";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4 (GSM)";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air (Wi-Fi)";
    if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air (Cellular)";
    if ([platform isEqualToString:@"iPad4,3"])      return @"iPad Air (China)";
    if ([platform isEqualToString:@"iPad4,4"])      return @"iPad Mini 2G (Wi-Fi)";
    if ([platform isEqualToString:@"iPad4,5"])      return @"iPad Mini 2G (Cellular)";
    if ([platform isEqualToString:@"iPad4,6"])      return @"iPad Mini 2G (China)";
    if ([platform isEqualToString:@"iPad4,7"])      return @"iPad Mini 3 (Wi-Fi)";
    if ([platform isEqualToString:@"iPad4,8"])      return @"iPad Mini 3 (Cellular)";
    if ([platform isEqualToString:@"iPad4,9"])      return @"iPad Mini 3 (China)";
    if ([platform isEqualToString:@"iPad5,1"])      return @"iPad Mini 4 (Wi-Fi)";
    if ([platform isEqualToString:@"iPad5,2"])      return @"iPad Mini 4 (Cellular)";
    if ([platform isEqualToString:@"iPad5,3"])      return @"iPad Air 2 (Wi-Fi)";
    if ([platform isEqualToString:@"iPad5,4"])      return @"iPad Air 2 (Cellular)";
    if ([platform isEqualToString:@"iPad6,3"])      return @"iPad Pro 9.7\" (Wi-Fi)";
    if ([platform isEqualToString:@"iPad6,4"])      return @"iPad Pro 9.7\" (Cellular)";
    if ([platform isEqualToString:@"iPad6,7"])      return @"iPad Pro 12.9\" (Wi-Fi)";
    if ([platform isEqualToString:@"iPad6,8"])      return @"iPad Pro 12.9\" (Cellular)";
    if ([platform isEqualToString:@"AppleTV2,1"])   return @"Apple TV 2G";
    if ([platform isEqualToString:@"AppleTV3,1"])   return @"Apple TV 3";
    if ([platform isEqualToString:@"AppleTV3,2"])   return @"Apple TV 3 (2013)";
    if ([platform isEqualToString:@"AppleTV5,3"])   return @"Apple TV 4";
    if ([platform isEqualToString:@"Watch1,1"])     return @"Apple Watch Series 1 (38mm, S1)";
    if ([platform isEqualToString:@"Watch1,2"])     return @"Apple Watch Series 1 (42mm, S1)";
    if ([platform isEqualToString:@"Watch2,6"])     return @"Apple Watch Series 1 (38mm, S1P)";
    if ([platform isEqualToString:@"Watch2,7"])     return @"Apple Watch Series 1 (42mm, S1P)";
    if ([platform isEqualToString:@"Watch2,3"])     return @"Apple Watch Series 2 (38mm, S2)";
    if ([platform isEqualToString:@"Watch2,4"])     return @"Apple Watch Series 2 (42mm, S2)";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    
    return platform;
}


NSString* removeSpecialCharacters(NSString* value){
    NSMutableCharacterSet *charactersToKeep = [NSMutableCharacterSet characterSetWithCharactersInString:SPECIAL_CHARACTER];
    [charactersToKeep formUnionWithCharacterSet:[NSCharacterSet alphanumericCharacterSet]];
    
    NSCharacterSet *charactersToRemove = [charactersToKeep invertedSet];
    
    NSString *trimmedReplacement = [[value componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@"" ];
    
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\u0080-\\u0400]"
                                                                           options:NSRegularExpressionUseUnicodeWordBoundaries
                                                                             error:&error];
    trimmedReplacement = [regex stringByReplacingMatchesInString:trimmedReplacement
                                                         options:0
                                                           range:NSMakeRange(0, [trimmedReplacement length])
                                                    withTemplate:@""];
    
    regex = [NSRegularExpression regularExpressionWithPattern:@"[\\u0500-\\uFFFF]"
                                                      options:NSRegularExpressionUseUnicodeWordBoundaries
                                                        error:&error];
    trimmedReplacement = [regex stringByReplacingMatchesInString:trimmedReplacement
                                                         options:0
                                                           range:NSMakeRange(0, [trimmedReplacement length])
                                                    withTemplate:@""];
    
    return trimmedReplacement;
}

NSMutableDictionary* removeSpecialCharactersfromDict(NSMutableDictionary* dict){
    for (NSString* key in [dict allKeys]) {
        if([[dict objectForKey:key] isKindOfClass:[NSString class]]){
            [dict setObject:removeSpecialCharacters([dict objectForKey:key]) forKey:key];
        }
    }
    return dict;
}

BOOL isNotIncludedSpecialCharacters(NSString* text){
    return [text isEqualToString:removeSpecialCharacters(text)];
}

BOOL isJailbroken()
{
#if !(TARGET_IPHONE_SIMULATOR)
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:@"/Applications/Cydia.app"] ||
        [[NSFileManager defaultManager] fileExistsAtPath:@"/Library/MobileSubstrate/MobileSubstrate.dylib"] ||
        [[NSFileManager defaultManager] fileExistsAtPath:@"/bin/bash"] ||
        [[NSFileManager defaultManager] fileExistsAtPath:@"/usr/sbin/sshd"] ||
        [[NSFileManager defaultManager] fileExistsAtPath:@"/etc/apt"] ||
        [[NSFileManager defaultManager] fileExistsAtPath:@"/private/var/lib/apt/"] ||
        [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"cydia://package/com.example.package"]])  {
        return YES;
    }
    
    FILE *f = NULL ;
    if ((f = fopen("/bin/bash", "r")) ||
        (f = fopen("/Applications/Cydia.app", "r")) ||
        (f = fopen("/Library/MobileSubstrate/MobileSubstrate.dylib", "r")) ||
        (f = fopen("/usr/sbin/sshd", "r")) ||
        (f = fopen("/etc/apt", "r")))  {
        fclose(f);
        return YES;
    }
    fclose(f);
    
    NSString *filePath = [NSString stringWithFormat:@"/private"];
    return [[NSFileManager defaultManager] isWritableFileAtPath:filePath];
    
#endif
    return NO;
}

/**
 strim string with pattern
 
 @param from target string
 @param pattern array pattern applied
 @return string strimed
 */
NSString* strimStringFrom(NSString* from, NSArray* pattern) {
    NSString* temp = from;
    for (NSString* p in pattern) {
        temp = [temp stringByReplacingOccurrencesOfString:p withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0,temp.length)];
    }
    return temp;
}


/**
 return list<String> satisfy with pattern
 
 @param string string need check
 @param pattern pattern you want
 @param expression type expression (follow NSRegularExpression)
 @return return array store string satisfy with pattern
 */
NSArray* getMatchesFromString(NSString* string, NSString* pattern, NSUInteger expression) {
    NSRegularExpression *regexUL = [NSRegularExpression regularExpressionWithPattern:pattern options:expression error:NULL];
    NSArray *uls = [regexUL matchesInString:string options:0 range:NSMakeRange(0, [string length])] ;
    NSMutableArray *matchesuls = [NSMutableArray arrayWithCapacity:[uls count]];
    for (NSTextCheckingResult *match in uls) {
        NSRange matchRange = [match range];
        [matchesuls addObject:[string substringWithRange:matchRange]];
    }
    return matchesuls;
}

void openWifiSettings() {
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"prefs:root"]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root"]];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root"]];
    }
}

// Chung Comment
//void trackGAICategory(NSString* category, NSString* action, NSString* label,NSNumber* value){
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//
//    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:category
//                                                          action:action
//                                                           label:label
//                                                           value:value] build]];
//}



@end
