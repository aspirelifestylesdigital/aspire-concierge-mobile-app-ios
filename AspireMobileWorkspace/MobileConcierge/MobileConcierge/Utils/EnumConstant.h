//
//  EnumConstant.h
//  MobileConcierge
//
//  Created by Chung Mai on 6/5/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#ifndef EnumConstant_h
#define EnumConstant_h


#endif /* EnumConstant_h */

/* Service Task */
enum WSTASK
{
    WS_AUTHENTICATION_MULTITASK = 1,
    WS_AUTHENTICATION_LOGIN = 101,
    WS_GET_USER_DETAILS = 2,
    WS_CREATE_USER = 7,
    WS_FORGOT_PASSWORD = 8,
    WS_GET_MY_PREFERENCE = 53,
    WS_GET_CUISINE_PREFERENCE = 54,
    WS_UPDATE_USER = 55,

    WS_GET_REQUEST_TOKEN = 101,
    WS_GET_ACCESS_TOKEN = 102,
    WS_GET_REFRESH_ACCESS_TOKEN = 103,
    WS_B2C_GET_USER_DETAILS = 104,
    WS_B2C_VERIFY_BIN = 105,

    WS_GET_ENTERTAINMENT_RECOMMEND = 108,
    WS_DCR_ITEM_SEARCH = 200,
    WS_DCR_ITEM_SEARCH_MORE = 201,
    WS_DCR_ITEM_DETAILS = 202,
    WS_B2C_GET_QUESTIONS = 203,
    WS_B2C_GET_CONTENT_FULLS = 204,
    WS_B2C_GET_TILES = 205,
    WS_B2C_SEARCH = 206,
    WS_B2C_GET_SEARCH_DETAIL = 207
    
};

enum WS_SUB_TASK
{
    WS_ST_NONE = 0,
    WS_AUTHENTICATION = 1,
};


/**
 * Reqeust Type
 **/

enum REQUEST_METHOD
{
    POST,
    GET
};

/**
 * Request Content Type (also used for return format)
 **/
enum REQUEST_CONTENT_TYPE
{
    RCT_JSON = 0,
    RCT_FORM = 1,
    RCT_XML = 2,
    RCT_IMAGE = 3,
};
