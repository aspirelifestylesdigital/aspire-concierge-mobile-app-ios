//
//  Constant.h
//  ALC
//
//  Created by Anh Tran on 8/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MCConfiguration.h"

@interface Constant : NSObject

#define EncryptKey @"#concierge.mastercard.com#91@#B8%*6EBC8045E1-84B3-E171A0C026F4@"
/**
 * More Information in URL
 **/
#define PRIVACY_POLICY_URL @"https://www.aspirelifestyles.com/en/privacy-policy"
#define MASTERCARD_REGISTER_URL @"http://www.mastercard.com/sea/consumer/world_worldelite_mastercard.html"
#define MASTERCARD_TRAVEL_URL @"https://travel.mastercard.com/travel/arc.cfm"
#define PRICELESS_URL @"https://priceless.com"
#define GOLF_URL @"https://priceless.com/golf"
#define MASTERCARD_SHARING @"https://concierge.mastercard.com/sign_in_mc.aspx?utm_source=tw&utm_medium=mcusa&utm_campaign=concierge&popup"


/**
 * URL Request
 **/
#define B2C_SUBDOMAIN       @"concierge.mastercard.com"
#define B2C_PW              @"91@#B8%*6EBC8045E1-84B3-E171A0C026F4"
//For Ask Concierge
#define B2C_API_SERVICES    @"https://api.vipdesk.com/services/"
#define B2C_ConsumerKey     @"MCD"
#define B2C_DeviceId        @"DeciveID10"

/**
* URL Production
**/
/*
// For Profile
#define MCD_API_URL         @"https://apiservice.vipdesk.com/ConciergeWebAPIProd/"
// For Explore
#define B2C_API_URL         @"https://tools.vipdesk.com/b2cwebservices/"

#define B2C_Callback_Url    @"mcd:inof:kl:oauth:2.0:brd"
#define B2C_ConsumerSecret  @"MCD-20010C08-M02"
#define B2C_Program_Key     @"MCD CONC"
*/

/**
* URL Production End
**/

/**
 * URL Testing
 **/

 #define MCD_API_URL        @"https://apiservice-stg.aspirelifestyles.com/ConciergeOauthWebAPI/"
 #define B2C_API_URL        @"https://tools.vipdesk.com/b2cwebservices.testing/"

 #define B2C_Callback_Url   @"MCD:ietf:wg:oauth:2.0:oob"
 #define B2C_ConsumerSecret @"MCD-20170C45-C95"
 #define B2C_Program_Key    @"MCD VASP"

/**
 * URL Testing End
 **/

//define error code
#define B2C_UNAVAILABLE_USER        @"ENR68-2"
#define B2C_INVALID_ACCESS_TOKEN    @"ENR73-2"
#define B2C_INVALID_USER_EMAIL      @"ENR008-3"
#define B2C_INVALID_PHONE_NUMBER    @"ENR007-3"
#define B2C_LOGIN_FAIL    @"ENR70-1"


/**************************** Development API V1.0  ****************************/

//B2C API
#define GetRequestToken               @"OAuth/AuthoriseRequestToken"
#define GetAccessToken                @"OAuth/GetAccessToken"
#define RefreshAccessToken            @"OAuth/RefereshToken"
#define PostRegistration              @"ManageUsers/Registration"
#define PostUpdateRegistration        @"ManageUsers/UpdateRegistration"
#define PostUserLogin                 @"ManageUsers/Login"
#define GetUserDetailsUrl             @"ManageUsers/GetUserDetails"
#define PostForgotPasswordUrl         @"ManageUsers/ForgotPassword"
#define UpdateRegistration            @"ManageUsers/UpdateRegistration"
#define CreateConciergeRequestUrl     @"ManageRequest/CreateConciergeRequest"


#define GetQuestionsAndAnswers      @"instantanswers/GetQuestionsAndAnswers"
#define GetQuestionAndAnswers       @"instantanswers/GetQuestionAndAnswers"
#define GetSubCategories            @"instantanswers/GetSubCategories"
#define GetContentFulls             @"conciergecontent/GetContentFulls"
#define GetContentFull              @"conciergecontent/GetContentFull"
#define GetTiles                    @"conciergecontent/GetTiles"
#define GetUtility                  @"utility/GetClientCopy"
#define GetDataBasedOnSearchText    @"utility/SearchIAAndCCA"
#define checkBIN                    @"utility/CheckMasterCardBIN"
#define POSTCreateNewConciergeCase  @"concierge/standard/v2.0/Requests/CreateNewConciergeCase"
//end B2C

// config user authenticate
#define userAuthenticate @"admin"
#define passAuthenticate @"Admin123!@#$"

#define aspire_api_service_username @"mcd-qa@aspire.org"
#define aspire_api_service_password @"P@ssw0rd12345"
#define aspire_api_program          @"MASTERCARD"
//

/**
 * Http Methods
 **/
#define kHTTPGET    @"GET"
#define kHTTPPUT    @"PUT"
#define kHTTPPOST   @"POST"
#define kHTTPDEL    @"DELETE"

/**
 * Error Code
 **/
#define kSTATUSOK               200
#define kSTATUSTOKENEXPIRE     401


/***
    /// Define Font
***/
FOUNDATION_EXPORT NSString *const FONT_MarkForMC_BOLD;
FOUNDATION_EXPORT NSString *const FONT_MarkForMC_MED;
FOUNDATION_EXPORT NSString *const FONT_MarkForMC_LIGHT;
FOUNDATION_EXPORT NSString *const FONT_MarkForMC_LIGHTIT;
FOUNDATION_EXPORT NSString *const FONT_MarkForMC_REGULAR;
FOUNDATION_EXPORT NSString *const FONT_MarkForMC_REGULAR_It;
FOUNDATION_EXPORT NSString *const FONT_MarkForMC_Nrw_BOLD;
FOUNDATION_EXPORT NSString *const FONT_MarkForMC_Nrw_MED;

/// Default Color Hex
//FOUNDATION_EXPORT NSString *const DEFAULT_BACKGROUND_COLOR;
//FOUNDATION_EXPORT NSString *const DEFAULT_PLACEHOLDER_COLOR;
#define APP_COLOR_FOR_TEXT [UIColor colorWithRed:157.0f/255.0f green:3.0f/255.0f blue:41.0f/255.0f alpha:1.0f];

/***
 *Define static key
 ***/
#define FLAG_STARTED @"0"
#define stLoginStatus  @"isUserLogin"
#define stSessionToken  @"sessionToken"

/***
 *SCREEN SIZE
 ***/
#define OLD_SCREEN_SCALE  (((float)[UIScreen mainScreen].bounds.size.width)/414.0)
#define BLANCE_SCALE_FONT 1.0
#define SCREEN_SCALE  OLD_SCREEN_SCALE//(((float)[UIScreen mainScreen].bounds.size.width)/375.0)
#define SCREEN_SCALE_BY_HEIGHT  (((float)[UIScreen mainScreen].bounds.size.width)/736.0)
#define SCREEN_WIDTH  (((float)[UIScreen mainScreen].bounds.size.width))
#define SCREEN_HEIGHT (((float)[UIScreen mainScreen].bounds.size.height))
#define IPHONE_5S (((float)[UIScreen mainScreen].bounds.size.width) == 320.0)

#define MARGIN_KEYBOARD 5.0

/***
 *For Background UIActivity
 ***/
#define SPINNER_BACKGROUND_WIDTH    60
#define SPINNER_BACKGROUND_HEIGHT   60
#define SPINNER_VIEW_WIDTH      16
#define SPINNER_VIEW_HEIGHT     16
#define NAVIGATION_HEIGHT       64
/* Table constant */
#define REFRESH_INDICATOR_HEIGHT                                    20
#define TABLE_PULL_HEIGHT                                           30.0
#define TABLE_PULL_LENGTH                                           20.0


#define MAX_FIRSTNAME_TEXT_LENGTH 25


/***
 *Date format
 ***/

#define SPECIAL_CHAR @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%&*()-+_=[]:;\",.?/ "



#define CURRENT_CITY_CODE               @"currentCityCode"
#define SIX_MONTH_SECONDS_TIME          (6 * 30 * 24 * 60 * 60)
#define BIN_TEXTFIELD_TAG               1000
#define PHONE_NUMBER_TEXTFIELD_TAG      1001
#define ZIP_CODE_TEXTFIELD_TAG          1002

/***
 *Concierege's string value
 ***/
#define CONCIERGE_PHONENUMBER       @"(877) 288-6503"
#define CONCIERGE_PHONE_STRING      @"8772886503"
#define CONCIERGE_INTERNATIONAL_PHONE_STRING @"13124799429"

#ifdef DEBUG
#define ISLOGINGINFO                  true
#else
#define ISLOGINGINFO                  NO
#endif

#define NOTIFICATION_NAME_ISUPDATINGPROFILE @"Observe:IsUpdatingProfle"


/***
 *Concierege's hard code value
 ***/
#define kEmailInputMaxLength 96
#define kPasswordInputMaxLength 25

#define MASKED_ANSWER @"*******"

@end
