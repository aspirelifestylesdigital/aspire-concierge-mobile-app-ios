//
//  SessionData.h
//  MobileConcierge
//
//  Created by user on 6/21/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserObject.h"
#import "NSDictionary+SBJSONHelper.h"

#define keyUUID                  @"UUID"
#define keyOnlineMemberDetailIDs @"OnlineMemberDetailIDs"
#define keyOnlineMemberID        @"OnlineMemberID"
#define keyBINNumber             @"binNumber"
#define keyFirstName             @"FirstName"
#define keyLastName              @"LastName"
#define keyConsumerKey           @"ConsumerKey"
#define keyEmail                 @"Email"
#define keyMobileNumber          @"MobileNumber"
#define keyZipCode               @"ZipCode"
#define keyProgram               @"Program"
#define keySalutation            @"Salutation"
#define keyRequestToken          @"RequestToken"
#define keyAccessToken           @"AccessToken"
#define keyRefreshToken          @"RefreshToken"
#define keyExprirationTime       @"ExprirationTime"
#define keyOptStatus             @"OptStatus"
#define keyCurrentPolicyVersion  @"POLICYVERSION"

@interface SessionData : NSObject

+(id)shareSessiondata;

//@property (strong, nonatomic) NSString *UUID;
//@property (strong, nonatomic) NSString *OnlineMemberDetailIDs;
//@property (strong, nonatomic) NSString *OnlineMemberID;
//
@property (strong, nonatomic) NSString *userFirstName;
@property (strong, nonatomic) NSString *userLastName;
@property (strong, nonatomic) NSString *userMobileNumber;
@property (strong, nonatomic) NSString *userEmail;
@property (strong, nonatomic) NSString *userConsumerKey;
@property (strong, nonatomic) NSString *userSalutation;
@property (strong, nonatomic) NSString *userZipCode;
@property (strong, nonatomic) NSString *userProgram;
//
- (void) setUUID:(NSString*)uuid;
- (NSString*) UUID;

- (void) setOnlineMemberDetailIDs:(NSString*)onlineMemberDetailIDs;
- (NSString*) OnlineMemberDetailIDs;

- (void) setOnlineMemberID:(NSString*)onlineMemberID;
- (NSString*) OnlineMemberID;

- (void) setBINNumber:(NSString*)bin;
- (NSString*) BINNumber;

- (void)setRequestToken:(NSString*)requestToken;
- (NSString*)RequestToken;

- (void) setAccessToken:(NSString*)accessToken;
- (NSString*)AccessToken;

- (void) setRefreshToken:(NSString*)refreshToken;
- (NSString*) RefreshToken;

- (void) setExpirationTime:(NSNumber*)exprirationTime;
- (NSNumber*) ExpirationTime;

- (void) setCurrentPolicyVersion:(NSString*)currentPolicyVersion;
- (NSString*) CurrentPolicyVersion;

- (void)setUserObjectWithDict:(NSDictionary*)dict;
- (void)setUserObject:(UserObject*)userObject;

- (UserObject*)UserObject;

- (void) updateUserObjectFromAPI;

@end
