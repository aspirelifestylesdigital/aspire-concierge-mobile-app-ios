//
//  MCSize.h
//  MobileConcierge
//
//  Created by Chung Mai on 7/31/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppSize : NSObject

+(CGFloat) titleGreetingSize;
+(CGFloat) largeTitleTextSize;
+(CGFloat) letter26px;
+(CGFloat) largeDescriptionTextSize;
+(CGFloat) descriptionGreetingSize;
+(CGFloat) titleTextSize;
+(CGFloat) descriptionTextSize;
+(CGFloat) headerTextSize;
+(CGFloat) moreInfoTextSize;
+(CGFloat) titleLetterSpacing;
+(CGFloat) descriptionLetterSpacing;
+(CGFloat) smallLetterSpacing;
+(CGFloat) titleLineSpacing;

@end
