//
//  MCSize.m
//  MobileConcierge
//
//  Created by Chung Mai on 7/31/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "AppSize.h"
#import "Constant.h"

@implementation AppSize

+(CGFloat) titleGreetingSize
{
    return 44.0f * SCREEN_SCALE * BLANCE_SCALE_FONT;
}

+(CGFloat) largeTitleTextSize
{
    return 36.0f * SCREEN_SCALE * BLANCE_SCALE_FONT;
}

+(CGFloat) letter26px
{
    return 26.0f * SCREEN_SCALE * BLANCE_SCALE_FONT;
}

+(CGFloat) largeDescriptionTextSize
{
    return 24.0f * SCREEN_SCALE * BLANCE_SCALE_FONT;
}

+(CGFloat) descriptionGreetingSize
{
    return 20.0f * SCREEN_SCALE * BLANCE_SCALE_FONT;
}

+(CGFloat) titleTextSize
{
    return 18.0f * SCREEN_SCALE;
}

+(CGFloat) descriptionTextSize
{
    return 16.0f * SCREEN_SCALE;
}

+(CGFloat) headerTextSize
{
    return 14.0f * SCREEN_SCALE * BLANCE_SCALE_FONT;
}

+(CGFloat) moreInfoTextSize
{
    return 12.0f * SCREEN_SCALE  * BLANCE_SCALE_FONT;
}


+(CGFloat) titleLetterSpacing
{
    return 1.6;
}

+(CGFloat) descriptionLetterSpacing
{
    return 1.4;
}

+(CGFloat) smallLetterSpacing
{
    return 1.2;
}

+(CGFloat) titleLineSpacing
{
    return 1.25;
}
@end
