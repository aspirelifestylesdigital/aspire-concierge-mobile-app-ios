//
//  MCColor.m
//  MobileConcierge
//
//  Created by Chung Mai on 7/31/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "AppColor.h"
#import "Common.h"

@implementation AppColor

+(UIColor *)backgroundColor
{
    return colorFromHexString(@"#272726");
}


+(UIColor *) disableButtonColor
{
    return colorFromHexString(@"#B32D00");
}

+(UIColor *) activeButtonColor
{
    return colorFromHexString(@"#FF4000");
}


+(UIColor *) clickedButtonColor
{
    return colorFromHexString(@"#FF8C56");
}


+(UIColor *) placeholderTextColor
{
    return colorFromHexString(@"#434342");
}


+(UIColor *) textColor
{
    return colorFromHexString(@"#272726");
}


+(UIColor *) disableTextColor
{
    return colorFromHexString(@"#F1EFEB");
}


+(UIColor *) activeTextColor
{
    return colorFromHexString(@"#FFFFFF");
}


+(UIColor *) childViewBackgroundColor
{
    return colorFromHexString(@"#5B5B5A");
}


+(UIColor *) separatingLineColor
{
    return colorFromHexString(@"#585B5A");
}


+(UIColor *) descriptionTextColor
{
    return colorFromHexString(@"#A1A1A1");
}

+(UIColor *) moreInfoTextColor
{
    return colorFromHexString(@"#A1A1A1");
}


+(UIColor *) selectedViewBackgroundColor
{
    return colorFromHexString(@"#F1EFEB");
}
@end
