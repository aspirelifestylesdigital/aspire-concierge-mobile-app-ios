//
//  MCColor.h
//  MobileConcierge
//
//  Created by Chung Mai on 7/31/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppColor : NSObject

+(UIColor *) backgroundColor;
+(UIColor *) disableButtonColor;
+(UIColor *) activeButtonColor;
+(UIColor *) clickedButtonColor;
+(UIColor *) placeholderTextColor;
+(UIColor *) textColor;
+(UIColor *) disableTextColor;
+(UIColor *) activeTextColor;
+(UIColor *) childViewBackgroundColor;
+(UIColor *) separatingLineColor;
+(UIColor *) descriptionTextColor;
+(UIColor *) moreInfoTextColor;
+(UIColor *) selectedViewBackgroundColor;

@end
