//
//  SessionData.m
//  MobileConcierge
//
//  Created by user on 6/21/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "SessionData.h"
#import "NSString+AESCrypt.h"
#import "Constant.h"
//#import <AspireApiFramework/AspireApiFramework.h>
@import AspireApiFramework;


@implementation SessionData
{
    UserObject *userObject;
    BOOL isUpdatingProfile;
}


+ (id)shareSessiondata{
    static SessionData *sharedMySession = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMySession = [[self alloc] init];
    });
    return sharedMySession;
}

-(id)init {
    self = [super init];
    if (self != nil) {
        // initialize stuff here
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeIsUpdatingProfile:) name:NOTIFICATION_NAME_ISUPDATINGPROFILE object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_NAME_ISUPDATINGPROFILE object:nil];
}

- (void) observeIsUpdatingProfile:(NSNotification*) notification {
    if ([notification.name isEqualToString:NOTIFICATION_NAME_ISUPDATINGPROFILE]) {
        if ([[notification.userInfo allKeys] containsObject:@"isUpdating"]) {
#if DEBUG
            NSLog(@"%s => %@",__PRETTY_FUNCTION__,[notification.userInfo boolForKey:@"isUpdating"] ? @"true" : @"false");
#endif
            [[NSUserDefaults standardUserDefaults] setBool:[notification.userInfo boolForKey:@"isUpdating"] forKey:NOTIFICATION_NAME_ISUPDATINGPROFILE];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
}

/////////
//                                      UUID
/////////
- (void)setUUID:(NSString *)uuid{
    if (uuid) {
        [self setUserDefaultValue:uuid withKey:keyUUID];
    }
}
- (NSString *)UUID{
    return [self getUserDefaultValueWithKey:keyUUID];
}
/////////
//                                      OnlineMemberID
/////////
- (void) setOnlineMemberID:(NSString *)onlineMemberID{
    if (onlineMemberID) {
        [self setUserDefaultValue:onlineMemberID withKey:keyOnlineMemberID];
    }
}
- (NSString *)OnlineMemberID{
    return [self getUserDefaultValueWithKey:keyOnlineMemberID];
}
/////////
//                                      OnlineMemberDetailIDs
/////////
- (void)setOnlineMemberDetailIDs:(NSString *)onlineMemberDetailIDs{
    if (onlineMemberDetailIDs) {
        [self setUserDefaultValue:onlineMemberDetailIDs withKey:keyOnlineMemberDetailIDs];
    }
}
- (NSString *)OnlineMemberDetailIDs{
    return [self getUserDefaultValueWithKey:keyOnlineMemberDetailIDs];
}
/////////
//                                      BINNumber
/////////

/*
- (void)setBINNumber:(NSString *)bin{
    if (bin) {
        [self setUserDefaultValue:bin withKey:keyBINNumber];
    }
}
- (NSString *)BINNumber{
    NSLog(@"BIN: %@",[self getUserDefaultValueWithKey:keyBINNumber]);
    return [self getUserDefaultValueWithKey:keyBINNumber];
}
*/

- (void)setBINNumber:(NSString *)bin{
    if (bin) {
        if(![self isEncryptBINData])
        {
            [self setIsEncryptBINData];
        }
        
        [self setUserDefaultValue:[bin AES256EncryptWithKey:EncryptKey] withKey:keyBINNumber];
        
        // update profile if user is login
        if (![User isValid]) return;
        if ([[[User current] getPasscode] isEqualToString:bin]) return;
        
        NSMutableDictionary *userDict = [[NSMutableDictionary alloc] init];
        UserObject* temp = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([UserObject class])];
        [userDict setValue:B2C_ConsumerKey forKey:@"referenceName"];
        [userDict setValue:temp.firstName forKey:@"FirstName"];
        [userDict setValue:temp.lastName forKey:@"LastName"];
        [userDict setValue:temp.mobileNumber forKey:@"MobileNumber"];
        [userDict setValue:temp.email forKey:@"Email"];
        [userDict setValue:temp.zipCode forKey:@"ZipCode"];
        [userDict setValue:@"Unknown" forKey:@"City"];
        [userDict setValue:@"USA" forKey:@"Country"];
        [userDict setObject:[[User current] isGetNewsletter] ? @"ON" : @"OFF" forKey:APP_USER_PREFERENCE_Newsletter];
        [userDict setObject:[User isValid] ? [[User current] getPolicyVersion] : @"0" forKey:APP_USER_PREFERENCE_Policy_Version];
        [userDict setValue:bin forKey:APP_USER_PREFERENCE_Passcode];
        if ([[User current] isGetNewsletterDate].length > 0)
            [userDict setObject:[[User current] isGetNewsletterDate] forKey:APP_USER_PREFERENCE_Newsletter_Date];
        if (temp.selectedCity) {
            if (temp.selectedCity.length > 0)
                [userDict setObject:temp.selectedCity forKey:APP_USER_PREFERENCE_Selected_City];
        }
        
        isUpdatingProfile = true;
        typeof(self) __weak _self = self;
        [ModelAspireApiManager updateProfileCurrentUserWithUserInfo:userDict completion:^(NSError *error) {
            if (!_self) return;
            typeof(self) __self = _self;
            __self->isUpdatingProfile = false;
            [[SessionData shareSessiondata] setUserObject:(UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([UserObject class])]];
        }];
    }
}
- (NSString *)BINNumber {
    
    if ([User isValid] && ![self getUserDefaultValueWithKey:keyBINNumber])
        return [[User current] getPasscode];
    
    NSString *binNumber = [self getUserDefaultValueWithKey:keyBINNumber];
    if(binNumber)
    {
        if(![self isEncryptBINData])
        {
            [self setIsEncryptBINData];
            [self setUserDefaultValue:[binNumber AES256EncryptWithKey:EncryptKey] withKey:keyBINNumber];
        }
        else
        {
            binNumber = [binNumber AES256DecryptWithKey:EncryptKey];
        }
    }
   
    return binNumber;
}



- (void)registerDefaultsFromSettingsBundle {
    NSString *settingsBundle = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"bundle"];
    if(!settingsBundle) {
        NSLog(@"Could not find Settings.bundle");
        return;
    }
    
    NSDictionary *settings = [NSDictionary dictionaryWithContentsOfFile:[settingsBundle stringByAppendingPathComponent:@"Root.plist"]];
    NSArray *preferences = [settings objectForKey:@"PreferenceSpecifiers"];
    
    NSMutableDictionary *defaultsToRegister = [[NSMutableDictionary alloc] initWithCapacity:[preferences count]];
    for(NSDictionary *prefSpecification in preferences) {
        NSString *key = [prefSpecification objectForKey:@"Key"];
        if(key) {
            [defaultsToRegister setObject:[prefSpecification objectForKey:@"DefaultValue"] forKey:key];
        }
    }
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaultsToRegister];
}

/////////
//                                      RequestToken
/////////
- (void)setRequestToken:(NSString *)requestToken{
    if (requestToken) {
        [self setUserDefaultValue:requestToken withKey:keyRequestToken];
    }
}
- (NSString *)RequestToken{
    return [self getUserDefaultValueWithKey:keyRequestToken];
}
/////////
//                                      AccessToken
/////////
- (void)setAccessToken:(NSString *)accessToken{
    if (accessToken) {
        [self setUserDefaultValue:accessToken withKey:keyAccessToken];
    }
}
- (NSString *)AccessToken{
    return [self getUserDefaultValueWithKey:keyAccessToken];
}
/////////
//                                      RefreshToken
/////////
- (void) setRefreshToken:(NSString *)refreshToken{
    if (refreshToken) {
        [self setUserDefaultValue:refreshToken withKey:keyRefreshToken];
    }
}
- (NSString *)RefreshToken{
    return [self getUserDefaultValueWithKey:keyRefreshToken];
}
/////////
//                                      ExprirationTime
/////////
- (void) setExpirationTime:(NSNumber *)exprirationTime{
    if (exprirationTime) {
        [self setUserDefaultNumberValue:exprirationTime withKey:keyExprirationTime];
    }
}
- (NSNumber *)ExpirationTime{
    return [self getUserDefaultNumberValueWithKey:keyExprirationTime];
}
/////////
//                                      CurrentPolicyVersion
/////////
- (void) setCurrentPolicyVersion:(NSString *)currentPolicyVersion{
    if (currentPolicyVersion) {
        [self setUserDefaultValue:currentPolicyVersion withKey:keyCurrentPolicyVersion];
        NSMutableDictionary *userDict = [[NSMutableDictionary alloc] init];
        
        if (![User isValid]) return;
        if ([[[User current] getPolicyVersion] isEqualToString:currentPolicyVersion]) return;
        
        UserObject* temp = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([UserObject class])];
        [userDict setValue:B2C_ConsumerKey forKey:@"referenceName"];
        [userDict setValue:temp.firstName forKey:@"FirstName"];
        [userDict setValue:temp.lastName forKey:@"LastName"];
        [userDict setValue:temp.mobileNumber forKey:@"MobileNumber"];
        [userDict setValue:temp.email forKey:@"Email"];
        [userDict setValue:currentPolicyVersion forKey:APP_USER_PREFERENCE_Policy_Version];
        [userDict setValue:[[SessionData shareSessiondata] BINNumber] forKey:APP_USER_PREFERENCE_Passcode];
        [userDict setValue:temp.zipCode forKey:@"ZipCode"];
        [userDict setValue:@"Unknown" forKey:@"City"];
        [userDict setValue:@"USA" forKey:@"Country"];
        [userDict setObject:[[User current] isGetNewsletter] ? @"ON" : @"OFF" forKey:APP_USER_PREFERENCE_Newsletter];
        [userDict setValue:temp.selectedCity forKey:APP_USER_PREFERENCE_Selected_City];
        [userDict setObject:temp.optStatus ? @"ON" : @"OFF" forKey:APP_USER_PREFERENCE_Newsletter];
        if (temp.selectedCity) {
            if (temp.selectedCity.length > 0)
                [userDict setObject:temp.selectedCity forKey:APP_USER_PREFERENCE_Selected_City];
        }
        
        isUpdatingProfile = true;
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NAME_ISUPDATINGPROFILE object:nil userInfo:@{@"isUpdating":@true}];
        typeof(self) __weak _self = self;
        [ModelAspireApiManager updateProfileCurrentUserWithUserInfo:userDict completion:^(NSError *error) {
            if (!_self) return;
            typeof(self) __self = _self;
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NAME_ISUPDATINGPROFILE object:nil userInfo:@{@"isUpdating":@false}];
            __self->isUpdatingProfile = false;
            [[SessionData shareSessiondata] setUserObject:(UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([UserObject class])]];
        }];
    }
}
- (NSString *)CurrentPolicyVersion {
    
    if([User isValid])
        return [[User current] getPolicyVersion];
    
    return [self getUserDefaultValueWithKey:keyCurrentPolicyVersion];
}
/////////
//                                      Userobject
/////////
- (UserObject *)UserObject{
    return  userObject;
}

- (void)setUserObject:(UserObject *)userObject1 {
    userObject = userObject1;
    if (!userObject1) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyBINNumber];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)setUserObjectWithDict:(NSDictionary *)dict{
    userObject = [[UserObject alloc] init];
    userObject.firstName = [dict stringForKey:keyFirstName];
    userObject.lastName = [dict stringForKey:keyLastName];
    userObject.email = [dict stringForKey:keyEmail];
    userObject.mobileNumber = [dict stringForKey:keyMobileNumber];
    userObject.ConsumerKey = [dict stringForKey:keyConsumerKey];
    userObject.Program = [dict stringForKey:keyProgram];
    userObject.zipCode = [dict stringForKey:keyZipCode];
    userObject.salutation = [dict stringForKey:keySalutation];
    userObject.optStatus = [dict boolForKey:keyOptStatus];
}

///*
    //
///*
- (void)setUserDefaultValue:(NSString*)value withKey:(NSString*)key{
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString*)getUserDefaultValueWithKey:(NSString*)key{
    return [[NSUserDefaults standardUserDefaults]
                            stringForKey:key];
}

- (void)setUserDefaultNumberValue:(NSNumber*)value withKey:(NSString*)key{
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSNumber*)getUserDefaultNumberValueWithKey:(NSString*)key{
    return (NSNumber *)[[NSUserDefaults standardUserDefaults]
            objectForKey:key];
}

-(BOOL) isEncryptBINData
{
    return [[NSUserDefaults standardUserDefaults]
     boolForKey:@"IsEncryptData"];
}

-(void) setIsEncryptBINData
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:YES] forKey:@"IsEncryptData"];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

- (void) updateUserObjectFromAPI {
    // update user & change to home
    UserObject* userObject = [[SessionData shareSessiondata] UserObject];
    NSMutableDictionary *userDict = [[NSMutableDictionary alloc] init];
    [userDict setValue:B2C_ConsumerKey forKey:@"referenceName"];
    [userDict setValue:userObject.firstName forKey:@"FirstName"];
    [userDict setValue:userObject.lastName forKey:@"LastName"];
    [userDict setValue:[[userObject.mobileNumber stringByReplacingOccurrencesOfString:@"1-" withString:@""] stringByReplacingOccurrencesOfString:@"-" withString:@""] forKey:@"MobileNumber"];
    [userDict setValue:userObject.email forKey:@"Email"];
    [userDict setValue:[userObject.zipCode stringByReplacingOccurrencesOfString:@"-" withString:@""] forKey:@"ZipCode"];
    [userDict setValue:@"Unknown" forKey:@"City"];
    [userDict setValue:@"USA" forKey:@"Country"];
    [userDict setObject:[[SessionData shareSessiondata] CurrentPolicyVersion] forKey:APP_USER_PREFERENCE_Policy_Version];
    [userDict setValue:[[SessionData shareSessiondata] BINNumber] forKey:APP_USER_PREFERENCE_Passcode];
    [userDict setObject:userObject.optStatus ? @"ON" : @"OFF" forKey:APP_USER_PREFERENCE_Newsletter];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [userDict setObject:[dateFormatter stringFromDate:[NSDate date]] forKey:APP_USER_PREFERENCE_Newsletter_Date];
    
    if ([User isValid]) {
        UserObject* temp = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([UserObject class])];
        if (temp.selectedCity) {
            if (temp.selectedCity.length > 0)
                [userDict setObject:temp.selectedCity forKey:APP_USER_PREFERENCE_Selected_City];
        }
    }
    
    [ModelAspireApiManager updateProfileCurrentUserWithUserInfo:userDict completion:^(NSError *error) {
        if (!error) {
            [[SessionData shareSessiondata] setUserObject:(UserObject *)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([UserObject class])]];
        }
    }];
}

@end
