//
//  UtilStyle.h
//  MobileConcierge
//
//  Created by Chung Mai on 6/20/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UtilStyle : NSObject

//For Table Cell
+(UIColor *) colorForSeparateLine;

// For Create Profile View and My Profile View
+(NSAttributedString *) setTextStyleForTextFieldWithMessage:(NSString *)text;
+(NSAttributedString *) setPlaceHolderTextStyleForTextFieldWithMessage:(NSString *)text;

+(NSAttributedString *) setTextStyleForTitleViewControllerWithMessage:(NSString *)text;
+(NSAttributedString *) setLargeSizeStyleForLabelWithMessage:(NSString *)text;
@end
