//
//  MCConfiguration.h
//  MobileConcierge
//
//  Created by VuongTC on 10/30/18.
//  Copyright © 2018 s3Corp. All rights reserved.
//

#ifndef MCConfiguration_h
#define MCConfiguration_h

#define kConfigurations @{}

@interface MCConfiguration : NSObject

+ (NSDictionary*) getConfigurations;

@end

#endif /* MCConfiguration_h */
