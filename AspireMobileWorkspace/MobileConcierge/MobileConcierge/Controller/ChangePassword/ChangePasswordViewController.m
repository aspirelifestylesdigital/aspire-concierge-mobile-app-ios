//
//  ChangePasswordViewController.m
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 8/23/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "UIView+Extension.h"
#import "UITextField+Extensions.h"
#import "UIButton+Extension.h"
#import "NSString+Utis.h"

#import "AppDelegate.h"
#import "MenuViewController.h"
#import "HomeViewController.h"
#import "NSString+AESCrypt.h"
#import "UILabel+Extension.h"
//#import <AspireApiFramework/AspireApiFramework.h>
#import "SWRevealViewController.h"
@import AspireApiFramework;

#import "Common.h"
#import "Constant.h"

#define BOTTOM_SPACE  30.0f

@interface ChangePasswordViewController ()<UITextFieldDelegate>{
    CGFloat keyboardHeight;
    AppDelegate* appdelegate;
    UIImageView *asteriskImage;
    IBOutlet UILabel *titleTemp;
    IBOutlet NSLayoutConstraint *topConstraintStackView;
    UITapGestureRecognizer *tappedOutsideKeyboard;
}

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    isNotAskConciergeBarButton = YES;
    [super viewDidLoad];
    [self backNavigationItem];
    [self setNavigationBarWithDefaultColorAndTitle:@"Change Password"];
    [self setUIStyle];
    
    if(!self.navigationController) {
        NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                     NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:(14.0f * SCREEN_SCALE)],
                                     NSKernAttributeName: @1.6};
        titleTemp.attributedText = [[NSAttributedString alloc] initWithString:@"Change Password".uppercaseString attributes:attributes];
        titleTemp.numberOfLines = 0;
        //    titleLabel.adjustsFontSizeToFitWidth = YES;
        titleTemp.textAlignment = NSTextAlignmentCenter;
    } else {
        titleTemp.hidden = YES;
        topConstraintStackView.constant = 50;
    }
    [self trackingScreenByName:@"Change password"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    tappedOutsideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tappedOutsideKeyboard];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view removeGestureRecognizer:tappedOutsideKeyboard];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUIStyle{
    
    [self.lblNote setLineSpacing:6];
    self.lblNote.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionTextSize]];
    
    [self setStatusSubmitButton];
    [self.scrollView setBackgroundColorForView];
    [self.contentView setBackgroundColorForView];
    
    [self.submitButton setBackgroundColorForDisableStatus];
    [self.submitButton setBackgroundColorForNormalStatus];
    self.submitButton.enabled = false;
    [self.submitButton setBackgroundColorForTouchingStatus];
    [self.submitButton configureDecorationForButton];
    
    [self.cancelButton setBackgroundColorForDisableStatus];
    [self.cancelButton setBackgroundColorForNormalStatus];
    self.cancelButton.enabled = false;
    [self.cancelButton setBackgroundColorForTouchingStatus];
    [self.cancelButton configureDecorationForButton];
    
//    [self.submitButton setBackgroundColorForNormalStatus];
//    [self.submitButton setBackgroundColorForTouchingStatus];
//    [self.submitButton configureDecorationForButton];
//    
//    [self.cancelButton setBackgroundColorForNormalStatus];
//    [self.cancelButton setBackgroundColorForTouchingStatus];
//    [self.cancelButton configureDecorationForButton];
    
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:([AppSize titleTextSize])],
                                 NSKernAttributeName: @([AppSize descriptionLetterSpacing])};
    self.submitButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.submitButton.titleLabel.text attributes:attributes];
    self.cancelButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.cancelButton.titleLabel.text attributes:attributes];
    
    [@[self.oldPWTextField,self.nPWTextField,self.confirmNewPWTextField] enumerateObjectsUsingBlock:^(UITextField*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.delegate = self;
        obj.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:obj.text];
        obj.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:obj.placeholder];
    }];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setTextFieldDefaultBottomBolder];
        self.topConstraintContentView.constant = CGRectGetHeight(self.contentView.frame) / 9;
    });
    [self createAsteriskForTextField:self.oldPWTextField];
    [self createAsteriskForTextField:self.nPWTextField];
    [self createAsteriskForTextField:self.confirmNewPWTextField];
}

#pragma mark - Configure UI
-(void) createAsteriskForTextField:(UITextField *)textField {
    
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 8, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [textField setLeftView:leftView];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    asteriskImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"asterisk_icon"]];
    asteriskImage.frame = CGRectMake(0.0f, 0.0f, 20.0f, 20.0f);
    asteriskImage.contentMode = UIViewContentModeCenter;
    [textField setRightView:asteriskImage];
    [textField setRightViewMode:UITextFieldViewModeAlways];
}
-(void) makeBecomeFirstResponderForTextField {
    [self resignFirstResponderForAllTextField];
    
    if([self.oldPWTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0){ //isValidWeakPassword
        [self.oldPWTextField becomeFirstResponder];
    }
    else if([self.nPWTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0 &&
             (![self.nPWTextField.text isValidStrongPassword] ||
              [self.nPWTextField.text.lowercaseString containsString:[[SessionData shareSessiondata] UserObject].firstName.lowercaseString] || [self.nPWTextField.text.lowercaseString containsString:[[SessionData shareSessiondata] UserObject].lastName.lowercaseString]
              )){//isValidStrongPassword
        [self.nPWTextField becomeFirstResponder];
    } else if (![self.nPWTextField.text isEqualToString:self.confirmNewPWTextField.text]) {
        [self.confirmNewPWTextField becomeFirstResponder];
    }
}
- (void)resignFirstResponderForAllTextField {
    
    if(self.oldPWTextField.isFirstResponder) {
        [self.oldPWTextField resignFirstResponder];
    }else if (self.nPWTextField.isFirstResponder) {
        [self.nPWTextField resignFirstResponder];
    }else if (self.confirmNewPWTextField.isFirstResponder) {
        [self.confirmNewPWTextField resignFirstResponder];
    }
}
- (void)setTextFieldDefaultBottomBolder {
    [self.oldPWTextField setBottomBolderDefaultColor];
    [self.nPWTextField setBottomBolderDefaultColor];
    [self.confirmNewPWTextField setBottomBolderDefaultColor];
}
- (void)setStatusSubmitButton{
    self.submitButton.enabled = self.cancelButton.enabled = (self.oldPWTextField.text.length > 0 || self.nPWTextField.text.length > 0 || self.confirmNewPWTextField.text.length > 0);
}

- (void)setEnableActionButtonsWithValueOldPW:(NSString *)oldPW withNewPW:(NSString*)newPW withConfirmNPW:(NSString*)confirmNPW{
    self.submitButton.enabled = self.cancelButton.enabled = (oldPW.length > 0 || newPW.length > 0 || confirmNPW.length > 0);
}

#pragma mark - Selector
- (void)dismissVC {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)dismissKeyboard {
    [self resignFirstResponderForAllTextField];
}

#pragma mark TEXT FIELD DELEGATE
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == self.oldPWTextField || textField == self.nPWTextField || textField == self.confirmNewPWTextField) {
        if ([string isEqualToString:@" "]) {
            return NO;
        }
        if (textField.text.length >= 25 && range.length == 0){
            return NO;
        }
        NSString *resultText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if (textField == self.oldPWTextField) {
            [self setEnableActionButtonsWithValueOldPW:resultText withNewPW:self.nPWTextField.text withConfirmNPW:self.confirmNewPWTextField.text];
        }else if (textField == self.nPWTextField) {
            [self setEnableActionButtonsWithValueOldPW:self.oldPWTextField.text withNewPW:resultText withConfirmNPW:self.confirmNewPWTextField.text];
        }else if (textField == self.confirmNewPWTextField) {
            [self setEnableActionButtonsWithValueOldPW:self.oldPWTextField.text withNewPW:self.nPWTextField.text withConfirmNPW:resultText];
        }
    }
    
    return YES;
}

#pragma mark - Validation
-(void)verifyData{

    NSString *errorAll = @"All fields are required.\n";
    NSMutableString *message = [[NSMutableString alloc] init];
    if (self.oldPWTextField.text.length == 0 || self.nPWTextField.text.length == 0 || self.confirmNewPWTextField.text.length == 0) {
        [message appendString:@"* "];
        [message appendString:errorAll];
        
        if(self.oldPWTextField.text.length == 0) {
            [self.oldPWTextField setBottomBorderRedColor];
        }
        if(self.nPWTextField.text.length == 0) {
            [self.nPWTextField setBottomBorderRedColor];
        }
        if(self.confirmNewPWTextField.text.length == 0) {
            [self.confirmNewPWTextField setBottomBorderRedColor];
        }
    }
    
    if(self.oldPWTextField.text.length > 0) {
        if([self verifyValueForTextField:self.oldPWTextField].length > 0) {
            [message appendString:@"* "];
            [message appendString:[self verifyValueForTextField:self.oldPWTextField]];
            [message appendString:@"\n"];
        }
    }
    if(self.nPWTextField.text.length > 0) {
        if([self verifyValueForTextField:self.nPWTextField].length > 0) {
            [message appendString:@"* "];
            [message appendString:[self verifyValueForTextField:self.nPWTextField]];
            [message appendString:@"\n"];
        }
    }
    if(self.confirmNewPWTextField.text.length > 0) {
        if([self verifyValueForTextField:self.confirmNewPWTextField].length > 0) {
            [message appendString:@"* "];
            [message appendString:[self verifyValueForTextField:self.confirmNewPWTextField]];
            [message appendString:@"\n"];
        }
    }
    
    [self showError:message];
}

-(void)showError:(NSString *)message
{
    if(message.length > 0){
        
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = @"Please confirm that the value entered is correct:";
        alert.msgAlert = message;
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;
            alert.lblAlertMessage.textAlignment = NSTextAlignmentLeft;
            [alert.view layoutIfNeeded];
        });
        
        alert.blockFirstBtnAction = ^(void){
            [self makeBecomeFirstResponderForTextField];
        };
        
        [self showAlert:alert forNavigation:NO];
    }
    else{
        if(!isNetworkAvailable()) {
            [self showNoNetWorkAlert];
            
        }else{
            [self resignFirstResponderForAllTextField];
            [self setTextFieldDefaultBottomBolder];
            [self changePasswordWithAPI];
        }
    }
}
-(NSString*)verifyValueForTextField:(UITextField *)textFied{
    
    NSString *errorMsg = @"";
   if(textFied == self.oldPWTextField && [(textFied.isFirstResponder ? textFied.text :self.oldPWTextField.text) stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0){ //isValidWeakPassword
        errorMsg = @"The password you entered is incorrect.";
        [textFied setBottomBorderRedColor];
   } else if (textFied == self.nPWTextField) {
//       
//   } &&
//             (![(textFied.isFirstResponder ? textFied.text :self.nPWTextField.text) isValidStrongPassword]/* ||
//                                               [(textFied.isFirstResponder ? textFied.text :self.nPWTextField.text).lowercaseString containsString:[[SessionData shareSessiondata] UserObject].firstName.lowercaseString] || [(textFied.isFirstResponder ? textFied.text :self.nPWTextField.text).lowercaseString containsString:[[SessionData shareSessiondata] UserObject].lastName.lowercaseString]*/
//              )) { //isValidStrongPassword
//       errorMsg = NSLocalizedString(@"input_invalid_password_msg", nil);
//       [textFied setBottomBorderRedColor];
   }else if (textFied == self.confirmNewPWTextField && ![(textFied.isFirstResponder ? textFied.text :self.confirmNewPWTextField.text) isEqualToString:self.nPWTextField.text]) {
       errorMsg = NSLocalizedString(@"input_invalid_confirm_password_msg", nil);
       [textFied setBottomBorderRedColor];
   }
    else{
        [textFied setBottomBolderDefaultColor];
    }
    
    return errorMsg;
}

#pragma mark - Actions
- (void)changePasswordWithAPI{
    
    [self startActivityIndicator];
    __weak typeof (self) _self = self;

    [ModelAspireApiManager changePasswordWithNewPassword:self.confirmNewPWTextField.text andOldPassword:self.oldPWTextField.text completion:^(NSError *error) {
        if (!_self) return;
        typeof (self) __self = _self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [_self stopActivityIndicator];
        });
        if (!error) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                [_self changePasswordSuccessRedirect];
            });
        }else{
            //            [_self showApiErrorWithMessage:@"Please confirm that the value entered is correct."];
            dispatch_sync(dispatch_get_main_queue(), ^{
                
                //            [_self showApiErrorWithMessage:@"Please confirm that the value entered is correct."];
                //            [_self showApiErrorWithMessage:[AspireApiError getMessageAPIChangePasswordFromError:error]];
                if ([AspireApiError getErrorTypeAPIChangePasswordFromError:error] == OLDPW_ERR) {
                    [_self showAlertWithTitle:@"Please confirm that the value entered is correct:"
                                      message:[NSString stringWithFormat:@"%@\n",@"The password you entered do not match."]
                                      buttons:@[NSLocalizedString(@"ok_button_title", nil)]
                                      actions:@[^{
                        [__self->_oldPWTextField setBottomBorderRedColor];
                        [__self->_oldPWTextField becomeFirstResponder];
                    }]
                             messageAlignment:NSTextAlignmentCenter];
                } else if ([AspireApiError getErrorTypeAPIChangePasswordFromError:error] == CHANGEPW_ERR) {
                    [_self showAlertWithTitle:@"Password requirements were not met"
                                      message:NSLocalizedString(@"Password must be at least 10 characters and contain at least 1 upper case, 1 lower case, 1 number and 1 special character !@#$%^&*. It cannot contain any part of your user name or be one of your last 12 passwords.", nil)
                                      buttons:@[NSLocalizedString(@"ok_button_title", nil)]
                                      actions:@[^{
                        [__self->_nPWTextField setBottomBorderRedColor];
                        [__self->_nPWTextField becomeFirstResponder];
                    }]
                             messageAlignment:NSTextAlignmentCenter];
                } else {
                    [_self showApiErrorWithMessage:@""];
                }
            });
        }
    }];
}

- (IBAction)SubmitChangePassword:(id)sender {
    [self updateEdgeInsetForHideKeyboard];
    [self verifyData];
}

- (IBAction)CancelAction:(id)sender {
    [self.view endEditing:YES];
    self.oldPWTextField.text = @"";
    self.nPWTextField.text = @"";
    self.confirmNewPWTextField.text = @"";
    [self setStatusSubmitButton];
    [self setTextFieldDefaultBottomBolder];
}

#pragma mark Keyboard
-(void)handleKeyboardWillShow:(NSNotification *)paramNotification
{
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         keyboardHeight = keyboardRect.size.height;
                         [self updateEdgeInsetForShowKeyboard];
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

-(void)handleKeyboardWillHide:(NSNotification *)paramNotification
{
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         [self updateEdgeInsetForHideKeyboard];
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
    
}

-(void)changePasswordSuccessRedirect{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.msgAlert = @"You have successfully updated your password.";
    alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
    alert.blockFirstBtnAction = ^{

        if (self.navigationController) {
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            appdelegate = appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
            
            [UIView transitionWithView:appdelegate.window
                              duration:0.5
                               options:UIViewAnimationOptionPreferredFramesPerSecond60
                            animations:^{
                                
                                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                MenuViewController *menuViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
                                UINavigationController *menuNavigationViewController = [[UINavigationController alloc] initWithRootViewController:menuViewController];
                                UIViewController *fontViewController = [[HomeViewController alloc] init];
                                
                                SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:menuNavigationViewController frontViewController:fontViewController];
                                
                                revealController.delegate = appdelegate;
                                revealController.rearViewRevealWidth = SCREEN_WIDTH;
                                revealController.rearViewRevealOverdraw = 0.0f;
                                revealController.rearViewRevealDisplacement = 0.0f;
                                appdelegate.viewController = revealController;
                                appdelegate.window.rootViewController = appdelegate.viewController;
                                [appdelegate.window makeKeyWindow];
                                
                                UIViewController *newFrontController = [[HomeViewController alloc] init];
                                UINavigationController *newNavigationViewController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                                [revealController pushFrontViewController:newNavigationViewController animated:YES];
                            }
                            completion:nil];
        }
        
    };
    dispatch_async(dispatch_get_main_queue(), ^{
        alert.seconBtn.hidden = YES;
        alert.midView.alpha = 0.0f;;
    });
    
    [self showAlert:alert forNavigation:NO];
}



-(void)updateEdgeInsetForShowKeyboard {

    self.bottomConstraintViewAction.constant = (keyboardHeight + MARGIN_KEYBOARD);
    
}


-(void)updateEdgeInsetForHideKeyboard
{
    self.bottomConstraintViewAction.constant = BOTTOM_SPACE;
}

@end
