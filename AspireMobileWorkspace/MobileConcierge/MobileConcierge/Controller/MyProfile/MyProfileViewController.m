//
//  MyProfileViewController.m
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/13/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "MyProfileViewController.h"
#import "ImageUtilities.h"
#import "NSString+Utis.h"
#import "Constant.h"
#import "SWRevealViewController.h"
#import "UIButton+Extension.h"
#import "HomeViewController.h"
#import "ChallengeQuestionObject.h"
#import "SessionData.h"

//#import <AspireApiFramework/AspireApiFramework.h>
@import AspireApiFramework;
@import AspireApiControllers;

#import "AppDelegate.h"
#import "UserObject.h"
#import "UITextField+Extensions.h"
#import "UtilStyle.h"

@interface MyProfileViewController ()<DropDownViewDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate>


@end

@implementation MyProfileViewController
{
    NSMutableArray *questionArray;
    NSMutableArray *questionTextArray;
    ChallengeQuestionObject *selectedChallengeQuestion;
    ChallengeQuestionObject *originalChallengeQuestion;
}
@synthesize hasFoucusedFiled;
- (void)viewDidLoad
{
    isNotAskConciergeBarButton = YES;
    self.isUpdateProfile = YES;
    [super viewDidLoad];
    [self createBackBarButton];
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"my_profile_menu_title", nil)];
    backupBottomConstraint = self.viewActionBottomConstraint.constant;

    [self startActivityIndicator];
    typeof(self) __weak _self = self;
    
    [self getUserInfo];
    [self setUpData];
    
    __block UITextField* answerTextfield = self.answerTextField;
    
    // retrieve profile
    [ModelAspireApiManager retrieveProfileCurrentUser:^(User *user, NSError *error) {
        if (!_self) return;
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error) {
                // store user local
                [[SessionData shareSessiondata] setUserObject:(UserObject *)[user convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([UserObject class])]];
                
                [_self getUserInfo];
                
                // get sercure question
                [ModelAspireApiManager getProfileFromEmail:[[SessionData shareSessiondata] UserObject].email completion:^(OKTAUserProfile * _Nullable OKTAuser, User * _Nullable user, NSError *error) {
                    if (!_self || !OKTAuser) return;
                    [_self stopActivityIndicator];
                    typeof(self) __self = _self;
                    int i = 0;
                    for (ChallengeQuestionObject* obj in __self->questionArray) {
                        if ([OKTAuser.credentials.recoveryQuestion.question isEqualToString:obj.questionText]) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                answerTextfield.text = MASKED_ANSWER;
                                __self->originalChallengeQuestion = obj;
                                [__self->_questionDropdown setSelectedIndex:i];
                            });
                            break;
                        }
                        i++;
                    }
                }];
            } else {
                
                if ([AspireApiError getErrorTypeAPIRetrieveProfileFromError:error] == ACCESSTOKEN_INVALID_ERR) {
                    [_self showAlertInvalidUserAndShouldLoginAgain:true];
                    return;
                }
                [_self showAlertWithTitle:NSLocalizedString(@"cannot_get_data", nil)
                                  message:NSLocalizedString(@"error_api_message", nil)
                                  buttons:@[NSLocalizedString(@"ok_button_title", nil)]
                                  actions:nil
                         messageAlignment:NSTextAlignmentCenter];
                
            }
        });
    }];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [@[self.zipCodeText,self.firstNameText,self.lastNameText,self.answerTextField,self.phoneNumberText] enumerateObjectsUsingBlock:^(UITextField*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(textFieldChanged:)
                                                     name:UITextFieldTextDidChangeNotification
                                                   object:obj];
    }];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    //  Set up Pop pan gesture
//    [self setUpCustomizedPanGesturePopRecognizer];
    

    if(self.navigationController.viewControllers.count == 1)
    {
        HomeViewController *homeView = [[HomeViewController alloc] init];
        [self.navigationController setViewControllers:@[homeView,self]];
    }
    
    self.signinBtn.hidden = YES;
    self.signInButton.hidden = YES;
    
    [self setTextViewsDefaultBottomBolder];
//    [self.view layoutIfNeeded];
    
//    [self setButtonStatus:NO];
}

//- (void) getUserInfo {
//    [super getUserInfo];
//    if ([[SessionData shareSessiondata] UserObject]) {
//        firstNameOnTyping = userInfo.firstName;
//        lastNameOnTyping = userInfo.lastName;
//        emailOnTyping = userInfo.email;
//        phoneOnTyping = userInfo.mobileNumber;
//        zipCodeOnTyping = userInfo.zipCode;
//#if DEBUG
//        NSLog(@"%s => fname: %@\nlname: %@\nemail: %@\nphone: %@\nzipcode: %@\n",__PRETTY_FUNCTION__,firstNameOnTyping,lastNameOnTyping,emailOnTyping,phoneOnTyping,zipCodeOnTyping);
//#endif
//    }
//}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [@[self.zipCodeText,self.firstNameText,self.lastNameText,self.answerTextField,self.phoneNumberText] enumerateObjectsUsingBlock:^(UITextField*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:obj];
    }];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
//    [self removeSetupForCustomizedPanGesturePopRecognizer];
}

- (void)onTextFieldChanged:(UITextField *)textfield {
    [textfield setOriginText:textfield.text];
    if (self.answerTextField.text.length == 0 ||
        [self.answerTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 3) {
        [self.answerTextField setBottomBolderDefaultColor];
    }
    if (textfield == self.answerTextField) {
        self.inputNewAnswer = textfield.text;
    }
    [self updateProfileButtonStatus];
}

-(void)textFieldChanged:(NSNotification*)notification {
    UITextField* txt = (UITextField*)[notification object];
    [self onTextFieldChanged:txt];
}

-(void) setUpData {
    questionArray = [[NSMutableArray alloc] init];
    questionTextArray = [[NSMutableArray alloc] init];
    
    id allKeys = [ModelAspireApiManager getSecurityQuetions];
    
    for (int i=0; i<[allKeys count]; i++) {
        NSDictionary *arrayResult = [allKeys objectAtIndex:i];
        ChallengeQuestionObject *object = [[ChallengeQuestionObject alloc] initFromDict:arrayResult];
        [questionArray addObject:object];
        [questionTextArray addObject:object.questionText];
    }
    
    self.questionDropdown.listItems = questionTextArray;
}

- (void)initView {
    self.phoneNumberText.tag = PHONE_NUMBER_TEXTFIELD_TAG;
    [self setButtonFrame];
    
    [self.updateButton setBackgroundColorForNormalStatus];
    [self.updateButton setBackgroundColorForTouchingStatus];
    [self.updateButton configureDecorationForButton];
    [self.updateButton setBackgroundColorForDisableStatus];
    self.updateButton.accessibilityIdentifier = @"btnUpdate";
    
    [self.cancelButton setBackgroundColorForNormalStatus];
    [self.cancelButton setBackgroundColorForTouchingStatus];
    [self.cancelButton configureDecorationForButton];
    [self.cancelButton setBackgroundColorForDisableStatus];
    self.cancelButton.accessibilityIdentifier = @"btnCancel";
    
    // Text Style For Greet Message
    NSString *message = NSLocalizedString(@"commitment_my_profile_message", nil);
    self.commitmentLabel.attributedText = [UtilStyle setLargeSizeStyleForLabelWithMessage:message];
    
    [self setCheckBoxState:isCheck];
    self.commitmentLabel.text = NSLocalizedString(@"commitment_my_profile_message", nil);
    [self.updateButton setTitle:NSLocalizedString(@"update_button", nil) forState:UIControlStateNormal];
    [self.cancelButton setTitle:NSLocalizedString(@"cancel_button", nil) forState:UIControlStateNormal];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:([AppSize titleTextSize])],
                                 NSKernAttributeName: @([AppSize descriptionLetterSpacing])};
    self.updateButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.updateButton.titleLabel.text attributes:attributes];
    self.cancelButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.cancelButton.titleLabel.text attributes:attributes];
    
    self.answerTextField.text = MASKED_ANSWER;
    self.answerTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.answerTextField.text];
    self.answerTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.answerTextField.placeholder];
    self.answerTextField.font =  self.firstNameText.font;
    self.answerTextField.delegate = self;
    [self.answerTextField setMarginLeftRight:5];
    
    self.questionDropdown.leadingTitle = 5.0f;
    self.questionDropdown.titleFontSize = [AppSize titleTextSize];
    self.questionDropdown.title = @"Security Question";
    self.questionDropdown.titleColor = [AppColor placeholderTextColor];
    self.questionDropdown.isBreakLine = true;
    self.questionDropdown.delegate = self;
    self.questionDropdown.itemsFont = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.questionDropdown.titleLabel.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    [self.questionDropdown setItemHeight:50];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.answerTextField setBottomBolderDefaultColor];
        [self.questionDropdown setBottomBolderDefaultColor];
    });
}

- (void) setButtonFrame{
    signBtnWidth = [[UIScreen mainScreen] bounds].size.width - 40;  //394.0f * SCREEN_SCALE;
    signBtnHeight = 55.0f * SCREEN_SCALE;
    signFrame = CGRectMake((SCREEN_WIDTH - signBtnWidth)/2.0f, SCREEN_HEIGHT - signBtnHeight - 30.0f, signBtnWidth, signBtnHeight);
    
    self.viewAction.frame = signFrame;
}

- (void)updateSuccessRedirect{    
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert =nil;
    alert.msgAlert = NSLocalizedString(@"update_profile_success_message", nil);
    alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
    dispatch_async(dispatch_get_main_queue(), ^{
        alert.seconBtn.hidden = YES;
        alert.midView.hidden = YES;
        [alert.view layoutIfNeeded];
    });
    
    alert.blockFirstBtnAction = ^(void){
        [self getUserInfo];
        [self setTextViewsDefaultBottomBolder];
        [self setButtonStatus:NO];
    };
    
    [self showAlert:alert forNavigation:NO];
}


- (IBAction)touchUpdate:(id)sender {
    [self verifyAccountData];
}

- (IBAction)touchCancel:(id)sender {
    [self getUserInfo];
    [self setTextViewsDefaultBottomBolder];
    [self dismissKeyboard];
    [self setButtonStatus:NO];
    
    int i = 0;
    for (ChallengeQuestionObject* obj in self->questionArray) {
        if ([originalChallengeQuestion.questionText isEqualToString:obj.questionText]) {
            [self.questionDropdown setSelectedIndex:i];
            break;
        }
        i++;
    }
}

-(void)resignFirstResponderForAllTextField
{
    [super resignFirstResponderForAllTextField];
    if(self.answerTextField.isFirstResponder)
    {
        [self.answerTextField resignFirstResponder];
    }
}

- (BOOL)verifyValueForTextField:(UITextField *)textFied andMessageError:(NSMutableString *)message
{
    NSString *currentText = nil;
    NSString *errorMsg = nil;
    BOOL isValid = NO;
    currentText = [textFied originText];
    if(textFied == self.firstNameText)
    {
        errorMsg = [NameValidation validateFirstNameWithName: (textFied.isFirstResponder ? textFied.text :currentText)];
        isValid = [AACStringUtils isEmptyWithString:errorMsg];
    }
    else if(textFied == self.lastNameText)
    {
        errorMsg = [NameValidation validateLastNameWithName: (textFied.isFirstResponder ? textFied.text :currentText)];
        isValid = [AACStringUtils isEmptyWithString:errorMsg];
    }
    else if(textFied == self.emailText)
    {
        errorMsg = NSLocalizedString(@"input_invalid_email_msg", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidEmail];
    }
    else if(textFied == self.phoneNumberText)
    {
        errorMsg = NSLocalizedString(@"input_invalid_phone_number_msg", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidPhoneNumber];
    }
    else if(textFied == self.zipCodeText)
    {
        errorMsg = NSLocalizedString(@"input_invalid_zip_code_msg", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidZipCode];
    } else if(textFied == self.answerTextField)
    {
        errorMsg = NSLocalizedString(@"input_invalid_answer_msg", nil);
        NSString* trimmedAnswer = [self.inputNewAnswer stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString* trimmedTextField = [textFied.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([self isChangedQuestion] == false) {
            if ([AACStringUtils isEmptyWithString:trimmedAnswer] == true ||
                [AACStringUtils isEmptyWithString:trimmedTextField] == true) {
                isValid = true;
            } else {
                if (trimmedAnswer.length > 3) {
                    isValid = true;
                }
            }
        } else {
            if (trimmedAnswer.length > 3) {
                isValid = true;
            }
        }
    }
    
    if(!isValid)
    {
        (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
        [message appendString:errorMsg];
        [textFied setBottomBorderRedColor];
        if(!hasFoucusedFiled)
        {
            [textFied becomeFirstResponder];
            hasFoucusedFiled = YES;
        }
        
    }
    else
    {
        [textFied setBottomBolderDefaultColor];
    }
    
    return isValid;
}

-(void)verifyAccountData
{
    [self resignFirstResponderForAllTextField];
    hasFoucusedFiled = NO;
    NSMutableString *message = [[NSMutableString alloc] init];
    if(self.firstNameText.text.length == 0 || self.lastNameText.text.length == 0 || self.emailText.text.length == 0 || self.phoneNumberText.text.length == 0 || self.zipCodeText.text.length == 0 )
    {
        [message appendString:@"* "];
        [message appendString:NSLocalizedString(@"all_field_required", nil)];
        
        [self verifyValueForTextField:self.firstNameText andMessageError:message];
        [self verifyValueForTextField:self.lastNameText andMessageError:message];
        [self verifyValueForTextField:self.emailText andMessageError:message];
        [self verifyValueForTextField:self.phoneNumberText andMessageError:message];
        [self verifyValueForTextField:self.zipCodeText andMessageError:message];
        [self verifyValueForTextField:self.answerTextField andMessageError:message];
        [self createProfileInforWithMessage:message isCreatedSuccessful:NO];
    }
    else{
        [self verifyValueForTextField:self.firstNameText andMessageError:message];
        [self verifyValueForTextField:self.lastNameText andMessageError:message];
        [self verifyValueForTextField:self.emailText andMessageError:message];
        [self verifyValueForTextField:self.phoneNumberText andMessageError:message];
        [self verifyValueForTextField:self.zipCodeText andMessageError:message];
        [self verifyValueForTextField:self.answerTextField andMessageError:message];
        
        [self createProfileInforWithMessage:message isCreatedSuccessful:(message.length == 0)];
    }
}

- (void)updateProfileWithAPI {
    [self.view endEditing:true];
    [self.answerTextField setBottomBolderDefaultColor];
    
    NSMutableDictionary *userDict = [[NSMutableDictionary alloc] init];
    [userDict setValue:B2C_ConsumerKey forKey:@"referenceName"];
    [userDict setValue:[self.firstNameText originText] forKey:@"FirstName"];
    [userDict setValue:[self.lastNameText originText] forKey:@"LastName"];
    [userDict setValue:[[[self.phoneNumberText originText] substringFromIndex:1] stringByReplacingOccurrencesOfString:@"-" withString:@""] forKey:@"MobileNumber"];
    [userDict setValue:[self.emailText originText] forKey:@"Email"];
    [userDict setValue:[[self.zipCodeText originText] stringByReplacingOccurrencesOfString:@"-" withString:@""] forKey:@"ZipCode"];
    [userDict setValue:@"Unknown" forKey:@"City"];
    [userDict setValue:@"USA" forKey:@"Country"];
    
    [userDict setObject:[[SessionData shareSessiondata] CurrentPolicyVersion] forKey:APP_USER_PREFERENCE_Policy_Version];
    [userDict setValue:[[SessionData shareSessiondata] BINNumber] forKey:APP_USER_PREFERENCE_Passcode];
    [userDict setObject:isCheck ? @"ON" : @"OFF" forKey:APP_USER_PREFERENCE_Newsletter];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [userDict setObject:[dateFormatter stringFromDate:[NSDate date]] forKey:APP_USER_PREFERENCE_Newsletter_Date];
    if ([User isValid]) {
        UserObject* temp = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([UserObject class])];
        if (temp.selectedCity) {
            if (temp.selectedCity.length > 0)
                [userDict setObject:temp.selectedCity forKey:APP_USER_PREFERENCE_Selected_City];
        }
    }
    
    __block NSString*answer = self.inputNewAnswer;
    
    typeof(self) __weak _self = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self startActivityIndicator];
    });
    
    __block BOOL isChangeString = [self checkFieldIsChangeString];
    __block BOOL isChangeAnswer = [self isChangeAnswer];
    __block NSError* errorTotal = nil;
    
    dispatch_queue_t queue = dispatch_queue_create("com.s3corp.update", DISPATCH_QUEUE_SERIAL);
    
    // update profile
    dispatch_async(queue, ^{
        if (isChangeString) {
            dispatch_suspend(queue);
            [ModelAspireApiManager updateProfileCurrentUserWithUserInfo:userDict completion:^(NSError *error) {
                errorTotal = error;
                dispatch_resume(queue);
            }];
        }
    });
    
    // change answer
    dispatch_async(queue, ^{
        if(errorTotal == nil && isChangeAnswer) {
            dispatch_suspend(queue);
            [ModelAspireApiManager changeRecoveryQuestionForEmail:[[SessionData shareSessiondata] UserObject].email password:[[AAFPassword alloc] initWithPassword:[AccountAuthentication password]] question:[[AAFRecoveryQuestion alloc] initWithQuestion:selectedChallengeQuestion.questionText answer:answer] completion:^(NSError * _Nullable error) {
                errorTotal = error;
                if (errorTotal == nil) {
                    typeof(self) __self = _self;
                    __self->originalChallengeQuestion = __self->selectedChallengeQuestion;
                }
                dispatch_resume(queue);
            }];
        }
    });
    
    // result
    dispatch_barrier_async(queue, ^{
        typeof(self) __self = _self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [_self stopActivityIndicator];
            if (!errorTotal) {
                __self.answerTextField.text = MASKED_ANSWER;
                __self.inputNewAnswer = @"";
                
                [[SessionData shareSessiondata] setUserObject:(UserObject *)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([UserObject class])]];
                [_self updateSuccessRedirect];
                return;
            }
            
            NSString* message = @"";
            switch ([AspireApiError getErrorTypeAPIUpdateProfileFromError:errorTotal]) {
                case NETWORK_UNAVAILABLE_ERR:{
                    [_self showNoNetWorkAlert];
                }
                    break;
                case UNKNOWN_ERR:
                    message = NSLocalizedString(@"error_api_message", nil);
                    break;
                    
                default:
                    message = NSLocalizedString(@"error_api_message", nil);
                    break;
            }
            
            switch ([AspireApiError getErrorTypeAPIChangeRecoveryQuestionOKTAFromError:errorTotal]) {
                case NETWORK_UNAVAILABLE_ERR:{
                    [_self showNoNetWorkAlert];
                }
                    break;
                case UNKNOWN_ERR:
                    message = NSLocalizedString(@"error_api_message", nil);
                    break;
                case ANSWER_ERR:
                    message = NSLocalizedString(@"input_invalid_answer_msg", nil);
                    break;
                case EMAIL_INVALID_ERR:
                    message = NSLocalizedString(@"error_api_message", nil);
                    break;
                case CHANGE_RECOVERY_QUESTION_ERR:
                    message = NSLocalizedString(@"error_api_message", nil);
                    break;
                default:
                    message = NSLocalizedString(@"error_api_message", nil);
                    break;
            }
            
            if (message.length == 0) return;
            AlertViewController *alert = [[AlertViewController alloc] init];
            alert.titleAlert = NSLocalizedString(@"cannot_get_data", nil);
            alert.msgAlert = message;
            alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
            alert.seconBtn.hidden = YES;
            alert.midView.hidden = YES;
            alert.lblAlertMessage.textAlignment = NSTextAlignmentCenter;
            [alert.view layoutIfNeeded];
            [_self showAlert:alert forNavigation:NO];
        });
    });
}

- (void)updateProfileButtonStatus {
    BOOL gotChanged = false;
    if ([self isAllFieldGotData]) {
        BOOL fieldsIsChanged = [self checkFieldIsChangeString];
        if (fieldsIsChanged) {
            gotChanged = true;
        } else {
            if ([AACStringUtils isEmptyWithString:self.inputNewAnswer] == false) {
                gotChanged = true;
            }
        }
    }
    [self setButtonStatus:gotChanged];
}

- (BOOL) isTextFieldEmpty: (UITextField *) textField {
    NSString* text = textField.isFirstResponder ? textField.text:[textField originText];
    if([AACStringUtils isEmptyWithString:text] == true) {
        return true;
    }
    return false;
}

- (BOOL) isAllFieldGotData {
    if([self isTextFieldEmpty:self.firstNameText]) {
        return false;
    }
    if ([self isTextFieldEmpty:self.lastNameText]) {
        return false;
    }
    if ([self isTextFieldEmpty:self.emailText]) {
        return false;
    }
    if ([self isTextFieldEmpty:self.phoneNumberText]) {
        return false;
    }
    if ([self isTextFieldEmpty:self.zipCodeText]) {
        return false;
    }
    if ([self isChangedQuestion]) {
        if ([AACStringUtils isEmptyWithString:self.inputNewAnswer]) {
            return false;
        }
    } else {
        if ([AACStringUtils isEmptyWithString:self.answerTextField.text]) {
            return false;
        }
    }
    return true;
}

- (void)checkTextFieldIsChange:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *resultText = [textField.text stringByReplacingCharactersInRange:range
                                                                   withString:string];
    
    if (textField == self.firstNameText && resultText.length <= MAX_FIRSTNAME_TEXT_LENGTH) {
        textField.text = resultText;
    }else if (textField == self.lastNameText  && resultText.length <= MAX_FIRSTNAME_TEXT_LENGTH) {
        textField.text = resultText;
    }else if (textField == self.emailText  && resultText.length <= 100){
        textField.text = resultText;
    }else if (textField == self.phoneNumberText  && resultText.length <= 14){
        textField.text = resultText;
    }else if (textField == self.zipCodeText  && resultText.length <= 10){
        textField.text = resultText;
    }
    [self updateProfileButtonStatus];
}

- (BOOL) checkFieldIsChangeString {
    if (!userInfo) return false;
    BOOL (^tempBlock)(void) = ^{
        NSString* phone1 = userInfo.mobileNumber;
        NSString* phone2 = self.phoneNumberText.isFirstResponder ? self.phoneNumberText.text :  [self.phoneNumberText originText];
        if (phone1.length > 2) {
            if (phone1.length > 10) phone1 = [phone1 substringFromIndex:phone1.length - 10];
            if ([[phone1 substringToIndex:2] isEqualToString:@"1-"])
                phone1 = [phone1 substringFromIndex:1];
        }
        if (phone2.length > 2) {
            if ([[phone2 substringToIndex:2] isEqualToString:@"1-"])
                phone2 = [phone2 substringFromIndex:1];
        }
        phone1 = [phone1 stringByReplacingOccurrencesOfString:@"-" withString:@""];
        phone2 = [phone2 stringByReplacingOccurrencesOfString:@"-" withString:@""];
        
        if (![userInfo.firstName isEqualToString:self.firstNameText.isFirstResponder ? self.firstNameText.text : [self.firstNameText originText]]) {
            return YES;
        }else if (![userInfo.lastName isEqualToString:self.lastNameText.isFirstResponder ? self.lastNameText.text : [self.lastNameText originText]]){
            return YES;
        }
        else if (![userInfo.email isEqualToString:[self.emailText originText]]){
            return YES;
        }
        else if (![phone1 isEqualToString:phone2]){
            return YES;
        }
        else if (![[userInfo.zipCode stringByReplacingOccurrencesOfString:@"-" withString:@""] isEqualToString:[self.zipCodeText.text stringByReplacingOccurrencesOfString:@"-" withString:@""]]){
            return YES;
        }else if (userInfo.optStatus != isCheck){
            return YES;
        }
        return NO;
    };
    
    __block BOOL temp = false;
    
    if (![NSThread isMainThread]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            temp = tempBlock();
        });
    } else {
        return tempBlock();
    }
    return temp;
}

- (BOOL) isChangedQuestion {
    if (originalChallengeQuestion != nil && [selectedChallengeQuestion isEqual:originalChallengeQuestion] == false) {
        return true;
    }
    return false;
}

- (BOOL) isChangeAnswer {
    if ([self isChangedQuestion]) {
        return true;
    }
    
    return [self.answerTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0 &&
    ![self.answerTextField.text isEqualToString:MASKED_ANSWER];
}

- (void)setButtonStatus:(BOOL)status{
    self.updateButton.enabled = status;
    self.cancelButton.enabled = status;
}

#pragma mark - TEXTFIELD DELEGATE
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == self.firstNameText)
    {
        [textField showTextMask:false];
        
    }
    else if(textField == self.lastNameText)
    {
        [textField showTextMask:false];
        
    }
    else if(textField == self.phoneNumberText)
    {
        if(textField.text.length == 2 && [[textField.text substringToIndex:2] isEqualToString:@"1-"]) {
            textField.text = nil;
        }
        [textField showTextMaskPhone];
    }
    else if(textField == self.emailText)
    {
        if([textField.text containsString:@"@"])
        {
            [textField showTextMask:true];
        }
    }
    else if(textField == self.zipCodeText)
    {
    }
    else if(textField == self.answerTextField)
    {
        if (self.inputNewAnswer.length > 0) {
            NSMutableString* temp = [NSMutableString stringWithString:@""];
            for (int i=0; i < self.inputNewAnswer.length; i++) {
                [temp appendString:@"*"];
            }
            self.answerTextField.text = temp;
        }
        NSString* updatedText = self.inputNewAnswer;
        if (updatedText != nil) {
            updatedText = [updatedText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        }
        if ([self isChangedQuestion] == true) {
            if ([AACStringUtils isEmptyWithString: updatedText] == false) {
                [self.answerTextField mask];
            }
        } else {
            if ([AACStringUtils isEmptyWithString: updatedText] == true) {
                [self.answerTextField maskFixedStars];
            }
        }
    }
    
    if (textField == self.emailText /*&& _isUpdateProfile*/) {
        self.emailText.text = [self.emailText originText];
        [self dismissKeyboard];
    }
    [self updateProfileButtonStatus];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [super textFieldDidBeginEditing:textField];
    if(textField == self.answerTextField) {
        [self updateProfileButtonStatus];
    }
}

#pragma mark - Dropdown delegate
- (void)didSelectItem:(DropDownView *)dropDownView atIndex:(int)index {
    selectedChallengeQuestion = [questionArray objectAtIndex:index];
    
    if ([selectedChallengeQuestion isEqual:originalChallengeQuestion]) {
        self.answerTextField.text = MASKED_ANSWER;
        [self.answerTextField setBottomBolderDefaultColor];
    } else {
        self.answerTextField.text = @"";
    }
    
    self.inputNewAnswer = @"";
    
    if ([dropDownView.titleLabel.text isEqualToString:@"Security Question"]) {
        dropDownView.titleColor = [AppColor placeholderTextColor];
    } else {
        dropDownView.titleColor = [AppColor textColor];
    }
    [self updateProfileButtonStatus];
}

-(void)didShow:(DropDownView *)dropDownView {
    [self.view endEditing:true];
}

- (void)didHiden:(DropDownView *)dropDownView {
    self.scrollView.contentInset = UIEdgeInsetsMake(self.scrollView.contentInset.top, self.scrollView.contentInset.left, 0, self.scrollView.contentInset.right);
}

@end
