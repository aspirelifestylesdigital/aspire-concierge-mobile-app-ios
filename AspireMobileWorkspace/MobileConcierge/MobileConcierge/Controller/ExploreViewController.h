//
//  ExploreViewController.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/9/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "CityItem.h"

@interface ExploreViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UIButton *cityBtn;
@property (nonatomic, weak) IBOutlet UIButton *categoryBtn;
@property (nonatomic, weak) IBOutlet UIButton *searchBtn;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *firstLine;
@property (nonatomic, strong) NSMutableArray *exploreLst;
@property (weak, nonatomic) IBOutlet UIView *secondLine;
@property (weak, nonatomic) IBOutlet UIStackView *menuButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *menuButtonsTopConstraint;
@property (weak, nonatomic) IBOutlet UIView *searchViewOutlet;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchViewOutletHeightConstraint;
@property (nonatomic, strong) CityItem *currentCity;

@end
