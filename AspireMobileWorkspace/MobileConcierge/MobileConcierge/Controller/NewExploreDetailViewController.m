//
//  NewExploreDetailViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 7/19/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//
#import "AskConciergeViewController.h"
// Utils
#import "Constant.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

// item
#import "AnswerItem.h"
#import "WSB2CGetContentFullFromCCA.h"
#import "ContentFullCCAResponseObject.h"
#import "ContentFullCCAItem.h"
#import "TileItem.h"
#import "SearchItem.h"
#import "WSB2CGetQuestionAndAnswers.h"
#import "AppData.h"
#import "CityItem.h"
#import "CategoryItem.h"
#import "UIImageView+AFNetworking.h"
#import "MCActivityItemSource.h"
#import "AlertViewController.h"
#import "MCImageActivityItemSource.h"
#import "MCTitleActivityItemSource.h"
#import "ImageUtilities.h"
#import "NewExploreDetailViewController.h"
#import "ExploreDetailTableCell.h"
#import "NSString+Utis.h"

@interface NewExploreDetailViewController ()<ExploreDetailTableCellDelegate, DataLoadDelegate>

@end

@implementation NewExploreDetailViewController
{
    ContentFullCCAItem *contentFullCCAItem;
    NSInteger totalItem;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    totalItem = 1;
    [self registTableView];
    [self setNavigationBarWithDefaultColorAndTitle:@""];
    [self backNavigationItem];
    [self createConciergeBarButton];
    [self setupDataForView];
    [self trackingScreenByName:@"Venue detail"];
}


- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setUpCustomizedPanGesturePopRecognizer];
}


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark REGISTER TABLE CELL
- (void) registTableView{
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerNib:[UINib nibWithNibName:@"ExploreDetailTableCell" bundle:nil] forCellReuseIdentifier:@"ExploreDetailTableCell"];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 150;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.allowsSelection = NO;
}

#pragma mark PROCESS VIEW LOGIC

-(UIImage *)getImageForCityGuideItem:(SearchItem *)searchItem
{
    NSArray *citySelections = [[AppData getInstance] getSelectionCityList];
    for(CityItem *cityItem  in citySelections)
    {
        for(CategoryItem *categoryItem in cityItem.subCategories)
        {
            if([searchItem.secondaryID isEqualToString:categoryItem.ID])
            {
                return categoryItem.categoryImgDetail;
            }
        }
    }
    
    return nil;
}
-(void)setupDataForView
{
    if([self.item isKindOfClass:[TileItem class]])
    {
        totalItem = 0;
        self.detailType = DetailType_Other;
        [self loadDataFromCCAServerWithItemId:self.item.ID];
    }
    else if([self.item isKindOfClass:[AnswerItem class]] && [self.item.categoryCode isEqualToString:@"dining"])
    {
        self.detailType = DetailType_Dining;
    }
    else if([self.item isKindOfClass:[AnswerItem class]] && [self.item.categoryCode isEqualToString:@"cityguide"])
    {
        self.detailType = DetailType_CityGuide;
    }
    else if([self.item isKindOfClass:[SearchItem class]])
    {
        totalItem = 0;
        SearchItem *searchItem = (SearchItem *)self.item;
        UIImage *headImage = [self getImageForCityGuideItem:searchItem];
        
        if([searchItem.product isEqualToString:@"CCA"])
        {
            self.detailType = DetailType_Other;
            [self loadDataFromCCAServerWithItemId:searchItem.ID];
        }
        else if([searchItem.product isEqualToString:@"IA"])
        {
            
            self.item.image = headImage;
            self.detailType = DetailType_Other;
            [self loadDataFromIAServerWithSearchItem:searchItem];
        }
    }
    SearchItem *searchItem = (SearchItem *)self.item;
    if(![self.item isKindOfClass:[SearchItem class]] || (([self.item isKindOfClass:[SearchItem class]]) && [searchItem.product isEqualToString:@"CCA"])) {
        [self setNavigationBarWithDefaultColorAndTitle:self.item.name.uppercaseString];
    }
}

-(void) loadDataFromIAServerWithSearchItem:(SearchItem *)item;
{
    [self startActivityIndicatorWithoutMask];
    
    WSB2CGetQuestionAndAnswers *wsAnswer = [[WSB2CGetQuestionAndAnswers alloc] init];
    wsAnswer.delegate = self;
    wsAnswer.categoryId = item.secondaryID;
    wsAnswer.questionId = item.ID;
    wsAnswer.task = WS_B2C_GET_SEARCH_DETAIL;
    [wsAnswer retrieveDataFromServer];
}

- (void) loadDataFromCCAServerWithItemId:(NSString *)itemID
{
    [self startActivityIndicatorWithoutMask];
    
    WSB2CGetContentFullFromCCA *wSB2CGetContentFullFromCCA = [[WSB2CGetContentFullFromCCA alloc] init];
    wSB2CGetContentFullFromCCA.delegate = self;
    wSB2CGetContentFullFromCCA.itemID = itemID;
    [wSB2CGetContentFullFromCCA loadDataFromServer];
}

-(void)shareUrl:(NSArray *)data{
    MCImageActivityItemSource *imageItemSource = [[MCImageActivityItemSource alloc] init];
    imageItemSource.data = data;
    
    MCTitleActivityItemSource *titleItemSource = [[MCTitleActivityItemSource alloc] init];
    imageItemSource.data = data;
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:@[titleItemSource,[data objectAtIndex:1],imageItemSource] applicationActivities:nil];
    [self presentViewController:activityVC animated:YES completion:^{
    }];
}

- (void) touchAskConcierge{
    AskConciergeViewController *vc = [[AskConciergeViewController alloc] init];
    if (self.detailType != DetailType_Other) {
        vc.itemName = self.item.name;
        vc.categoryName = self.item.categoryName;
        
        if (self.detailType == DetailType_CityGuide) {
            vc.cityName = self.cityName;
        }
    }else{
        vc.itemName = contentFullCCAItem.title;
        vc.categoryName = contentFullCCAItem.category;
    }
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) gotoAppleMapWith:(NSString*)address{
    // Check for iOS 6
    Class mapItemClass = [MKMapItem class];
    if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)])
    {
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder geocodeAddressString:address
                     completionHandler:^(NSArray *placemarks, NSError *error) {
                         
                         // Convert the CLPlacemark to an MKPlacemark
                         // Note: There's no error checking for a failed geocode
                         CLPlacemark *geocodedPlacemark = [placemarks objectAtIndex:0];
                         MKPlacemark *placemark = [[MKPlacemark alloc]
                                                   initWithCoordinate:geocodedPlacemark.location.coordinate
                                                   addressDictionary:geocodedPlacemark.addressDictionary];
                         
                         // Create a map item for the geocoded address to pass to Maps app
                         MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
                         [mapItem setName:((BaseItem*)self.item).name];
                         [MKMapItem openMapsWithItems:@[mapItem] launchOptions:nil];
                         
                     }];
    }
}

- (void) seeDetailMapWithAddress:(NSString *)address{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"alert_title", nil);
    alert.msgAlert = NSLocalizedString(@"category_more_info_msg", nil);
    alert.secondBtnTitle = NSLocalizedString(@"yes_button", nil);
    alert.blockSecondBtnAction = ^(void){
        [self gotoAppleMapWith:address];
    };
    
    alert.firstBtnTitle = NSLocalizedString(@"alert_cancel_button", nil);
    alert.blockFirstBtnAction = ^(void){};
    [self showAlert:alert forNavigation:YES];
}

- (void) gotoWebBrowser{
    
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"alert_title", nil);
    alert.msgAlert = NSLocalizedString(@"category_more_info_msg", nil);
    alert.secondBtnTitle = NSLocalizedString(@"yes_button", nil);
    alert.blockSecondBtnAction = ^(void){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:((AnswerItem*)self.item).URL]];
    };
    
    alert.firstBtnTitle = NSLocalizedString(@"alert_cancel_button", nil);
    alert.blockFirstBtnAction  = ^(void){};
    [self showAlert:alert forNavigation:YES];
}



- (void)openURL:(NSURL *)url{
    if ([[url absoluteString] containsString:@"http"]) {
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"alert_title", nil);
        alert.msgAlert = NSLocalizedString(@"category_more_info_msg", nil);
        alert.secondBtnTitle = NSLocalizedString(@"yes_button", nil);
        alert.blockSecondBtnAction = ^(void){
            [[UIApplication sharedApplication] openURL:url];
        };
        
        alert.firstBtnTitle = NSLocalizedString(@"alert_cancel_button", nil);
        alert.blockFirstBtnAction  = ^(void){};
        [self showAlert:alert forNavigation:YES];
    }
}


#pragma mark TABLE DELEGATE


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ExploreDetailTableCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ExploreDetailTableCell"];
    
    cell.delegate = self;
    [self cellHeader:cell];
    [self cellInfo:cell];
    [self cellInsider:cell];
    [self cellBenefits:cell];
    [self cellTerm:cell];
    [self cellPrice:cell];
    [self cellDescription:cell];
    [self cellHours:cell];
    
    [cell layoutIfNeeded];
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return totalItem;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewAutomaticDimension;
}


#pragma mark TABLE CELL VIEW
- (void)cellHeader:(ExploreDetailTableCell *)cell
{
    if (self.detailType != DetailType_Other) {
        cell.questionName = self.item.name;
        
        if(self.detailType == DetailType_CityGuide){
            
            cell.headImg.image = [ImageUtilities imageWithImage:self.item.image convertToSize:CGSizeMake(SCREEN_WIDTH, SCREEN_WIDTH)];;
            [cell layoutIfNeeded];
            //cell.headImg.image = self.item.image;
            
        }
        else{
            cell.imageURL = self.item.imageURL;
        }
        
    }else{
        cell.questionName = contentFullCCAItem.title;
        cell.imageURL = [contentFullCCAItem.imgURL isEqualToString:@""] ? self.item.imageURL : contentFullCCAItem.imgURL;
        cell.shareLink = @"";
//        cell.diningDetailHeaderTableViewCellDelegate = self;
    }
    
    if(cell.imageURL)
    {
        __weak typeof(cell) weakCell = cell;
        __weak typeof(self.tableView) weakTable = self.tableView;
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[weakCell.imageURL removeBlankFromURL]]] ;
        
        [cell.headImg setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"placehoder_explore_detail"] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
            
            float imgWidth = image.size.width;
            image = [ImageUtilities imageWithImage:image convertToSize:CGSizeMake(SCREEN_WIDTH, (SCREEN_WIDTH/imgWidth)*image.size.height)];
            
            weakCell.headImg.image = image;
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            if(([weakTable.visibleCells containsObject:weakCell]))
            {
                [weakTable reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            }
             
            [weakCell layoutIfNeeded];
            
        } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
        }];
    }
    
    [cell loadDataForHeader];
}


- (void)cellInfo:(ExploreDetailTableCell *)cell{
    if(self.detailType != DetailType_Other)
    {
        if (self.detailType == DetailType_CityGuide) {
            cell.address1 = [NSString stringWithFormat:@"%@ %@, %@ %@ %@ %@",((AnswerItem*)self.item).address1, ((AnswerItem*)self.item).cityName, ((AnswerItem*)self.item).state, ((AnswerItem*)self.item).zipCode, ((AnswerItem*)self.item).address3, ((AnswerItem*)self.item).address2];
        }else{
            cell.address1 = [NSString stringWithFormat:@"%@, %@, %@, %@ %@",((AnswerItem*)self.item).address1, ((AnswerItem*)self.item).cityName, ((AnswerItem*)self.item).state, ((AnswerItem*)self.item).zipCode, ((AnswerItem*)self.item).address3];
        }
        cell.googleAddress = [NSString stringWithFormat:@"%@, %@, %@",((AnswerItem*)self.item).address1, ((AnswerItem*)self.item).cityName, ((AnswerItem*)self.item).state];
        cell.webLink = ((AnswerItem*)self.item).URL;
    }
    else
    {
        cell.address1 = nil;
        cell.googleAddress = nil;
        cell.webLink = nil;
    }
 
    [cell loadDataForInfo];
}

- (void)cellInsider:(ExploreDetailTableCell *)cell{

    if(self.detailType != DetailType_Other)
    {
        cell.insiderTip = ((AnswerItem*)self.item).insiderTip;
    }
    else{
        cell.insiderTip = nil;
    }
    
    [cell loadDataForInsiderTip];
}


- (void)cellHours:(ExploreDetailTableCell *)cell{
    if (self.detailType == DetailType_Dining) {
        cell.workingHour = ((AnswerItem*)self.item).hoursOfOperation;
    }
    else{
        cell.workingHour = nil;
    }
    
    [cell loadDataForHours];
}

- (void)cellPrice:(ExploreDetailTableCell *)cell{
    if (self.detailType == DetailType_Dining) {
        cell.priceRange = [NSString stringWithFormat:@"%.2f",((AnswerItem*)self.item).price];
        cell.cuisine = ((AnswerItem*)self.item).userDefined1;
    }
    else{
        cell.priceRange = nil;
        cell.cuisine = nil;
    }
    [cell loadDataForPrice];
}

- (void)cellBenefits:(ExploreDetailTableCell *)cell{
    
    if (self.detailType == DetailType_Dining) {
        cell.benefits = ((AnswerItem*)self.item).offer2;
        
    }
    else if(self.detailType == DetailType_Other)
    {
        cell.itemID = contentFullCCAItem.ID;
        cell.benefits = contentFullCCAItem.offerText;
    }
    else{
        cell.benefits = nil;
    }
    
    [cell loadDataForBenefits];
}


- (void)cellDescription:(ExploreDetailTableCell *)cell{
    //cell.titleLbl.text = @"DESCRIPTION";
    if (self.detailType != DetailType_Other) {
        if(self.detailType == DetailType_Dining)
        {
            cell.lblDecriptionTitle.hidden = YES;
        }
        cell.cellDescription = ((AnswerItem*)self.item).answerDescription;
    }else{
        cell.cellDescription = contentFullCCAItem.descriptionContent;
    }
    
    [cell loadDataForDescription];
}

- (void)cellTerm:(ExploreDetailTableCell *)cell{

    cell.lblTermTitle.text = NSLocalizedString(@"terms_and_conditions_explore_detail", nil);
    if(self.detailType == DetailType_Dining){
        AnswerItem *answerIten = (AnswerItem*)self.item;
        
        cell.termDetail = answerIten.offer2.length > 0 ? NSLocalizedString(@"terms_and_conditions", nil) : @"";
    }
    else if(self.detailType == DetailType_Other){
        cell.termDetail = NSLocalizedString(@"terms_and_use", nil);
    }
    else{
        cell.termDetail = nil;
    }
    
    [cell loadDataForTermOfCondition];
}


#pragma mark DATA LOAD DELEGATE


- (void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_B2C_GET_SEARCH_DETAIL && ws.data.count > 0)
    {
        UIImage *headImage = self.item.image;
        self.item = [ws.data objectAtIndex:0];
        [self setNavigationBarWithDefaultColorAndTitle:self.item.name.uppercaseString];
        if(headImage){
            self.detailType = DetailType_CityGuide;
            self.item.image = headImage;
        }
        else{
            self.detailType = DetailType_Dining;
        }
    }
    else if (ws.data.count > 0) {
        contentFullCCAItem = (ContentFullCCAItem*)[ws.data objectAtIndex:0];
    }
    totalItem = 1;
    [self.tableView reloadData];
    [self stopActivityIndicatorWithoutMask];
}
@end
