//
//  SignInViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 4/26/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "PasscodeController.h"
#import "SWRevealViewController.h"
#import "PrivacyPolicyViewController.h"
#import "Constant.h"
#import "NSString+Utis.h"
#import "UIButton+Extension.h"
#import "UILabel+Extension.h"
#import "UITextField+Extensions.h"
#import "Common.h"

#import "AppData.h"
#import "PolicyViewController.h"
#import "TermsOfUseViewController.h"
#import "CreateProfileViewController.h"
#import "HomeViewController.h"
#import "UtilStyle.h"
#import "UIView+Extension.h"
#import "EnableSubmitBINButton.h"
#import "WSB2CVerifyBIN.h"
#import "BINItem.h"
#import "WSB2CGetRequestToken.h"
#import "WSB2CGetUserDetails.h"
#import "WSB2CGetAccessToken.h"
#import "WSB2CGetRequestToken.h"
#import "WSB2CGetPolicyInfo.h"
#import "PolicyInfoItem.h"

#import "MenuViewController.h"
//#import <AspireApiFramework/AspireApiFramework.h>
@import AspireApiFramework;

#import "CheckEmailView.h"
#import "AppDelegate.h"

@import AspireApiControllers;

@interface PasscodeController ()<DataLoadDelegate, UITextFieldDelegate,UITextViewDelegate,EnableSubmitBINButton>
{
    CGFloat backupBottomConstraint;
    UIImageView *asteriskImageView;
    BOOL isReadTerm;
    BOOL isReadPolicy;
    UILabel *termOfUseLbl;
    UILabel *policyLbl;
    BOOL isLoadedPolicy;
    __weak IBOutlet UIButton *btnBack;
    
    WSB2CGetRequestToken *wsRequestToken;
}
@end

@implementation PasscodeController

- (void)viewDidLoad {
    isNotAskConciergeBarButton = YES;
    [super viewDidLoad];
    
    [self.scrollView setBackgroundColorForView];
    [self.mainView setBackgroundColorForView];
    [self setTextStyleForView];
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"concierge", nil)];
    self.binNumberText.delegate = self;
    self.binNumberText.returnKeyType = UIReturnKeyDone;
    self.binNumberText.keyboardType = UIKeyboardTypeNumberPad;
    [self.binNumberText addTarget:self
                           action:@selector(textFieldDidChange)
                 forControlEvents:UIControlEventEditingChanged];
    
    UITapGestureRecognizer *tappedOutsideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tappedOutsideKeyboard];
    
    if(self.isBINInvalid)
    {
        btnBack.hidden = true;
//        _titleLabel.hidden = true;
        [self expiredBinMessage];
    }
    [self initRightViewForTextField];
    [self hideAsteriskLabel];
    [self loadPolicy];
    //    [greetingsTooltip dismiss];
    [self trackingScreenByName:@"Sign in"];
}

- (void) expiredBinMessage{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = nil;
    alert.msgAlert =@"We’re sorry, but your issuing bank has discontinued subscription of your card type to Mastercard Concierge. Click APPLY below to apply for an eligible card. To try again, click Cancel.";
    alert.firstBtnTitle = NSLocalizedString(@"alert_cancel_button", nil);
    alert.secondBtnTitle = NSLocalizedString(@"apply_button", nil);
    alert.blockSecondBtnAction = ^(void){
        NSURL *url = [NSURL URLWithString:MASTERCARD_REGISTER_URL];
        [[UIApplication sharedApplication] openURL:url];
    };
    
    [self showAlert:alert forNavigation:NO];
}

- (void) loadPolicy{
    // load policy privacy
    isLoadedPolicy = false;
    WSB2CGetPolicyInfo *wsPolicy = [[WSB2CGetPolicyInfo alloc] init];
    wsPolicy.delegate = self;
    [wsPolicy retrieveDataFromServer];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //    For Next Sprint
    [self.view layoutIfNeeded];
    
    
    if ([self.confirmView.subviews count] <= 0) {
        [self buildAgreeTextViewFromString:NSLocalizedString(@"bin_confirm_msg",nil)];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWill:) name:UIKeyboardWillHideNotification object:nil];
    
    
    [self.checkBoxButton setImage:[UIImage imageNamed:@"checkbox_uncheck"] forState:UIControlStateNormal];
    [self.checkBoxButton setImage:[UIImage imageNamed:@"checkbox_check"] forState:UIControlStateSelected];
    [self.checkBoxButton addTarget:self action:@selector(enableSubmitButton:) forControlEvents:UIControlEventTouchUpInside];
    
    backupBottomConstraint = self.submitButtonBottomConstraint.constant;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setTextStyleForView
{
    self.titleLabel.attributedText = [UtilStyle setTextStyleForTitleViewControllerWithMessage:NSLocalizedString(@"concierge", nil)];
    
    // Text Style For Greet Message
    NSString *greetMsg = NSLocalizedString(@"signin_greeting_message", nil);
    NSRange range = NSMakeRange(0, greetMsg.length);
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:greetMsg];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionTextSize]] range:range];
    [attributeString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:range];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init] ;
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    paragraphStyle.lineSpacing = [AppSize titleLineSpacing];
    [attributeString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    
    self.greetingLabel.attributedText = attributeString;
    self.greetingLabel.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionTextSize]];
    self.confirmLabel.font = [UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize titleTextSize]];
    
    // Text Style For Title
    greetMsg = NSLocalizedString(@"concierge", nil);
    self.titleViewController.attributedText = [UtilStyle setTextStyleForTitleViewControllerWithMessage:greetMsg];
    
    // Text Style For BIN TextField
    [self.binNumberText setFont:[UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize titleTextSize]]];
    [self.binNumberText setTextColor:[AppColor textColor]];
    
    self.binNumberText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.binNumberText.placeholder];
    
    // Text Style For Submit Button
    [self.submitButton setTitle:NSLocalizedString(@"submit_button_title", nil) forState:UIControlStateNormal];
    [self.submitButton setBackgroundColorForNormalStatus];
    [self.submitButton setBackgroundColorForTouchingStatus];
    [self.submitButton setBackgroundColorForDisableStatus];
    [self.submitButton configureDecorationForButton];
    [self.submitButton addTarget:self action:@selector(submitTapped:) forControlEvents:UIControlEventTouchUpInside];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:([AppSize titleTextSize])],
                                 NSKernAttributeName: @([AppSize descriptionLetterSpacing])};
    self.submitButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.submitButton.titleLabel.text attributes:attributes];
}


- (void)initRightViewForTextField{
    asteriskImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"exclamation_icon"]];
    asteriskImageView.frame = CGRectMake(0.0f, 0.0f, 20.0f*SCREEN_SCALE, 20.0f*SCREEN_SCALE);
    asteriskImageView.contentMode = UIViewContentModeCenter;
    
    [self.binNumberText setRightView:asteriskImageView];
    [self.binNumberText setRightViewMode:UITextFieldViewModeAlways];
}

- (void) showAsteriskLabel{
    asteriskImageView.hidden = NO;
}

- (void) hideAsteriskLabel{
    asteriskImageView.hidden = YES;
}


-(BOOL)isDisableSubmitTapped:(id)sender
{
    if([sender isKindOfClass:[UITapGestureRecognizer class]])
    {
        UITapGestureRecognizer *tapRecognization = (UITapGestureRecognizer *)sender;
        CGPoint location = [tapRecognization locationInView:self.view];
        CGRect fingerRect = CGRectMake(location.x-5, location.y-5, 10, 10);
        
        if(CGRectIntersectsRect(fingerRect, self.submitButton.frame) && !self.submitButton.enabled)
        {
            [self dismissKeyboard];
            return YES;
        }
    }
    
    return NO;
}
-(void) enableSubmitButton:(id)sender
{
    if([self isDisableSubmitTapped:sender])
    {
        return;
    }
    
    [self.checkBoxButton setHighlighted:NO];
    self.checkBoxButton.selected = !(self.checkBoxButton.selected);
    self.checkBoxButton.accessibilityIdentifier = self.checkBoxButton.selected ? @"btnCheck" : @"btnUnCheck";
    [self.checkBoxButton setBackgroundColor:[UIColor clearColor]];
    if([self.binNumberText.text isValidBinNumber])
    {
        [self.submitButton setEnabled:self.checkBoxButton.selected];
    }
    
    for (UIView *i in self.confirmView.subviews){
        if([i isKindOfClass:[UILabel class]]){
            if (((UILabel*)i).tag == 1000) {
                /// Write your code
                if (self.checkBoxButton.selected) {
                    [(UILabel*)i setTextColor:[UIColor whiteColor]];
                }else{
                    [(UILabel*)i setTextColor:[AppColor disableTextColor]];
                }
            }
        }
    }
}

- (void)submitTapped:(id)sender {
    if(ISLOGINGINFO){
        NSLog(@"card number: %@", self.binNumberText.text);
    }
    
    [self dismissKeyboard];
    if(![self.binNumberText.text isValidBinNumber])
    {
        if ([self.binNumberText.text length] < 7) {
            [self showAsteriskLabel];
            //            [self showTooltip];
        }else{
            [self showAlertForMoreInformationWithURL:[NSURL URLWithString:MASTERCARD_REGISTER_URL]];
        }
        return;
    }
    
    [self startActivityIndicator];
    typeof(self) __weak _self = self;
    [ModelAspireApiManager verifyBin:[self.binNumberText.text stringByReplacingOccurrencesOfString:@"-" withString:@""] completion:^(BOOL isPass, NSString *bin, id  _Nullable error) {
        if(!_self) return;
        if (isPass && bin) {
            [ModelAspireApiManager registerServiceUsername:aspire_api_service_username
                                                  password:aspire_api_service_password
                                                   program:aspire_api_program
                                                       bin:bin];
            [[SessionData shareSessiondata] setBINNumber:bin];
            
            if(![AppData isCreatedProfile])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_self stopActivityIndicator];
                    CheckEmailView* v = [[CheckEmailView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
                    AACCheckEmailController* vc = [AACCheckEmailController new];
                    vc.subview = v;
                    v.controller = vc;
                    [_self.navigationController pushViewController:vc animated:YES];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_self stopActivityIndicator];
                    [[SessionData shareSessiondata] setUserObject:(UserObject *)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([UserObject class])]];
                    [_self trackingEventByName:@"Sign in" withAction:ClickActionType withCategory:AuthenticationCategoryType];
                    [_self stopActivityIndicator];
                    
                    AppDelegate*appdelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
                    [UIView transitionWithView:appdelegate.window
                                      duration:0.5
                                       options:UIViewAnimationOptionPreferredFramesPerSecond60
                                    animations:^{
                                        
                                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                        MenuViewController *menuViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
                                        UINavigationController *menuNavigationViewController = [[UINavigationController alloc] initWithRootViewController:menuViewController];
                                        UIViewController *fontViewController = [[HomeViewController alloc] init];
                                        
                                        SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:menuNavigationViewController frontViewController:fontViewController];
                                        
                                        revealController.delegate = appdelegate;
                                        revealController.rearViewRevealWidth = SCREEN_WIDTH;
                                        revealController.rearViewRevealOverdraw = 0.0f;
                                        revealController.rearViewRevealDisplacement = 0.0f;
                                        appdelegate.viewController = revealController;
                                        appdelegate.window.rootViewController = appdelegate.viewController;
                                        [appdelegate.window makeKeyWindow];
                                        
                                        UIViewController *newFrontController = [[HomeViewController alloc] init];
                                        UINavigationController *newNavigationViewController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                                        [revealController pushFrontViewController:newNavigationViewController animated:YES];
                                    }
                                    completion:nil];
                });
            }
            
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [_self stopActivityIndicator];
                [_self onErrorVerifyBIN:error];
            });
        }
    }];
}

- (void) onErrorVerifyBIN:(NSError*) error {
    if ([[[SessionData shareSessiondata] BINNumber] isEqualToString:[self.binNumberText.text stringByReplacingOccurrencesOfString:@"-" withString:@""]]) {
        [self expiredBinMessage];
    }else{
        if (error != nil) {
            switch ([AspireApiError getErrorTypeAPIVerifyBinFromError:error]) {
                case NETWORK_UNAVAILABLE_ERR:
                    [self showNoNetWorkAlert];
                    break;
                default:
                    [self showAlertForMoreInformationWithURL:[NSURL URLWithString:MASTERCARD_REGISTER_URL]];
                    break;
            }
        } else {
            [self showAlertForMoreInformationWithURL:[NSURL URLWithString:MASTERCARD_REGISTER_URL]];
        }
    }
}

- (IBAction)backEvent:(id)sender {
    if (self.navigationController) {
        [self.navigationController popViewControllerAnimated:true];
    } else {
        [self dismissViewControllerAnimated:true completion:nil];
    }
}

-(void) showAlertForMoreInformationWithURL:(NSURL*)url
{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = nil;
    alert.msgAlert = NSLocalizedString(@"create_mastercard_account_message",nil);
    alert.secondBtnTitle = NSLocalizedString(@"apply_button", nil);
    alert.blockSecondBtnAction = ^(void){
        [[UIApplication sharedApplication] openURL:url];
    };
    
    alert.firstBtnTitle = NSLocalizedString(@"alert_cancel_button", nil);
    alert.blockFirstBtnAction  = ^(void){};
    
    [self showAlert:alert forNavigation:NO];
}
-(void)dismissKeyboard
{
    if([self.binNumberText isFirstResponder])
    {
        [self.binNumberText resignFirstResponder];
    }
}

#pragma mark ENABLE SUBMIT BUTTON DELEGATE
-(void)enableSubmitBINButton:(NSString *)currentViewController
{
    if([currentViewController isEqualToString:@"TermsOfUseViewController"])
    {
        isReadTerm = YES;
    }
    else if([currentViewController isEqualToString:@"PolicyViewController"])
    {
        isReadPolicy = YES;
    }
    
    if(isReadTerm && isReadPolicy)
    {
        self.checkBoxButton.selected = YES;
        [self.checkBoxButton setBackgroundColor:[UIColor clearColor]];
        [self.submitButton setEnabled:self.checkBoxButton.selected];
    }
}

#pragma mark CONFIRM MESSAGE PROCESS

- (void)buildAgreeTextViewFromString:(NSString *)localizedString
{
    // 1. Split the localized string on the # sign:
    NSArray *localizedStringPieces = [localizedString componentsSeparatedByString:@"#"];
    self.confirmView.userInteractionEnabled = YES;
    // 2. Loop through all the pieces:
    NSUInteger msgChunkCount = localizedStringPieces ? localizedStringPieces.count : 0;
    CGPoint wordLocation = CGPointMake(0.0, 0.0);
    for (NSUInteger i = 0; i < msgChunkCount; i++)
    {
        NSString *chunk = [localizedStringPieces objectAtIndex:i];
        if ([chunk isEqualToString:@""])
        {
            continue;     // skip this loop if the chunk is empty
        }
        
        // 3. Determine what type of word this is:
        BOOL isTermsOfServiceLink = [chunk hasPrefix:@"<ts>"];
        BOOL isPrivacyPolicyLink  = [chunk hasPrefix:@"<pp>"];
        BOOL isLink = (BOOL)(isTermsOfServiceLink || isPrivacyPolicyLink);
        
        // 4. Create label, styling dependent on whether it's a link:
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize titleTextSize]];
        
        label.text = chunk;
        label.userInteractionEnabled = isLink;
        
        if (isLink)
        {
            // 5. Set tap gesture for this clickable text:
            SEL selectorAction = isTermsOfServiceLink ? @selector(tapOnTermsOfServiceLink:) : @selector(tapOnPrivacyPolicyLink:);
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                         action:selectorAction];
            [label addGestureRecognizer:tapGesture];
            
            // Trim the markup characters from the label:
            if (isTermsOfServiceLink)
            {
                label.text = [label.text stringByReplacingOccurrencesOfString:@"<ts>" withString:@""];
                termOfUseLbl = label;
            }
            
            
            if (isPrivacyPolicyLink)
            {
                label.text = [label.text stringByReplacingOccurrencesOfString:@"<pp>" withString:@""];
                policyLbl = label;
            }
            
            
            NSArray * objects = [[NSArray alloc] initWithObjects:[AppColor activeButtonColor], [NSNumber numberWithInt:NSUnderlineStyleSingle], nil];
            NSArray * keys = [[NSArray alloc] initWithObjects:NSForegroundColorAttributeName, NSUnderlineStyleAttributeName, nil];
            
            NSDictionary * linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
            
            NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:label.text attributes:linkAttributes];
            
            [label setAttributedText:attributedString];
            
        }
        else
        {
            UITapGestureRecognizer *tappedOutsideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enableSubmitButton:)];
            [label addGestureRecognizer:tappedOutsideKeyboard];
            label.userInteractionEnabled = YES;
            [label setTextColor:[AppColor disableTextColor]];
            label.tag = 1000;
        }
        
        // 6. Lay out the labels so it forms a complete sentence again:
        
        // If this word doesn't fit at end of this line, then move it to the next
        // line and make sure any leading spaces are stripped off so it aligns nicely:
        
        [label sizeToFit];
        
        if (self.confirmView.frame.size.width < wordLocation.x + label.bounds.size.width)
        {
            wordLocation.x = 0.0;                       // move this word all the way to the left...
            wordLocation.y += label.frame.size.height;  // ...on the next line
            
            // And trim of any leading white space:
            NSRange startingWhiteSpaceRange = [label.text rangeOfString:@"^\\s*"
                                                                options:NSRegularExpressionSearch];
            if (startingWhiteSpaceRange.location == 0)
            {
                label.text = [label.text stringByReplacingCharactersInRange:startingWhiteSpaceRange
                                                                 withString:@""];
                [label sizeToFit];
            }
        }
        
        // Set the location for this label:
        label.frame = CGRectMake(wordLocation.x,
                                 wordLocation.y,
                                 label.frame.size.width,
                                 label.frame.size.height);
        
        // Show this label:
        [self.confirmView addSubview:label];
        
        // Update the horizontal position for the next word:
        wordLocation.x += label.frame.size.width;
    }
    
    if (self.confirmView.subviews.count > 0) {
        UILabel* label = (UILabel*)self.confirmView.subviews.lastObject;
        for (NSLayoutConstraint* c in self.confirmView.constraints) {
            if ([c.firstItem isEqual:self.confirmView] && c.firstAttribute == NSLayoutAttributeHeight) {
                c.constant = CGRectGetMaxY(label.frame);
                NSMutableArray* temp = [NSMutableArray new];
                if (termOfUseLbl) [temp addObject:termOfUseLbl];
                if (policyLbl) [temp addObject:policyLbl];
                [temp enumerateObjectsUsingBlock:^(UITextField*  _Nonnull label, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSArray * objects = [[NSArray alloc] initWithObjects:[AppColor activeButtonColor], [NSNumber numberWithInt:NSUnderlineStyleSingle], nil];
                    NSArray * keys = [[NSArray alloc] initWithObjects:NSForegroundColorAttributeName, NSUnderlineStyleAttributeName, nil];
                    
                    NSDictionary * linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
                    
                    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:label.text attributes:linkAttributes];
                    [label setAttributedText:attributedString];
                    
                }];
            }
        }
    }
}

- (void)tapOnTermsOfServiceLink:(UITapGestureRecognizer *)tapGesture
{
    if([self isDisableSubmitTapped:tapGesture])
    {
        return;
    }
    
    [self dismissKeyboard];
    
    if (tapGesture.state == UIGestureRecognizerStateEnded)
    {
        TermsOfUseViewController *vc = [[TermsOfUseViewController alloc] init];
        vc.delegate = self;
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        [self.navigationController pushViewController:vc animated:NO];
    }
}


- (void)tapOnPrivacyPolicyLink:(UITapGestureRecognizer *)tapGesture
{
    if([self isDisableSubmitTapped:tapGesture])
    {
        return;
    }
    
    [self dismissKeyboard];
    
    if (tapGesture.state == UIGestureRecognizerStateEnded)
    {
        PolicyViewController *vc = [[PolicyViewController alloc] init];
        vc.delegate = self;
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        [self.navigationController pushViewController:vc animated:NO];
    }
}


#pragma mark HANDLE KEYBOARD
-(void)handleKeyboardWillShow:(NSNotification *)paramNotification
{
    //    backupBottomConstraint = self.submitButtonBottomConstraint.constant;
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         //                         if ([ [ UIScreen mainScreen ] bounds ].size.height <= 568) {
                         //                             self.scrollView.contentInset = UIEdgeInsetsMake(-50, 0, 0, 0);
                         //                             self.scrollBottom.constant = (100 + 20)*SCREEN_SCALE;
                         //                         }else{
                         self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
                         self.scrollBottom.constant = 120*SCREEN_SCALE;
                         //                         }
                         
                         self.submitButtonBottomConstraint.constant = keyboardRect.size.height + MARGIN_KEYBOARD;
                         
                         //                         if (greetingsTooltip.isShowing == YES) {
                         //                             [self showTooltip];
                         //                         }
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

-(void)handleKeyboardWill:(NSNotification *)paramNotification
{
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
                         self.scrollBottom.constant = 0.0f;
                         self.submitButtonBottomConstraint.constant =  backupBottomConstraint;
                         //                         if (greetingsTooltip.isShowing == YES) {
                         //                             [self showTooltip];
                         //                         }
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
    
}


# pragma mark DATA LOAD DELEGATE
-(void)loadDataDoneFrom:(WSBase *)ws
{
    if ([ws isKindOfClass:[WSB2CGetPolicyInfo class]]) {
        if(ws.data.count > 0)
        {
            [[SessionData shareSessiondata] setCurrentPolicyVersion:((PolicyInfoItem *)[ws.data objectAtIndex:0]).CurrentVersion];
            isLoadedPolicy = true;
        }
    }
}

#pragma mark TEXT FIELD DELEGATE
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([textField.text length] <= 6) {
    }
}

- (void)textFieldDidChange{
    if ([self.binNumberText.text length] > 4 && ![self.binNumberText.text containsString:@"-"]) {
        NSMutableString *newString = [[NSMutableString alloc] initWithString:self.binNumberText.text];
        [newString insertString:@"-" atIndex:4];
        self.binNumberText.text = newString;
    }
    if([self.binNumberText.text isValidBinNumber] && self.checkBoxButton.selected)
    {
        self.submitButton.enabled = YES;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(string.length > 1)
    {
        return NO;
    }
    else
    {
        if(textField.text.length == 7 && ![string isEqualToString:@""])
        {
            return NO;
        }
        
        if(range.location < textField.text.length)
        {
            textField.text = [self reformatForBINTextWithNewString:string WithRange:range];
            NSRange newRange;
            if([string isEqualToString:@""])
            {
                newRange = (textField.text.length < range.location) ? NSMakeRange(textField.text.length, 0) : NSMakeRange(range.location, 0);
            }
            else
            {
                newRange = (range.location == 4) ? NSMakeRange(range.location + 2, 0) : NSMakeRange(range.location + 1, 0);
            }
            [textField updateCursorPositionAtRange:newRange];
            self.submitButton.enabled = [textField.text isValidBinNumber] ? YES: NO;
            return NO;
        }
        
        BOOL isPressedBackspaceAfterSingleSpaceSymbol = [string isEqualToString:@""] && range.location == 4 && range.length == 1;
        if (isPressedBackspaceAfterSingleSpaceSymbol) {
            //  your actions for deleteBackward actions
            textField.text = [textField.text stringByReplacingCharactersInRange:range withString:@""];
            range.location = range.location - 1;
        }
        //limit check lenght of string
        NSString *resultText = [textField.text stringByReplacingCharactersInRange:range
                                                                       withString:string];
        if (resultText.length > 0) {
            [self hideAsteriskLabel];
            //            [greetingsTooltip dismiss];
        }
        self.submitButton.enabled = [textField.text isValidBinNumber] ? YES: NO;
        return resultText.length <= 7;
    }
}

-(NSString *)reformatForBINTextWithNewString:(NSString *)newString WithRange:(NSRange)range
{
    NSMutableString *mutableString = [[NSMutableString alloc] initWithString:self.binNumberText.text];
    NSRange newRange = range;
    if([newString isEqualToString:@""] && range.location == 4)
    {
        newRange = NSMakeRange(range.location - 1, 1);
    }
    [mutableString replaceCharactersInRange:newRange withString:newString];
    mutableString = [[NSMutableString alloc] initWithString:[mutableString stringByReplacingOccurrencesOfString:@"-" withString:@""]];
    if(mutableString.length > 4)
    {
        [mutableString insertString:@"-" atIndex:4];
    }
    
    return mutableString;
}

-(void)enableTermOfUseAndPrivacyPolicy
{
    if([self.binNumberText.text isValidBinNumber])
    {
        termOfUseLbl.textColor = [AppColor activeButtonColor];
        policyLbl.textColor = [AppColor activeButtonColor];
    }
    else
    {
        termOfUseLbl.textColor = colorFromHexString(@"#141413");
        policyLbl.textColor = colorFromHexString(@"#141413");
    }
}
@end
