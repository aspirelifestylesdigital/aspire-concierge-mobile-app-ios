//
//  HomeViewController.m
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/13/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "HomeViewController.h"
#import "SWRevealViewController.h"
#import "UIView+Extension.h"
#import "NSString+Utis.h"
#import "UIButton+Extension.h"
#import "UILabel+Extension.h"
#import "Constant.h"
#import "GalleryViewController.h"
#import "AskConciergeViewController.h"
#import "ExploreViewController.h"
#import "Common.h"
#import "AppData.h"
#import "CityViewController.h"

#import "AskConciergeConfirmationViewController.h"


@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (_gotoAskConcierge == YES) {
        AskConciergeViewController *vc = [[AskConciergeViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
    [self setTextStyleForView];
    [self createMenuBarButton];
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"home_tile", nil)];
    [self trackingScreenByName:@"Home"];
}

-(void)setTextStyleForView
{
    [self.btnGallery setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnGallery setTitle:NSLocalizedString(@"inspiration_gallery_button_title", nil) forState:UIControlStateNormal];
    NSString *message = NSLocalizedString(@"inspiration_gallery_button_title", nil);
    NSRange range = NSMakeRange(0, message.length);
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:message];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize headerTextSize]] range:range];
    [attributeString addAttribute:NSKernAttributeName value:@([AppSize smallLetterSpacing]) range:range];
    self.btnGallery.titleLabel.attributedText = attributeString;
    
    [self.btnGallery setWhiteBackgroundColorForTouchingStatus];
    self.btnGallery.layer.borderWidth = 1;
    self.btnGallery.layer.borderColor = [[UIColor whiteColor] CGColor];
}


-(void) initView{
    [self.footerView setBackgroundColorForView];
    
    self.lblWelcome.attributedText = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"welcome", nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleGreetingSize]], NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.lblWelcome.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleGreetingSize]];
    
    self.lblWelcomeMessage.attributedText = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"welcome_message", nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionGreetingSize]], NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    self.lblWelcomeMessage.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionGreetingSize]];
    
    self.lblAskConciergeMessage.attributedText = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"ask_concierge_explain_message", nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionTextSize]], NSForegroundColorAttributeName:[AppColor descriptionTextColor]}];
    
    self.lblExploreMessage.attributedText = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"explore_explain_message", nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionTextSize]], NSForegroundColorAttributeName:[AppColor descriptionTextColor]}];
    
    self.lblExploreMessage.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionTextSize]];
    self.lblAskConciergeMessage.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionTextSize]];
    // 
    NSDictionary *attributes = @{NSForegroundColorAttributeName:
                                     [AppColor textColor],
                                 
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_BOLD size:([AppSize descriptionTextSize])],
                                 
                                 NSKernAttributeName: @2.2};
    
    NSDictionary *attributesSelected = @{NSForegroundColorAttributeName:
                                             [AppColor activeButtonColor],
                                 
                                         NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_BOLD size:([AppSize descriptionTextSize])],
                                 
                                         NSKernAttributeName: @2.2};
//    [self.lblTitleExplore setAttributedText:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"explore_button_title", nil) attributes:attributes]];
//    [self.lblTitleAskConcierge setAttributedText:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"ask_concierge_button_tile", nil) attributes:attributes]];
    [self.btnExplore setAttributedTitle:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"explore_button_title", nil) attributes:attributes]  forState:UIControlStateNormal];
    [self.btnExplore setAttributedTitle:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"explore_button_title", nil) attributes:attributesSelected]  forState:UIControlStateHighlighted];
    
    
    [self.btnAskConcierge setAttributedTitle:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"ask_concierge_button_tile", nil) attributes:attributes]  forState:UIControlStateNormal];
    [self.btnAskConcierge setAttributedTitle:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"ask_concierge_button_tile", nil) attributes:attributesSelected]  forState:UIControlStateHighlighted];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.topConstraintFooterView.constant = self.topConstraintFooterView.constant*SCREEN_SCALE;
        self.topConstraintGalleryView.constant = self.topConstraintGalleryView.constant*SCREEN_SCALE;
        self.topConstraintWelcomeView.constant = self.topConstraintWelcomeView.constant*SCREEN_SCALE;
        self.topConstraintHeightAskButton.constant = self.topConstraintHeightAskButton.constant*SCREEN_SCALE;
        self.topExploreLabelConstraint.constant = self.topExploreLabelConstraint.constant*SCREEN_SCALE;
        self.topAskConciergeLabelConstraint.constant = self.topAskConciergeLabelConstraint.constant*SCREEN_SCALE;
    });
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [self setNavigationBarColor];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)touchAskConcierge:(id)sender {
    self.btnAskConcierge.highlighted = NO;
    AskConciergeViewController *vc = [[AskConciergeViewController alloc] init];
    
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)touchViewGallery:(id)sender {
    GalleryViewController *vc = [[GalleryViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)touchExplore:(id)sender {
    UIViewController *newFrontController = nil;
    CityItem *selectedCity = [[AppData getInstance] getSelectedCity];
    if(selectedCity)
    {
        ExploreViewController *exploreViewController = [[ExploreViewController alloc] init];
        exploreViewController.currentCity = selectedCity;
        newFrontController = exploreViewController;
        
    }
    else
    {
        newFrontController = [[CityViewController alloc] init];
    }
    [self.navigationController pushViewController:newFrontController animated:YES];
}
@end
