//
//  SignInViewController.m
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/13/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "CreateProfileViewController.h"
#import "MenuViewController.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "WSCreateUser.h"
#import "WSB2CGetAccessToken.h"
#import "WSB2CGetUserDetails.h"
#import "WSB2CGetRequestToken.h"
#import "NSString+Utis.h"
#import "UITextField+Extensions.h"
#import "UIButton+Extension.h"
#import "UIView+Extension.h"
#import "UILabel+Extension.h"
#import "HomeViewController.h"
#import "Constant.h"
#import "MyProfileViewController.h"
#import "AppData.h"
#import "AppDelegate.h"
#import "UserObject.h"
#import "UtilStyle.h"
#import "UserRegistrationItem.h"

#import "CreateSecureView.h"
@import AspireApiControllers;
//#import <AspireApiFramework/AspireApiFramework.h>
@import AspireApiFramework;


@interface CreateProfileViewController ()<DataLoadDelegate, UITextFieldDelegate,UIScrollViewDelegate>
{
    float heightInset, currentOffsetY;
    BOOL shouldCheckContentOffset;
}

@property (nonatomic, strong) WSCreateUser *wsCreateUser;

@end

@implementation CreateProfileViewController
@synthesize partyId, hasFoucusedFiled;

- (void)viewDidLoad {
    isNotAskConciergeBarButton = YES;
    [super viewDidLoad];
    self.navigationController.navigationBar.translucent = YES;
    tappedOutsideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tappedOutsideKeyboard.cancelsTouchesInView = false;
    [self.scrollView addGestureRecognizer:tappedOutsideKeyboard];
    
    self.signInButton = [UIButton buttonWithType:UIButtonTypeCustom];
    signBtnWidth = [[UIScreen mainScreen] bounds].size.width - 40;//355.0f * SCREEN_SCALE;
    signBtnHeight = 50.0f * SCREEN_SCALE;
    signFrame = CGRectMake((SCREEN_WIDTH - signBtnWidth)/2.0f, SCREEN_HEIGHT - signBtnHeight - 30.0f, signBtnWidth, signBtnHeight);
    self.signInButton.frame = signFrame;
    
    // Text Style For Submit Button
    [self.signInButton setTitle:NSLocalizedString(@"next_button_title", nil) forState:UIControlStateNormal];
    [self.signInButton setBackgroundColorForNormalStatus];
    [self.signInButton setBackgroundColorForTouchingStatus];
    [self.signInButton configureDecorationForButton];
    self.signinBtn.accessibilityIdentifier = @"btnSubmit";
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:([AppSize titleTextSize])],
                                 NSKernAttributeName: @([AppSize descriptionLetterSpacing])};
    self.signInButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.signInButton.titleLabel.text attributes:attributes];
    
    [self.signInButton addTarget:self action:@selector(signinAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.signInButton];
    
    UITapGestureRecognizer *tab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkBox:)];
    self.commitmentLabel.userInteractionEnabled = YES;
    [self.commitmentLabel addGestureRecognizer:tab];
    if (_isUpdateProfile == NO || !_isUpdateProfile) {
        [self trackingScreenByName:@"Sign up"];
    }else{
        [self trackingScreenByName:@"My profile"];
    }
    
    [self setTextFieldStyte];
    self.titleLable.attributedText = [UtilStyle setTextStyleForTitleViewControllerWithMessage:@"REGISTER"];
    
    [self setEmail:_emailChecked];
    [self setPhone:_phoneChecked];
    [self setZipCode:_zipCodeChecked];
    [self setLastName:_lastNameChecked];
    [self setFirstName:_firstNameChecked];
    
    self.scrollView.delegate = self;
}

- (void) setTextFieldStyte {
    self.phoneNumberText.keyboardType = UIKeyboardTypePhonePad;
    self.phoneNumberText.tag = PHONE_NUMBER_TEXTFIELD_TAG;
    self.zipCodeText.keyboardType = UIKeyboardTypePhonePad;
    self.zipCodeText.tag = ZIP_CODE_TEXTFIELD_TAG;
    self.emailText.keyboardType = UIKeyboardTypeEmailAddress;
    self.firstNameText.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    self.lastNameText.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    
    self.firstNameText.delegate = self;
    self.lastNameText.delegate = self;
    self.emailText.delegate = self;
    self.phoneNumberText.delegate = self;
    self.zipCodeText.delegate = self;
    
    [self.firstNameText showAsterisk:5];
    [self.lastNameText showAsterisk:5];
    [self.emailText showAsterisk:5];
    [self.phoneNumberText showAsterisk:5];
    [self.zipCodeText showAsterisk:5];
    
    self.firstNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.firstNameText.text];
    self.lastNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.lastNameText.text];
    self.emailText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.emailText.text];
    self.phoneNumberText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.phoneNumberText.text];
    self.zipCodeText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.zipCodeText.text];
    
    
    self.firstNameText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.firstNameText.placeholder];
    self.lastNameText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.lastNameText.placeholder];;
    self.emailText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.emailText.placeholder];;
    self.phoneNumberText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.phoneNumberText.placeholder];;
    self.zipCodeText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.zipCodeText.placeholder];;
    
    self.firstNameText.font =  [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.lastNameText.font =  [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.emailText.font =  [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.phoneNumberText.font =  [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.zipCodeText.font =  [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.lblBanner.font =  [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionTextSize]];
    self.commitmentLabel.font = [UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize titleTextSize]];
    
    [self.myView setBackgroundColorForView];
    [self.scrollView setBackgroundColorForView];
    [self.signInButton setBackgroundColorForNormalStatus];
    [self.signInButton setBackgroundColorForTouchingStatus];
    [self.signInButton configureDecorationForButton];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:([AppSize titleTextSize])],
                                 NSKernAttributeName: @([AppSize descriptionLetterSpacing])};
    self.signInButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.signInButton.titleLabel.text attributes:attributes];
}

- (void)initView{
    
    // Text Style For Title
    NSString *message = NSLocalizedString(@"create_profile_title_message", nil).uppercaseString;
    self.titleLable.attributedText = [UtilStyle setTextStyleForTitleViewControllerWithMessage:message];
    
    // Text Style For Greet Message
    message = NSLocalizedString(@"commitment_profile_message", nil);
    self.commitmentLabel.attributedText = [UtilStyle setLargeSizeStyleForLabelWithMessage:message];
    [self setCheckBoxState:isCheck];
}

- (void) getUserInfo{
    if ([[SessionData shareSessiondata] UserObject]) {
        
        userInfo = [[SessionData shareSessiondata] UserObject];
        [self setEmail:userInfo.email];
        [self setLastName:userInfo.lastName];
        [self setFirstName:userInfo.firstName];
        [self setZipCode:userInfo.zipCode];
        [self setPhone:userInfo.mobileNumber];
        isCheck = userInfo.optStatus;
        [self setCheckBoxState:isCheck];
#if DEBUG
        NSLog(@"%s => %@",__PRETTY_FUNCTION__,[_phoneNumberText originText]);
#endif
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}


-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self setTextViewsDefaultBottomBolder];
    [self.view layoutIfNeeded];
    
    [self getUserInfo];
    
}

-(void)setTextViewsDefaultBottomBolder
{
    [self.firstNameText setBottomBolderDefaultColor];
    [self.lastNameText setBottomBolderDefaultColor];
    [self.emailText setBottomBolderDefaultColor];
    [self.phoneNumberText setBottomBolderDefaultColor];
    [self.zipCodeText setBottomBolderDefaultColor];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    tappedOutsideKeyboard.cancelsTouchesInView = NO;
    [self.view removeGestureRecognizer:tappedOutsideKeyboard];
}

- (void) setCheckBoxState:(BOOL)check
{
    if (!check) {
        check = NO;
    }
    (check == YES) ? [self.checkBoxBtn setImage:[UIImage imageNamed:@"checkbox_check"] forState:UIControlStateNormal] : [self.checkBoxBtn setImage:[UIImage imageNamed:@"checkbox_uncheck"] forState:UIControlStateNormal];
    [self updateProfileButtonStatus];
    self.checkBoxBtn.accessibilityIdentifier = check ? @"btnCheck" : @"btnUnCheck";
}
- (IBAction)checkBox:(id)sender {
    isCheck = !isCheck;
    [self setCheckBoxState:isCheck];
}

- (void)updateProfileButtonStatus{
//    [self.checkBoxBtn setImageEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
}

- (void) setEmail:(NSString *)email {
    if (email != nil) {
        _emailText.text = email;
        [_emailText setOriginText:email];
        [_emailText showTextMask:true];
    }
}

- (void) setPhone:(NSString *)phone {
    if (phone != nil) {
        NSMutableString* newString = [NSMutableString stringWithString:@""];
        if(![phone isValidPhoneNumber]) {
            NSString* temp = phone;
            NSMutableArray *characters = [NSMutableArray array];
            for (int i=0; i<temp.length; i++) {
                [characters addObject:[temp substringWithRange:NSMakeRange(i, 1)]];
            }
            [newString insertString:@"1-" atIndex:0];
            
            NSInteger start = characters.count > 10 ? characters.count - 10 : 0;
            for (NSInteger i = start; i < characters.count; i++) {
                [newString appendString:characters[i]];
                if (i - start == 2 || i - start == 5)
                    [newString appendString:@"-"];
            }
        } else {
            newString = [NSMutableString stringWithString:phone];
        }
//        [self updateTextFiel:_phoneNumberText shouldChangeCharactersInRange:NSMakeRange(0, 0) replacementString:newString];
        _phoneNumberText.text = newString;
        [_phoneNumberText setOriginText:newString];
        [_phoneNumberText showTextMaskPhone];
    }
}

- (void) setZipCode:(NSString*) zipcode {
    if (zipcode != nil) {
        NSMutableString* newString = [NSMutableString stringWithString:zipcode];
        if([newString isValidZipCode] || newString.length <= 5)
        {
            _zipCodeText.text = newString;
            [_zipCodeText setOriginText:newString];
        }
        else if(newString.length > 5)
        {
            if(![[newString substringWithRange:NSMakeRange(5, 1)] isEqualToString:@"-"])
            {
                [newString insertString:@"-" atIndex:5];
            }
            
            _zipCodeText.text = (newString.length > 10) ? [newString substringWithRange:NSMakeRange(0, 10)] : newString;
            [_zipCodeText setOriginText:_zipCodeText.text];
        }
    }
}

- (void) setFirstName:(NSString*) firstName {
    if (firstName != nil) {
        _firstNameText.text = firstName;
        [_firstNameText setOriginText:firstName];
        [_firstNameText showTextMask:false];
    }
}

- (void) setLastName:(NSString*) lastName {
    if (lastName != nil) {
        _lastNameText.text = lastName;
        [_lastNameText setOriginText:lastName];
        [_lastNameText showTextMask:false];
    }
}

-(void) enableSignInButton:(UIButton *)button
{
    [button setHighlighted:NO];
    self.checkBoxBtn.selected = !(button.selected);
    [button setBackgroundColor:[UIColor clearColor]];
    [self.signinBtn setEnabled:button.selected];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)signinAction
{
    [self verifyAccountData];
}


- (void)updateProfileWithAPI {
    
    NSMutableDictionary *userDict = [[NSMutableDictionary alloc] init];
    [userDict setValue:B2C_ConsumerKey forKey:@"referenceName"];
    [userDict setValue:[_firstNameText originText] forKey:@"FirstName"];
    [userDict setValue:[_lastNameText originText] forKey:@"LastName"];
    [userDict setValue:[[[_phoneNumberText originText] substringFromIndex:1] stringByReplacingOccurrencesOfString:@"-" withString:@""] forKey:@"MobileNumber"];
    [userDict setValue:[_emailText originText] forKey:@"Email"];
    [userDict setValue:[[_zipCodeText originText] stringByReplacingOccurrencesOfString:@"-" withString:@""] forKey:@"ZipCode"];
    [userDict setValue:@"Unknown" forKey:@"City"];
    [userDict setValue:@"USA" forKey:@"Country"];
    if (partyId) {
        [userDict setValue:partyId forKey:@"partyId"];
    }
  
    NSString* policyVersion = [[SessionData shareSessiondata] CurrentPolicyVersion];
//    if ([AACStringUtils isEmptyWithString:policyVersion] == false) {
        [userDict setValue:policyVersion forKey:APP_USER_PREFERENCE_Policy_Version];
//    }
    [userDict setValue:[[SessionData shareSessiondata] BINNumber] forKey:APP_USER_PREFERENCE_Passcode];
    [userDict setObject:isCheck ? @"ON" : @"OFF" forKey:APP_USER_PREFERENCE_Newsletter];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [userDict setObject:[dateFormatter stringFromDate:[NSDate date]] forKey:APP_USER_PREFERENCE_Newsletter_Date];
    
    if ([User isValid]) {
        UserObject* temp = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([UserObject class])];
        if (temp.selectedCity) {
            if (temp.selectedCity.length > 0)
                [userDict setObject:temp.selectedCity forKey:APP_USER_PREFERENCE_Selected_City];
        }
    }
    
    //    self.wsCreateUser = [[WSCreateUser alloc] init];
    //    self.wsCreateUser.delegate = self;
    typeof(self) __weak _self = self;
    if (!_isUpdateProfile) {
        
        CreateSecureView* v = [[CreateSecureView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        AACForgotPasswordController* vc = [AACForgotPasswordController new];
        vc.subview = v;
        v.controller = vc;
        [v updateUserInfor:userDict];
        [self.navigationController pushViewController:vc animated:true];
        _lblBanner.hidden = true;
        
    }else{
        [self startActivityIndicator];
        [ModelAspireApiManager updateProfileCurrentUserWithUserInfo:userDict completion:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [_self stopActivityIndicator];
                if (!error) {
                    [[SessionData shareSessiondata] setUserObject:(UserObject *)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([UserObject class])]];
                    [_self updateSuccessRedirect];
                    return;
                }
                
                if ([AspireApiError getErrorTypeAPIRetrieveProfileFromError:error] == ACCESSTOKEN_INVALID_ERR) {
                    [_self showAlertInvalidUserAndShouldLoginAgain:true];
                    return;
                }
                
                NSString* message = @"";
                switch ([AspireApiError getErrorTypeAPIUpdateProfileFromError:error]) {
                    case NETWORK_UNAVAILABLE_ERR:{
                        [_self showNoNetWorkAlert];
                    }
                        break;
                    case UNKNOWN_ERR:
                        message = NSLocalizedString(@"error_api_message", nil);
                        break;
                        
                    default:
                        message = NSLocalizedString(@"error_api_message", nil);
                        break;
                }
                if (message.length == 0) return;
                AlertViewController *alert = [[AlertViewController alloc] init];
                alert.titleAlert = NSLocalizedString(@"cannot_get_data", nil);
                alert.msgAlert = message;
                alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
                alert.seconBtn.hidden = YES;
                alert.midView.hidden = YES;
                alert.lblAlertMessage.textAlignment = NSTextAlignmentCenter;
                [alert.view layoutIfNeeded];
                [_self showAlert:alert forNavigation:NO];
            });
        }];
        //        [self.wsCreateUser updateUser:userDict];
    }
    
}

-(void)loadDataDoneFrom:(WSBase*)ws
{
    [self stopActivityIndicator];
    
    if (ws.data) {
        [self setUserDefaultInfoWithDict:ws.data[0]];
    }
    
    [self performSelector:@selector(setUserObjectWithDict:) withObject:((WSCreateUser*)ws).user afterDelay:30];
    
    if (((WSCreateUser*)ws).user) {
        [self setUserObjectWithDict:((WSCreateUser*)ws).user];
    }
    
    [self updateSuccessRedirect];
    [AppData setCreatedProfileSuccessful];
}


- (void)setUserObjectWithDict:(NSDictionary*)dict{
    [[SessionData shareSessiondata] setUserObjectWithDict:dict];
}

- (void)setUserDefaultInfoWithDict:(UserRegistrationItem*)item{
    [[SessionData shareSessiondata] setOnlineMemberID:item.OnlineMemberID];
    [[SessionData shareSessiondata] setOnlineMemberDetailIDs:item.OnlineMemberDetailIDs];
}

#pragma mark KEYBOARD PROCESS

-(void)handleKeyboardWillShow:(NSNotification *)paramNotification
{
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         keyboardHeight = keyboardRect.size.height;
                         self.signInButton.frame = CGRectMake(self.signInButton.frame.origin.x, SCREEN_HEIGHT - (keyboardRect.size.height + MARGIN_KEYBOARD) - signBtnHeight, signBtnWidth, signBtnHeight);
                         [self updateEdgeInsetForShowKeyboard];
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

-(void)handleKeyboardWillHide:(NSNotification *)paramNotification
{
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         self.signInButton.frame = signFrame;
                         [self updateEdgeInsetForHideKeyboard];
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
    
}


-(void)updateEdgeInsetForShowKeyboard
{
    heightInset = 0.0;
    
    if(self.emailText.isFirstResponder)
    {
        heightInset = 60.0f*SCREEN_SCALE;
        
    }else if (self.phoneNumberText.isFirstResponder || self.zipCodeText.isFirstResponder){
        heightInset = 100.0f*SCREEN_SCALE;
    }else if (self.answerTextField.isFirstResponder){
        heightInset = 180.0f*SCREEN_SCALE;
    }
    
    self.scrollView.contentInset = UIEdgeInsetsMake(- heightInset,  0.0f, 0.0f, 0.0f);
    self.viewActionBottomConstraint.constant = (keyboardHeight + MARGIN_KEYBOARD);
    self.scrollBottom.constant = 150.0f + heightInset;
    
    currentOffsetY = self.scrollView.contentOffset.y;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    shouldCheckContentOffset = true;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if ((self.zipCodeText || self.answerTextField) && shouldCheckContentOffset) {
        if (scrollView.contentOffset.y < currentOffsetY && scrollView.contentInset.top != 0) {
            self.scrollView.contentInset = UIEdgeInsetsMake(0.0f,  0.0f, 0.0f, 0.0f);
        } else if (scrollView.contentOffset.y > currentOffsetY && scrollView.contentInset.top != - heightInset){
            self.scrollView.contentInset = UIEdgeInsetsMake(- heightInset,  0.0f, 0.0f, 0.0f);
        }
    }
    shouldCheckContentOffset = false;
}

-(void)updateEdgeInsetForHideKeyboard
{
    heightInset = 0.0;
    
    self.scrollView.contentInset = UIEdgeInsetsMake(0.0f,  0.0f, 0.0f, 0.0f);
    //    [self resignFirstResponderForAllTextField];
    self.scrollBottom.constant = 0.0f;
    self.viewActionBottomConstraint.constant = backupBottomConstraint;
}

-(void)resignFirstResponderForAllTextField
{
    if(self.firstNameText.isFirstResponder)
    {
        [self.firstNameText resignFirstResponder];
    }
    else if(self.lastNameText.isFirstResponder)
    {
        [self.lastNameText resignFirstResponder];
    }
    else if(self.emailText.isFirstResponder)
    {
        [self.emailText resignFirstResponder];
    }
    else if(self.phoneNumberText.isFirstResponder)
    {
        [self.phoneNumberText resignFirstResponder];
    }
    else if(self.zipCodeText.isFirstResponder)
    {
        [self.zipCodeText resignFirstResponder];
    }
}

-(void)dismissKeyboard
{
    if (_isUpdateProfile) {
        [self.emailText showTextMask:true];
    }
    [self resignFirstResponderForAllTextField];
    [self updateEdgeInsetForHideKeyboard];
}

- (BOOL)verifyValueForTextField:(UITextField *)textFied andMessageError:(NSMutableString *)message
{
    NSString *currentText = nil;
    NSString *errorMsg = nil;
    BOOL isValid = NO;
    currentText = [textFied originText];
    if(textFied == self.firstNameText)
    {
        errorMsg = NSLocalizedString(@"input_invalid_first_name_msg", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidName];
    }
    else if(textFied == self.lastNameText)
    {
        errorMsg = NSLocalizedString(@"input_invalid_last_name_msg", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidName];
    }
    else if(textFied == self.emailText)
    {
        errorMsg = NSLocalizedString(@"input_invalid_email_msg", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidEmail];
    }
    else if(textFied == self.phoneNumberText)
    {
        errorMsg = NSLocalizedString(@"input_invalid_phone_number_msg", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidPhoneNumber];
    }
    else if(textFied == self.zipCodeText)
    {
        errorMsg = NSLocalizedString(@"input_invalid_zip_code_msg", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidZipCode];
    }
    
    
    if((textFied.isFirstResponder ? textFied.text :currentText).length > 0)
    {
        if(!isValid)
        {
            (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
            [message appendString:errorMsg];
            [textFied setBottomBorderRedColor];
            if(!hasFoucusedFiled)
            {
                [textFied becomeFirstResponder];
                hasFoucusedFiled = YES;
            }
            
        }
        else
        {
            [textFied setBottomBolderDefaultColor];
        }
    }
    else
    {
        [textFied setBottomBorderRedColor];
        if(!hasFoucusedFiled)
        {
            [textFied becomeFirstResponder];
            hasFoucusedFiled = YES;
        }
    }
    
    return isValid;
}
#pragma mark LOCAL PROCESS
-(void)verifyAccountData
{
    [self resignFirstResponderForAllTextField];
    hasFoucusedFiled = NO;
    NSMutableString *message = [[NSMutableString alloc] init];
    if(self.firstNameText.text.length == 0 || self.lastNameText.text.length == 0 || self.emailText.text.length == 0 || self.phoneNumberText.text.length == 0 || self.zipCodeText.text.length == 0 )
    {
        [message appendString:@"* "];
        [message appendString:NSLocalizedString(@"all_field_required", nil)];
        
        [self verifyValueForTextField:self.firstNameText andMessageError:message];
        [self verifyValueForTextField:self.lastNameText andMessageError:message];
        [self verifyValueForTextField:self.emailText andMessageError:message];
        [self verifyValueForTextField:self.phoneNumberText andMessageError:message];
        [self verifyValueForTextField:self.zipCodeText andMessageError:message];
        
        [self createProfileInforWithMessage:message isCreatedSuccessful:NO];
    }
    else{
        [self verifyValueForTextField:self.firstNameText andMessageError:message];
        [self verifyValueForTextField:self.lastNameText andMessageError:message];
        [self verifyValueForTextField:self.emailText andMessageError:message];
        [self verifyValueForTextField:self.phoneNumberText andMessageError:message];
        [self verifyValueForTextField:self.zipCodeText andMessageError:message];
        
        [self createProfileInforWithMessage:message isCreatedSuccessful:(message.length == 0)];
    }
}

-(void)createProfileInforWithMessage:(NSString *)message isCreatedSuccessful:(BOOL)isSuccessful
{
    if(isSuccessful)
    {
        [self updateEdgeInsetForHideKeyboard];
        [self setTextViewsDefaultBottomBolder];
        
        //      save profile is create
        [self updateProfileWithAPI];
    }
    else
    {
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"confirm_value_correct", nil);
        alert.msgAlert = message;
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.hidden = YES;
            alert.lblAlertMessage.textAlignment = NSTextAlignmentLeft;
            [alert.view layoutIfNeeded];
        });
        
        //alert.blockFirstBtnAction = ^(void){
        //[self makeBecomeFirstResponderForTextField];
        //};
        
        [self showAlert:alert forNavigation:NO];
        
    }
}

- (void) updateSuccessRedirect{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = nil;
    alert.msgAlert = NSLocalizedString(@"create_user_successful_msg", nil);
    alert.firstBtnTitle = NSLocalizedString(@"back_to_profile_button_title", nil);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        alert.firstBtn.titleLabel.font = [UIFont fontWithName:FONT_MarkForMC_BOLD size:[AppSize headerTextSize]];
        alert.seconBtn.titleLabel.font = [UIFont fontWithName:FONT_MarkForMC_BOLD size:[AppSize headerTextSize]];
    });
    
    alert.firstBtnTitle = NSLocalizedString(@"alert_cancel_button", nil);
    alert.blockFirstBtnAction = ^(void){
        SWRevealViewController *revealController = self.revealViewController;
        UIViewController *newFrontController  = [[MyProfileViewController alloc] init];
        UINavigationController *newNavigationViewController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
        [revealController pushFrontViewController:newNavigationViewController animated:YES];
    };
    
    alert.secondBtnTitle = NSLocalizedString(@"home_button_title", nil);
    alert.blockSecondBtnAction  = ^(void){
        SWRevealViewController *revealController = self.revealViewController;
        UIViewController *newFrontController = [[HomeViewController alloc] init];
        UINavigationController *newNavigationViewController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
        [revealController pushFrontViewController:newNavigationViewController animated:YES];
    };
    
    [self trackingEventByName:@"Sign up" withAction:ClickActionType withCategory:AuthenticationCategoryType];
    [self showAlert:alert forNavigation:NO];
    
}
-(void) makeBecomeFirstResponderForTextField
{
    [self resignFirstResponderForAllTextField];
    if(![[self.firstNameText originText] isValidName])
    {
        [self.firstNameText becomeFirstResponder];
    }
    else if(![[self.lastNameText originText] isValidName])
    {
        [self.lastNameText becomeFirstResponder];
    }
    else if(![[self.emailText originText] isValidEmail])
    {
        [self.emailText becomeFirstResponder];
    }
    else if(![[self.phoneNumberText originText] isValidPhoneNumber])
    {
        [self.phoneNumberText becomeFirstResponder];
    }
    else if(![[self.zipCodeText originText] isValidZipCode])
    {
        [self.zipCodeText becomeFirstResponder];
    }
}

- (void) handleAsterickIcon{
    
}
- (IBAction)bakcEvent:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}
#pragma mark TEXT FIELD DELEGATE

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.text = [textField.text removeRedudantWhiteSpaceInText];
    [self onTextFieldChanged:textField];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField == self.emailText) {
        [self dismissKeyboard];
        self.emailText.text = [self.emailText originText];
        return NO;
    }
    else{
        [self.emailText showTextMask:true];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField setOriginText:textField.text];
    
    if(textField == self.firstNameText)
    {
        [textField showTextMask:false];
        
    }
    else if(textField == self.lastNameText)
    {
        [textField showTextMask:false];
        
    }
    else if(textField == self.phoneNumberText)
    {
        if(textField.text.length == 2 && [[textField.text substringToIndex:2] isEqualToString:@"1-"]) {
            textField.text = nil;
        }
        [textField showTextMaskPhone];
//        if([textField.text length] > 3 && [textField.text isValidPhoneNumber])
//        {
//            textField.text = [NSString stringWithFormat:@"*-%@", [[textField.text substringFromIndex:2] createMaskStringBeforeNumberCharacter:3]];
//        }
    }
    else if(textField == self.emailText)
    {
        if([textField.text containsString:@"@"])
        {
            [textField showTextMask:true];
        }
    }
    else if(textField == self.answerTextField)
    {
        self.answerTextField.text = MASKED_ANSWER;
        if (self.inputNewAnswer.length > 0) {
            NSMutableString* temp = [NSMutableString stringWithString:@""];
            for (int i=0; i < self.inputNewAnswer.length; i++) {
                [temp appendString:@"*"];
            }
            self.answerTextField.text = temp;
        }
    }
    
    if (textField == self.emailText /*&& _isUpdateProfile*/) {
        self.emailText.text = [self.emailText originText];
        [self dismissKeyboard];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    textField.text = [textField originText];
    if(textField == self.firstNameText)
    {
//        textField.text = currentFirstName;
    }
    
    else if(textField == self.lastNameText)
    {
//        textField.text = currentLastName;
    }
    
    else if(textField == self.phoneNumberText)
    {
//        textField.text = currentPhone;
        
        if(textField.text.length > 0)
        {
            if(![[textField.text substringWithRange:NSMakeRange(0, 2)] isEqualToString:@"1-"] && textField.text.length > 10)
            {
                NSMutableString *insertCountryCode = [[NSMutableString alloc] initWithString:[textField.text substringWithRange:NSMakeRange(textField.text.length - 10, 10)]];
                BOOL shouldChangeFormat = false;
                if (![insertCountryCode containsString:@"-"]) {
                    shouldChangeFormat = true;
                }
                [insertCountryCode insertString:@"1-" atIndex:0];
                textField.text = insertCountryCode;
            } else
                if(![[textField.text substringWithRange:NSMakeRange(0, 2)] isEqualToString:@"1-"])
                {
                    NSMutableString *insertCountryCode = [[NSMutableString alloc] initWithString:textField.text];
                    [insertCountryCode insertString:@"1-" atIndex:0];
                    textField.text = insertCountryCode;
                }
            [self updateTextFiel:textField shouldChangeCharactersInRange:NSMakeRange(0, 0) replacementString:textField.text];
        }
        else
        {
            textField.text = @"1-";
        }
    }
    else if(textField == self.emailText)
    {
//        textField.text = currentEmail;
    }else if (textField == self.answerTextField) {
        textField.text = self.inputNewAnswer.length > 0 ? self.inputNewAnswer : @"";
    }
    
}

-(void) onTextFieldChanged:(UITextField*) textfield {
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([textField isEqual:self.phoneNumberText]) {
        UITextRange *selRange = textField.selectedTextRange;
        UITextPosition *selStartPos = selRange.start;
        NSInteger idx = [textField offsetFromPosition:textField.beginningOfDocument toPosition:selStartPos];
        if (idx == 0) {
            UITextPosition* start = [textField positionFromPosition:textField.beginningOfDocument offset:2];
            [textField setSelectedTextRange:[textField textRangeFromPosition:start toPosition:[textField positionFromPosition:start offset:0]]];
            return NO;
        }
    }
//    [self checkTextFieldIsChange:textField shouldChangeCharactersInRange:range replacementString:string];
    return [self updateTextFiel:textField shouldChangeCharactersInRange:range replacementString:string];
}

-(void)checkTextFieldIsChange:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
}

-(BOOL)updateTextFiel:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(string.length > 1)
    {
        NSMutableString *newString = [[NSMutableString alloc] initWithString:[string removeRedudantWhiteSpaceInText]];
        
        NSString *resultText = [textField.text stringByReplacingCharactersInRange:range
                                                                       withString:string];
        resultText = [resultText removeRedudantWhiteSpaceInText];
        
        if(textField == self.firstNameText || textField == self.lastNameText)
        {
            textField.text = (resultText.length > MAX_FIRSTNAME_TEXT_LENGTH) ? [resultText substringWithRange:NSMakeRange(0, MAX_FIRSTNAME_TEXT_LENGTH)] : resultText;
        }
        
        if(textField == self.zipCodeText)
        {
            if(textField.text.length == 10 && ![string isEqualToString:@""])
            {
                return NO;
            }
            
            if([newString isValidZipCode] || newString.length <= 5)
            {
                textField.text = newString;
            }
            else if(newString.length > 5)
            {
                if(![[newString substringWithRange:NSMakeRange(5, 1)] isEqualToString:@"-"])
                {
                    [newString insertString:@"-" atIndex:5];
                }
                
                textField.text = (newString.length > 10) ? [newString substringWithRange:NSMakeRange(0, 10)] : newString;
            }
        }
        
        if(textField == self.phoneNumberText)
        {
            if([newString isValidPhoneNumber])
            {
                textField.text = newString;
            }
            else{
                if(newString.length <= 10 && ![[newString substringWithRange:NSMakeRange(0, 2)] isEqualToString:@"1-"])
                {
                    [newString insertString:@"1-" atIndex:0];
                }
                
                if(newString.length > 5 && ![[newString substringWithRange:NSMakeRange(5, 1)] isEqualToString:@"-"])
                {
                    [newString insertString:@"-" atIndex:5];
                }
                
                if(newString.length > 9 && ![[newString substringWithRange:NSMakeRange(9, 1)] isEqualToString:@"-"])
                {
                    [newString insertString:@"-" atIndex:9];
                }
                
                textField.text = (newString.length > 14) ? [newString substringWithRange:NSMakeRange(0, 14)] : newString;
            }
            
        }
        
        if(textField == self.emailText)
        {
            textField.text = (resultText.length > 96) ? [resultText substringWithRange:NSMakeRange(0, 96)] : resultText;
        }
        
        return true;
    }
    
    if(textField == self.firstNameText || textField == self.lastNameText)
    {
        if(textField.text.length == MAX_FIRSTNAME_TEXT_LENGTH && ![string isEqualToString:@""])
        {
            return NO;
        }
        return YES;
    }
    else if(textField == self.phoneNumberText)
    {
        if(textField.text.length == 14 && ![string isEqualToString:@""])
        {
            return NO;
        }
        
        if(range.location < 2 && [string isEqualToString:@""])
        {
            return NO;
        }
        
//        if(range.location < textField.text.length)
//        {
//            textField.text = [self reformatForText:textField WithNewString:string WithRange:range];
//            NSRange newRange;
//            if([string isEqualToString:@""])
//            {
//                newRange = (textField.text.length < range.location) ? NSMakeRange(textField.text.length, 0) : NSMakeRange(range.location, 0);
//            }
//            else
//            {
//                newRange = (range.location == 5 || range.location == 9) ? NSMakeRange(range.location + 2, 0) : NSMakeRange(range.location + 1, 0);
//
//            }
//            [textField updateCursorPositionAtRange:newRange];
//            return NO;
//        }
        
        if(textField.text.length == 5 && ![string isEqualToString:@""])
        {
            textField.text = [NSString stringWithFormat:@"%@-",textField.text];
        }
        
        if(textField.text.length == 7 && [string isEqualToString:@""])
        {
            textField.text = [textField.text substringToIndex:6];
        }
        
        if(textField.text.length == 9 && ![string isEqualToString:@""])
        {
            textField.text = [NSString stringWithFormat:@"%@-",textField.text];
        }
        
        if(textField.text.length == 11 && [string isEqualToString:@""])
        {
            textField.text = [textField.text substringToIndex:10];
        }
    }
    else if(textField == self.zipCodeText)
    {
//        if ([string isEqualToString:@""]) {
//            return YES;
//        }
        
        if(textField.text.length == 10 && ![string isEqualToString:@""])
        {
            return NO;
        }
        
//        if(range.location < textField.text.length)
//        {
//            textField.text = [self reformatForText:textField WithNewString:string WithRange:range];
//            NSRange newRange;
//            if([string isEqualToString:@""])
//            {
//                newRange = (textField.text.length < range.location) ? NSMakeRange(textField.text.length, 0) : NSMakeRange(range.location, 0);
//            }
//            else
//            {
//                newRange = (range.location == 5) ? NSMakeRange(range.location + 2, 0) : NSMakeRange(range.location + 1, 0);
//            }
//            [textField updateCursorPositionAtRange:newRange];
//            return true;
//        }
        
        
        if(textField.text.length == 5 && ![string isEqualToString:@""])
        {
            textField.text = [NSString stringWithFormat:@"%@-",textField.text];
        }
        
        if(textField.text.length == 7 && [string isEqualToString:@""])
        {
            textField.text = [textField.text substringToIndex:6];
        }
        
        return YES;
    }
    else if(textField == self.emailText)
    {
        if(textField.text.length == 100 && ![string isEqualToString:@""])
        {
            return NO;
        }
        
        if([textField.text occurrenceCountOfCharacter:'@'] == 1 && [string isEqualToString:@"@"]){
            return NO;
        }
        
        return YES;
    }
    
    return YES;
}


#pragma mark LOGICAL FUNCTION

-(NSString *)reformatForText:(UITextField *)textField WithNewString:(NSString *)newString WithRange:(NSRange)range
{
    NSMutableString *mutableString = [[NSMutableString alloc] initWithString:textField.text];
    NSRange newRange = range;
    if([newString isEqualToString:@""] && (range.location == 5 ||  range.location == 9))
    {
        newRange = NSMakeRange(range.location - 1, 1);
    }
    [mutableString replaceCharactersInRange:newRange withString:newString];
    mutableString = [[NSMutableString alloc] initWithString:[mutableString stringByReplacingOccurrencesOfString:@"-" withString:@""]];
    
    if(mutableString.length >= 1 && textField.tag == PHONE_NUMBER_TEXTFIELD_TAG)
    {
        [mutableString insertString:@"-" atIndex:1];
    }
    
    if(mutableString.length > 5)
    {
        [mutableString insertString:@"-" atIndex:5];
    }
    
    if(mutableString.length > 9 && textField.tag == PHONE_NUMBER_TEXTFIELD_TAG)
    {
        [mutableString insertString:@"-" atIndex:9];
    }
    
    return mutableString;
}
@end
