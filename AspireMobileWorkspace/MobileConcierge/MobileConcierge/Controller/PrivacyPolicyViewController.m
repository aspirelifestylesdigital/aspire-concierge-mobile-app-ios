//
//  PrivacyPolicyViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 4/26/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "PrivacyPolicyViewController.h"
#import "Constant.h"
#import "SWRevealViewController.h"
#import "CreateProfileViewController.h"
#import "UIButton+Extension.h"
#import "UILabel+Extension.h"
#import "AppData.h"
#import "HomeViewController.h"
#import "Common.h"
#import "WSB2CGetPolicyInfo.h"
#import "PolicyInfoItem.h"
#import "WSB2CGetAboutInfo.h"
#import "AboutInfoItem.h"
#import "WSB2CGetTermsOfUse.h"
#import "TermsOfUseInfoItem.h"

@interface PrivacyPolicyViewController ()<DataLoadDelegate,UIWebViewDelegate,UIScrollViewDelegate>
@end

@implementation PrivacyPolicyViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTextStyleForView];
   
    switch (self.getClientCopyType) {
        case GetClientCopy_PolicyPrivacy:
            [self caseGetClientCopyPrivacyPolicy];
            [self trackingScreenByName:@"Privacy policy"];
            break;
        case GetClientCopy_About:
            [self trackingScreenByName:@"About this app"];
            [self caseGetClientCopyAbout];
            break;
        case GetClientCopy_TermOfUse:
            [self trackingScreenByName:@"Terms of use"];
            [self caseGetClientCopyTermOfUse];
            break;
        default:
            break;
    }
}

- (void) caseGetClientCopyPrivacyPolicy{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkButtonAndEnableSubmit)];
    [self.confirmMessageLabel addGestureRecognizer:tap];
    
    [self.checkBoxButton setImage:[UIImage imageNamed:@"checkbox_uncheck"] forState:UIControlStateNormal];
    [self.checkBoxButton setImage:[UIImage imageNamed:@"checkbox_check"] forState:UIControlStateSelected];
    [self.checkBoxButton addTarget:self action:@selector(enableSubmitButton:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    if(self.isRecheckPolicy)
    {
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = nil;
        alert.msgAlert = NSLocalizedString(@"privacy_policy_updated_confirmation", nil);
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.midView.hidden = YES;
            alert.seconBtn.hidden = YES;
        });
        
        [self showAlert:alert forNavigation:NO];
    }
    
    if (self.policyText) {
        content = self.policyText;
        
        [self getContentHeight];
        
        [self.webView loadHTMLString:centerHTMLString(content, FONT_MarkForMC_REGULAR, ([AppSize descriptionTextSize]), @"#FFFFFF") baseURL:nil];
    }else{
        [self startActivityIndicator];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            WSB2CGetPolicyInfo *ws = [[WSB2CGetPolicyInfo alloc] init];
            ws.delegate = self;
            [ws retrieveDataFromServer];
        });
    }
}

- (void) caseGetClientCopyAbout{
    [self startActivityIndicator];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        WSB2CGetAboutInfo *ws = [[WSB2CGetAboutInfo alloc] init];
        ws.delegate = self;
        [ws retrieveDataFromServer];
    });
}

- (void) caseGetClientCopyTermOfUse{
    [self startActivityIndicator];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        WSB2CGetTermsOfUse *ws = [[WSB2CGetTermsOfUse alloc] init];
        ws.delegate = self;
        [ws retrieveDataFromServer];
    });
}

-(void)setTextStyleForView
{
    [self setConfirmMessageLabelStatus:NO];
    [self enableCheckButton:NO];
    // Text Style For Title
    self.titleViewController.attributedText = [self attributedStringWithString:NSLocalizedString(@"privacy_policy_title_message", nil) andFont:FONT_MarkForMC_MED andSize:[AppSize headerTextSize] andColor:[AppColor activeTextColor]];
    
    
    // Text Style For Submit Button
    [self.submitButton setTitle:NSLocalizedString(@"submit_button_title", nil) forState:UIControlStateNormal];
    [self.submitButton setBackgroundColorForNormalStatus];
    [self.submitButton setBackgroundColorForTouchingStatus];
    [self.submitButton configureDecorationForButton];
    [self.submitButton setBackgroundColorForDisableStatus];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:([AppSize titleTextSize])],
                                 NSKernAttributeName: @([AppSize descriptionLetterSpacing])};
    if (self.submitButton)
        self.submitButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.submitButton.titleLabel.text attributes:attributes];
    
    self.webView.delegate = self;
    self.webView.scrollView.delegate = self;
    [self.webView setBackgroundColor:[UIColor clearColor]];
    [self.webView setOpaque:NO];
}

- (NSAttributedString*)attributedStringWithString:(NSString*)string andFont:(NSString*)fontString andSize:(float)size andColor:(UIColor*)color{
    
    NSRange range = NSMakeRange(0, string.length);
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:string];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont fontWithName:fontString size:size] range:range];
    [attributeString addAttribute:NSForegroundColorAttributeName value:color range:range];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init] ;
    paragraphStyle.lineSpacing = [AppSize titleLineSpacing];
    [attributeString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    return attributeString;
}

-(void)checkButtonAndEnableSubmit
{
    
    if(self.checkBoxButton.selected)
    {
        self.checkBoxButton.selected = NO;
    }
    else{
        self.checkBoxButton.selected = YES;
    }

    [self.checkBoxButton setBackgroundColor:[UIColor clearColor]];
    [self.submitButton setEnabled:self.checkBoxButton.selected];
    if(self.checkBoxButton.selected)
    {
        [AppData storedTimeAcceptedPolicy];
    }
}

- (void)setConfirmMessageLabelStatus:(BOOL)status{
    if (status == YES) {
        // Text Style For Greet Message
        self.confirmMessageLabel.attributedText = [self attributedStringWithString:NSLocalizedString(@"privacy_policy_confirm_message", nil) andFont:FONT_MarkForMC_REGULAR andSize:[AppSize titleTextSize] andColor:[AppColor activeTextColor]];
        self.confirmMessageLabel.userInteractionEnabled = YES;
    }else{
        // Text Style For Greet Message
        self.confirmMessageLabel.attributedText = [self attributedStringWithString:NSLocalizedString(@"privacy_policy_confirm_message", nil) andFont:FONT_MarkForMC_REGULAR andSize:[AppSize titleTextSize] andColor:[AppColor disableTextColor]];
        self.confirmMessageLabel.userInteractionEnabled = NO;
    }
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void) enableCheckButton:(BOOL)status{
    [self.checkBoxButton setEnabled:status];
    self.checkBoxButton.userInteractionEnabled = status;
}


-(void) enableSubmitButton:(UIButton *)button
{
    [button setHighlighted:NO];
    self.checkBoxButton.selected = !(button.selected);
    [button setBackgroundColor:[UIColor clearColor]];
    [self.submitButton setEnabled:button.selected];
    
    if(self.checkBoxButton.selected)
    {
        [AppData storedTimeAcceptedPolicy];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getContentHeight{
    UITextView *tempTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, self.webView.frame.size.width, self.webView.frame.size.height)];
    
    tempTextView.attributedText = [[NSAttributedString alloc]
                                   initWithData: [content dataUsingEncoding:NSUnicodeStringEncoding]
                                   options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                   documentAttributes: nil
                                   error: nil
                                   ];
    textHeight = tempTextView.contentSize.height;
}



- (IBAction)submitButtonTapped:(id)sender {
    if(self.isRecheckPolicy || [AppData isCreatedProfile])
    {
        if (self.policyVersion) {
            [[SessionData shareSessiondata] setCurrentPolicyVersion:self.policyVersion];
        }
        SWRevealViewController *revealViewController = self.revealViewController;
        HomeViewController *homeViewConroller = [[HomeViewController alloc] init];
        UINavigationController *fontViewController = [[UINavigationController alloc] initWithRootViewController:homeViewConroller];
        [revealViewController pushFrontViewController:fontViewController animated:YES];
    }
    else
    {
        SWRevealViewController *revealViewController = self.revealViewController;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        CreateProfileViewController *createProfileVC = [storyboard instantiateViewControllerWithIdentifier:@"CreateProfileViewController"];
        UINavigationController *fontViewController = [[UINavigationController alloc] initWithRootViewController:createProfileVC];
        [revealViewController pushFrontViewController:fontViewController animated:YES];
    }
}

- (NSString*) replaceWebLinkInString:(NSString*)string{
    
    NSArray *tempArr = getMatchesFromString(string,@"href+=+\"(?!#+)(.*?)+\"",NSRegularExpressionCaseInsensitive);
    
    NSMutableArray* vvv = [NSMutableArray new];
    for (NSString* test in tempArr) {
        NSString* t = strimStringFrom(test,@[@"href=|\"?"]);
        if(![t containsString:@"http"]) {
            [vvv addObject:@{test:[[@"href=\"http://" stringByAppendingString:t] stringByAppendingString:@"\""]}];
        }
    }
    NSMutableString* tttt = [NSMutableString stringWithString:string];
    for (NSDictionary* t in vvv) {
        if([string containsString:[[t allKeys] firstObject]]) {
            string = [tttt stringByReplacingOccurrencesOfString:[[t allKeys] firstObject] withString:[t objectForKey:[[t allKeys] firstObject]]];
        }
    }
    return string;
}

-(void)loadDataDoneFrom:(WSBase *)ws
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if([ws isKindOfClass:[WSB2CGetPolicyInfo class]] && ws.data.count > 0)
        {
            [[SessionData shareSessiondata] setCurrentPolicyVersion:((PolicyInfoItem *)[ws.data objectAtIndex:0]).CurrentVersion];
            content = ((PolicyInfoItem *)[ws.data objectAtIndex:0]).textInfo;
            //        [self getContentHeight];
        }else if ([ws isKindOfClass:[WSB2CGetAboutInfo class]] && ws.data.count > 0){
            content = ((AboutInfoItem *)[ws.data objectAtIndex:0]).textInfo;
        }else if ([ws isKindOfClass:[WSB2CGetTermsOfUse class]] && ws.data.count > 0){
            content = ((TermsOfUseInfoItem *)[ws.data objectAtIndex:0]).textInfo;
        }
        
        NSRange bodyHtmlRange = [content rangeOfString:@"font-family"];
        if (bodyHtmlRange.location == NSNotFound) {
            [self.webView loadHTMLString:centerHTMLString(content, FONT_MarkForMC_REGULAR, ([AppSize descriptionTextSize]), @"#FFFFFF") baseURL:nil];
        }else{
            [self.webView loadHTMLString:content baseURL:nil];
        }
        
    });
}


-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    UITextView *tempTextView = [[UITextView alloc] initWithFrame:self.webView.frame];
    tempTextView.text = content;
    
    if(textHeight < self.webView.bounds.size.height)
    {
        [self didScrollToEnd];
    }
    [self stopActivityIndicator];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self stopActivityIndicator];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    if(ISLOGINGINFO)
    {
        NSLog(@"%@", request.URL);
    }
    
    if (navigationType==UIWebViewNavigationTypeLinkClicked) {
        if ([request.URL.absoluteString containsString:@"http"]) {
            
            AlertViewController *alert = [[AlertViewController alloc] init];
            alert.titleAlert = NSLocalizedString(@"alert_title", nil);
            alert.msgAlert = NSLocalizedString(@"category_more_info_msg", nil);
            alert.secondBtnTitle = NSLocalizedString(@"yes_button", nil);
            alert.blockSecondBtnAction = ^(void){
                [[UIApplication sharedApplication] openURL:request.URL];
            };
            
            alert.firstBtnTitle = NSLocalizedString(@"alert_cancel_button", nil);
            alert.blockFirstBtnAction  = ^(void){};
            [self showAlert:alert forNavigation:YES];
            // It was a link
            
            return NO;
        }
    }
    
    return YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height)
    {
        [self didScrollToEnd];
        
    }
}
- (void) didScrollToEnd{
    [self setConfirmMessageLabelStatus:YES];
    [self enableCheckButton:YES];
}
@end
