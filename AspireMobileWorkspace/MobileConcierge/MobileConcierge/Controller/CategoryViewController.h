//
//  CategoryViewController.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "CityItem.h"

@protocol SelectingCategoryDelegate;

@interface CategoryViewController : BaseViewController

@property(nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property(nonatomic, strong) NSArray *categoryLst;
@property(nonatomic, weak) id<SelectingCategoryDelegate> delegate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
@property (strong, nonatomic)  CityItem *currentCity;
@property (weak, nonatomic) IBOutlet UIButton *btnAllCategory;


@end
