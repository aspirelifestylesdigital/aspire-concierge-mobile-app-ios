//
//  AboutAppViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/16/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "AboutAppViewController.h"
#import "WSB2CBase.h"


@interface AboutAppViewController ()<UIScrollViewDelegate, UIWebViewDelegate>



@end

@implementation AboutAppViewController

- (void)viewDidLoad {
    self.getClientCopyType = GetClientCopy_About;
    isNotAskConciergeBarButton = YES;
    [super viewDidLoad];
    [self createBackBarButtonWithIconName:@"clear_icon"];
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"about_this_app_title", nil)];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //  Set up Pop pan gesture
    [self setUpCustomizedPanGesturePopRecognizer];
}

- (void)handlePopRecognizer:(UIPanGestureRecognizer *)recognizer{
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) didScrollToEnd{
    backButtonItem.enabled = YES;
}
@end
