//
//  SearchViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "SearchViewController.h"
#import "UIView+Extension.h"
#import "UIButton+Extension.h"
#import "Constant.h"
#import "Common.h"

@interface SearchViewController ()<UITextFieldDelegate>
{
    CGFloat backupBottomConstraint;
}
@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UITapGestureRecognizer *tappedOutsideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tappedOutsideKeyboard];
    
    backupBottomConstraint = self.searchBtnBottomConstraint.constant;
    [self trackingScreenByName:@"Search"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldChanged:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:self.searchText];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWill:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:self.searchText];
}

-(void)textFieldChanged:(NSNotification*)notification {
    
    if ([self.searchText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0) {
        self.bookCheckBox.selected = NO;
        self.offerCheckBox.selected = NO;
    }
    
    [self checkToChangeStatusControl:[self.searchText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0];
}

- (void) checkToChangeStatusControl:(BOOL) isShow {
    self.stackOffer.hidden = !isShow;
    self.searchButton.enabled = isShow;
    self.bookCheckBox.enabled = isShow;
    self.offerCheckBox.enabled = isShow;
}

-(void)initView
{
    [self backNavigationItem];
    [self.offerCheckBox setImage:[UIImage imageNamed:@"checkbox_uncheck"] forState:UIControlStateNormal];
    [self.offerCheckBox setImage:[UIImage imageNamed:@"checkbox_check"] forState:UIControlStateSelected];
    [self.offerCheckBox addTarget:self action:@selector(enableSubmitButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.offerCheckBox setBackgroundColor:[UIColor clearColor]];
    
    if(self.currentKeyWord.length == 0)
    {
        self.offerCheckBox.enabled = NO;
    }
    else
    {
        if(self.currentIsOffer)
        {
            self.searchText.text = self.currentKeyWord;
            [self enableSubmitButton:nil];
        }
    }
    
    [self.bookCheckBox setImage:[UIImage imageNamed:@"checkbox_uncheck"] forState:UIControlStateNormal];
    [self.bookCheckBox setImage:[UIImage imageNamed:@"checkbox_check"] forState:UIControlStateSelected];
    [self.bookCheckBox addTarget:self action:@selector(enableSubmitButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.bookCheckBox setBackgroundColor:[UIColor clearColor]];
    self.bookCheckBox.enabled = NO;
    
    [self.bookText setText:NSLocalizedString(@"can_book_search", nil)];
    [self.offerText setText:NSLocalizedString(@"offer_search", nil)];
    self.offerText.font = [UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize titleTextSize]];
    self.offerText.textColor = [AppColor textColor];
    
    UITapGestureRecognizer *offerTextGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enableSubmitButton:)];
    [self.offerText setUserInteractionEnabled:YES];
    [self.offerText addGestureRecognizer:offerTextGesture];
    
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"search_title", nil)];
    
    [self.searchButton setBackgroundColorForNormalStatus];
    [self.searchButton setBackgroundColorForTouchingStatus];
    [self.searchButton setBackgroundColorForDisableStatus];
    
    if(self.currentKeyWord.length == 0)
    {
        self.searchButton.enabled = NO;
    }
    
    self.searchButton.titleLabel.font = [UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize titleTextSize]];
    self.searchButton.titleLabel.textColor = [UIColor whiteColor];
    [self.searchButton setTitle:NSLocalizedString(@"search_title", nil) forState:UIControlStateNormal];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:([AppSize titleTextSize])],
                                 NSKernAttributeName: @([AppSize descriptionLetterSpacing])};
    self.searchButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.searchButton.titleLabel.text attributes:attributes];
    
    UIImageView *searchIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"search_icon"]];
    searchIcon.frame = CGRectMake(0.0f, 0.0f, 30.0f, 30.0f);
    self.searchText.leftView = searchIcon;
    self.searchText.leftViewMode = UITextFieldViewModeAlways;
    self.searchText.delegate = self;
    self.searchText.textColor = [AppColor textColor];
    
    UIFont *font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.searchText.font = font;
    
    if(self.currentKeyWord.length == 0)
    {
        NSString *placeHolderMsg = NSLocalizedString(@"search_text", nil);
        NSMutableAttributedString *placeHolderAttribute = [[NSMutableAttributedString alloc] initWithString:placeHolderMsg];
        [placeHolderAttribute addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, placeHolderMsg.length)];
        [placeHolderAttribute addAttribute:NSForegroundColorAttributeName value:[AppColor placeholderTextColor] range:NSMakeRange(0, placeHolderMsg.length)];
        self.searchText.attributedPlaceholder = placeHolderAttribute;
    }
    else{
        self.searchText.text = self.currentKeyWord;
    }

    [self.searchTextView setBackgroundColor:[AppColor childViewBackgroundColor]];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    [self checkToChangeStatusControl:[self.searchText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0];
}


-(void)handleKeyboardWillShow:(NSNotification *)paramNotification
{
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         self.searchBtnBottomConstraint.constant = keyboardRect.size.height + MARGIN_KEYBOARD;
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

-(void)handleKeyboardWill:(NSNotification *)paramNotification
{
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         self.searchBtnBottomConstraint.constant =  backupBottomConstraint;
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
    
}


-(void)dismissKeyboard
{
    if([self.searchText becomeFirstResponder])
    {
        [self.searchText resignFirstResponder];
    }
}

-(void) enableSubmitButton:(id)sender
{
    if(self.searchText.text.length > 0)
    {
        [self.offerCheckBox setHighlighted:NO];
        self.offerCheckBox.selected = !(self.offerCheckBox.selected);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)searchTappedAction:(id)sender {
    [self searchAction];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return [self searchAction];
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    if(string.length > 0)
//    {
//        self.searchButton.enabled = YES;
//        self.bookCheckBox.enabled = YES;
//        self.offerCheckBox.enabled = YES;
//    }
//    else{
//        if(textField.text.length == 1){
//            self.searchButton.enabled = NO;
//            self.bookCheckBox.enabled = NO;
//            self.bookCheckBox.selected = NO;
//            self.offerCheckBox.enabled = NO;
//            self.offerCheckBox.selected = NO;
//        }
//        else{
//            self.searchButton.enabled = YES;
//            self.bookCheckBox.enabled = YES;
//            self.offerCheckBox.enabled = YES;
//        }
//    }
//    return YES;
//}

- (void)textFieldDidEndEditing:(UITextField *)textField;
{
    
}

-(BOOL)searchAction
{
    if([self.searchText.text length] > 0 || self.offerCheckBox.selected)
    {
        if([self.delegate respondsToSelector:@selector(getDataBaseOnSearchText:withOffer:withBookOnline:)])
        {
            [self.searchText resignFirstResponder];
            [self.navigationController popViewControllerAnimated:YES];
            [self.delegate getDataBaseOnSearchText:self.searchText.text withOffer:self.offerCheckBox.selected withBookOnline:self.bookCheckBox.selected];
        }
        return YES;
    }
    else{
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"alert_title", nil);;
        alert.msgAlert = @"Enter a search term or select a check box.";
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.hidden = YES;
        });
        
        [self showAlert:alert forNavigation:YES];
        return NO;
    }
}

@end
