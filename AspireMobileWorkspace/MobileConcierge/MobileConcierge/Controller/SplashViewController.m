//
//  SplashViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/5/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "SplashViewController.h"
#import "SWRevealViewController.h"
#import "PasscodeController.h"
#import "Constant.h"
#import "EnumConstant.h"
#import "HomeViewController.h"
#import "UIView+Extension.h"
#import "AppData.h"
#import "PrivacyPolicyViewController.h"
#import "CreateProfileViewController.h"
#import "Common.h"
#import "WSB2CGetRequestToken.h"
#import "WSB2CGetAccessToken.h"
#import "WSB2CGetUserDetails.h"
#import "WSB2CGetPolicyInfo.h"
#import "WSB2CVerifyBIN.h"
#import "PolicyInfoItem.h"
#import "BINItem.h"
#import "Common.h"
//#import <AspireApiFramework/AspireApiFramework.h>
#import "SignInViewController.h"
@import AspireApiFramework;

@interface SplashViewController () <DataLoadDelegate>

@end

@implementation SplashViewController
{
    NSInteger currentTask;
    BOOL isLoadPolicy;
    WSB2CGetRequestToken *wsRequestToken;
    WSB2CGetPolicyInfo *wsPolicy;
    
    NSNumber *currentPolicy;
    dispatch_group_t group;
    dispatch_queue_t queue;
    float policyVersion;
    CFAbsoluteTime startTime;
    CFAbsoluteTime endTime;
    
}


- (void)viewDidLoad {
    isIgnoreScaleView = YES;
    startTime = CFAbsoluteTimeGetCurrent();
    [super viewDidLoad];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        [NSThread sleepForTimeInterval:(3.0)];
        dispatch_async(dispatch_get_main_queue(), ^{
            if(isNetworkAvailable())
            {
                [self startActivityIndicatorWithoutMask];
            }
        });
    });
    [self trackingScreenByName:@"Splash"];
    
//     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    
}

- (void)dealloc
{
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.view layoutIfNeeded];
    
    group = dispatch_group_create();
    queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_group_notify(group, queue, ^{
        if(ISLOGINGINFO)
        {
            NSLog(@"All tasks done");
        }
    });
    
    [self CheckBINNumber];
    [self getUUID];
}

- (void) showSpinner{
    [self startActivityIndicatorWithoutMask];
}

- (void) stopSpinner{
    [self stopActivityIndicatorWithoutMask];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getUUID{
    if (![[SessionData shareSessiondata] UUID]) {
        NSString *uuidString = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [[SessionData shareSessiondata] setUUID:uuidString];
    }
}

- (void)appWillEnterForeground:(NSNotification*) noti{
    [self showSpinner];
}

- (void) CheckBINNumber{
    if(isJailbroken())
    {
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = nil;
        alert.msgAlert = NSLocalizedString(@"jailbroken_message", nil);
        alert.firstBtnTitle = NSLocalizedString(@"alert_ok_button", nil);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.hidden = YES;
        });
        
        alert.blockFirstBtnAction = ^(void){
            //Quit app
            exit(0);
        };
        
        [self showAlert:alert forNavigation:NO];

        return;
    }

    if(![User isValid])
    {
        endTime = CFAbsoluteTimeGetCurrent();
        double timeElapsed = endTime - startTime;
        if(timeElapsed < 3.0)
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [NSThread sleepForTimeInterval:(3.0 - timeElapsed)];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self navigationToInitialBinViewController:NO];
                });
            });
        }
        else{
            [self navigationToInitialBinViewController:NO];
        }
    }
    else {
        typeof(self) __weak _self = self;
        [ModelAspireApiManager retrieveProfileCurrentUser:^(User *user, NSError *error) {
            if (!_self) return;
            typeof(self) __self = _self;
            if (!error) {
                [[SessionData shareSessiondata] setUserObject:(UserObject *)[user convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([UserObject class])]];
//                WSB2CVerifyBIN *wsVerifyBIN = [[WSB2CVerifyBIN alloc] init];
//                wsVerifyBIN.bin = [[SessionData shareSessiondata] BINNumber];
//                wsVerifyBIN.delegate = __self;
//                wsVerifyBIN.task = WS_B2C_VERIFY_BIN;
//                [wsVerifyBIN verifyBIN];
                [ModelAspireApiManager verifyBin:[[User current] getPasscode] completion:^(BOOL isPass, NSString *bin, id  _Nullable error) {
                    if (isPass && bin) {
                        [ModelAspireApiManager registerServiceUsername:aspire_api_service_username
                                                              password:aspire_api_service_password
                                                               program:aspire_api_program
                                                                   bin:bin];
                        [[SessionData shareSessiondata] setBINNumber:bin];
                        [_self loadPolicy];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self appRedirect];
                        });
                    } else {
//                        __self->endTime = CFAbsoluteTimeGetCurrent();
//                        double timeElapsed = __self->endTime - __self->startTime;
//                        if(timeElapsed < 3.0)
//                        {
//                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                                [NSThread sleepForTimeInterval:(3.0 - timeElapsed)];
//                                dispatch_async(dispatch_get_main_queue(), ^{
//                                    SWRevealViewController *revealViewController = self.revealViewController;
//                                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//                                        PasscodeController *signInViewController = [storyboard instantiateViewControllerWithIdentifier:@"PasscodeController"];
//                                        signInViewController.isBINInvalid = true;
//                                    UINavigationController *fontViewController = [[UINavigationController alloc] initWithRootViewController:signInViewController];
//                                    [revealViewController pushFrontViewController:fontViewController animated:NO];
//                                });
//                            });
//                        }
//                        else{
                            dispatch_async(dispatch_get_main_queue(), ^{
                                SWRevealViewController *revealViewController = self.revealViewController;
                                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                PasscodeController *signInViewController = [storyboard instantiateViewControllerWithIdentifier:@"PasscodeController"];
                                signInViewController.isBINInvalid = true;
                                UINavigationController *fontViewController = [[UINavigationController alloc] initWithRootViewController:signInViewController];
                                [revealViewController pushFrontViewController:fontViewController animated:NO];
                            });
//                        }
                    }
                }];
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([AspireApiError getErrorTypeAPIRetrieveProfileFromError:error] == ACCESSTOKEN_INVALID_ERR) {
                        [_self showAlertInvalidUserAndShouldLoginAgain:true];
                        return;
                    }
                    [_self showAlertWithTitle:NSLocalizedString(@"cannot_get_data", nil)
                                      message:NSLocalizedString(@"error_api_message", nil)
                                      buttons:@[NSLocalizedString(@"ok_button_title", nil)]
                                      actions:nil
                             messageAlignment:NSTextAlignmentCenter];
                });
            }
        }];
    }
}

- (void) loadPolicy{
    // load policy privacy
    wsPolicy = [[WSB2CGetPolicyInfo alloc] init];
    wsPolicy.delegate = self;
    isLoadPolicy = YES;
    [wsPolicy retrieveDataFromServer];
}

- (void) getRequestToken{
    wsRequestToken = [[WSB2CGetRequestToken alloc] init];
    wsRequestToken.delegate = self;
    currentTask = WS_GET_REQUEST_TOKEN;
    [wsRequestToken getRequestToken];
}



- (void)loadDataDoneFrom:(id<WSBaseProtocol>)ws{
//    if([ws isKindOfClass:[WSB2CVerifyBIN class]])
//    {
//        WSB2CVerifyBIN *wsVerify = (WSB2CVerifyBIN *)ws;
//        if(wsVerify.data.count > 0)
//        {
//            BINItem *binItem = (BINItem *)[wsVerify.data objectAtIndex:0];
//            if(binItem.valid){
//                //                [self getRequestToken];
//                [self loadPolicy];
//            }
//            else{
//                endTime = CFAbsoluteTimeGetCurrent();
//                double timeElapsed = endTime - startTime;
//                if(timeElapsed < 3.0)
//                {
//                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                        [NSThread sleepForTimeInterval:(3.0 - timeElapsed)];
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            [self navigationToInitialBinViewController:YES];
//                        });
//                    });
//                }
//                else{
//                    [self navigationToInitialBinViewController:YES];
//                }
//            }
//        }
//    } else
        if ([ws isKindOfClass:[WSB2CGetPolicyInfo class]]) {
//        endTime = CFAbsoluteTimeGetCurrent();
//        double timeElapsed = endTime - startTime;
//        if(timeElapsed < 3.0)
//        {
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                double since = 3.0 - timeElapsed;
//                if (since < 0) since = 0;
//                [NSThread sleepForTimeInterval:since];
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [self appRedirect];
//                });
//            });
//        }
//        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self appRedirect];
            });
//        }
    }
//    else if (![ws isKindOfClass:[WSB2CGetPolicyInfo class]]) {
//        if (currentTask == WS_GET_REQUEST_TOKEN) {
//            WSB2CGetAccessToken *wsAccessToken = [[WSB2CGetAccessToken alloc] init];
//            wsAccessToken.delegate = self;
//            [[SessionData shareSessiondata] setRequestToken:wsRequestToken.requestToken];
//            currentTask = WS_GET_ACCESS_TOKEN;
//            [wsAccessToken requestAccessToken:wsRequestToken.requestToken member:[[SessionData shareSessiondata] OnlineMemberID]];
//
//            [self appRedirect]; // app redirect in aspire api
//
//        }else if(currentTask == WS_GET_ACCESS_TOKEN){
//            // get user detail
//            WSB2CGetUserDetails *wsGetUser = [[WSB2CGetUserDetails alloc] init];
//            wsGetUser.delegate = self;
//            currentTask = WS_UPDATE_USER;
//            [wsGetUser getUserDetails];
//        }else if (currentTask == WS_UPDATE_USER){
//            endTime = CFAbsoluteTimeGetCurrent();
//            double timeElapsed = endTime - startTime;
//            if(timeElapsed < 3.0)
//            {
//                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                    [NSThread sleepForTimeInterval:(3.0 - timeElapsed)];
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        [self appRedirect];
//                    });
//                });
//            }
//            else{
//                [self appRedirect];
//            }
//        }
//    }
}
-(void)loadDataFailFrom:(id<WSBaseProtocol>)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message{
    [self stopActivityIndicator];
    NSString *title;
    message = NSLocalizedString(@"error_api_message", nil);
    title = NSLocalizedString(@"cannot_get_data", nil);
    
    [self showAPIErrorAlertWithMessage:message andTitle:title];
}

- (void) showAPIErrorAlertWithMessage:(NSString*)message andTitle:(NSString*)title{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.msgAlert = message;
    alert.titleAlert = title;
    alert.secondBtnTitle = NSLocalizedString(@"settings_button", nil);
    alert.blockSecondBtnAction = ^(void){
        openWifiSettings();
    };
    alert.firstBtnTitle = NSLocalizedString(@"alert_ok_button", nil);
    
    [self showAlert:alert forNavigation:NO];
}


-(void)navigationToInitialBinViewController:(BOOL)invalidBIN
{
    SWRevealViewController *revealViewController = self.revealViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SignInViewController *signInViewController = [storyboard instantiateViewControllerWithIdentifier:@"UDASignInViewController"];
//    PasscodeController *signInViewController = [storyboard instantiateViewControllerWithIdentifier:@"PasscodeController"];
//    signInViewController.isBINInvalid = invalidBIN;
    UINavigationController *fontViewController = [[UINavigationController alloc] initWithRootViewController:signInViewController];
    [revealViewController pushFrontViewController:fontViewController animated:NO];
}
- (void) appRedirect{
    if(ISLOGINGINFO)
    {
       NSLog(@"Done Update User: CurrentPolicyVersion %@", [[SessionData shareSessiondata] CurrentPolicyVersion]);
    }
    
    if (((PolicyInfoItem *)[wsPolicy.data objectAtIndex:0]).CurrentVersion.length <= 0)  {
        [self navigationToHomeViewController];
        return;
    }
    
    if ([((PolicyInfoItem *)[wsPolicy.data objectAtIndex:0]).CurrentVersion intValue] != [[[SessionData shareSessiondata] CurrentPolicyVersion] intValue]) {
        [self navigationToPrivacyPolicyViewController];
    }
    else{
        [self navigationToHomeViewController];
    }
}

-(void)navigationToHomeViewController
{
    SWRevealViewController *revealViewController = self.revealViewController;
    HomeViewController *homeViewConroller = [[HomeViewController alloc] init];
    UINavigationController *fontViewController = [[UINavigationController alloc] initWithRootViewController:homeViewConroller];
    [revealViewController pushFrontViewController:fontViewController animated:YES];
    
}

-(void)navigationToPrivacyPolicyViewController
{
    SWRevealViewController *revealViewController = self.revealViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PrivacyPolicyViewController *privacyPolicyViewController = [storyboard instantiateViewControllerWithIdentifier:@"PrivacyPolicyViewController"];
    privacyPolicyViewController.isRecheckPolicy = YES;
    NSString *str = ((PolicyInfoItem *)[wsPolicy.data objectAtIndex:0]).textInfo;
    privacyPolicyViewController.policyText = str;

    if(ISLOGINGINFO)
    {
        NSLog(@"return policy version: %@", ((PolicyInfoItem *)[wsPolicy.data objectAtIndex:0]).CurrentVersion);
    }

    privacyPolicyViewController.policyVersion = ((PolicyInfoItem *)[wsPolicy.data objectAtIndex:0]).CurrentVersion;
    [revealViewController pushFrontViewController:privacyPolicyViewController animated:YES];
}

-(void)navigationToCreateProfileViewController
{
    SWRevealViewController *revealViewController = self.revealViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CreateProfileViewController *createProfileViewController = [storyboard instantiateViewControllerWithIdentifier:@"CreateProfileViewController"];
    UINavigationController *fontViewController = [[UINavigationController alloc] initWithRootViewController:createProfileViewController];
    [revealViewController pushFrontViewController:fontViewController animated:YES];
}
- (void) crashApp{
    [self performSelector:@selector(die_die)];
}
@end
