//
//  SignInViewController.h
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/13/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "UserObject.h"
#import "CustomTextField.h"

@interface CreateProfileViewController : BaseViewController
{
//    NSString *currentFirstName;
    BOOL isCheck;
    CGFloat backupBottomConstraint;
    CGRect signFrame;
    CGFloat signBtnWidth;
    CGFloat signBtnHeight;
    UserObject* userInfo;
    NSString *onlineMemberId;
//    NSString *currentLastName;
//    NSString *currentEmail;
//    NSString *currentPhone;
//    NSString *currentZipCode;
    UIImageView *asteriskImage;
    CGFloat keyboardHeight;
    UITapGestureRecognizer *tappedOutsideKeyboard;
    
}

@property (weak, nonatomic) IBOutlet UILabel *lblBanner;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (nonatomic, weak) IBOutlet UIButton *signinBtn;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIView *myView;
@property (nonatomic, weak) IBOutlet CustomTextField *firstNameText;
@property (weak, nonatomic) IBOutlet CustomTextField *lastNameText;
@property (weak, nonatomic) IBOutlet CustomTextField *emailText;
@property (weak, nonatomic) IBOutlet CustomTextField *phoneNumberText;
@property (weak, nonatomic) IBOutlet CustomTextField *answerTextField;
@property (weak, nonatomic) IBOutlet CustomTextField *zipCodeText;
@property (weak, nonatomic) IBOutlet UIButton *checkBoxBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UILabel *commitmentLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *signinBottomConstraint;
@property (strong, nonatomic) UIButton *signInButton;
@property (assign, nonatomic) BOOL isUpdateProfile; //YES = UpdateProfile ; NO = create profile
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstNameWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstNameHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewActionBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollBottom;
@property (strong,nonatomic) NSString* emailChecked, *lastNameChecked, *firstNameChecked, *zipCodeChecked, *phoneChecked, *partyId;
@property (assign,nonatomic) BOOL hasFoucusedFiled;
@property (strong,nonatomic) NSString*inputNewAnswer;

-(IBAction)checkBox:(id)sender;
-(BOOL)updateTextFiel:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
-(void)checkTextFieldIsChange:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
-(void)setTextViewsDefaultBottomBolder;
-(void) setCheckBoxState:(BOOL)check;
-(void) updateSuccessRedirect;
-(void) createAsteriskForTextField:(UITextField *)textField;
//-(IBAction)signinAction:(id)sender;
-(void)dismissKeyboard;
- (void) getUserInfo;
- (void) handleAsterickIcon;
- (void) updateProfileButtonStatus;
- (void) updateProfileWithAPI;
-(void) verifyAccountData;
- (void) resignFirstResponderForAllTextField;
-(void)createProfileInforWithMessage:(NSString *)message isCreatedSuccessful:(BOOL)isSuccessful;
- (BOOL)verifyValueForTextField:(UITextField *)textFied andMessageError:(NSMutableString *)message;

- (void)textFieldDidBeginEditing:(UITextField *)textField;

-(void) onTextFieldChanged:(UITextField*) textfield;

@end
