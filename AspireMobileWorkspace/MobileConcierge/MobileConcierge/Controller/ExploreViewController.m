//
//  ExploreViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/9/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "ExploreViewController.h"
#import "UIButton+Extension.h"
#import "ExploreTableViewCell.h"
#import "CityViewController.h"
#import "CityItem.h"
#import "CategoryViewController.h"
#import "CategoryItem.h"
#import "SearchViewController.h"
#import "Constant.h"
#import "SearchHeaderView.h"
#import "UIView+Extension.h"
#import "BaseResponseObject.h"
#import "UITableView+Extensions.h"
#import "AFImageDownloader.h"
#import "UIImageView+AFNetworking.h"
#import "AFURLResponseSerialization.h"
#import "WSBase.h"
#import "SWRevealViewController.h"
#import "SelectingCategoryDelegate.h"
#import "CategoryCityGuideViewController.h"
#import <Foundation/Foundation.h>
#import "AppData.h"
#import "WSB2CGetQuestions.h"
#import "ExploreQuestionTableViewCell.h"
#import "AnswerItem.h"
#import "CategoryItem.h"
#import "NSString+Utis.h"
#import "WSB2CGetTiles.h"
#import "TileItem.h"
#import <objc/runtime.h>
#import "WSB2CSearch.h"
#import "TableHeaderView.h"
#import "ImageUtilities.h"
#import "NewExploreDetailViewController.h"

@interface ExploreViewController ()<UITableViewDelegate, UITableViewDataSource, SelectingCityDelegate, SearchDelegate, DataLoadDelegate, SelectingCategoryDelegate, UITextFieldDelegate>
{
    CGFloat searchViewYPos;
    CGFloat lastContentOffset;
    CGFloat heightForHeaderView;
    CGFloat orginalHeightHeaderView;
    BOOL isHideMenuButtons;
    enum WSTASK currentWSTask;
    NSUInteger session;
    BOOL isTappedClearBtn;
    
}

@property (nonatomic, strong) CategoryItem *currentCategory;
@property (nonatomic, copy) NSString *searchText;
@property (nonatomic, assign) BOOL currentIsOffer;
@property (nonatomic, copy) NSString *currentTitle;;
@property (nonatomic, strong) WSB2CGetQuestions *wsGetQuestion;
@property (nonatomic, strong) WSB2CGetTiles *wsGetTiles;
@property (nonatomic, strong) WSB2CSearch *wsSearch;

@property (nonatomic, assign) BOOL isHasMoreData;
@property (nonatomic, strong) SearchHeaderView *searchView;
@property (nonatomic, strong) TableHeaderView *tableHeaderView;
@end

@implementation ExploreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *tappedOutsideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tappedOutsideKeyboard.cancelsTouchesInView = NO;
    
    [self.view addGestureRecognizer:tappedOutsideKeyboard];
    [self trackingScreenByName:@"Explore"];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setNavigationBarWithDefaultColorAndTitle:self.currentTitle];
    
    if(isTappedClearBtn)
    {
        [self getDataBaseOnCity:self.currentCity];
    }
    isTappedClearBtn = NO;
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    UIPanGestureRecognizer *popNavigationBarRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.revealViewController action:@selector(_handleRevealGesture:)];
    [self.navigationController.navigationBar addGestureRecognizer:popNavigationBarRecognizer];
    orginalHeightHeaderView = self.searchViewOutletHeightConstraint.constant;
    
    if([self.searchView.searchText isFirstResponder])
    {
        [self.searchView.searchText resignFirstResponder];
    }
}


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)dismissKeyboard
{
    if([self.searchView.searchText isFirstResponder])
    {
        [self.searchView.searchText resignFirstResponder];
    }
}

-(void)initView
{
    [self createMenuBarButton];
    self.currentTitle = NSLocalizedString(@"explore_menu_title", nil);
    
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ExploreTableViewCell" bundle:nil] forCellReuseIdentifier:@"ExploreTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ExploreQuestionTableViewCell" bundle:nil] forCellReuseIdentifier:@"ExploreQuestionTableViewCell"];
    
    self.tableView.rowHeight = 120.f*SCREEN_SCALE;
    self.tableView.estimatedRowHeight = 120.f*SCREEN_SCALE;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    AFImageDownloader *downloader = [UIImageView sharedImageDownloader];
    AFImageResponseSerializer* serializer =  (AFImageResponseSerializer *) downloader.sessionManager.responseSerializer;
    serializer.acceptableContentTypes = [serializer.acceptableContentTypes setByAddingObject:@"image/jpg"];
    self.searchViewOutletHeightConstraint.constant = 0.0f;
    
//  Set up menu bar
    [self.cityBtn setImage:[UIImage imageNamed:@"location_white_icon"] forState:UIControlStateNormal];
    [self.cityBtn setImage:[UIImage imageNamed:@"location_interaction_icon"] forState:UIControlStateHighlighted];
    [self.cityBtn setTitle:self.currentCity.name forState:UIControlStateNormal];
    [self.cityBtn centerVertically];
    [self.cityBtn configureDecorationForExploreButton];
    [self.cityBtn addTarget:self action:@selector(selectCityAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.categoryBtn setImage:[UIImage imageNamed:@"category_white_icon"] forState:UIControlStateNormal];
    [self.categoryBtn setImage:[UIImage imageNamed:@"category_interaction_icon"] forState:UIControlStateHighlighted];
    [self.categoryBtn setTitle:self.currentCategory.categoryName forState:UIControlStateNormal];
    [self.categoryBtn centerVertically];
    [self.categoryBtn configureDecorationForExploreButton];
    [self.categoryBtn addTarget:self action:@selector(selectCategoryAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.searchBtn setImage:[UIImage imageNamed:@"search_white_icon"] forState:UIControlStateNormal];
    [self.searchBtn setImage:[UIImage imageNamed:@"search_interaction_icon"] forState:UIControlStateHighlighted];
    [self.searchBtn setTitle:@"Search" forState:UIControlStateNormal];
    self.searchBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.searchBtn.titleLabel.numberOfLines = 2;
    self.searchBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.searchBtn centerVertically];
    [self.searchBtn configureDecorationForExploreButton];
    [self.searchBtn addTarget:self action:@selector(selectSearchAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.firstLine setbackgroundColorForLineMenu];
    [self.secondLine setbackgroundColorForLineMenu];
}

-(void) initData
{
    self.currentCategory = [[CategoryItem alloc] init];
    self.currentCategory.categoryName = @"Category";
    self.currentCategory.code = @"all";
    self.currentCategory.categoriesForService = @[@"Flowers",@"Golf Merchandise",@"Wine",@"VIP Travel Services",@"Tickets",@"Specialty Travel"];
    
    self.exploreLst = [[NSMutableArray alloc] init];
    
    [self startActivityIndicatorWithoutMask];
    [self getDataBaseOnCity:self.currentCity];
}


#pragma mark BUTTON ACTION

-(void)selectCityAction:(id)sender
{
    CityViewController *cityViewController = [[CityViewController alloc] init];
    cityViewController.delegate = self;
    cityViewController.currentCity = self.currentCity;
    
    [self.navigationController pushViewController:cityViewController animated:YES];
}

-(void)selectCategoryAction:(id)sender
{
    CategoryViewController *categoryViewController = [[CategoryViewController alloc] init];
    categoryViewController.delegate = self;
    categoryViewController.currentCity = self.currentCity;
    
    [self.navigationController pushViewController:categoryViewController animated:NO];
}

-(void)selectSearchAction:(id)sender
{
    if([self.searchView.searchText isFirstResponder])
    {
        [self.searchView.searchText resignFirstResponder];
    }
    SearchViewController *searchViewController = [[SearchViewController alloc] init];
    searchViewController.delegate = self;
    searchViewController.currentKeyWord = ![sender isKindOfClass:[UIButton class]] ? self.searchText : @"";
    if(searchViewController.currentKeyWord.length > 0 && self.currentIsOffer)
    {
        searchViewController.currentIsOffer = YES;
    }
    
    [self.navigationController pushViewController:searchViewController animated:YES];
}

-(void)navigateToCategoryCityGuide:(id)sender
{
    if(self.currentCity.subCategories.count > 0)
    {
        CategoryCityGuideViewController *categoryCityGuideViewController = [[CategoryCityGuideViewController alloc] init];
        categoryCityGuideViewController.delegate = self;
        categoryCityGuideViewController.subCategories = self.currentCity.subCategories;
        
        [self.navigationController pushViewController:categoryCityGuideViewController animated:YES];
    }
}


#pragma mark TABLEVIEW DELEGATE

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.exploreLst.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if((currentWSTask == WS_B2C_GET_QUESTIONS && [self.currentCategory.supperCategoryID isEqualToString:@"80"]) || currentWSTask == WS_B2C_SEARCH)
    {
        ExploreQuestionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ExploreQuestionTableViewCell"];
        BaseItem *item = [self.exploreLst objectAtIndex:indexPath.row];
        cell.nameLabel.text = item.name;
        cell.addressLabel.text = item.address;
        cell.descriptionLabel.text = item.offer;
        cell.offerImage.hidden = !item.isOffer;
        return cell;
    }
    else
    {
        ExploreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ExploreTableViewCell"];
        BaseItem *item = [_exploreLst objectAtIndex:indexPath.row];
        cell.nameLbl.text = item.name;
        
        if([[item.categoryCode lowercaseString] isEqualToString:@"dining"]) {
            cell.addressLbl.text = item.address;
            if([self.currentCategory.supperCategoryID isEqualToString:@"80"])
                cell.addressLbl.text = item.address3;
            else {
                NSMutableString* temp = [NSMutableString stringWithString:@""];
                if([@[@"USA",@"Canada"] containsObject:self.currentCity.countryCode]) {
                    if(item.cityName.length > 0)
                    [temp appendFormat:@"%@, ",item.cityName];
                    if(item.state.length > 0)
                    [temp appendFormat:@"%@",item.state];
                    
                } else {
                    if(item.cityName.length > 0)
                    [temp appendFormat:@"%@, ",item.cityName];
                    if(item.countryName.length > 0)
                    [temp appendFormat:@"%@",item.countryName];
                }
                if(temp.length > 0)
                {
                    cell.addressLbl.text = temp;
                    if(temp.length > 2)
                    {
                        NSString *lastChar = [temp substringFromIndex:[temp length] - 2];
                        if([lastChar isEqualToString:@", "]){
                            cell.addressLbl.text = [temp substringToIndex:temp.length - 2];
                        }
                    }
                }
                
            }
            
        } else {
            cell.addressLbl.text = item.address.length > 0?item.address:@" ";
        }
        
        cell.offerLbl.text = [item.offer stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        cell.exploreImg.image = [UIImage imageNamed:@"placehoder_explore_list"];
        if(item.imageURL && item.imageURL.length > 0){
            __weak typeof(cell) weakCell = cell;
            
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[item.imageURL removeBlankFromURL]]] ;
            
            [cell.exploreImg setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"placehoder_explore_detail"] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {

                float scaleFactor = [[UIScreen mainScreen] scale];
                
                dispatch_async(dispatch_get_main_queue() , ^{
                    
                    if(image.size.width * scaleFactor < 100.0f * SCREEN_SCALE * scaleFactor){
                        weakCell.exploreImg.contentMode = UIViewContentModeScaleToFill;
                    }
                    else{
                        weakCell.exploreImg.contentMode = UIViewContentModeScaleAspectFill;
                    }
                });
                /*
                if(image.size.height < 100.0f && SCREEN_SCALE == 1)
                {
                    image = [ImageUtilities imageWithImage:image convertToSize:CGSizeMake(image.size.width * (100.0f/image.size.height), 100.0f * SCREEN_SCALE )];
                }
                 */
                weakCell.exploreImg.image = image;
        
            } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
            }];
            
        }
        else if(item.image){
            cell.exploreImg.image = item.image;
            
        }
        if ([self.currentCategory.code isEqualToString:@"all"]) {
            cell.categoryName.text = [item.categoryCode isEqualToString:@"cityguide"] ? @"City Guide" : item.categoryName;
            cell.maskImageView.hidden = NO;
            if([cell.categoryName.text isEqualToString:@"EXPERIENCES"])
            {
                cell.categoryName.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:13.5 * SCREEN_SCALE];
            }
            else{
                cell.categoryName.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionTextSize]];
            }
        }
        else{
            cell.categoryName.text = @"";
            cell.maskImageView.hidden = YES;
        }
        
        [cell.contentView layoutIfNeeded];
        cell.starImage.hidden = !item.isOffer;

        return cell;
    }
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BaseItem *item = [_exploreLst objectAtIndex:indexPath.row];
    NewExploreDetailViewController *vc = [[NewExploreDetailViewController alloc] init];
    vc.item = item;
    vc.cityName = self.currentCity.name;
    [self.navigationController pushViewController:vc animated:YES];
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(currentWSTask == WS_B2C_SEARCH && self.wsSearch.hasNextItem)
    {
        if([self isLastRowVisible])
        {
            [self loadMoreDataForSearch];
            NSInteger lastRowIndex = [tableView numberOfRowsInSection:0] - 1;
            if ((indexPath.row == lastRowIndex))
            {
                [self loadMoreIndicatorIcon:isLoading];
            }
        }
        else
        {
            NSInteger lastRowIndex = [tableView numberOfRowsInSection:0] - 3;
            if ((indexPath.row == lastRowIndex)) {
                
                [self loadMoreDataForSearch];
            }
            
            lastRowIndex = [tableView numberOfRowsInSection:0] - 1;
            if ((indexPath.row == lastRowIndex))
            {
                [self loadMoreIndicatorIcon:isLoading];
            }
        }
        
    }
    else if(self.wsGetQuestion.hasNextItem)
    {
        NSInteger lastRowIndex = [tableView numberOfRowsInSection:0] - 3;
        if ((indexPath.row == lastRowIndex)) {
            
            if(isLoading)
            {
                return;
            }
            isLoading = YES;
            self.wsGetQuestion.session = session;
            self.wsGetQuestion.pageIndex += 1;
            [self.wsGetQuestion loadQuestionsForCategory];
        }
        
        // Only show loading icon when last row is showing
        lastRowIndex = [tableView numberOfRowsInSection:0] - 1;
        if ((indexPath.row == lastRowIndex))
        {
            [self loadMoreIndicatorIcon:isLoading];
        }
    }
    
}


-(BOOL)isLastRowVisible {
    NSArray *indexes = [self.tableView indexPathsForVisibleRows];
    // ignore this case
    if(indexes.count < 3)
        return YES;
    
    for (NSIndexPath *index in indexes) {
        if (index.row == self.exploreLst.count - 1) {
            return YES;
        }
    }
    
    return NO;
}

-(void)loadMoreDataForSearch
{
    if(isLoading)
    {
        return;
    }
    isLoading = YES;
    self.wsSearch.session = session;
    self.wsSearch.pageIndex += 1;
    [self.wsSearch retrieveDataFromServer];
}


-(void)loadMoreIndicatorIcon:(BOOL)loading
{
    if(loading)
    {
        [self.tableView startLoadMoreIndicator];
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 30, 0);
        [UIView commitAnimations];;
    }
}


#pragma mark DATALOADER DELEGATE
-(void)loadDataDoneFrom:(WSBase *)ws{
    
    [self stopActivityIndicatorWithoutMask];
    isLoading = NO;
    [self.tableView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    [self.tableView stopLoadMoreIndicator];
    
    if(ws.session != session)
    {
        return;
    }
    
    currentWSTask = ws.task;
    
    if(ws.task == WS_B2C_GET_TILES)
    {
        [self.exploreLst addObjectsFromArray:ws.data];
    }
    else if(ws.task == WS_B2C_GET_QUESTIONS)
    {
        NSArray *answerItems = [ws.data valueForKeyPath:@"answers"];
        for (NSArray *arr in answerItems )
        {
            [self addObjectToExploreListWithData:arr withWS:ws];
        }
    }
    else{
        if(currentWSTask == WS_B2C_SEARCH && ws.data.count == 0 && self.exploreLst.count == 0)
        {
            self.tableHeaderView = [self.tableView.tableHeaderView viewWithTag:3001];
            if(!self.tableHeaderView)
            {
                self.tableHeaderView = [[[NSBundle mainBundle] loadNibNamed:@"TableHeaderView" owner:self options:nil] objectAtIndex:0];
                self.tableHeaderView.tag = 3001;
                
                self.tableHeaderView.frame = self.tableView.bounds;
                [self.tableHeaderView setupView];
                [self.tableHeaderView.askConciergeBtn addTarget:self action:@selector(askConciergeAction:) forControlEvents:UIControlEventTouchUpInside];
            }
            [self.tableView setTableHeaderView:self.tableHeaderView];
        }
        else{
            self.tableHeaderView = [self.tableView.tableHeaderView viewWithTag:3001];
            if(self.tableHeaderView)
            {
                [self.tableView setTableHeaderView:nil];
            }
            
            [self addObjectToExploreListWithData:ws.data withWS:ws];
        }
    }
    
    
     NSArray *sortedArray = [self.exploreLst sortedArrayUsingComparator:^NSComparisonResult(BaseItem *obj1, BaseItem *obj2){ return [[obj1.name lowercaseString] compare:[obj2.name lowercaseString] options:NSLiteralSearch];}];
    
    NSNumber *exploreSession = (NSNumber  *)objc_getAssociatedObject(self.exploreLst, @"Session");
    if([exploreSession integerValue] == session)
    {
        self.exploreLst = [sortedArray mutableCopy];
    }
    else {
        [self.exploreLst removeAllObjects];
        self.exploreLst = [sortedArray mutableCopy];
    }
    
    [self.tableView reloadData];
}

-(void)addObjectToExploreListWithData:(NSArray *)arr withWS:(WSBase *)ws
{
    if(ws.session == session)
    {
        [self.exploreLst addObjectsFromArray:arr];
    }
}


-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode
{
    
    if(ISLOGINGINFO){
        NSError* err = [NSError errorWithDomain:@"Error" code:errorCode userInfo:nil];
        NSLog(@"ERROR:%@", err.userInfo);
    }
    
    [self stopActivityIndicatorWithoutMask];
    isLoading = NO;
    [self.tableView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    [self.tableView stopLoadMoreIndicator];
    [self showAPIErrorAlertWithMessage:NSLocalizedString(@"error_api_message", nil) andTitle:NSLocalizedString(@"cannot_get_data", nil)];
}

-(void)loadDataFailFrom:(id<WSBaseProtocol>)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message
{
    [self stopActivityIndicatorWithoutMask];
    isLoading = NO;
    [self.tableView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    [self.tableView stopLoadMoreIndicator];
    
    if(![message isEqualToString:@"cancelled"])
    {
        [self showAPIErrorAlertWithMessage:message andTitle:NSLocalizedString(@"alert_error_title", nil)];
    }
}

#pragma mark SELECTING PROCESS
-(void) getDataBaseOnCity:(CityItem *)item
{
    //new session
    session += 1;
    
    //reset table view
    [self.exploreLst removeAllObjects];
    [self.tableView reloadData];
    
    [self.searchView removeFromSuperview];
    self.searchViewOutletHeightConstraint.constant = 0.01f;
    [self.tableView setTableHeaderView:nil];
    [self.view layoutIfNeeded];
    self.currentCity = item;
    [self.cityBtn setTitle:item.name forState:UIControlStateNormal];
    [self.cityBtn centerVertically];
    
    //Save user city selection
    [[AppData getInstance] setCurrentCityWithCode:item.name];
    
    if([self.currentCity.cityCode isEqualToString:@"all"])
    {
        self.exploreLst = [NSMutableArray arrayWithArray:[[AppData getInstance] getDefaultAnswers]];
        [self.tableView reloadData];
    }
    else
    {
        if([self.currentCategory.code isEqualToString:@"all"])
        {
            [self getDataForAllCategory];
        }
        else if([self.currentCategory.supperCategoryID isEqualToString:@"80"] && self.currentCity.subCategories.count > 0)
        {
            //Update current category based on City Selection
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"code == %@", self.currentCategory.code];
            NSArray *newCategoryItem = [self.currentCity.subCategories filteredArrayUsingPredicate:predicate];
            if(newCategoryItem.count > 0)
            {
                self.currentCategory = [newCategoryItem objectAtIndex:0];
                
                [self startActivityIndicatorWithoutMask];
                
                self.wsGetQuestion = [[WSB2CGetQuestions alloc] init];
                self.wsGetQuestion.delegate = self;
                self.wsGetQuestion.task = WS_B2C_GET_QUESTIONS;
                self.wsGetQuestion.session = session;
                self.wsGetQuestion.categoryId = self.currentCategory.ID;
                self.wsGetQuestion.categoryCode = @"cityguide";
                self.wsGetQuestion.categoryName = self.currentCategory.categoryName;
                [self.wsGetQuestion loadQuestionsForCategory];
            }
        }
        else if(![self.currentCategory.supperCategoryID isEqualToString:@"80"] &&([self.currentCategory.code isEqualToString:@"dining"] && self.currentCity.diningID))
        {
            [self startActivityIndicatorWithoutMask];
          
            self.wsGetQuestion = [[WSB2CGetQuestions alloc] init];
            self.wsGetQuestion.delegate = self;
            self.wsGetQuestion.task = WS_B2C_GET_QUESTIONS;
            self.wsGetQuestion.session = session;
            self.wsGetQuestion.categoryCode = @"dining";
            self.wsGetQuestion.categoryName = @"DINING";
            self.wsGetQuestion.categoryId = self.currentCity.diningID;
            [self.wsGetQuestion loadQuestionsForCategory];
        }
        
        else if(self.currentCategory.categoriesForService)
        {
            [self startActivityIndicatorWithoutMask];
            [self invokeGetTileAPIWithData:nil withSearchText:nil withOffer:NO];
        } else if([self.currentCategory.supperCategoryID isEqualToString:@"80"] && self.currentCity.subCategories.count == 0) {
            self.tableHeaderView = [self.tableView.tableHeaderView viewWithTag:3001];
            if(!self.tableHeaderView)
            {
                self.tableHeaderView = [[[NSBundle mainBundle] loadNibNamed:@"TableHeaderView" owner:self options:nil] objectAtIndex:0];
                self.tableHeaderView.tag = 3001;
                
                self.tableHeaderView.frame = self.tableView.bounds;
                [self.tableHeaderView setupViewWithMessage:NSLocalizedString(@"no_result_searching_msg", nil)];
                [self.tableHeaderView.askConciergeBtn addTarget:self action:@selector(askConciergeAction:) forControlEvents:UIControlEventTouchUpInside];
            }
            [self.tableView setTableHeaderView:self.tableHeaderView];
        }
    }
}

-(void) getDataBaseOnCategory:(CategoryItem *)item
{
    //New session
    session += 1;
    
    //reset table view
    [self.exploreLst removeAllObjects];
    [self.tableView reloadData];
    
    [self.searchView removeFromSuperview];
    self.searchViewOutletHeightConstraint.constant = 0.01f;
    [self.tableView setTableHeaderView:nil];
    [self.view layoutIfNeeded];
    self.currentCategory = item;
    
    //Update UI
    [self updateUIBasedOnSelection:item];
    
    //Integration Instance Answers API
    if([item.supperCategoryID isEqualToString:@"80"] || ([item.code isEqualToString:@"dining"] && self.currentCity.diningID.length > 0))
    {
        // Get Questions
        [self startActivityIndicatorWithoutMask];
        
        self.wsGetQuestion = [[WSB2CGetQuestions alloc] init];
        self.wsGetQuestion.delegate = self;
        self.wsGetQuestion.task = WS_B2C_GET_QUESTIONS;
        self.wsGetQuestion.session = session;
        self.wsGetQuestion.categoryId = item.ID;
        
        if([item.supperCategoryID isEqualToString:@"80"])
        {
            self.wsGetQuestion.categoryCode = @"cityguide";
            self.wsGetQuestion.categoryName = item.categoryName;
            //track event for GA
            NSString *eventName = [NSString stringWithFormat:@"City Guide - %@",[item.categoryName capitalizedString]];
            [self trackingEventByName:eventName withAction:SelectActionType withCategory:CategorySelectionCategoryType];
        }
        else if([item.code isEqualToString:@"dining"])
        {
            self.wsGetQuestion.categoryCode = item.code;
            self.wsGetQuestion.categoryName = item.categoryName;
        }
        
        [self.wsGetQuestion loadQuestionsForCategory];
    }
    
    else if([item.code isEqualToString:@"all"])
    {
        [self getDataForAllCategory];
    }
    //Integration CCA API
    else if(item.categoriesForService)
    {
        
        [self startActivityIndicatorWithoutMask];
        [self invokeGetTileAPIWithData:nil withSearchText:nil withOffer:NO];
    }
}

-(void)getDataForAllCategory
{
    [self startActivityIndicatorWithoutMask];
    [self invokeGetTileAPIWithData:nil withSearchText:nil withOffer:NO];
    
    
    if(self.currentCity.diningID.length > 0)
    {
        
        self.wsGetQuestion = [[WSB2CGetQuestions alloc] init];
        self.wsGetQuestion.delegate = self;
        self.wsGetQuestion.task = WS_B2C_GET_QUESTIONS;
        self.wsGetQuestion.session = session;
        
        self.wsGetQuestion.categoryId = self.currentCity.diningID;
        self.wsGetQuestion.categoryCode = @"dining";
        self.wsGetQuestion.categoryName = @"DINING";
        self.wsGetQuestion.pageSize = 20;
        self.wsGetQuestion.pageIndex = 1;
        [self.wsGetQuestion loadQuestionsForCategory];
    }
    
    
    /* Get City Guide Data
    if(self.currentCity.subCategories.count > 0)
    {
        for(CategoryItem *subCategory in self.currentCity.subCategories)
        {
            WSB2CGetQuestions *wsGetQuestion = [[WSB2CGetQuestions alloc] init];
            wsGetQuestion.delegate = self;
            wsGetQuestion.task = WS_B2C_GET_QUESTIONS;
            wsGetQuestion.session = session;
            wsGetQuestion.categoryId = subCategory.ID;
            wsGetQuestion.categoryCode = @"cityguide";
            wsGetQuestion.categoryName = subCategory.categoryName;
            [wsGetQuestion loadQuestionsForCategory];
        }
    }
     */
}

-(void)invokeGetTileAPIWithData:(NSArray *)categories withSearchText:(NSString *)searchText withOffer:(BOOL)hasOffer;
{
    //prevent the questions from more loading
    if(![self.currentCategory.code isEqualToString:@"all"] && self.wsGetQuestion)
    {
        self.wsGetQuestion = nil;
    }
    
    self.wsGetTiles = [[WSB2CGetTiles alloc] init];
    self.wsGetTiles.delegate = self;
    self.wsGetTiles.task = searchText ? WS_B2C_SEARCH : WS_B2C_GET_TILES;
    self.wsGetTiles.session = session;
    self.wsGetTiles.categories = categories ? categories : self.currentCategory.categoriesForService;
    self.wsGetTiles.categoryCode = self.currentCategory.code;
    self.wsGetTiles.categoryName = self.currentCategory.categoryName;
    self.wsGetTiles.currentCategory = self.currentCategory;
    self.wsGetTiles.searchText = searchText;
    self.wsGetTiles.hasOffer = hasOffer;
    [self.wsGetTiles loadTilesForCategory];
}


-(void)updateUIBasedOnSelection:(CategoryItem*)item
{
    if([item.supperCategoryID isEqualToString:@"80"] )
    {
        self.currentTitle = item.categoryName;
        [self.searchBtn setTitle:@"Discover\nMore" forState:UIControlStateNormal];
        [self.searchBtn setImage:[self createDummyImage] forState:UIControlStateNormal];
        [self.searchBtn setImage:[self createDummyImage] forState:UIControlStateHighlighted];
        [self.searchBtn centerVertically];
        [self.searchBtn removeTarget:self action:@selector(selectSearchAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.searchBtn addTarget:self action:@selector(navigateToCategoryCityGuide:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.categoryBtn setTitle:@"City Guide" forState:UIControlStateNormal];
        [self.categoryBtn centerVertically];
    }
    
    else
    {
        self.currentTitle = NSLocalizedString(@"explore_menu_title", nil);
        
        [self.searchBtn setImage:[UIImage imageNamed:@"search_white_icon"] forState:UIControlStateNormal];
        [self.searchBtn setImage:[UIImage imageNamed:@"search_interaction_icon"] forState:UIControlStateHighlighted];
        [self.searchBtn setTitle:@"Search" forState:UIControlStateNormal];
        [self.searchBtn centerVertically];
        
        [self.searchBtn removeTarget:self action:@selector(navigateToCategoryCityGuide:) forControlEvents:UIControlEventTouchUpInside];
        [self.searchBtn addTarget:self action:@selector(selectSearchAction:) forControlEvents:UIControlEventTouchUpInside];
        
        if([item.code isEqualToString:@"all"])
        {
            [self.categoryBtn setTitle:@"All" forState:UIControlStateNormal];
        }
        else{
            NSString *newName = [self.currentCategory.categoryName stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            [self.categoryBtn setTitle:[newName capitalizedString] forState:UIControlStateNormal];
        }
        [self.categoryBtn centerVertically];
    }
}
- (UIImage *)createDummyImage
{
    UILabel *imageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 72.0, 21.0)];
    UIGraphicsBeginImageContextWithOptions(imageLabel.bounds.size, NO, 0);
    
    if ([imageLabel respondsToSelector:@selector(drawViewHierarchyInRect:afterScreenUpdates:)])
        [imageLabel drawViewHierarchyInRect:imageLabel.bounds afterScreenUpdates:YES];  // if we have efficient iOS 7 method, use it ...
    else
        [imageLabel.layer renderInContext:UIGraphicsGetCurrentContext()];         // ... otherwise, fall back to tried and true methods
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

-(void)getDataBaseOnSearchText:(NSString *)text withOffer:(BOOL) isOffer withBookOnline:(BOOL)isBookOnline
{
    isTappedClearBtn = NO; //New searching is performed
    [self setupSearchHeaderViewWithText:text withOffer:isOffer withBookOnline:isBookOnline];
    [self searchWithText:self.searchText withHasOffer:isOffer];
    
}


-(void) setupSearchHeaderViewWithText:(NSString *)text withOffer:(BOOL) isOffer withBookOnline:(BOOL)isBookOnline
{
    /*
    self.searchView = [self.tableView viewWithTag:3000];
    if(!self.searchView)
    {
        self.searchView = [[[NSBundle mainBundle] loadNibNamed:@"SearchHeaderView" owner:self options:nil] objectAtIndex:0];
        self.searchView.tag = 3000;
        [self.searchViewOutlet addSubview:self.searchView];
    }
     */
    if(self.searchView)
    {
        [self.searchView removeFromSuperview];
        self.searchView = nil;
    }
    self.searchView = [[[NSBundle mainBundle] loadNibNamed:@"SearchHeaderView" owner:self options:nil] objectAtIndex:0];
    self.searchView.tag = 3000;
    [self.searchViewOutlet addSubview:self.searchView];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] init];
        [str appendAttributedString:[[NSAttributedString alloc] initWithString:@"Search results for " attributes:@{ NSFontAttributeName : [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]], NSForegroundColorAttributeName : [AppColor textColor]}]];
        [str appendAttributedString:[[NSAttributedString alloc] initWithString:text.length == 0 ? @"offers" : text attributes:@{NSForegroundColorAttributeName : [AppColor textColor], NSFontAttributeName :[UIFont fontWithName:FONT_MarkForMC_BOLD size:[AppSize titleTextSize]]}]];
        self.searchView.searchText.attributedText = str;
    });
    
    if(isOffer)
    {
        [self.searchView.offerClear addTarget:self action:@selector(searchWithoutOffers) forControlEvents:UIControlEventTouchUpInside];
    }
    
    
    self.searchView.searchText.delegate = self;
    
    self.searchView.isSearchOffer = isOffer;
    self.searchView.isSearchBookOnline = isBookOnline;
    self.searchText = text;
    self.currentIsOffer = isOffer;
    [self.searchView setupView];
    heightForHeaderView = [self.searchView calculateHeightView];
    self.searchView.frame = CGRectMake(0.f, 0.f, SCREEN_WIDTH, heightForHeaderView);
    [self.searchView setBottomLine];
    
    self.searchViewOutletHeightConstraint.constant = heightForHeaderView;
}


-(void)searchWithText:(NSString *)text withHasOffer:(BOOL)isOffer
{
    //new session
    session += 1;
    
    //reset table view
    [self.exploreLst removeAllObjects];
    [self.tableView reloadData];
    [self.tableView setTableHeaderView:nil];
    
    //clear to prevent load more
    self.wsGetQuestion = nil;
    self.wsGetTiles = nil;
    self.wsSearch = nil;
    
    [self startActivityIndicator];
    
    // Search CCA Item with call CCA API
    if(![self.currentCategory.code isEqualToString:@"all"] && self.currentCategory.categoriesForService.count > 0)
    {
        [self invokeGetTileAPIWithData:nil withSearchText:self.searchText withOffer:isOffer];
    }
    // Call search API for data that not retrieved from CCA api
    else
    {
        self.wsSearch = [[WSB2CSearch alloc] init];
        self.wsSearch.session = session;
        self.wsSearch.delegate = self;
        self.wsSearch.task = WS_B2C_SEARCH;
        self.wsSearch.searchText = text;
        self.wsSearch.hasOffer = isOffer;
        self.wsSearch.cities = ![self.currentCity.cityCode isEqualToString:@"all"] ? @[self.currentCity.name] : nil;
        self.wsSearch.categoryCode = self.currentCategory.code;
        self.wsSearch.categoryName = self.currentCategory.categoryName;
        self.wsSearch.currentCity = self.currentCity;
        self.wsSearch.currentCategory = self.currentCategory;
        [self.wsSearch retrieveDataFromServer];
    }
}

#pragma mark TEXT FIELD DELEGATE
-(void)searchWithoutOffers
{
    self.currentIsOffer = NO;
    if(self.searchText.length == 0){
        [self.wsSearch cancelRequest];
        self.searchText = @"";
        self.wsSearch = nil;
        [self.exploreLst removeAllObjects];
        [self.tableView reloadData];
        [self selectSearchAction:nil];
    }
    else{
//        self.searchView.isSearchOffer = NO;
//        [self.searchView setupView];
//        heightForHeaderView = [self.searchView calculateHeightView];
//        self.searchView.frame = CGRectMake(0.f, 0.f, SCREEN_WIDTH, heightForHeaderView);
//        [self.searchView setBottomLine];
//        
//        self.searchViewOutletHeightConstraint.constant = heightForHeaderView;
        [self setupSearchHeaderViewWithText:self.searchText withOffer:NO withBookOnline:NO];
        [self searchWithText:self.searchText withHasOffer:NO];
    }
}
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    isTappedClearBtn = YES;
    [self.wsSearch cancelRequest];
    
    textField.text = @"";
    self.searchText = @"";
    [self.searchView removeFromSuperview];
    self.searchViewOutletHeightConstraint.constant = 0.01f;
    self.wsSearch = nil;
    [self.exploreLst removeAllObjects];
    [self.tableView reloadData];
    [self selectSearchAction:nil];
    
    return NO;
}

/*
 - (BOOL)textFieldShouldReturn:(UITextField *)textField
 {
    [self startActivityIndicator];
 
    [textField resignFirstResponder];
 
    [self searchWithText:[textField.text copy] withHasOffer:self.searchView.isSearchOffer];
 
    textField.text = [NSString stringWithFormat:@"Search results for %@",textField.text];
    return YES;
 }
*/
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self selectSearchAction:nil];
    return NO;
}

#pragma mark - SCROLLVIEW DELEGATE
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.tableView)
    {
        if (isLoading) {
            return;
        }
        
        if (scrollView.contentOffset.y < 0) {
            /*
            if (scrollView.contentOffset.y < - (TABLE_PULL_HEIGHT + TABLE_PULL_LENGTH)) {
                [self.tableView setRefreshText:NSLocalizedString(@"TABLE_REFRESH_RELEASE_TEXT", @"") isDown:NO];
            } else {
                [self.tableView setRefreshText:NSLocalizedString(@"TABLE_REFRESH_PULL_TEXT", @"") isDown:YES];
            }
            */
        }
        else{
            if([self isLastRowVisible])
                return;
            
            [super scrollViewDidScroll:scrollView];
        }
    }
}

-(void)changePositionForView:(BOOL)isHide{
    /*
    if(isHide)
    {
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.menuButtonsTopConstraint.constant = -100.f * SCREEN_SCALE;
            [self.view layoutIfNeeded];
        } completion:nil];
    }
    else{
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.menuButtonsTopConstraint.constant = 0.0f;
            [self.view layoutIfNeeded];
        } completion:nil];
    }
     */
}

-(void)loadDataForScreen:(BOOL)isRefresh
{
    [self.tableView stopRefreshIndicator];
    [self.tableView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    isLoading = NO;
}

- (void)refreshContentForTableView
{
    isLoading = YES;
    [self.tableView startRefreshIndicator];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    self.tableView.contentInset = UIEdgeInsetsMake(30, 0, 0, 0);
    self.tableView.contentOffset = CGPointMake(0, 0 - self.tableView.contentInset.top);
    [UIView commitAnimations];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^()
                   {
                       //Call service
                   });
    
}


@end
