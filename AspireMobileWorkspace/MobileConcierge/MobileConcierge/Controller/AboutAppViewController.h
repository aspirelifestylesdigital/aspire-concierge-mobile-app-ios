//
//  AboutAppViewController.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/16/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "PrivacyPolicyViewController.h"

@interface AboutAppViewController : PrivacyPolicyViewController

@end
