//
//  AskConciergeConfirmationViewController.m
//  MobileConcierge
//
//  Created by Mac OS on 5/29/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "AskConciergeConfirmationViewController.h"
#import "UIButton+Extension.h"
#import "Constant.h"
#import "HomeViewController.h"
#import "SWRevealViewController.h"
#import "ExploreViewController.h"
#import "AppData.h"
#import "CityViewController.h"
#import "Common.h"


@interface AskConciergeConfirmationViewController ()

@end

@implementation AskConciergeConfirmationViewController

- (void)viewDidLoad {
    isNotAskConciergeBarButton = YES;
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   
}

- (void)viewDidAppear:(BOOL)animated{
     [self setUpCustomizedPanGesturePopRecognizer];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}

-(void)initView{
    
    
    
    NSDictionary *attributes = @{NSForegroundColorAttributeName:[AppColor backgroundColor],
                                 
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_BOLD size:([AppSize titleTextSize])],
                                 
                                 NSKernAttributeName: @1.8};
    
    NSDictionary *attributesSelected = @{NSForegroundColorAttributeName:
                                             [AppColor activeButtonColor],
                                         
                                         NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_BOLD size:([AppSize titleTextSize])],
                                         
                                         NSKernAttributeName: @1.8};
    
    [self.btnExplore setAttributedTitle:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"continue_explore", nil) attributes:attributes]  forState:UIControlStateNormal];
    [self.btnExplore setAttributedTitle:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"continue_explore", nil) attributes:attributesSelected]  forState:UIControlStateHighlighted];
    
    
    [self.btnAnotherRequest setAttributedTitle:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"place_another_request", nil) attributes:attributes]  forState:UIControlStateNormal];
    [self.btnAnotherRequest setAttributedTitle:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"place_another_request", nil) attributes:attributesSelected]  forState:UIControlStateHighlighted];
    
    [self.btnExplore addTarget:self action:@selector(touchExplore:) forControlEvents:UIControlEventTouchUpOutside];
    [self.btnAnotherRequest addTarget:self action:@selector(touchAnotherRequest:) forControlEvents:UIControlEventTouchUpOutside];
    
    self.topConstraintHeaderView.constant = self.topConstraintHeaderView.constant*SCREEN_SCALE;
    self.topConstraintThanksView.constant = self.topConstraintThanksView.constant*SCREEN_SCALE;
    
    self.bottomConstraintExploreView.constant = self.bottomConstraintExploreView.constant*SCREEN_SCALE;
    self.heightConstraintHeaderView.constant = self.heightConstraintHeaderView.constant*SCREEN_SCALE;
    
    [self createBackBarButton];
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"request_confirmation_title",nil)];
    
    
    NSString *greetMsg = NSLocalizedString(@"request_confirmation_message", nil);
    NSRange range = NSMakeRange(0, greetMsg.length);
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:greetMsg];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionGreetingSize]] range:range];
    [attributeString addAttribute:NSForegroundColorAttributeName value:[AppColor textColor] range:range];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init] ;
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    paragraphStyle.lineSpacing = [AppSize descriptionLetterSpacing];
    [attributeString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    
    self.lblMessage.attributedText = attributeString;
}

- (void)touchBack{
//     [self.navigationController popViewControllerAnimated:YES];
    UIViewController *View = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-3];
    [self.navigationController popToViewController:View animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)touchAnotherRequest:(id)sender {
    self.viewOverlay.alpha = 0.0f;
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)touchExplore:(id)sender {
    self.viewOverlay.alpha = 0.0f;
    UIViewController *newFrontController = nil;
    CityItem *selectedCity = [[AppData getInstance] getSelectedCity];
    if(selectedCity)
    {
        ExploreViewController *exploreViewController = [[ExploreViewController alloc] init];
        exploreViewController.currentCity = selectedCity;
        newFrontController = exploreViewController;
        
    }
    else
    {
        newFrontController = [[CityViewController alloc] init];
    }
    [self.navigationController pushViewController:newFrontController animated:YES];
}
@end
