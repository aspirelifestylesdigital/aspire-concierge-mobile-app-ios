//
//  AskConciergeViewController.m
//  MobileConcierge
//
//  Created by user on 5/9/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "AskConciergeViewController.h"
#import "Constant.h"
#import "UIButton+Extension.h"
#import "Constant.h"
#import "Common.h"
#import "WSB2CCreateConciergeCase.h"
#import "SWRevealViewController.h"
#import "HomeViewController.h"
#import "AskConciergeConfirmationViewController.h"
#import<CoreTelephony/CTTelephonyNetworkInfo.h>
#import<CoreTelephony/CTCarrier.h>
//#import <AspireApiFramework/AspireApiFramework.h>
@import AspireApiFramework;


@interface AskConciergeViewController ()<UITextViewDelegate, DataLoadDelegate>

@end

@implementation AskConciergeViewController
{
    UILabel *lblPlaceholder;
    float backupHeightContentView;
    
    BOOL checkPhone;
    BOOL checkMail;
    BOOL viewControllerIsPopped;
    
    NSInteger timeToChange;
}

- (void)viewDidLoad {
    isNotAskConciergeBarButton = YES;
    [super viewDidLoad];
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"ask_concierge_title", nil)];
    [self createBackBarButton];
    [self trackingScreenByName:@"Ask concierge"];
}


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self removeSetupForCustomizedPanGesturePopRecognizer];
    
    [self dismissKeyboard];
    
}


- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setUpCustomizedPanGesturePopRecognizer];
    if (viewControllerIsPopped == YES)
    {
        [self.tvAskConcierge becomeFirstResponder];
    }
}


- (void)touchBack{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.msgAlert = NSLocalizedString(@"askconcierge_back_confirm", nil);
    alert.firstBtnTitle = NSLocalizedString(@"no_button", nil).uppercaseString;
    alert.secondBtnTitle = NSLocalizedString(@"yes_button", nil).uppercaseString;
    alert.blockSecondBtnAction = ^(void){
        [self.navigationController popViewControllerAnimated:YES];
    };
    [self showAlert:alert forNavigation:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initView{
    [self scaleConstraintForSubView];
    [self.viewAccessories setBackgroundColor:[AppColor backgroundColor]];
    [self.btnCallConcierge configureDecorationBlackTitleButtonForTouchingStatus];
    [self.btnCallConcierge configureDecorationBlackTitleButton];
    [self.btnCallConcierge setTitle:CONCIERGE_PHONENUMBER forState:UIControlStateNormal];
    [self.btnCallConcierge addTarget:self action:@selector(callConcierge:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.btnInternationalCall addTarget:self action:@selector(callInternational:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.btnAskConcierge configureDecorationBlackTitleButtonForTouchingStatus];
    [self.btnAskConcierge setImage:[UIImage imageNamed:@"send_filled_icon"] forState:UIControlStateNormal];
    [self.btnAskConcierge setImage:[UIImage imageNamed:@"send_interact_icon"] forState:UIControlStateHighlighted];
    [self addAskConciergeTextViewPlaceHolder];
    if (self.itemName) {
        self.tvAskConcierge.text = [NSString stringWithFormat:@"%@\n",self.itemName];
        lblPlaceholder.hidden = YES;
        self.btnAskConcierge.hidden = NO;
        [self showAccessoryView];
    }else{
        self.btnAskConcierge.hidden = YES;
        [self hideAccessoryView];
    }
    
    UITapGestureRecognizer *dismiss = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:dismiss];
    
    self.lblMessage.attributedText = letterSpacing([AppSize titleLetterSpacing], NSLocalizedString(@"call_the_concierge", nil));
    
    UIFont *font = [UIFont fontWithName:FONT_MarkForMC_BOLD size:[AppSize descriptionTextSize]];
    [self.lblRespondToMe setFont:font];
    self.lblRespondToMe.attributedText = letterSpacing([AppSize titleLetterSpacing], NSLocalizedString(@"respond_to_me", nil));
    
    self.tvAskConcierge.delegate = self;
    
    
    backupHeightContentView = self.heightConstraintAskConciergeView.constant;
    
//    [self setCheckButtonsStateWithPhone:NO andEmail:NO];
   
        [self.btnCheckPhone setAttributedTitle:[self stringWithImageName:@"checkbox_uncheck" andString:NSLocalizedString(@"checkbox_phone", nil)] forState:UIControlStateNormal];
  
        [self.btnCheckPhone setAttributedTitle:[self stringWithImageName:@"checkbox_check" andString:NSLocalizedString(@"checkbox_phone", nil)] forState:UIControlStateSelected];

    
 
        [self.btnCheckMail setAttributedTitle:[self stringWithImageName:@"checkbox_uncheck" andString:NSLocalizedString(@"checkbox_email", nil)] forState:UIControlStateNormal];

        [self.btnCheckMail setAttributedTitle:[self stringWithImageName:@"checkbox_check" andString:NSLocalizedString(@"checkbox_email", nil)] forState:UIControlStateSelected];
    
    if ([self.tvAskConcierge hasText]) {
        
    }

}

- (void)callConcierge:(UIButton *)btn{
    self.btnCallConcierge.enabled = false;
    [NSTimer scheduledTimerWithTimeInterval:1.0f
                                     target:self
                                   selector:@selector(timerCheck:)
                                   userInfo:nil
                                    repeats:YES];
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        self.btnCallConcierge.enabled = true;
//    });
    NSString *phone_number = CONCIERGE_PHONE_STRING;
    [self callWithPhoneString:phone_number];
}



- (void)callInternational:(id)sender {
    self.btnInternationalCall.enabled = false;
    [NSTimer scheduledTimerWithTimeInterval:1.0f
                                     target:self
                                   selector:@selector(timerCheck:)
                                   userInfo:nil
                                    repeats:YES];
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        self.btnInternationalCall.enabled = true;
//    });
    
    NSString *phone_number = CONCIERGE_INTERNATIONAL_PHONE_STRING;
    [self callWithPhoneString:phone_number];
}

- (void)timerCheck:(NSTimer*)timer{
    timeToChange += 1;
    if (timeToChange == 2) {
        self.btnCallConcierge.enabled = true;
        self.btnInternationalCall.enabled = true;
        
        [timer invalidate];
        timeToChange = 0;
    }
}


- (void) callWithPhoneString:(NSString*)phoneString{
    CTTelephonyNetworkInfo* info = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier* carrier = info.subscriberCellularProvider;
    NSString *isoCountryCode = carrier.isoCountryCode;
    
    if (!isoCountryCode && iosVersion() < 10.0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:phoneString message:@"" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *call = [UIAlertAction actionWithTitle:NSLocalizedString(@"call_alert_button", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSString *phoneStr = [[NSString alloc] initWithFormat:@"tel:%@",phoneString];
            NSURL *phoneURL = [[NSURL alloc] initWithString:phoneStr];
            [[UIApplication sharedApplication] openURL:phoneURL];
        }];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"cancel_button_lowcase", nil) style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:cancel];
        [alert addAction:call];

        [self presentViewController:alert animated:YES completion:nil];
        
    }else{
        NSString *phoneStr = [[NSString alloc] initWithFormat:@"tel:%@",phoneString];
        NSURL *phoneURL = [[NSURL alloc] initWithString:phoneStr];
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
    
}

- (void)initData{
    
}


- (void)scaleConstraintForSubView{
    self.topConstraintAskConciergeView.constant = self.topConstraintAskConciergeView.constant*SCREEN_SCALE;
    self.topMessageLabel.constant = self.topMessageLabel.constant*SCREEN_SCALE;
    self.topConstrainAskConciergeTextView.constant = self.topConstrainAskConciergeTextView.constant*SCREEN_SCALE;
    self.heightConstraintAskConciergeView.constant = self.heightConstraintAskConciergeView.constant*SCREEN_SCALE;
    self.heightConstraintViewCallConcierge.constant = self.heightConstraintViewCallConcierge.constant*SCREEN_SCALE;
    
    self.leadConstraintAskConciergeTextView.constant = self.leadConstraintAskConciergeTextView.constant*SCREEN_SCALE;
    self.trailConstraintAskConciergeTextView.constant = self.trailConstraintAskConciergeTextView.constant*SCREEN_SCALE;
}

-(void)addAskConciergeTextViewPlaceHolder{
    lblPlaceholder = [[UILabel alloc] initWithFrame:CGRectMake(5.0, 2.0,self.tvAskConcierge.frame.size.width - 10.0, 34.0)];
    
    [lblPlaceholder setText:NSLocalizedString(@"ask_concierge_placeholder", nil)];
    [lblPlaceholder setBackgroundColor:[UIColor clearColor]];
    [lblPlaceholder setTextColor:[AppColor moreInfoTextColor]];
    lblPlaceholder.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR_It size:[AppSize largeDescriptionTextSize]];
    
    [self.tvAskConcierge addSubview:lblPlaceholder];
}


- (void) showAccessoryView{
    if (self.viewAccessories.hidden == YES) {
        self.viewAccessories.alpha = 0;
        self.viewAccessories.hidden = NO;
        [UIView animateWithDuration:0.3 animations:^{
            self.viewAccessories.alpha = 1;
        }];
    }
}
- (void) hideAccessoryView{
    if (self.viewAccessories.hidden == NO){
        [UIView animateWithDuration:0.3 animations:^{
            self.viewAccessories.alpha = 0;
        } completion: ^(BOOL finished) {//creates a variable (BOOL) called "finished" that is set to *YES* when animation IS completed.
            self.viewAccessories.hidden = finished;//if animation is finished ("finished" == *YES*), then hidden = "finished" ... (aka hidden = *YES*)
        }];
    }
}



- (void)dismissKeyboard{
    [self.tvAskConcierge resignFirstResponder];
}


- (NSAttributedString*) stringWithImageName:(NSString*)imageName andString:(NSString*)string{
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    attachment.image = [UIImage imageNamed:imageName];
    attachment.bounds = CGRectMake(0, -2*SCREEN_SCALE, 18*SCREEN_SCALE, 18*SCREEN_SCALE);
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    
    NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithAttributedString:attachmentString];
    
    
    [myString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"   %@", string]]];
    [myString addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize titleTextSize]] range:NSMakeRange(4, string.length)];
    [myString addAttribute:NSForegroundColorAttributeName value:[AppColor activeTextColor] range:NSMakeRange(4, string.length)];
    
    return myString;
}

- (void) expandTextView{
    [UIView animateWithDuration:0.2 animations:^{
//        self.heightConstraintAskConciergeView.constant = backupHeightContentView +  100*SCREEN_SCALE_BY_HEIGHT;
        self.heightConstraintAskConciergeView.constant = backupHeightContentView;
        [self.view layoutIfNeeded];
    }];
}

- (void) collapseTextView{
    [UIView animateWithDuration:0.2 animations:^{
        self.heightConstraintAskConciergeView.constant = backupHeightContentView;
        [self.view layoutIfNeeded];
    }];
}

- (IBAction)touchAskConcierge:(id)sender {
    if (self.btnCheckPhone.selected == NO && self.btnCheckMail.selected == NO) {
        [self stopActivityIndicator];
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.msgAlert = NSLocalizedString(@"noncheck_respond_to_me_alert", nil);;
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.hidden = YES;
        });
        
        __weak typeof(self) weakSelf = self;
        alert.blockFirstBtnAction = ^{
            [weakSelf.tvAskConcierge becomeFirstResponder];
        };
        
        [self showAlert:alert forNavigation:NO];
    }else{
        [self startActivityIndicator];
        
//        WSB2CCreateConciergeCase *wSB2CCreateConciergeCase = [[WSB2CCreateConciergeCase alloc] init];
//        wSB2CCreateConciergeCase.delegate = self;
        if (!self.categoryName) {
            self.categoryName = @"";
        }
        if (!self.cityName) {
            self.cityName = @"n/a";
        }
        
        NSString* preferredResponse = @"Email";
        if (_btnCheckPhone.selected && !_btnCheckMail.selected) {
            preferredResponse = @"Phone";
        } else if (_btnCheckPhone.selected && _btnCheckMail.selected) {
             preferredResponse = @"Email or Phone";
        }
        
        NSString *requestDetails = [NSString stringWithFormat:@"%@%@%@\n%@",
                                    [[SessionData shareSessiondata] BINNumber],
                                    [self.cityName isEqualToString:@""] ? @"" : [NSString stringWithFormat:@"\n%@", self.cityName],
                                    [self.categoryName isEqualToString:@""] ? @"" : [NSString stringWithFormat:@"\n%@", self.categoryName],
                                    self.tvAskConcierge.text];
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:@{@"requestDetails": requestDetails,
                                                                                    @"requestType":@"O Client Specific",
                                                                                    @"requestCity":self.cityName,
                                                                                    @"preferredResponse":preferredResponse
                                                                                    }];
//        [wSB2CCreateConciergeCase askConciergeWithMessage:dict];
        __weak typeof(self) _self = self;
        [ModelAspireApiManager createRequestWithUserInfo:dict completion:^(NSError *error) {
            if(!_self) return;
            dispatch_async(dispatch_get_main_queue(), ^{
                [_self stopActivityIndicator];
            });
            if (!error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    typeof(self) __self = _self;
                    [_self trackingEventByName:@"Request accepted" withAction:SubmitActionType withCategory:RequestCategoryType];
                    [_self.tvAskConcierge setText:@""];
                    __self->lblPlaceholder.hidden = NO;
                    _self.btnAskConcierge.hidden = YES;
                    _self.btnCheckMail.selected = NO;
                    _self.btnCheckPhone.selected = NO;
                    __self->viewControllerIsPopped = YES;
                    [_self hideAccessoryView];
                    AskConciergeConfirmationViewController *vc = [[AskConciergeConfirmationViewController alloc] init];
                    [_self.navigationController pushViewController:vc animated:YES];
                });
                return;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (error) {
                    if ([AspireApiError getErrorTypeAPIRetrieveProfileFromError:error] == ACCESSTOKEN_INVALID_ERR) {
                        [_self showAlertInvalidUserAndShouldLoginAgain:true];
                        return;
                    }
                }
                
                [_self trackingEventByName:@"Request denied" withAction:SubmitActionType withCategory:RequestCategoryType];
                AlertViewController *alert = [[AlertViewController alloc] init];
                alert.msgAlert = NSLocalizedString(@"askconcierge_fail_message", nil);
                alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
                dispatch_async(dispatch_get_main_queue(), ^{
                    alert.seconBtn.hidden = YES;
                    alert.midView.hidden = YES;
                    [_self showAlert:alert forNavigation:YES];
                });
            });
        }];
    }
    
}




/*
 // UITextViewDelegate
 */
- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (![textView hasText]) {
        lblPlaceholder.hidden = NO;
        self.btnAskConcierge.hidden = YES;
    }
}


- (void) textViewDidBeginEditing:(UITextView *)textView{
    lblPlaceholder.hidden = YES;
}



-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    NSString *resultText = [textView.text stringByReplacingCharactersInRange:range
                                                                   withString:text];
    NSCharacterSet *charSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    resultText = [resultText stringByTrimmingCharactersInSet:charSet];

    if ([resultText isEqualToString:@""]) {
        lblPlaceholder.hidden = YES;
        self.btnAskConcierge.hidden = YES;
        [self hideAccessoryView];
    }else{
        lblPlaceholder.hidden = YES;
        self.btnAskConcierge.hidden = NO;
        [self showAccessoryView];
    }
    return YES;
}


/*
 // DataLoadDelegate
 */
- (void)loadDataDoneFrom:(id<WSBaseProtocol>)ws{
    [self stopActivityIndicator];
    [self trackingEventByName:@"Request accepted" withAction:SubmitActionType withCategory:RequestCategoryType];
    AskConciergeConfirmationViewController *vc = [[AskConciergeConfirmationViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
    [self.tvAskConcierge setText:@""];
    lblPlaceholder.hidden = NO;
    self.btnAskConcierge.hidden = YES;
    self.btnCheckMail.selected = NO;
    self.btnCheckPhone.selected = NO;
    viewControllerIsPopped = YES;
    [self hideAccessoryView];
}



- (void)loadDataFailFrom:(id<WSBaseProtocol>)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self stopActivityIndicator];
        [self trackingEventByName:@"Request denied" withAction:SubmitActionType withCategory:RequestCategoryType];
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.msgAlert = NSLocalizedString(@"askconcierge_fail_message", nil);
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.hidden = YES;
        });
        
        [self showAlert:alert forNavigation:YES];
    });
}

- (void)loadDataFailFrom:(id<BaseResponseObjectProtocol>)result withErrorCode:(NSInteger)errorCode{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self stopActivityIndicator];
        [self trackingEventByName:@"Request denied" withAction:SubmitActionType withCategory:RequestCategoryType];
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.msgAlert = NSLocalizedString(@"askconcierge_fail_message", nil);
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.hidden = YES;
        });
        
        [self showAlert:alert forNavigation:YES];
    });
}

- (IBAction)touchPhone:(id)sender {
    self.btnCheckPhone.selected = !((UIButton *)sender).selected;
}


- (IBAction)touchMail:(id)sender {
    self.btnCheckMail.selected = !((UIButton *)sender).selected;
}



@end
