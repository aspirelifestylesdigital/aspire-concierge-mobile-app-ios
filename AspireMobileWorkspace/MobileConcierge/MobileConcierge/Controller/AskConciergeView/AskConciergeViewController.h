//
//  AskConciergeViewController.h
//  MobileConcierge
//
//  Created by user on 5/9/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface AskConciergeViewController : BaseViewController
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraintAskConciergeView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintAskConciergeView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstrainAskConciergeTextView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadConstraintAskConciergeTextView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trailConstraintAskConciergeTextView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topMessageLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintViewCallConcierge;

@property (weak, nonatomic) IBOutlet UITextView *tvAskConcierge;
@property (weak, nonatomic) IBOutlet UIButton *btnAskConcierge;
- (IBAction)touchAskConcierge:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCallConcierge;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UIView *viewAccessories;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintAccessoriesView;
@property (weak, nonatomic) IBOutlet UILabel *lblRespondToMe;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckPhone;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckMail;
- (IBAction)touchPhone:(id)sender;
- (IBAction)touchMail:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnInternationalCall;


//@property(assign,nonatomic) BOOL isFromMenu;
@property(nonatomic,strong) NSString *itemName;
@property(nonatomic,strong) NSString *categoryName;
@property(nonatomic,strong) NSString *cityName;

@end
