//
//  SelectingCategoryDelegate.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/26/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CategoryItem;


@protocol SelectingCategoryDelegate <NSObject>

-(void)getDataBaseOnCategory:(CategoryItem *)item;

@end
