//
//  UDASignInViewController.m
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 7/14/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "SignInViewController.h"
#import "SWRevealViewController.h"
#import "CreateProfileViewController.h"
#import "HomeViewController.h"
#import "WSB2CGetAccessToken.h"
#import "WSB2CGetUserDetails.h"
#import "WSB2CGetRequestToken.h"
#import "UserRegistrationItem.h"
#import "UtilStyle.h"
#import "UIButton+Extension.h"
#import "NSString+Utis.h"
#import "NSString+AESCrypt.h"
#import "UITextField+Extensions.h"
#import "AppDelegate.h"
#import "MenuViewController.h"
#import "WSB2CVerifyBIN.h"

#import "BINItem.h"
#import "AppData.h"
#import "MenuViewController.h"
#import "NSDictionary+SBJSONHelper.h"
@import AspireApiFramework;
#import "MCConfiguration.h"
#import "PasscodeController.h"
@import AspireApiControllers;
#import "ForgotPasswordView.h"

@interface SignInViewController () <DataLoadDelegate, UITextFieldDelegate>{
    
    BOOL isInputEmail, isInputPassword;
    WSB2CGetAccessToken* wsB2CGetAccessToek;
    WSB2CGetRequestToken* wsB2CGetRequestToken;
    WSB2CGetUserDetails* wsGetUser;
    AppDelegate* appdelegate;
    WSB2CVerifyBIN *wsVerifyBIN;
    NSTimer* timer;
}

@end

@implementation SignInViewController

#pragma mark - INIT
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *tappedOutsideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tappedOutsideKeyboard];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldChanged:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:self.passwordTextField];
    [self setUIStyte];
    [self configureView];
    [self trackingScreenByName:@"Sign in"];
    if (self.shouldGotoForgotPasswordImmediately)
        [self ForgetPasswordAction:nil];
}

- (void) configureView {
#if DEBUG
    NSString* email = [[MCConfiguration getConfigurations] objectForKey:@"email"];
    if ([AACStringUtils isEmptyWithString:email] == false) {
        self.emailTextField.text = email;
    }
    NSString* pass = [[MCConfiguration getConfigurations] objectForKey:@"password"];
    if ([AACStringUtils isEmptyWithString:pass] == false) {
        self.passwordTextField.text = pass;
    }
    isInputEmail = isInputPassword = YES;
    [self setStatusSignInButton];
#else
    NSLog(@"Not in debug mode");
#endif
    NSLog(@"Done");
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setStatusSignInButton];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self setTextViewsDefaultBottomBolder];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)dealloc {
    [timer invalidate];
}

#pragma mark - PRIVATE
-(void)setTextViewsDefaultBottomBolder {
    [self.emailTextField setBottomBolderDefaultColor];
    [self.passwordTextField setBottomBolderDefaultColor];
}

-(void)resignFirstResponderForAllTextField{
    if(self.emailTextField.isFirstResponder){
        [self.emailTextField resignFirstResponder];
    }
    else if(self.passwordTextField.isFirstResponder){
        [self.passwordTextField resignFirstResponder];
    }
}

-(void)dismissKeyboard
{
    [self resignFirstResponderForAllTextField];
}

-(void)verifyData{
    NSString *errorAll = @"All fields are required.";
    NSMutableString *message = [[NSMutableString alloc] init];
    if (self.emailTextField.text.length == 0 && self.passwordTextField.text.length == 0) {
        [message appendString:@"* "];
        [message appendString:errorAll];
        [self.emailTextField setBottomBorderRedColor];
        [self.passwordTextField setBottomBorderRedColor];
        
    }else{
        if (self.emailTextField.text.length == 0 || self.passwordTextField.text.length == 0 || [self verifyValueForTextField:self.emailTextField].length > 0 || [self verifyValueForTextField:self.passwordTextField].length > 0) {
            message = [NSMutableString stringWithString:@"Your User Name or Password is incorrect. Please enter again."];
        }
    }
    
    [self showError:message];
}

-(void)showError:(NSString *)message
{
    if(message.length > 0){
        [self showAlertWithTitle:nil
                         message:message
                         buttons:@[NSLocalizedString(@"ok_button_title", nil)]
                         actions:@[^(void){
            [self makeBecomeFirstResponderForTextField];
        }]
                messageAlignment:NSTextAlignmentCenter];
    }
    else{
        [self resignFirstResponderForAllTextField];
        [self setTextViewsDefaultBottomBolder];
        [self signInWithEmail:self.emailTextField.text withPassword:self.passwordTextField.text];
    }
}

-(void) makeBecomeFirstResponderForTextField
{
    [self resignFirstResponderForAllTextField];
    if(![self.emailTextField.text isValidEmail]){
        [self.emailTextField becomeFirstResponder];
    }
    else if([self.passwordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0){
        [self.passwordTextField becomeFirstResponder];
    }
}

-(NSString*)verifyValueForTextField:(UITextField *)textFied{
    
    NSString *errorMsg = @"";
    if(textFied == self.emailTextField && ![(textFied.isFirstResponder ? textFied.text :self.emailTextField.text) isValidEmail]){
        errorMsg = NSLocalizedString(@"input_invalid_email_msg", nil);
        [textFied setBottomBorderRedColor];
    }
    else if(textFied == self.passwordTextField && [(textFied.isFirstResponder ? textFied.text :self.passwordTextField.text) stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0){
        errorMsg = NSLocalizedString(@"input_invalid_password_msg", nil);
        [textFied setBottomBorderRedColor];
    }
    else{
        [textFied setBottomBolderDefaultColor];
    }
    
    return errorMsg;
}
- (void)WSBaseNetworkUnavailable{
    [self stopActivityIndicator];
}

- (void)signInWithEmail:(NSString*)email withPassword:(NSString*)password {
    
    [self startActivityIndicator];
    __weak typeof(self) _self = self;
    [ModelAspireApiManager loginWithUsername:email password:password completion:^(NSError *error) {
        if(!_self) return;
        typeof(self) __self = _self;
        if (!error) {
            UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([UserObject class])];
            [[SessionData shareSessiondata] setUserObject:userObject];
            
            [ModelAspireApiManager verifyBin:[[User current] getPasscode] completion:^(BOOL isPass, NSString *bin, id  _Nullable error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_self stopActivityIndicator];
                });
                if (isPass && bin) {
                    [ModelAspireApiManager registerServiceUsername:aspire_api_service_username
                                                          password:aspire_api_service_password
                                                           program:aspire_api_program
                                                               bin:bin];
                    [[SessionData shareSessiondata] setBINNumber:bin];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_self updateSuccessRedirect];
                    });
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        PasscodeController *signInViewController = [storyboard instantiateViewControllerWithIdentifier:@"PasscodeController"];
                        signInViewController.isBINInvalid = true;
                        [_self.navigationController pushViewController:signInViewController animated:false];
                    });
                }
            }];
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [_self stopActivityIndicator];
                NSString* message = @"";
                switch ([AspireApiError getErrorTypeAPISignInFromError:error]) {
                    case SIGNIN_INVALID_USER_ERR:
                        message = @"We're sorry. Your username or password do not match what we have in our system. Click OK to try again.";
                        break;
                    case OKTA_LOCKED_ERR:
                        message = @"We're sorry. Please contact us at 877-288-6503 for assistance accessing this account.";
                        break;
                    case UNKNOWN_ERR:
                        break;
                    default:
                        break;
                }
                
                switch ([AspireApiError getErrorTypeAPIGetProfileOKTAFromError:error]) {
                    case OKTA_PROFILE_EXIST_ERR:
                        message = @"We're sorry. Your username or password do not match what we have in our system. Click OK to try again.";
                        break;
                    case UNKNOWN_ERR:
                        break;
                    default:
                        break;
                }
                
                [_self handleAPIErrorWithCode:message];
            });
        }
    }];
}

- (void) handleAPIErrorWithCode:(NSString*)errorCode{
    [self.emailTextField setBottomBorderRedColor];
    [self.passwordTextField setBottomBorderRedColor];
    if (![errorCode isEqualToString:@""]) {
        [self showAlertWithTitle:NSLocalizedString(@"alert_error_title", nil)
                         message:errorCode
                         buttons:@[NSLocalizedString(@"ok_button_title", nil)]
                         actions:@[^{
            [self makeBecomeFirstResponderForTextField];
        }]
                messageAlignment:NSTextAlignmentCenter];
        
        [self.emailTextField setBottomBorderRedColor];
        [self.passwordTextField setBottomBorderRedColor];
    }else{
        [self showApiErrorWithMessage:@""];
    }
    
}

- (void) setUIStyte {
    
    isInputEmail = isInputPassword = NO;
    self.emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    self.passwordTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    self.passwordTextField.secureTextEntry = YES;
    self.emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    self.emailTextField.delegate = self;
    self.passwordTextField.delegate = self;
    
    [self createAsteriskForTextField:self.emailTextField];
    [self createAsteriskForTextField:self.passwordTextField];
    
    self.emailTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.emailTextField.text];
    self.passwordTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.passwordTextField.text];
    self.emailTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.emailTextField.placeholder];
    self.passwordTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.passwordTextField.placeholder];
    
    self.emailTextField.font =  [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.passwordTextField.font =  [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    
    [self setStatusSignInButton];
    
    //    [self.backgroundSignInView setBackgroundColor:[AppColor backgroundColor]];
    
    [self.signInButton setBackgroundColorForDisableStatus];
    [self.signInButton setBackgroundColorForNormalStatus];
    [self.signInButton setBackgroundColorForTouchingStatus];
    [self.signInButton configureDecorationForButton];
    
    [self.forgotButton configureDecorationForButton];
    [self.signUpButton configureDecorationForButton];
    
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:([AppSize titleTextSize])],
                                 NSKernAttributeName: @([AppSize descriptionLetterSpacing])};
    self.forgotButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.forgotButton.titleLabel.text attributes:attributes];
    self.signInButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.signInButton.titleLabel.text attributes:attributes];
    self.signUpButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.signUpButton.titleLabel.text attributes:attributes];
}

- (void)setStatusSignInButton{
    self.signInButton.enabled = (isInputEmail && isInputPassword);
}

-(void) createAsteriskForTextField:(UITextField *)textField
{
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 8, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [textField setLeftView:leftView];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    
}

#pragma mark TEXT FIELD DELEGATE
-(void)textFieldChanged:(NSNotification*)notification {
    isInputPassword = self.passwordTextField.text.length > 0;
    [self setStatusSignInButton];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *inputString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (textField == self.passwordTextField) {
        if ([string isEqualToString:@" "]) {
            return NO;
        }
        isInputPassword = (inputString.length > 0);
        if (textField.text.length >= 25 && range.length == 0){
            return NO;
        }
    }else if (textField == self.emailTextField){
        isInputEmail = inputString.length > 0;
    }
    [self setStatusSignInButton];
    
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.text = [textField.text removeRedudantWhiteSpaceInText];
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == self.emailTextField)
    {
        self.emailTextField.text = textField.text;
    }
    else if(textField == self.passwordTextField)
    {
        self.passwordTextField.text = textField.text;
    }
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if(textField == self.emailTextField){
        textField.text = self.emailTextField.text;
    }
    else if(textField == self.passwordTextField){
        textField.text = self.passwordTextField.text;
    }
}

- (void) updateSuccessRedirect{
    appdelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    typeof(self) __weak _self = self;
    [UIView transitionWithView:appdelegate.window
                      duration:0.5
                       options:UIViewAnimationOptionPreferredFramesPerSecond60
                    animations:^{
                        if (!_self) return;
                        typeof(self) __self = _self;
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        MenuViewController *menuViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
                        UINavigationController *menuNavigationViewController = [[UINavigationController alloc] initWithRootViewController:menuViewController];
                        UIViewController *fontViewController = [[HomeViewController alloc] init];
                        
                        SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:menuNavigationViewController frontViewController:fontViewController];
                        
                        revealController.delegate = __self->appdelegate;
                        revealController.rearViewRevealWidth = SCREEN_WIDTH;
                        revealController.rearViewRevealOverdraw = 0.0f;
                        revealController.rearViewRevealDisplacement = 0.0f;
                        __self->appdelegate.viewController = revealController;
                        __self->appdelegate.window.rootViewController = __self->appdelegate.viewController;
                        [__self->appdelegate.window makeKeyWindow];
                        
                        UIViewController *newFrontController = [[HomeViewController alloc] init];
                        UINavigationController *newNavigationViewController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                        [revealController pushFrontViewController:newNavigationViewController animated:YES];
                    }
                    completion:nil];
    [self trackingEventByName:@"Sign in" withAction:ClickActionType withCategory:AuthenticationCategoryType];
    
}


- (void)setUserDefaultInfoWithDict:(UserRegistrationItem*)item{
    [[SessionData shareSessiondata] setOnlineMemberID:item.OnlineMemberID];
    [[SessionData shareSessiondata] setOnlineMemberDetailIDs:item.OnlineMemberDetailIDs];
}


#pragma mark - DATALOAD
- (void)loadDataDoneFrom:(id<WSBaseProtocol>)ws {
    [self stopActivityIndicator];
    if([ws isKindOfClass:[WSB2CVerifyBIN class]])
    {
        WSB2CVerifyBIN *wsVerify = (WSB2CVerifyBIN *)ws;
        if(wsVerify.data.count > 0)
        {
            BINItem *binItem = (BINItem *)[wsVerify.data objectAtIndex:0];
            if(binItem.valid) {
                [self updateSuccessRedirect];
            }
            else{
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                PasscodeController *signInViewController = [storyboard instantiateViewControllerWithIdentifier:@"PasscodeController"];
                signInViewController.isBINInvalid = true;
                [self.navigationController pushViewController:signInViewController animated:false];
            }
        }
    }
}

- (void)loadDataFailFrom:(id<BaseResponseObjectProtocol>)ws withErrorCode:(NSInteger)errorCode {
    [self stopActivityIndicator];
    [self handleAPIErrorWithCode:@""];
}


#pragma mark - Action
- (IBAction)ForgetPasswordAction:(id)sender {
    self.emailTextField.text = @"";
    self.passwordTextField.text = @"";
    isInputEmail = isInputPassword = NO;
    [self.view endEditing:YES];
    ForgotPasswordView* v = [[ForgotPasswordView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    AACForgotPasswordController* vc = [AACForgotPasswordController new];
    vc.subview = v;
    v.controller = vc;
    __weak SignInViewController *weakSelf = self;
    vc.onDoneForgotPassword = ^(NSString * _Nonnull email, BOOL success) {
        if (success == true) {
            [weakSelf onForgotPassSuccessfully:email];
        }
    };
    [self.navigationController pushViewController:vc animated:true];
}

- (void) onForgotPassSuccessfully:(NSString *) email {
    self.emailTextField.text = email;
    isInputEmail = true;
}

- (IBAction)SignInAction:(id)sender {
    [self verifyData];
}

- (IBAction)SignUpAction:(id)sender {
    [self.view endEditing:true];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PasscodeController *signInViewController = [storyboard instantiateViewControllerWithIdentifier:@"PasscodeController"];
    signInViewController.isBINInvalid = false;
    [self.navigationController pushViewController:signInViewController animated:true];
    
    self.emailTextField.text = @"";
    self.passwordTextField.text = @"";
}

-(void)navigationToCreateProfileViewController {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CreateProfileViewController *createProfileViewController = [storyboard instantiateViewControllerWithIdentifier:@"CreateProfileViewController"];
    
    if ([AppData isCreatedProfile])  {
        SWRevealViewController *revealViewController = self.revealViewController;
        
        [revealViewController pushFrontViewController:createProfileViewController animated:YES];
    } else
        [self.navigationController pushViewController:createProfileViewController animated:YES];
}
@end
