//
//  BaseViewController.h
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/12/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlertViewController.h"
#import "AppColor.h"
#import "AppSize.h"
#import "UtilStyle.h"
#import <Google/Analytics.h>

typedef enum : NSUInteger {
    ClickActionType,
    OpenActionType,
    LeaveActionType,
    SubmitActionType,
    SelectActionType
} ActionType;

typedef enum : NSUInteger {
    AuthenticationCategoryType,
    UserInteractivityCategoryType,
    RequestCategoryType,
    CitySelectionCategoryType,
    CategorySelectionCategoryType,
    SignOutCategoryType
} CategoryType;

@interface BaseViewController : UIViewController<UIScrollViewDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate>
{
    UIView                              *spinnerBackground;
    UIActivityIndicatorView             *spinner;
    UIImageView                         *indicatorImageView;
    BOOL                                showingSpinner;
    UIView                              *maskViewForSpinner;
    float                               translationPointY;
    BOOL                                isLoading;
    BOOL                                isNotChangeNavigationBarColor;
    BOOL                                isNotAskConciergeBarButton;
    BOOL                                isIgnoreScaleView;
}

@property (nonatomic, strong) UIPercentDrivenInteractiveTransition *interactivePopTransition;
@property (nonatomic, strong) UIPanGestureRecognizer *popNavigationBarRecognizer;

-(void)createMenuBarButton;
-(void)createBackBarButton;
-(void)createBackBarButtonWithIconName:(NSString*)iconName;
-(void)showMenu;
-(void)decorateForButton:(UIButton *)button;
-(void)startActivityIndicator;
-(void)stopActivityIndicator;
-(void)signOutCurrentUser;
-(void)createConciergeBarButton;
-(void)initData;
-(void)initView;
- (void)touchBack;
-(void)setNavigationBarColor;
-(void) setNavigationBarWithDefaultColorAndTitle:(NSString*)title;
-(void)backNavigationItem;
-(void)alterWithMessage:(NSString *)message;
-(void) showAlertMCStyleWithTitle:(NSString*)title andMessage:(NSString*)message andOkButtonTitle:(NSString*)okTitle;
-(void) showAlert:(AlertViewController *)alert forNavigation:(BOOL)isNavigationView;
-(void) showNoNetWorkAlert;
- (void) showAlertInvalidUserAndShouldLoginAgain:(BOOL) isLoginAgain;
-(void)handlePopRecognizer:(UIPanGestureRecognizer*)recognizer;
-(void) returnNormalState;
-(void)startActivityIndicatorWithoutMask;
-(void)stopActivityIndicatorWithoutMask;
-(void)setNavigationBarWithColor:(UIColor *)color;
-(void)changePositionForView;
-(void)setUpCustomizedPanGesturePopRecognizer;
-(void)removeSetupForCustomizedPanGesturePopRecognizer;
-(void)askConciergeAction:(id)sender;
- (void) showAPIErrorAlertWithMessage:(NSString*)message andTitle:(NSString*)title;
- (void)showApiErrorWithMessage:(NSString*)message;
- (void) showAlertWithTitle:(NSString*)title
                    message:(NSString*)message
                    buttons:(NSArray*)buttons actions:(NSArray*)actions
           messageAlignment:(NSTextAlignment) textAlign;

//#Tracking
- (void)trackingEventByName:(NSString*)eventName withAction:(ActionType)action withCategory:(CategoryType)category;
- (void)trackingScreenByName:(NSString*)screenName;
@end
