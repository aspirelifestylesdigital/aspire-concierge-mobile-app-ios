//
//  CheckEmailViewController.m
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/13/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "CheckEmailViewController.h"
#import "ImageUtilities.h"
#import "NSString+Utis.h"
#import "Constant.h"
#import "SWRevealViewController.h"
#import "UIButton+Extension.h"
#import "HomeViewController.h"


#import "AppDelegate.h"
#import "UserObject.h"
#import "UITextField+Extensions.h"
#import "UtilStyle.h"
#import <AspireApiFramework/AspireApiFramework.h>
@interface CheckEmailViewController ()


@end

@implementation CheckEmailViewController
{
    NSString *firstNameOnTyping;
    NSString *lastNameOnTyping;
    NSString *emailOnTyping;
    NSString *phoneOnTyping;
    NSString *zipCodeOnTyping;
}
- (void)viewDidLoad
{
    isNotAskConciergeBarButton = YES;
    self.isUpdateProfile = YES;
    [super viewDidLoad];
    [self createBackBarButton];
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"my_profile_menu_title", nil)];
    backupBottomConstraint = self.viewActionBottomConstraint.constant;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    //  Set up Pop pan gesture
    [self setUpCustomizedPanGesturePopRecognizer];
    

    if(self.navigationController.viewControllers.count == 1)
    {
        HomeViewController *homeView = [[HomeViewController alloc] init];
        [self.navigationController setViewControllers:@[homeView,self]];
    }
    
    self.signinBtn.hidden = YES;
    self.signInButton.hidden = YES;
    
    [self setTextViewsDefaultBottomBolder];
    [self.view layoutIfNeeded];
    
    firstNameOnTyping = userInfo.firstName;
    lastNameOnTyping = userInfo.lastName;
    emailOnTyping = userInfo.email;
    phoneOnTyping = userInfo.mobileNumber;
    zipCodeOnTyping = userInfo.zipCode;
    
    
    [self setButtonStatus:NO];
}


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}


- (void)initView{
    self.phoneNumberText.tag = PHONE_NUMBER_TEXTFIELD_TAG;
    [self setButtonFrame];
    
    [self.updateButton setBackgroundColorForNormalStatus];
    [self.updateButton setBackgroundColorForTouchingStatus];
    [self.updateButton configureDecorationForButton];
    [self.updateButton setBackgroundColorForDisableStatus];
    
    [self.cancelButton setBackgroundColorForNormalStatus];
    [self.cancelButton setBackgroundColorForTouchingStatus];
    [self.cancelButton configureDecorationForButton];
    [self.cancelButton setBackgroundColorForDisableStatus];
    
    // Text Style For Greet Message
    NSString *message = NSLocalizedString(@"commitment_my_profile_message", nil);
    self.commitmentLabel.attributedText = [UtilStyle setLargeSizeStyleForLabelWithMessage:message];
    
    [self setCheckBoxState:isCheck];
    self.commitmentLabel.text = NSLocalizedString(@"commitment_my_profile_message", nil);
    [self.updateButton setTitle:NSLocalizedString(@"update_button", nil) forState:UIControlStateNormal];
    [self.cancelButton setTitle:NSLocalizedString(@"cancel_button", nil) forState:UIControlStateNormal];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:([MCSize titleTextSize])],
                                 NSKernAttributeName: @([MCSize descriptionLetterSpacing])};
    self.updateButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.updateButton.titleLabel.text attributes:attributes];
    self.cancelButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.cancelButton.titleLabel.text attributes:attributes];
    
}

- (void) setButtonFrame{
    signBtnWidth = 394.0f * SCREEN_SCALE;
    signBtnHeight = 55.0f * SCREEN_SCALE;
    signFrame = CGRectMake((SCREEN_WIDTH - signBtnWidth)/2.0f, SCREEN_HEIGHT - signBtnHeight - 30.0f, signBtnWidth, signBtnHeight);
    
    self.viewAction.frame = signFrame;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void) createAsteriskForTextField:(UITextField *)textField{
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [textField setLeftView:leftView];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    asteriskImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"asterisk_icon"]];
    asteriskImage.frame = CGRectMake(0.0f, 0.0f, 20.0f, 20.0f);
    asteriskImage.contentMode = UIViewContentModeCenter;
    [textField setRightView:asteriskImage];
    [textField setRightViewMode:UITextFieldViewModeAlways];
}


- (void)updateSuccessRedirect{    
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert =nil;
    alert.msgAlert = NSLocalizedString(@"update_profile_success_message", nil);
    alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
    dispatch_async(dispatch_get_main_queue(), ^{
        alert.seconBtn.hidden = YES;
        alert.midView.hidden = YES;
        [alert.view layoutIfNeeded];
    });
    
    alert.blockFirstBtnAction = ^(void){
        [self getUserInfo];
        [self setTextViewsDefaultBottomBolder];
        [self setButtonStatus:NO];
    };
    
    [self showAlert:alert forNavigation:NO];
}


- (IBAction)touchUpdate:(id)sender {
    [self verifyAccountData];
}

- (IBAction)touchCancel:(id)sender {
    [self getUserInfo];
    [self setTextViewsDefaultBottomBolder];
    [self dismissKeyboard];
    [self setButtonStatus:NO];
    
    currentFirstName = firstNameOnTyping = userInfo.firstName;
    currentLastName = lastNameOnTyping = userInfo.lastName;
    currentEmail = emailOnTyping = userInfo.email;
    currentPhone = phoneOnTyping = userInfo.mobileNumber;
    currentZipCode = zipCodeOnTyping = userInfo.zipCode;
}

- (void)updateProfileButtonStatus{
    [self setButtonStatus:[self checkFieldIsChangeString]];
}

- (void)checkTextFieldIsChange:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *resultText = [textField.text stringByReplacingCharactersInRange:range
                                                                   withString:string];
    
    if (textField == self.firstNameText && resultText.length <= MAX_FIRSTNAME_TEXT_LENGTH) {
        firstNameOnTyping = resultText;
    }else if (textField == self.lastNameText  && resultText.length <= MAX_FIRSTNAME_TEXT_LENGTH) {
        lastNameOnTyping = resultText;
    }else if (textField == self.emailText  && resultText.length <= 100){
        emailOnTyping = resultText;
    }else if (textField == self.phoneNumberText  && resultText.length <= 14){
        phoneOnTyping = resultText;
    }else if (textField == self.zipCodeText  && resultText.length <= 10){
        zipCodeOnTyping = resultText;
    }
    [self setButtonStatus:[self checkFieldIsChangeString]];
}

- (BOOL) checkFieldIsChangeString{
    if (![userInfo.firstName isEqualToString:firstNameOnTyping]) {
        return YES;
    }else if (![userInfo.lastName isEqualToString:lastNameOnTyping]){
        return YES;
    }
    else if (![userInfo.email isEqualToString:emailOnTyping]){
        return YES;
    }
    else if (![userInfo.mobileNumber isEqualToString:phoneOnTyping]){
        return YES;
    }
    else if (![userInfo.zipCode isEqualToString:zipCodeOnTyping]){
        return YES;
    }else if (userInfo.optStatus != isCheck){
        return YES;
    }
    return NO;
}

- (void)setButtonStatus:(BOOL)status{
    self.updateButton.enabled = status;
    self.cancelButton.enabled = status;
}

@end
