//
//  CategoryViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "CategoryViewController.h"
#import "CategoryItem.h"
#import "CategoryCollectionViewCell.h"
#import "Constant.h"
#import "AppData.h"
#import "SWRevealViewController.h"
#import "CustomPopTransition.h"
#import "SelectingCategoryDelegate.h"
#import "SubCategoryViewController.h"
#import "CategoryCityGuideViewController.h"
#import "Common.h"
#import "AlertViewController.h"

#define kNumberOfColumn    2.0
#define kPadding           8.0

@interface CategoryViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{
    CGFloat verticalSpaceDistance;
    CGFloat horizontalSpaceDistance;
    CGPoint subViewCenter;
    CGFloat widthOfItem;
    CategoryItem *headerCategory, *footerCategory;
}

@end

@implementation CategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self trackingScreenByName:@"Category list"];
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    horizontalSpaceDistance = kPadding * SCREEN_SCALE;
    widthOfItem = round((self.collectionView.frame.size.width -(horizontalSpaceDistance * (kNumberOfColumn - 1)))/kNumberOfColumn);
    widthOfItem -= 2;
    verticalSpaceDistance = kPadding * SCREEN_SCALE;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //  Set up Pop pan gesture
    [self setUpCustomizedPanGesturePopRecognizer];
}


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}

-(void)initView
{
    //    self.navigationItem.title = NSLocalizedString(@"category_title", nil);
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"category_title", nil)];
    [self backNavigationItem];
    [self.collectionView registerNib:[UINib nibWithNibName:@"CategoryCollectionViewCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"CategoryCollectionViewCell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"CategoryCollectionViewCell" bundle:[NSBundle mainBundle]] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"CategoryCollectionViewCell"];
    
    //  Update view base on Device size
    [self.view setBackgroundColor:[UIColor whiteColor]];
    self.topConstraint.constant = self.topConstraint.constant * SCREEN_SCALE;
    self.bottomConstraint.constant = self.bottomConstraint.constant * SCREEN_SCALE;
    self.leftConstraint.constant = self.leftConstraint.constant * SCREEN_SCALE;
    self.rightConstraint.constant = self.rightConstraint.constant * SCREEN_SCALE;
    horizontalSpaceDistance = 9.0f * SCREEN_SCALE;
    //verticalSpaceDistance = 9.0f * SCREEN_SCALE;
    
    self.btnAllCategory.backgroundColor = colorFromHexString(@"1A1A1A");
    self.btnAllCategory.titleLabel.font = [UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize titleTextSize]];
    self.btnAllCategory.titleLabel.textColor = [UIColor whiteColor];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:([AppSize titleTextSize])],
                                 NSKernAttributeName: @([AppSize descriptionLetterSpacing])};
    [self.btnAllCategory setTitle:@"All Categories".uppercaseString forState:UIControlStateNormal];
    self.btnAllCategory.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.btnAllCategory.titleLabel.text attributes:attributes];
}

-(void)checkCityGuideEnable
{
    
}
-(void)initData
{
    self.categoryLst = [[AppData getInstance] getSelectionCategoryList];
    if(self.currentCity.subCategories.count == 0) {
        NSMutableArray* temp = [NSMutableArray arrayWithArray:self.categoryLst];
        [temp removeLastObject];
        self.categoryLst = temp;
    }
    NSMutableArray *rawList = [NSMutableArray arrayWithArray:self.categoryLst];
    headerCategory = [self getCategoryWithCode:@"dining"];
    [rawList removeObject:headerCategory];
    footerCategory = [self getCategoryWithCode:@"all"];
    [rawList removeObject:footerCategory];
    self.categoryLst = rawList;
}

#pragma mark COLLECTION DELEGATE

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.categoryLst.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryCollectionViewCell *cell = (CategoryCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"CategoryCollectionViewCell" forIndexPath:indexPath];
    CategoryItem *item = [self.categoryLst objectAtIndex:indexPath.row];
    ;
    BOOL isDisableItem = (!(self.currentCity.subCategories.count > 0) && [item.ID isEqualToString:@"5"]);

    [cell initCellWithData:item withDisableItem:isDisableItem];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /*
    CGFloat cellWidth = (SCREEN_WIDTH - (self.leftConstraint.constant + self.rightConstraint.constant) - (horizontalSpaceDistance * 2)) / 3.0;
    cellWidth = cellWidth - (IPHONE_5S ? 0.15f : 0.0); //total round value greater than width
    CGSize size = CGSizeMake(cellWidth, cellWidth);
    verticalSpaceDistance = (self.collectionView.bounds.size.height - (cellWidth * 4))/4.0f;
    self.bottomConstraint.constant = verticalSpaceDistance;
    self.topConstraint.constant = verticalSpaceDistance;
    return size;
     */
    return CGSizeMake(widthOfItem, widthOfItem*2/3);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return verticalSpaceDistance;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return horizontalSpaceDistance;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryCollectionViewCell *cell = (CategoryCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
    CategoryItem *item = [self.categoryLst objectAtIndex:indexPath.row];
    
    if (![item.code isEqualToString:@"city guide"]) {
        [self trackingEventByName:[item.categoryName capitalizedString] withAction:SelectActionType withCategory:CategorySelectionCategoryType];
    }
    if(![item.code isEqualToString:@"city guide"] || ([item.code isEqualToString:@"city guide"] && self.currentCity.subCategories.count > 0))
    {
        cell.name.textColor = colorFromHexString(@"#FF8C66");
        cell.name.shadowColor = [UIColor clearColor];
    }
    
    if([item.code isEqualToString:@"city guide"])
    {
        if(self.currentCity.subCategories.count > 0)
        {
            // wait for text-color change
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [NSThread sleepForTimeInterval:(0.1)];
                dispatch_async(dispatch_get_main_queue(), ^{
                    cell.name.shadowColor = [UIColor blackColor];
                    CategoryCityGuideViewController *cityGuideViewController = [[CategoryCityGuideViewController alloc] init];
                    cityGuideViewController.delegate = self.delegate;
                    cityGuideViewController.subCategories = self.currentCity.subCategories;
                    [self.navigationController pushViewController:cityGuideViewController animated:NO];
                    [cell setCellToDefault];
                });
            });
        }
    }
    else  if([item.code isEqualToString:@"mastercard travel"])
    {
        [self showAlertForMoreInformationWithURL:[NSURL URLWithString:MASTERCARD_TRAVEL_URL] ForCell:cell];
    }
    else if([item.code isEqualToString:@"golf"])
    {
        [self showAlertForMoreInformationWithURL:[NSURL URLWithString:GOLF_URL] ForCell:cell];
    }
    else if([item.code isEqualToString:@"priceless city"])
    {
        [self showAlertForMoreInformationWithURL:[NSURL URLWithString:PRICELESS_URL] ForCell:cell];
    }
    else
    {
        CategoryItem *itemForData = [item copy];
        
        if([item.code isEqualToString:@"dining"])
        {
            itemForData.ID = self.currentCity.diningID;
        }
        
        
        if([self.delegate respondsToSelector:@selector(getDataBaseOnCategory:)])
        {
            [self.navigationController popViewControllerAnimated:YES];
            [self.delegate getDataBaseOnCategory:itemForData];
        }
    }
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if (headerCategory) {
        return CGSizeMake(0., self.collectionView.frame.size.width/3 + kPadding);
    }
    return CGSizeZero;;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath {
    if (headerCategory) {
        CategoryCollectionViewCell *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:
                                                  UICollectionElementKindSectionHeader withReuseIdentifier:@"CategoryCollectionViewCell" forIndexPath:indexPath];
        [self updateSectionHeader:headerView forIndexPath:indexPath];
        
        UIView *spacingBottomView = [[UIView alloc] initWithFrame:CGRectMake(0, headerView.frame.size.height - kPadding, self.collectionView.frame.size.width, kPadding)];
        spacingBottomView.backgroundColor = [UIColor whiteColor];
        [headerView addSubview:spacingBottomView];
        
        UITapGestureRecognizer *tapHeaderGesture = [[UITapGestureRecognizer alloc]
                                                    initWithTarget:self
                                                    action:@selector(onTapHeader:)];
        
        [headerView addGestureRecognizer:tapHeaderGesture];
        
        return headerView;
    }
    return nil;
}

- (void)onTapHeader:(UITapGestureRecognizer *)gesture {
    if (headerCategory) {
        CategoryItem *itemForData = headerCategory;
        if([headerCategory.code isEqualToString:@"dining"]) {
            itemForData.ID = self.currentCity.diningID;
        }
        if([self.delegate respondsToSelector:@selector(getDataBaseOnCategory:)]) {
            [self.navigationController popViewControllerAnimated:YES];
            [self.delegate getDataBaseOnCategory:itemForData];
        }
    }
}

- (void)updateSectionHeader:(UICollectionReusableView *)header forIndexPath:(NSIndexPath *)indexPath {
    CategoryCollectionViewCell *cell = (CategoryCollectionViewCell *)header;
    [cell initCellWithData:headerCategory withDisableItem:NO];
}
#pragma mark LOGICAL FUNCTION
- (CategoryItem*)getCategoryWithCode:(NSString*)code {
    for (CategoryItem *item in self.categoryLst) {
        if ([item.code.uppercaseString isEqualToString:code.uppercaseString]) {
            return item;
        }
    }
    return nil;
}
-(void) showAlertForMoreInformationWithURL:(NSURL*)url ForCell:(CategoryCollectionViewCell *)cell
{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"alert_title", nil);
    alert.msgAlert = NSLocalizedString(@"category_more_info_msg", nil);
    alert.secondBtnTitle = NSLocalizedString(@"yes_button", nil);
    alert.blockSecondBtnAction = ^(void){
        [[UIApplication sharedApplication] openURL:url];
        cell.name.textColor = [UIColor whiteColor];
        cell.name.shadowColor = [UIColor blackColor];
        [cell.maskView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.4]];
    };
    
    alert.firstBtnTitle = NSLocalizedString(@"alert_cancel_button", nil);
    alert.blockFirstBtnAction  = ^(void){
        cell.name.textColor = [UIColor whiteColor];
        cell.name.shadowColor = [UIColor blackColor];
        [cell.maskView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.4]];
    };
    
    [self showAlert:alert forNavigation:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ACTIONS
- (IBAction)onSelectAllCategory:(id)sender {
    if([self.delegate respondsToSelector:@selector(getDataBaseOnCategory:)])
    {
        [self.navigationController popViewControllerAnimated:YES];
        [self.delegate getDataBaseOnCategory:footerCategory];
    }
}

@end
