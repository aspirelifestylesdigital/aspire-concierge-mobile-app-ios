//
//  TermsOfUseViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/16/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "TermsOfUseViewController.h"

#import "Constant.h"

@interface TermsOfUseViewController () <UIScrollViewDelegate, UIWebViewDelegate>

@end

@implementation TermsOfUseViewController

- (void)viewDidLoad {
    isNotAskConciergeBarButton = YES;
    self.getClientCopyType = GetClientCopy_TermOfUse;
    [super viewDidLoad];
    [self createBackBarButtonWithIconName:@"clear_icon"];
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"terms_of_use_title", nil)];
    // Do any additional setup after loading the view from its nib.


    backButtonItem = self.navigationItem.leftBarButtonItem;
    backButtonItem.enabled = YES;
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //  Set up Pop pan gesture
    [self setUpCustomizedPanGesturePopRecognizer];
}

- (void)handlePopRecognizer:(UIPanGestureRecognizer *)recognizer{
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) didScrollToEnd{
    backButtonItem.enabled = YES;
}



@end
