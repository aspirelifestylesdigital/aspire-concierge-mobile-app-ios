//
//  MenuViewController.m
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/12/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "MenuViewController.h"
#import "SWRevealViewController.h"
#import "Constant.h"
#import "UIView+Extension.h"
#import "AppData.h"
#import "ExploreViewController.h"
#import "AskConciergeViewController.h"
#import "AboutAppViewController.h"
#import "TermsOfUseViewController.h"
#import "PrivacyPolicyViewController.h"
#import "SWRevealViewController.h"
#import "ChangePasswordViewController.h"
//
#import "HomeViewController.h"
#import "MyProfileViewController.h"
#import "AppDelegate.h"
#import "Common.h"
#import "PolicyViewController.h"
#import "UtilStyle.h"
#import "CityItem.h"
#import "CityViewController.h"

@interface MenuViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    NSIndexPath *presentedRow;
    AppDelegate *appDelegate;
    BOOL isNavigating;
}


@property (nonatomic, strong) NSDictionary *menuDataDict;
@property (nonatomic, strong) NSArray *menuSectionTitle;
@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createMenuBarButton];
    
    self.menuDataDict = [AppData getMenuItems];
    self.menuSectionTitle = [self.menuDataDict allKeys];
    
    self.menuTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.menuTableView setBackgroundColorForView];
    self.menuTableView.alwaysBounceVertical = NO;
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(displayFrontView:)];
    [self.view addGestureRecognizer:panGesture];
    
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"", nil)];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

-(void)createMenuBarButton
{
    SWRevealViewController *revealViewController = [self revealViewController];
    [revealViewController tapGestureRecognizer];

    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [menuButton addTarget:revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [menuButton setBackgroundImage:[UIImage imageNamed:@"menu_white_icon"] forState:UIControlStateNormal];
    [menuButton setBackgroundImage:[UIImage imageNamed:@"menu_interaction_icon"] forState:UIControlStateHighlighted];
    UIBarButtonItem *menuButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    self.navigationItem.leftBarButtonItem = menuButtonItem;
}

-(void)displayFrontView:(UIPanGestureRecognizer*)panGesture
{
    SWRevealViewController *revealViewController = [self revealViewController];
    [revealViewController _handleRevealGesture:panGesture];
}
-(void)askConciergeAction:(id)sender
{
    SWRevealViewController *revealController = self.revealViewController;
    UIViewController *newFrontController = nil;
    newFrontController = [[AskConciergeViewController alloc] init];
    if([revealController.frontViewController isKindOfClass:[UINavigationController class]])
    {
        UINavigationController *currentNavigation = (UINavigationController *)revealController.frontViewController;
        [currentNavigation pushViewController:newFrontController animated:NO];
        [revealController revealToggle:nil];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIColor *backgroundColor = [AppColor backgroundColor];
    [self setNavigationBarWithColor:backgroundColor];
    [self.view setBackgroundColor:backgroundColor];
    [self.menuTableView setBackgroundColor:backgroundColor];
    [self.menuTableView reloadData];
    isNavigating = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    isNavigating = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma marl - UITableView Data Source

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    {
        return 0.0f;
    }
    return 1.0f;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, SCREEN_WIDTH, 1.0f)];
    [headerView setBackgroundColor:[UtilStyle colorForSeparateLine]];
    return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *menuSection = [self.menuDataDict allKeys];
    NSString *menuTitle = [menuSection objectAtIndex:section];
    NSArray *allValuesInSection = [self.menuDataDict objectForKey:menuTitle];
    return allValuesInSection.count;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.menuDataDict allKeys].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"MenuCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (nil == cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    NSString *sectionTitle = [self.menuSectionTitle objectAtIndex:indexPath.section];
    NSArray *valuesInSection = [self.menuDataDict objectForKey:sectionTitle];
    cell.textLabel.text = [valuesInSection objectAtIndex:indexPath.row];
    if (presentedRow.row == indexPath.row && presentedRow.section == indexPath.section) {
        cell.textLabel.textColor = colorFromHexString(@"#FF8C66");
    }else{
        cell.textLabel.textColor = [UIColor whiteColor];
    }
    cell.textLabel.attributedText = [[NSAttributedString alloc] initWithString:[valuesInSection objectAtIndex:indexPath.row] attributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize titleTextSize]]}];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setBackgroundColor:[UIColor clearColor]];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Prevent from double tap
    if(isNavigating)
    {
        return;
    }
    
    isNavigating = YES;
    
    // Set back default Color
    appDelegate.currentCollorForMenu = [AppColor backgroundColor];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:presentedRow];
    cell.textLabel.textColor = [UIColor whiteColor];
    
    cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.textColor = colorFromHexString(@"#FF8C66");
    
    presentedRow = indexPath;
    

    SWRevealViewController *revealController = self.revealViewController;
    UIViewController *newFrontController = nil;

    if(indexPath.section == 0)
    {
        if(indexPath.row == 0)
        {
            newFrontController = [[HomeViewController alloc] init];
            
        }
        else if(indexPath.row == 1)
        {
            newFrontController = [[AskConciergeViewController alloc] init];
            if([revealController.frontViewController isKindOfClass:[UINavigationController class]])
            {
                UINavigationController *currentNavigation = (UINavigationController *)revealController.frontViewController;
                [currentNavigation pushViewController:newFrontController animated:NO];
                [revealController revealToggle:nil];
                return;
            }
        }
        else if(indexPath.row == 2)
        {
            CityItem *selectedCity = [[AppData getInstance] getSelectedCity];
            if(selectedCity)
            {
                ExploreViewController *exploreViewController = [[ExploreViewController alloc] init];
                exploreViewController.currentCity = selectedCity;
                newFrontController = exploreViewController;
                
            }
            else
            {
                newFrontController = [[CityViewController alloc] init];
            }
            
        }
    }
    else if(indexPath.section == 1)
    {
        if(indexPath.row == 0)
        {
            newFrontController = [[MyProfileViewController alloc] init];
            if([revealController.frontViewController isKindOfClass:[UINavigationController class]])
            {
                UINavigationController *currentNavigation = (UINavigationController *)revealController.frontViewController;
                [currentNavigation pushViewController:newFrontController animated:NO];
                [revealController revealToggle:nil];
                return;
            }
        } else if (indexPath.row == 1){
            newFrontController = [[ChangePasswordViewController alloc] init];
            if([revealController.frontViewController isKindOfClass:[UINavigationController class]])
            {
                UINavigationController *currentNavigation = (UINavigationController *)revealController.frontViewController;
                [currentNavigation pushViewController:newFrontController animated:NO];
                [revealController revealToggle:nil];
                return;
            }
        }
    }
    else if(indexPath.section == 2)
    {
        if(indexPath.row == 0)
        {
            newFrontController = [[AboutAppViewController alloc] init];
            if([revealController.frontViewController isKindOfClass:[UINavigationController class]])
            {
                UINavigationController *currentNavigation = (UINavigationController *)revealController.frontViewController;
                [currentNavigation pushViewController:newFrontController animated:NO];
                [revealController revealToggle:nil];
                return;
            }
        }
        else if(indexPath.row == 1)
        {
            newFrontController = [[PolicyViewController alloc] init];
            if([revealController.frontViewController isKindOfClass:[UINavigationController class]])
            {
                UINavigationController *currentNavigation = (UINavigationController *)revealController.frontViewController;
                [currentNavigation pushViewController:newFrontController animated:NO];
                [revealController revealToggle:nil];
                return;
            }
        }
        else if(indexPath.row == 2)
        {
            newFrontController = [[TermsOfUseViewController alloc] init];
            if([revealController.frontViewController isKindOfClass:[UINavigationController class]])
            {
                UINavigationController *currentNavigation = (UINavigationController *)revealController.frontViewController;
                [currentNavigation pushViewController:newFrontController animated:NO];
                [revealController revealToggle:nil];
                return;
            }
        }
        else if (indexPath.row == 3){
            [self trackingEventByName:@"Sign out" withAction:ClickActionType withCategory:SignOutCategoryType];
            [self signOutCurrentUser];
            return;
        }
    }
    
    UINavigationController *newNavigationViewController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
    [revealController pushFrontViewController:newNavigationViewController animated:YES];
}

@end
