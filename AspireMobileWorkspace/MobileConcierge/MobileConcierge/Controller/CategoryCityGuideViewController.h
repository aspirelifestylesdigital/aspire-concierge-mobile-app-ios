//
//  CityGuideViewController.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/26/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "SelectingCategoryDelegate.h"

@interface CategoryCityGuideViewController : BaseViewController

@property(nonatomic, weak) IBOutlet UITableView *tableView;
@property(nonatomic, strong) id<SelectingCategoryDelegate> delegate;
@property(nonatomic, strong) NSString *superCategoryId;
@property(nonatomic, strong) NSArray *subCategories;

@end
