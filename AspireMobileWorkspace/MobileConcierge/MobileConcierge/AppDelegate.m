//
//  AppDelegate.m
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/12/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "AppDelegate.h"
#import "MenuViewController.h"
#import "HomeViewController.h"
#import "SWRevealViewController.h"
#import "PasscodeController.h"
#import "UserObject.h"
#import "PrivacyPolicyViewController.h"
#import "SplashViewController.h"
#import "Constant.h"
#import "GalleryViewController.h"
#import "Common.h"
#import <Google/Analytics.h>
#import "CCAGeographicManager.h"
@import AspireApiFramework;
@import AspireApiControllers;

@interface AppDelegate ()<SWRevealViewControllerDelegate>
{
    UserObject* loggedInUser;
    UIViewController *blankViewController;
    BOOL isEnterBackground, isNotFirstLoad;
}
@end

@implementation AppDelegate

#define debug 1

- (CoreDataHelper*)cdh {
    if (debug==1) {
        //NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    if (!_coreDataHelper) {
        _coreDataHelper = [CoreDataHelper new];
        [_coreDataHelper setupCoreData];
    }
    return _coreDataHelper;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //Where App Store
    //NSLog(@"%@",[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory  inDomains:NSUserDomainMask] lastObject]);
    //Begin Configure GA
    
    GAI *gai = [GAI sharedInstance];

    //For Production
    [gai trackerWithTrackingId:@"UA-111695780-1"];
    
    gai.trackUncaughtExceptions = YES;
    // Set white status
//    [application setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [ModelAspireApiManager registerServiceUsername:aspire_api_service_username
                                          password:aspire_api_service_password
                                           program:aspire_api_program
                                               bin:@""];
    [ModelAspireApiManager registerClient:@"MCD" xAppId:@"3342aa59fd004973a6773d682a029f2a"];
    
    [ModelAspireApiManager shared].checkingNetwork = ^BOOL{
        return isNetworkAvailable();
    };
    
    [ModelAspireApiManager shared].errorAuthorized = ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showAlertInvalidUserAndShouldLoginAgain:true];
        });
    };
    
    [AACManager configAppNameWithName:AppNameMc];
    
    [CCAGeographicManager startService];
    //  Remove navigation bar bottom line
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init]
                                      forBarPosition:UIBarPositionAny
                                          barMetrics:UIBarMetricsDefault];
    
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    
    //Set default color for menu
    self.currentCollorForMenu = [AppColor backgroundColor];
    // set root view controller
    UIWindow *window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window = window;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MenuViewController *menuViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
    UINavigationController *menuNavigationViewController = [[UINavigationController alloc] initWithRootViewController:menuViewController];
    UIViewController *fontViewController = fontViewController = [storyboard instantiateViewControllerWithIdentifier:@"SplashViewController"];

    SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:menuNavigationViewController frontViewController:fontViewController];
    
    revealController.delegate = self;
    revealController.rearViewRevealWidth = SCREEN_WIDTH;
    revealController.rearViewRevealOverdraw = 0.0f;
    revealController.rearViewRevealDisplacement = 0.0f;
    self.viewController = revealController;
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];

    isNotFirstLoad = true;
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    if (!blankViewController) {
        blankViewController = [UIViewController new];
        blankViewController.view.backgroundColor = [UIColor blackColor];
    }
    if (blankViewController.presentingViewController == nil) {
        // Pass NO for the animated parameter. Any animation will not complete
        // before the snapshot is taken.
        [self.window.rootViewController presentViewController:blankViewController animated:NO completion:nil];
    }
    
    [[ModelAspireApiManager shared] stopRequest];
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    //[[self cdh] saveContext];
    
    isEnterBackground = YES;
    
    // Your application can present a full screen modal view controller to
    // cover its contents when it moves into the background. If your
    // application requires a password unlock when it retuns to the
    // foreground, present your lock screen or authentication view controller here.
    
    if (!blankViewController) {
        blankViewController = [UIViewController new];
        blankViewController.view.backgroundColor = [UIColor blackColor];
        
    }
    if (blankViewController.presentingViewController == nil) {
        // Pass NO for the animated parameter. Any animation will not complete
        // before the snapshot is taken.
        [self.window.rootViewController presentViewController:blankViewController animated:NO completion:NULL];
    }
    [self trackingEventLeaveApp];
    if (![[ModelAspireApiManager shared] isRequesting])
        [ModelAspireApiManager revokeToken];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    // This should be omitted if your application presented a lock screen
    // in -applicationDidEnterBackground:
    if (isNotFirstLoad && [User isValid]) { // checking user change password from another device, browser.
        typeof(self) __weak _self = self;
        [ModelAspireApiManager retrieveProfileCurrentUser:^(User *user, NSError *error) {
            if (!_self) return;
            if (!error) {
                [[SessionData shareSessiondata] setUserObject:(UserObject *)[user convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([UserObject class])]];
            }
        }];
    }
    
    if (blankViewController) {
//         [self.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
        [blankViewController dismissViewControllerAnimated:false completion:nil];
        blankViewController = nil;
    }
    [self trackingEventOpenApp];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    if(isEnterBackground) {
        isEnterBackground = NO;
    }else{
        [self trackingEventOpenApp];
    }
    
    if (blankViewController.presentingViewController != nil) {
        [self.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
    }
    
    [[ModelAspireApiManager shared] continueRequest];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    //[self saveContext];
}

-(void) saveAccessToken:(NSString*) accesToken refreshToken:(NSString*) refreshToken expired:(NSString*) expiredTime{
    if(expiredTime!=nil && expiredTime.length > 0){
        _B2C_ExpiredAt = [[NSDate date] dateByAddingTimeInterval:[expiredTime integerValue]];
    }
}

#pragma mark - Tracking

- (void)trackingEventOpenApp {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"User interactivity"
                                                          action:@"Open"
                                                           label:@"Open the app"
                                                           value:nil] build]];
}

- (void)trackingEventLeaveApp {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"User interactivity"
                                                          action:@"Leave"
                                                           label:@"Leave the app"
                                                           value:nil] build]];
}

#pragma mark - private
- (void) showAlertInvalidUserAndShouldLoginAgain:(BOOL) isLoginAgain{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);
    alert.msgAlert = NSLocalizedString(@"invalid_credentials", nil);
    alert.firstBtnTitle = NSLocalizedString(@"alert_ok_button", nil);
    if (isLoginAgain) {
        alert.blockFirstBtnAction = ^(void){
            [ModelAspireApiManager logout];
            AppDelegate* delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            UIWindow *window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            delegate.window = window;
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            MenuViewController *menuViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
            UINavigationController *menuNavigationViewController = [[UINavigationController alloc] initWithRootViewController:menuViewController];
            UIViewController *fontViewController = fontViewController = [storyboard instantiateViewControllerWithIdentifier:@"SplashViewController"];
            
            SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:menuNavigationViewController frontViewController:fontViewController];
            
            revealController.delegate = delegate;
            revealController.rearViewRevealWidth = SCREEN_WIDTH;
            revealController.rearViewRevealOverdraw = 0.0f;
            revealController.rearViewRevealDisplacement = 0.0f;
            delegate.viewController = revealController;
            delegate.window.rootViewController = delegate.viewController;
            [delegate.window makeKeyAndVisible];
        };
    }
    
    alert.providesPresentationContextTransitionStyle = YES;
    alert.definesPresentationContext = YES;
    [alert setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    
    id rootViewController = [UIApplication sharedApplication].delegate.window.rootViewController;
    if([rootViewController isKindOfClass:[UINavigationController class]])
    {
        rootViewController = ((UINavigationController *)rootViewController).viewControllers.firstObject;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        alert.seconBtn.hidden = YES;
        alert.midView.hidden = true;
    });
    
    [rootViewController presentViewController:alert animated:YES completion:nil];
}
@end
