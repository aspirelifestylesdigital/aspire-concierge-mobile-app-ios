//
//  AppDelegate.h
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/12/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "CoreDataHelper.h"

@class SWRevealViewController;
@class UserObject;
@class ALRadialMenu;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) SWRevealViewController *viewController;
@property (strong, nonatomic, readonly) CoreDataHelper *coreDataHelper;

//B2C data
@property (nonatomic, strong)NSDate* B2C_ExpiredAt;

// Webservices
@property (strong, nonatomic) NSString* AuthToken;
@property (strong, nonatomic) NSDate* AuthExpDate;
@property (strong, nonatomic) UIColor *currentCollorForMenu;

//- (void)saveContext;
-(void) saveAccessToken:(NSString*) accesToken refreshToken:(NSString*) refreshToken expired:(NSString*) expiredTime;

@end

