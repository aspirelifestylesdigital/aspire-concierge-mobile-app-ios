//
//  ChallengeQuestionObject.m
//  MobileConciergeUSDemo
//
//  Created by Den on 8/3/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "ChallengeQuestionObject.h"

@implementation ChallengeQuestionObject

- (id) initFromDict:(NSDictionary*) dict{
    self = [[ChallengeQuestionObject alloc] init];
    _question = [dict stringForKey:@"question"];
    _questionText = [dict stringForKey:@"questionText"];

    return self;
}


@end
