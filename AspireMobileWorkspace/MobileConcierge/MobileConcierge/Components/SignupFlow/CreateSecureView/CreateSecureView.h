//
//  ForgotPasswordViewController.h
//  MobileConciergeUSDemo
//
//  Created by Den on 8/3/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseComponentView.h"

@class DropDownView;
@class CustomTextField;
@class PasswordTextField;
@class AACForgotPasswordController;

@interface CreateSecureView : BaseComponentView
    
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomScrollView;
@property (weak, nonatomic) IBOutlet UILabel *lblTop;
@property (weak, nonatomic) IBOutlet UILabel *lblNote;
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet CustomTextField *emailTextField;
@property (weak, nonatomic) IBOutlet DropDownView *questionDropdown;
@property (weak, nonatomic) IBOutlet UITextField *answerTextField;
@property (weak, nonatomic) IBOutlet PasswordTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet PasswordTextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomButtonConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintHidenView;

@property (weak,nonatomic) AACForgotPasswordController* controller;

- (void) updateUserInfor:(NSDictionary*) userInfo;
@end
