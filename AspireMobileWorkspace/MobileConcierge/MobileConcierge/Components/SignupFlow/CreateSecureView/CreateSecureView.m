//
//  CreateSecureViewController.m
//  MobileConciergeUSDemo
//
//  Created by Den on 8/3/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "CreateSecureView.h"
#import "UITextField+Extensions.h"
#import "UILabel+Extension.h"
#import "UIView+Extension.h"
#import "UIButton+Extension.h"
#import "ChallengeQuestionObject.h"
#import "NSString+Utis.h"
#import "UtilStyle.h"
#import "AlertViewController.h"
#import "CustomTextField.h"
#import "DropDownView.h"
#import "PasswordTextField.h"
#import "Common.h"
#import "Constant.h"
#import "AppSize.h"
#import "AppColor.h"
#import "AppDelegate.h"
#import "HomeViewController.h"
#import "MyProfileViewController.h"
#import "SWRevealViewController.h"
#import "MenuViewController.h"
#import "NSString+Utis.h"

//#import <AspireApiFramework/AspireApiFramework.h>
@import AspireApiFramework;

@import AspireApiControllers;

@interface CreateSecureView () <DropDownViewDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate> {
    CGFloat keyboardHeight;
    UITapGestureRecognizer *tappedOutsideKeyboards;
    NSMutableArray *questionArray;
    NSMutableArray *questionTextArray;
    ChallengeQuestionObject *selectedChallengeQuestion;
    NSDictionary* userInfo;
    NSString* currentEmail;
}

@end

@implementation CreateSecureView

#pragma mark - API
- (void)setController:(AACForgotPasswordController *)controller {
    _controller = controller;
    
    if (_controller) {
        self.vc = _controller;
        typeof(self) __weak _self = self;
    
        _controller.onViewWillAppear= ^{
            if (!_self) return;
            [_self viewWillAppear];
        };
        
        _controller.onViewWillDisappear= ^{
            if (!_self) return;
            [_self viewWillDisappear];
        };
    }
}

- (void) updateUserInfor:(NSDictionary *)userInfor {
    userInfo = userInfor;
    if ([[userInfo allKeys] containsObject:@"Email"]) {
        [self setEmail:[userInfo objectForKey:@"Email"]];
    }
}

- (void) setEmail:(NSString *)email {
    if (email != nil) {
        currentEmail = email;
        _emailTextField.text = [email createMaskForText:true];
    }
}

#pragma mark - event
- (IBAction)backAction:(id)sender {
    [_controller successRedirect];
}


#pragma mark - INIT
- (void)config {
    
    if(!self.didUpdateLayout && !self.isIgnoreScaleView){
        self.didUpdateLayout = YES;
        resetScaleViewBaseOnScreen(self.view);
    }
    
    [self setUpData];
    [self setUIStyte];
}

-(void) viewWillAppear {
    [self setTextViewsDefaultBottomBolder];
    tappedOutsideKeyboards = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tappedOutsideKeyboards.delegate = self;
    [_controller.navigationController.view addGestureRecognizer:tappedOutsideKeyboards];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear {
    [_controller.navigationController.view removeGestureRecognizer:tappedOutsideKeyboards];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void) setUIStyte {
    
    [self.lblTop setLineSpacing:6];
    [self.lblNote setLineSpacing:6];
    
    self.titleLable.attributedText = [UtilStyle setTextStyleForTitleViewControllerWithMessage:@"REGISTER"];
    
    self.view.backgroundColor = [AppColor backgroundColor];
    
    self.emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    
    self.answerTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    self.emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    
    [self createAsteriskForTextField:self.emailTextField];
    //    [self createAsteriskForTextField:self.questionDropdown];
    [self createAsteriskForTextField:self.answerTextField];
    [self createAsteriskForTextField:self.passwordTextField];
    [self createAsteriskForTextField:self.confirmPasswordTextField];
    
    
    
    self.questionDropdown.leadingTitle = 8.0f;
    self.questionDropdown.titleFontSize = 18;
    self.questionDropdown.title = @"Security Question";
    self.questionDropdown.listItems = questionTextArray;
    self.questionDropdown.titleColor = [AppColor placeholderTextColor];
    self.questionDropdown.isBreakLine = true;
    self.questionDropdown.delegate = self;
    self.questionDropdown.itemsFont = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.questionDropdown.titleLabel.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    [self.questionDropdown setItemHeight:50];
    
    self.emailTextField.delegate = self;
    self.answerTextField.delegate = self;
    self.passwordTextField.delegate = self;
    self.confirmPasswordTextField.delegate = self;
    
    
    self.emailTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.emailTextField.text];
    self.answerTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.answerTextField.text];
    self.passwordTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.passwordTextField.text];
    self.confirmPasswordTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.confirmPasswordTextField.text];
    
    
    self.emailTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.emailTextField.placeholder];
    self.answerTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.answerTextField.placeholder];
    self.passwordTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.passwordTextField.placeholder];
    self.confirmPasswordTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.confirmPasswordTextField.placeholder];
    
    self.emailTextField.font =  [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.answerTextField.font =  [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.passwordTextField.font =  [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.confirmPasswordTextField.font =  [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.lblTop.font =  [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionTextSize]];
    self.lblNote.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionTextSize]];
    
        [self.submitButton setBackgroundColorForDisableStatus];
//    self.submitButton.backgroundColor = [AppColor normalButtonColor];
    [self.submitButton setBackgroundColorForNormalStatus];
    self.submitButton.enabled = false;
    [self.submitButton setBackgroundColorForTouchingStatus];
    [self.submitButton configureDecorationForButton];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:([AppSize titleTextSize])],
                                 NSKernAttributeName: @([AppSize descriptionLetterSpacing])};
    self.submitButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.submitButton.titleLabel.text attributes:attributes];
    
    [self.emailTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.answerTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.passwordTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.confirmPasswordTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}

-(void) createAsteriskForTextField:(UITextField *)textField
{
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 8, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [textField setLeftView:leftView];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
}

-(void)setTextViewsDefaultBottomBolder
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.emailTextField setBottomBolderDefaultColor];
        [self.answerTextField setBottomBolderDefaultColor];
        [self.passwordTextField setBottomBolderDefaultColor];
        [self.confirmPasswordTextField setBottomBolderDefaultColor];
        [self.questionDropdown setBottomBolderDefaultColor];
    });
}

#pragma mark - Setup data

-(void) setUpData {
    questionArray = [[NSMutableArray alloc] init];
    questionTextArray = [[NSMutableArray alloc] init];
    
    id allKeys = [ModelAspireApiManager getSecurityQuetions];
    
    for (int i=0; i<[allKeys count]; i++) {
        NSDictionary *arrayResult = [allKeys objectAtIndex:i];
        ChallengeQuestionObject *object = [[ChallengeQuestionObject alloc] initFromDict:arrayResult];
        [questionArray addObject:object];
        [questionTextArray addObject:object.questionText];
    }
}


#pragma mark - handle keyboard event

-(void)handleKeyboardWillShow:(NSNotification *)paramNotification
{
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         keyboardHeight = keyboardRect.size.height;
                         [self updateEdgeInsetForShowKeyboard];
                         [self layoutIfNeeded];
                     }
                     completion:nil];
}

-(void)handleKeyboardWillHide:(NSNotification *)paramNotification
{
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         [self updateEdgeInsetForHideKeyboard];
                         [self layoutIfNeeded];
                     }
                     completion:nil];
    
}


-(void)updateEdgeInsetForShowKeyboard
{
    _bottomButtonConstraint.constant =  keyboardHeight + 20;
    _heightConstraintHidenView.constant = 0;
    self.bottomScrollView.constant = -10;
    
}


-(void)updateEdgeInsetForHideKeyboard
{
    _bottomButtonConstraint.constant = 20;
    _heightConstraintHidenView.constant = 140;
    self.bottomScrollView.constant = 0;
}

-(void)dismissKeyboard {
    [self endEditing:true];
    [self updateEdgeInsetForHideKeyboard];
}


#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    
    //    CGPoint touchPoint = [touch locationInView:touch.view];
    
    UIView *view = touch.view.superview;
    if ([view isKindOfClass:[UITableViewCell class]]) {
        return NO;
    }
    return YES;
}

#pragma mark - Dropdown delegate
- (void)didSelectItem:(DropDownView *)dropDownView atIndex:(int)index {
    
    
    //    NSString *valueSelected = [dropDownView.listItems objectAtIndex:index];
    selectedChallengeQuestion = [questionArray objectAtIndex:index];
    [self checkEnableSubmitButton];
    //    _questionDropdown.title = valueSelected;
    
    //    [self setButtonStatus:[self isChangeData]];
    
}

-(void)didShow:(DropDownView *)dropDownView {
    [self endEditing:true];
}

#pragma mark - UITextField delegate


-(void) makeBecomeFirstResponderForTextField
{
    if(![self.emailTextField.text isValidEmail])
    {
        [self.emailTextField becomeFirstResponder];
    }
    else if(![self.passwordTextField.text isValidStrongPassword])
    {
        [self.passwordTextField becomeFirstResponder];
    }
    else if(![self.confirmPasswordTextField.text isEqualToString:self.passwordTextField.text])
    {
        [self.confirmPasswordTextField becomeFirstResponder];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField == self.emailTextField) {
        [self dismissKeyboard];
        self.emailTextField.text = currentEmail;
        return NO;
    }
    else{
        NSString* temp = currentEmail;
        self.emailTextField.text = [temp createMaskForText:YES];
    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.text = [textField.text removeRedudantWhiteSpaceInText];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.passwordTextField || textField == self.confirmPasswordTextField) {
        if ([string isEqualToString:@" "]) {
            return NO;
        }
    }
    return [self updateTextFiel:textField shouldChangeCharactersInRange:range replacementString:string];
}

-(void)checkTextFieldIsChange:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
}

- (void)textFieldDidChange:(UITextField *)textField {
    [self checkEnableSubmitButton];
}

-(BOOL)updateTextFiel:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
//    if(string.length > 1)
//    {
//        NSMutableString *newString = [[NSMutableString alloc] initWithString:[string removeRedudantWhiteSpaceInText]];
//        NSString *resultText = [textField.text stringByReplacingCharactersInRange:range
//                                                                       withString:string];
//        resultText = [resultText removeRedudantWhiteSpaceInText];
//
//
//        if(textField == self.passwordTextField || textField == self.confirmPasswordTextField)
//        {
//            /*if([newString isValidStrongPassword])//isValidStrongPassword
//            {
//                textField.text = newString;
//            }
//            else*/ if(string.length > 25)
//            {
//                textField.text = [newString substringToIndex:25];
//            }
//            else
//            {
//                textField.text = newString;
//            }
//        }
//
//
////        if(textField == self.emailTextField)
////        {
////            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
////            if([newString length] > kEmailInputMaxLength) {
////                return NO;
////            }
////
//////            //textField.text = newString;
//////            textField.text = (resultText.length > 96)?[resultText substringWithRange:NSMakeRange(0, 96)] : resultText;
////        }
//
//        return NO;
//    }

    
    if(textField == self.passwordTextField || textField == self.confirmPasswordTextField)
    {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if([newString length] > kPasswordInputMaxLength) {
            return NO;
        }

        return YES;
    }
    
    else if(textField == self.emailTextField)
    {
        if([textField.text occurrenceCountOfCharacter:'@'] == 1 && [string isEqualToString:@"@"]){
            return NO;
        }
        
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if([newString length] > kEmailInputMaxLength) {
            return NO;
        }
        
        return YES;
    }
    
    return YES;
}

-(void)checkEnableSubmitButton {
    bool is = [_emailTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0 && [_answerTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0 && [_passwordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0 && [_confirmPasswordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0 && selectedChallengeQuestion != nil;
    [_submitButton setEnabled:is];
    
//    self.submitButton.backgroundColor = is ? [AppColor highlightButtonColor] :[AppColor normalButtonColor];
}


#pragma mark - Action for button

-(IBAction)submitButtonTapped:(UIButton*)sender {
    [self dismissKeyboard];
    [self verifyData];
}



#pragma mark - Validate fields


-(void)verifyData {
    
    [_controller receiveInputWithInput:currentEmail type:FPErrorInputTypeEmail];
    [_controller receiveInputWithInput:_answerTextField.text type:FPErrorInputTypeAnswer];
    [_controller receiveInputWithInput:_passwordTextField.text type:FPErrorInputTypePassword];
    [_controller receiveInputWithInput:_confirmPasswordTextField.text type:FPErrorInputTypeConfirmPassword];
    if (userInfo.count > 0) {
        if ([userInfo.allKeys containsObject:@"FirstName"]) {
            _controller.firstName = [userInfo objectForKey:@"FirstName"];
        }
        
        if ([userInfo.allKeys containsObject:@"LastName"]) {
            _controller.lastName = [userInfo objectForKey:@"LastName"];
        }
    }
    _controller.shouldCheckStrongPassword = true;
    
    NSMutableString *message = [[NSMutableString alloc] init];
    [self setTextViewsDefaultBottomBolder];
    
    typeof(self) __weak _self = self;
    [_controller verifyDataWithResponse:^(NSArray<NSNumber *> * _Nullable listError) {
        if (!_self) return;
        
        typeof(self) __self = _self;
        if (listError.count == 0) {
            NSMutableDictionary* temp = [NSMutableDictionary dictionaryWithDictionary:__self->userInfo];
            [temp addEntriesFromDictionary:@{@"Password":__self->_passwordTextField.text}];
            __self->userInfo = temp;
            [_self updateEdgeInsetForHideKeyboard];
            [_self setTextViewsDefaultBottomBolder];
            [_self createUser];
        } else {
            NSMutableArray* listTextfieldError = [NSMutableArray new];
            [listError enumerateObjectsUsingBlock:^(NSNumber * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSString* errorMsg = nil;
                errorMsg = [self handleEmailError:obj listTextfieldError:listTextfieldError];
                if ([AACStringUtils isEmptyWithString:errorMsg] == true) {
                    errorMsg = [self handlePasswordError:obj listTextfieldError:listTextfieldError];
                }
                if ([AACStringUtils isEmptyWithString:errorMsg] == true) {
                    errorMsg = [self handleConfirmPasswordError:obj listTextfieldError:listTextfieldError];
                }
                if ([AACStringUtils isEmptyWithString:errorMsg] == true) {
                    errorMsg = [self handleAnswerError:obj listTextfieldError:listTextfieldError];
                }
                if ([AACStringUtils isEmptyWithString:errorMsg] == false) {
                    (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
                    [message appendString:errorMsg];
                }
            }];
            
            [_self showValidateErrorDialog: listTextfieldError message:message];
        }
    }];
}

- (NSString *) handleEmailError:(NSNumber *) code listTextfieldError:(NSMutableArray *) listTextfieldError {
    if (code == nil) return nil;
    if (code.intValue == FPErrorInputTypeEmail) {
        if ([listTextfieldError containsObject:self.emailTextField] == false) {
            [listTextfieldError addObject:self.emailTextField];
            return NSLocalizedString(@"input_invalid_email_msg", nil);
        }
    }
    return nil;
}

- (NSString *) handlePasswordError:(NSNumber *) code listTextfieldError:(NSMutableArray *) listTextfieldError {
    if (code == nil) return nil;
    if ([listTextfieldError containsObject:self.passwordTextField] == false) {
        if (code.intValue == FPErrorInputTypePassword) {
            [listTextfieldError addObject:self.passwordTextField];
            return NSLocalizedString(@"input_invalid_password_msg", nil);
        }
        if (code.intValue == FPErrorInputTypePasswordContainName) {
            [listTextfieldError addObject:self.passwordTextField];
            return [PasswordValidation errorCodeToMessageWithCode: FPErrorInputTypePasswordContainName];
        }
    }
    return nil;
}

- (NSString *) handleConfirmPasswordError:(NSNumber *) code listTextfieldError:(NSMutableArray *) listTextfieldError {
    if (code == nil) return nil;
    if (code.intValue == FPErrorInputTypeConfirmPassword &&
        [listTextfieldError containsObject:self.confirmPasswordTextField] == false)
    {
        [listTextfieldError addObject:self.confirmPasswordTextField];
        return NSLocalizedString(@"input_invalid_confirm_password_msg", nil);
    }
    return nil;
}

- (NSString *) handleAnswerError:(NSNumber *) code listTextfieldError:(NSMutableArray *) listTextfieldError {
    if (code == nil) return nil;
    if (code.intValue == FPErrorInputTypeAnswer &&
        [listTextfieldError containsObject:self.answerTextField] == false)
    {
        [listTextfieldError addObject:self.answerTextField];
        return NSLocalizedString(@"input_invalid_answer_msg", nil);
    }
    return nil;
}

- (void) showValidateErrorDialog:(NSArray *) listTextfieldError message: (NSString *) message {
    NSMutableString *newMessage = [[NSMutableString alloc] init];
    [newMessage appendString:@"\n"];
    [newMessage appendString:message];
    [newMessage appendString:@"\n"];
    NSArray* buttons = @[NSLocalizedString(@"ok_button_title", nil)];
    NSArray* actions = @[^(void) { [self onErrorDialogOK:listTextfieldError]; }];
    [self showAlertWithTitle:@"Please confirm that the value entered is correct:"
                     message:newMessage
                     buttons:buttons
                     actions:actions
            messageAlignment:NSTextAlignmentLeft];
}

- (void) onErrorDialogOK:(NSArray *) listTextfieldError {
    [listTextfieldError enumerateObjectsUsingBlock:^(UITextField*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj setBottomBorderRedColor];
    }];
    if (listTextfieldError.count > 0) {
        [listTextfieldError.firstObject becomeFirstResponder];
    }
}

#pragma mark - API

-(void)createUser {
    [self startActivityIndicator];
    __weak typeof(self) _self = self;
    
    [ModelAspireApiManager createProfileWithUserInfo:userInfo completion:^(User *user, NSError *error) {
        if(!_self) {return;}
        typeof(self) __self = _self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [_self stopActivityIndicator];
            if (!error) {
                __block UserObject* userObject = (UserObject *)[user convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([UserObject class])];
                [ModelAspireApiManager changeRecoveryQuestionForEmail:[__self->userInfo objectForKey:@"Email"]
                                                             password:[[AAFPassword alloc] initWithPassword:__self->_passwordTextField.text]
                                                             question:[[AAFRecoveryQuestion alloc] initWithQuestion:__self->selectedChallengeQuestion.questionText
                                                                                                             answer:__self->_answerTextField.text]
                                                           completion:^(NSError * _Nullable error) {
                                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                                   [_self stopActivityIndicator];
                                                                   [[SessionData shareSessiondata] setUserObject:userObject];
                                                                   [_self showAlertWithTitle:nil
                                                                                     message:@"Profile created successfully."
                                                                                     buttons:@[NSLocalizedString(@"back_to_profile_button_title", nil),NSLocalizedString(@"home_button_title", nil)]
                                                                                     actions:@[^{
                                                                       AppDelegate*appdelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
                                                                       [UIView transitionWithView:appdelegate.window
                                                                                         duration:0.5
                                                                                          options:UIViewAnimationOptionPreferredFramesPerSecond60
                                                                                       animations:^{
                                                                                           
                                                                                           UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                                                                           MenuViewController *menuViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
                                                                                           UINavigationController *menuNavigationViewController = [[UINavigationController alloc] initWithRootViewController:menuViewController];
                                                                                           UIViewController *fontViewController = [[HomeViewController alloc] init];
                                                                                           
                                                                                           SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:menuNavigationViewController frontViewController:fontViewController];
                                                                                           [revealController revealToggle:nil];
                                                                                           revealController.delegate = appdelegate;
                                                                                           revealController.rearViewRevealWidth = SCREEN_WIDTH;
                                                                                           revealController.rearViewRevealOverdraw = 0.0f;
                                                                                           revealController.rearViewRevealDisplacement = 0.0f;
                                                                                           appdelegate.viewController = revealController;
                                                                                           appdelegate.window.rootViewController = appdelegate.viewController;
                                                                                           [appdelegate.window makeKeyWindow];
                                                                                           
                                                                                           UIViewController *newFrontController = [[MyProfileViewController alloc] init];
                                                                                           UINavigationController *newNavigationViewController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                                                                                           [revealController pushFrontViewController:newNavigationViewController animated:YES];
                                                                                       }
                                                                                       completion:nil];
                                                                   },^{
                                                                       AppDelegate*appdelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
                                                                       [UIView transitionWithView:appdelegate.window
                                                                                         duration:0.5
                                                                                          options:UIViewAnimationOptionPreferredFramesPerSecond60
                                                                                       animations:^{
                                                                                           
                                                                                           UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                                                                           MenuViewController *menuViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
                                                                                           UINavigationController *menuNavigationViewController = [[UINavigationController alloc] initWithRootViewController:menuViewController];
                                                                                           UIViewController *fontViewController = [[HomeViewController alloc] init];
                                                                                           
                                                                                           SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:menuNavigationViewController frontViewController:fontViewController];
                                                                                           
                                                                                           revealController.delegate = appdelegate;
                                                                                           revealController.rearViewRevealWidth = SCREEN_WIDTH;
                                                                                           revealController.rearViewRevealOverdraw = 0.0f;
                                                                                           revealController.rearViewRevealDisplacement = 0.0f;
                                                                                           appdelegate.viewController = revealController;
                                                                                           appdelegate.window.rootViewController = appdelegate.viewController;
                                                                                           [appdelegate.window makeKeyWindow];
                                                                                           
                                                                                           UIViewController *newFrontController = [[HomeViewController alloc] init];
                                                                                           UINavigationController *newNavigationViewController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                                                                                           [revealController pushFrontViewController:newNavigationViewController animated:YES];
                                                                                       }
                                                                                       completion:nil];
                                                                   }]
                                                                            messageAlignment:NSTextAlignmentCenter];
                                                               });
                                                           }];
                return;
            }
            
            NSString* message = @"";
            switch ([AspireApiError getErrorTypeAPISignupFromError:error]) {
                case NETWORK_UNAVAILABLE_ERR:
                    [_self showErrorNetwork];
                    return;
                    break;
                case SIGNUP_EXIST_ERR:
                    message = NSLocalizedString(@"email_exists", nil);
                    break;
                case PW_ERR:
                {
                    [_self showAlertWithTitle:@"Password does not meet all requirements"
                                      message:NSLocalizedString(@"input_invalid_password_msg", nil)
                                      buttons:@[NSLocalizedString(@"alert_ok_button", nil)]
                                      actions:@[^{
                        __self->_confirmPasswordTextField.text = @"";
                        __self->_passwordTextField.text = @"";
                        [__self->_passwordTextField becomeFirstResponder];
                        [__self->_passwordTextField setBottomBorderRedColor];
                        [_self checkEnableSubmitButton];
                    }]
                             messageAlignment:NSTextAlignmentCenter];
                    message = @"";
                    break;
                }
                    break;
                default:
                    message = NSLocalizedString(@"error_api_message", nil);
                    break;
            }
            if (message.length == 0) return;
            [_self showAlertWithTitle:NSLocalizedString(@"alert_error_title", nil)
                              message:message
                              buttons:@[NSLocalizedString(@"ok_button_title", nil)]
                              actions:@[]
                     messageAlignment:NSTextAlignmentCenter];
        });
    }];
}
@end
