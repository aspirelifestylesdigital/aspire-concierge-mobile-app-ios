//
//  ChallengeQuestionObject.h
//  MobileConciergeUSDemo
//
//  Created by Den on 8/3/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChallengeQuestionObject : NSObject

@property (strong, nonatomic) NSString* question;
@property (strong, nonatomic) NSString* questionText;


- (id) initFromDict:(NSDictionary*) dict;
@end
