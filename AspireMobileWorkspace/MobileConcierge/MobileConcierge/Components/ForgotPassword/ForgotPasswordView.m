//
//  ForgotPasswordViewController.m
//  MobileConciergeUSDemo
//
//  Created by Den on 8/3/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "ForgotPasswordView.h"
#import "UITextField+Extensions.h"
#import "UILabel+Extension.h"
#import "UIView+Extension.h"
#import "UIButton+Extension.h"
#import "ChallengeQuestionObject.h"
#import "NSString+Utis.h"
#import "UtilStyle.h"
#import "AlertViewController.h"
#import "CustomTextField.h"
#import "DropDownView.h"
#import "PasswordTextField.h"
#import "Common.h"
#import "Constant.h"
#import "AppSize.h"
#import "AppColor.h"

//#import <AspireApiFramework/AspireApiFramework.h>
@import AspireApiFramework;
@import AspireApiControllers;

@interface ForgotPasswordView () <DropDownViewDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate> {
    CGFloat keyboardHeight;
    UITapGestureRecognizer *tappedOutsideKeyboards;
    NSMutableArray *questionArray;
    NSMutableArray *questionTextArray;
    ChallengeQuestionObject *selectedChallengeQuestion;
}

@end

@implementation ForgotPasswordView

- (void)setController:(AACForgotPasswordController *)controller {
    _controller = controller;
    
    if (_controller) {
        self.vc = _controller;
        typeof(self) __weak _self = self;
    
        _controller.onViewWillAppear= ^{
            if (!_self) return;
            [_self viewWillAppear];
        };
        
        _controller.onViewWillDisappear= ^{
            if (!_self) return;
            [_self viewWillDisappear];
        };
    }
}
- (void)config {
    
    if(!self.didUpdateLayout && !self.isIgnoreScaleView){
        self.didUpdateLayout = YES;
        resetScaleViewBaseOnScreen(self.view);
    }
    
    [self setUpData];
    [self setUIStyte];
}

-(void) viewWillAppear {
    [self setTextViewsDefaultBottomBolder];
    tappedOutsideKeyboards = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tappedOutsideKeyboards.delegate = self;
    [_controller.navigationController.view addGestureRecognizer:tappedOutsideKeyboards];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear {
    [_controller.navigationController.view removeGestureRecognizer:tappedOutsideKeyboards];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}



#pragma mark - Setup Layout



- (IBAction)backAction:(id)sender {
    [_controller successRedirect];
}


- (void) setUIStyte {
    
    [self.lblTop setLineSpacing:6];
    [self.lblNote setLineSpacing:6];
    
    self.titleLable.attributedText = [UtilStyle setTextStyleForTitleViewControllerWithMessage:@"FORGOT PASSWORD"];
    
    self.view.backgroundColor = [AppColor backgroundColor];
    
    self.emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    
    
    self.answerTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    self.emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    
    [self createAsteriskForTextField:self.emailTextField];
    //    [self createAsteriskForTextField:self.questionDropdown];
    [self createAsteriskForTextField:self.answerTextField];
    [self createAsteriskForTextField:self.passwordTextField];
    [self createAsteriskForTextField:self.confirmPasswordTextField];
    
    
    
    self.questionDropdown.leadingTitle = 8.0f;
    self.questionDropdown.titleFontSize = 18;
    self.questionDropdown.title = @"Security Question";
    self.questionDropdown.listItems = questionTextArray;
    self.questionDropdown.titleColor = [AppColor placeholderTextColor];
    self.questionDropdown.isBreakLine = true;
    self.questionDropdown.delegate = self;
    self.questionDropdown.itemsFont = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.questionDropdown.titleLabel.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    [self.questionDropdown setItemHeight:50];
    
    self.emailTextField.delegate = self;
    self.answerTextField.delegate = self;
    self.passwordTextField.delegate = self;
    self.confirmPasswordTextField.delegate = self;
    
    
    self.emailTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.emailTextField.text];
    self.answerTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.answerTextField.text];
    self.passwordTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.passwordTextField.text];
    self.confirmPasswordTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.confirmPasswordTextField.text];
    
    
    self.emailTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.emailTextField.placeholder];
    self.answerTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.answerTextField.placeholder];
    self.passwordTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.passwordTextField.placeholder];
    self.confirmPasswordTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.confirmPasswordTextField.placeholder];
    
    self.emailTextField.font =  [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.answerTextField.font =  [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.passwordTextField.font =  [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.confirmPasswordTextField.font =  [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.lblTop.font =  [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionTextSize]];
    self.lblNote.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionTextSize]];
    
    [self.submitButton setBackgroundColorForDisableStatus];
    [self.submitButton setBackgroundColorForNormalStatus];
    self.submitButton.enabled = false;
    [self.submitButton setBackgroundColorForTouchingStatus];
    [self.submitButton configureDecorationForButton];
    
    [self.emailTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.answerTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.passwordTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.confirmPasswordTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:([AppSize titleTextSize])],
                                 NSKernAttributeName: @([AppSize descriptionLetterSpacing])};
    self.submitButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.submitButton.titleLabel.text attributes:attributes];
}


-(void) createAsteriskForTextField:(UITextField *)textField
{
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 8, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [textField setLeftView:leftView];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
}

-(void)setTextViewsDefaultBottomBolder
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.emailTextField setBottomBolderDefaultColor];
        [self.answerTextField setBottomBolderDefaultColor];
        [self.passwordTextField setBottomBolderDefaultColor];
        [self.confirmPasswordTextField setBottomBolderDefaultColor];
        [self.questionDropdown setBottomBolderDefaultColor];
    });
}

#pragma mark - Setup data

-(void) setUpData {
    questionArray = [[NSMutableArray alloc] init];
    questionTextArray = [[NSMutableArray alloc] init];
    
    TestController *test = [[TestController alloc] init];
    [test test];
    NSArray * allKeys = [ModelAspireApiManager getSecurityQuetions];
    for (int i=0; i<[allKeys count]; i++) {
        NSDictionary *arrayResult = [allKeys objectAtIndex:i];
        ChallengeQuestionObject *object = [[ChallengeQuestionObject alloc] initFromDict:arrayResult];
        [questionArray addObject:object];
        [questionTextArray addObject:object.questionText];
    }
}


#pragma mark - handle keyboard event

-(void)handleKeyboardWillShow:(NSNotification *)paramNotification
{
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         keyboardHeight = keyboardRect.size.height;
                         [self updateEdgeInsetForShowKeyboard];
                         [self layoutIfNeeded];
                     }
                     completion:nil];
}

-(void)handleKeyboardWillHide:(NSNotification *)paramNotification
{
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         [self updateEdgeInsetForHideKeyboard];
                         [self layoutIfNeeded];
                     }
                     completion:nil];
    
}


-(void)updateEdgeInsetForShowKeyboard
{
    _bottomButtonConstraint.constant =  keyboardHeight + 20;
    _heightConstraintHidenView.constant = 0;
    
}


-(void)updateEdgeInsetForHideKeyboard
{
    _bottomButtonConstraint.constant = 20;
    _heightConstraintHidenView.constant = 140;
}

-(void)dismissKeyboard {
    [self endEditing:true];
    [self updateEdgeInsetForHideKeyboard];
}


#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    UIView *view = touch.view.superview;
    if ([view isKindOfClass:[UITableViewCell class]]) {
        return NO;
    }
    return YES;
}

#pragma mark - Dropdown delegate
- (void)didSelectItem:(DropDownView *)dropDownView atIndex:(int)index {
    selectedChallengeQuestion = [questionArray objectAtIndex:index];
    [self checkEnableSubmitButton];
}

-(void)didShow:(DropDownView *)dropDownView {
    [self endEditing:true];
}

#pragma mark - UITextField delegate


-(void) makeBecomeFirstResponderForTextField
{
    if(![self.emailTextField.text isValidEmail])
    {
        [self.emailTextField becomeFirstResponder];
    }
    else if([self.passwordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0) //(![self.passwordTextField.text isValidStrongPassword])
    {
        [self.passwordTextField becomeFirstResponder];
    }
    else if(![self.confirmPasswordTextField.text isEqualToString:self.passwordTextField.text])
    {
        [self.confirmPasswordTextField becomeFirstResponder];
    }
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.text = [textField.text removeRedudantWhiteSpaceInText];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.passwordTextField || textField == self.confirmPasswordTextField) {
        if ([string isEqualToString:@" "]) {
            return NO;
        }
    }
    return [self updateTextFiel:textField shouldChangeCharactersInRange:range replacementString:string];
}

-(void)checkTextFieldIsChange:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
}

- (void)textFieldDidChange:(UITextField *)textField {
    [self checkEnableSubmitButton];
}

-(BOOL)updateTextFiel:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
//    if(string.length > 1)
//    {
//        NSMutableString *newString = [[NSMutableString alloc] initWithString:[string removeRedudantWhiteSpaceInText]];
//        NSString *resultText = [textField.text stringByReplacingCharactersInRange:range
//                                                                       withString:string];
//        resultText = [resultText removeRedudantWhiteSpaceInText];
//
//
//        if(textField == self.passwordTextField || textField == self.confirmPasswordTextField)
//        {
//            /*if([newString isValidStrongPassword])//isValidStrongPassword
//            {
//                textField.text = newString;
//            }
//            else*/ if(string.length > 25)
//            {
//                textField.text = [newString substringToIndex:25];
//            }
//            else
//            {
//                textField.text = newString;
//            }
//        }
//
//
//        if(textField == self.emailTextField)
//        {
//            //textField.text = newString;
//            textField.text = (resultText.length > 100)?[resultText substringWithRange:NSMakeRange(0, 100)] : resultText;
//        }
//
//        return NO;
//    }
//
//
//    if(textField == self.passwordTextField || textField == self.confirmPasswordTextField)
//    {
//        if(textField.text.length == 25 && ![string isEqualToString:@""])
//        {
//            return NO;
//        }
//        return YES;
//    }
//
//    else if(textField == self.emailTextField)
//    {
//        if([textField.text occurrenceCountOfCharacter:'@'] == 1 && [string isEqualToString:@"@"]){
//            return NO;
//        }
//
//        return YES;
//    }
    
    if(textField == self.passwordTextField || textField == self.confirmPasswordTextField)
    {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if([newString length] > kPasswordInputMaxLength) {
            return NO;
        }
        
        return YES;
    }
    
    else if(textField == self.emailTextField)
    {
        if([textField.text occurrenceCountOfCharacter:'@'] == 1 && [string isEqualToString:@"@"]){
            return NO;
        }
        
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if([newString length] > kEmailInputMaxLength) {
            return NO;
        }
        
        return YES;
    }
    
    return YES;
}

-(void)checkEnableSubmitButton {
    bool is = [_emailTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0 && [_answerTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0 && [_passwordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0 && [_confirmPasswordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0 && selectedChallengeQuestion != nil;
    [_submitButton setEnabled:is];
    
//    self.submitButton.backgroundColor = is ? [AppColor highlightButtonColor] :[AppColor normalButtonColor];
}


#pragma mark - Action for button

-(IBAction)submitButtonTapped:(UIButton*)sender {
    [self dismissKeyboard];
    [self verifyData];
}



#pragma mark - Validate fields


-(void)verifyData {
    
    [_controller receiveInputWithInput:_emailTextField.text type:FPErrorInputTypeEmail];
    [_controller receiveInputWithInput:_answerTextField.text type:FPErrorInputTypeAnswer];
    [_controller receiveInputWithInput:_passwordTextField.text type:FPErrorInputTypePassword];
    [_controller receiveInputWithInput:_confirmPasswordTextField.text type:FPErrorInputTypeConfirmPassword];
    _controller.shouldCheckStrongPassword = true;
    
    NSMutableString *message = [[NSMutableString alloc] init];
    if (selectedChallengeQuestion == nil) {
        (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
        [message appendString:@"Please select a sercure question."];
    }
    [self setTextViewsDefaultBottomBolder];
    
    typeof(self) __weak _self = self;
    [_controller verifyDataWithUseLocalPasswordValidation:false response:^(NSArray<NSNumber *> * _Nullable listError) {
        if (!_self) return;
        
        typeof(self) __self = _self;
        if (listError.count == 0) {
            [_self updateEdgeInsetForHideKeyboard];
            [_self setTextViewsDefaultBottomBolder];
            [_self requestForgotPassword];
        } else {
            NSMutableArray* listTextfieldError = [NSMutableArray new];
            [listError enumerateObjectsUsingBlock:^(NSNumber * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSString* errorMsg = nil;
                switch ([obj intValue]) {
                    case FPErrorInputTypeEmail:
                        errorMsg = NSLocalizedString(@"input_invalid_email_msg", nil);
                        [listTextfieldError addObject:__self->_emailTextField];
                        break;
                    case FPErrorInputTypePassword:
                        errorMsg = NSLocalizedString(@"input_invalid_password_msg", nil);
                        [listTextfieldError addObject:__self->_passwordTextField];
                        break;
                    case FPErrorInputTypeConfirmPassword:
                        errorMsg = NSLocalizedString(@"input_invalid_confirm_password_msg", nil);
                        [listTextfieldError addObject:__self->_confirmPasswordTextField];
                        break;
                    case FPErrorInputTypeAnswer:
                        errorMsg = NSLocalizedString(@"input_invalid_answer_msg", nil);
                        [listTextfieldError addObject:__self->_answerTextField];
                        break;
                    default:
                        break;
                }
                (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
                [message appendString:errorMsg];
            }];
            [self showValidateErrorDialog:listTextfieldError message:message];
        }
    }];
}

- (void) showValidateErrorDialog:(NSArray *) listTextfieldError message: (NSString *) message {
    NSMutableString *newMessage = [[NSMutableString alloc] init];
    [newMessage appendString:@"\n"];
    [newMessage appendString:message];
    [newMessage appendString:@"\n"];
    NSArray* buttons = @[NSLocalizedString(@"ok_button_title", nil)];
    NSArray* actions = @[^(void) { [self onErrorDialogOK:listTextfieldError]; }];
    [self showAlertWithTitle:@"Please confirm that the value entered is correct:"
                     message:newMessage
                     buttons:buttons
                     actions:actions
            messageAlignment:NSTextAlignmentLeft];
}

- (void) onErrorDialogOK:(NSArray *) listTextfieldError {
    [listTextfieldError enumerateObjectsUsingBlock:^(UITextField*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj setBottomBorderRedColor];
    }];
    if (listTextfieldError.count > 0) {
        [listTextfieldError.firstObject becomeFirstResponder];
    }
}

#pragma mark - API

-(void)requestForgotPassword {
    [self startActivityIndicator];
    __weak typeof(self) _self = self;
    [_controller submitForgetPasswordWithEmail:_emailTextField.text
                                      question:[[AAFRecoveryQuestion alloc] initWithQuestion:selectedChallengeQuestion.questionText answer:_answerTextField.text]
                                   newPassword:[[AAFPassword alloc] initWithPassword:_passwordTextField.text]
                                    completion:^(BOOL isSucces, NSString * _Nullable message, AspireApiErrorType errorType) {
//        NSUInteger errorType = NETWORK_UNAVAILABLE_ERR;
//    }];
//
//    [_controller requestForgetPasswordWithEmail:_emailTextField.text
//                                       question:[[AAFRecoveryQuestion alloc] initWithQuestion:selectedChallengeQuestion.questionText answer:_answerTextField.text]
//                                    newPassword:[[AAFPassword alloc] initWithPassword:_passwordTextField.text]
//                                       complete:^(NSError * error) {
                                           if(!_self) return;
                                           
                                           typeof(self) __self = _self;
                                           [_self stopActivityIndicator];
                                           if (isSucces == true) {
                                               [_self showAlertWithTitle:nil
                                                                 message:@"Your password has been successfully updated!"
                                                                 buttons:@[NSLocalizedString(@"ok_button_title", nil)]
                                                                 actions:@[^{
                                                   [__self->_controller successRedirect];
                                               }]
                                                        messageAlignment:NSTextAlignmentCenter];
                                               
                                           } else {
                                               UITextField *currentTextField = nil;
                                               NSString* msg = @"Enter a valid email address.";
//                                               switch ([AspireApiError getErrorTypeAPIForgetPasswordFromError:error]) {
                                               switch (errorType) {
                                                   case NETWORK_UNAVAILABLE_ERR:
                                                       msg = @"";
                                                       [_self showErrorNetwork];
                                                       break;
                                                   case OKTA_LOCKED_ERR:
                                                       [_self showAlertWithTitle:NSLocalizedString(@"alert_error_title", nil)
                                                                         message:@"We're sorry. Please contact us at 877-288-6503 for assistance accessing this account."
                                                                         buttons:@[NSLocalizedString(@"alert_ok_button", nil)]
                                                                         actions:nil
                                                                messageAlignment:NSTextAlignmentCenter];
                                                       break;
                                                   case EMAIL_INVALID_ERR: {
                                                       [__self onEmailInvalidError: currentTextField];
                                                       break;
                                                   }
                                                   case RECOVERYQUESTION_ERR:
                                                   case ANSWERQUESTION_ERR: {
                                                       [__self onAnswerQuestionError: currentTextField];
                                                       break;
                                                   }
                                                    case FORGETPW_USER_NOT_ALLOWED_ERR: {
                                                       currentTextField = __self->_emailTextField;
                                                    
                                                       [_self showAlertWithTitle:NSLocalizedString(@"alert_error_title", nil)
                                                                         message:@"Forgot password not allowed on specified user."
                                                                         buttons:@[NSLocalizedString(@"alert_ok_button", nil)]
                                                                         actions:@[^{
                                                           [currentTextField becomeFirstResponder];
                                                           [currentTextField setBottomBorderRedColor];
                                                           [_self checkEnableSubmitButton];
                                                       }]
                                                                messageAlignment:NSTextAlignmentCenter];
                                                       break;
                                                   }
                                                       break;
                                                    case FORGETPW_ERR:
                                                    case PW_ERR:
                                                   {
                                                       currentTextField = __self->_passwordTextField;
                                                       
                                                       [_self showAlertWithTitle:@"Password requirements were not met"
                                                                         message:NSLocalizedString(@"input_invalid_password_msg", nil)
                                                                         buttons:@[NSLocalizedString(@"alert_ok_button", nil)]
                                                                         actions:@[^{
                                                           [currentTextField becomeFirstResponder];
                                                           [currentTextField setBottomBorderRedColor];
                                                           [_self checkEnableSubmitButton];
                                                       }]
                                                                messageAlignment:NSTextAlignmentCenter];
                                                       break;
                                                   }
                                                   default:
                                                       [_self showApiErrorWithMessage:@""];
                                                       break;
                                               }
                                           }
                                       }];
}

- (NSArray *) yesNoButtons {
    NSArray* buttons = @[ NSLocalizedString(@"no_button", nil),
                          NSLocalizedString(@"yes_button", nil)];
    return buttons;
}
- (void) onAnswerQuestionError: (UITextField *) currentTextField {
    currentTextField = _answerTextField;
    [self.answerTextField setBottomBorderRedColor];
    void(^yesAction)(void) = ^{
        self.confirmPasswordTextField.text = @"";
        self.answerTextField.text = @"";
        self.passwordTextField.text = @"";
        [self clearQuestionView];
        [self setTextViewsDefaultBottomBolder];
        [self checkEnableSubmitButton];
    };
    void(^noAction)(void) = ^{
        [self.controller successRedirect];
    };
    [self showAlertWithTitle:NSLocalizedString(@"alert_error_title", nil)
                      message:@"The answer to the Security question is not correct. Would you like to try again?"
                      buttons:[self yesNoButtons]
                      actions:@[noAction, yesAction]
             messageAlignment:NSTextAlignmentCenter];
}

- (void) clearQuestionView {
    self.questionDropdown.title = @"Security Question";
    self.questionDropdown.titleColor = [AppColor placeholderTextColor];
    selectedChallengeQuestion = nil;
}

- (void) onEmailInvalidError: (UITextField *) currentTextField {
    [self.emailTextField setBottomBorderRedColor];
    currentTextField = self.emailTextField;
    
    void(^yesAction)(void) = ^{
        self.emailTextField.text = @"";
        self.answerTextField.text = @"";
        self.passwordTextField.text = @"";
        self.confirmPasswordTextField.text = @"";
        [self clearQuestionView];
        [self setTextViewsDefaultBottomBolder];
        [self checkEnableSubmitButton];
    };
    void(^noAction)(void) = ^{
        [self.controller successRedirect];
    };
    NSArray* actions = @[yesAction, noAction];
    [self showAlertWithTitle:NSLocalizedString(@"alert_error_title", nil)
                     message:@"A user account with this email address cannot be found. Would you like to try again?"
                     buttons:[self yesNoButtons]
                     actions:actions
            messageAlignment:NSTextAlignmentCenter];
}
@end
