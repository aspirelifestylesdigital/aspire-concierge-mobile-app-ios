//
//  UtilStyle.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/20/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "UtilStyle.h"
#import "Common.h"
#import "Constant.h"

@implementation UtilStyle

+(NSAttributedString *) setTextStyleForTextFieldWithMessage:(NSString *)text
{
    return [[NSAttributedString alloc] initWithString:(text ? text : @"") attributes:@{NSForegroundColorAttributeName: [AppColor textColor], NSFontAttributeName:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]]}];
}


+(NSAttributedString *) setPlaceHolderTextStyleForTextFieldWithMessage:(NSString *)text
{
    return [[NSAttributedString alloc] initWithString:(text ? text : @"") attributes:@{NSForegroundColorAttributeName: [AppColor placeholderTextColor],
        NSFontAttributeName:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]]}];
}

+(NSAttributedString *) setTextStyleForTitleViewControllerWithMessage:(NSString *)text
{
    NSRange range = NSMakeRange(0, text.length);
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:text];
    [attributeString addAttribute:NSKernAttributeName value:@(1.6) range:range];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize headerTextSize]] range:range];
    [attributeString addAttribute:NSForegroundColorAttributeName value:[AppColor titleViewControllerColor] range:range];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init] ;
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    paragraphStyle.lineSpacing = [AppSize titleLineSpacing];
    [attributeString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    
    return attributeString;
}

+(NSAttributedString *) setLargeSizeStyleForLabelWithMessage:(NSString *)text
{
    NSRange range = NSMakeRange(0, text.length);
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:text];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize titleTextSize]] range:range];
    [attributeString addAttribute:NSForegroundColorAttributeName value:[AppColor normalTextColor] range:range];
    
    return attributeString;
}
@end
