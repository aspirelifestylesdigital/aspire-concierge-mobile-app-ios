//
//  BaseComponentView.m
//  MobileConciergeUSDemo
//
//  Created by Dai on 8/8/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "BaseComponentView.h"
#import "AlertViewController.h"
#import "Constant.h"

@interface BaseComponentView () {
    UIView                              *spinnerBackground;
    UIActivityIndicatorView             *spinner;
    UIImageView                         *indicatorImageView;
    BOOL                                showingSpinner;
    UIView                              *maskViewForSpinner;
}

@end

@implementation BaseComponentView
@synthesize vc,view;

#pragma mark - API
- (void)showErrorNetwork {
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"cannot_get_data", nil);
    alert.msgAlert = NSLocalizedString(@"no_network_connection", nil);
    alert.secondBtnTitle = NSLocalizedString(@"settings_button", nil);
    alert.blockSecondBtnAction = ^(void){
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"prefs:root"]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root"]];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root"]];
        }
    };
    alert.firstBtnTitle = NSLocalizedString(@"alert_ok_button", nil);
    
    alert.blockFirstBtnAction = ^(void){
        [self stopActivityIndicator];
    };
    alert.providesPresentationContextTransitionStyle = YES;
    alert.definesPresentationContext = YES;
    [alert setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [vc presentViewController:alert animated:YES completion:nil];
    
}

- (void)showApiErrorWithMessage:(NSString*)message{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"cannot_get_data", nil);
    if ([message isEqualToString:@""]) {
        message = NSLocalizedString(@"api_common_error_message", nil);
    }
    
    alert.msgAlert = message;
    alert.secondBtnTitle = NSLocalizedString(@"settings_button", nil);
    alert.blockSecondBtnAction = ^(void){
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"prefs:root"]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root"]];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root"]];
        }
    };
    alert.firstBtnTitle = NSLocalizedString(@"alert_ok_button", nil);
    
    alert.blockFirstBtnAction = ^(void){
        [self stopActivityIndicator];
    };
    alert.providesPresentationContextTransitionStyle = YES;
    alert.definesPresentationContext = YES;
    [alert setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [vc presentViewController:alert animated:YES completion:nil];
}

- (void) checkCreateMaskIndicator {
    
    BOOL isExistMask = [self.subviews containsObject:maskViewForSpinner];
    
    if(!maskViewForSpinner) {
        UIView *maskView = [[UIView alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT)];
        maskView.backgroundColor = [UIColor clearColor];
        maskView.alpha = 1;
        maskViewForSpinner = maskView;
    }
    
    if(!isExistMask)
        [self addSubview:maskViewForSpinner];
    
    BOOL isExist = [self.subviews containsObject:indicatorImageView];
    
    if(!isExist) {
        [self addSubview:[self createIndicator]];
        indicatorImageView.translatesAutoresizingMaskIntoConstraints = NO;
        [indicatorImageView.superview addConstraint:[NSLayoutConstraint constraintWithItem:indicatorImageView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:indicatorImageView.superview attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
        [indicatorImageView.superview addConstraint:[NSLayoutConstraint constraintWithItem:indicatorImageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:indicatorImageView.superview attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        [indicatorImageView addConstraint:[NSLayoutConstraint constraintWithItem:indicatorImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:SPINNER_BACKGROUND_WIDTH*SCREEN_SCALE]];
        [indicatorImageView addConstraint:[NSLayoutConstraint constraintWithItem:indicatorImageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:SPINNER_BACKGROUND_HEIGHT*SCREEN_SCALE]];
    }
    
    [self bringSubviewToFront:maskViewForSpinner];
    [self bringSubviewToFront:indicatorImageView];
}

- (void)startActivityIndicator
{
    printf("%s",[[NSString stringWithFormat:@"%s\n",__PRETTY_FUNCTION__] UTF8String]);
    dispatch_async(dispatch_get_main_queue(), ^{
        if (showingSpinner) {
            return;
        }
        
        [self checkCreateMaskIndicator];
        
        [maskViewForSpinner setHidden:NO];
        [indicatorImageView setHidden:NO];
        
        //Start the animation
        [indicatorImageView startAnimating];
        
        showingSpinner = YES;
    });
}

- (void)startActivityIndicatorWithoutMask
{
    printf("%s",[[NSString stringWithFormat:@"%s\n",__PRETTY_FUNCTION__] UTF8String]);
    dispatch_async(dispatch_get_main_queue(), ^{
        if (showingSpinner) {
            return;
        }
        
        [self checkCreateMaskIndicator];
        
        [maskViewForSpinner setHidden:YES];
        [indicatorImageView setHidden:NO];
        
        [indicatorImageView startAnimating];
        showingSpinner = YES;
    });
}

- (void)stopActivityIndicatorWithoutMask
{
    
    [self stopActivityIndicator];
}

- (void)stopActivityIndicator
{
    printf("%s",[[NSString stringWithFormat:@"%s\n",__PRETTY_FUNCTION__] UTF8String]);
    dispatch_async(dispatch_get_main_queue(), ^{
        //        [indicatorImageView.layer removeAllAnimations];
        [maskViewForSpinner setHidden:YES];
        [indicatorImageView setHidden:YES];
        showingSpinner = NO;
    });
    
}

- (void) showAlertWithTitle:(NSString*)title
                    message:(NSString*)message
                    buttons:(NSArray*)buttons
                    actions:(NSArray*)actions
           messageAlignment:(NSTextAlignment) textAlign {
    AlertViewController *alert = [[AlertViewController alloc] init];
    if (title)
        alert.titleAlert = title;
    if (message)
        alert.msgAlert = message;
    alert.firstBtnTitle = buttons.firstObject;
    if (buttons.count > 1)
        alert.secondBtnTitle = buttons.lastObject;
    
    if (buttons.count == 1) {
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;
            alert.lblAlertMessage.textAlignment = textAlign;
            [alert.view layoutIfNeeded];
        });
    }
    
    if (actions.count > 0) {
        alert.blockFirstBtnAction = actions.firstObject;
        alert.blockSecondBtnAction = actions.lastObject;
    }
    
    alert.providesPresentationContextTransitionStyle = true;
    alert.definesPresentationContext = true;
    alert.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    if (vc.navigationController) {
        [vc.navigationController presentViewController:alert animated:false completion:nil];
    } else {
        [vc presentViewController:alert animated:false completion:nil];
    }
}

#pragma mark - PRIVATE
- (UIImageView*) createIndicator{
    if (!indicatorImageView) {
        indicatorImageView = [[UIImageView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - SPINNER_BACKGROUND_WIDTH)/2.0,
                                                                           (SCREEN_HEIGHT - SPINNER_BACKGROUND_HEIGHT-NAVIGATION_HEIGHT)/2.0,
                                                                           SPINNER_BACKGROUND_WIDTH*SCREEN_SCALE, SPINNER_BACKGROUND_HEIGHT*SCREEN_SCALE)];
        
        [indicatorImageView setImage:[UIImage imageNamed:@"activity_indicator_icon_1"]];
        //Add more images which will be used for the animation
        indicatorImageView.animationImages =  [NSArray arrayWithObjects:
                                               [UIImage imageNamed:@"activity_indicator_icon_1"],
                                               [UIImage imageNamed:@"activity_indicator_icon_2"],
                                               [UIImage imageNamed:@"activity_indicator_icon_3"],
                                               [UIImage imageNamed:@"activity_indicator_icon_4"],
                                               [UIImage imageNamed:@"activity_indicator_icon_5"],
                                               [UIImage imageNamed:@"activity_indicator_icon_6"],
                                               [UIImage imageNamed:@"activity_indicator_icon_7"],
                                               [UIImage imageNamed:@"activity_indicator_icon_8"],
                                               [UIImage imageNamed:@"activity_indicator_icon_9"],
                                               [UIImage imageNamed:@"activity_indicator_icon_10"],
                                               [UIImage imageNamed:@"activity_indicator_icon_11"],
                                               [UIImage imageNamed:@"activity_indicator_icon_12"],
                                               nil];
        indicatorImageView.animationDuration = 1.0f;
    }
    
    return indicatorImageView;
}

#pragma mark - INIT
- (void) config {
    
}

- (void) loadNIb {
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
    [self addSubview:view];
    view.translatesAutoresizingMaskIntoConstraints = false;
    [view.topAnchor constraintEqualToAnchor:self.topAnchor].active = true;
    [view.bottomAnchor constraintEqualToAnchor:self.bottomAnchor].active = true;
    [view.trailingAnchor constraintEqualToAnchor:self.trailingAnchor].active = true;
    [view.leadingAnchor constraintEqualToAnchor:self.leadingAnchor].active = true;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self loadNIb];
        [self config];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self loadNIb];
        [self config];
    }
    return self;
}
@end
