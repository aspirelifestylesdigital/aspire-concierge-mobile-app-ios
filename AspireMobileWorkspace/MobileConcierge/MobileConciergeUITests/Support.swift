//
//  Support.swift
//  Personal Concierge ServiceUITests
//
//  Created by dai on 8/28/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

import XCTest

extension XCUIElement {
    static func button(app:XCUIApplication, id:String) ->XCUIElement {
        return app.buttons.element(matching: .button, identifier: id)
    }
    
    static func label(app:XCUIApplication,id:String) ->XCUIElement {
        return app.staticTexts.element(matching: .staticText, identifier: id)
    }
    
    static func textField(app:XCUIApplication,id:String,isSecure:Bool) ->XCUIElement {
        return isSecure ? app.secureTextFields.element(matching: .secureTextField, identifier: id) : app.textFields.element(matching: .textField, identifier: id)
    }
    
    static func input(app:XCUIApplication,id:String, value:String, isSecure:Bool) {
        if isSecure {
            app.secureTextFields[id].tap()
        } else {
            app.textFields[id].tap()
        }
        
        if isSecure {
            if let text = app.secureTextFields[id].value as? String {
                let deleteString = text.characters.map { _ in XCUIKeyboardKey.delete.rawValue }.joined(separator: "")
                app.secureTextFields[id].typeText(deleteString)
            }
            app.secureTextFields[id].typeText(value)
        } else {
            if let text = app.textFields[id].value as? String {
                let deleteString = text.characters.map { _ in XCUIKeyboardKey.delete.rawValue }.joined(separator: "")
                app.textFields[id].typeText(deleteString)
            }
            app.textFields[id].typeText(value)
        }
    }
    
    static func checkButtonIsExist(app:XCUIApplication,id:String, onwer:XCTestCase) -> Bool{
        let btn = app.buttons.element(matching: .button, identifier: id)
        
        let predicate = NSPredicate(format: "exists == 1")
        let expect = onwer.expectation(for: predicate, evaluatedWith: btn, handler: nil)
        
        let resultOK = XCTWaiter().wait(for: [expect], timeout: 1)
        
        return resultOK == .completed
    }
    
    static func checkDropdownIsDefault(app:XCUIApplication,id:String, onwer:XCTestCase) -> Bool {
        let btn = app.staticTexts[id]
        
        let predicate = NSPredicate(format: "exists == 1")
        let expect = onwer.expectation(for: predicate, evaluatedWith: btn, handler: nil)
        
        let resultOK = XCTWaiter().wait(for: [expect], timeout: 1)
        
        return resultOK == .completed
    }
}
