//
//  MobileConciergeTests.m
//  MobileConciergeTests
//
//  Created by dai on 9/18/18.
//  Copyright © 2018 s3Corp. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSString+Utis.h"

@interface MobileConciergeTests : XCTestCase

@end

@implementation MobileConciergeTests

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.continueAfterFailure = true;
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testExample {
    
    XCTAssertFalse([@"1234" isValidStrongPassword]);
    [@[@"\"", @"!", @"#", @"$", @"%", @"&", @"’", @"(", @")", @"*", @"+", @",", @"-", @".", @"/", @":", @";", @"<", @"=", @">", @"?", @"@", @"[", @"]", @"^", @"_", @"`", @"{", @"|", @"}", @"~"] enumerateObjectsUsingBlock:^(NSString*  _Nonnull symbol, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString* sample = [@"[__]Admin1234" stringByReplacingOccurrencesOfString:@"[__]" withString:symbol];
        [XCTContext runActivityNamed:sample block:^(id<XCTActivity>  _Nonnull activity) {
            XCTAssertTrue([sample isValidStrongPassword]);
        }];
    }];
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
