#
# Be sure to run `pod lib lint AspireApiControllers.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = 'AspireApiControllers'
s.version          = '0.1.29'
    s.summary          = 'A short description of AspireApiControllers.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
  - 0.1.2: Log all view did load message for AACBaseController.swift
  - 0.1.3: Add password validation rules
                       DESC

  s.homepage         = 'git@gitlab.s3corp.com.vn:vuong.tran/AspireApiControllers.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'vuongtrancong' => 'vuong.tran@s3corp.com.vn' }
  s.source           = { :git => 'git@gitlab.s3corp.com.vn:vuong.tran/AspireApiControllers.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

#  s.ios.deployment_target = '8.0'

  s.source_files = 'AspireApiControllers/Classes/**/*'
  
   s.resource_bundles = {
     'AspireApiControllers' => ['AspireApiControllers/Assets/**/*']
   }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'AspireApiFramework' #, :path => '../AspireApiFramework'
  s.swift_version = '4.2'
  s.ios.deployment_target  = '9.0'
end
