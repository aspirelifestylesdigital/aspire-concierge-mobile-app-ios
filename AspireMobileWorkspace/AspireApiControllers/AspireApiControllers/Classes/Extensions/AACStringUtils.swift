//
//  AACStringUtils.swift
//  AspireApiControllers
//
//  Created by VuongTC on 10/30/18.
//

import UIKit

@objc public class AACStringUtils: NSObject {
    @objc public static func isEmpty(string: String?) -> Bool {
        if string == nil {
            return true
        }
        if string?.isEmptyOrWhitespace() == true {
            return true
        }
        if string?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == "" {
            return true
        }
        return false
    }
    
    @objc public static func isNumeric(string: String?) -> Bool
    {
        if string == nil {
            return false
        }
        if string?.isEmptyOrWhitespace() == true {
            return false
        }
        let number = Int(string!)
        return number != nil
    }
}

@objc public extension NSString {
    @objc public func isValidEmail() -> Bool {
        let string = self as String
        return string.isValidEmail(stricterFilter: true)
    }
}

public extension String {
    func isEmptyOrWhitespace() -> Bool {
        if(self.isEmpty) {
            return true
        }
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""
    }
    
    func isValidEmail(stricterFilter: Bool = true) -> Bool{
        if self.count > 100 {
            return false
        }
        
        let stricterFilterString = AACManager.shared.value(for: "stricterFilterString") ?? "" // "^[A-Z0-9a-z\\._%+-]+@{1}([A-Za-z0-9-]+\\.)+[A-Za-z]{2,96}$"
        
//        let stricterFilterString = "^[A-Z0-9a-z\\._%+-]+@{1}([A-Za-z0-9-]+\\.)+[A-Za-z]{2,}$"

        let laxString = "^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$"
        let emailRegex = stricterFilter ? stricterFilterString : laxString
        let emailTest:NSPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        
        let result = emailTest.evaluate(with:self)
        return result
    }
    
    func isContainPartOfString(string: String) -> Bool {
        let words = string.components(separatedBy: " ")
        let foundWords = words.filter { (word) -> Bool in
            return self.contains(word)
        }
        return foundWords.count > 0
    }

}
