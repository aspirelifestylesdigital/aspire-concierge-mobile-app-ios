//
//  Extensions.swift
//  AspireApiControllers
//
//  Created by Dai on 8/10/18.
//  Copyright © 2018 S3Corp. All rights reserved.
//

import UIKit

func log(message: String = "", filePath: String = #file, line: Int = #line, function: String = #function) {
    #if DEBUG
    var threadName = ""
    threadName = Thread.current.isMainThread ? "MAIN THREAD" : (Thread.current.name ?? "UNKNOWN THREAD")
    threadName = "[" + threadName + "] "
    
    let fileName = NSURL(fileURLWithPath: filePath).deletingPathExtension?.lastPathComponent ?? "???"
    
    var msg = ""
    if message != "" {
        msg = " - \(message)"
    }
    
    NSLog("-- " + threadName + fileName + "(\(line))" + " -> " + function + msg)
    #endif
}

func NSLocalizedStringAAC(string: String) -> String {
    let bundle = Bundle(for: AACBaseController.classForCoder())
    let localizedString = NSLocalizedString(string, tableName: nil, bundle: bundle, value: "", comment: "")
    return localizedString
}
