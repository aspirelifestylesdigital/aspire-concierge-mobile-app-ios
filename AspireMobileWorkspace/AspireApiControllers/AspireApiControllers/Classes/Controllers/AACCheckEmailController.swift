//
//  AACForgotPasswordController.swift
//  AspireApiControllers
//
//  Created by Dai on 8/8/18.
//  Copyright © 2018 S3Corp. All rights reserved.
//

import UIKit
import AspireApiFramework

public class AACCheckEmailController: AACBaseController {

    
    // MARK: - API
    @objc public func checkEmailOKTA(email:String,complete:@escaping ((Bool)->Void)) {
        ModelAspireApiManager.getProfileFromEmail(email) {[weak self] (oktaUser, user, error) in
            guard let _ = self else {return}
            DispatchQueue.main.async {
                complete(error == nil)
            }
        }
    }
    
    @objc public func getProfilePartyPMA(email:String,complete:@escaping ((PMAUser?,NSError?)->Void)) {
        ModelAspireApiManager.getProfilePartyPMA(email, completion: {[weak self] (pmaUser, error) in
            guard let _ = self else {return}
            DispatchQueue.main.async {
                complete(pmaUser,error as NSError?)
            }
        })
    }
    
    @objc public func receiveInput(input:String,type:FPErrorInputType) {
        print("\(#function) => \(FPErrorInputType.self) \(input)")
        switch type {
        case .email:
            email = input
        default:
            print("\(#function) out of enum FPErrorInputType")
        }
    }
    
    @objc public func successRedirect() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc public func verifyData(response:(([Int]?)->Void)) {
        var data:[Int] = []
        if let e = email {
            if !e.isValidEmail() || e.trimmingCharacters(in: .whitespacesAndNewlines).characters.count <= 0 {
                data.append(FPErrorInputType.email.rawValue)
            }
        }
        print("\(#function) => list Error\(data)")
        response(data)
    }
    
    // MARK: - PRIVATE
    deinit {
        print("\(#function)")
    }
    
    // MARK: - properties
    var email:String?
}
