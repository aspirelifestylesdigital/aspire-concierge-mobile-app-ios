//
//  APFPasscodeController.swift
//  AspireApiFramework
//
//  Created by Dai on 7/12/18.
//  Copyright © 2018 com.aspirelifestyles.ios.mobileconcierge.UnitTest. All rights reserved.
//

import UIKit
import AspireApiFramework

public class AACPasscodeController: AACBaseController {
    
    // MARK: - API
    @objc public func submit(passcode:String,complete:@escaping ((Bool,String?,NSError?)->Void)) {
        ModelAspireApiManager.verifyBin(passcode) { (isPass, bin, error) in
            complete(isPass,bin,error as? NSError)
        }
    }
    
    // MARK: - INIT
    override public func viewDidLoad() {
        super.viewDidLoad()
        title = "FORGOT PASSWORD";
    }
    
    // MARK: -  properties
}
