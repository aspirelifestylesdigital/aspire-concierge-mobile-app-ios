//
//  ConfigController.swift
//  AspireApiControllers
//
//  Created by VuongTC on 11/7/18.
//

import UIKit

@objc public enum AppName: Int {
    case ms
    case uda
    case mc
}

var configFileName: String?

@objc public class AACManager: NSObject {
    static let shared = AACManager()
    var config: [String: Any]?
    
    public override init() {
        super.init()
        if AACStringUtils.isEmpty(string: configFileName) {
            AACManager.configAppName(name: AppName.ms)
        }
        loadConfigFromFile(configFile: configFileName ?? "ms_config")
    }
    
    @objc public static func configAppName(name: AppName) {
        switch name {
        case .mc:
            configFileName = "mc_config"
        case .uda:
            configFileName = "uda_config"
        default:
            configFileName = "ms_config"
        }
    }
    
    func loadConfigFromFile(configFile: String) {
        let bundle = Bundle.init(for: self.classForCoder)
        if let bundleURL = bundle.resourceURL?.appendingPathComponent("AspireApiControllers.bundle") {
            let resourceBundle = Bundle.init(url: bundleURL)
            if let filePath = resourceBundle?.path(forResource: configFile, ofType: "json") {
                do {
                    let json = try String(contentsOfFile: filePath)
                    if let data = json.data(using: String.Encoding.utf8) {
                        config = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? [String : Any]
                    }
                } catch {
                    
                }
            }
        }
    }
    
    func value(for key: String) -> String? {
        if let config = config {
            return config[key] as? String
        }
        return nil
    }
}
