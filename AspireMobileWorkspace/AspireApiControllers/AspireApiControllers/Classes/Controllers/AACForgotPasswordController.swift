//
//  AACForgotPasswordController.swift
//  AspireApiControllers
//
//  Created by Dai on 8/8/18.
//  Copyright © 2018 S3Corp. All rights reserved.
//

import UIKit
import AspireApiFramework

public typealias forgotPassCompletion = (_ isSuccess: Bool, _ message: String?, _ errorType: AspireApiErrorType) -> Void

public class AACForgotPasswordController: AACBaseController {
    @objc public var onDoneForgotPassword: ((_ email: String, _ success: Bool) -> Void)?
    
    // MARK: - API
    @objc public func requestForgetPassword(email:String,
                                            question:AAFRecoveryQuestion,
                                            newPassword:AAFPassword,
                                            complete:@escaping ((NSError?)->Void)) {
        isForgotPasswordSucess = false
        ModelAspireApiManager.forgotPassword(forEmail: email, with: question, newPassword: newPassword) {[weak self] (error) in
            guard let _ = self else {return}
            self?.isForgotPasswordSucess = (error == nil)
            DispatchQueue.main.async {
                complete(error as NSError?)
            }
        }
    }
    
    @objc public func submitForgetPassword(email:String,
                                           question:AAFRecoveryQuestion,
                                           newPassword:AAFPassword,
                                           completion:@escaping (forgotPassCompletion)) {
        self.requestForgetPassword(email: email, question: question, newPassword: newPassword) { (error) in
            if let error = error {
                self.onForgetPasswordError(error: error, completion: completion)
            } else {
                completion(true, NSLocalizedStringAAC(string: "Your password has been successfully updated!"), NONE)
            }
        }
    }
    
    func onForgetPasswordError(error: NSError ,completion:@escaping (forgotPassCompletion)) {
        let errorType = AspireApiError.getErrorTypeAPIForgetPassword(fromError: error)
        switch errorType {
        case NETWORK_UNAVAILABLE_ERR:
            completion(false, "", errorType)
        case OKTA_LOCKED_ERR:
            completion(false, NSLocalizedStringAAC(string: "We're sorry. Please contact us at 877-288-6503 for assistance accessing this account."), errorType)
        case EMAIL_INVALID_ERR:
            completion(false, NSLocalizedStringAAC(string: "A user account with this email address cannot be found. Would you like to try again?"), errorType)
        case RECOVERYQUESTION_ERR, ANSWERQUESTION_ERR:
            completion(false, NSLocalizedStringAAC(string: "The answer to the Security question is not correct. Would you like to try again?"), errorType)
        case FORGETPW_USER_NOT_ALLOWED_ERR:
            completion(false, NSLocalizedStringAAC(string: "Forgot password not allowed on specified user."), errorType)
        case FORGETPW_ERR, PW_ERR:
            completion(false, NSLocalizedStringAAC(string: "Password requirements were not met. Password must be at least 10 characters and contain at least 1 upper case, 1 lower case, 1 number and 1 special character !@#$%^&*. It cannot contain any part of your user name or be one of your last 12 passwords."), errorType)
        default:
            completion(false, "", errorType)
        }
    }
    
    @objc public func receiveInput(input:String,type:FPErrorInputType) {
        print("\(#function) => \(FPErrorInputType.self) \(input)")
        switch type {
        case .email:
            email = input
        case .answer:
            answer = input
        case .password:
            password = input
        case .confirmPassword:
            confirmPassword = input
        default:
            print("\(#function) out of enum FPErrorInputType")
        }
    }
    
    @objc public func successRedirect() {
        self.navigationController?.popViewController(animated: true)
        if (self.onDoneForgotPassword != nil) {
            self.onDoneForgotPassword?(self.email ?? "", self.isForgotPasswordSucess)
        }
    }
    
    @objc public func verifyData(response:(([Int]?)->Void)) {
        verifyData(useLocalPasswordValidation: true) { (data) in
            response(data)
        }
    }
    
    @objc public func verifyData(useLocalPasswordValidation: Bool, response:(([Int]?)->Void)) {
        var data:[Int] = []
        if let e = answer {
            if e.trimmingCharacters(in: .whitespacesAndNewlines).count < 4 {
                data.append(FPErrorInputType.answer.rawValue)
            }
        }
        
        if let e = email {
            if !e.isValidEmail() || e.trimmingCharacters(in: .whitespacesAndNewlines).count <= 0 {
                data.append(FPErrorInputType.email.rawValue)
            }
        }
        
        if let pass = password, let cPass = confirmPassword {
            if useLocalPasswordValidation == true {
                let validatePasswords = PasswordValidation.validate(password: pass, confirmPassword: cPass, checkStrongPassword: shouldCheckStrongPassword, notContainNames: [lastName, firstName, turnEmailToName()])
                data.append(contentsOf: validatePasswords)
            } else {
                if pass != cPass {
                    data.append(FPErrorInputType.confirmPassword.rawValue)
                }
            }
        }
        
        print("\(#function) => list Error\(data)")
        response(data)
    }
    
    func turnEmailToName() -> String {
        if AACStringUtils.isEmpty(string: email) {
            return ""
        }
        let breakEmail = email?.components(separatedBy: "@")
        if let userName = breakEmail?.first {
            return userName
        }
        return ""
    }
    
    // MARK: - PRIVATE
    deinit {
        print("\(#function)")
    }
    
    // MARK: - properties
    var email:String?
    var isForgotPasswordSucess: Bool = false
    var confirmPassword:String?
    var password:String?
    var answer:String?
    @objc public var firstName:String = ""
    @objc public var lastName:String = ""
    @objc public var shouldCheckStrongPassword = false;
}
