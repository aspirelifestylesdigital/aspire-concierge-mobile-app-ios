//
//  AACBaseController.swift
//  AspireApiControllers
//
//  Created by Dai on 7/12/18.
//  Copyright © 2018 S3Corp. All rights reserved.
//

import UIKit

@objc public enum FPErrorInputType:Int {
    case email = 456
    case password = 457
    case confirmPassword = 458
    case answer = 459
    case passwordContainName = 460
}

@objc open class AACBaseController: UIViewController {

    // MARK: - public
    func addview(_ sub:UIView) {
        view.addSubview(topView)
        topView.translatesAutoresizingMaskIntoConstraints = false
        topView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        topView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        topView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        topView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        view.addSubview(sub)
        sub.translatesAutoresizingMaskIntoConstraints = false
        sub.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        sub.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        if #available(iOS 11.0, *) {
            sub.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
            sub.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            sub.topAnchor.constraint(equalTo: view.topAnchor, constant: 20).isActive = true
            sub.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }
        
        topView.backgroundColor = sub.subviews.first?.backgroundColor
    }
    
    @objc public func show(alert:UIViewController,forNavigation:Bool) {
        alert.providesPresentationContextTransitionStyle = true
        alert.definesPresentationContext = true
        alert.modalPresentationStyle = .overCurrentContext
        if(self.navigationController?.presentedViewController == nil) {
            if forNavigation {
                self.navigationController?.present(alert, animated: false, completion: nil)
            } else {
                self.present(alert, animated: false, completion: nil)
            }
        }
    }
    
    // MARK: - INIT
    override open func viewDidLoad() {
        super.viewDidLoad()
        self.onViewDidLoad?()
        log(message: "\(self)")
    }

    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.onViewWillAppear?()
    }
    
    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.onViewDidAppear?()
    }
    
    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.onViewWillDisappear?()
    }
    
    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.onViewDidDisappear?()
    }
    
    deinit {
        log(message: "\(self)")
    }
    
    @objc public weak var subview:UIView? {
        didSet {
            if let sub = subview {
                self.addview(sub)
            }
        }
    }
    
    // MARK: - properties
    @objc public let topView: UIView = UIView()
    
    // MARK: - clourse
    @objc public var onViewDidLoad:(()->Void)?
    @objc public var onViewWillAppear:(()->Void)?
    @objc public var onViewDidAppear:(()->Void)?
    @objc public var onViewWillDisappear:(()->Void)?
    @objc public var onViewDidDisappear:(()->Void)?
}
