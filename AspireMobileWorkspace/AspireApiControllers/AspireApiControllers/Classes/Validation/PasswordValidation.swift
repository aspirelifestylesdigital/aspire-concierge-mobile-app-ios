//
//  PasswordValidation.swift
//  AspireApiControllers
//
//  Created by VuongTC on 10/29/18.
//

import UIKit

@objc public class PasswordValidation: NSObject {
    /**
     Validate password
     return: Error code array
     **/
    static public func validate(password: String, confirmPassword: String, checkStrongPassword: Bool, notContainNames: [String] = []) -> [Int] {
        var data:[Int] = []
        
        if checkStrongPassword {
            if isStrongPassword(password: password) == false {
                data.append(FPErrorInputType.password.rawValue)
            }
            let isContainNames = isContainAnyPartOfNames(password: password, names: notContainNames)
            if isContainNames == true {
                data.append(FPErrorInputType.passwordContainName.rawValue)
            }
        }
        
        if password != confirmPassword {
            data.append(FPErrorInputType.confirmPassword.rawValue)
        }
        return data
    }
    
    @objc public static func errorCodeToMessage(code: FPErrorInputType) -> String {
        switch code {
        case .email:
            return NSLocalizedStringAAC(string: "Please enter a valid email address.")
        case .password:
            return NSLocalizedStringAAC(string: "Password must be at least 10 characters and contain at least 1 upper case, 1 lower case, 1 number and 1 special character !@#$%^&*. It cannot contain any part of your user name or be one of your last 12 passwords.")
        case .passwordContainName:
            return NSLocalizedStringAAC(string: "Password must be at least 10 characters and contain at least 1 upper case, 1 lower case, 1 number and 1 special character !@#$%^&*. It cannot contain any part of your user name or be one of your last 12 passwords.")
        case .confirmPassword:
            return NSLocalizedStringAAC(string: "The password entries do not match.")
        case .answer:
            return NSLocalizedStringAAC(string: "A valid security answer must be at least 4 characters long.")
        default:
            return ""
        }
    }
    
    static func isStrongPassword(password: String) -> Bool{
        if password.count < 10 {
            return false
        }
        
        let stricterFilterString = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*]).{10,25}"
        let emailTest:NSPredicate = NSPredicate(format: "SELF MATCHES %@", stricterFilterString)
        
        return emailTest.evaluate(with:password)
    }
    
    static func isContainAnyPartOfNames(password: String, names: [String]) -> Bool {
        let lowercasesPass = password.lowercased()
        for name in names {
            if lowercasesPass.isContainPartOfString(string: name.lowercased()) {
                return true
            }
        }
        return false
    }
}
