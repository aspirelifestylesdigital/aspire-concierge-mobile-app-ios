//
//  NameValidation.swift
//  AspireApiControllers
//
//  Created by VuongTC on 11/1/18.
//

import UIKit

@objc public class NameValidation: NSObject {
    @objc public static func validateSpecialChars(name: String) -> Bool {
        //User can enter characters and Number with Dash, Apostrophe and Space.
        let regex = "^[a-zA-Z0-9 ’`'\\-]+$"
        let test = NSPredicate(format:"SELF MATCHES %@", regex)
        return test.evaluate(with: name)
    }
    
    @objc public static func validateLength(name: String) -> Bool {
        if name.count < 2 || name.count > 25 {
            return false
        }
        return true
     }
    
    @objc public static func validateFirstName(name: String) -> String? {
        let isValidLength = validateLength(name: name)
        if isValidLength == false {
            return NSLocalizedStringAAC(string: "A valid first name must be at least 2 characters long.")
        }
        let isValidSpecialChars = validateSpecialChars(name: name)
        if isValidSpecialChars == false {
            return NSLocalizedStringAAC(string: "Please enter a valid first name. Acceptable special characters are - , \' and space.")
        }
        return nil
    }
    
    @objc public static func validateLastName(name: String) -> String? {
        let isValidLength = validateLength(name: name)
        if isValidLength == false {
            return NSLocalizedStringAAC(string: "A valid last name must be at least 2 characters long.")
        }
        let isValidSpecialChars = validateSpecialChars(name: name)
        if isValidSpecialChars == false {
            return NSLocalizedStringAAC(string: "Please enter a valid last name. Acceptable special characters are - , \' and space.")
        }
        return nil
    }
}
