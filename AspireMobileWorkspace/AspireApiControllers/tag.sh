set -ex
TAG=$1
MESSAGE=$2

version="'$TAG'"

find . -name 'AspireApiControllers.podspec' -print0 | xargs -0 sed -i '' '11 c\
 s.version          = '$version'
'

git add -A
git commit -m "$MESSAGE"
git push origin head
git tag $TAG
git push --tags