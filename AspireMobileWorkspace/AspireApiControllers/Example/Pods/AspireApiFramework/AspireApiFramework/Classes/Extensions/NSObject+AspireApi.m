//
//  NSObject+AspireApi.m
//  AspireApiFramework
//
//  Created by dai on 8/24/18.
//  Copyright © 2018 com.aspirelifestyles.ios.mobileconcierge.UnitTest. All rights reserved.
//

#import "NSObject+AspireApi.h"

@implementation NSObject (AspireAPI)
- (void)checkAndSetValue:(id)value forKey:(NSString *)key {
    if ([self respondsToSelector:NSSelectorFromString(key)] && value != nil) {
        [self setValue:value forKey:key];
    } else {
#if DEBUG
        NSLog(@"%s => cant found key:%@ from class:%@",__PRETTY_FUNCTION__,key,NSStringFromClass([self class]));
#endif
    }
}
@end
