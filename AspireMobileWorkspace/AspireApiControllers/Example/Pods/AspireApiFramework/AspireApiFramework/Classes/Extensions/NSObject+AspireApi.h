//
//  NSObject+AspireApi.h
//  AspireApiFramework
//
//  Created by dai on 8/24/18.
//  Copyright © 2018 com.aspirelifestyles.ios.mobileconcierge.UnitTest. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (AspireAPI)
- (void) checkAndSetValue:(id)value forKey:(NSString*) key;
@end
