//
//  AspireLogging.h
//  AspireApiFramework
//
//  Created by VuongTC on 10/31/18.
//

#import <Foundation/Foundation.h>
#import "ModelAspireApiManager.h"

void NSLogAAM(NSString *format, ...) {
    if ([ModelAspireApiManager isLogging] == false) {
        return;
    }
    va_list args;
    va_start(args, format);
    NSLogv(format, args);
    va_end(args);
}
