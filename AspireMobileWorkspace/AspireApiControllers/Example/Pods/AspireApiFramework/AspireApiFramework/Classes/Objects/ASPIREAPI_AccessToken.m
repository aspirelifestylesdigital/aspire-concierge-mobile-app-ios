// UserAccessToken.h

// To parse this JSON:
//
//   NSError *error;
//   UserAccessToken *accessToken = [UserAccessToken fromJSON:json encoding:NSUTF8Encoding error:&error];

#import <Foundation/Foundation.h>
#import "AspireAPIValidate.h"
#import "ASPIREAPI_AccessToken.h"

static ASPIREAPI_AccessToken *user = nil;
static ASPIREAPI_AccessToken *service = nil;

#define FILE_ACCESSTOKEN_USER @"resunekotsseccaelif"

// Shorthand for simple blocks
#define λ(decl, expr) (^(decl) { return (expr); })

// nil → NSNull conversion for JSON dictionaries
static id NSNullify(id _Nullable x) {
    return (x == nil || x == NSNull.null) ? NSNull.null : x;
}

NS_ASSUME_NONNULL_BEGIN

#pragma mark - Private model interfaces

@interface ASPIREAPI_AccessToken (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

#pragma mark - JSON serialization
NSData *_Nullable UserAccessTokenToData(ASPIREAPI_AccessToken *accessToken, NSError **error)
{
    @try {
        id json = [accessToken JSONDictionary];
        NSData *data = [NSJSONSerialization dataWithJSONObject:json options:kNilOptions error:error];
        return *error ? nil : data;
    } @catch (NSException *exception) {
        *error = [NSError errorWithDomain:@"JSONSerialization" code:-1 userInfo:@{ @"exception": exception }];
        return nil;
    }
}

NSString *_Nullable UserAccessTokenToJSON(ASPIREAPI_AccessToken *accessToken, NSStringEncoding encoding, NSError **error)
{
    NSData *data = UserAccessTokenToData(accessToken, error);
    return data ? [[NSString alloc] initWithData:data encoding:encoding] : nil;
}

@implementation ASPIREAPI_AccessToken
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"access_token": @"accessToken",
                                                    @"token_type": @"tokenType",
                                                    @"expires_in": @"expiresIn",
                                                    @"scope": @"scope",
                                                    @"id_token": @"theIDToken",
                                                    };
}

+ (void)saveJSONUserDictionary:(NSDictionary *)dict {
    NSMutableDictionary* temp =  [NSMutableDictionary dictionaryWithDictionary:dict];
    if (![[dict allKeys] containsObject:@"date_login"]) { // if dict store this key => user dont load from login view or create
        [temp setObject:[NSDate date] forKey:@"date_login"];
    }
    [AspireAPIValidate storedData:temp toFileName:FILE_ACCESSTOKEN_USER];
//    [[NSUserDefaults standardUserDefaults] setObject:temp forKey:@"APP::ASPIREAPIUSERTOKENSTORED"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{    
    [self setValuesForKeysWithDictionary:dict];
    NSMutableDictionary* temp =  [NSMutableDictionary dictionaryWithDictionary:dict];
    if (![[dict allKeys] containsObject:@"date_login"]) { // if dict store this key => user dont load from login view or create
        [temp setObject:[NSDate date] forKey:@"date_login"];
    }
    [[NSUserDefaults standardUserDefaults] setObject:temp forKey: ([self isEqual:user]) ? @"APP::ASPIREAPIUSERTOKENSTORED" : @"APP::ASPIREAPISERIVCETOKENSTORED"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    return self;
}

- (void)setValue:(nullable id)value forKey:(NSString *)key
{
    id resolved = ASPIREAPI_AccessToken.properties[key];
    if (resolved) [super setValue:value forKey:resolved];
}

- (NSDictionary *)JSONDictionary
{
    id dict = [[self dictionaryWithValuesForKeys:ASPIREAPI_AccessToken.properties.allValues] mutableCopy];
    
    // Rewrite property names that differ in JSON
    for (id jsonName in ASPIREAPI_AccessToken.properties) {
        id propertyName = ASPIREAPI_AccessToken.properties[jsonName];
        if (![jsonName isEqualToString:propertyName]) {
            dict[jsonName] = dict[propertyName];
            [dict removeObjectForKey:propertyName];
        }
    }
    
    return dict;
}

- (NSData *_Nullable)toData:(NSError *_Nullable *)error
{
    return UserAccessTokenToData(self, error);
}

- (NSString *_Nullable)toJSON:(NSStringEncoding)encoding error:(NSError *_Nullable *)error
{
    return UserAccessTokenToJSON(self, encoding, error);
}

+ (void)resetUser {
    [AspireAPIValidate storedData:@"" toFileName:FILE_ACCESSTOKEN_USER];
}

+ (BOOL)isValid {
    
    NSLog(@"%s",__PRETTY_FUNCTION__);
    
    id dictTemp = [AspireAPIValidate getDataFromFileName:FILE_ACCESSTOKEN_USER];
    if (![dictTemp isKindOfClass:[NSDictionary class]]) {return false;}
    NSDictionary* dict = (NSDictionary*)dictTemp;
    if(![[dict allKeys] containsObject:@"date_login"]) {return false;};
    NSDate* date = [dict valueForKey:@"date_login"];
    if (date) {
        return [[NSDate date] timeIntervalSinceDate:date] < [ASPIREAPI_AccessToken user].expiresIn;
    } else {
        return false;
    }
}

#pragma mark - INIT
+ (ASPIREAPI_AccessToken *)user {
    user = [[ASPIREAPI_AccessToken alloc] initUser];
    return user;
}

+ (ASPIREAPI_AccessToken *)service {
    static dispatch_once_t onceToken;
    @synchronized([ASPIREAPI_AccessToken class]) {
        if (!service)
            dispatch_once(&onceToken, ^{
                service = [[ASPIREAPI_AccessToken alloc] initService];
            });
        return service;
    }
    return nil;
}

+ (void)removeToken {
#if DEBUG
    NSLog(@"%s",__PRETTY_FUNCTION__);
#endif
    [ASPIREAPI_AccessToken resetUser];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"APP::ASPIREAPISERIVCETOKENSTORED"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (id)initUser
{
    id dictTemp = [AspireAPIValidate getDataFromFileName:FILE_ACCESSTOKEN_USER];
    if (![dictTemp isKindOfClass:[NSDictionary class]]) {return nil;}
    NSDictionary* dict = (NSDictionary*)dictTemp;
        user = [super init];
        return [user initWithJSONDictionary:dict];
    return nil;
}

- (id)initService
{
    if (!service) {
        service = [super init];
    }
    if ([[NSUserDefaults standardUserDefaults] dictionaryForKey:@"APP::ASPIREAPISERIVCETOKENSTORED"]) {
        service = [service initWithJSONDictionary:[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"APP::ASPIREAPISERIVCETOKENSTORED"]];
    }
    return service;
}

@end

NS_ASSUME_NONNULL_END
