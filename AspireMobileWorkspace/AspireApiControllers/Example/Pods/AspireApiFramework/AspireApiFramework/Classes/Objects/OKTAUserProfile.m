//
//  OKTAUserProfile.m
//  AspireApiFramework
//
//  Created by Dai on 7/24/18.
//  Copyright © 2018 com.aspirelifestyles.ios.mobileconcierge.UnitTest. All rights reserved.
//

#import "NSObject+AspireApi.h"
#import "OKTAUserProfile.h"

// Shorthand for simple blocks
#define λ(decl, expr) (^(decl) { return (expr); })

// nil → NSNull conversion for JSON dictionaries
static id NSNullify(id _Nullable x) {
    return (x == nil || x == NSNull.null) ? NSNull.null : x;
}

NS_ASSUME_NONNULL_BEGIN

#pragma mark - Private model interfaces

@interface OKTAUserProfile (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface OKTAUserCredentials (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface OKTAUserPassword (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface OKTAUserProvider (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface OKTARecoveryQuestion (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface OKTAUserLinks (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface OKTAUserDeactivate (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface OKTAUserSelf (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface OKTAUserProfileClass (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

#pragma mark - JSON serialization

OKTAUserProfile *_Nullable OKTAUserProfileFromData(NSData *data, NSError **error)
{
    @try {
        id json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:error];
        return [OKTAUserProfile fromJSONDictionary:json];
    } @catch (NSException *exception) {
        *error = [NSError errorWithDomain:@"JSONSerialization" code:-1 userInfo:@{ @"exception": exception }];
        return nil;
    }
}

OKTAUserProfile *_Nullable OKTAUserProfileFromJSON(NSString *json, NSStringEncoding encoding, NSError **error)
{
    return OKTAUserProfileFromData([json dataUsingEncoding:encoding], error);
}

NSData *_Nullable OKTAUserProfileToData(OKTAUserProfile *profile, NSError **error)
{
    @try {
        id json = [profile JSONDictionary];
        NSData *data = [NSJSONSerialization dataWithJSONObject:json options:kNilOptions error:error];
        return *error ? nil : data;
    } @catch (NSException *exception) {
        *error = [NSError errorWithDomain:@"JSONSerialization" code:-1 userInfo:@{ @"exception": exception }];
        return nil;
    }
}

NSString *_Nullable OKTAUserProfileToJSON(OKTAUserProfile *profile, NSStringEncoding encoding, NSError **error)
{
    NSData *data = OKTAUserProfileToData(profile, error);
    return data ? [[NSString alloc] initWithData:data encoding:encoding] : nil;
}

@implementation OKTAUserProfile
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"id": @"id",
                                                    @"status": @"status",
                                                    @"created": @"created",
                                                    @"activated": @"activated",
                                                    @"statusChanged": @"statusChanged",
                                                    @"lastLogin": @"lastLogin",
                                                    @"lastUpdated": @"lastUpdated",
                                                    @"passwordChanged": @"passwordChanged",
                                                    @"profile": @"profile",
                                                    @"credentials": @"credentials",
                                                    @"_links": @"_links",
                                                    };
}

+ (_Nullable instancetype)fromData:(NSData *)data error:(NSError *_Nullable *)error
{
    return OKTAUserProfileFromData(data, error);
}

+ (_Nullable instancetype)fromJSON:(NSString *)json encoding:(NSStringEncoding)encoding error:(NSError *_Nullable *)error
{
    return OKTAUserProfileFromJSON(json, encoding, error);
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[OKTAUserProfile alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            __block NSString* key = obj;
            [[OKTAUserProfile properties] enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull k, NSString * _Nonnull val, BOOL * _Nonnull stop) {
                if ([k isEqualToString:key])
                    key = val;
            }];
            if ([self respondsToSelector:NSSelectorFromString(key)]) {
                [self setValue:[dict valueForKey:obj] forKey:key];
            }
        }];
        _profile = [OKTAUserProfileClass fromJSONDictionary:(id)_profile];
        _credentials = [OKTAUserCredentials fromJSONDictionary:(id)_credentials];
        __links = [OKTAUserLinks fromJSONDictionary:(id)__links];
    }
    return self;
}

- (void)setValue:(nullable id)value forKey:(NSString *)key
{
    id resolved = OKTAUserProfile.properties[key];
    if (resolved) {
        [super setValue:value forKey:resolved];
    }
}

- (NSDictionary *)JSONDictionary
{
    id dict = [[self dictionaryWithValuesForKeys:OKTAUserProfile.properties.allValues] mutableCopy];
    
    // Rewrite property names that differ in JSON
    for (id jsonName in OKTAUserProfile.properties) {
        id propertyName = OKTAUserProfile.properties[jsonName];
        if (![jsonName isEqualToString:propertyName]) {
            dict[jsonName] = dict[propertyName];
            [dict removeObjectForKey:propertyName];
        }
    }
    
    // Map values that need translation
    [dict addEntriesFromDictionary:@{
                                     @"profile": [_profile JSONDictionary],
                                     @"credentials": [_credentials JSONDictionary],
                                     @"_links": [__links JSONDictionary],
                                     }];
    
    return dict;
}

- (NSData *_Nullable)toData:(NSError *_Nullable *)error
{
    return OKTAUserProfileToData(self, error);
}

- (NSString *_Nullable)toJSON:(NSStringEncoding)encoding error:(NSError *_Nullable *)error
{
    return OKTAUserProfileToJSON(self, encoding, error);
}
@end

@implementation OKTAUserCredentials
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"password": @"password",
                                                    @"recovery_question": @"recoveryQuestion",
                                                    @"provider": @"provider",
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[OKTAUserCredentials alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            __block  NSString* key = obj;
            [[OKTAUserCredentials properties] enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull k, NSString * _Nonnull val, BOOL * _Nonnull stop) {
                if ([k isEqualToString:key])
                    key = val;
            }];
            if ([self respondsToSelector:NSSelectorFromString(key)]) {
                [self setValue:[dict valueForKey:obj] forKey:key];
            }
        }];
        _password = [OKTAUserPassword fromJSONDictionary:(id)_password];
        _provider = [OKTAUserProvider fromJSONDictionary:(id)_provider];
        _recoveryQuestion = [OKTARecoveryQuestion fromJSONDictionary:(id)_recoveryQuestion];
    }
    return self;
}

- (NSDictionary *)JSONDictionary
{
    id dict = [[self dictionaryWithValuesForKeys:OKTAUserCredentials.properties.allValues] mutableCopy];
    
    // Map values that need translation
    [dict addEntriesFromDictionary:@{
                                     @"password": [_password JSONDictionary],
                                     @"provider": [_provider JSONDictionary],
                                     }];
    
    return dict;
}
@end

@implementation OKTARecoveryQuestion
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"question": @"question",
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[OKTARecoveryQuestion alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

- (NSDictionary *)JSONDictionary
{
    return [self dictionaryWithValuesForKeys:OKTARecoveryQuestion.properties.allValues];
}
@end

@implementation OKTAUserPassword
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[OKTAUserPassword alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            __block  NSString* key = obj;
            [[OKTAUserPassword properties] enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull k, NSString * _Nonnull val, BOOL * _Nonnull stop) {
                if ([k isEqualToString:key])
                    key = val;
            }];
            if ([self respondsToSelector:NSSelectorFromString(key)]) {
                [self setValue:[dict valueForKey:obj] forKey:key];
            }
        }];
    }
    return self;
}

- (NSDictionary *)JSONDictionary
{
    return [self dictionaryWithValuesForKeys:OKTAUserPassword.properties.allValues];
}
@end

@implementation OKTAUserProvider
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"type": @"type",
                                                    @"name": @"name",
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[OKTAUserProvider alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            __block NSString* key = obj;
            [[OKTAUserProvider properties] enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull k, NSString * _Nonnull val, BOOL * _Nonnull stop) {
                if ([k isEqualToString:key])
                    key = val;
            }];
            if ([self respondsToSelector:NSSelectorFromString(key)]) {
                [self setValue:[dict valueForKey:obj] forKey:key];
            }
        }];
    }
    return self;
}

- (NSDictionary *)JSONDictionary
{
    return [self dictionaryWithValuesForKeys:OKTAUserProvider.properties.allValues];
}
@end

@implementation OKTAUserLinks
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"suspend": @"suspend",
                                                    @"changePassword": @"changePassword",
                                                    @"forgotPassword": @"forgotPassword",
                                                    @"changeRecoveryQuestion": @"changeRecoveryQuestion",
                                                    @"resetPassword": @"resetPassword",
                                                    @"expirePassword": @"expirePassword",
                                                    @"self": @"self",
                                                    @"deactivate": @"deactivate",
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[OKTAUserLinks alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            __block NSString* key = obj;
            [[OKTAUserLinks properties] enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull k, NSString * _Nonnull val, BOOL * _Nonnull stop) {
                if ([key isEqualToString:k])
                    key = val;
            }];
            if ([self respondsToSelector:NSSelectorFromString(key)]) {
                [self setValue:[dict valueForKey:obj] forKey:key];
            }
        }];
        _suspend = [OKTAUserDeactivate fromJSONDictionary:(id)_suspend];
        _resetPassword = [OKTAUserDeactivate fromJSONDictionary:(id)_resetPassword];
        _expirePassword = [OKTAUserDeactivate fromJSONDictionary:(id)_expirePassword];
        _self = [OKTAUserSelf fromJSONDictionary:(id)_self];
        _deactivate = [OKTAUserDeactivate fromJSONDictionary:(id)_deactivate];
        _forgotPassword = [OKTAUserDeactivate fromJSONDictionary:(id)_forgotPassword];
        _changeRecoveryQuestion = [OKTAUserDeactivate fromJSONDictionary:(id)_changeRecoveryQuestion];
    }
    return self;
}

- (NSDictionary *)JSONDictionary
{
    id dict = [[self dictionaryWithValuesForKeys:OKTAUserLinks.properties.allValues] mutableCopy];
    
    // Map values that need translation
    [dict addEntriesFromDictionary:@{
                                     @"suspend": [_suspend JSONDictionary],
                                     @"resetPassword": [_resetPassword JSONDictionary],
                                     @"expirePassword": [_expirePassword JSONDictionary],
                                     @"forgotPassword": [_forgotPassword JSONDictionary],
                                     @"changeRecoveryQuestion": [_changeRecoveryQuestion JSONDictionary],
                                     @"self": [_self JSONDictionary],
                                     @"deactivate": [_deactivate JSONDictionary],
                                     }];
    
    return dict;
}
@end

@implementation OKTAUserDeactivate
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"href": @"href",
                                                    @"method": @"method",
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[OKTAUserDeactivate alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([self respondsToSelector:NSSelectorFromString(obj)]) {
                [self setValue:[dict valueForKey:obj] forKey:obj];
            }
        }];
    }
    return self;
}

- (NSDictionary *)JSONDictionary
{
    return [self dictionaryWithValuesForKeys:OKTAUserDeactivate.properties.allValues];
}
@end

@implementation OKTAUserSelf
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"href": @"href",
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[OKTAUserSelf alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([self respondsToSelector:NSSelectorFromString(obj)]) {
                [self setValue:[dict valueForKey:obj] forKey:obj];
            }
        }];
    }
    return self;
}

- (NSDictionary *)JSONDictionary
{
    return [self dictionaryWithValuesForKeys:OKTAUserSelf.properties.allValues];
}
@end

@implementation OKTAUserProfileClass
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"firstName": @"firstName",
                                                    @"lastName": @"lastName",
                                                    @"mobilePhone": @"mobilePhone",
                                                    @"organization": @"organization",
                                                    @"secondEmail": @"secondEmail",
                                                    @"userType": @"userType",
                                                    @"partyId": @"partyId",
                                                    @"login": @"login",
                                                    @"email": @"email",
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[OKTAUserProfileClass alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            __block NSString* key = obj;
            [[OKTAUserProfileClass properties] enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull k, NSString * _Nonnull val, BOOL * _Nonnull stop) {
                if ([key isEqualToString:k])
                    key = val;
            }];
            if ([self respondsToSelector:NSSelectorFromString(key)]) {
                [self setValue:[dict valueForKey:obj] forKey:key];
            }
        }];
    }
    return self;
}

- (void)setValue:(nullable id)value forKey:(NSString *)key
{
    id resolved = OKTAUserProfileClass.properties[key];
    if (resolved) [super setValue:value forKey:resolved];
}

- (NSDictionary *)JSONDictionary
{
    id dict = [[self dictionaryWithValuesForKeys:OKTAUserProfileClass.properties.allValues] mutableCopy];
    
    // Rewrite property names that differ in JSON
    for (id jsonName in OKTAUserProfileClass.properties) {
        id propertyName = OKTAUserProfileClass.properties[jsonName];
        if (![jsonName isEqualToString:propertyName]) {
            dict[jsonName] = dict[propertyName];
            [dict removeObjectForKey:propertyName];
        }
    }
    
    return dict;
}
@end

NS_ASSUME_NONNULL_END
