// User.h

// To parse this JSON:
//
//   NSError *error;
//   User * = [User fromJSON:json encoding:NSUTF8Encoding error:&error];
// User.m


#import "GroupObject.h"
#import "AspireAPIValidate.h"
#import "ModelAspireApiManager.h"
#import "NSObject+AspireApi.h"

#define USERDEFAULT_AUTEHNTICATION_ACCOUNT @"APP::ASPIREAPIACCOUNTAUTHENTICATIONSTORED"
#define USERDEFAULT_USER @"APP::ASPIREAPIUSERSTORED"

#define FILE_AUTEHNTICATION_ACCOUNT @"ashdhjadjhjsdadasd"
#define FILE_USER @"dajidhadhiahdadqbd"

// Shorthand for simple blocks
#define λ(decl, expr) (^(decl) { return (expr); })

// nil → NSNull conversion for JSON dictionaries
static id NSNullify(id _Nullable x) {
    return (x == nil || x == NSNull.null) ? NSNull.null : x;
}

NS_ASSUME_NONNULL_BEGIN

#pragma mark - Private model interfaces

@interface User (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface UserAppUserPreference (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface UserEmail (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface UserFax (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface UserLocation (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface UserAddress (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface UserMembership (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface UserPhone (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface UserPreference (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface UserRelationship (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface UserSocialMedia (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface UserWebsite (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

static id map(id collection, id (^f)(id value)) {
    id result = nil;
    if ([collection isKindOfClass:NSArray.class]) {
        result = [NSMutableArray arrayWithCapacity:[collection count]];
        for (id x in collection) [result addObject:f(x)];
    } else if ([collection isKindOfClass:NSDictionary.class]) {
        result = [NSMutableDictionary dictionaryWithCapacity:[collection count]];
        for (id key in collection) [result setObject:f([collection objectForKey:key]) forKey:key];
    }
    return result;
}

#pragma mark - JSON serialization

User *_Nullable UserFromData(NSData *data, NSError **error)
{
    @try {
        id json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:error];
        return [User fromJSONDictionary:json];
    } @catch (NSException *exception) {
        *error = [NSError errorWithDomain:@"JSONSerialization" code:-1 userInfo:@{ @"exception": exception }];
        return nil;
    }
}

User *_Nullable UserFromJSON(NSString *json, NSStringEncoding encoding, NSError **error)
{
    return UserFromData([json dataUsingEncoding:encoding], error);
}

NSData *_Nullable UserToData(User * user, NSError **error)
{
    @try {
        id json = [user JSONDictionary];
        NSData *data = [NSJSONSerialization dataWithJSONObject:json options:kNilOptions error:error];
        return *error ? nil : data;
    } @catch (NSException *exception) {
        *error = [NSError errorWithDomain:@"JSONSerialization" code:-1 userInfo:@{ @"exception": exception }];
        return nil;
    }
}

NSString *_Nullable UserToJSON(User * user, NSStringEncoding encoding, NSError **error)
{
    NSData *data = UserToData(user, error);
    return data ? [[NSString alloc] initWithData:data encoding:encoding] : nil;
}

static User *shared = nil;
static AccountAuthentication *currentAAuthen = nil;
@interface AccountAuthentication() {
    NSString* password,*username;
}
@end

@implementation AccountAuthentication

+ (void)reset {
    [AspireAPIValidate storedData:@"" toFileName:FILE_AUTEHNTICATION_ACCOUNT];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USERDEFAULT_AUTEHNTICATION_ACCOUNT];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)storedWithDictionary:(NSDictionary *)dict {
    __block NSString* username = @"";
    __block NSString* password = @"";
    NSMutableDictionary* dicttemp = [NSMutableDictionary new];
    [[dict allKeys] enumerateObjectsUsingBlock:^(NSString*  _Nonnull key, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([key isEqualToString:@"username"]) {
            username = [dict objectForKey:key];
            [dicttemp setObject:username forKey:key];
        } else if ([key isEqualToString:@"password"]) {
            password = [dict objectForKey:key];
            NSString *authStr = [NSString stringWithFormat:@"%@[==√==]%@", username, password];
            NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
            NSString* endcode = [authData base64EncodedStringWithOptions:0];
            [dicttemp setObject:endcode forKey:key];
        }
    }];
    if (dicttemp.count > 0) {
//        NSData *dictData = [NSKeyedArchiver archivedDataWithRootObject:dicttemp];
//        [[NSUserDefaults standardUserDefaults] setObject:dictData forKey:USERDEFAULT_AUTEHNTICATION_ACCOUNT];
//        [[NSUserDefaults standardUserDefaults] synchronize];
        [AspireAPIValidate storedData:dicttemp toFileName:FILE_AUTEHNTICATION_ACCOUNT];
    }
}

+ (nullable NSString *)username {
    id dict = [AspireAPIValidate getDataFromFileName:FILE_AUTEHNTICATION_ACCOUNT];
    if (![dict isKindOfClass:[NSDictionary class]]) {return nil;}
    if (((NSDictionary*)dict).count > 0) {
        if ([[dict allKeys] containsObject:@"username"]) {
            return [dict valueForKey:@"username"];
        }
    }
//    if ([[[NSUserDefaults standardUserDefaults] objectForKey:USERDEFAULT_AUTEHNTICATION_ACCOUNT]]) {
//        dict = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:USERDEFAULT_AUTEHNTICATION_ACCOUNT]];
//        if ([[dict allKeys] containsObject:@"username"]) {
//            return [dict valueForKey:@"username"];
//        }
//    }
    return nil;
}

+ (nullable NSString *)password {
    
    id dict = [AspireAPIValidate getDataFromFileName:FILE_AUTEHNTICATION_ACCOUNT];
    if (![dict isKindOfClass:[NSDictionary class]]) {return nil;}
    if (((NSDictionary*)dict).count > 0) {
        if ([[dict allKeys] containsObject:@"password"]) {
            NSData *authData = [[NSData alloc] initWithBase64EncodedString:[dict valueForKey:@"password"] options:0];
            NSString* decode = [[[NSString alloc] initWithData:authData encoding:NSUTF8StringEncoding] componentsSeparatedByString:@"[==√==]"].lastObject;
            return decode;
        }
    }
    
//    NSDictionary*dict = nil;
//    if ([[NSUserDefaults standardUserDefaults] objectForKey:USERDEFAULT_AUTEHNTICATION_ACCOUNT]) {
//        dict = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:USERDEFAULT_AUTEHNTICATION_ACCOUNT]];
//        if ([[dict allKeys] containsObject:@"password"]) {
//            NSData *authData = [[NSData alloc] initWithBase64EncodedString:[dict valueForKey:@"password"] options:0];
//            NSString* decode = [[[NSString alloc] initWithData:authData encoding:NSUTF8StringEncoding] componentsSeparatedByString:@"[==√==]"].lastObject;
//            return decode;
//        }
//    }
    return nil;
}

@end

@implementation User
+ (NSDictionary *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"partyId": @"partyId",
                                                    @"salutation": @"salutation",
                                                    @"fullName": @"fullName",
                                                    @"firstName": @"firstName",
                                                    @"middleName": @"middleName",
                                                    @"lastName": @"lastName",
                                                    @"displayName": @"displayName",
                                                    @"aliasName": @"aliasName",
                                                    @"prefix": @"prefix",
                                                    @"suffix": @"suffix",
                                                    @"gender": @"gender",
                                                    @"maritalStatus": @"maritalStatus",
                                                    @"recordEffectiveFrom": @"recordEffectiveFrom",
                                                    @"recordEffectiveTo": @"recordEffectiveTo",
                                                    @"dateOfBirth": @"dateOfBirth",
                                                    @"residentCountry": @"residentCountry",
                                                    @"homeCountry": @"homeCountry",
                                                    @"nationality": @"nationality",
                                                    @"signature": @"signature",
                                                    @"profilePic": @"profilePic",
                                                    @"disabilityStatus": @"disabilityStatus",
                                                    @"placeOfBirth": @"placeOfBirth",
                                                    @"vip": @"vip",
                                                    @"employmentStatus": @"employmentStatus",
                                                    @"locations": @"locations",
                                                    @"emails": @"emails",
                                                    @"preferences": @"preferences",
                                                    @"memberships": @"memberships",
                                                    @"phones": @"phones",
                                                    @"faxes": @"faxes",
                                                    @"websites": @"websites",
                                                    @"socialMedias": @"socialMedias",
                                                    @"relationships": @"relationships",
                                                    @"appUserPreferences": @"appUserPreferences"
                                                    };
}

+ (NSDictionary * _Nullable)convertDictionary {
    if([User current].fullName.length == 0)
        return nil;
    return [shared convertDictionary];
}

- (NSDictionary*) convertDictionary {
    return   @{
                                                    @"partyId":_partyId,
                                                    @"salutation": _salutation,
                                                    @"fullName": _fullName,
                                                    @"firstName": _firstName,
                                                    @"middleName": _middleName,
                                                    @"lastName": _lastName,
                                                    @"displayName": _displayName,
                                                    @"aliasName": _aliasName,
                                                    @"prefix": _prefix,
                                                    @"suffix": _suffix,
                                                    @"gender": _gender,
                                                    @"maritalStatus": _maritalStatus,
                                                    @"recordEffectiveFrom": _recordEffectiveFrom,
                                                    @"recordEffectiveTo": _recordEffectiveTo,
                                                    @"dateOfBirth": _dateOfBirth,
                                                    @"residentCountry": _residentCountry,
                                                    @"homeCountry": _homeCountry,
                                                    @"nationality": _nationality,
                                                    @"signature": _signature,
                                                    @"profilePic": _profilePic,
                                                    @"disabilityStatus": _disabilityStatus,
                                                    @"placeOfBirth": _placeOfBirth,
                                                    @"vip": _vip,
                                                    @"employmentStatus": _employmentStatus,
                                                    @"locations": @"locations",
                                                    @"emails": @"emails",
                                                    @"preferences": @"preferences",
                                                    @"memberships": @"memberships",
                                                    @"phones": @"phones",
                                                    @"faxes": @"faxes",
                                                    @"websites": @"websites",
                                                    @"socialMedias": @"socialMedias",
                                                    @"relationships": @"relationships",
                                                    };
}

+ (_Nullable instancetype)fromData:(NSData *)data error:(NSError *_Nullable *)error
{
    return UserFromData(data, error);
}

+ (_Nullable instancetype)fromJSON:(NSString *)json encoding:(NSStringEncoding)encoding error:(NSError *_Nullable *)error
{
    return UserFromJSON(json, encoding, error);
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[User alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
//        [self setValuesForKeysWithDictionary:dict];
        if (dict.count > 0) {
            [AspireAPIValidate storedData:dict toFileName:FILE_USER];
        }
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([self respondsToSelector:NSSelectorFromString(obj)]) {
                [self setValue:[dict valueForKey:obj] forKey:obj];
            }
        }];
        if(_appUserPreferences.count>0)_appUserPreferences = map(_appUserPreferences, λ(id x, [UserAppUserPreference fromJSONDictionary:x]));
        if(_locations.count>0)_locations = map(_locations, λ(id x, [UserLocation fromJSONDictionary:x]));
        if(_emails.count>0)_emails = map(_emails, λ(id x, [UserEmail fromJSONDictionary:x]));
        if(_preferences.count >0)_preferences = map(_preferences, λ(id x, [UserPreference fromJSONDictionary:x]));
        if(_memberships.count > 0)_memberships = map(_memberships, λ(id x, [UserMembership fromJSONDictionary:x]));
        if(_phones.count >0)_phones = map(_phones, λ(id x, [UserPhone fromJSONDictionary:x]));
        if(_faxes.count > 0)_faxes = map(_faxes, λ(id x, [UserFax fromJSONDictionary:x]));
        if (_websites.count > 0)_websites = map(_websites, λ(id x, [UserWebsite fromJSONDictionary:x]));
        if (_socialMedias.count > 0)_socialMedias = map(_socialMedias, λ(id x, [UserSocialMedia fromJSONDictionary:x]));
        if (_relationships.count > 0) _relationships = map(_relationships, λ(id x, [UserRelationship fromJSONDictionary:x]));
    }
    return self;
}



- (NSDictionary *)JSONDictionary
{
    id dict = [[self dictionaryWithValuesForKeys:User.properties.allValues] mutableCopy];
    
    // Map values that need translation
    [dict addEntriesFromDictionary:@{
                                     @"locations": map(_locations, λ(id x, [x JSONDictionary])),
                                     @"emails": map(_emails, λ(id x, [x JSONDictionary])),
                                     @"preferences": map(_preferences, λ(id x, [x JSONDictionary])),
                                     @"memberships": map(_memberships, λ(id x, [x JSONDictionary])),
                                     @"phones": map(_phones, λ(id x, [x JSONDictionary])),
                                     @"faxes": map(_faxes, λ(id x, [x JSONDictionary])),
                                     @"websites": map(_websites, λ(id x, [x JSONDictionary])),
                                     @"socialMedias": map(_socialMedias, λ(id x, [x JSONDictionary])),
                                     @"relationships": map(_relationships, λ(id x, [x JSONDictionary])),
                                     @"appUserPreferences": map(_appUserPreferences, λ(id x, [x JSONDictionary]))
                                     }];
    
    return dict;
}

- (NSData *_Nullable)toData:(NSError *_Nullable *)error
{
    return UserToData(self, error);
}

- (NSString *_Nullable)toJSON:(NSStringEncoding)encoding error:(NSError *_Nullable *)error
{
    return UserToJSON(self, encoding, error);
}

- (id)convertToUserObjectWithClassName:(NSString *)className andPreferencesClassName:(nonnull NSString *)subClassName {
    id temp = [NSClassFromString(className) new];
    
    [temp checkAndSetValue:_firstName forKey:@"firstName"];
    [temp checkAndSetValue:_lastName forKey:@"lastName"];

    [temp checkAndSetValue:@"" forKey:@"ConsumerKey"];
    [temp checkAndSetValue:@"" forKey:@"Program"];
    [temp checkAndSetValue:@"" forKey:@"zipCode"];
    
    [temp checkAndSetValue:[_salutation convertSalutationFromAspireAPI] forKey:@"salutation"];
    [temp checkAndSetValue:_homeCountry forKey:@"homeCountry"];
    [temp checkAndSetValue:@"" forKey:@"optStatus"];
    if (_emails.count > 0) {
        UserEmail* userEmail = _emails.firstObject;
        [temp checkAndSetValue:userEmail.emailAddress forKey:@"email"];
    }
    if (_phones.count > 0) {
        UserPhone* userPhone = _phones.firstObject;
        [temp checkAndSetValue:userPhone.phoneNumber forKey:@"mobileNumber"];
    }
    if (_locations.count > 0) {
        UserLocation* userLocation = _locations.firstObject;
        if (userLocation.address.city.length > 0)
            [temp checkAndSetValue:userLocation.address.city forKey:@"currentCity"];
        if (userLocation.address.addressLine5.length > 0) {
            [temp checkAndSetValue:@([userLocation.address.addressLine5.lowercaseString containsString:[@"YES" lowercaseString]]) forKey:@"isUseLocation"];
        }
        if (userLocation.address.zipCode.length > 0) {
            [temp checkAndSetValue:userLocation.address.zipCode forKey:@"zipCode"];
        }
    } else {
        [temp checkAndSetValue:@(false) forKey:@"isUseLocation"];
    }
    if (_appUserPreferences.count > 0) {
        [_appUserPreferences enumerateObjectsUsingBlock:^(UserAppUserPreference * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            // get value is use location
            if ([obj.preferenceKey isEqualToString:APP_USER_PREFERENCE_Location]) {
                [temp checkAndSetValue:@([[obj.preferenceValue lowercaseString] isEqualToString:@"on"]) forKey:@"isUseLocation"];
            }
            
            // get selected city
            if (![temp respondsToSelector:NSSelectorFromString(@"selectedCity")]) {
                if ([obj.preferenceKey isEqualToString:APP_USER_PREFERENCE_Selected_City]) {
                    [temp checkAndSetValue:obj.preferenceValue forKey:@"currentCity"];
                }
            } else {
                if ([obj.preferenceKey isEqualToString:APP_USER_PREFERENCE_Selected_City]) {
                    [temp checkAndSetValue:obj.preferenceValue forKey:@"selectedCity"];
                }
            }
            
            // get newsletter
            if ([obj.preferenceKey isEqualToString:APP_USER_PREFERENCE_Newsletter]) {
                [temp checkAndSetValue:@([[obj.preferenceValue lowercaseString] isEqualToString:@"on"]) forKey:@"optStatus"];
            }

        }];
    }
    
    if (_preferences.count > 0) {
        NSMutableArray* preferences = [NSMutableArray new];
        [_preferences enumerateObjectsUsingBlock:^(UserPreference * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            id pre = [NSClassFromString(subClassName) new];
            
            NSUInteger type = -1;
            if ([obj.preferenceType containsString:@"Dining"]) {
                type = 0;
            }else if ([obj.preferenceType containsString:@"Car Type"]) {
                type = 2;
            } else if ([obj.preferenceType containsString:@"Hotel"]) {
                type = 1;
            }
            
            if (type != -1) {
                [pre checkAndSetValue:@(type) forKey:@"type"];
                [pre checkAndSetValue:obj.preferenceValue forKey:@"value"];
                [preferences addObject:pre];
            }
            
        }];
        [temp checkAndSetValue:preferences forKey:@"arrayPreferences"];        
    }
    
    return temp;
}

- (nullable NSString *)getPasscode {
    
    if (_appUserPreferences.count > 0) {
        for (UserAppUserPreference* obj in _appUserPreferences) {
            if ([obj.preferenceKey isEqualToString:APP_USER_PREFERENCE_Passcode]) {
                return obj.preferenceValue;
            }
        }
    }
    
    // keep check this for old version
    if (_preferences.count > 0) {
        for (UserPreference* obj in _preferences) {
            if ([obj.preferenceType isEqualToString:@"Other Preferences"]) {
                return obj.preferenceValue;
            }
        }
    }
    
    return nil;
}

- (nullable NSString *)getPolicyVersion {
    
    if (_appUserPreferences.count > 0) {
        for (UserAppUserPreference* obj in _appUserPreferences) {
            if ([obj.preferenceKey isEqualToString:APP_USER_PREFERENCE_Policy_Version]) {
                return obj.preferenceValue;
            }
        }
    }
    
    return @"0";
}

- (BOOL) isGetNewsletter {
    
    if (_appUserPreferences.count > 0) {
        for (UserAppUserPreference* obj in _appUserPreferences) {
            if ([obj.preferenceKey isEqualToString:APP_USER_PREFERENCE_Newsletter]) {
                return [[obj.preferenceValue lowercaseString] isEqualToString:@"on"];
            }
        }
    }
    
    return false;
}

- (NSString*) isGetNewsletterDate {
    
    if (_appUserPreferences.count > 0) {
        for (UserAppUserPreference* obj in _appUserPreferences) {
            if ([obj.preferenceKey isEqualToString:APP_USER_PREFERENCE_Newsletter_Date]) {
                return [obj.preferenceValue lowercaseString];
            }
        }
    }
    
    return @"";
}

- (nullable NSString*) getPreferencesIdFromType:(NSString *)type {
    
    for (UserPreference* pre in _preferences) {
        if ([pre.preferenceType isEqualToString:type]) {
            return pre.preferenceId;
        }
    }
    
    return @"";
}

- (nullable NSString*) getLocationIdFromType:(NSString *)type {
    
    for (UserLocation* pre in _locations) {
        if ([pre.locationType isEqualToString:type]) {
            return pre.locationId;
        }
    }
    
    return @"";
}

- (nullable NSString*) getPhoneIdFromType:(NSString *)type {
    
    for (UserPhone* pre in _phones) {
        if ([pre.phoneType isEqualToString:type]) {
            return pre.phoneId;
        }
    }
    
    return @"";
}

- (nullable NSString*) getEmailIdFromType:(NSString *)type {
    
    for (UserEmail* pre in _emails) {
        if ([pre.emailType isEqualToString:type]) {
            return pre.emailId;
        }
    }
    
    return @"";
}

- (nullable NSString*) getMembershipIdFromCategory:(NSString *)type {
    
    for (UserMembership* pre in _memberships) {
        if ([pre.membershipCategory isEqualToString:type]) {
            return pre.membershipId;
        }
    }
    
    return @"";
}

- (nullable NSString *)getAppUserPreferenceIdFromKey:(NSString *)key {
    for (UserAppUserPreference* pre in _appUserPreferences) {
        if ([pre.preferenceKey isEqualToString:key]) {
            return pre.appUserPreferenceId;
        }
    }
    
    return @"";
}

+ (User *)convertFromOKTAUser:(OKTAUserProfile *)obj {
    if (obj == nil) return nil;
    
    User* user = [[User alloc] initWithJSONDictionary:@{}];
    user.firstName = obj.profile.firstName;
    user.lastName = obj.profile.lastName;
    user.partyId = obj.profile.partyId;
    if (obj.profile.mobilePhone && ![obj.profile.mobilePhone isEqual:[NSNull null]]) {
        UserPhone* uphone = [UserPhone new];
        uphone.phoneNumber = obj.profile.mobilePhone;
        [user setPhones:@[uphone]];
    }
    if (obj.profile.login && ![obj.profile.login isEqual:[NSNull null]]) {
        UserEmail* uemail = [UserEmail new];
        uemail.emailAddress = obj.profile.login;
        [user setEmails:@[uemail]];
    }
    
    return user;
}

+ (nullable User *) current {
    id dict = [AspireAPIValidate getDataFromFileName:FILE_USER];
    if (![dict isKindOfClass:[NSDictionary class]]) {return nil;}
    if (((NSDictionary*)dict).count > 0) {
        shared = [[User alloc] initWithJSONDictionary:dict];
        return shared;
    }
    return nil;
}

+ (BOOL)isValid {
    return [User current] != nil;
}

+ (void)reset {
    [AspireAPIValidate storedData:@"" toFileName:FILE_USER];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USERDEFAULT_USER];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


#pragma mark - INIT
- (id)init
{
    if (!shared) {
        shared = [super init];
    }
    id dict = [AspireAPIValidate getDataFromFileName:FILE_USER];
    if (![dict isKindOfClass:[NSDictionary class]]) {return nil;}
    if (((NSDictionary*)dict).count > 0) {
        shared = [shared initWithJSONDictionary:dict];
        return shared;
    }
    return shared;
}
@end

@implementation UserAppUserPreference
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"appUserPreferenceId": @"appUserPreferenceId",
                                                    @"preferenceKey": @"preferenceKey",
                                                    @"preferenceValue": @"preferenceValue",
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[UserAppUserPreference alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([self respondsToSelector:NSSelectorFromString(obj)]) {
                [self setValue:[dict valueForKey:obj] forKey:obj];
            }
        }];
    }
    return self;
}

- (NSDictionary *)JSONDictionary
{
    return [self dictionaryWithValuesForKeys:UserAppUserPreference.properties.allValues];
}
@end

@implementation UserEmail
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"emailId": @"emailId",
                                                    @"emailType": @"emailType",
                                                    @"emailAddress": @"emailAddress",
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[UserEmail alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([self respondsToSelector:NSSelectorFromString(obj)]) {
                [self setValue:[dict valueForKey:obj] forKey:obj];
            }
        }];
    }
    return self;
}

- (NSDictionary *)JSONDictionary
{
    return [self dictionaryWithValuesForKeys:UserEmail.properties.allValues];
}
@end

@implementation UserFax
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"countryCode": @"countryCode",
                                                    @"faxNumber": @"faxNumber",
                                                    @"faxNote": @"faxNote",
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[UserFax alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([self respondsToSelector:NSSelectorFromString(obj)]) {
                [self setValue:[dict valueForKey:obj] forKey:obj];
            }
        }];
    }
    return self;
}

- (NSDictionary *)JSONDictionary
{
    return [self dictionaryWithValuesForKeys:UserFax.properties.allValues];
}
@end

@implementation UserLocation
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"additionalInformation": @"additionalInformation",
                                                    @"locationType": @"locationType",
                                                    @"locationId": @"locationId",
                                                    @"address": @"address",
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[UserLocation alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([self respondsToSelector:NSSelectorFromString(obj)]) {
                [self setValue:[dict valueForKey:obj] forKey:obj];
            }
        }];
        _address = [UserAddress fromJSONDictionary:(id)_address];
    }
    return self;
}

- (NSDictionary *)JSONDictionary
{
    id dict = [[self dictionaryWithValuesForKeys:UserLocation.properties.allValues] mutableCopy];
    
    // Map values that need translation
    [dict addEntriesFromDictionary:@{
                                     @"address": [_address JSONDictionary],
                                     }];
    
    return dict;
}
@end

@implementation UserAddress
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"addressLine1": @"addressLine1",
                                                    @"addressLine2": @"addressLine2",
                                                    @"addressLine3": @"addressLine3",
                                                    @"addressLine4": @"addressLine4",
                                                    @"addressLine5": @"addressLine5",
                                                    @"city": @"city",
                                                    @"country": @"country",
//                                                    @"stateProvince": @"stateProvince",
                                                    @"zipCode": @"zipCode",
//                                                    @"geoLatitute": @"geoLatitute",
//                                                    @"geoLongitude": @"geoLongitude",
                                                    @"addressDescription": @"addressDescription",
                                                    @"county": @"county",
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[UserAddress alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([self respondsToSelector:NSSelectorFromString(obj)]) {
                [self setValue:[dict valueForKey:obj] forKey:obj];
            }
        }];
    }
    return self;
}

- (NSDictionary *)JSONDictionary
{
    return [self dictionaryWithValuesForKeys:UserAddress.properties.allValues];
}
@end

@implementation UserMembership
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"membershipCategory": @"membershipCategory",
                                                    @"referenceName": @"referenceName",
                                                    @"referenceNumber": @"referenceNumber",
                                                    @"externalReferenceNumber": @"externalReferenceNumber",
                                                    @"uniqueIdentifier": @"uniqueIdentifier",
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[UserMembership alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([self respondsToSelector:NSSelectorFromString(obj)]) {
                [self setValue:[dict valueForKey:obj] forKey:obj];
            }
        }];
    }
    return self;
}

- (NSDictionary *)JSONDictionary
{
    return [self dictionaryWithValuesForKeys:UserMembership.properties.allValues];
}
@end

@implementation UserPhone
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"phoneId": @"phoneId",
                                                    @"phoneType": @"phoneType",
                                                    @"phoneCountryCode": @"phoneCountryCode",
                                                    @"phoneAreaCode": @"phoneAreaCode",
                                                    @"phoneNumber": @"phoneNumber",
                                                    @"phoneExtension": @"phoneExtension",
                                                    @"phoneNote": @"phoneNote",
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[UserPhone alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([self respondsToSelector:NSSelectorFromString(obj)]) {
                [self setValue:[dict valueForKey:obj] forKey:obj];
            }
        }];
    }
    return self;
}

- (NSDictionary *)JSONDictionary
{
    return [self dictionaryWithValuesForKeys:UserPhone.properties.allValues];
}
@end

@implementation UserPreference
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"preferenceType": @"preferenceType",
                                                    @"preferenceValue": @"preferenceValue",
                                                    @"preferenceCategory": @"preferenceCategory",
                                                    @"preferenceId": @"preferenceId"
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[UserPreference alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([self respondsToSelector:NSSelectorFromString(obj)]) {
                [self setValue:[dict valueForKey:obj] forKey:obj];
            }
        }];
    }
    return self;
}

- (NSDictionary *)JSONDictionary
{
    return [self dictionaryWithValuesForKeys:UserPreference.properties.allValues];
}
@end

@implementation UserRelationship
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"relationshipType": @"relationshipType",
                                                    @"partyId": @"partyID",
                                                    @"partyType": @"partyType",
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[UserRelationship alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([self respondsToSelector:NSSelectorFromString(obj)]) {
                [self setValue:[dict valueForKey:obj] forKey:obj];
            }
        }];
    }
    return self;
}

- (void)setValue:(nullable id)value forKey:(NSString *)key
{
    id resolved = UserRelationship.properties[key];
    if (resolved) [super setValue:value forKey:resolved];
}

- (NSDictionary *)JSONDictionary
{
    id dict = [[self dictionaryWithValuesForKeys:UserRelationship.properties.allValues] mutableCopy];
    
    // Rewrite property names that differ in JSON
    for (id jsonName in UserRelationship.properties) {
        id propertyName = UserRelationship.properties[jsonName];
        if (![jsonName isEqualToString:propertyName]) {
            dict[jsonName] = dict[propertyName];
            [dict removeObjectForKey:propertyName];
        }
    }
    
    return dict;
}
@end

@implementation UserSocialMedia
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"webpage": @"webpage",
                                                    @"url": @"url",
                                                    @"pageType": @"pageType",
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[UserSocialMedia alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([self respondsToSelector:NSSelectorFromString(obj)]) {
                [self setValue:[dict valueForKey:obj] forKey:obj];
            }
        }];
    }
    return self;
}

- (NSDictionary *)JSONDictionary
{
    return [self dictionaryWithValuesForKeys:UserSocialMedia.properties.allValues];
}
@end

@implementation UserWebsite
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"websiteType": @"websiteType",
                                                    @"webURL": @"webURL",
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[UserWebsite alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([self respondsToSelector:NSSelectorFromString(obj)]) {
                [self setValue:[dict valueForKey:obj] forKey:obj];
            }
        }];
    }
    return self;
}

- (NSDictionary *)JSONDictionary
{
    return [self dictionaryWithValuesForKeys:UserWebsite.properties.allValues];
}
@end

#pragma mark - Objects send to api
@implementation AAFPassword
@synthesize value;
- (id)initWithPassword:(NSString *)password {
    if (self = [super init]) {
        value = password;
    }
    return self;
}
@end

@implementation AAFRecoveryQuestion
- (id)initWithQuestion:(NSString *)question answer:(NSString *)answer {
    if (self = [super init]) {
        _answer = answer;
        _question = question;
    }
    return self;
}
@end
NS_ASSUME_NONNULL_END
