//
//  PMAUser.m
//  AspireApiFramework
//
//  Created by dai on 8/24/18.
//  Copyright © 2018 com.aspirelifestyles.ios.mobileconcierge.UnitTest. All rights reserved.
//

#import "PMAUser.h"
#import "NSObject+AspireApi.h"

// Shorthand for simple blocks
#define λ(decl, expr) (^(decl) { return (expr); })

// nil → NSNull conversion for JSON dictionaries
static id NSNullify(id _Nullable x) {
    return (x == nil || x == NSNull.null) ? NSNull.null : x;
}

NS_ASSUME_NONNULL_BEGIN

#pragma mark - Private model interfaces

@interface PMAUser (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface PMAContacts (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface PMAEmail (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface PMAPhone (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface PMALocation (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface PMAParentParty (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface PMAPartyPerson (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

static id map(id collection, id (^f)(id value)) {
    id result = nil;
    if ([collection isKindOfClass:NSArray.class]) {
        result = [NSMutableArray arrayWithCapacity:[collection count]];
        for (id x in collection) [result addObject:f(x)];
    } else if ([collection isKindOfClass:NSDictionary.class]) {
        result = [NSMutableDictionary dictionaryWithCapacity:[collection count]];
        for (id key in collection) [result setObject:f([collection objectForKey:key]) forKey:key];
    }
    return result;
}

#pragma mark - JSON serialization

PMAUser *_Nullable PMAUserFromData(NSData *data, NSError **error)
{
    @try {
        id json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:error];
        return *error ? nil : [PMAUser fromJSONDictionary:json];
    } @catch (NSException *exception) {
        *error = [NSError errorWithDomain:@"JSONSerialization" code:-1 userInfo:@{ @"exception": exception }];
        return nil;
    }
}

PMAUser *_Nullable PMAUserFromJSON(NSString *json, NSStringEncoding encoding, NSError **error)
{
    return PMAUserFromData([json dataUsingEncoding:encoding], error);
}

NSData *_Nullable PMAUserToData(PMAUser *user, NSError **error)
{
    @try {
        id json = [user JSONDictionary];
        NSData *data = [NSJSONSerialization dataWithJSONObject:json options:kNilOptions error:error];
        return *error ? nil : data;
    } @catch (NSException *exception) {
        *error = [NSError errorWithDomain:@"JSONSerialization" code:-1 userInfo:@{ @"exception": exception }];
        return nil;
    }
}

NSString *_Nullable PMAUserToJSON(PMAUser *user, NSStringEncoding encoding, NSError **error)
{
    NSData *data = PMAUserToData(user, error);
    return data ? [[NSString alloc] initWithData:data encoding:encoding] : nil;
}

@implementation PMAUser
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"partyId": @"partyID",
                                                    @"partyPerson": @"partyPerson",
                                                    @"active": @"active",
                                                    @"parentParties": @"parentParties",
                                                    @"names": @"names",
                                                    @"additionalInformations": @"additionalInformations",
                                                    @"rewards": @"rewards",
                                                    @"partyEntitlements": @"partyEntitlements",
                                                    @"consents": @"consents",
                                                    @"identifications": @"identifications",
                                                    @"partyVerifications": @"partyVerifications",
                                                    @"personalAttributes": @"personalAttributes",
                                                    @"PDIRecord": @"pdiRecord",
                                                    @"partyType": @"partyType",
                                                    @"partyStatusCode": @"partyStatusCode",
                                                    @"partyName": @"partyName",
                                                    @"recordEffectiveFrom": @"recordEffectiveFrom",
                                                    @"recordEffectiveTo": @"recordEffectiveTo",
                                                    @"importanceLevel": @"importanceLevel",
                                                    @"locations": @"locations",
                                                    @"contacts": @"contacts",
                                                    };
}

+ (_Nullable instancetype)fromData:(NSData *)data error:(NSError *_Nullable *)error
{
    return PMAUserFromData(data, error);
}

+ (_Nullable instancetype)fromJSON:(NSString *)json encoding:(NSStringEncoding)encoding error:(NSError *_Nullable *)error
{
    return PMAUserFromJSON(json, encoding, error);
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[PMAUser alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [self setValue:[dict valueForKey:obj] forKey:obj];
        }];
        _partyPerson = [PMAPartyPerson fromJSONDictionary:(id)_partyPerson];
        _parentParties = map(_parentParties, λ(id x, [PMAParentParty fromJSONDictionary:x]));
        _locations = map(_locations, λ(id x, [PMALocation fromJSONDictionary:x]));
        _contacts = [PMAContacts fromJSONDictionary:(id)_contacts];
    }
    return self;
}

- (void)setValue:(nullable id)value forKey:(NSString *)key
{
    id resolved = PMAUser.properties[key];
    if (resolved) [super setValue:value forKey:resolved];
}

- (NSDictionary *)JSONDictionary
{
    id dict = [[self dictionaryWithValuesForKeys:PMAUser.properties.allValues] mutableCopy];
    
    // Rewrite property names that differ in JSON
    for (id jsonName in PMAUser.properties) {
        id propertyName = PMAUser.properties[jsonName];
        if (![jsonName isEqualToString:propertyName]) {
            dict[jsonName] = dict[propertyName];
            [dict removeObjectForKey:propertyName];
        }
    }
    
    // Map values that need translation
    [dict addEntriesFromDictionary:@{
                                     @"partyPerson": [_partyPerson JSONDictionary],
                                     @"parentParties": map(_parentParties, λ(id x, [x JSONDictionary])),
                                     @"PDIRecord": _pdiRecord ? @YES : @NO,
                                     @"locations": map(_locations, λ(id x, [x JSONDictionary])),
                                     @"contacts": [_contacts JSONDictionary],
                                     }];
    
    return dict;
}

- (NSData *_Nullable)toData:(NSError *_Nullable *)error
{
    return PMAUserToData(self, error);
}

- (NSString *_Nullable)toJSON:(NSStringEncoding)encoding error:(NSError *_Nullable *)error
{
    return PMAUserToJSON(self, encoding, error);
}
@end

@implementation PMAContacts
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"emails": @"emails",
                                                    @"faxes": @"faxes",
                                                    @"phones": @"phones",
                                                    @"websites": @"websites",
                                                    @"socialMedias": @"socialMedias",
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[PMAContacts alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [self setValue:[dict valueForKey:obj] forKey:obj];
        }];
        _emails = map(_emails, λ(id x, [PMAEmail fromJSONDictionary:x]));
        _phones = map(_phones, λ(id x, [PMAPhone fromJSONDictionary:x]));
    }
    return self;
}

- (NSDictionary *)JSONDictionary
{
    id dict = [[self dictionaryWithValuesForKeys:PMAContacts.properties.allValues] mutableCopy];
    
    // Map values that need translation
    [dict addEntriesFromDictionary:@{
                                     @"emails": map(_emails, λ(id x, [x JSONDictionary])),
                                     @"phones": map(_phones, λ(id x, [x JSONDictionary])),
                                     }];
    
    return dict;
}
@end

@implementation PMAEmail
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"emailId": @"emailID",
                                                    @"emailTypeDescription": @"emailTypeDescription",
                                                    @"contactTypeDescription": @"contactTypeDescription",
                                                    @"PDIRecord": @"pdiRecord",
                                                    @"emailAddress": @"emailAddress",
                                                    @"recordEffectiveFrom": @"recordEffectiveFrom",
                                                    @"recordEffectiveTo": @"recordEffectiveTo",
                                                    @"emailTypeId": @"emailTypeID",
                                                    @"emailTypeCode": @"emailTypeCode",
                                                    @"description": @"theDescription",
                                                    @"contactTypeId": @"contactTypeID",
                                                    @"contactTypeCode": @"contactTypeCode",
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[PMAEmail alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [self setValue:[dict valueForKey:obj] forKey:obj];
        }];
    }
    return self;
}

- (void)setValue:(nullable id)value forKey:(NSString *)key
{
    id resolved = PMAEmail.properties[key];
    if (resolved) [super setValue:value forKey:resolved];
}

- (NSDictionary *)JSONDictionary
{
    id dict = [[self dictionaryWithValuesForKeys:PMAEmail.properties.allValues] mutableCopy];
    
    // Rewrite property names that differ in JSON
    for (id jsonName in PMAEmail.properties) {
        id propertyName = PMAEmail.properties[jsonName];
        if (![jsonName isEqualToString:propertyName]) {
            dict[jsonName] = dict[propertyName];
            [dict removeObjectForKey:propertyName];
        }
    }
    
    // Map values that need translation
    [dict addEntriesFromDictionary:@{
                                     @"PDIRecord": _pdiRecord ? @YES : @NO,
                                     }];
    
    return dict;
}
@end

@implementation PMAPhone
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"phoneId": @"phoneID",
                                                    @"phoneTypeDescription": @"phoneTypeDescription",
                                                    @"contactTypeDescription": @"contactTypeDescription",
                                                    @"PDIRecord": @"pdiRecord",
                                                    @"phoneCountryCode": @"phoneCountryCode",
                                                    @"phoneAreaCode": @"phoneAreaCode",
                                                    @"phoneNumber": @"phoneNumber",
                                                    @"phoneExtension": @"phoneExtension",
                                                    @"recordEffectiveFrom": @"recordEffectiveFrom",
                                                    @"recordEffectiveTo": @"recordEffectiveTo",
                                                    @"phoneTypeId": @"phoneTypeID",
                                                    @"phoneTypeCode": @"phoneTypeCode",
                                                    @"description": @"theDescription",
                                                    @"contactTypeId": @"contactTypeID",
                                                    @"contactTypeCode": @"contactTypeCode",
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[PMAPhone alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [self setValue:[dict valueForKey:obj] forKey:obj];
        }];
    }
    return self;
}

- (void)setValue:(nullable id)value forKey:(NSString *)key
{
    id resolved = PMAPhone.properties[key];
    if (resolved) [super setValue:value forKey:resolved];
}

- (NSDictionary *)JSONDictionary
{
    id dict = [[self dictionaryWithValuesForKeys:PMAPhone.properties.allValues] mutableCopy];
    
    // Rewrite property names that differ in JSON
    for (id jsonName in PMAPhone.properties) {
        id propertyName = PMAPhone.properties[jsonName];
        if (![jsonName isEqualToString:propertyName]) {
            dict[jsonName] = dict[propertyName];
            [dict removeObjectForKey:propertyName];
        }
    }
    
    // Map values that need translation
    [dict addEntriesFromDictionary:@{
                                     @"PDIRecord": _pdiRecord ? @YES : @NO,
                                     }];
    
    return dict;
}
@end

@implementation PMALocation
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"locationId": @"locationID",
                                                    @"locationTypeDescription": @"locationTypeDescription",
                                                    @"contactTypeDescription": @"contactTypeDescription",
                                                    @"PDIRecord": @"pdiRecord",
                                                    @"recordEffectiveFrom": @"recordEffectiveFrom",
                                                    @"recordEffectiveTo": @"recordEffectiveTo",
                                                    @"additionalInformation": @"additionalInformation",
                                                    @"locationTypeId": @"locationTypeID",
                                                    @"locationTypeCode": @"locationTypeCode",
                                                    @"address": @"address",
                                                    @"siteDetail": @"siteDetail",
                                                    @"description": @"theDescription",
                                                    @"contactTypeId": @"contactTypeID",
                                                    @"contactTypeCode": @"contactTypeCode",
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[PMALocation alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [self setValue:[dict valueForKey:obj] forKey:obj];
        }];
    }
    return self;
}

- (void)setValue:(nullable id)value forKey:(NSString *)key
{
    id resolved = PMALocation.properties[key];
    if (resolved) [super setValue:value forKey:resolved];
}

- (NSDictionary *)JSONDictionary
{
    id dict = [[self dictionaryWithValuesForKeys:PMALocation.properties.allValues] mutableCopy];
    
    // Rewrite property names that differ in JSON
    for (id jsonName in PMALocation.properties) {
        id propertyName = PMALocation.properties[jsonName];
        if (![jsonName isEqualToString:propertyName]) {
            dict[jsonName] = dict[propertyName];
            [dict removeObjectForKey:propertyName];
        }
    }
    
    // Map values that need translation
    [dict addEntriesFromDictionary:@{
                                     @"PDIRecord": _pdiRecord ? @YES : @NO,
                                     }];
    
    return dict;
}
@end

@implementation PMAParentParty
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"partyId": @"partyID",
                                                    @"active": @"active",
                                                    @"PDIRecord": @"pdiRecord",
                                                    @"partyType": @"partyType",
                                                    @"partyStatusCode": @"partyStatusCode",
                                                    @"partyName": @"partyName",
                                                    @"recordEffectiveFrom": @"recordEffectiveFrom",
                                                    @"recordEffectiveTo": @"recordEffectiveTo",
                                                    @"importanceLevel": @"importanceLevel",
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[PMAParentParty alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [self setValue:[dict valueForKey:obj] forKey:obj];
        }];
    }
    return self;
}

- (void)setValue:(nullable id)value forKey:(NSString *)key
{
    id resolved = PMAParentParty.properties[key];
    if (resolved) [super setValue:value forKey:resolved];
}

- (NSDictionary *)JSONDictionary
{
    id dict = [[self dictionaryWithValuesForKeys:PMAParentParty.properties.allValues] mutableCopy];
    
    // Rewrite property names that differ in JSON
    for (id jsonName in PMAParentParty.properties) {
        id propertyName = PMAParentParty.properties[jsonName];
        if (![jsonName isEqualToString:propertyName]) {
            dict[jsonName] = dict[propertyName];
            [dict removeObjectForKey:propertyName];
        }
    }
    
    // Map values that need translation
    [dict addEntriesFromDictionary:@{
                                     @"PDIRecord": _pdiRecord ? @YES : @NO,
                                     }];
    
    return dict;
}
@end

@implementation PMAPartyPerson
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"employee": @"employee",
                                                    @"fullName": @"fullName",
                                                    @"firstName": @"firstName",
                                                    @"middleName": @"middleName",
                                                    @"lastName": @"lastName",
                                                    @"displayName": @"displayName",
                                                    @"aliasName": @"aliasName",
                                                    @"prefix": @"prefix",
                                                    @"suffix": @"suffix",
                                                    @"salutation": @"salutation",
                                                    @"gender": @"gender",
                                                    @"maritalStatus": @"maritalStatus",
                                                    @"recordEffectiveFrom": @"recordEffectiveFrom",
                                                    @"recordEffectiveTo": @"recordEffectiveTo",
                                                    @"dateOfBirth": @"dateOfBirth",
                                                    @"residentCountry": @"residentCountry",
                                                    @"homeCountry": @"homeCountry",
                                                    @"nationality": @"nationality",
                                                    @"signature": @"signature",
                                                    @"profilePic": @"profilePic",
                                                    @"disabilityStatus": @"disabilityStatus",
                                                    @"placeOfBirth": @"placeOfBirth",
                                                    @"VIP": @"vip",
                                                    @"personType": @"personType",
                                                    @"employmentStatus": @"employmentStatus",
                                                    @"statusTest": @"statusTest",
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[PMAPartyPerson alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [[dict allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [self setValue:[dict valueForKey:obj] forKey:obj];
        }];
    }
    return self;
}

- (void)setValue:(nullable id)value forKey:(NSString *)key
{
    id resolved = PMAPartyPerson.properties[key];
    if (resolved) [super setValue:value forKey:resolved];
}

- (NSDictionary *)JSONDictionary
{
    id dict = [[self dictionaryWithValuesForKeys:PMAPartyPerson.properties.allValues] mutableCopy];
    
    // Rewrite property names that differ in JSON
    for (id jsonName in PMAPartyPerson.properties) {
        id propertyName = PMAPartyPerson.properties[jsonName];
        if (![jsonName isEqualToString:propertyName]) {
            dict[jsonName] = dict[propertyName];
            [dict removeObjectForKey:propertyName];
        }
    }
    
    return dict;
}
@end

NS_ASSUME_NONNULL_END
