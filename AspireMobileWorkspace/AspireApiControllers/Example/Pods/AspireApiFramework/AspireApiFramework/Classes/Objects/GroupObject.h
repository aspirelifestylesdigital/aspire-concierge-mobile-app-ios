//
//  GroupObject.h
//  ModelTest
//
//  Created by 💀 on 5/28/18.
//  Copyright © 2018 com.s3corp.testmodel.UNitTest111. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "OKTAUserProfile.h"

@class User;
@class UserEmail;
@class UserFax;
@class UserLocation;
@class UserAddress;
@class UserMembership;
@class UserPhone;
@class UserPreference;
@class UserRelationship;
@class UserSocialMedia;
@class UserWebsite;
@class UserAppUserPreference;

NS_ASSUME_NONNULL_BEGIN

#pragma mark - Object interfaces

@interface AccountAuthentication: NSObject
+ (void) reset;
+ (void)storedWithDictionary:(NSDictionary *)dict;
+ (nullable NSString*) username;
+ (nullable NSString*) password;
@end

@interface User : NSObject
@property (nonatomic, copy) NSString *partyId;
@property (nonatomic, copy) NSString *salutation;
@property (nonatomic, copy) NSString *fullName;
@property (nonatomic, copy) NSString *firstName;
@property (nonatomic, copy) NSString *middleName;
@property (nonatomic, copy) NSString *lastName;
@property (nonatomic, copy) NSString *displayName;
@property (nonatomic, copy) NSString *aliasName;
@property (nonatomic, copy) NSString *prefix;
@property (nonatomic, copy) NSString *suffix;
@property (nonatomic, copy) NSString *gender;
@property (nonatomic, copy) NSString *maritalStatus;
@property (nonatomic, copy) NSString *recordEffectiveFrom;
@property (nonatomic, copy) NSString *recordEffectiveTo;
@property (nonatomic, copy) NSString *dateOfBirth;
@property (nonatomic, copy) NSString *residentCountry;
@property (nonatomic, copy) NSString *homeCountry;
@property (nonatomic, copy) NSString *nationality;
@property (nonatomic, copy) NSString *signature;
@property (nonatomic, copy) NSString *profilePic;
@property (nonatomic, copy) NSString *disabilityStatus;
@property (nonatomic, copy) NSString *placeOfBirth;
@property (nonatomic, copy) NSString *vip;
@property (nonatomic, copy) NSString *employmentStatus;
@property (nonatomic, copy) NSArray<UserLocation *> *locations;
@property (nonatomic, copy) NSArray<UserEmail *> *emails;
@property (nonatomic, copy) NSArray<UserPreference *> *preferences;
@property (nonatomic, copy) NSArray<UserMembership *> *memberships;
@property (nonatomic, copy) NSArray<UserPhone *> *phones;
@property (nonatomic, copy) NSArray<UserFax *> *faxes;
@property (nonatomic, copy) NSArray<UserWebsite *> *websites;
@property (nonatomic, copy) NSArray<UserSocialMedia *> *socialMedias;
@property (nonatomic, copy) NSArray<UserRelationship *> *relationships;
@property (nonatomic, copy) NSArray<UserAppUserPreference *> *appUserPreferences;

+ (nullable User*) current;
+ (nullable NSDictionary *)convertDictionary;
+ (BOOL) isValid;
+ (void) reset;

+ (_Nullable instancetype)fromJSON:(NSString *)json encoding:(NSStringEncoding)encoding error:(NSError *_Nullable *)error;
+ (_Nullable instancetype)fromData:(NSData *)data error:(NSError *_Nullable *)error;
- (NSString *_Nullable)toJSON:(NSStringEncoding)encoding error:(NSError *_Nullable *)error;
- (NSData *_Nullable)toData:(NSError *_Nullable *)error;

- (id)convertToUserObjectWithClassName:(NSString*) className andPreferencesClassName:(NSString*) subClassName;
- (nullable NSString*)getPasscode;
- (nullable NSString*)getPolicyVersion;
- (BOOL) isGetNewsletter;
- (NSString*) isGetNewsletterDate;
- (nullable NSString*)getPreferencesIdFromType:(NSString*)type;
- (nullable NSString*)getLocationIdFromType:(NSString*)type;
- (nullable NSString*)getEmailIdFromType:(NSString*)type;
- (nullable NSString*)getPhoneIdFromType:(NSString*)type;
- (nullable NSString*)getMembershipIdFromCategory:(NSString*)type;
- (nullable NSString*)getAppUserPreferenceIdFromKey:(NSString*)key;

+ (User*) convertFromOKTAUser:(OKTAUserProfile*)obj;
@end

@interface UserAppUserPreference : NSObject
@property (nonatomic, copy) NSString *appUserPreferenceId;
@property (nonatomic, copy) NSString *preferenceKey;
@property (nonatomic, copy) NSString *preferenceValue;
@end

@interface UserEmail : NSObject
@property (nonatomic, copy) NSString *emailId;
@property (nonatomic, copy) NSString *emailType;
@property (nonatomic, copy) NSString *emailAddress;
@end

@interface UserFax : NSObject
@property (nonatomic, copy) NSString *countryCode;
@property (nonatomic, copy) NSString *faxNumber;
@property (nonatomic, copy) NSString *faxNote;
@end

@interface UserLocation : NSObject
@property (nonatomic, copy)   NSString *additionalInformation;
@property (nonatomic, copy)   NSString *locationType;
@property (nonatomic, copy)   NSString *locationId;
@property (nonatomic, strong) UserAddress *address;
@end

@interface UserAddress : NSObject
@property (nonatomic, copy)   NSString *addressLine1;
@property (nonatomic, copy)   NSString *addressLine2;
@property (nonatomic, copy)   NSString *addressLine3;
@property (nonatomic, copy)   NSString *addressLine4;
@property (nonatomic, copy)   NSString *addressLine5;
@property (nonatomic, copy)   NSString *city;
@property (nonatomic, copy)   NSString *country;
@property (nonatomic, copy)   NSString *stateProvince;
@property (nonatomic, copy)   NSString *zipCode;
//@property (nonatomic, assign) double geoLatitute;
//@property (nonatomic, assign) double geoLongitude;
@property (nonatomic, copy)   NSString *addressDescription;
@property (nonatomic, copy)   NSString *county;
@end

@interface UserMembership : NSObject
@property (nonatomic, copy) NSString *membershipId;
@property (nonatomic, copy) NSString *membershipCategory;
@property (nonatomic, copy) NSString *referenceName;
@property (nonatomic, copy) NSString *referenceNumber;
@property (nonatomic, copy) NSString *externalReferenceNumber;
@property (nonatomic, copy) NSString *uniqueIdentifier;
@end

@interface UserPhone : NSObject
@property (nonatomic, copy) NSString *phoneType;
@property (nonatomic, copy) NSString *phoneId;
@property (nonatomic, copy) NSString *phoneCountryCode;
@property (nonatomic, copy) NSString *phoneAreaCode;
@property (nonatomic, copy) NSString *phoneNumber;
@property (nonatomic, copy) NSString *phoneExtension;
@property (nonatomic, copy) NSString *phoneNote;
@end

@interface UserPreference : NSObject
@property (nonatomic, copy) NSString *preferenceId;
@property (nonatomic, copy) NSString *preferenceCategory;
@property (nonatomic, copy) NSString *preferenceType;
@property (nonatomic, copy) NSString *preferenceValue;
@end

@interface UserRelationship : NSObject
@property (nonatomic, copy) NSString *relationshipType;
@property (nonatomic, copy) NSString *partyID;
@property (nonatomic, copy) NSString *partyType;
@end

@interface UserSocialMedia : NSObject
@property (nonatomic, copy) NSString *webpage;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *pageType;
@end

@interface UserWebsite : NSObject
@property (nonatomic, copy) NSString *websiteType;
@property (nonatomic, copy) NSString *webURL;
@end

#pragma mark - Objects send to api
@interface AAFPassword : NSObject
@property (nonatomic, copy) NSString *value;
-(id) initWithPassword:(NSString*)password;
@end

@interface AAFRecoveryQuestion : NSObject
@property (nonatomic, copy) NSString *question;
@property (nonatomic, copy) NSString *answer;
-(id) initWithQuestion:(NSString*)question answer:(NSString*) answer;
@end
NS_ASSUME_NONNULL_END
