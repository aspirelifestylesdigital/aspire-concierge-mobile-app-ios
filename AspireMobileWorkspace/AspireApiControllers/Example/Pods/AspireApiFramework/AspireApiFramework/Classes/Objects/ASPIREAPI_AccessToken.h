//
//  UserAccessToken.h
//  ModelTest
//
//  Created by 💀 on 5/28/18.
//  Copyright © 2018 com.s3corp.testmodel.UNitTest111. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#pragma mark - Object interfaces

@interface ASPIREAPI_AccessToken : NSObject
@property (nonatomic, copy)   NSString *accessToken;
@property (nonatomic, copy)   NSString *tokenType;
@property (nonatomic, assign) NSInteger expiresIn;
@property (nonatomic, copy)   NSString *scope;
@property (nonatomic, copy)   NSString *theIDToken;


+ (ASPIREAPI_AccessToken*) user;
+ (ASPIREAPI_AccessToken*) service;
+ (void) resetUser;
+ (BOOL) isValid;
+ (void) saveJSONUserDictionary:(NSDictionary *)dict;
+ (void) removeToken;
- (instancetype) initWithJSONDictionary:(NSDictionary *)dict;
- (NSString *_Nullable)toJSON:(NSStringEncoding)encoding error:(NSError *_Nullable *)error;
- (NSData *_Nullable)toData:(NSError *_Nullable *)error;
@end

NS_ASSUME_NONNULL_END
