//
//  TestObject.h
//  AspireApiFramework
//
//  Created by VuongTC on 10/26/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TestObject : NSObject

- (void) printSth;

@end

NS_ASSUME_NONNULL_END
