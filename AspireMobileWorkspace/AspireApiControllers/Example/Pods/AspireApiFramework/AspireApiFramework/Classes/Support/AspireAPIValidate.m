//
//  AspireAPIValidate.m
//  MobileConciergeUSDemo
//
//  Created by 💀 on 5/31/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "AspireAPIValidate.h"

static AspireAPIValidate *shared = nil;

@implementation NSString (AspireAPI_validate)

- (NSString*) convertSalutationToAspireAPI {
    if ([self isValidSalutation]) {
        return self;
    } else if  ([self isEqualToString:@"Mrs"] || [self isEqualToString:@"Mr"]) {
        return [self stringByAppendingString:@"."];
    } else {
        return @"Doctor";
    }
}

- (NSString*) convertSalutationFromAspireAPI {
    if ([[self lowercaseString] containsString:[@"Doctor" lowercaseString]]) {
        return @"Dr";
    } else if  ([self isEqualToString:@"Mrs."]) {
        return @"Mrs";
    } else if  ([self isEqualToString:@"Mr."]) {
        return @"Mr";
    } else {
        return self;
    }
}

- (BOOL) isValidGender {
    
    __block BOOL isValid = false;
    
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_enter(group);
    [AspireAPIValidate getDataFromFileName:@"update-profile-schema" completion:^(NSDictionary * _Nullable data) {
        isValid = true;
        if (data != nil) {
            NSDictionary* listvalid = [self getResultFromDictionay:data withKey:@"gender"];
            if (listvalid != nil) {
                isValid = [[listvalid mutableArrayValueForKey:@"enum"] containsObject:self];
            } else {
                isValid = true;
            }
        } else {
            isValid = true;
        }
        dispatch_group_leave(group);
    }];
    dispatch_group_wait(group, DISPATCH_TIME_FOREVER);

    return isValid;
}

- (BOOL)isValidSalutation {
    
    __block BOOL isValid = false;
    
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_enter(group);
    [AspireAPIValidate getDataFromFileName:@"update-profile-schema" completion:^(NSDictionary * _Nullable data) {
        isValid = true;
        if (data != nil) {
            NSDictionary* listvalid = [self getResultFromDictionay:data withKey:@"salutation"];
            if (listvalid != nil) {
                isValid = [[listvalid mutableArrayValueForKey:@"enum"] containsObject:self];
            } else {
                isValid = true;
            }
        } else {
            isValid = true;
        }
        dispatch_group_leave(group);
    }];
    dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
    
    return isValid;
}

#pragma mark - PRIVATE
- (nullable id) getResultFromDictionay:(NSDictionary*)dict withKey:(NSString*) keyMain {
    
    if ([[dict allKeys] containsObject:keyMain]) {
        return [dict objectForKey:keyMain];
    } else {
        for (NSString* key in [dict allKeys]) {
            if ([[dict objectForKey:key] isKindOfClass:[NSDictionary class]]) {
                return [self getResultFromDictionay:[dict objectForKey:key] withKey:keyMain];
            }
        }
    }
    
    return nil;
}
@end

@implementation AspireAPIValidate

#pragma mark - API
+ (void)storedData:(id)data toFileName:(NSString *)fileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, fileName];
//    NSLog(@"%s => filePath %@ with data: %@", __PRETTY_FUNCTION__, filePath,data);
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) { // if file is not exist, create it.
//        NSLog(@"%s => create file %@",__PRETTY_FUNCTION__, filePath);
        [[NSData data] writeToFile:filePath atomically:true];
    }
    
    NSData *dictData = [NSKeyedArchiver archivedDataWithRootObject:data];
    [dictData writeToFile:filePath atomically:true];
    
//    NSLog(@"%s => %@ : stored",__PRETTY_FUNCTION__,fileName);
}

+ (id)getDataFromFileName:(NSString *)fileName {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, fileName];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) { // if file is not exist, create it.
        NSError* error = nil;
        NSData* data = [[NSData alloc] initWithContentsOfFile:filePath options:(NSDataReadingUncached) error:&error];
//        NSString*myJson = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
        if (error) {NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description); return nil;}
        id result = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        return result;
    } else {
        NSLog(@"%s => file not found {%@}",__PRETTY_FUNCTION__,filePath);
    }
    return nil;
}

#pragma mark - PRIVATE
+ (void) getDataFromFileName:(NSString*) fileName completion:(void (^)(NSDictionary * _Nullable))completion {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSBundle *bundle = [NSBundle bundleForClass:self.classForCoder];
        NSURL *bundleURL = [[bundle resourceURL] URLByAppendingPathComponent:@"AspireApiFramework.bundle"];
        NSBundle *resourceBundle = [NSBundle bundleWithURL:bundleURL];
        NSString*  filepath = [resourceBundle pathForResource:fileName ofType:@"json"];
        NSError* error;
        NSString*myJson = [[NSString alloc] initWithContentsOfFile:filepath encoding:NSUTF8StringEncoding error:&error];
        if(error != nil) {
             completion(nil);
            return;
        }
        
        id result = [NSJSONSerialization JSONObjectWithData:[myJson dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
        
        if(error != nil) {
            completion(nil);
            return;
        }
        completion(result);
    });
}

+ (void)getListFromFileName:(NSString *)fileName completion:(void (^)(NSArray * _Nullable))completion {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSBundle *bundle = [NSBundle bundleForClass:self.classForCoder];
        NSURL *bundleURL = [[bundle resourceURL] URLByAppendingPathComponent:@"AspireApiFramework.bundle"];
        NSBundle *resourceBundle = [NSBundle bundleWithURL:bundleURL];
        NSString*  filepath = [resourceBundle pathForResource:fileName ofType:@"json"];
        NSError* error;
        NSString*myJson = [[NSString alloc] initWithContentsOfFile:filepath encoding:NSUTF8StringEncoding error:&error];
        if(error != nil) {
            completion(nil);
            return;
        }
        
        id result = [NSJSONSerialization JSONObjectWithData:[myJson dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
        
        if(error != nil) {
            completion(nil);
            return;
        }
        completion(result);
    });
}

#pragma mark - INIT
+ (AspireAPIValidate *)current {
    static dispatch_once_t onceToken;
    @synchronized([AspireAPIValidate class]) {
        if (!shared)
            dispatch_once(&onceToken, ^{
                shared = [[AspireAPIValidate alloc] init];
            });
        return shared;
    }
    return nil;
}

- (id)init
{
    if (!shared) {
        shared = [super init];
    }
    return shared;
}
@end
