//
//  AspireAPIService.m
//  AspireAPI
//
//  Created by 💀 on 6/15/18.
//  Copyright © 2018 com.aspirelifestyles.ios.mobileconcierge.UnitTest. All rights reserved.
//

#import "AspireAPIService.h"
#import "ModelAspireApiManager.h"
#import "constant.h"

@implementation AspireAPIService

#define SHOULD_LOG true

#pragma mark - API
+ (void)revokeToken:(NSString *)token {
    NSMutableData *postData = [[NSMutableData alloc] initWithData:[[NSString stringWithFormat:@"token=%@",token] dataUsingEncoding:NSUTF8StringEncoding]];
//    [postData appendData:[@"&grant_type=password" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", DMA_AUTH_ACC, DMA_AUTH_PAS];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString* endcode = [authData base64EncodedStringWithOptions:0];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", endcode];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/x-www-form-urlencoded"};
    
#if DEBUG
    if (SHOULD_LOG) {
        printf("\n====POST revoke token ====\n%s\n=============\n",[[[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding] UTF8String]);
    }
#endif
    
    [AspireAPIService requestWithMethod:@"POST" urlString:ASPIREAPI_REVOKE_TOKEN_URL data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
#if DEBUG
            NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
        } else {
//            NSError* err;
//            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
//                                                                 options:kNilOptions
//                                                                   error:&err];
#if DEBUG
            if (SHOULD_LOG) {
                printf("\n==== response revoke token ====\n%s\n=============\n",[[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] UTF8String]);
            }
#endif
        }
    }];
}

+ (void)revokeTokenNotCache:(NSString *)token {
    NSMutableData *postData = [[NSMutableData alloc] initWithData:[[NSString stringWithFormat:@"token=%@",token] dataUsingEncoding:NSUTF8StringEncoding]];
    //    [postData appendData:[@"&grant_type=password" dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", DMA_AUTH_ACC, DMA_AUTH_PAS];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString* endcode = [authData base64EncodedStringWithOptions:0];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", endcode];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/x-www-form-urlencoded"};
    
#if DEBUG
    if (SHOULD_LOG) {
        printf("\n====POST revoke token ====\n%s\n=============\n",[[[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding] UTF8String]);
    }
#endif
    
    [AspireAPIService requestNotCacheWithMethod:@"POST" urlString:ASPIREAPI_REVOKE_TOKEN_URL data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
#if DEBUG
            NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
        } else {
            //            NSError* err;
            //            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
            //                                                                 options:kNilOptions
            //                                                                   error:&err];
#if DEBUG
            if (SHOULD_LOG) {
                printf("\n==== response revoke token ====\n%s\n=============\n",[[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] UTF8String]);
            }
#endif
        }
    }];
}

+ (void)requestAspireTokenWithUsername:(NSString *)username password:(NSString *)password completion:(void (^)(id, id))completion {
    
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion(nil,[NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    if ([username componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].count == 0 ||
        [password componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].count == 0) {
        if (completion)
        completion(nil,[NSError errorWithDomain:@"com.error" code:-1011 userInfo:@{@"message":@"username or password is invalid"}]);
        return;
    }
    
    NSMutableData *postData = [[NSMutableData alloc] initWithData:[@"grant_type=password" dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"&scope=openid" dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[[NSString stringWithFormat:@"&username=%@",username] dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[[NSString stringWithFormat:@"&password=%@",password] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", DMA_AUTH_ACC, DMA_AUTH_PAS];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString* endcode = [authData base64EncodedStringWithOptions:0];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", endcode];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/x-www-form-urlencoded"};
    
#if DEBUG
    if (SHOULD_LOG) {
        printf("\n====POST request accesstoken ====\n%s\n=============\n",[[[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding] UTF8String]);
    }
#endif
    
    [AspireAPIService requestWithMethod:@"POST" urlString:ASPIREAPI_TOKEN_URL data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
#if DEBUG
            NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
            if(completion) {
                completion(nil,error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
#if DEBUG
            if (SHOULD_LOG) {
                printf("\n==== response accesstoken ====\n%s\n=============\n",[[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] UTF8String]);
            }
#endif
            if (err == nil) {
                if(completion) {
                    completion(json,[AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(nil,error);
                }
            }
        }
    }];
}
    
+ (void) retrieveCurrentUserFromAccessToken:(NSString *)token tokenType:(NSString*)tokenType completion:(void (^)(id _Nullable, id _Nullable))completion {
    
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion(nil,[NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json",
                               @"X-App-Id": [AspireAPIService getXAppId],
                               @"X-Organization": [AspireAPIService getClient]
                               };
    #if DEBUG
    if (SHOULD_LOG) {
        printf("\n====GET retrieve user ====\n%s\n=============\n",[[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:headers options:(NSJSONWritingPrettyPrinted) error:nil] encoding:NSUTF8StringEncoding] UTF8String]);
    }
#endif
    [AspireAPIService requestWithMethod:@"GET" urlString:ASPIREAPI_PROFILE_URL data:nil header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
#if DEBUG
            NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
            if(completion) {
                completion(nil,error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
#if DEBUG
            if (SHOULD_LOG) {
                printf("\n==== response retrieve user ====\n%s\n=============\n",[[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] UTF8String]);
            }
#endif
            
            if (err == nil) {
                if(completion) {
                    completion(json,[AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1021 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(nil,error);
                }
            }
        }
    }];
}
    
+ (void)createUserFromAccessToken:(NSString *)token tokenType:(NSString *)tokenType withUserInfo:(NSDictionary *)userInfo completion:(void (^)(id _Nullable))completion {
    
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion([NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    #if DEBUG
    if (SHOULD_LOG) {
        printf("\n====POST create user ====\n%s\n=============\n",[[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:userInfo options:(NSJSONWritingPrettyPrinted) error:nil] encoding:NSUTF8StringEncoding] UTF8String]);
    }
#endif
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:userInfo options:0 error:nil];

    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json",
                               @"X-App-Id": [AspireAPIService getXAppId],
                               @"X-Organization": [AspireAPIService getClient]
                               };
    
    [AspireAPIService requestWithMethod:@"POST" urlString:ASPIREAPI_CREATE_CUSTOMER_URL data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
#if DEBUG
            NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
            if(completion) {
                completion(error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
#if DEBUG
            if (SHOULD_LOG) {
                printf("\n==== response create user ====\n%s\n=============\n",[[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] UTF8String]);
            }
#endif
            
            if (err == nil) {
                if(completion) {
                    completion([AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1021 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(error);
                }
            }
        }
    }];
}
    
+ (void)updateUserFromAccessToken:(NSString *)token tokenType:(NSString *)tokenType withUserInfo:(NSDictionary *)userInfo completion:(void (^)(id _Nullable))completion {
    
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion([NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    #if DEBUG
    if (SHOULD_LOG) {
        printf("\n====PUT update user ====\n%s\n=============\n",[[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:userInfo options:(NSJSONWritingPrettyPrinted) error:nil] encoding:NSUTF8StringEncoding] UTF8String]);
    }
#endif
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:userInfo options:0 error:nil];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json",
                               @"X-App-Id": [AspireAPIService getXAppId],
                               @"X-Organization": [AspireAPIService getClient]
                               };
    
    [AspireAPIService requestWithMethod:@"PUT" urlString:ASPIREAPI_PROFILE_URL data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
#if DEBUG
            NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
            if(completion) {
                completion(error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
#if DEBUG
            if (SHOULD_LOG) {
                printf("\n==== response update user ====\n%s\n=============\n",[[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] UTF8String]);
            }
#endif
            if (err == nil) {
                if(completion) {
                    completion([AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(error);
                }
            }
        }
    }];
}
    
+ (void)createRequestFromAccessToken:(NSString *)token tokenType:(NSString *)tokenType withUserInfo:(NSDictionary *)userInfo completion:(void (^)(id _Nullable))completion {
    
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion([NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    #if DEBUG
    if (SHOULD_LOG) {
        printf("\n====POST create request ====\n%s\n=============\n",[[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:userInfo options:(NSJSONWritingPrettyPrinted) error:nil] encoding:NSUTF8StringEncoding] UTF8String]);
    }
#endif
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:userInfo options:0 error:nil];

    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json",
                               @"X-App-Id": [AspireAPIService getXAppId],
                               @"X-Organization": [AspireAPIService getClient]
                               };
    
    [AspireAPIService requestWithMethod:@"POST" urlString:ASPIREAPI_REQUEST_URL data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
#if DEBUG
            NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
            if(completion) {
                completion(error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
#if DEBUG
            if (SHOULD_LOG) {
                printf("\n==== response create request ====\n%s\n=============\n",[[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] UTF8String]);
            }
#endif
            
            if (err == nil) {
                if(completion) {
                    completion([AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(error);
                }
            }
        }
    }];
}

+ (void)updateRequestFromAccessToken:(NSString *)token tokenType:(NSString *)tokenType withUserInfo:(NSDictionary *)userInfo completion:(void (^)(id _Nullable))completion {
    
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion([NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
#if DEBUG
    if (SHOULD_LOG) {
        printf("\n====PUT update request ====\n%s\n=============\n",[[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:userInfo options:(NSJSONWritingPrettyPrinted) error:nil] encoding:NSUTF8StringEncoding] UTF8String]);
    }
#endif
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:userInfo options:0 error:nil];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json"};
    
    [AspireAPIService requestWithMethod:@"PUT" urlString:ASPIREAPI_REQUEST_URL data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
#if DEBUG
            NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
            if(completion) {
                completion(error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
#if DEBUG
            if (SHOULD_LOG) {
                printf("\n==== response update request ====\n%s\n=============\n",[[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] UTF8String]);
            }
#endif
            
            if (err == nil) {
                if(completion) {
                    completion([AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(error);
                }
            }
        }
    }];
}

+ (void)deleteRequestFromAccessToken:(NSString *)token tokenType:(NSString *)tokenType withUserInfo:(NSDictionary *)userInfo completion:(void (^)(id _Nullable))completion {
    
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion([NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
#if DEBUG
    if (SHOULD_LOG) {
        printf("\n====DELETE request ====\n%s\n=============\n",[[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:userInfo options:(NSJSONWritingPrettyPrinted) error:nil] encoding:NSUTF8StringEncoding] UTF8String]);
    }
#endif
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:userInfo options:0 error:nil];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json"};
    
    [AspireAPIService requestWithMethod:@"DELETE" urlString:ASPIREAPI_REQUEST_URL data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
#if DEBUG
            NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
            if(completion) {
                completion(error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
#if DEBUG
            if (SHOULD_LOG) {
                printf("\n==== response delete request ====\n%s\n=============\n",[[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] UTF8String]);
            }
#endif
            
            if (err == nil) {
                if(completion) {
                    completion([AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(error);
                }
            }
        }
    }];
}

+ (void)changePasswordWithToken:(NSString *)token tokenType:(NSString*)tokenType email:(NSString*)email userInfo:(NSDictionary *)userinfo completion:(void (^)(id _Nullable)) completion {
    
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion([NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    #if DEBUG
    if (SHOULD_LOG) {
        printf("\n====POST change password ====\n%s\n=============\n",[[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:userinfo options:(NSJSONWritingPrettyPrinted) error:nil] encoding:NSUTF8StringEncoding] UTF8String]);
    }
#endif
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:userinfo options:0 error:nil];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json"};
    
    
    [AspireAPIService requestWithMethod:@"POST" urlString:[ASPIREAPI_CHANGE_PASSWORD_URL stringByReplacingOccurrencesOfString:@"[@email]" withString:email] data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
#if DEBUG
            NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
            if(completion) {
                completion(error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
#if DEBUG
            if (SHOULD_LOG) {
                printf("\n==== response change password ====\n%s\n=============\n",[[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] UTF8String]);
            }
#endif
            if (err == nil) {
                if(completion) {
                    completion([AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(error);
                }
            }
        }
    }];
}
    
+ (void)resetPasswordWithToken:(NSString *)token tokenType:(NSString *)tokenType email:(NSString *)email completion:(void (^)(id _Nullable))completion {
    
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion([NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json"};
    #if DEBUG
    if (SHOULD_LOG) {
        printf("\n====POST reset password ====\n%s\n=============\n",[[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:headers options:(NSJSONWritingPrettyPrinted) error:nil] encoding:NSUTF8StringEncoding] UTF8String]);
    }
#endif
    
    [AspireAPIService requestWithMethod:@"POST" urlString:[ASPIREAPI_RESET_PASSWORD_URL stringByReplacingOccurrencesOfString:@"[@email]" withString:email] data:nil header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
#if DEBUG
            NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
            if(completion) {
                completion(error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
#if DEBUG
            if (SHOULD_LOG) {
                printf("\n==== response reset password ====\n%s\n=============\n",[[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] UTF8String]);
            }
#endif
            
            if (err == nil) {
                if(completion) {
                    completion([AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(error);
                }
            }
        }
    }];
}

+ (void)forgotPasswordWithUrl:(NSString*)url method:(NSString*)method token:(NSString *)token tokenType:(NSString *)tokenType email:(NSString *)email params:(NSDictionary *)params completion:(void (^)(id _Nullable))completion {
    
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion([NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    if(url == nil) {
        url = [ASPIREAPI_FORGOT_PASSWORD_URL stringByReplacingOccurrencesOfString:@"[@email]" withString:email];
    }
    if (method == nil) {
        method = @"POST";
    }
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json"};
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
    
#if DEBUG
    if (SHOULD_LOG) {
        printf("\n====POST forgot password: %s ====\n%s\n=============\n", [url UTF8String], [[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:params options:(NSJSONWritingPrettyPrinted) error:nil] encoding:NSUTF8StringEncoding] UTF8String]);
    }
#endif
    
    [AspireAPIService requestWithMethod:method urlString:url data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
#if DEBUG
            NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
            if(completion) {
                completion(error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
#if DEBUG
            if (SHOULD_LOG) {
                printf("\n==== response forgot password ====\n%s\n=============\n",[[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] UTF8String]);
            }
#endif
            
            if (err == nil) {
                if(completion) {
                    completion([AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(error);
                }
            }
        }
    }];
}

+ (void)verifyBinWithToken:(NSString *)token tokenType:(NSString *)tokenType bin:(NSInteger)bin completion:(void (^)(BOOL, id _Nullable))completion {
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion(false,[NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json",
                               @"X-App-Id": [AspireAPIService getXAppId],
                               @"X-Organization": [AspireAPIService getClient]
                               };
    
    NSDictionary *parameters = @{ @"verificationMetadata": @{ @"bin": @(bin) } };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
#if DEBUG
    if (SHOULD_LOG) {
        printf("\n====POST verify bin ====\nHeader: %s\ndata %s\n=============\n",[[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:headers options:(NSJSONWritingPrettyPrinted) error:nil] encoding:NSUTF8StringEncoding] UTF8String],[[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:parameters options:(NSJSONWritingPrettyPrinted) error:nil] encoding:NSUTF8StringEncoding] UTF8String]);
    }
#endif
    
    [AspireAPIService requestWithMethod:@"POST" urlString:ASPIREAPI_VERIFY_BIN_URL data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
#if DEBUG
            NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
            if(completion) {
                completion(false,error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
#if DEBUG
            if (SHOULD_LOG) {
                printf("\n==== response verify bin ====\n%s\n=============\n",[[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] UTF8String]);
            }
#endif
            
            if (err == nil) {
                if(completion) {
                    BOOL isSuccess = false;
                    if ([[json allKeys] containsObject:@"result"]) {
                        isSuccess = [[json objectForKey:@"result"] isEqualToString:@"success"];
                    }
                    completion(isSuccess,nil);
                }
            } else {
                if(completion) {
                    completion(false,[AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            }
        }
    }];
}

+ (void) getProfileWithToken:(NSString *)token tokenType:(NSString *)tokenType email:(NSString *)email completion:(void (^)(id _Nullable,id _Nullable))completion {
    
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion(nil,[NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json"};
#if DEBUG
    if (SHOULD_LOG) {
        printf("\n====GET profile OKTA ====\n%s\n=============\n",[[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:headers options:(NSJSONWritingPrettyPrinted) error:nil] encoding:NSUTF8StringEncoding] UTF8String]);
    }
#endif
    
    [AspireAPIService requestWithMethod:@"GET" urlString:[ASPIREAPI_PROFILE_OKTA_URL stringByReplacingOccurrencesOfString:@"[@email]" withString:email] data:nil header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
#if DEBUG
            NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
            if(completion) {
                completion(nil,error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
#if DEBUG
            if (SHOULD_LOG) {
                printf("\n==== response retreive profile OKTA ====\n%s\n=============\n",[[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] UTF8String]);
            }
#endif
            
            if (err == nil) {
                if(completion) {
                    completion(json,[AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(nil,error);
                }
            }
        }
    }];
}

+ (void)changeRecoveryQuestionWithUrl:(NSString*)url method:(NSString*)method token:(NSString *)token tokenType:(NSString *)tokenType withOKTAId:(NSString *)oktaId params:(NSDictionary *)params completion:(void (^)(id _Nullable, id _Nullable))completion {
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion(nil,[NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    if(url == nil) {
        url = [ASPIREAPI_CHANGE_RECOVERY stringByReplacingOccurrencesOfString:@"[@id]" withString:oktaId];
    }
    if (method == nil) {
        method = @"POST";
    }
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/json"};
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
    
#if DEBUG
    if (SHOULD_LOG) {
        printf("\n====POST change recovery question OKTA: %s ====\n%s\n=============\n", [url UTF8String], [[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:params options:(NSJSONWritingPrettyPrinted) error:nil] encoding:NSUTF8StringEncoding] UTF8String]);
    }
#endif
    
    [AspireAPIService requestWithMethod:method urlString:url data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
#if DEBUG
            NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
            if(completion) {
                completion(nil,error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
#if DEBUG
            if (SHOULD_LOG) {
                printf("\n==== response change recovery question OKTA ====\n%s\n=============\n",[[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] UTF8String]);
            }
#endif
            
            if (err == nil) {
                if(completion) {
                    completion(json,[AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(nil,error);
                }
            }
        }
    }];
}

#pragma mark - PMA
+ (void)requestPMATokenWithUsername:(NSString *)username password:(NSString *)password completion:(void (^)(id _Nullable, id _Nullable))completion {
    
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion(nil,[NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    if ([username componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].count == 0 ||
        [password componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].count == 0) {
        if (completion)
            completion(nil,[NSError errorWithDomain:@"com.error" code:-1011 userInfo:@{@"message":@"username or password is invalid"}]);
        return;
    }
    
    NSMutableData *postData = [[NSMutableData alloc] initWithData:[@"grant_type=password" dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"&scope=openid" dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[[NSString stringWithFormat:@"&username=%@",username] dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[[NSString stringWithFormat:@"&password=%@",password] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", PMA_AUTH_ACC, PMA_AUTH_PAS];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString* endcode = [authData base64EncodedStringWithOptions:0];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", endcode];
    
    NSDictionary *headers = @{ @"Authorization": authValue,
                               @"Content-Type": @"application/x-www-form-urlencoded"};
    
#if DEBUG
    if (SHOULD_LOG) {
        printf("\n====POST request accesstoken PMA ====\n%s\n=============\n",[[[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding] UTF8String]);
    }
#endif
    
    [AspireAPIService requestWithMethod:@"POST" urlString:PMA_GET_TOKEN_URL data:postData header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
#if DEBUG
            NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
            if(completion) {
                completion(nil,error);
            }
        } else {
            NSError* err;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
#if DEBUG
            if (SHOULD_LOG) {
                printf("\n==== response accesstoken PMA ====\n%s\n=============\n",[[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] UTF8String]);
            }
#endif
            if (err == nil) {
                if(completion) {
                    completion(json,[AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(nil,error);
                }
            }
        }
    }];
}

+ (void)findPartyPMAWithToken:(NSString *)token tokenType:(NSString *)tokenType email:(NSString *)email completion:(void (^)(id _Nullable, id _Nullable))completion {
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion(nil,[NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSDictionary *headers = @{ @"Authorization": authValue};
#if DEBUG
    if (SHOULD_LOG) {
        printf("\n====GET find party PMA ====\n%s\n=============\n",[[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:headers options:(NSJSONWritingPrettyPrinted) error:nil] encoding:NSUTF8StringEncoding] UTF8String]);
    }
#endif
    
    [AspireAPIService requestWithMethod:@"GET" urlString:[[PMA_SEARCH_PARTY_PERSON_URL stringByReplacingOccurrencesOfString:@"[@clientID]" withString:[AspireAPIService getClient]] stringByAppendingFormat:@"emailAddress=%@",email] data:nil header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
#if DEBUG
            NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
            if(completion) {
                completion(nil,error);
            }
        } else {
            NSError* err;
            id json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
#if DEBUG
            if (SHOULD_LOG) {
                printf("\n==== response find party PMA ====\n%s\n=============\n",[[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] UTF8String]);
            }
#endif
            
            if (err == nil) {
                if(completion) {
                    completion(json,[AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(nil,error);
                }
            }
        }
    }];
}

+ (void)getPartyPMAWithToken:(NSString *)token tokenType:(NSString *)tokenType partyId:(NSString *)partyId completion:(void (^)(id _Nullable, id _Nullable))completion {
    if ([ModelAspireApiManager shared].checkingNetwork) {
        if (![ModelAspireApiManager shared].checkingNetwork()) {
            if (completion) {
                completion(nil,[NSError errorWithDomain:@"com.s3corp.error" code:ERR_NETWORK userInfo:@{@"message":@"Network unavailable"}]);
                return;
            }
        }
    }
    
    NSString *authValue = [NSString stringWithFormat:@"%@ %@", tokenType, token];
    
    NSString* url = [[PMA_GET_PARTY_PERSON stringByReplacingOccurrencesOfString:@"[@partyID]" withString:partyId] stringByReplacingOccurrencesOfString:@"[@clientID]" withString:[AspireAPIService getClient]];
    
    NSDictionary *headers = @{ @"Authorization": authValue};
#if DEBUG
    if (SHOULD_LOG) {
        printf("\n====GET get profile party PMA ====\nheaders: %s \nurl: %s\n=============\n",[[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:headers options:(NSJSONWritingPrettyPrinted) error:nil] encoding:NSUTF8StringEncoding] UTF8String], [url UTF8String]);
    }
#endif
    
    [AspireAPIService requestWithMethod:@"GET" urlString:url data:nil header:headers completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
#if DEBUG
            NSLog(@"%s => %@",__PRETTY_FUNCTION__,error.description);
#endif
            if(completion) {
                completion(nil,error);
            }
        } else {
            NSError* err;
            id json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&err];
#if DEBUG
            if (SHOULD_LOG) {
                printf("\n==== response get profile party PMA ====\n%s\n=============\n",[[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] UTF8String]);
            }
#endif
            
            if (err == nil) {
                if(completion) {
                    completion(json,[AspireAPIService isJsonError:json] ? [NSError errorWithDomain:@"com.s3corp.error" code:-1011 userInfo:json] : nil);
                }
            } else {
                if(completion) {
                    completion(nil,error);
                }
            }
        }
    }];
}

#pragma mark - Private
+ (void) requestWithMethod:(NSString*)method urlString:(NSString*) urlString data:(nullable NSData*)data header:(NSDictionary*) headers completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error)) completion {

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    
    [request setHTTPMethod:method];
    [request setAllHTTPHeaderFields:headers];
    
    if (data != nil) {
         [request setHTTPBody:data];
    }
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (completion)
                                                        completion(data,response,error);
                                                }];
    [dataTask resume];
}

+ (void) requestNotCacheWithMethod:(NSString*)method urlString:(NSString*) urlString data:(nullable NSData*)data header:(NSDictionary*) headers completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error)) completion {
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:30.0];
    
    [request setHTTPMethod:method];
    [request setAllHTTPHeaderFields:headers];
    
    if (data != nil) {
        [request setHTTPBody:data];
    }
    NSURLSessionConfiguration* sessionConfig = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (completion)
                                                        completion(data,response,error);
                                                }];
    [dataTask resume];
}

+ (BOOL) isJsonError:(id)json {
    if ([json isKindOfClass:[NSArray class]]) return false;
    return ([[json allKeys] containsObject:@"error"] ||
            [[json allKeys] containsObject:@"errorCode"]);
}

#define FILE_SERVICE_OKTA  @"data2"
+ (NSString*) getClient {
    
    id dictTemp = [AspireAPIValidate getDataFromFileName:FILE_SERVICE_OKTA];
    if (![dictTemp isKindOfClass:[NSDictionary class]]) {return @"";}
    
    NSDictionary* dict = (NSDictionary*)dictTemp;
    if([[dict allKeys] containsObject:@"client"]) {
        return [dict valueForKey:@"client"];
    }
    
    return @"";
}

+ (NSString*) getXAppId {
    
    id dictTemp = [AspireAPIValidate getDataFromFileName:FILE_SERVICE_OKTA];
    if (![dictTemp isKindOfClass:[NSDictionary class]]) {return @"";}
    
    NSDictionary* dict = (NSDictionary*)dictTemp;
    if([[dict allKeys] containsObject:@"xAppId"]) {
        return [dict valueForKey:@"xAppId"];
    }
    
    return @"";
}
@end
