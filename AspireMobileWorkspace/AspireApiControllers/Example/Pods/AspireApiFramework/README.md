# AspireApiFramework

[![CI Status](https://img.shields.io/travis/vuongtrancong/AspireApiFramework.svg?style=flat)](https://travis-ci.org/vuongtrancong/AspireApiFramework)
[![Version](https://img.shields.io/cocoapods/v/AspireApiFramework.svg?style=flat)](https://cocoapods.org/pods/AspireApiFramework)
[![License](https://img.shields.io/cocoapods/l/AspireApiFramework.svg?style=flat)](https://cocoapods.org/pods/AspireApiFramework)
[![Platform](https://img.shields.io/cocoapods/p/AspireApiFramework.svg?style=flat)](https://cocoapods.org/pods/AspireApiFramework)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## How to
### Release new version 

1. Go to root folder
2. execute below command
```
./tag.sh <version> <release message>
```
Sample
```
./tag.sh 0.1.14 "validate email as username"

## Installation

AspireApiFramework is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'AspireApiFramework'
```

## Author

vuongtrancong, vuong.tran@s3corp.com.vn

## License

AspireApiFramework is available under the MIT license. See the LICENSE file for more info.
