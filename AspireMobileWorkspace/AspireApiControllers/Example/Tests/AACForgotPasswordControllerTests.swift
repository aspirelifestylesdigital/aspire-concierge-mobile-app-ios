//
//  AACForgotPasswordControllerTests.swift
//  AspireApiControllers_Example
//
//  Created by VuongTC on 10/29/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import XCTest
import AspireApiControllers
import AspireApiFramework

let aspire_api_service_username = "mcd-qa@aspire.org"
let aspire_api_service_password = "P@ssw0rd12345"
let aspire_api_program          = "MASTERCARD"

class AACForgotPasswordControllerTests: XCTestCase {
    let view = AACForgotPasswordController()
    let question = AAFRecoveryQuestion(question: "Who is your favorite book/movie character?", answer: "dada")

    override func setUp() {
        super.setUp()
        ModelAspireApiManager.registerServiceUsername(aspire_api_service_username, password: aspire_api_service_password, program: aspire_api_program, bin: "")
        
        ModelAspireApiManager.registerClient("MCD", xAppId: "3342aa59fd004973a6773d682a029f2a")
    }
    
    func testValidatePassNotAtLocal() {
        let password = "123"
        
        view.receiveInput(input: "emailtest@test.com", type: .email)
        view.receiveInput(input: "answer", type: .answer)
        view.receiveInput(input: password, type: .password)
        view.receiveInput(input: password, type: .confirmPassword)
        view.shouldCheckStrongPassword = true
        let waitExpectation = expectation(description: "Waiting")

        view.verifyData(useLocalPasswordValidation: false) { (errors) in
            XCTAssert(errors != nil && errors!.count == 0)
            waitExpectation.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func testValidateWithEmail() {
        let password = "Vuong1234!@#$"
        
        view.receiveInput(input: "vuong@test.com", type: .email)
        view.receiveInput(input: "answer", type: .answer)
        view.receiveInput(input: password, type: .password)
        view.receiveInput(input: password, type: .confirmPassword)
        view.shouldCheckStrongPassword = true
        let waitExpectation = expectation(description: "Waiting")
        
        view.verifyData(useLocalPasswordValidation: true) { (errors) in
            XCTAssert(errors != nil && errors!.contains(FPErrorInputType.passwordContainName.rawValue))
            waitExpectation.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func testValidateWithEmailAndWeakPassword() {
        let password = "Vuong12"
        
        view.receiveInput(input: "vuong@test.com", type: .email)
        view.receiveInput(input: "answer", type: .answer)
        view.receiveInput(input: password, type: .password)
        view.receiveInput(input: password, type: .confirmPassword)
        view.shouldCheckStrongPassword = true
        let waitExpectation = expectation(description: "Waiting")
        
        view.verifyData(useLocalPasswordValidation: true) { (errors) in
            XCTAssert(errors != nil && errors!.contains(FPErrorInputType.passwordContainName.rawValue))
            waitExpectation.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func testForgotPasswordSubmission() {
        //Test happy case
        let password = AAFPassword(password: "Ee1234!@#$")
        let waitExpectation = expectation(description: "Waiting")
        view.submitForgetPassword(email: "tcvuong89@gmail.com", question: question, newPassword: password) { (isSuccess, message, errorType) in
            XCTAssert(isSuccess == true)
            XCTAssert(message == "Your password has been successfully updated!")
            waitExpectation.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func testForgotPasswordSubmissionInvalidEmail() {
        //Test happy case
        let password = AAFPassword(password: "Ff1234!@#$")
        let waitExpectation = expectation(description: "Waiting")
        view.submitForgetPassword(email: "invalidemail@invalid.com", question: question, newPassword: password) { (isSuccess, message, errorType) in
            XCTAssert(isSuccess == false)
            XCTAssert(message == "A user account with this email address cannot be found. Would you like to try again?")
            waitExpectation.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func testForgotPasswordSubmissionWeakPassword() {
        //Test happy case
        let password = AAFPassword(password: "weakpassword")
        let waitExpectation = expectation(description: "Waiting")
        view.submitForgetPassword(email: "tcvuong89@gmail.com", question: question, newPassword: password) { (isSuccess, message, errorType) in
            XCTAssert(isSuccess == false)
            XCTAssert(message == "Password requirements were not met. Password must be at least 10 characters and contain at least 1 upper case, 1 lower case, 1 number and 1 special character !@#$%^&*. It cannot contain any part of your user name or be one of your last 12 passwords.")
            waitExpectation.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
}
