//
//  NameValidationTests.swift
//  AspireApiControllers_Example
//
//  Created by VuongTC on 11/1/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import XCTest
import AspireApiControllers

class NameValidationTests: XCTestCase {
    
    func testValidateName() {
        print("All below should be true")
        var name = "Vuong Tran"
        var result = NameValidation.validateSpecialChars(name: name)
        XCTAssert(result == true)

        name = "Vu"
        result = NameValidation.validateLength(name: name)
        XCTAssert(result == true)
        
        name = "1234567890123456789012345"
        result = NameValidation.validateLength(name: name)
        XCTAssert(result == true)
        
        name = "Vuong-Tran"
        result = NameValidation.validateSpecialChars(name: name)
        XCTAssert(result == true)
        
        name = "Vuong'Tran"
        result = NameValidation.validateSpecialChars(name: name)
        XCTAssert(result == true)
        
        name = "Vuong`Tran"
        result = NameValidation.validateSpecialChars(name: name)
        XCTAssert(result == true)
        
        name = "Vuong’Tran"
        result = NameValidation.validateSpecialChars(name: name)
        XCTAssert(result == true)
        
        print("All below should be false")
        
        name = "V"
        result = NameValidation.validateLength(name: name)
        XCTAssert(result == false)
        
        name = "12345678901234567890123456"
        result = NameValidation.validateLength(name: name)
        XCTAssert(result == false)
        
        name = "Vuong.Tran"
        result = NameValidation.validateSpecialChars(name: name)
        XCTAssert(result == false)
    }
    
    func testValidateFirstName() {
        var name = "Vuong Tran"
        var result = NameValidation.validateFirstName(name: name)
        XCTAssert(result == nil)
        
        name = "Vuong.Tran"
        result = NameValidation.validateFirstName(name: name)
        XCTAssert(result == "Please enter a valid first name. Acceptable special characters are - , \' and space.")

        name = "V"
        result = NameValidation.validateFirstName(name: name)
        XCTAssert(result == "A valid first name must be at least 2 characters long.")
    }
    
    func testValidateLastName() {
        var name = "Vuong Tran"
        var result = NameValidation.validateLastName(name: name)
        XCTAssert(result == nil)
        
        name = "Vuong.Tran"
        result = NameValidation.validateLastName(name: name)
        XCTAssert(result == "Please enter a valid last name. Acceptable special characters are - , \' and space.")
        
        name = "V"
        result = NameValidation.validateLastName(name: name)
        XCTAssert(result == "A valid last name must be at least 2 characters long.")
    }
}
