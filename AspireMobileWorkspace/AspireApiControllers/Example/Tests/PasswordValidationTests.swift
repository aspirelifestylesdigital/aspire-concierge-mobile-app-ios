//
//  PasswordValidationTests.swift
//  AspireApiControllers_Example
//
//  Created by VuongTC on 10/29/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import XCTest
import AspireApiControllers

class PasswordValidationTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    func testNormalRule() {
        let password = "Zz1234!@#$"
        let confirmPassword = password
        
        let result = PasswordValidation.validate(password: password, confirmPassword: confirmPassword, checkStrongPassword: false, notContainNames: [])
        XCTAssert(result.count == 0, "Pass")
    }
    
    func testAtleast10Chars() {
        var password = "Zz1234!@#$"
        var confirmPassword = password
        
        var result = PasswordValidation.validate(password: password, confirmPassword: confirmPassword, checkStrongPassword: false, notContainNames: [])
        XCTAssert(result.count == 0, "Pass")
        
        password = "Zz1234!@#"
        confirmPassword = password
        result = PasswordValidation.validate(password: password, confirmPassword: confirmPassword, checkStrongPassword: true, notContainNames: [])
        XCTAssert(result.contains(FPErrorInputType.password.rawValue), "Password must be at least 10 chars")

        password = "Minhu@123456"
        let names = ["Nhu", "Nguyen"]
        confirmPassword = password
        result = PasswordValidation.validate(password: password, confirmPassword: confirmPassword, checkStrongPassword: true, notContainNames: names)
        XCTAssert(result.contains(FPErrorInputType.passwordContainName.rawValue), "Password must not be contains name")
    }
    
    
    func testContainNames() {
        var password = "Minhu@123456"
        var names = ["Nhu", "Nguyen"]
        var confirmPassword = password
        var result = PasswordValidation.validate(password: password, confirmPassword: confirmPassword, checkStrongPassword: true, notContainNames: names)
        XCTAssert(result.contains(FPErrorInputType.passwordContainName.rawValue), "Password must not be contains name")
        
        password = "Minhu@123456"
        names = [" ", "Nguyen"]
        confirmPassword = password
        result = PasswordValidation.validate(password: password, confirmPassword: confirmPassword, checkStrongPassword: true, notContainNames: names)
        XCTAssert(result.count == 0)
        
        password = "Minhu@123456"
        names = ["", "Nguyen"]
        confirmPassword = password
        result = PasswordValidation.validate(password: password, confirmPassword: confirmPassword, checkStrongPassword: true, notContainNames: names)
        XCTAssert(result.count == 0)

        password = "Minhu@123456"
        names = ["M", "Nguyen"]
        confirmPassword = password
        result = PasswordValidation.validate(password: password, confirmPassword: confirmPassword, checkStrongPassword: true, notContainNames: names)
        XCTAssert(result.contains(FPErrorInputType.passwordContainName.rawValue), "Password must not be contains name")
    }
    
    func testWeakPassword() {
        let password = "DeepMS@1"
        let confirmPassword = password
        let result = PasswordValidation.validate(password: password, confirmPassword: confirmPassword, checkStrongPassword: true, notContainNames: [])
        XCTAssert(result.contains(FPErrorInputType.password.rawValue), "Password must be stronger")
    }
    
}
