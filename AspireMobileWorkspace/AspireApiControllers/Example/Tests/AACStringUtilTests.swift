//
//  AACStringUtilTests.swift
//  AspireApiControllers_Tests
//
//  Created by VuongTC on 11/7/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import XCTest
import AspireApiControllers

class AACStringUtilTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testValidEmail() {
        var email: String = "tcvuong89@gmail.com"
        var isValid = email.isValidEmail()
        XCTAssert(isValid == true)
        
        email = "thanh123@gm.com.vn"
        isValid = email.isValidEmail()
        XCTAssert(isValid == true)
    }

}
