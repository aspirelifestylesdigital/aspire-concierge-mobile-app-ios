import UIKit

//let regex = "^[a-zA-Z0-9 ’`'\\-]{2,25}"
let regex = "^[a-zA-Z0-9 ’`'\\-]+$"
let test = NSPredicate(format:"SELF MATCHES %@", regex)

print("All below should be true")

var str = "VuongTran"
print( test.evaluate(with: str))

str = "Vuong Tran"
print( test.evaluate(with: str))

str = "Vu"
print( test.evaluate(with: str))

str = "1234567890123456789012345"
print( test.evaluate(with: str))

str = "Vuong-Tran"
print( test.evaluate(with: str))

str = "Vuong'Tran"
print( test.evaluate(with: str))

str = "Vuong`Tran"
print( test.evaluate(with: str))

str = "Vuong’Tran"
print( test.evaluate(with: str))

print("All below should be false")

str = "Vuong.Tran"
print( test.evaluate(with: str))

str = "Vuong+Tran"
print( test.evaluate(with: str))

let regexLength = "^[a-z]{2,25}$"
let testLength = NSPredicate(format:"SELF MATCHES %@", regexLength)

str = "V"
print(testLength.evaluate(with: str))

str = "12345678901234567890123456"
print( testLength.evaluate(with: str))

