//
//  ViewController.swift
//  AspireApiControllers
//
//  Created by vuongtrancong on 10/26/2018.
//  Copyright (c) 2018 vuongtrancong. All rights reserved.
//

import UIKit
import AspireApiControllers
import AspireApiFramework

class ViewController: AACBaseController {

    override func viewDidLoad() {
        super.viewDidLoad()

        var password = "Minhu@123456"
        let names = ["Nhu", "Nguyen"]
        let confirmPassword = password
        let result = PasswordValidation.validate(password: password, confirmPassword: confirmPassword, checkStrongPassword: true, notContainNames: names)
        print("\(result)")
        
//        let test = FPErrorInputType.passwordContainName
//        switch test {
//        case .password:
//            print("\(test)")
//        case .passwordContainName:
//            print("\(test) 123")
//        default:
//            print("\(test) 1234")
//        }
        
        
//        password = "Vuong1234!@#$"
//        
//        let view = AACForgotPasswordController()
//        view.receiveInput(input: "vuong@test.com", type: .email)
//        view.receiveInput(input: "answer", type: .answer)
//        view.receiveInput(input: password, type: .password)
//        view.receiveInput(input: password, type: .confirmPassword)
//        view.shouldCheckStrongPassword = true
//        
//        view.verifyData(useLocalPasswordValidation: true) { (errors) in
//            print("\(String(describing: errors))")
//        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

