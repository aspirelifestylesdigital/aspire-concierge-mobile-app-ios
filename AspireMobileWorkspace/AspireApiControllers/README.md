# AspireApiControllers

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

AspireApiControllers is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'AspireApiControllers'
```

## How to
### Release new version 

1. Go to root folder
2. execute below command
```
./tag.sh <version> <release message>
```
Sample
```
./tag.sh 0.1.14 "validate email as username"
```

## Author

vuongtrancong, vuong.tran@s3corp.com.vn

## License

AspireApiControllers is available under the MIT license. See the LICENSE file for more info.
