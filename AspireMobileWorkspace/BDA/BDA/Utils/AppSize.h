//
//  AppSize.h
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 10/19/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppSize : NSObject

+(CGFloat) titleGreetingSize;
+(CGFloat) largeTitleTextSize;
+(CGFloat) letter26px;
+(CGFloat) largeDescriptionTextSize;
+(CGFloat) descriptionGreetingSize;
+(CGFloat) titleTextSize;
+(CGFloat) descriptionTextSize;
+(CGFloat) headerTextSize;
+(CGFloat) moreInfoTextSize;
+(CGFloat) titleLetterSpacing;
+(CGFloat) descriptionLetterSpacing;
+(CGFloat) smallLetterSpacing;
+(CGFloat) titleLineSpacing;

@end
