//
//  ImageUtilities.m
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/14/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "ImageUtilities.h"
#import <QuartzCore/QuartzCore.h>

@implementation ImageUtilities

+(void) setImageViewCircularFrame:(UIImageView *)imageView
{
    imageView.layer.cornerRadius = imageView.frame.size.width/2.0f;
    imageView.clipsToBounds = YES;
}

+ (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}
@end
