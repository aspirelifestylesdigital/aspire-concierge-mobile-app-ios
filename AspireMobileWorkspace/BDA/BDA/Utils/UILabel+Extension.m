//
//  UILabel+Extension.m
//  MobileConcierge
//
//  Created by Home on 5/7/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "UILabel+Extension.h"
#import "Constant.h"

@implementation UILabel (Extension)

-(void)configureDecorationForLabel
{
    [self setTextColor:[UIColor whiteColor]];
}

-(void)configureDecorationForGrayLabel
{
    [self setTextColor:[UIColor colorWithRed:153.0f/255.0f green:161.0f/255.0f blue:168.0f/255.0f alpha:1]];
}

-(void)setTextStyleForText:(NSString *)text WithFontName:(NSString *)fontName WithColor:(UIColor *)color WithTextSize:(float)textSize WithTextCenter:(BOOL)isCenter WithSpacing:(float)spacing;
{
    NSRange range = NSMakeRange(0, text.length);
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:text];
    [attributeString addAttribute:NSKernAttributeName value:@(spacing) range:range];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont fontWithName:fontName size:textSize*SCREEN_SCALE] range:range];
    [attributeString addAttribute:NSForegroundColorAttributeName value:color range:range];
    
    if(isCenter)
    {
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init] ;
        [paragraphStyle setAlignment:NSTextAlignmentCenter];
        [attributeString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    }
    
    self.attributedText = attributeString;
}

- (NSAttributedString*)handleHTMLString:(NSString*)str{
//    str = [self stringByStrippingHTMLWithString:str];
    str = [str stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%fpx; color:#011627; font-weight:normal;line-height: 22px;}</style>",self.font.fontName,self.font.pointSize]];
    
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [str dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                        NSFontAttributeName: [UIFont fontWithName:self.font.fontName size:self.font.pointSize]}
                                            documentAttributes: nil
                                            error: nil
                                            ];
    return attributedString;
}

- (NSAttributedString*)handleHTMLString:(NSString*)str witLineSpace:(CGFloat) lineSpacing{
    //    str = [self stringByStrippingHTMLWithString:str];
    if(lineSpacing == 0) {
        lineSpacing = 22;
    }
    str = [str stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%fpx; color:#011627; font-weight:normal;line-height: %fpx;}</style>",self.font.fontName,self.font.pointSize,lineSpacing]];
    
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [str dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                        NSFontAttributeName: [UIFont fontWithName:self.font.fontName size:self.font.pointSize]}
                                            documentAttributes: nil
                                            error: nil
                                            ];
    return attributedString;
}

- (void)setLineSpacing:(CGFloat)lineSpacing {
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init] ;
    [paragraphStyle setAlignment:self.textAlignment];
    [paragraphStyle setLineSpacing:lineSpacing];
    NSMutableAttributedString* attrib = [[NSMutableAttributedString alloc] initWithAttributedString:self.attributedText];
    [attrib addAttributes:@{NSParagraphStyleAttributeName:paragraphStyle} range:NSMakeRange(0, self.text.length)];
    self.attributedText = attrib;
}

//-(NSString *) stringByStrippingHTMLWithString:(NSString*)str {
//    NSRange r;
//    NSString *s = [str copy];
//    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
//        s = [s stringByReplacingCharactersInRange:r withString:@""];
//    return s;
//}

-(NSString *) stringByStrippingHTMLWithString:(NSString*)str {
    if ([str containsString:@"<b>"]) {
        str = [str stringByReplacingOccurrencesOfString:@"<b>" withString:@""];
        str = [str stringByReplacingOccurrencesOfString:@"</b>" withString:@""];
    }
    if ([str containsString:@"<i>"]) {
        str = [str stringByReplacingOccurrencesOfString:@"<i>" withString:@""];
        str = [str stringByReplacingOccurrencesOfString:@"</i>" withString:@""];
    }
    if ([str containsString:@"<u>"]) {
        str = [str stringByReplacingOccurrencesOfString:@"<u>" withString:@""];
        str = [str stringByReplacingOccurrencesOfString:@"</u>" withString:@""];
    }
    if ([str containsString:@"<h3>"]) {
        str = [str stringByReplacingOccurrencesOfString:@"<h3>" withString:@""];
        str = [str stringByReplacingOccurrencesOfString:@"</h3>" withString:@""];
    }

    
    return str;
}

- (void)setHeightOfLabel {
    UILabel* label = self;
    
    //get the height of label content
    CGRect textRect = [label.text boundingRectWithSize:label.bounds.size
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:@{NSFontAttributeName:label.font}
                                         context:nil];
    
    CGFloat height = textRect.size.height;

    //set the frame according to calculated height
    CGRect frame = label.frame;
    if([label.text length] > 0) {
        frame.size.height = height;
    }
    else {
        frame.size.height = 0;
    }
    label.frame = frame;
}


- (void)setWidthOfLabel {
    UILabel* label = self;
    
    //get the height of label content
    CGRect textRect = [label.text boundingRectWithSize:CGSizeMake(99999, label.bounds.size.height)
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:@{NSFontAttributeName:label.font}
                                               context:nil];
    CGFloat width = textRect.size.width;

    //set the frame according to calculated height
    CGRect frame = label.frame;
    if([label.text length] > 0) {
        
        frame.size.width = width+5;
    }
    else {
        frame.size.width = 0;
    }
    label.frame = frame;
}

- (void)setHeightOfLabelWithMaxHeight:(float)maxHeight {
    UILabel* label = self;
    
    //get the height of label content
    CGRect textRect = [label.text boundingRectWithSize:CGSizeMake(label.bounds.size.width, maxHeight)
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:@{NSFontAttributeName:label.font}
                                               context:nil];
    CGFloat height = textRect.size.height;

    //set the frame according to calculated height
    CGRect frame = label.frame;
    
    if([label.text length] > 0) {
        if (height > maxHeight) {
            frame.size.height = maxHeight;
        }
        else {
            frame.size.height = height;
        }
        
    }
    else {
        frame.size.height = 0;
    }
    label.frame = frame;
}

- (void)setWidthOfLabelWithMaxWidth:(float)maxWidth  {
    UILabel* label = self;
    
    //get the height of label content
    CGRect textRect = [label.text boundingRectWithSize:CGSizeMake(99999, label.bounds.size.height)
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:@{NSFontAttributeName:label.font}
                                               context:nil];
    CGFloat width = textRect.size.width;

    //set the frame according to calculated height
    CGRect frame = label.frame;
    if([label.text length] > 0) {
        
        if (width > maxWidth) {
            frame.size.width = maxWidth;
        }
        else {
            frame.size.width = width;
        }
    }
    else {
        frame.size.width = 0;
    }
    label.frame = frame;
}

@end
