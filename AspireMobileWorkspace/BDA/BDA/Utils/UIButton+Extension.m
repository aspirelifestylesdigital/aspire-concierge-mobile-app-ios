//
//  UIButton+Extension.m
//  MobileConcierge
//
//  Created by Home on 5/7/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "UIButton+Extension.h"
#import "Constant.h"
#import "Common.h"
#import "UtilStyle.h"

@implementation UIButton (Extension)

-(void)setBackgroundColorForNormalStatus
{
    UIGraphicsBeginImageContext(CGSizeMake(1.0f, 1.0f));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [AppColor normalButtonColor].CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 1));
    UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self setBackgroundImage:colorImage forState:UIControlStateNormal];
}

-(void) setBackgroundColorForTouchingStatus
{
    UIGraphicsBeginImageContext(CGSizeMake(1.0f, 1.0f));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [AppColor highlightButtonColor].CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 1));
    UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self setBackgroundImage:colorImage forState:UIControlStateHighlighted];
}

-(void) setBackgroundColorForSelectedStatus
{
    UIGraphicsBeginImageContext(CGSizeMake(1.0f, 1.0f));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [AppColor highlightButtonColor].CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 1));
    UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self setBackgroundImage:colorImage forState:UIControlStateSelected];
}


-(void) setBackgroundColorForDisableStatus
{
    UIGraphicsBeginImageContext(CGSizeMake(1.0f, 1.0f));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [AppColor disableButtonColor].CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 1));
    UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self setBackgroundImage:colorImage forState:UIControlStateDisabled];
}

-(void) setWhiteBackgroundColorForTouchingStatus
{
    UIGraphicsBeginImageContext(CGSizeMake(1.0f, 1.0f));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:0.2f].CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 1));
    UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self setBackgroundImage:colorImage forState:UIControlStateHighlighted];
}


-(void)configureDecorationForButton
{
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.titleLabel setFont:[UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize titleTextSize]]];
    
    NSDictionary *attributes = @{NSForegroundColorAttributeName: self.titleLabel.textColor,
                                 NSFontAttributeName: self.titleLabel.font,
                                 NSKernAttributeName: @1.4};
    self.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.titleLabel.text attributes:attributes];
}

-(void)configureDecorationForExploreButton
{
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.titleLabel setFont:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionTextSize]]];
}

-(void)configureDecorationBlackTitleButton
{
    [self setTitleColor:[AppColor backgroundColor] forState:UIControlStateNormal];
}

-(void)configureDecorationBlackTitleButtonForTouchingStatus
{
    [self setTitleColor:[AppColor highlightButtonColor] forState:UIControlStateHighlighted];
}

-(void)centerVertically
{
    CGFloat spacing = 3.0;
    
    // lower the text and push it left so it appears centered
    //  below the image
    CGSize imageSize = self.imageView.image.size;
    self.titleEdgeInsets = UIEdgeInsetsMake(0.0, - imageSize.width, - (imageSize.height + spacing), 0.0);
    
    // raise the image and push it right so it appears centered
    //  above the text
    CGSize titleSize = [self.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: self.titleLabel.font}];
    self.imageEdgeInsets = UIEdgeInsetsMake(- (titleSize.height + spacing), 0.0, 0.0, - titleSize.width);
    
    // increase the content height to avoid clipping
    CGFloat edgeOffset = fabs(titleSize.height - imageSize.height) / 2.0;
    self.contentEdgeInsets = UIEdgeInsetsMake(edgeOffset, 0.0, edgeOffset, 0.0);
    
}

-(void)setUnderlineForTextWithDefaultColorForNormalStatus:(NSString*)title
{
    NSMutableAttributedString *normalString = [[NSMutableAttributedString alloc] initWithString:title];
    
    [normalString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [normalString length])];
    [self setAttributedTitle:normalString forState:UIControlStateNormal];
}


-(void)setUnderlineForTextWithCustomizedColorForTouchingStatus:(NSString*)title
{
    NSMutableAttributedString *tappedString = [[NSMutableAttributedString alloc] initWithString:title];
    [tappedString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, tappedString.length)];
    [tappedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:157.0f/255.0f green:3.0f/255.0f blue:41.0f/255.0f alpha:1.0f] range:NSMakeRange(0, tappedString.length)];
    [self setAttributedTitle:tappedString forState:UIControlStateHighlighted];
}

-(void)setUnderlineForText:(NSString*)title withColor:(UIColor *)color withStatus:(UIControlState)status
{
    NSMutableAttributedString *tappedString = [[NSMutableAttributedString alloc] initWithString:title];
    [tappedString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, tappedString.length)];
    [tappedString addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0, tappedString.length)];
    [self setAttributedTitle:tappedString forState:status];
}

@end
