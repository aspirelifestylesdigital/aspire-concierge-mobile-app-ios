//
//  MCActivityItemSource.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/30/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "MCActivityItemSource.h"

@implementation MCActivityItemSource

- (id)activityViewControllerPlaceholderItem:(UIActivityViewController *)activityViewController
{
    return @"";
}
- (id)activityViewController:(UIActivityViewController *)activityViewController itemForActivityType:(NSString *)activityType
{
    return nil;
}

@end
