//
//  Constant.h
//  ALC
//
//  Created by Anh Tran on 8/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constant : NSObject

/**
 * More Information in URL
 **/
#define EncryptKey @"#concierge.uda.com#91@#B8%*6EBC8045E1-84B3-E171A0C026F4@"
#define PRIVACY_POLICY_URL @"https://www.aspirelifestyles.com/en/privacy-policy"
#define MASTERCARD_REGISTER_URL @"http://www.mastercard.com/sea/consumer/world_worldelite_mastercard.html"
#define MASTERCARD_TRAVEL_URL @"https://travel.mastercard.com/travel/arc.cfm"
#define PRICELESS_URL @"https://priceless.com"
#define GOLF_URL @"https://priceless.com/golf"
#define MASTERCARD_SHARING @"https://concierge.mastercard.com/sign_in_mc.aspx?utm_source=tw&utm_medium=mcusa&utm_campaign=concierge&popup"
#define APP_NAME @"BDA"
/**
 * URL Request
 **/
#define MCD_API_URL    @"https://apiservice-stg.aspirelifestyles.com/ConciergeOauthWebAPI/"

// Explorer
#define B2C_API_URL    @"https://tools.vipdesk.com/b2cwebservices.testing/"
#define B2C_IA_API_URL @"https://tools.vipdesk.com/b2cwebservices.testing/"
// Product API
//#define B2C_API_URL    @"https://tools.vipdesk.com/b2cwebservices/"
//#define B2C_IA_API_URL @"https://tools.vipdesk.com/b2cwebservices/"
#define B2C_REQUEST_URL @"https://api.vipdesk.com/services/"
#define B2C_REQUEST_API @"concierge/standard/v2.0/Requests/CreateNewConciergeCase"

// Client key
#define B2C_SUBDOMAIN  @"Aspire NA App Demo - Brazil"
#define B2C_PW         @"92516941-9669-4AC3-AFB2-3C493980CA8D"
#define LICENSE_KEY    @"16A22941-3F8A-4884-A4C4-46670C56BEA1"

//#define B2C_Callback_Url @"MCD:ietf:wg:oauth:2.0:oob"
//#define B2C_ConsumerKey @"MCD"
//#define B2C_ConsumerSecret @"MCD-20170C45-C95"
//#define B2C_DeviceId @"DeciveID10"

#define B2C_VERIFICATION_CODE @"Deepa.SHANKAR@internationalsos.com"
#define B2C_Callback_Url @"urn:ietf:wg:oauth:2.0:oob"
#define B2C_ConsumerKey @"DMA"
#define B2C_ConsumerSecret @"DMAConsumerSecret"
#define B2C_DeviceId @"DeviceID10"
#define B2C_MemberRefNo @"TestMemRef127"
#define B2C_EPCClientCode @"DMA"
#define B2C_EPCProgramCode @"DMA SINGAPORE"


#define DCR_SUBCRIPTION_KEY @"Ocp-Apim-Subscription-Key"
#define DCR_ITEM_SEARCH_API @"items/search"
#define DCR_ITEM_DETAILS_API @"items"


//define error code
#define B2C_UNAVAILABLE_USER        @"ENR68-2"
#define B2C_INVALID_ACCESS_TOKEN    @"ENR73-2"
#define B2C_INVALID_USER_EMAIL      @"ENR008-3"
#define B2C_INVALID_PHONE_NUMBER    @"ENR007-3"
#define B2C_LOGIN_FAIL    @"ENR70-1"


/**************************** Development API V1.0  ****************************/

//B2C API
#define GetRequestToken               @"OAuth/AuthoriseRequestToken"
#define GetAccessToken                @"OAuth/GetAccessToken"
#define RefreshAccessToken            @"OAuth/RefereshToken"
#define PostRegistration              @"ManageUsers/Registration"
#define PostUpdateRegistration        @"ManageUsers/UpdateRegistration"
#define PostUserLogin                 @"ManageUsers/Login"
#define GetUserDetailsUrl             @"ManageUsers/GetUserDetails"
#define PostForgotPasswordUrl         @"ManageUsers/ForgotPassword"
#define PostChangePasswordUrl         @"ManageUsers/ChangePassword"
#define UpdateRegistration            @"ManageUsers/UpdateRegistration"
#define CreateConciergeRequestUrl     @"ManageRequest/CreateConciergeRequest"
#define GetPreferences                @"ManageUsers/GetPreference"
#define AddPreferences                @"ManageUsers/AddPreference"
#define UpdatePerferences             @"ManageUsers/UpdatePreference"

#define GetQuestionsAndAnswers      @"instantanswers/GetQuestionsAndAnswers"
#define GetQuestionAndAnswers       @"instantanswers/GetQuestionAndAnswers"
#define GetSubCategories            @"instantanswers/GetSubCategories"
#define GetContentFulls             @"conciergecontent/GetContentFulls"
#define GetContentFull              @"conciergecontent/GetContentFull"
#define GetTiles                    @"conciergecontent/GetTiles"
#define GetUtility                  @"utility/GetClientCopy"
#define GetDataBasedOnSearchText    @"utility/SearchIAAndCCA"
#define checkBIN                    @"utility/CheckMasterCardBIN"
#define CheckPasscode               @"utility/CheckPasscode"

//end B2C

// config user authenticate
#define userAuthenticate @"admin"
#define passAuthenticate @"Admin123!@#$"
//

/**
 * Http Methods
 **/
#define kHTTPGET    @"GET"
#define kHTTPPUT    @"PUT"
#define kHTTPPOST   @"POST"
#define kHTTPDEL    @"DELETE"

/**
 * Error Code
 **/
#define kSTATUSOK               200
#define kSTATUSTOKENEXPIRE     401


/***
    /// Define Font
***/
FOUNDATION_EXPORT NSString *const FONT_MarkForMC_BOLD;
FOUNDATION_EXPORT NSString *const FONT_MarkForMC_MED;
FOUNDATION_EXPORT NSString *const FONT_MarkForMC_LIGHT;
FOUNDATION_EXPORT NSString *const FONT_MarkForMC_REGULAR;
FOUNDATION_EXPORT NSString *const FONT_MarkForMC_REGULAR_It;
FOUNDATION_EXPORT NSString *const FONT_MarkForMC_Nrw_BOLD;

FOUNDATION_EXPORT NSString *const BDA_PRODUCTID;

/// Default Color Hex
#define APP_COLOR_FOR_TEXT [UIColor colorWithRed:157.0f/255.0f green:3.0f/255.0f blue:41.0f/255.0f alpha:1.0f];

/***
 *Define static key
 ***/
#define FLAG_STARTED @"0"
#define stLoginStatus  @"isUserLogin"
#define stSessionToken  @"sessionToken"

/***
 *SCREEN SIZE
 ***/
#define SCREEN_SCALE  (((float)[UIScreen mainScreen].bounds.size.width)/414.0)
#define SCREEN_SCALE_BY_HEIGHT  (((float)[UIScreen mainScreen].bounds.size.width)/736.0)
#define SCREEN_WIDTH  (((float)[UIScreen mainScreen].bounds.size.width))
#define SCREEN_HEIGHT (((float)[UIScreen mainScreen].bounds.size.height))
#define IPHONE_5S (((float)[UIScreen mainScreen].bounds.size.width) == 320.0)

#define PHONE_NUMBER_LENGTH 20
#define EMAIL_MAX_LENGTH 100
#define MARGIN_KEYBOARD 5.0

#define IPAD ([[[UIDevice currentDevice] model] rangeOfString:@"iPad"].location != NSNotFound)

/***
 *For Background UIActivity
 ***/
#define SPINNER_BACKGROUND_WIDTH    60
#define SPINNER_BACKGROUND_HEIGHT   60
#define SPINNER_VIEW_WIDTH      16
#define SPINNER_VIEW_HEIGHT     16
#define NAVIGATION_HEIGHT       64
/* Table constant */
#define REFRESH_INDICATOR_HEIGHT                                    20
#define TABLE_PULL_HEIGHT                                           30.0
#define TABLE_PULL_LENGTH                                           20.0


#define HEADER_COLOR [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.1]
#define FONT_SIZE_15 15.0
#define FONT_SIZE_16 16.0
#define FONT_SIZE_23 23.0
#define FONT_SIZE_21 21.0
#define FONT_SIZE_18 18.0
#define FONT_SIZE_14 14.0


#define kJpegPhotoExt                   @"jpg"
#define kPngPhotoExt                    @"png"
#define kPhotoUploadMaxSize             2097152

/***
 *Date format
 ***/
#define DATE_FORMAT @"MMM dd yyyy"
#define DATE_SEND_REQUEST_FORMAT @"yyyy-MM-dd"
#define DATETIME_SEND_REQUEST_FORMAT @"yyyy-MM-dd HH:mm:ss"
#define TIME_FORMAT @"h:mm a"
#define TIME_SEND_REQUEST_FORMAT @"HH:mm:ss"
#define STORYBOARD_ID_LOCATION_POP_UP_VIEW_CONTROLLER @"LocationPopUpViewController"
#define DID_OPEN_LOCATION_POP_UP @"DIDOPENLOCATIONPOPUP"
#define USER_DEFAULT_YOUR_COUNTRY @"USERDEFAULTYOURCOUNTRY"
#define USER_DEFAULT_YOUR_CITY @"USERDEFAULTYOURCITY"
#define UTC_TIMEZONE @"UTC"
#define SPECIAL_CHAR @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%&*()-+_=[]:;\",.?/ "


#define DCR_ITEM_TYPE_VALUE             @[@"dining", @"accommodation",@"sightseeing_activity",@"vacation_package",@"event",@"privilege"]
#define DCR_DATE_FORMAT                 @"yyyy-MM-dd'T'HH:mm:ssZZZZ"


#define VERIFIED_BIN                    @"verifiedBINNumber"
#define CURRENT_CITY_CODE               @"currentCityCode"
#define SIX_MONTH_SECONDS_TIME          (6 * 30 * 24 * 60 * 60)
#define BIN_TEXTFIELD_TAG               1000
#define PHONE_NUMBER_TEXTFIELD_TAG      1001
#define ZIP_CODE_TEXTFIELD_TAG          1002

#define USER_CITY_LOCATION_DEVICE       @"USER_CITY_LOCATION_DEVICE"

//#define LIST_ALL_TILES  @[@"Specialty Travel",@"Hotels",@"Flowers",@"Tickets",@"Golf",@"Golf Merchandise",@"Vacation Packages",@"Cruises",@"VIP Travel Services",@"Private Jet Travel",@"Transportation",@"Wine",@"Dining",@"Retail Shopping"]
#define LIST_ALL_TILES  @[@"Dining",@"Specialty Travel",@"Hotels",@"Flowers",@"Tickets",@"Vacation Packages",@"VIP Travel Services",@"Private Jet Travel",@"Transportation",@"Wine",@"Retail Shopping",@"Golf"]

#define LIST_REGIONS @[@"Africa",@"Asia Pacific",@"Latin America",@"Caribbean",@"Europe"]
#define keyPasscode @"passcode"

@end
