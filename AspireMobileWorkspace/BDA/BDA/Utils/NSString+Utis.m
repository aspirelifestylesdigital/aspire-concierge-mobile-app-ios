//
//  NSString+Utis.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/5/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "NSString+Utis.h"
#import <CommonCrypto/CommonDigest.h>
#import "HGCheckCountryCode.h"
#import "Constant.h"

@implementation NSString (Utis)
-(BOOL)isValidEmail
{
    if(self.length > EMAIL_MAX_LENGTH)
    {
        return NO;
    }
    
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@{1}([A-Za-z0-9-]+\\.)+[A-Za-z]{2,}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

-(NSString *)createMaskStringBeforeNumberCharacter:(NSInteger)number
{
    NSMutableString *asterisks = [[NSMutableString alloc] init];
    for(int i = 0; i < self.length - number; i++)
    {
        [asterisks appendString:@"*"];
    }
    
    NSRange range = NSMakeRange(0,self.length - number);
    return [self stringByReplacingCharactersInRange:range withString:asterisks];
}

-(NSString *)createMaskForText:(BOOL)isEmail
{
    NSRange rangeForAtSign = NSMakeRange(NSNotFound, 0);
    if(self.length > 0)
        rangeForAtSign = [self rangeOfString:@"@" options:NSBackwardsSearch];
    NSString *beforeSign = isEmail ? [self substringToIndex:rangeForAtSign.location] : self;
    NSString *afterSign = isEmail ? [self substringFromIndex:rangeForAtSign.location+1] : @"";
    NSString *atSign = isEmail ? @"@" : @"";
    
    NSMutableString *asterisks = [[NSMutableString alloc] init];
    NSInteger length = 0;
    
    if(beforeSign.length == 1)
    {
        [asterisks appendString:@"*"];
        length = 0;
    }
    else if(beforeSign.length == 2)
    {
        [asterisks appendString:@"*"];
        length = 1;
    }
    else if(beforeSign.length == 3)
    {
        [asterisks appendString:@"**"];
        length = 1;
    }
    else if(beforeSign.length > 3)
    {
        for(int i = 0; i < beforeSign.length - 3; i++)
        {
            [asterisks appendString:@"*"];
        }
        length = 3;
    }
    
    NSRange range = NSMakeRange(0,beforeSign.length - length);
    NSString *newString =  [beforeSign stringByReplacingCharactersInRange:range withString:asterisks];
    return [NSString stringWithFormat:@"%@%@%@",newString,atSign,afterSign];
}


- (NSString*)MD5
{
    // Create pointer to the string as UTF8
    const char *ptr = [self UTF8String];
    
    // Create byte array of unsigned chars
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    
    // Create 16 bytes MD5 hash value, store in buffer
    CC_MD5(ptr, (uint32_t)strlen(ptr), md5Buffer);
    
    // Convert unsigned char buffer to NSString of hex values
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x",md5Buffer[i]];
    
    return output;
}

-(BOOL)isValidPhoneNumber
{
    NSString *phoneRegex1 = @"^(\\+[0-9]{1,20})$";
    NSPredicate *phonePredicate1 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex1];
    
    return [phonePredicate1 evaluateWithObject:self];
}

-(BOOL)isValidPassword {
    
    //NSString *passwordRegex = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,}";
    //NSPredicate *passwordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", passwordRegex];
    
    return YES;
}
-(BOOL)isValidWeakPassword{
    return self.length >= 5;
}
-(BOOL)isValidStrongPassword{
    //NSString *passwordRegex = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[d$@$!%*?&#])[A-Za-z\\dd$@$!%*?&#]{10,}"; //"'()+,-./:;<=>?@[\]^_`{|}~"
    NSString *passwordRegex = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[d$@$!%*?&#'()+,-./:;<=>^_`~|\\[\\]\\{\\}\"\\\\])[A-Za-z\\dd$@$!%*?&#'()+,-./:;<=>^_`~|\\[\\]\\{\\}\"\\\\]{10,}";
    NSPredicate *passwordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", passwordRegex];
    
    return [passwordPredicate evaluateWithObject:self];
}

-(BOOL)isValidZipCode
{
    NSString *zipRegex = @"^(\\d{5}(-\\d{4})*)$";
    NSPredicate *zipPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", zipRegex];
    
    return [zipPredicate evaluateWithObject:self];
}

-(BOOL)isValidBinNumber
{

//    NSString *binRegex = @"(^(5)[0-9]{3}(-[0-9]{2})$)";
    NSString *binRegex = @"(^[0-9]{4}(-[0-9]{2})$)";
    NSPredicate *binPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", binRegex];
    return [binPredicate evaluateWithObject:self];
}

-(BOOL)isValidName {    
    return  ([self length] > 1 && [self length] <= 19);
}

-(BOOL)isValidPasscode {
    return  ([self length] > 0 && [self length] <= 25);
}

-(BOOL)isValidCountryCode {

    NSString *mobileNumber =  [self stringByReplacingOccurrencesOfString:@"+" withString:@""];
    int index = (mobileNumber.length > 4) ? 4 : (int)mobileNumber.length;
    HGCheckCountryCode* checkCode= [[HGCheckCountryCode alloc] init];
    for (int i = 1; i <= index; i ++) {
        if ([checkCode checkCountryCode:[mobileNumber substringWithRange:NSMakeRange(0, i)]]) {
            return YES;
        }
    }
    return NO;
}
-(NSString*)getCoutryCode{
    NSString *stringC =  [self stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int index = (stringC.length > 4) ? 4 : (int)stringC.length;
    
    HGCheckCountryCode* checkCode= [[HGCheckCountryCode alloc] init];
    for (int i = 1; i <= index; i ++) {
        NSString *code = [stringC substringWithRange:NSMakeRange(0, i)];
        if ([checkCode checkCountryCode: code]) {
            return code;
        }
    }
    return @"";
}
-(BOOL)isValidNameWithSpecialCharacter {
    
    NSString *nameRegex = @"^['a-zA-Z0-9-\\s]*$"; //@"^[A-Za-z][A-Za-z0-9!@#$%^&*]*$";
    NSPredicate *namePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nameRegex];
    return [namePredicate evaluateWithObject:self];
}

-(NSString *)removeBlankFromURL
{
    NSString* temp = [self stringByReplacingOccurrencesOfString:@"\\s" withString:@"%20"
                                              options:NSRegularExpressionSearch
                                                range:NSMakeRange(0, [self length])];
    return temp;
}

- (NSUInteger)occurrenceCountOfCharacter:(UniChar)character
{
    CFStringRef selfAsCFStr = (__bridge CFStringRef)self;
    
    CFStringInlineBuffer inlineBuffer;
    CFIndex length = CFStringGetLength(selfAsCFStr);
    CFStringInitInlineBuffer(selfAsCFStr, &inlineBuffer, CFRangeMake(0, length));
    
    NSUInteger counter = 0;
    
    for (CFIndex i = 0; i < length; i++) {
        UniChar c = CFStringGetCharacterFromInlineBuffer(&inlineBuffer, i);
        if (c == character) counter += 1;
    }
    
    return counter;
}

-(NSString *)removeRedudantWhiteSpaceInText
{
    NSMutableString *removeWhiteSpace = [[NSMutableString alloc] initWithString:[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    return [removeWhiteSpace stringByReplacingOccurrencesOfString:@"\\s+" withString:@" " options: NSRegularExpressionSearch range: NSMakeRange(0, removeWhiteSpace.length)];
}

-(NSString *)removeRedudantNewLineInText
{
    NSString *newString = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    newString = [newString stringByReplacingOccurrencesOfString:@"\r"
                                                     withString:@""
                                                        options:NSRegularExpressionSearch
                                                          range:NSMakeRange(0, newString.length)];
    
    
    newString = [newString stringByReplacingOccurrencesOfString:@"\n\n"
                                                     withString:@"\n"
                                                        options:NSRegularExpressionSearch
                                                          range:NSMakeRange(0, newString.length)];
    newString = [newString stringByReplacingOccurrencesOfString:@">&nbsp; &nbsp;"
                                                     withString:@"&nbsp;"
                                                        options:NSRegularExpressionSearch
                                                          range:NSMakeRange(0, newString.length)];
    
    newString = [newString stringByReplacingOccurrencesOfString:@"<p>&nbsp;</p>"
                                                     withString:@"\n"
                                                        options:NSRegularExpressionSearch
                                                          range:NSMakeRange(0, newString.length)];
    
    newString = [newString stringByReplacingOccurrencesOfString:@"<p>\n</p>"
                                                     withString:@"\n"
                                                        options:NSRegularExpressionSearch
                                                          range:NSMakeRange(0, newString.length)];
    
    
    newString = [newString stringByReplacingOccurrencesOfString:@"\n<p>"
                                                     withString:@"<p>"
                                                        options:NSRegularExpressionSearch
                                                          range:NSMakeRange(0, newString.length)];
    newString = [newString stringByReplacingOccurrencesOfString:@"<br/><br/>"
                                                     withString:@""
                                                        options:NSRegularExpressionSearch
                                                          range:NSMakeRange(0, newString.length)];
    newString = [newString stringByReplacingOccurrencesOfString:@"<p><b> </p>"
                                                     withString:@""
                                                        options:NSRegularExpressionSearch
                                                          range:NSMakeRange(0, newString.length)];
    newString = [newString stringByReplacingOccurrencesOfString:@"<p style=\"margin-left:6.0pt\">&nbsp;</p>"
                                                     withString:@""
                                                        options:NSRegularExpressionSearch
                                                          range:NSMakeRange(0, newString.length)];
    
    
    newString = [newString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return newString;
}

-(NSString *)replaceWhiteSpacingTag
{
   NSString *newString = [self stringByReplacingOccurrencesOfString:@"&nbsp;"
                                                     withString:@" "
                                                        options:NSRegularExpressionSearch
                                                          range:NSMakeRange(0, self.length)];
    return newString;
}

//- (NSString*)removeSpecifiedSentencesOfHTMLText{
//    NSString *newString = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//    newString = [newString stringByReplacingOccurrencesOfString:@"Click here OR"
//                                                     withString:@""
//                                                        options:NSRegularExpressionSearch
//                                                          range:NSMakeRange(0, newString.length)];
//    
//    newString = [newString stringByReplacingOccurrencesOfString:@"Click here OR"
//                                                     withString:@""
//                                                        options:NSRegularExpressionSearch
//                                                          range:NSMakeRange(0, newString.length)];
//    
//    newString = [newString stringByReplacingOccurrencesOfString:@"Browse and Book Sightseeing & Tours Worldwide"
//                                                     withString:@""
//                                                        options:NSRegularExpressionSearch
//                                                          range:NSMakeRange(0, newString.length)];
//    
//    newString = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//    return newString;
//}

-(NSString*)converSalutationForAPI {
    if ([self.lowercaseString isEqualToString:@"Dr.".lowercaseString]) {
        return @"Dr";
    }
    if ([self.lowercaseString isEqualToString:@"Dra.".lowercaseString]) {
        return @"Dr. (Mrs)";
    }
    if ([self.lowercaseString isEqualToString:@"Srta.".lowercaseString]) {
        return @"Miss";
    }
    if ([self.lowercaseString isEqualToString:@"Sr.".lowercaseString]) {
        return @"Mr";
    }
    if ([self.lowercaseString isEqualToString:@"Sra.".lowercaseString]) {
        return @"Mrs";
    }
    return @"";
//    "Dr" = "Dr.";
//    "Dra" = "Dra.";
//    "Miss" = "Srta.";
//    "Mr" = "Sr.";
//    "Mrs" = "Sra.";
//    "Ms" = "Sr.";
    
//     ["Captain", "Doctor", "Dr. (Miss)", "Dr. (Mr)", "Dr. (Mrs)", "Dr. (Ms)", "Fraulein", "Herr", "Madam", "Master", "Miss", "Mr.", "Mrs.", "Ms", "Professor"]
}
-(NSString*)converSalutationFormAPI {
    if ([self.lowercaseString isEqualToString:@"Dr".lowercaseString]) {
        return @"Dr.";
    }
    if ([self.lowercaseString isEqualToString:@"Miss".lowercaseString]) {
        return @"Srta.";
    }
    if ([self.lowercaseString isEqualToString:@"Mr".lowercaseString]) {
        return @"Sr.";
    }
    if ([self.lowercaseString isEqualToString:@"Mrs".lowercaseString]) {
        return @"Sra.";
    }
    if ([self.lowercaseString isEqualToString:@"Dr. (Mrs)".lowercaseString]) {
        return @"Dra.";
    }
    return @"";
}
@end
