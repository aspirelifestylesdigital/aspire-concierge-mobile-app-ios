//
//  MCStandardUIButton.m
//  MobileConcierge
//
//  Created by 😱 on 7/4/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "MCSmallUIButton.h"
#import "MCBaseUIButton.h"

@implementation MCSmallUIButton

- (instancetype) init {
    if (self = [super init]) {
        self.btnType = MCButtonTypeSmall;
    }
    return self;
}

- (void) awakeFromNib {
    [super awakeFromNib];
    
    self.btnType = MCButtonTypeSmall;
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.btnType = MCButtonTypeSmall;
    }
    return self;
}

- (id) initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.btnType = MCButtonTypeSmall;
    }
    return self;
}
@end
