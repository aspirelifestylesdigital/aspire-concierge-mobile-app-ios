//
//  MCStandardUIButton.m
//  MobileConcierge
//
//  Created by 😱 on 7/4/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "MCLargerUIButton.h"
#import "MCBaseUIButton.h"

@implementation MCLargerUIButton

- (instancetype) init {
    if (self = [super init]) {
        self.btnType = MCButtonTypeLarge;
    }
    return self;
}

- (void) awakeFromNib {
    [super awakeFromNib];
    
    self.btnType = MCButtonTypeLarge;
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.btnType = MCButtonTypeLarge;
    }
    return self;
}

- (id) initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.btnType = MCButtonTypeLarge;
    }
    return self;
}
@end
