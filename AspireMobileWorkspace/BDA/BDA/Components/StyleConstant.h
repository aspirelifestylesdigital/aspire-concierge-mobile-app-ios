//
//  StyleConstant.h
//  MobileConcierge
//
//  Created by 😱 on 7/3/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//




#import "Constant.h"
#import "Common.h"

// -- Navigation Bar --
//#define BAR_FONT_NAME       FONT_MarkForMC_Nrw_BOLD
//#define BAR_FONT_SIZE       18.0f

//#define BAR_FONT_COLOR      @"#FFFFFF"

//#define APP_TINT_COLOR      @"#FFFFFF"
//#define BAR_TINT_COLOR      [UIColor colorWithRed:1.0f/255.0f green:22.0f/255.0f blue:39.0f/255.0f alpha:1.0f]


// -- Title View Controller --
//#define TITLE_VIEW_CONTROLLER_FONT_NAME     FONT_MarkForMC_MED
//#define TITLE_VIEW_CONTROLLER_FONT_SIZE     14
//#define TITLE_VIEW_CONTROLLER_TEXT_COLOR    @"#FFFFFF"
//#define TITLE_VIEW_CONTROLLER_LINE_SPACING  1.25

// - OTHER -
//#define TEXT_DISABLE                    @"#141413"

//#define BUTTON_BG_COLOR_NORMAL          @"#9D0329"
//#define BUTTON_BG_COLOR_HIGHLIGHT       @"#C54B53"

//#define TABLE_CELL_HIGHLIGHT @"#E5EBEE"

//#define SEPARATE_LINE_COLOR @"#253847"



