//
//  MCStandardUIButton.m
//  MobileConcierge
//
//  Created by 😱 on 7/4/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "MCStandardUITextField.h"

@implementation MCStandardUITextField

- (instancetype) init {
    if (self = [super init]) {
        self.TextfieldType = MCTextfieldTypeStandard;
    }
    return self;
}

- (void) awakeFromNib {
    [super awakeFromNib];
    
    self.TextfieldType = MCTextfieldTypeStandard;
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.TextfieldType = MCTextfieldTypeStandard;        
    }
    return self;
}

- (id) initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.TextfieldType = MCTextfieldTypeStandard;
    }
    return self;
}
@end
