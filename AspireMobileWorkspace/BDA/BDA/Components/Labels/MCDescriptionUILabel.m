//
//  MCCommonUILabel.m
//  MobileConcierge
//
//  Created by 😱 on 7/4/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "StyleConstant.h"
#import "MCDescriptionUILabel.h"

@implementation MCDescriptionUILabel

- (instancetype) init {
    if (self = [super init]) {
        self.labelType = MCLabelTypeDescription;
    }
    return self;
}

- (void) awakeFromNib {
    [super awakeFromNib];
    
    self.labelType = MCLabelTypeDescription;
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.labelType = MCLabelTypeDescription;
    }
    return self;
}

- (id) initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.labelType = MCLabelTypeDescription;
    }
    return self;
}

@end
