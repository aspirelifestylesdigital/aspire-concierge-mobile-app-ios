//
//  AppData.m
//  MobileConcierge
//
//  Created by Home on 5/7/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "AppData.h"
#import <Foundation/Foundation.h>
#import "Constant.h"
#import "CityItem.h"
#import "CategoryItem.h"
#import "AnswerItem.h"

#import "CCAMapCategories.h"
#import "CCAGeographicManager.h"
#import <AspireApiFramework/AspireApiFramework.h>
#import "AppDelegate.h"
#import "PreferenceObject.h"

@interface AppData()
{
    NSArray *cityItemList;
    NSArray *categoryItemList;
    NSArray *cityGuideList;
    NSArray *subCategoryItemList;
    NSArray *defaultAnswers;
    
    NSDictionary* listFeatures;
    BOOL validPasscode;
}

@end

static AppData *sharedMyManager;

@implementation AppData

+ (AppData *)getInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

+ (void)startService {
    if(!sharedMyManager) {
        [AppData getInstance];
    }
    printf("%s",[[NSString stringWithFormat:@"%s\n",__PRETTY_FUNCTION__] UTF8String]);
    [sharedMyManager createSelectionCityList];
    [sharedMyManager createCityGuideList];
}

- (instancetype) init {
    if(self = [super init]) {
        NSString * plistPath = [[NSBundle mainBundle] pathForResource:@"NewFeature" ofType:@"plist"];
        listFeatures = [NSDictionary dictionaryWithContentsOfFile:plistPath];
        [self createSelectionCategoryListForObject:self onDone:^(AppData* item,NSArray* list){
            item->categoryItemList = list;
        }];
        validPasscode = YES;
        
    }
    return self;
}

-(BOOL)isValidPasscode
{
    return validPasscode;
}

-(void)setValidPasscode:(BOOL)isValid
{
    validPasscode = isValid;
}

+ (NSArray *)getDefaultAnswers
{
    return sharedMyManager->defaultAnswers;
}


+(NSArray *)getSubCategoryItemList
{
    return sharedMyManager->subCategoryItemList;
}

+(NSArray *)getSelectionCityList
{
    for(CityItem *item in sharedMyManager->cityItemList)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cityCode == %@",item.name];
        NSArray *tempArray = [sharedMyManager->cityGuideList filteredArrayUsingPredicate:predicate];
        
        if(tempArray.count > 0)
        {
            item.ID = ((CityItem *)[tempArray objectAtIndex:0]).ID;
        }
        
    }
    
    return sharedMyManager-> cityItemList;
}

+ (NSArray *)getSelectionCategoryList
{
    return sharedMyManager->categoryItemList;
}

+(NSArray *)getCityGuideList
{
    return sharedMyManager->cityGuideList;
}

-(void)createCityGuideList
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSMutableArray *cityGuides = [[NSMutableArray alloc] init];
        
        CityItem *city = [[CityItem alloc] init];
        
        city = [[CityItem alloc] init];
        city.ID = @"6308";
        city.name = @"Chicago";
        city.cityCode = @"Chicago";
        city.image = [UIImage imageNamed:@"chicago"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6310";
        city.name = @"Las Vegas";
        city.cityCode = @"Las Vegas";
        city.image = [UIImage imageNamed:@"lasvegas"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6312";
        city.name = @"Los Angeles";
        city.cityCode = @"Los Angeles";
        city.image = [UIImage imageNamed:@"losangeles"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6307";
        city.name = @"Miami";
        city.cityCode = @"Miami";
        city.image = [UIImage imageNamed:@"miami"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6306";
        city.name = @"New York";
        city.cityCode = @"New York";
        city.image = [UIImage imageNamed:@"new_york"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6389";
        city.name = @"Orlando";
        city.cityCode = @"Orlando";
        city.image = [UIImage imageNamed:@"orlando"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6313";
        city.name = @"São Francisco";//@"San Francisco";
        city.cityCode = @"San Francisco";
        city.image = [UIImage imageNamed:@"sanfrancisco"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6309";
        city.name = @"Washington, D.C";
        city.cityCode = @"Washington, DC";
        city.image = [UIImage imageNamed:@"washington"];
        [cityGuides addObject:city];
        dispatch_async(dispatch_get_main_queue(), ^{
            printf("%s",[[NSString stringWithFormat:@"%s\n",__PRETTY_FUNCTION__] UTF8String]);
            cityGuideList = cityGuides;
        });
    });
}

-(void)createSelectionCategoryListForObject:(id)object onDone:(void(^)(id object,NSArray* list))onDone
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSMutableArray *categories = [[NSMutableArray alloc] init];
        
        CategoryItem *category = [[CategoryItem alloc] init];
        category.categoryName = @"";
        category.code = @"all";
        category.categoryImg = [UIImage imageNamed:@"all_category"];
        category.categoriesForService = LIST_ALL_TILES;
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.ID = @"2";
        category.categoryName = NSLocalizedString(@"DINING", nil);
        category.code = @"dining";
        category.categoryImg = [UIImage imageNamed:@"dining"];
        category.categoriesForService = @[@"Dining",@"Specialty Travel"];
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = NSLocalizedString(@"HOTELS", nil);
        category.code = @"hotels";
        category.categoryImg = [UIImage imageNamed:@"hotel"];
        category.categoriesForService = @[@"Hotels"];
        [categories addObject:category];
    
        category = [[CategoryItem alloc] init];
        category.categoryName = NSLocalizedString(@"TRANSPORTATION", nil);
        category.code = @"transportation";
        category.categoryImg = [UIImage imageNamed:@"transportation"];
        category.categoriesForService = @[@"Transportation"];
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = NSLocalizedString(@"VACATION \nPACKAGES", nil);
        category.code = @"vacation packages";
        category.categoryImg = [UIImage imageNamed:@"vacation"];
        category.categoriesForService = @[@"Vacation Packages"];
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = NSLocalizedString(@"TOURS", nil);
        category.code = @"tours";
        category.categoryImg = [UIImage imageNamed:@"tour"];
        category.categoriesForService = @[@"VIP Travel Services"];
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = NSLocalizedString(@"ENTERTAINMENT", nil);
        category.code = @"entertainment";
        category.categoryImg = [UIImage imageNamed:@"entertainment"];
        category.categoriesForService = @[@"Tickets",@"Specialty Travel"];
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = NSLocalizedString(@"SPORT", nil);
        category.code = @"sport";
        category.ID = @"7";
        category.categoryImg = [UIImage imageNamed:@"sport"];
        category.categoriesForService = @[@"Golf",@"Specialty Travel"];
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = NSLocalizedString(@"SHOPPING", nil);
        category.code = @"shopping";
        category.categoryImg = [UIImage imageNamed:@"shopping"];
        category.categoriesForService = @[@"Golf",@"Flowers",@"Wine",@"Retail Shopping"];
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = NSLocalizedString(@"FLOWERS", nil);
        category.code = @"flowers";
        category.categoryImg = [UIImage imageNamed:@"flowers"];
        category.categoriesForService = @[@"Flowers"];
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = NSLocalizedString(@"TRAVEL", nil);
        category.code = @"travel";
        category.categoryImg = [UIImage imageNamed:@"travel"];
        category.categoriesForService = @[@"Private Jet Travel",@"VIP Travel Services"];
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.ID = @"5";
        category.categoryName = NSLocalizedString(@"CITY GUIDE", nil);
        category.code = @"city guide";
        category.categoryImg = [UIImage imageNamed:@"city_guide"];
        [categories addObject:category];
        dispatch_async(dispatch_get_main_queue(), ^{
            if(onDone)
                onDone(object,categories);
        });
    });
}

-(void)createSubCategoriesWithIDs:(NSArray *)categoryIDs forObject:(id)object onDone:(void (^)(id obj, NSArray* list)) onDone
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSMutableArray *subCategories = [[NSMutableArray alloc] init];
        for(NSInteger i = 0; i< categoryIDs.count; i++)
        {
            CategoryItem *item = [[CategoryItem alloc] init];
            item.ID = [categoryIDs objectAtIndex:i];
            item.supperCategoryID = @"80";
            switch (i) {
                 /*
                case 0:
                    item.categoryName = @"ACCOMMODATIONS";
                    item.code = @"accommodation";
                    item.categoryImg =  [UIImage imageNamed:@"asset_cityguide"];
                    item.categoryImgDetail =  [UIImage imageNamed:@"asset_cityguide_detail"];
                    break;
                  */
                    
                case 0:
                    item.categoryName = NSLocalizedString(@"DINING", nil);
                    item.code = @"dining";
                    item.categoryImg =  [UIImage imageNamed:@"dining_cityguide"];
                    item.categoryImgDetail =  [UIImage imageNamed:@"dining_cityguide_detail"];
                    break;
                case 1:
                    item.categoryName = NSLocalizedString(@"BARS/CLUBS", nil);
                    item.code = @"bars";
                    item.categoryImg =  [UIImage imageNamed:@"bar_club_cityguide"];
                    item.categoryImgDetail =  [UIImage imageNamed:@"bar_club_cityguide_detail"];
                    break;
                case 2:
                    item.categoryName = NSLocalizedString(@"SPAS", nil);
                    item.code = @"spas";
                    item.categoryImg =  [UIImage imageNamed:@"spa_wellness_cityguide"];
                    item.categoryImgDetail =  [UIImage imageNamed:@"spa_wellness_cityguide_detail"];
                    break;
                case 3:
                    item.categoryName = NSLocalizedString(@"SHOPPING", nil);
                    item.code = @"shopping";
                    item.categoryImg =  [UIImage imageNamed:@"shopping_cityguide"];
                    item.categoryImgDetail =  [UIImage imageNamed:@"shopping_cityguide_detail"];
                    break;
                case 4:
                    item.categoryName = NSLocalizedString(@"CULTURE", nil);
                    item.code = @"culture";
                    item.categoryImg =  [UIImage imageNamed:@"culture_cityguide"];
                    item.categoryImgDetail =  [UIImage imageNamed:@"culture_cityguide_detail"];
                    break;
                    
                    
                default:
                    break;
            }
            [subCategories addObject:item];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if(onDone)
                onDone(object,subCategories);
        });
    });
}

-(void) createSelectionCityList
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSMutableArray *cityLst = [[NSMutableArray alloc] init];
        NSArray *subCategoryId;
        
        CityItem*city = [[CityItem alloc] init];
        city.name = @"Bariloche";
        city.cityCode = @"Bariloche";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.countryCode = @"Argentina";
        city.image = [UIImage imageNamed:@"barioche"];
        city.diningID = @"6638";
//        NSArray *subCategoryId = @[/*@"6478",*/@"7018",@"7019",@"7020",@"7021",@"7022"];
//        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Buenos Aires";
        city.cityCode = @"Buenos Aires";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.countryCode = @"Argentina";
        city.image = [UIImage imageNamed:@"BuenosAires"];
        city.diningID = @"6651";
//        subCategoryId = @[/*@"6478",*/@"7023",@"7024",@"7025",@"7026",@"7027"];
//        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Cartagena";
        city.cityCode = @"Cartagena";
        city.countryCode = @"Colômbia";//@"Columbia";
        city.image = [UIImage imageNamed:@"cartagana"];
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.diningID = @"6643";
//        subCategoryId = @[/*@"6478",*/@"7028",@"7029",@"7030",@"7031",@"7032"];
//        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Chicago";
        city.cityCode = @"Chicago";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.countryCode = @"EUA";
        city.image = [UIImage imageNamed:@"chicago"];
        city.diningID = @"6720";
        subCategoryId = @[/*@"6478",*/@"7177",@"7178",@"7179",@"7180",@"7181"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        
        city = [[CityItem alloc] init];
        city.name = @"Fernando de Noronha";
        city.cityCode = @"Fernando de Noronha";
        city.stateCode = @"Pernambuco";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.countryCode = @"Brasil";
        city.image = [UIImage imageNamed:@"fernanado"];
        city.diningID = @"6652";
        subCategoryId = @[/*@"6478",*/@"7033",@"7034",@"7035",@"7036",@"7037"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Florianópolis"; //@"Florianopolis";
        city.cityCode = @"Florianopolis";
        city.stateCode = @"Santa Catarina";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.countryCode = @"Brasil";
        city.image = [UIImage imageNamed:@"floria"];
        city.diningID = @"6653";
        subCategoryId = @[/*@"6478",*/@"7038",@"7039",@"7040",@"7041",@"7042"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Las Vegas";
        city.cityCode = @"Las Vegas";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.countryCode = @"EUA";
        city.image = [UIImage imageNamed:@"lasvegas"];
        city.diningID = @"6721";
        subCategoryId = @[/*@"6478",*/@"6990",@"6991",@"6992",@"6993",@"6994"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Los Angeles";
        city.cityCode = @"Los Angeles";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.countryCode = @"EUA";
        city.image = [UIImage imageNamed:@"losangeles"];
        city.diningID = @"6722";
        subCategoryId = @[/*@"6478",*/@"7182",@"7183",@"7184",@"7185",@"7186"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Manaus";
        city.cityCode = @"Manaus";
        city.stateCode = @"Amazonas";
        city.countryCode = @"Brasil";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.image = [UIImage imageNamed:@"Manaus"];
        city.diningID = @"6654";
        subCategoryId = @[/*@"6478",*/@"7043",@"7044",@"7045",@"7046",@"7047"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
       
        city = [[CityItem alloc] init];
        city.name = @"Miami";
        city.cityCode = @"Miami";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.countryCode = @"EUA";
        city.image = [UIImage imageNamed:@"miami"];
        city.diningID = @"6723";
        subCategoryId = @[/*@"6478",*/@"7187",@"7188",@"7189",@"7190",@"7191"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Montevidéu";//@"Montevideo";
        city.cityCode = @"Montevideo";
        city.countryCode = @"Uruguai";//@"Uruguay";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.image = [UIImage imageNamed:@"Montevideo"];
        city.diningID = @"6644";
//        subCategoryId = @[/*@"6479",*/@"7048",@"7049",@"7050",@"7051",@"7052"];
//        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"New York";
        city.cityCode = @"New York";
        city.countryCode = @"EUA";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.image = [UIImage imageNamed:@"new_york"];
        city.diningID = @"6724";
        subCategoryId = @[/*@"6478",*/@"7192",@"7193",@"7194",@"7195",@"7196"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Orlando";
        city.cityCode = @"Orlando";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.countryCode = @"EUA";
        city.image = [UIImage imageNamed:@"orlando"];
        city.diningID = @"6725";
        subCategoryId = @[/*@"6478",*/@"7197",@"7198",@"7199",@"7200",@"7201"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        /*
        city = [[CityItem alloc] init];
        city.name = @"Pantanal";
        city.cityCode = @"Pantanal";
        city.stateCode = @"Mato Grosso do Sul, MS";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.countryCode = @"Brasil";
        city.image = [UIImage imageNamed:@"panatal"];
        city.diningID = @"6655";
//        subCategoryId = @[@"6699",@"6700",@"6701",@"6702",@"6703"];
//        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
        [cityLst addObject:city];
        */
        
        city = [[CityItem alloc] init];
        city.name = @"Rio de Janeiro";
        city.cityCode = @"Rio de Janeiro";
        city.stateCode = @"Rio de Janeiro";
        city.countryCode = @"Brasil";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.image = [UIImage imageNamed:@"rio"];
        city.diningID = @"6656";
        subCategoryId = @[/*@"6478",*/@"7058",@"7059",@"7060",@"7061",@"7062"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Salvador";
        city.cityCode = @"Salvador";
        city.stateCode = @"Bahia";
        city.countryCode = @"Brasil";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.image = [UIImage imageNamed:@"salvador"];
        city.diningID = @"6657";
        subCategoryId = @[/*@"6478",*/@"7063",@"7064",@"7065",@"7066",@"7067"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"São Francisco";//@"San Francisco";
        city.cityCode = @"San Francisco";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.countryCode = @"EUA";
        city.image = [UIImage imageNamed:@"sanfrancisco"];
        city.diningID = @"6719";
        subCategoryId = @[/*@"6478",*/@"7207",@"7208",@"7209",@"7210",@"7211"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Santiago";
        city.cityCode = @"Santiago";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.countryCode = @"Chile";
        city.image = [UIImage imageNamed:@"santiago"];
        city.diningID = @"6640";
//        subCategoryId = @[/*@"6478",*/@"7002",@"7003",@"7004",@"7005",@"7006"];
//        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"São Paulo";
        city.cityCode = @"Sao Paulo";
        city.stateCode = @"São Paulo";
        city.countryCode = @"Brasil";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.image = [UIImage imageNamed:@"saopaolo"];
        city.diningID = @"6658";
        subCategoryId = @[/*@"6479",*/@"7068",@"7069",@"7070",@"7071",@"7072"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Washington, D.C.";
        city.cityCode = @"Washington D.C.";
        city.countryCode = @"EUA";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.image = [UIImage imageNamed:@"washington"];
        city.diningID = @"6726";
        subCategoryId = @[/*@"6478",*/@"7212",@"7213",@"7214",@"7215",@"7216"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        NSArray *sortedArray = [cityLst sortedArrayUsingComparator:^NSComparisonResult(CityItem *obj1, CityItem *obj2){ return [obj1.name compare:obj2.name];}];
        
        [cityLst removeAllObjects];
        [cityLst addObjectsFromArray:sortedArray];
        
        city = [[CityItem alloc] init];
        city.name = @"África - Emirados Árabes";//@"Africa - Emirados Arabes";
        city.regionCode = @"Africa";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.regionCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.regionCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.regionCode];
        city.image = [UIImage imageNamed:@"africa"];
        city.diningID = @"7099";
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"América Latina";
        city.encodeCityName = @"Ame&#769;rica Latina";
        city.regionCode = @"Latin America";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.regionCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.regionCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.regionCode];
        city.image = [UIImage imageNamed:@"latin_america"];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Ásia";
        city.encodeCityName = @"A&#769;sia";
        city.regionCode = @"Asia Pacific";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.regionCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.regionCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.regionCode];
        city.image = [UIImage imageNamed:@"asia_pacific"];
        city.diningID = @"6966";
//        subCategoryId = @[/*@"6478",*/@"6973",@"6974",@"6975",@"6976",@"6977"];
//        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Canadá";
        city.encodeCityName = @"Canada&#769;";
        city.regionCode = @"Canada";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.regionCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.regionCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.regionCode];
        city.image = [UIImage imageNamed:@"canada"];
//        city.diningID = @"-1";
        //    city.subCategories = [self createSubCategoriesWithIDs:@[@"6480"]];
        city.diningID = @"7102";
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Caribe";
        city.regionCode = @"Caribbean";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.regionCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.regionCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.regionCode];
        city.image = [UIImage imageNamed:@"caribbean"];
        city.diningID = @"7106";
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"EUA";
        city.regionCode = @"EUA";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.regionCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.regionCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.regionCode];
        city.image = [UIImage imageNamed:@"eua"];
        city.diningID = @"7104";
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Europa";
        city.regionCode = @"Europe";
        city.image = [UIImage imageNamed:@"europe"];
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.regionCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.regionCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.regionCode];
        city.diningID = @"7097";
        [cityLst addObject:city];

        dispatch_async(dispatch_get_main_queue(), ^{
            printf("%s",[[NSString stringWithFormat:@"%s\n",__PRETTY_FUNCTION__] UTF8String]);
            cityItemList = cityLst;
        });
    });
}
+(NSDictionary *)getMenuItems
{
    return @{@"0":@[NSLocalizedString(@"HOME", nil),
                    NSLocalizedString(@"ASK THE CONCIERGE", nil),
                    NSLocalizedString(@"EXPLORE", nil)],
             @"1":@[NSLocalizedString(@"MY PROFILE", nil),
                    NSLocalizedString(@"PREFERENCES", nil),
                    NSLocalizedString(@"CHANGE PASSWORD", nil)
                    ],
             @"2":@[NSLocalizedString(@"ABOUT THIS APP", nil),
                    NSLocalizedString(@"PRIVACY POLICY", nil),
                    NSLocalizedString(@"TERMS OF USE", nil),
//                    NSLocalizedString(@"GENERATE FAKE BIN", nil),
                    NSLocalizedString(@"SIGN OUT", nil),
//                    NSLocalizedString(@"Invalid Passcode", nil)
                    ],
             };
    
}

+(void)storedTimeAcceptedPolicy
{
    [[NSUserDefaults standardUserDefaults] setDouble:[[NSDate date] timeIntervalSince1970] forKey:@"TimeAcceptedPolicy"];
    [[NSUserDefaults standardUserDefaults]  synchronize];
}

+(void)setCreatedProfileSuccessful
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ProfileCreatedSuccessful"];
    [[NSUserDefaults standardUserDefaults]  synchronize];
}


+ (BOOL) isCreatedProfile
{
    return ([[NSUserDefaults standardUserDefaults] dictionaryForKey:keyProfile] != nil);
}

+(NSTimeInterval)getTimeAcceptedPolicy
{
    return [[NSUserDefaults standardUserDefaults] doubleForKey:@"TimeAcceptedPolicy"];
}

+(BOOL)isAcceptedPolicyOverSixMonths
{
    return (([[NSDate date] timeIntervalSince1970] - [AppData getTimeAcceptedPolicy]) > SIX_MONTH_SECONDS_TIME);
}


+(void)setCurrentCityWithCode:(NSString *)cityName
{
    [[NSUserDefaults standardUserDefaults] setValue:cityName forKey:CURRENT_CITY_CODE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(BOOL)isCurrentCity
{
    if([[NSUserDefaults standardUserDefaults] valueForKey:CURRENT_CITY_CODE])
    {
        return YES;
    }
    return NO;
}

+(CityItem *)getSelectedCity
{
    AppDelegate* dl = (AppDelegate*)[UIApplication sharedApplication].delegate;
    //    NSString *currentCityCode = [[NSUserDefaults standardUserDefaults] valueForKey:CURRENT_CITY_CODE];
    NSString *currentCityCode;
    if ([User isValid] && ![dl isUpdatingProfile]) {
        UserObject *userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
        currentCityCode = userObject.currentCity;
    } else {
        currentCityCode = [[NSUserDefaults standardUserDefaults] valueForKey:CURRENT_CITY_CODE];
    }
    
    if(currentCityCode)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@",currentCityCode];
        NSArray *cities = sharedMyManager-> cityItemList;
        NSArray *tempArray = [cities filteredArrayUsingPredicate:predicate];
        if(tempArray.count > 0)
        {
            return [tempArray objectAtIndex:0];
        }
    }
    return nil;
}

+ (BOOL) isApplyFeatureWithKey:(NSString *)key {
    BOOL result = NO;
    if(!sharedMyManager)
        return result;
    if([[sharedMyManager->listFeatures allKeys] containsObject:key]) {
        result = [[sharedMyManager->listFeatures objectForKey:key] boolValue];
    }
    return result;
}

+ (void)setUserCityLocationDevice:(NSString *)city {
    if(city.length > 0)
        [[NSUserDefaults standardUserDefaults] setValue:city forKey:USER_CITY_LOCATION_DEVICE];
    else
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_CITY_LOCATION_DEVICE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)getUserCityLocationDevice {
    return [[NSUserDefaults standardUserDefaults] valueForKey:USER_CITY_LOCATION_DEVICE];
}

+ (BOOL) isApplyNewFeatureWithKey:(NSString*)keyFeature {
//    !!!: Apply New Feature
    return [AppData isApplyFeatureWithKey:keyFeature];
}

+(NSDictionary *)getPreferencesItems {
    return @{
             @"DINING":@[@"None", @"African", @"Amalfitano", @"American", @"Arabic", @"Argentinian", @"Asian", @"Australian", @"Austrian", @"Brazilian",@"British",@"Burmese",@"Californian" ,@"Canadian" ,@"Cantonese" ,@"Caribbean" ,@"Chinese",@"Classic Italian/Traditional",@"Cuban",@"Dutch",@"European",@"French",@"German",@"Greek",@"Hawaii Regional",@"Indian",@"Indonesian",@"International/Global",@"Italian",@"Jamaican",@"Japanese",@"Korean",@"Latin",@"Lebanese",@"Mandarin",@"Mediterranean",@"Mexican",@"Middle Eastern",@"Modern/Contempo American",@"Modern/Contempo French",@"Modern/Contempo German",@"Modern/Contempo Italian",@"Modern/Contempo Mexican",@"Moroccan",@"Persian",@"Peruvian",@"Pizza",@"Polish",@"Roman",@"Rortuguese",@"Russian",@"Scandinavian",@"Seafood",@"Singaporean",@"South American",@"Spanish",@"Sri Lankan",@"Steak/Steakhouse", @"Swedish",@"Swiss",@"Szechuan",@"Taiwanese",@"Thai",@"Turkish",@"Tuscan",@"Umbrian",@"Venetian",@"Vietnamese"],
             @"HOTEL":@[@"None", @"1 Star", @"2 Stars", @"3 Stars", @"4 Stars", @"5 Stars"],
             @"TRANSPORTATION":@[@"None", @"Luxury Sedan", @"MPV", @"Standard (Sedan)", @"Compacto Sedan", @"SUV", @"Executivo Sedan", @"Minivan"],
             };
}
+(NSDictionary *)getPreferencesItemsLocalizable {
    return @{
             @"DINING":@[NSLocalizedString(@"None", nil), NSLocalizedString(@"African", nil), NSLocalizedString(@"Amalfitano", nil), NSLocalizedString(@"American", nil), NSLocalizedString(@"Arabic",nil), NSLocalizedString(@"Argentinian", nil ), NSLocalizedString(@"Asian", nil), NSLocalizedString(@"Australian", nil), NSLocalizedString(@"Austrian", nil), NSLocalizedString(@"Brazilian", nil), NSLocalizedString(@"British", nil), NSLocalizedString(@"Burmese", nil), NSLocalizedString(@"Californian", nil), NSLocalizedString(@"Canadian", nil), NSLocalizedString(@"Cantonese", nil), NSLocalizedString(@"Caribbean", nil), NSLocalizedString(@"Chinese", nil), NSLocalizedString(@"Classic Italian/Traditional", nil), NSLocalizedString(@"Cuban", nil), NSLocalizedString(@"Dutch", nil), NSLocalizedString(@"European",nil), NSLocalizedString(@"French",nil), NSLocalizedString(@"German",nil), NSLocalizedString(@"Greek",nil), NSLocalizedString(@"Hawaii Regional",nil),NSLocalizedString(@"Indian",nil), NSLocalizedString(@"Indonesian",nil),NSLocalizedString(@"International/Global",nil), NSLocalizedString(@"Italian",nil), NSLocalizedString(@"Jamaican",nil), NSLocalizedString(@"Japanese",nil), NSLocalizedString(@"Korean",nil), NSLocalizedString(@"Latin",nil), NSLocalizedString(@"Lebanese", nil), NSLocalizedString(@"Mandarin", nil), NSLocalizedString(@"Mediterranean", nil), NSLocalizedString(@"Mexican", nil), NSLocalizedString(@"Middle Eastern", nil), NSLocalizedString(@"Modern/Contempo American",nil), NSLocalizedString(@"Modern/Contempo French",nil), NSLocalizedString(@"Modern/Contempo German", nil), NSLocalizedString(@"Modern/Contempo Italian", nil), NSLocalizedString(@"Modern/Contempo Mexican", nil), NSLocalizedString(@"Moroccan", nil), NSLocalizedString(@"Persian",nil), NSLocalizedString(@"Peruvian",nil), NSLocalizedString(@"Pizza",nil), NSLocalizedString(@"Polish",nil), NSLocalizedString(@"Roman",nil), NSLocalizedString(@"Rortuguese",nil), NSLocalizedString(@"Russian",nil), NSLocalizedString(@"Scandinavian",nil), NSLocalizedString(@"Seafood",nil), NSLocalizedString(@"Singaporean",nil), NSLocalizedString(@"South American",nil), NSLocalizedString(@"Spanish",nil), NSLocalizedString(@"Sri Lankan",nil), NSLocalizedString(@"Steak/Steakhouse",nil), NSLocalizedString(@"Swedish",nil), NSLocalizedString(@"Swiss",nil), NSLocalizedString(@"Szechuan",nil), NSLocalizedString(@"Taiwanese",nil), NSLocalizedString(@"Thai",nil), NSLocalizedString(@"Turkish",nil), NSLocalizedString(@"Tuscan",nil), NSLocalizedString(@"Umbrian",nil), NSLocalizedString(@"Venetian",nil), NSLocalizedString(@"Vietnamese",nil)],
             @"HOTEL":@[NSLocalizedString(@"None", nil), NSLocalizedString(@"1 Star", nil), NSLocalizedString(@"2 Stars", nil), NSLocalizedString(@"3 Stars", nil), NSLocalizedString(@"4 Stars", nil), NSLocalizedString(@"5 Stars", nil)],
             @"TRANSPORTATION":@[NSLocalizedString(@"None", nil), NSLocalizedString(@"Luxury Sedan", nil), NSLocalizedString(@"MPV", nil), NSLocalizedString(@"Standard (Sedan)", nil), NSLocalizedString(@"Compacto Sedan", nil), NSLocalizedString(@"SUV", nil), NSLocalizedString(@"Executivo Sedan", nil), NSLocalizedString(@"Minivan", nil)],
             };
}

+ (BOOL)shouldGetDistanceForListDiningFromCity:(CityItem *)city {
    BOOL have = NO;
    NSString* deviceCity = [[AppData getUserCityLocationDevice] lowercaseString];
    NSString* selectCity = [city.name lowercaseString];
    have = [deviceCity containsString:selectCity];
    if([LIST_REGIONS containsObject:city.regionCode]) {
        have = YES;
    }
    return have;
}
@end
