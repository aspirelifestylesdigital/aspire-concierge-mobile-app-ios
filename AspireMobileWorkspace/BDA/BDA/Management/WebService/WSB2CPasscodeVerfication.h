//
//  WSB2CPasscodeVerfication.h
//  MobileConciergeUSDemo
//
//  Created by Chung Mai on 10/19/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "WSB2CBase.h"

@interface WSB2CPasscodeVerfication : WSB2CBase<DataLoadDelegate>

-(void) verifyPasscode:(NSDictionary *)infoDict;

@end
