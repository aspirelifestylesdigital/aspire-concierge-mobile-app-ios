//
//  WSB2CCreateNewConciergeCase.m
//  MobileConcierge
//
//  Created by user on 5/15/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSB2CCreateConciergeCase.h"
#import "CreateConciergeCaseResponseObject.h"
#import "UserObject.h"

@implementation WSB2CCreateConciergeCase
-(void) askConciergeWithMessage:(NSDictionary*)dict
{
    NSString* url = [B2C_REQUEST_URL stringByAppendingString:B2C_REQUEST_API];
    
    [self POST:url withParams:[self buildRequestParamsWithMessage:dict]];
    
    // return response
}
-(NSMutableDictionary*) buildRequestParamsWithMessage:(NSDictionary*)dict{
    NSMutableDictionary *contentDict = [[NSMutableDictionary alloc] init];
    
    [contentDict setObject:[self configurationAccessTokenDict] forKey:@"forThisExampleImplementationOnly_ConfigurationAccessToken"];
    [contentDict setObject:[self newCaseRequestDictWithMessage:dict] forKey:@"newCaseRequest"];
    NSLog(@"%@",contentDict);
    return contentDict;
}

- (NSMutableDictionary *)newCaseRequestDictWithMessage:(NSDictionary*)dict{
    
    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[self configurationAccessTokenDict] forKey:@"accessToken"];
    
    [params setValue:[NSNull null] forKey:@"programId"];
    [params setValue:[[SessionData shareSessiondata] OnlineMemberID] forKey:@"memberId"];

    if (profileDictionary.count > 2) {
        NSString *salutation = [profileDictionary objectForKey:keySalutation];
        [params setValue:(salutation.length > 0)?salutation:@"N/A" forKey:@"salutation"];
        [params setValue:[profileDictionary objectForKey:keyFirstName] forKey:@"firstName"];
        [params setValue:[profileDictionary objectForKey:keyLastName] forKey:@"lastName"];
        [params setValue:[profileDictionary objectForKey:keyMobileNumber] forKey:@"phoneNumber"];
        [params setValue:[profileDictionary objectForKey:keyEmail] forKey:@"emailAddress"];
        NSString *requestCity = dict[@"requestCity"];
        NSString *requestType = dict[@"requestType"];
        [params setValue:(requestCity.length > 0)?requestCity:@"N/A" forKey:@"requestCity"];
        [params setValue:(requestType.length > 0)?requestType:@"N/A" forKey:@"requestType"];
        
        NSString *requestDetail = @"";
        
        if ([dict[@"phone"] boolValue] == YES && [dict[@"mail"] boolValue] == YES) {
            requestDetail = [requestDetail stringByAppendingString:[NSString stringWithFormat:@"Respond by %@ or Respond by %@\n\n",[profileDictionary objectForKey:keyMobileNumber],[profileDictionary objectForKey:keyEmail]]];
        } else {
            if ([dict[@"phone"] boolValue] == YES) {
                requestDetail = [requestDetail stringByAppendingString:[NSString stringWithFormat:@"Respond by %@\n\n",[profileDictionary objectForKey:keyMobileNumber]]];
            }else if ([dict[@"mail"] boolValue] == YES) {
                requestDetail = [requestDetail stringByAppendingString:[NSString stringWithFormat:@"Respond by %@\n\n",[profileDictionary objectForKey:keyEmail]]];
            }
        }
        requestDetail = [requestDetail stringByAppendingString:dict[@"message"]];
        [params setValue:requestDetail forKey:@"requestDetails"];
    }
    return params;
}
- (NSMutableDictionary*)configurationAccessTokenDict{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    NSDictionary *keyDict = @{@"key":LICENSE_KEY};
    
    [dict setObject:keyDict forKey:@"licenseKey"];
    [dict setValue:B2C_SUBDOMAIN forKey:@"applicationName"];
    [dict setValue:B2C_PW forKey:@"programPassword"];
    
    return dict;
}

- (NSString*) getJSONStringFromArray:(NSMutableArray*)array{
    NSError* error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:&error];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}
-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        CreateConciergeCaseResponseObject *response = [[CreateConciergeCaseResponseObject alloc] initFromDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && response.isSuccess){
            self.data = [response data];
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.isSuccess];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}
@end
