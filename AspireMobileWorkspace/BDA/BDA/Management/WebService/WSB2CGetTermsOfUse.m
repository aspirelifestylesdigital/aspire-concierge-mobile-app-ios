//
//  WSB2CGetTermsOfUse.m
//  MobileConcierge
//
//  Created by user on 6/26/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSB2CGetTermsOfUse.h"
#import "TermsOfUseInfoResponseObject.h"

@implementation WSB2CGetTermsOfUse
-(void)retrieveDataFromServer
{
    NSString* url = [B2C_IA_API_URL stringByAppendingString:GetUtility];
    [self POST:url withParams:[self buildRequestParams]];
}

-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    
    // cause of security scanning, not allow get and set password directly
    NSString* userPass = B2C_PW;
    [dictKeyValues setObject:B2C_SUBDOMAIN forKey:@"subDomain"];
    [dictKeyValues setObject:userPass forKey:@"password"];
    [dictKeyValues setObject:@"iOSTOU" forKey:@"type"];
    return dictKeyValues;
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        TermsOfUseInfoResponseObject *response = [[TermsOfUseInfoResponseObject alloc] initFromDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && response.isSuccess){
            self.data = [response data];
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.isSuccess];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}

-(void)processDataResultWithError:(NSError *)error
{
    [self.delegate loadDataFailFrom:nil withErrorCode:error.code errorMessage:error.localizedDescription];
}
@end
