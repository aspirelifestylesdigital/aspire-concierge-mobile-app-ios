//
//  WSB2CRefreshToken.m
//  ALC
//
//  Created by Anh Tran on 11/1/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CRefreshToken.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "BaseResponseObject.h"
#import "AppDelegate.h"

@implementation WSB2CRefreshToken

-(void) refreshAccessToken:(NSString*) token requestToken:(NSString*) requestToken{
    self.subTask = WS_ST_NONE;
    self.task = WS_GET_REFRESH_ACCESS_TOKEN;

    if(token && requestToken){
        NSString* url = [MCD_API_URL stringByAppendingString:RefreshAccessToken];
        NSString* urlString = [NSString stringWithFormat:@"%@?AccessToken=%@&RefreshToken=%@&ConsumerKey=%@",url,token,requestToken,B2C_ConsumerKey];
        [self GET:urlString withParams:nil];
    }
}
-(void) refreshAccessToken{
    self.subTask = WS_ST_NONE;
    self.task = WS_GET_REFRESH_ACCESS_TOKEN;
    
    NSString* url = [MCD_API_URL stringByAppendingString:RefreshAccessToken];
    [self GET:url withParams:[self buildRequestParams]];
}

-(void) processDataResults:(NSDictionary*)jsonResult forTask:(NSInteger)ntask forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat
{
    if(jsonResult){
        BaseResponseObject *result = [[BaseResponseObject alloc] initFromDict:jsonResult];
        result.task = ntask;
        if(result.isSuccess){
            //update expired time
            AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
            appdele.B2C_ExpiredAt = [[NSDate date] dateByAddingTimeInterval:3599];
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            
            
            [self.delegate loadDataFailFrom:result withErrorCode:kSTATUSTOKENEXPIRE];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}

-(void)processDataResultWithError:(NSError *)error
{
    [self.delegate loadDataFailFrom:nil withErrorCode:error.code];
}

-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
//    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
//    [dictKeyValues setObject:appdele.ACCESS_TOKEN forKey:@"AccessToken"];
//    [dictKeyValues setObject:appdele.REFRESH_TOKEN forKey:@"RefreshToken"];
    [dictKeyValues setObject:B2C_ConsumerKey forKey:@"ConsumerKey"];
    [dictKeyValues setObject:B2C_ConsumerKey forKey:@"AccessToken"];
    [dictKeyValues setObject:B2C_ConsumerKey forKey:@"RequestToken"];
    
    return dictKeyValues;
}

@end
