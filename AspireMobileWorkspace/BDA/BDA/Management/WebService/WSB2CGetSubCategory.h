//
//  WSB2CGetSubCategory.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/30/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSB2CBase.h"

@interface WSB2CGetSubCategory : WSB2CBase

@property(nonatomic, strong) NSString *categoryId;
-(void) loadSubCategoryForCategory;

@end
