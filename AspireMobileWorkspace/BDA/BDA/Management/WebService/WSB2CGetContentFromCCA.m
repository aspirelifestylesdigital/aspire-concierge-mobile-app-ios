//
//  WSB2CGetContentFromCCA.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/5/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSB2CGetContentFromCCA.h"
#import "QuestionResponseObject.h"
#import "SBJsonWriter.h"
#import "ContentFullCCAResponseObject.h"

@implementation WSB2CGetContentFromCCA

-(void) loadContentFulls
{
    NSString* url = [B2C_IA_API_URL stringByAppendingString:GetContentFulls];
    [self POST:url withParams:[self buildRequestParams]];
}


-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    
    // cause of security scanning, not allow get and set password directly
    NSString* userPass = B2C_PW;
    [dictKeyValues setObject:B2C_SUBDOMAIN forKey:@"subDomain"];
    [dictKeyValues setObject:userPass forKey:@"password"];
   
    NSData *json = [NSJSONSerialization dataWithJSONObject:self.categories options:0 error:nil];
    //assign back
    NSArray *array = [NSJSONSerialization JSONObjectWithData:json options:0 error:nil];
    
    [dictKeyValues setObject:array forKey:@"categories"];
    return dictKeyValues;
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        ContentFullCCAResponseObject *response = [[ContentFullCCAResponseObject alloc] initFromDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && response.isSuccess){
            self.data = [response data];
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.isSuccess];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}

-(void)processDataResultWithError:(NSError *)error
{
    [self.delegate loadDataFailFrom:nil withErrorCode:error.code];
}


@end
