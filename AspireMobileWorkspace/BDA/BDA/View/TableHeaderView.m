//
//  TableHeaderView.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/21/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "TableHeaderView.h"
#import "Common.h"
#import "Constant.h"
#import "UIButton+Extension.h"
#import "UtilStyle.h"

@implementation TableHeaderView

-(void)setupViewWithMessage:(NSString *)message
{
    resetScaleViewBaseOnScreen(self);
    self.messageLbl.text = (isNetworkAvailable() ? message : @"");
    self.messageLbl.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionGreetingSize]];
    self.messageLbl.textColor = [AppColor placeholderTextColor];
    if (isNetworkAvailable()) {
        self.askConciergeBtn.hidden = NO;
        [self.askConciergeBtn setTitle:NSLocalizedString(@"ASK THE CONCIERGE", nil) forState:UIControlStateNormal];
        [self.askConciergeBtn setBackgroundColorForNormalStatus];
        [self.askConciergeBtn setBackgroundColorForTouchingStatus];
        [self.askConciergeBtn configureDecorationForButton];
    }else{
        self.askConciergeBtn.hidden = YES;
    }

}

@end
