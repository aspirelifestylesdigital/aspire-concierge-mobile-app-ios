//
//  SearchHeaderView.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/11/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "SearchHeaderView.h"
#import "Common.h"
#import "Constant.h"
#import "UtilStyle.h"
#import "UIView+Extension.h"

@implementation SearchHeaderView

-(void)setupView
{
    resetScaleViewBaseOnScreen(self);
    [self.offerClear setImage:[UIImage imageNamed:@"clear_icon"] forState:UIControlStateNormal];
    [self.bookOnlineClear setImage:[UIImage imageNamed:@"clear_icon"] forState:UIControlStateNormal];
    
    [self.bookOnlineText setText:NSLocalizedString(@"Can Book Online", nil)];
    [self.offerText setText:NSLocalizedString(@"With Offers", nil)];
    
    UIColor *color = [AppColor backgroundColor];
    self.searchText.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.searchText.textColor = color;
    self.searchText.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Search", nil) attributes:@{NSForegroundColorAttributeName: [AppColor placeholderTextColor]}];
    self.offerText.font = [UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize titleTextSize]];
    self.offerText.textColor = color;
//    [self.searchTextView setbackgroundColorForLineMenu];
    
    UIButton *clearButton = [self.searchText valueForKey:@"_clearButton"];
    [clearButton setImage:[UIImage imageNamed:@"clear_icon"] forState:UIControlStateNormal];
    [clearButton setImage:[UIImage imageNamed:@"clear_icon"] forState:UIControlStateHighlighted];
    
    self.searchText.clearButtonMode = UITextFieldViewModeAlways;
    self.searchText.returnKeyType = UIReturnKeySearch;
    
    if(!self.isSearchOffer && !self.isSearchBookOnline)
    {
        self.offerClear.hidden = YES;
        self.offerText.hidden = YES;
        self.bookOnlineClear.hidden = YES;
        self.bookOnlineText.hidden = YES;
    }
    else if(self.isSearchOffer && !self.isSearchBookOnline)
    {
        self.bookOnlineClear.hidden = YES;
        self.bookOnlineText.hidden = YES;
    }
    else if(!self.isSearchOffer && self.isSearchBookOnline)
    {
        self.offerText.text = self.bookOnlineText.text;
        self.bookOnlineClear.hidden = YES;
        self.bookOnlineText.hidden = YES;
    }
}

-(CGFloat)calculateHeightView
{
    if(self.isSearchOffer && self.isSearchBookOnline)
    {
        return (self.searchTextView.frame.size.height + self.bookOnlineClear.frame.size.height + self.offerClear.frame.size.height) * SCREEN_SCALE + 30.0f;
    }
    else if(self.isSearchOffer && !self.isSearchBookOnline)
    {
        return (self.searchTextView.frame.size.height + self.offerClear.frame.size.height) * SCREEN_SCALE  + 20.0f;
    }
    else if(!self.isSearchOffer && self.isSearchBookOnline)
    {
        return (self.searchTextView.frame.size.height + self.bookOnlineClear.frame.size.height ) * SCREEN_SCALE + 20.0f;
    }
    else{
        return self.searchTextView.frame.size.height * SCREEN_SCALE;
    }
}

-(void)setBottomLine
{
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0.0f, self.frame.size.height - 1, SCREEN_WIDTH, 1.0f)];
    [line setBackgroundColor:[UIColor colorWithRed:0.8f green:0.8f blue:0.8f alpha:1.f]];
    [self addSubview:line];
}

@end
