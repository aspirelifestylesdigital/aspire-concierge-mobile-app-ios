//
//  PrivacyPolicyViewController.h
//  MobileConcierge
//
//  Created by Chung Mai on 4/26/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "BaseViewController.h"

@interface PrivacyPolicyViewController : BaseViewController
{
    UIBarButtonItem *backButtonItem;
    NSString *content;
    float textHeight;
}


@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, weak) IBOutlet UILabel *titleViewController;
@property (nonatomic, weak) IBOutlet UILabel *confirmMessageLabel;
@property (nonatomic, weak) IBOutlet UIButton *checkBoxButton;
@property (nonatomic, weak) IBOutlet UIButton *submitButton;
@property (nonatomic, strong) NSString *urlStr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *submitBtnWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *submitBtnHeightConstraint;
@property (nonatomic, assign) BOOL             isRecheckPolicy;
@property (nonatomic, strong) NSString *policyText;
@property (nonatomic, strong) NSString *policyVersion;

- (IBAction)submitButtonTapped:(id)sender;

- (void) didScrollToEnd;
@end
