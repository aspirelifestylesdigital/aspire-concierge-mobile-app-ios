//
//  HomeViewController.h
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/13/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface HomeViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UIImageView *imgBackground;


@property (weak, nonatomic) IBOutlet UILabel *lblWelcomeMessage;
@property (weak, nonatomic) IBOutlet UILabel *lblWelcome;

@property (weak, nonatomic) IBOutlet UIView *galleryView;
@property (weak, nonatomic) IBOutlet UIButton *btnGallery;
- (IBAction)touchViewGallery:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnAskConcierge;
@property (weak, nonatomic) IBOutlet UILabel *lblAskConciergeMessage;
- (IBAction)touchAskConcierge:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnExplore;
- (IBAction)touchExplore:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblExploreMessage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraintWelcomeView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraintGalleryView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraintFooterView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topExploreLabelConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topAskConciergeLabelConstraint;

@property (weak, nonatomic) IBOutlet UIImageView *heightConstraintGalleryView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintFooterView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintFooter2View;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintWelcomeView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraintHeightAskButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstaintFooterWithTopView;


@property (assign,nonatomic) BOOL gotoAskConcierge;

@end
