//
//  SignInViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 4/26/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "SignInViewController.h"
#import "SWRevealViewController.h"
#import "PrivacyPolicyViewController.h"
#import "Constant.h"
#import "NSString+Utis.h"
#import "UIButton+Extension.h"
#import "UILabel+Extension.h"
#import <SexyTooltip.h>
#import "UITextField+Extensions.h"
#import "Common.h"

#import "AppData.h"
#import "PolicyViewController.h"
#import "TermsOfUseViewController.h"
#import "CreateProfileViewController.h"
#import "HomeViewController.h"
#import "UtilStyle.h"
#import "UIView+Extension.h"
#import "EnableSubmitBINButton.h"
#import "WSB2CVerifyBIN.h"
#import "BINItem.h"
#import "WSB2CGetRequestToken.h"
#import "WSB2CGetUserDetails.h"
#import "WSB2CGetAccessToken.h"
#import "WSB2CGetRequestToken.h"

@interface SignInViewController ()<DataLoadDelegate, UITextFieldDelegate,UITextViewDelegate,EnableSubmitBINButton>
{
    CGFloat backupBottomConstraint;
    SexyTooltip *greetingsTooltip;
    UIImageView *asteriskImageView;
    BOOL isReadTerm;
    BOOL isReadPolicy;
    WSB2CGetRequestToken *wsRequestToken;
    UILabel *termOfUseLbl;
    UILabel *policyLbl;
}
@end

@implementation SignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.scrollView setBackgroundColorForView];
    [self.mainView setBackgroundColorForView];
    [self setTextStyleForView];

    self.binNumberText.delegate = self;
    self.binNumberText.returnKeyType = UIReturnKeyDone;
    self.binNumberText.keyboardType = UIKeyboardTypeNumberPad;
    [self.binNumberText addTarget:self
                           action:@selector(textFieldDidChange)
        forControlEvents:UIControlEventEditingChanged];

    UITapGestureRecognizer *tappedOutsideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tappedOutsideKeyboard];
    
    
    if(self.isBINInvalid)
    {
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = nil;
        alert.msgAlert =@"We’re sorry, but your issuing bank has discontinued subscription of your card type to Mastercard Concierge. Click APPLY below to apply for an eligible card. To try again, click Cancel.";
        alert.firstBtnTitle = NSLocalizedString(@"apply_button", nil);
        alert.secondBtnTitle = NSLocalizedString(@"arlet_cancel_button", nil);
        alert.blockFirstBtnAction = ^(void){
            NSURL *url = [NSURL URLWithString:MASTERCARD_REGISTER_URL];
            [[UIApplication sharedApplication] openURL:url];
        };

        [self showAlert:alert forNavigation:NO];
    }
    [self initRightViewForTextField];
    [self hideAsteriskLabel];
    [self createBINTextFieldSexyTooltip];
    [greetingsTooltip dismiss];
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //    For Next Sprint
    [self.view layoutIfNeeded];
    if ([self.confirmView.subviews count] <= 0) {
        [self buildAgreeTextViewFromString:NSLocalizedString(@"bin_confirm_msg",nil)];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWill:) name:UIKeyboardWillHideNotification object:nil];
    
    
    [self.checkBoxButton setImage:[UIImage imageNamed:@"checkbox_uncheck"] forState:UIControlStateNormal];
    [self.checkBoxButton setImage:[UIImage imageNamed:@"checkbox_check"] forState:UIControlStateSelected];
    [self.checkBoxButton addTarget:self action:@selector(enableSubmitButton:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setTextStyleForView
{
    // Text Style F\or Greet Message
    NSString *greetMsg = NSLocalizedString(@"signin_greeting_message", nil);
    NSRange range = NSMakeRange(0, greetMsg.length);
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:greetMsg];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionTextSize]] range:range];
    [attributeString addAttribute:NSForegroundColorAttributeName value:[AppColor activeTextColor] range:range];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init] ;
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    paragraphStyle.lineSpacing = 1.25;
    [attributeString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];

    self.greetingLabel.attributedText = attributeString;
    
    // Text Style For Title
    greetMsg = NSLocalizedString(@"concierge", nil);
    self.titleViewController.attributedText = [UtilStyle setTextStyleForTitleViewControllerWithMessage:greetMsg];
    
    // Text Style For BIN TextField
    [self.binNumberText setFont:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]]];
    [self.binNumberText setTextColor:[AppColor textColor]];
    
    self.binNumberText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.binNumberText.placeholder];
    
    // Text Style For Submit Button
    [self.submitButton setTitle:NSLocalizedString(@"submit_button_title", nil) forState:UIControlStateNormal];
    [self.submitButton setBackgroundColorForNormalStatus];
    [self.submitButton setBackgroundColorForTouchingStatus];
    [self.submitButton configureDecorationForButton];
    [self.submitButton addTarget:self action:@selector(submitTapped:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)initRightViewForTextField{
    asteriskImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"exclamation_icon"]];
    asteriskImageView.frame = CGRectMake(0.0f, 0.0f, 20.0f*SCREEN_SCALE, 20.0f*SCREEN_SCALE);
    asteriskImageView.contentMode = UIViewContentModeCenter;
    
    [self.binNumberText setRightView:asteriskImageView];
    [self.binNumberText setRightViewMode:UITextFieldViewModeAlways];
}

- (void) showAsteriskLabel{
    asteriskImageView.hidden = NO;
}

- (void) hideAsteriskLabel{
    asteriskImageView.hidden = YES;
}

-(void) createBINTextFieldSexyTooltip{
    UIColor *color = [AppColor activeTextColor];
    UIFont *font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionTextSize]];
    NSString *string = NSLocalizedString(@"bin_error_tooltip", nil);
    NSDictionary *attrs = @{ NSForegroundColorAttributeName : color,
                             NSFontAttributeName: font};
    NSAttributedString *errString = [[NSAttributedString alloc] initWithString:string attributes:attrs];
    
    greetingsTooltip = [[SexyTooltip alloc] initWithAttributedString:errString
                                                         sizedToView:self.view
                                                         withPadding:UIEdgeInsetsMake(10, 5, 10, 5)
                                                           andMargin:UIEdgeInsetsMake(20, 20, 20, 20)];
    greetingsTooltip.permittedArrowDirections = @[@(SexyTooltipArrowDirectionUp)];
    greetingsTooltip.color = [UIColor blackColor];
    //    greetingsTooltip.borderColor = colorFromHexString(@"#FFFFFF");
    [self.view addSubview:greetingsTooltip];
}

- (void) showTooltip{
    [greetingsTooltip presentFromView:asteriskImageView
                               inView:self.view
                           withMargin:10
                             animated:NO];
}

-(void) enableSubmitButton:(UIButton *)button
{
    if([self.binNumberText.text isValidBinNumber])
    {
        [button setHighlighted:NO];
        self.checkBoxButton.selected = !(button.selected);
        [button setBackgroundColor:[UIColor clearColor]];
        [self.submitButton setEnabled:button.selected];
    }
}

- (void)submitTapped:(id)sender {
    [self dismissKeyboard];
    if(![self.binNumberText.text isValidBinNumber])
    {
        if ([self.binNumberText.text length] < 7) {
            [self showAsteriskLabel];
            [self showTooltip];
        }else{
            [self showAlertForMoreInformationWithURL:[NSURL URLWithString:MASTERCARD_REGISTER_URL]];
        }
        return;
    }
    
    [self startActivityIndicator];
    WSB2CVerifyBIN *ws = [[WSB2CVerifyBIN alloc] init];
    ws.bin = [self.binNumberText.text stringByReplacingOccurrencesOfString:@"-" withString:@""];
    ws.delegate = self;
    ws.task = WS_B2C_VERIFY_BIN;
    [ws verifyBIN];
}


-(void) showAlertForMoreInformationWithURL:(NSURL*)url
{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = nil;
    alert.msgAlert = NSLocalizedString(@"create_mastercard_account_message",nil);
    alert.firstBtnTitle = NSLocalizedString(@"apply_button", nil);
    alert.blockFirstBtnAction = ^(void){
        [[UIApplication sharedApplication] openURL:url];
    };
    
    alert.blockSecondBtnAction  = ^(void){};
    
    [self showAlert:alert forNavigation:NO];
}
-(void)dismissKeyboard
{
    [self.binNumberText resignFirstResponder];
}

#pragma mark ENABLE SUBMIT BUTTON DELEGATE
-(void)enableSubmitBINButton:(NSString *)currentViewController
{
    if([currentViewController isEqualToString:@"TermsOfUseViewController"])
    {
        isReadTerm = YES;
    }
    else if([currentViewController isEqualToString:@"PolicyViewController"])
    {
        isReadPolicy = YES;
    }
    
    if(isReadTerm && isReadPolicy)
    {
        self.checkBoxButton.selected = YES;
        [self.checkBoxButton setBackgroundColor:[UIColor clearColor]];
        [self.submitButton setEnabled:self.checkBoxButton.selected];
    }
}

#pragma mark CONFIRM MESSAGE PROCESS

- (void)buildAgreeTextViewFromString:(NSString *)localizedString
{
    // 1. Split the localized string on the # sign:
    NSArray *localizedStringPieces = [localizedString componentsSeparatedByString:@"#"];
    self.confirmView.userInteractionEnabled = YES;
    // 2. Loop through all the pieces:
    NSUInteger msgChunkCount = localizedStringPieces ? localizedStringPieces.count : 0;
    CGPoint wordLocation = CGPointMake(0.0, 0.0);
    for (NSUInteger i = 0; i < msgChunkCount; i++)
    {
        NSString *chunk = [localizedStringPieces objectAtIndex:i];
        if ([chunk isEqualToString:@""])
        {
            continue;     // skip this loop if the chunk is empty
        }
        
        // 3. Determine what type of word this is:
        BOOL isTermsOfServiceLink = [chunk hasPrefix:@"<ts>"];
        BOOL isPrivacyPolicyLink  = [chunk hasPrefix:@"<pp>"];
        BOOL isLink = (BOOL)(isTermsOfServiceLink || isPrivacyPolicyLink);
        
        // 4. Create label, styling dependent on whether it's a link:
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionTextSize]];
        
        label.text = chunk;
        label.userInteractionEnabled = isLink;
        
        if (isLink)
        {
            // 5. Set tap gesture for this clickable text:
            SEL selectorAction = isTermsOfServiceLink ? @selector(tapOnTermsOfServiceLink:) : @selector(tapOnPrivacyPolicyLink:);
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                         action:selectorAction];
            [label addGestureRecognizer:tapGesture];
            
            // Trim the markup characters from the label:
            if (isTermsOfServiceLink)
            {
                label.text = [label.text stringByReplacingOccurrencesOfString:@"<ts>" withString:@""];
                termOfUseLbl = label;
            }
            
            
            if (isPrivacyPolicyLink)
            {
                label.text = [label.text stringByReplacingOccurrencesOfString:@"<pp>" withString:@""];
                policyLbl = label;
            }
            
            
            NSArray * objects = [[NSArray alloc] initWithObjects:colorFromHexStringWithAlpha(LABEL_COLOR_EXPLORE_NAME, 1.0), [NSNumber numberWithInt:NSUnderlineStyleSingle], nil];
            NSArray * keys = [[NSArray alloc] initWithObjects:NSForegroundColorAttributeName, NSUnderlineStyleAttributeName, nil];
            
            NSDictionary * linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
            
            NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:label.text attributes:linkAttributes];
            
            [label setAttributedText:attributedString];

        }
        else
        {
            label.textColor = colorFromHexString(LABEL_COLOR_DETAIL);
        }
        
        // 6. Lay out the labels so it forms a complete sentence again:
        
        // If this word doesn't fit at end of this line, then move it to the next
        // line and make sure any leading spaces are stripped off so it aligns nicely:
        
        [label sizeToFit];
        
        if (self.confirmView.frame.size.width < wordLocation.x + label.bounds.size.width)
        {
            wordLocation.x = 0.0;                       // move this word all the way to the left...
            wordLocation.y += label.frame.size.height;  // ...on the next line
            
            // And trim of any leading white space:
            NSRange startingWhiteSpaceRange = [label.text rangeOfString:@"^\\s*"
                                                                options:NSRegularExpressionSearch];
            if (startingWhiteSpaceRange.location == 0)
            {
                label.text = [label.text stringByReplacingCharactersInRange:startingWhiteSpaceRange
                                                                 withString:@""];
                [label sizeToFit];
            }
        }
        
        // Set the location for this label:
        label.frame = CGRectMake(wordLocation.x,
                                 wordLocation.y,
                                 label.frame.size.width,
                                 label.frame.size.height);
        
        // Show this label:
        [self.confirmView addSubview:label];
        
        // Update the horizontal position for the next word:
        wordLocation.x += label.frame.size.width;
    }
}

- (void)tapOnTermsOfServiceLink:(UITapGestureRecognizer *)tapGesture
{
    if (tapGesture.state == UIGestureRecognizerStateEnded)
    {
        TermsOfUseViewController *vc = [[TermsOfUseViewController alloc] init];
        vc.delegate = self;
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        [self.navigationController pushViewController:vc animated:NO];
    }
}


- (void)tapOnPrivacyPolicyLink:(UITapGestureRecognizer *)tapGesture
{
    if (tapGesture.state == UIGestureRecognizerStateEnded)
    {
        PolicyViewController *vc = [[PolicyViewController alloc] init];
        vc.delegate = self;
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        [self.navigationController pushViewController:vc animated:NO];
    }
}


#pragma mark HANDLE KEYBOARD
-(void)handleKeyboardWillShow:(NSNotification *)paramNotification
{
    backupBottomConstraint = self.submitButtonBottomConstraint.constant;
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         self.scrollView.contentInset = UIEdgeInsetsMake(-70, 0, 0, 0);
                         self.submitButtonBottomConstraint.constant = keyboardRect.size.height + MARGIN_KEYBOARD;
                         if (greetingsTooltip.isShowing == YES) {
                             [self showTooltip];
                         }
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

-(void)handleKeyboardWill:(NSNotification *)paramNotification
{
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
                         self.submitButtonBottomConstraint.constant =  backupBottomConstraint;
                         if (greetingsTooltip.isShowing == YES) {
                             [self showTooltip];
                         }
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
    
}


# pragma mark DATA LOAD DELEGATE
-(void)loadDataDoneFrom:(WSBase *)ws
{
    
    if(ws.task == WS_B2C_VERIFY_BIN)
    {
        if(ws.data.count > 0)
        {
            BINItem *item = (BINItem *)[ws.data objectAtIndex:0];
            if(item.valid)
            {
                [[NSUserDefaults standardUserDefaults] setValue:self.binNumberText.text forKey:VERIFIED_BIN];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                // storeBinNumber
                [[SessionData shareSessiondata] setBINNumber:[self.binNumberText.text stringByReplacingOccurrencesOfString:@"-" withString:@""]];
                //
                if([AppData isCreatedProfile])
                {
                    [self getRequestToken];
                }
                else
                {
                    [self stopActivityIndicator];
                    SWRevealViewController *revealViewController = self.revealViewController;
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    CreateProfileViewController *createProfileVC = [storyboard instantiateViewControllerWithIdentifier:@"CreateProfileViewController"];
                    [revealViewController pushFrontViewController:createProfileVC animated:YES];
                }
            }
            else{
                [self stopActivityIndicator];
                [self showAlertForMoreInformationWithURL:[NSURL URLWithString:MASTERCARD_REGISTER_URL]];
            }
        }
    }
    else if (ws.task == WS_GET_REQUEST_TOKEN) {
        WSB2CGetAccessToken *wsAccessToken = [[WSB2CGetAccessToken alloc] init];
        wsAccessToken.delegate = self;
        [[SessionData shareSessiondata] setRequestToken:wsRequestToken.requestToken];
        [wsAccessToken requestAccessToken:wsRequestToken.requestToken member:[[SessionData shareSessiondata] OnlineMemberID]];
    }
    
    else if(ws.task == WS_GET_ACCESS_TOKEN){
        // get user detail
        WSB2CGetUserDetails *wsGetUser = [[WSB2CGetUserDetails alloc] init];
        wsGetUser.delegate = self;
        [wsGetUser getUserDetails];
    }
    else if(ws.task == WS_B2C_GET_USER_DETAILS)
    {
        [self stopActivityIndicator];
        HomeViewController *homeViewConroller = [[HomeViewController alloc] init];
        [self.navigationController setNavigationBarHidden:NO animated:YES];
        [self.navigationController pushViewController:homeViewConroller animated:YES];
    }
}

-(void) loadDataFailFrom:(id<BaseResponseObjectProtocol>)result withErrorCode:(NSInteger)errorCode
{
    [self stopActivityIndicator];
    
}

-(void)loadDataFailFrom:(id<WSBaseProtocol>)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message
{
    [self stopActivityIndicator];
    
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);;
    alert.msgAlert = message;
    alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        alert.seconBtn.hidden = YES;
        alert.midView.alpha = 0.0f;;
    });
    
    [self showAlert:alert forNavigation:YES];
}

- (void) getRequestToken{
    wsRequestToken = [[WSB2CGetRequestToken alloc] init];
    wsRequestToken.delegate = self;
    wsRequestToken.task = WS_GET_REQUEST_TOKEN;
    [wsRequestToken getRequestToken];
}


#pragma mark TEXT FIELD DELEGATE
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([textField.text length] <= 6) {
    }
}


- (void)textFieldDidChange{
    if ([self.binNumberText.text length] > 4 && ![self.binNumberText.text containsString:@"-"]) {
        NSMutableString *newString = [[NSMutableString alloc] initWithString:self.binNumberText.text];
        [newString insertString:@"-" atIndex:4];
        self.binNumberText.text = newString;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(string.length > 1)
    {
        return NO;
    }
    else
    {
        if(textField.text.length == 7 && ![string isEqualToString:@""])
        {
            return NO;
        }
        
        if(range.location < textField.text.length)
        {
            textField.text = [self reformatForBINTextWithNewString:string WithRange:range];
            NSRange newRange;
            if([string isEqualToString:@""])
            {
                newRange = (textField.text.length < range.location) ? NSMakeRange(textField.text.length, 0) : NSMakeRange(range.location, 0);
            }
            else
            {
                newRange = (range.location == 4) ? NSMakeRange(range.location + 2, 0) : NSMakeRange(range.location + 1, 0);
            }
            [textField updateCursorPositionAtRange:newRange];
            return NO;
        }
        
        BOOL isPressedBackspaceAfterSingleSpaceSymbol = [string isEqualToString:@""] && range.location == 4 && range.length == 1;
        if (isPressedBackspaceAfterSingleSpaceSymbol) {
            //  your actions for deleteBackward actions
            textField.text = [textField.text stringByReplacingCharactersInRange:range withString:@""];
            range.location = range.location - 1;
        }
        //limit check lenght of string
        NSString *resultText = [textField.text stringByReplacingCharactersInRange:range
                                                                       withString:string];
        if (resultText.length > 0) {
            [self hideAsteriskLabel];
            [greetingsTooltip dismiss];
        }
        return resultText.length <= 7;
    }
}

-(NSString *)reformatForBINTextWithNewString:(NSString *)newString WithRange:(NSRange)range
{
    NSMutableString *mutableString = [[NSMutableString alloc] initWithString:self.binNumberText.text];
    NSRange newRange = range;
    if([newString isEqualToString:@""] && range.location == 4)
    {
        newRange = NSMakeRange(range.location - 1, 1);
    }
    [mutableString replaceCharactersInRange:newRange withString:newString];
    mutableString = [[NSMutableString alloc] initWithString:[mutableString stringByReplacingOccurrencesOfString:@"-" withString:@""]];
    if(mutableString.length > 4)
    {
        [mutableString insertString:@"-" atIndex:4];
    }
    
    return mutableString;
}

-(void)enableTermOfUseAndPrivacyPolicy
{
    if([self.binNumberText.text isValidBinNumber])
    {
        termOfUseLbl.textColor = colorFromHexString(LABEL_COLOR_EXPLORE_NAME);
        policyLbl.textColor = colorFromHexString(LABEL_COLOR_EXPLORE_NAME);
    }
    else
    {
        termOfUseLbl.textColor = colorFromHexString(LABEL_COLOR_DETAIL_WITH_LINK);
        policyLbl.textColor = colorFromHexString(LABEL_COLOR_DETAIL_WITH_LINK);
    }
}
@end
