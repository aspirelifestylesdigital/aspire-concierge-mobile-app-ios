//
//  UDAForgotPasswordViewController.m
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 7/19/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "UDAForgotPasswordViewController.h"
#import "SWRevealViewController.h"
#import "UITextField+Extensions.h"
#import "NSString+Utis.h"
#import "UIButton+Extension.h"
#import "UtilStyle.h"
#import "WSForgotPassword.h"
#import "UDASignInViewController.h"
#import <AspireApiFramework/AspireApiFramework.h>
@interface UDAForgotPasswordViewController ()<DataLoadDelegate, UITextFieldDelegate> {
    BOOL isInputEmail;
}
@property (nonatomic, strong) WSForgotPassword *wsForgotPassword;

@end

@implementation UDAForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *tappedOutsideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tappedOutsideKeyboard];
    [self createBackButton];
    [self setUIStyte];
    [self trackingScreenByName:@"Forgot password"];
    
    _titleLabel.text = NSLocalizedString(@"We will send you an email with your new password.", nil);
    [_submitButton setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setUpCustomizedPanGesturePopRecognizer];
    
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)createBackButton{
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 20, 30, 30)];
    [menuButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [menuButton setBackgroundImage:[UIImage imageNamed:@"back_white_icon"] forState:UIControlStateNormal];
    [menuButton setBackgroundImage:[UIImage imageNamed:@"back_interaction_icon"] forState:UIControlStateHighlighted];
    [self.view addSubview:menuButton];
}

- (void)backAction {
    [self updateSuccessRedirect];
}
- (void)resignFirstResponderForAllTextField {
    if(self.emailTextField.isFirstResponder){
        [self.emailTextField resignFirstResponder];
    }
}

-(void)dismissKeyboard {
    [self resignFirstResponderForAllTextField];
}

-(void)verifyData{
    
    NSMutableString *message = [[NSMutableString alloc] init];
    if(self.emailTextField.text.length == 0){
        [message appendString:@"* "];
        [message appendString:NSLocalizedString(@"Enter a valid email address.", nil)];
    }else {
        if([self verifyValueForTextField:self.emailTextField].length > 0) {
            [message appendString:@"* "];
            [message appendString:[self verifyValueForTextField:self.emailTextField]];
        }
    }
    [self showError:message];
}

-(void)showError:(NSString *)message {
    if(message.length > 0){
        
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"Please confirm that the value entered is correct:", nil);
        alert.msgAlert = message;
        alert.firstBtnTitle = NSLocalizedString(@"OK", nil);
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
            alert.lblAlertMessage.textAlignment = NSTextAlignmentLeft;
            [alert.view layoutIfNeeded];
        });
        
        alert.blockFirstBtnAction = ^(void){
            [self makeBecomeFirstResponderForTextField];
        };
        
        [self showAlert:alert forNavigation:NO];
    }
    else{
        if(!isNetworkAvailable()) {
            [self showErrorNetwork];
            
        }else{
            [self resignFirstResponderForAllTextField];
            [self setTextViewsDefaultBottomBolder];
            [self submitForgorPasswordWithEmail:self.emailTextField.text];
        }
    }
}

-(void)setTextViewsDefaultBottomBolder {
    [self.emailTextField setBottomBolderDefaultColor];
}

-(void) makeBecomeFirstResponderForTextField {
    [self resignFirstResponderForAllTextField];
    if(![self.emailTextField.text isValidEmail]){
        [self.emailTextField becomeFirstResponder];
    }
}

-(NSString*)verifyValueForTextField:(UITextField *)textFied{
    
    NSString *errorMsg = @"";
    if(textFied == self.emailTextField && ![(textFied.isFirstResponder ? textFied.text :self.emailTextField.text) isValidEmail]){
        errorMsg = NSLocalizedString(@"Enter a valid email address.", nil);
        [textFied setBottomBorderRedColor];
    }else{
        [textFied setBottomBorderRedColor];
    }
    
    return errorMsg;
}

- (void)submitForgorPasswordWithEmail:(NSString*)email {
        [self startActivityIndicator];
    __weak typeof (self) _self = self;
    [ModelAspireApiManager resetPasswordForEmail:email completion:^(NSError *error) {
        [_self startActivityIndicator];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error) {
                [_self forgotPasswordSuccessAlert];
            }else{
                switch ([AspireApiError getErrorTypeAPIResetPasswordFromError:error]) {
                    case RESETPW_ERR:
                        [_self showAPIErrorAlertWithMessage: NSLocalizedString(@"Enter a valid email address.", nil)];
                        break;
                    case UNKNOWN_ERR:
                        [_self showAPIErrorAlertWithMessage:NSLocalizedString(@"An error has occurred. Check your internet settings or try again.", nil) andTitle:NSLocalizedString(@"Cannot Get Data", nil)];
                        break;
                    case NETWORK_UNAVAILABLE_ERR:
                        [_self showErrorNetwork];
                        break;
                    default:
                        [_self showAPIErrorAlertWithMessage:NSLocalizedString(@"An error has occurred. Check your internet settings or try again.", nil) andTitle:NSLocalizedString(@"Cannot Get Data", nil)];
                        break;
                }
                
            }
        });
    }];
    return;
        NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
        [dataDict setValue:B2C_ConsumerKey forKey:@"ConsumerKey"];
        [dataDict setValue:@"ForgotPassword" forKey:@"Functionality"];
        [dataDict setValue:email forKey:@"Email"];
    
        self.wsForgotPassword = [[WSForgotPassword alloc] init];
        self.wsForgotPassword.delegate = self;
        [self.wsForgotPassword submitForgotPassword:dataDict];
}

- (void) setUIStyte {
    
    isInputEmail = NO;
    [self setStatusSubmitButton];
    self.emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    self.emailTextField.delegate = self;
    self.emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    [self createAsteriskForTextField:self.emailTextField];
    
    self.emailTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.emailTextField.text];
    self.emailTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.emailTextField.placeholder];
    
    [self.buttonBackgroundView setBackgroundColor:[AppColor backgroundColor]];
    
    [self.submitButton setBackgroundColorForNormalStatus];
    [self.submitButton setBackgroundColorForTouchingStatus];
    [self.submitButton configureDecorationForButton];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.topLayoutConstraint.constant = (IPAD) ? 280.0f : 200.0f;
    });

}

-(void) createAsteriskForTextField:(UITextField *)textField
{
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 8, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [textField setLeftView:leftView];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    
}
- (void)setStatusSubmitButton{
    self.submitButton.enabled = isInputEmail;
}
#pragma mark TEXT FIELD DELEGATE

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *inputString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (textField == self.emailTextField) {
        isInputEmail = inputString.length > 0;
    }

    [self setStatusSubmitButton];
    
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.text = [textField.text removeRedudantWhiteSpaceInText];
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == self.emailTextField) {
        self.emailTextField.text = textField.text;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if(textField == self.emailTextField){
        textField.text = self.emailTextField.text;
    }
}

- (void) forgotPasswordSuccessAlert{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.msgAlert = NSLocalizedString(@"Please check your email for your new password.", nil);
    alert.firstBtnTitle = NSLocalizedString(@"OK", nil);
    alert.blockFirstBtnAction = ^{
        [self updateSuccessRedirect];
    };
    dispatch_async(dispatch_get_main_queue(), ^{
        alert.seconBtn.hidden = YES;
        alert.midView.alpha = 0.0f;;
    });
    
    [self showAlert:alert forNavigation:NO];
}

- (void)showAPIErrorAlertWithMessage:(NSString *)message{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"Please confirm that the value entered is correct:", nil);
    alert.msgAlert = [NSString stringWithFormat:@"* %@",message];
    alert.firstBtnTitle = NSLocalizedString(@"OK", nil);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        alert.seconBtn.hidden = YES;
        alert.midView.alpha = 0.0f;;
        alert.lblAlertMessage.textAlignment = NSTextAlignmentLeft;
    });
    
    [self showAlert:alert forNavigation:NO];
}

#pragma mark - Delegate from API

-(void)loadDataDoneFrom:(WSBase*)ws
{
    [self stopActivityIndicator];
    if (((WSForgotPassword*)ws).isSuccesful) {
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.msgAlert = NSLocalizedString(@"Please check your email for your new password.", nil);
        alert.firstBtnTitle = NSLocalizedString(@"OK", nil);
        alert.blockFirstBtnAction = ^{
            [self updateSuccessRedirect];
        };
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
        });
        
        [self showAlert:alert forNavigation:NO];
    }
    
}
- (void)updateSuccessRedirect{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)loadDataFailFrom:(WSBase *)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message {
    
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }else{
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"Please confirm that the value entered is correct:", nil);
        alert.msgAlert = [NSString stringWithFormat:@"* %@",NSLocalizedString(@"Enter a valid email address.", nil)];
        alert.firstBtnTitle = NSLocalizedString(@"OK", nil);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
            alert.lblAlertMessage.textAlignment = NSTextAlignmentLeft;
        });
        
        [self showAlert:alert forNavigation:NO];
    }

}
#pragma mark - Action
- (IBAction)ForgotPasswordAction:(id)sender {
    [self verifyData];
}

@end
