//
//  AskConciergeViewController.m
//  MobileConcierge
//
//  Created by user on 5/9/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "AskConciergeViewController.h"
#import "Constant.h"
#import "UIButton+Extension.h"
#import "Constant.h"
#import "Common.h"
#import "WSB2CCreateConciergeCase.h"
#import "SWRevealViewController.h"
#import "HomeViewController.h"
#import "AskConciergeConfirmationViewController.h"
#import<CoreTelephony/CTTelephonyNetworkInfo.h>
#import<CoreTelephony/CTCarrier.h>
#import "AppDelegate.h"
#import <AspireApiFramework/AspireApiFramework.h>

@interface AskConciergeViewController ()<UITextViewDelegate, DataLoadDelegate>

@end

@implementation AskConciergeViewController
{
    UILabel *lblPlaceholder;
    float backupHeightContentView;
    
    BOOL checkPhone;
    BOOL checkMail;
    BOOL viewControllerIsPopped;
    int timeToChange;
    
    AppDelegate* appdelegate;
}

- (void)viewDidLoad {
    isNotAskConciergeBarButton = YES;
    [super viewDidLoad];
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"REQUEST", nil)];
    [self createBackBarButton];
    [self trackingScreenByName:@"Ask concierge"];
    appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
}


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self removeSetupForCustomizedPanGesturePopRecognizer];
    
    [self dismissKeyboard];
    
}


- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setUpCustomizedPanGesturePopRecognizer];
    if (viewControllerIsPopped == YES)
    {
        //[self.tvAskConcierge becomeFirstResponder];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}
- (void)touchBack{
    AlertViewController *alert = [[AlertViewController alloc] init];
    //alert.titleAlert = NSLocalizedString(@"ERROR!", nil);
    alert.msgAlert = NSLocalizedString(@"You have not submitted your request, click \"OK\" to continue or \"CANCEL\" to submit your request.", nil);
    alert.firstBtnTitle = NSLocalizedString(@"YES", nil).uppercaseString;
    alert.blockFirstBtnAction = ^(void){
        [self.navigationController popViewControllerAnimated:YES];
    };
    alert.secondBtnTitle = NSLocalizedString(@"NO", nil).uppercaseString;
    [self showAlert:alert forNavigation:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initView{
    [self scaleConstraintForSubView];
    [self.viewAccessories setBackgroundColor:[AppColor backgroundColor]];
    [self.btnCallConcierge configureDecorationBlackTitleButtonForTouchingStatus];
    [self.btnCallConcierge configureDecorationBlackTitleButton];
    [self.btnCallConcierge setTitle:@"(800) 373-4830" forState:UIControlStateNormal];
    
    [self.btnAskConcierge configureDecorationBlackTitleButtonForTouchingStatus];
    [self.btnAskConcierge setImage:[UIImage imageNamed:@"send_filled_icon"] forState:UIControlStateNormal];
    [self.btnAskConcierge setImage:[UIImage imageNamed:@"send_interact_icon"] forState:UIControlStateHighlighted];
    [self addAskConciergeTextViewPlaceHolder];
    if (self.itemName) {
        self.tvAskConcierge.text = [NSString stringWithFormat:@"%@\n",self.itemName];
        lblPlaceholder.hidden = YES;
        self.btnAskConcierge.hidden = NO;
        [self showAccessoryView];
    }else{
        self.btnAskConcierge.hidden = YES;
        [self hideAccessoryView];
    }
    
    UITapGestureRecognizer *dismiss = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:dismiss];
    
    self.lblMessage.attributedText = letterSpacing(1.6, NSLocalizedString(@"OR CALL THE CONCIERGE", nil));
    
    UIFont *font = [UIFont fontWithName:FONT_MarkForMC_BOLD size:[AppSize descriptionTextSize]];
    [self.lblRespondToMe setFont:font];
    self.lblRespondToMe.attributedText = letterSpacing(1.6, NSLocalizedString(@"RESPOND TO ME BY", nil));
    
    self.tvAskConcierge.delegate = self;
    
    
    backupHeightContentView = self.heightConstraintAskConciergeView.constant;
    
//    [self setCheckButtonsStateWithPhone:NO andEmail:NO];
   
        [self.btnCheckPhone setAttributedTitle:[self stringWithImageName:@"checkbox_uncheck" andString:NSLocalizedString(@"Phone", nil)] forState:UIControlStateNormal];
  
        [self.btnCheckPhone setAttributedTitle:[self stringWithImageName:@"checkbox_check" andString:NSLocalizedString(@"Phone", nil)] forState:UIControlStateSelected];
 
        [self.btnCheckMail setAttributedTitle:[self stringWithImageName:@"checkbox_uncheck" andString:NSLocalizedString(@"Email", nil)] forState:UIControlStateNormal];

        [self.btnCheckMail setAttributedTitle:[self stringWithImageName:@"checkbox_check" andString:NSLocalizedString(@"Email", nil)] forState:UIControlStateSelected];
    
    if ([self.tvAskConcierge hasText]) {
        
    }
    
//    _lblPhoneNumber.text = NSLocalizedString(@"International: +1 (703) 891-4643", nil);
    _lblPhoneDescription.text = NSLocalizedString(@"Roaming charges may apply", nil);

}


- (void)initData{
    
}


- (void)scaleConstraintForSubView{
    self.topConstraintAskConciergeView.constant = self.topConstraintAskConciergeView.constant*SCREEN_SCALE;
    self.topMessageLabel.constant = self.topMessageLabel.constant*SCREEN_SCALE;
    self.topConstrainAskConciergeTextView.constant = self.topConstrainAskConciergeTextView.constant*SCREEN_SCALE;
    self.heightConstraintAskConciergeView.constant = self.heightConstraintAskConciergeView.constant*SCREEN_SCALE;
    self.heightConstraintViewCallConcierge.constant = self.heightConstraintViewCallConcierge.constant*SCREEN_SCALE;
    
    self.leadConstraintAskConciergeTextView.constant = self.leadConstraintAskConciergeTextView.constant*SCREEN_SCALE;
    self.trailConstraintAskConciergeTextView.constant = self.trailConstraintAskConciergeTextView.constant*SCREEN_SCALE;
}

-(void)addAskConciergeTextViewPlaceHolder{
    lblPlaceholder = [[UILabel alloc] initWithFrame:CGRectMake(2.0, 2.0,self.tvAskConcierge.frame.size.width, (IPAD)?100.0:80.0)];
    [lblPlaceholder setText:NSLocalizedString(@"What can we do for you today?", nil)];
    lblPlaceholder.lineBreakMode = NSLineBreakByWordWrapping;
    lblPlaceholder.numberOfLines = 0;
    [lblPlaceholder setBackgroundColor:[UIColor clearColor]];
    [lblPlaceholder setTextColor:[AppColor placeholderTextColor]];
    lblPlaceholder.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR_It size:24*SCREEN_SCALE];
    dispatch_async(dispatch_get_main_queue(), ^{
        [lblPlaceholder setFrame:CGRectMake(2.0, 2.0,self.tvAskConcierge.frame.size.width, (IPAD)?100.0:80.0)];
        [lblPlaceholder sizeToFit];
    });
    [self.tvAskConcierge addSubview:lblPlaceholder];
}


- (void) showAccessoryView{
    if (self.viewAccessories.hidden == YES) {
        self.viewAccessories.alpha = 0;
        self.viewAccessories.hidden = NO;
        [UIView animateWithDuration:0.3 animations:^{
            self.viewAccessories.alpha = 1;
        }];
    }
}
- (void) hideAccessoryView{
    if (self.viewAccessories.hidden == NO){
        [UIView animateWithDuration:0.3 animations:^{
            self.viewAccessories.alpha = 0;
        } completion: ^(BOOL finished) {//creates a variable (BOOL) called "finished" that is set to *YES* when animation IS completed.
            self.viewAccessories.hidden = finished;//if animation is finished ("finished" == *YES*), then hidden = "finished" ... (aka hidden = *YES*)
        }];
    }
}

- (void)dismissKeyboard{
    [self.tvAskConcierge resignFirstResponder];
}

- (NSAttributedString*) stringWithImageName:(NSString*)imageName andString:(NSString*)string{
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    attachment.image = [UIImage imageNamed:imageName];
    attachment.bounds = CGRectMake(0, -2*SCREEN_SCALE, 18*SCREEN_SCALE, 18*SCREEN_SCALE);
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    
    NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithAttributedString:attachmentString];
    
    
    [myString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"   %@", string]]];
    [myString addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize titleTextSize]] range:NSMakeRange(4, string.length)];
    [myString addAttribute:NSForegroundColorAttributeName value:[AppColor normalTextColor] range:NSMakeRange(4, string.length)];
    
    return myString;
}

- (void) expandTextView{
    [UIView animateWithDuration:0.2 animations:^{
//        self.heightConstraintAskConciergeView.constant = backupHeightContentView +  100*SCREEN_SCALE_BY_HEIGHT;
        self.heightConstraintAskConciergeView.constant = backupHeightContentView;
        [self.view layoutIfNeeded];
    }];
}

- (void) collapseTextView{
    [UIView animateWithDuration:0.2 animations:^{
        self.heightConstraintAskConciergeView.constant = backupHeightContentView;
        [self.view layoutIfNeeded];
    }];
}

- (IBAction)touchAskConcierge:(id)sender {
    
    [self.tvAskConcierge endEditing:YES];
    if (self.btnCheckPhone.selected == NO && self.btnCheckMail.selected == NO) {
        [self stopActivityIndicator];
        AlertViewController *alert = [[AlertViewController alloc] init];
        //alert.titleAlert = NSLocalizedString(@"ERROR!", nil);
        alert.msgAlert = NSLocalizedString(@"Please select at least one response option to place a concierge request.", nil);;
        alert.firstBtnTitle = NSLocalizedString(@"OK", nil);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
        });
        
        [self showAlert:alert forNavigation:NO];
    }else{
        [self startActivityIndicator];
        
        WSB2CCreateConciergeCase *wSB2CCreateConciergeCase = [[WSB2CCreateConciergeCase alloc] init];
        wSB2CCreateConciergeCase.delegate = self;
        if (!self.categoryName) {
            self.categoryName = @"";
        }
        if (!self.cityName) {
            self.cityName = @"";
        }
        
        
        UserObject *userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
        
        NSString *requestDetails = [NSString stringWithFormat:@"%@%@\n%@",
                                    [self.cityName isEqualToString:@""] ? @"" : [NSString stringWithFormat:@"\n%@", self.cityName],
                                    [self.categoryName isEqualToString:@""] ? @"" : [NSString stringWithFormat:@"\n%@", self.categoryName],
                                    self.tvAskConcierge.text];
        NSString *responseString = @"";
        
        if (self.btnCheckMail.selected == YES && self.btnCheckPhone.selected == YES) {
            responseString = [responseString stringByAppendingString:[NSString stringWithFormat:@"Respond by %@ or Respond by %@\n\n",userObject.mobileNumber,userObject.email]];
        } else {
            if (self.btnCheckPhone.selected == YES) {
                responseString = [responseString stringByAppendingString:[NSString stringWithFormat:@"Respond by %@\n\n",userObject.mobileNumber]];
            }else if (self.btnCheckMail.selected == YES) {
                responseString = [responseString stringByAppendingString:[NSString stringWithFormat:@"Respond by %@\n\n",userObject.email]];
            }
        }
        requestDetails = [responseString stringByAppendingString:requestDetails];
        
        NSDictionary *dict = @{@"requestDetails": requestDetails,
                               //                               @"requestType":self.categoryName,
                               @"requestType":@"O Client Specific",
                               @"requestCity":[self.cityName isEqualToString:@""] ? @"N/A" : self.cityName};
        __weak typeof (self) _self = self;
        [ModelAspireApiManager createRequestWithUserInfo:dict completion:^(NSError *error) {
            [_self stopActivityIndicator];
            if (!error) {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    
                    [_self trackingEventByName:@"Request accepted" withAction:SubmitActionType withCategory:RequestCategoryType];
                    AskConciergeConfirmationViewController *vc = [[AskConciergeConfirmationViewController alloc] init];
                    [_self.navigationController pushViewController:vc animated:YES];
                    [_self.tvAskConcierge setText:@""];
                    lblPlaceholder.hidden = NO;
                    _self.btnAskConcierge.hidden = YES;
                    _self.btnCheckMail.selected = NO;
                    _self.btnCheckPhone.selected = NO;
                    viewControllerIsPopped = YES;
                    [_self hideAccessoryView];
                });
            }else{
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [_self trackingEventByName:@"Request denied" withAction:SubmitActionType withCategory:RequestCategoryType];
                    NSString *mess;
                    switch ([AspireApiError getErrorTypeAPICreateRequestFromError:error]) {
                        case ACCESSTOKEN_INVALID_ERR:
                            [_self handleInvalidCredentials];
                            break;
                        case UNKNOWN_ERR:
                            mess = @"";
                            break;
                        case NETWORK_UNAVAILABLE_ERR:
                            [_self showErrorNetwork];
                            break;
                        default:
                            mess = @"";
                            break;
                    }
                    if(mess != nil){
                        AlertViewController *alert = [[AlertViewController alloc] init];
                        //alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);
                        alert.msgAlert = NSLocalizedString(@"Unfortunately, your request submission was not successfully received. Please call Concierge to make the request.", nil);
                        alert.firstBtnTitle = NSLocalizedString(@"OK", nil);
                        dispatch_async(dispatch_get_main_queue(), ^{
                            alert.seconBtn.hidden = YES;
                            alert.midView.alpha = 0.0f;;
                        });
                        
                        [_self showAlert:alert forNavigation:YES];
                    }
                });
            }
        }];
        
        return;
        
//        NSString *requestDetails = [NSString stringWithFormat:@"%@%@\n%@",
//                                    [self.cityName isEqualToString:@""] ? @"" : [NSString stringWithFormat:@"\n%@", self.cityName],
//                                    [self.categoryName isEqualToString:@""] ? @"" : [NSString stringWithFormat:@"\n%@", self.categoryName],
//                                    self.tvAskConcierge.text];
//
//        NSDictionary *dict = @{@"message": requestDetails,
//                               @"requestType":self.categoryName,
//                               @"requestCity":self.cityName,
//                               @"phone":@(self.btnCheckPhone.selected),
//                               @"mail":@(self.btnCheckMail.selected)};
//        [wSB2CCreateConciergeCase askConciergeWithMessage:dict];
    }
}


- (IBAction)touchCallConcierge:(id)sender {
    self.btnCallConcierge.enabled = false;
    [self setUpTimeDisableButton];
    appdelegate.preventShowMaskScreen = YES;
    NSString *phone_number = @"8003734830";
    [self callWithPhoneString:phone_number];
}

- (IBAction)touchInternationalCall:(id)sender {
    self.btnInternationalCall.enabled = false;
    [self setUpTimeDisableButton];
    appdelegate.preventShowMaskScreen = YES;
    NSString *phone_number = @"17038914643";
    [self callWithPhoneString:phone_number];
}

-(void)setUpTimeDisableButton {
    [NSTimer scheduledTimerWithTimeInterval:1.0f
                                     target:self
                                   selector:@selector(timerCheck:)
                                   userInfo:nil
                                    repeats:YES];
}


- (void)timerCheck:(NSTimer*)timer{
    timeToChange += 1;
    if (timeToChange == 2) {
        self.btnCallConcierge.enabled = true;
        self.btnInternationalCall.enabled = true;
        [timer invalidate];
        timeToChange = 0;
    }
}


- (void) callWithPhoneString:(NSString*)phoneString{
    CTTelephonyNetworkInfo* info = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier* carrier = info.subscriberCellularProvider;
    NSString *isoCountryCode = carrier.isoCountryCode;
    
    if (!isoCountryCode && iosVersion() < 10.0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:phoneString message:@"" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *call = [UIAlertAction actionWithTitle:NSLocalizedString(@"Call", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSString *phoneStr = [[NSString alloc] initWithFormat:@"tel:%@",phoneString];
            NSURL *phoneURL = [[NSURL alloc] initWithString:phoneStr];
            [[UIApplication sharedApplication] openURL:phoneURL];
        }];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:call];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        NSString *phoneStr = [[NSString alloc] initWithFormat:@"tel:%@",phoneString];
        NSURL *phoneURL = [[NSURL alloc] initWithString:phoneStr];
        [[UIApplication sharedApplication] openURL:phoneURL];
        
    }

}

/*
 // UITextViewDelegate
 */
- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (![textView hasText]) {
        lblPlaceholder.hidden = NO;
        self.btnAskConcierge.hidden = YES;
    }
}


- (void) textViewDidBeginEditing:(UITextView *)textView{
    lblPlaceholder.hidden = YES;
}



-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    NSString *resultText = [textView.text stringByReplacingCharactersInRange:range
                                                                   withString:text];
    NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
    resultText = [resultText stringByTrimmingCharactersInSet:charSet];

    if ([resultText isEqualToString:@""]) {
        lblPlaceholder.hidden = YES;
        self.btnAskConcierge.hidden = YES;
        [self hideAccessoryView];
    }else{
        lblPlaceholder.hidden = YES;
        self.btnAskConcierge.hidden = NO;
        [self showAccessoryView];
    }
    return YES;
}

#pragma mark - API Delegate
/*
 // DataLoadDelegate
 */
- (void)loadDataDoneFrom:(id<WSBaseProtocol>)ws{
    [self stopActivityIndicator];
    [self trackingEventByName:@"Request accepted" withAction:SubmitActionType withCategory:RequestCategoryType];
    AskConciergeConfirmationViewController *vc = [[AskConciergeConfirmationViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
    [self.tvAskConcierge setText:@""];
    lblPlaceholder.hidden = NO;
    self.btnAskConcierge.hidden = YES;
    self.btnCheckMail.selected = NO;
    self.btnCheckPhone.selected = NO;
    viewControllerIsPopped = YES;
    [self hideAccessoryView];
}

- (void)loadDataFailFrom:(id<BaseResponseObjectProtocol>)result withErrorCode:(NSInteger)errorCode{
    [self stopActivityIndicator];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self trackingEventByName:@"Request denied" withAction:SubmitActionType withCategory:RequestCategoryType];
    });
    [self showAPIErrorAlertWithMessage:NSLocalizedString(@"An error has occurred. Check your internet settings or try again.", nil) andTitle:NSLocalizedString(@"Cannot Get Data", nil)];
}


- (void)WSBaseNetworkUnavailable{
    [self stopActivityIndicator];
}

#pragma mark - Actions

- (IBAction)touchPhone:(id)sender {
    self.btnCheckPhone.selected = !((UIButton *)sender).selected;
}

- (IBAction)touchMail:(id)sender {
    self.btnCheckMail.selected = !((UIButton *)sender).selected;
}

@end
