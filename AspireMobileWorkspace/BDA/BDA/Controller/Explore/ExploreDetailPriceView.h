//
//  ExploreDetailPriceView.h
//  MobileConciergeUSDemo
//
//  Created by Nghia Dinh on 7/20/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseItem.h"

@interface ExploreDetailPriceView : UIView

-(void)setupData:(BaseItem*)item;

@end
