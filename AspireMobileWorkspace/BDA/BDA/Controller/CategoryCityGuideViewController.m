

//
//  CityGuideViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/26/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "CategoryCityGuideViewController.h"
#import "AppData.h"
#import "CityTableViewCell.h"
#import "CategoryItem.h"
#import "CityItem.h"
#import "ExploreViewController.h"
#import "WSBase.h"
#import "WSB2CGetSubCategory.h"
#import "SubCategoryItem.h"
#import "WSB2CGetQuestions.h"

@interface CategoryCityGuideViewController ()<UITableViewDelegate, UITableViewDataSource, DataLoadDelegate>

@property(nonatomic, strong) WSB2CGetSubCategory *ws;

@end

@implementation CategoryCityGuideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    [self trackingScreenByName:@"City list"];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setUpCustomizedPanGesturePopRecognizer];
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"CITY GUIDE CATEGORY", nil)];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)initView
{
    [self.tableView registerNib:[UINib nibWithNibName:@"CityTableViewCell" bundle:nil] forCellReuseIdentifier:@"CityTableViewCell"];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100.f;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self backNavigationItem];
    self.navigationItem.title = NSLocalizedString(@"CHOOSE A CITY", nil);
}

-(void)initData
{
//    [self loadData];
    self.subCategories = [self.subCategories sortedArrayUsingComparator:^NSComparisonResult(CategoryItem *obj1, CategoryItem *obj2){ return [obj1.categoryName compare:obj2.categoryName]; }];
    
}

#pragma mark TABLEVIEW DELEGATE
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.subCategories.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CityTableViewCell"];
    [cell initCellWithCategoryData:[self.subCategories objectAtIndex:indexPath.row]];
//    [cell setHiddenBottomLine:indexPath.row == self.subCategories.count-1];
//    [cell setHiddenBottomLine:YES];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryItem *categoryItem = [self.subCategories objectAtIndex:indexPath.row];
    if(self.delegate)
        [self.delegate Explore:self onSelectCategory:categoryItem];
}

#pragma mark DATA LOADER DELEGATE
-(void)loadDataDoneFrom:(WSB2CGetSubCategory *)ws
{
    [self stopActivityIndicatorWithoutMask];
    isLoading = NO;
    [self.tableView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    self.subCategories = [ws.data sortedArrayUsingComparator:^NSComparisonResult(SubCategoryItem *obj1, SubCategoryItem *obj2){ return [obj1.name compare:obj2.name]; }];
    [self.tableView reloadData];
}


-(void)loadDataFailFrom:(id<BaseResponseObjectProtocol>) result withErrorCode:(NSInteger)errorCode
{
    
}


#pragma mark PROCESS DATA
-(void)loadData
{
    [self startActivityIndicatorWithoutMask];
    self.ws = [[WSB2CGetSubCategory alloc] init];
    self.ws.delegate = self;
    self.ws.categoryId = self.superCategoryId;
    [self.ws loadSubCategoryForCategory];
}
@end
