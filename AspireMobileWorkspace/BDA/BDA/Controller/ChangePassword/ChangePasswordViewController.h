//
//  ChangePasswordViewController.h
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 8/23/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "PasswordTextField.h"

@interface ChangePasswordViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *detailView;
@property (weak, nonatomic) IBOutlet PasswordTextField *oldPWTextField;
@property (weak, nonatomic) IBOutlet PasswordTextField *nPWTextField;

@property (weak, nonatomic) IBOutlet PasswordTextField *confirmNewPWTextField;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraintContentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraintViewAction;

@end
