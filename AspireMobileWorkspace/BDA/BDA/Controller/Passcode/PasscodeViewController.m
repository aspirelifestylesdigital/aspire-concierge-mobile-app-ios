//
//  PasscodeViewController.m
//  MobileConciergeUSDemo
//
//  Created by Chung Mai on 10/23/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "PasscodeViewController.h"
#import "UIButton+Extension.h"
#import "EnumConstant.h"
#import "WSBase.h"
#import "WSB2CPasscodeVerfication.h"
#import "SWRevealViewController.h"
#import "PasscodeItem.h"
#import "WSB2CGetUserDetails.h"
#import "AppDelegate.h"
#import "HomeViewController.h"
#import "MenuViewController.h"
#import "WSPreferences.h"
#import "PreferenceObject.h"
#import "CreateProfileViewController.h"
#import "AppData.h"
#import "ChangePasswordViewController.h"
#import <AspireApiFramework/AspireApiFramework.h>

#define LIMITCHARACTERS     25

@interface PasscodeViewController ()<DataLoadDelegate, UITextFieldDelegate>
{
   CGFloat backupBottomConstraint;
    WSPreferences *wsPreferences;
    WSB2CGetUserDetails *wsGetUser;
    WSB2CPasscodeVerfication *wsPasscodeVerification;
}
@end

@implementation PasscodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.headerLbl.attributedText = [UtilStyle setTextStyleForTitleViewControllerWithMessage:NSLocalizedString(@"CONCIERGE", nil)];
    
    UITapGestureRecognizer *dismiss = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:dismiss];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWill:) name:UIKeyboardWillHideNotification object:nil];
    
    [self.submitBtn setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
    
    [self.submitBtn setBackgroundColorForNormalStatus];
    [self.submitBtn setBackgroundColorForTouchingStatus];
    [self.submitBtn configureDecorationForButton];
    [self.submitBtn setEnabled:NO];
    
    [self.titleLbl setTextColor:[AppColor normalTextColor]];
    self.titleLbl.text = NSLocalizedString(@"Enter a valid Program Passcode", @"");
    
    self.passcodeTxtView.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.passcodeTxtView.text];

    self.passcodeTxtView.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:NSLocalizedString(@"Program Passcode", @"")];
    [self.passcodeTxtView setDelegate:self];
    
    backupBottomConstraint = self.submitBtnBottomConstraint.constant;
}


-(void)handleKeyboardWillShow:(NSNotification *)paramNotification
{
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         self.submitBtnBottomConstraint.constant = keyboardRect.size.height + MARGIN_KEYBOARD;
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

-(void)handleKeyboardWill:(NSNotification *)paramNotification
{
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         self.submitBtnBottomConstraint.constant =  backupBottomConstraint;
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.passcodeTxtView) {
        NSString *currentText = textField.text;
        if([string isEqualToString:@""]){
            currentText = (currentText.length == 1) ? @"" : [currentText substringToIndex:currentText.length - 2];
        }
        else{
            currentText = [currentText stringByAppendingString:string];
        }
        
        (currentText.length > 0) ? [self.submitBtn setEnabled:YES] :  [self.submitBtn setEnabled:NO];
        
        return !([textField.text length] >= LIMITCHARACTERS && [string length] > range.length);
    }
    return YES;
}

#pragma mark - Delegate from API
- (void)WSBaseNetworkUnavailable{
    [self stopActivityIndicator];
}

- (void)dismissKeyboard{
    if([self.passcodeTxtView becomeFirstResponder])
    {
        [self.passcodeTxtView resignFirstResponder];
    }
}

-(void)loadDataDoneFrom:(WSBase*)ws
{
    
    if(ws.task == WS_B2C_VERIFY_PASSCODE)
    {
        PasscodeItem *passcodeItem = (PasscodeItem *)ws.data[0];
        if(passcodeItem.valid)
        {
            
            [self updateProfile];
        }
        else
        {
            [self stopActivityIndicator];
            [self showAlertForRetryPasscode];
        }
        
    }
    else if(ws.task == WS_UPDATE_MY_PREFERENCE || ws.task == WS_ADD_MY_PREFERENCE)
    {
        wsGetUser = [[WSB2CGetUserDetails alloc] init];
        wsGetUser.delegate = self;
        [wsGetUser getUserDetails];
    }
    else if(ws.task == WS_B2C_GET_USER_DETAILS)
    {
        NSDictionary *profileDetailDict = [[SessionData shareSessiondata] getUserInfo];
        if(!profileDetailDict)
        {
            profileDetailDict = [[NSMutableDictionary alloc] init];
        }
        [profileDetailDict setValue:self.passcodeTxtView.text forKey:keyPasscode];
        [[SessionData shareSessiondata] setUserObjectWithDict:profileDetailDict];
        
        [self updateSuccessRedirect];
        [self stopActivityIndicator];
    }
}

-(IBAction)submitPasscode:(id)sender
{
    [self.passcodeTxtView endEditing:YES];
    [self startActivityIndicator];
    wsPasscodeVerification = [[WSB2CPasscodeVerfication alloc] init];
    wsPasscodeVerification.delegate = self;
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    [dataDict setValue:self.passcodeTxtView.text forKey:keyPasscode];
    [wsPasscodeVerification verifyPasscode:dataDict];
}

- (void)updateProfile{
    __weak typeof(self) _self = self;
    if([User isValid])
    {
        UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
        NSMutableDictionary *dictDai = [[NSMutableDictionary alloc] init];
        [dictDai setValue:userObject.salutation forKey:@"Salutation"];
        [dictDai setValue:userObject.firstName forKey:@"FirstName"];
        [dictDai setValue:userObject.lastName forKey:@"LastName"];
        [dictDai setValue:userObject.mobileNumber forKey:@"MobileNumber"];
        [dictDai setValue:userObject.email forKey:@"Email"];
        [dictDai setValue:self.passcodeTxtView.text forKey:APP_USER_PREFERENCE_Passcode];
        [dictDai setValue:userObject.isUseLocation ? @"ON" : @"OFF" forKey:APP_USER_PREFERENCE_Location];
        [dictDai setValue:[self getPreferenceByType:DiningPreferenceType].value forKey:@"Dining Preference"];
        [dictDai setValue:[self getPreferenceByType:HotelPreferenceType].value forKey:@"Hotel Preference"];
        [dictDai setValue:[self getPreferenceByType:TransportationPreferenceType].value forKey:@"Car Type Preference"];
        [dictDai setValue:[userObject.currentCity isEqualToString:@""] ? @"Unknown" : userObject.currentCity forKey:APP_USER_PREFERENCE_Selected_City];
        [dictDai setValue:[userObject.currentCity isEqualToString:@""] ? @"Unknown" : userObject.currentCity forKey:@"City"];
        [dictDai setValue:@"USA" forKey:@"Country"];
        [dictDai setValue:APP_NAME forKey:@"referenceName"];
        
        [ModelAspireApiManager updateProfileCurrentUserWithUserInfo:dictDai completion:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [_self stopActivityIndicator];
                if (!error) {
                    [_self updateSuccessRedirect];
                }else{
                    NSString* mess;
                    switch ([AspireApiError getErrorTypeAPIUpdateProfileFromError:error]) {
                        case UNKNOWN_ERR:
                            // show general message for update profile
                            mess = @"";
                            break;
                        case ACCESSTOKEN_INVALID_ERR:
                            // show message invalid accesstoken
                            [_self handleInvalidCredentials];
                            // login again
                            break;
                        case NETWORK_UNAVAILABLE_ERR:
                            [_self showErrorNetwork];
                        default:
                            mess = @"";
                            break;
                    }
                    if (mess != nil) {
                        [_self showAPIErrorAlertWithMessage:NSLocalizedString(@"An error has occurred. Check your internet settings or try again.", nil) andTitle:NSLocalizedString(@"Cannot Get Data", nil)];                    }
                }
            });
            
        }];
    }else
    {
        
        [self handleInvalidCredentials];
        
    }
    
    return;
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    NSMutableArray *arraySubDictValues = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *hotelDict = [[NSMutableDictionary alloc] init];
    [hotelDict setValue:@"Hotel" forKey:@"Type"];
    
    PreferenceObject *hotelPref = [self getPreferenceByType:HotelPreferenceType];
    NSString *value = ([[SessionData shareSessiondata] isUseLocation])?@"YES":@"NO";

    if ([SessionData shareSessiondata].arrayPreferences.count > 0 && hotelPref.preferenceID.length > 0){
        [hotelDict setValue:hotelPref.preferenceID forKey:@"MyPreferencesId"];
//        [hotelDict setValue:hotelPref.value forKey:@"Preferredstarrating"];
        [hotelDict setValue:value forKey:@"SmokingPreference"];
        [hotelDict setValue:self.passcodeTxtView.text forKey:@"RoomPreference"];
        [arraySubDictValues addObject:hotelDict];
        [dataDict setValue:arraySubDictValues forKey:@"PreferenceDetails"];
        
        wsPreferences = [[WSPreferences alloc] init];
        wsPreferences.delegate = self;
        [wsPreferences updatePreference:dataDict];
    }else{
        [hotelDict setValue:@"" forKey:@"Preferredstarrating"];
        [hotelDict setValue:value forKey:@"SmokingPreference"];
        [hotelDict setValue:self.passcodeTxtView.text forKey:@"RoomPreference"];
        [arraySubDictValues addObject:hotelDict];
        [dataDict setValue:arraySubDictValues forKey:@"PreferenceDetails"];
        
        wsPreferences = [[WSPreferences alloc] init];
        wsPreferences.delegate = self;
        [wsPreferences addPreference:dataDict];
    }
}

-(void)showAlertForRetryPasscode
{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"ERROR!", nil);
    alert.msgAlert =  NSLocalizedString(@"We’re sorry, but your Program Passcode is not valid. Click OK to enter a new value.", nil);;;
    alert.firstBtnTitle = NSLocalizedString(@"OK", nil);
    alert.secondBtnTitle = NSLocalizedString(@"CANCEL", nil);

    __weak typeof(self) weakSelf = self;
    alert.blockFirstBtnAction = ^{
        weakSelf.passcodeTxtView.text = @"";
        [weakSelf.passcodeTxtView becomeFirstResponder];
        [weakSelf.submitBtn setEnabled:NO];
    };
    
    alert.blockSecondBtnAction = ^{
        [weakSelf.navigationController popViewControllerAnimated:NO];
    };
    
    /*
    dispatch_async(dispatch_get_main_queue(), ^{
        alert.seconBtn.hidden = YES;
        alert.midView.alpha = 0.0f;;
    });
     */
    
    [self showAlert:alert forNavigation:NO];
}

- (void) updateSuccessRedirect{
//    if ([[SessionData shareSessiondata] hasForgotAccount]) {
//        ChangePasswordViewController *changePasswordVC = [[ChangePasswordViewController alloc] init];
//        [self presentViewController:changePasswordVC animated:YES completion:nil];
//    }else{
        AppDelegate *appdelegate =(AppDelegate*) [[UIApplication sharedApplication] delegate];
        
        [UIView transitionWithView:appdelegate.window
                          duration:0.5
                           options:UIViewAnimationOptionPreferredFramesPerSecond60
                        animations:^{
                            
                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                            MenuViewController *menuViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
                            UINavigationController *menuNavigationViewController = [[UINavigationController alloc] initWithRootViewController:menuViewController];
                            UIViewController *fontViewController = [[HomeViewController alloc] init];
                            
                            SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:menuNavigationViewController frontViewController:fontViewController];
                            
                            revealController.delegate = appdelegate;
                            revealController.rearViewRevealWidth = SCREEN_WIDTH;
                            revealController.rearViewRevealOverdraw = 0.0f;
                            revealController.rearViewRevealDisplacement = 0.0f;
                            appdelegate.viewController = revealController;
                            appdelegate.window.rootViewController = appdelegate.viewController;
                            [appdelegate.window makeKeyWindow];
                            
                            UIViewController *newFrontController = [[HomeViewController alloc] init];
                            UINavigationController *newNavigationViewController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                            [revealController pushFrontViewController:newNavigationViewController animated:YES];
                        }
                        completion:nil];
//    }
    
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }
    else
    {
        [self showAlertForRetryPasscode];
    }
}

-(void)loadDataFailFrom:(WSBase *)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message {
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }else{
        [self showAlertForRetryPasscode];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)setValidPasscode:(id)sender
{
    [[AppData getInstance] setValidPasscode:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
