//
//  AnswerItem.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/1/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "AnswerItem.h"
#import "BaseItem.h"

@implementation AnswerItem

- (instancetype)init {
    if(self = [super init]) {
        
    }
    return self;
}

-(NSString *)name
{
    return self.answerName;
}

-(NSString *)offer
{
    if([self.categoryCode isEqualToString:@"dining"])
    {
      return self.offer2;
    }
    
    return self.insiderTip;
}


-(NSString *)address
{
    return self.address3;
}

-(BOOL)isOffer
{
    return ([self.offer2 stringByReplacingOccurrencesOfString:@"<p>&nbsp;</p>" withString:@""].length > 0);
}


-(UIImage *)image
{
    if([self.categoryCode isEqualToString:@"cityguide"])
    {
        if([self.categoryName isEqualToString:NSLocalizedString(@"ACCOMMODATIONS",nil)])
        {
            return [UIImage imageNamed:@"asset_cityguide_detail"];
        }
        
        if([self.categoryName isEqualToString:NSLocalizedString(@"BARS/CLUBS",nil)])
        {
            return [UIImage imageNamed:@"bar_club_cityguide_detail"];
        }
        
        if([self.categoryName isEqualToString:NSLocalizedString(@"CULTURE",nil)])
        {
            return [UIImage imageNamed:@"culture_cityguide_detail"];
        }
        
        if([self.categoryName isEqualToString:NSLocalizedString(@"DINING",nil)])
        {
            return [UIImage imageNamed:@"dining_cityguide_detail"];
        }
        
        if([self.categoryName isEqualToString:NSLocalizedString(@"SHOPPING",nil)])
        {
            return [UIImage imageNamed:@"shopping_cityguide_detail"];
        }
        
        if([self.categoryName isEqualToString:NSLocalizedString(@"SPAS",nil)])
        {
            return [UIImage imageNamed:@"spa_wellness_cityguide_detail"];
        }
    }
    return super.image;
}
@end
