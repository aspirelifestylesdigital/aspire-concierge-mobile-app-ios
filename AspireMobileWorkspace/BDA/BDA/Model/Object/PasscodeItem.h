//
//  PasscodeItem.h
//  MobileConciergeUSDemo
//
//  Created by Chung Mai on 10/19/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PasscodeItem : NSObject

@property(assign, nonatomic) BOOL valid;
@property(strong, nonatomic) NSString *message;

@end
