//
//  TileItem.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/6/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//
#import "TileItem.h"
#import "CCAMapCategories.h"

@implementation TileItem

-(NSString *)name
{
    return self.title;
}

-(NSString *)offer
{
    return self.tileText;
}

-(NSString *)imageURL
{
    return self.tileImage;
}

-(NSString *)address
{
    NSString* add = @" ";
    if([[CCAMapCategories getCategoriesIDsMutilCities] containsObject:self.ID]) {
        add = NSLocalizedString(@"Multiple Cities", nil);
    }
    return add;
}

-(BOOL) isOffer
{
    return YES;
}

-(NSString *)categoryName
{
    if([self.tileCategory compare:@"Dining" options:NSCaseInsensitiveSearch] == NSOrderedSame || ([self.tileCategory compare:@"Specialty Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.tileSubCategory compare:@"Culinary Experiences" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return NSLocalizedString(@"DINING", nil);
    }
    else if([self.tileCategory compare:@"Hotels" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return NSLocalizedString(@"HOTELS", nil);
    }
    else if([self.tileCategory compare:@"Flowers" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return NSLocalizedString(@"FLOWERS", nil);
    }
    else if([self.tileCategory compare:@"Golf" options:NSCaseInsensitiveSearch] == NSOrderedSame
            || ([self.tileCategory compare:@"Specialty Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.tileSubCategory compare:@"Golf Experiences" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return NSLocalizedString(@"SPORT", nil);
    }
    else if(([self.tileCategory compare:@"Tickets" options:NSCaseInsensitiveSearch] == NSOrderedSame)
            || ([self.tileCategory compare:@"Specialty Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame &&
                ([self.tileSubCategory compare:@"Entertainment Experiences" options:NSCaseInsensitiveSearch] == NSOrderedSame
                 || [self.tileSubCategory compare:@"Major Sports Events" options:NSCaseInsensitiveSearch] == NSOrderedSame)))
    {
        return NSLocalizedString(@"ENTERTAINMENT", nil);
    }
    else if([self.tileCategory compare:@"Vacation Packages" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return NSLocalizedString(@"VACATION \nPACKAGES", nil);
    }
    /*
    else if([self.tileCategory compare:@"Cruises" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return NSLocalizedString(@"CRUISES", nil);
    }
     */
    else  if([self.tileCategory compare:@"VIP Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.tileSubCategory compare:@"Sightseeing" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return NSLocalizedString(@"TOURS", nil);
    }
    else if(([self.tileCategory compare:@"Private Jet Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame)
            || ([self.tileCategory compare:@"VIP Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.tileSubCategory compare:@"Airport Services" options:NSCaseInsensitiveSearch] == NSOrderedSame)
            || ([self.tileCategory compare:@"VIP Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.tileSubCategory compare:@"Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return NSLocalizedString(@"TRAVEL", nil);
    }
    else if([self.tileCategory compare:@"Transportation" options:NSCaseInsensitiveSearch] == NSOrderedSame && ([self.tileSubCategory compare:@"Car Rental" options:NSCaseInsensitiveSearch] == NSOrderedSame || [self.tileSubCategory compare:@"Limos and Private Car Service" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return NSLocalizedString(@"TRANSPORTATION", nil);
    }
    else if([self.tileCategory compare:@"Golf" options:NSCaseInsensitiveSearch] == NSOrderedSame
            || [self.tileCategory compare:@"Flowers" options:NSCaseInsensitiveSearch] == NSOrderedSame
            || [self.tileCategory compare:@"Wine" options:NSCaseInsensitiveSearch] == NSOrderedSame
            || [self.tileCategory compare:@"Retail Shopping" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return NSLocalizedString(@"SHOPPING", nil);
    }
    else
    {
        return @"";
    }
}

-(NSString *)categoryCode
{
    if([self.tileCategory compare:@"Dining" options:NSCaseInsensitiveSearch] == NSOrderedSame || ([self.tileCategory compare:@"Specialty Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.tileSubCategory compare:@"Culinary Experiences" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return @"dining";
    }
    else if([self.tileCategory compare:@"Hotels" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"hotels";
    }
    else if(([self.currentCategoryCode isEqualToString:@"flowers"] || [self.currentCategoryCode isEqualToString:@"all"])
            && ([self.tileCategory compare:@"Flowers" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return @"flowers";
    }
    else if(([self.currentCategoryCode isEqualToString:@"sport"] || [self.currentCategoryCode isEqualToString:@"all"])
            && ([self.tileCategory compare:@"Golf" options:NSCaseInsensitiveSearch] == NSOrderedSame || ([self.tileCategory compare:@"Specialty Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.tileSubCategory compare:@"Golf Experiences" options:NSCaseInsensitiveSearch] == NSOrderedSame)))
    {
        return @"sport";
    }
    else if(([self.tileCategory compare:@"Tickets" options:NSCaseInsensitiveSearch] == NSOrderedSame)
            || ([self.tileCategory compare:@"Specialty Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame &&
                ([self.tileSubCategory compare:@"Entertainment Experiences" options:NSCaseInsensitiveSearch] == NSOrderedSame
                 || [self.tileSubCategory compare:@"Major Sports Events" options:NSCaseInsensitiveSearch] == NSOrderedSame)))
    {
        return @"entertainment";
    }
    else if([self.tileCategory compare:@"Vacation Packages" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"vacation packages";
    }
    else  if([self.tileCategory compare:@"VIP Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.tileSubCategory compare:@"Sightseeing" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"tours";
    }
    else if(([self.tileCategory compare:@"Private Jet Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame)
            || ([self.tileCategory compare:@"VIP Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.tileSubCategory compare:@"Airport Services" options:NSCaseInsensitiveSearch] == NSOrderedSame)
            || ([self.tileCategory compare:@"VIP Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.tileSubCategory compare:@"Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return @"travel";
    }
    else if([self.tileCategory compare:@"Transportation" options:NSCaseInsensitiveSearch] == NSOrderedSame && ([self.tileSubCategory compare:@"Car Rental" options:NSCaseInsensitiveSearch] == NSOrderedSame || [self.tileSubCategory compare:@"Limos and Private Car Service" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return @"transportation";
    }
    else if(([self.currentCategoryCode isEqualToString:@"shopping"] || [self.currentCategoryCode isEqualToString:@"all"])
            && ([self.tileCategory compare:@"Golf" options:NSCaseInsensitiveSearch] == NSOrderedSame
                || [self.tileCategory compare:@"Flowers" options:NSCaseInsensitiveSearch] == NSOrderedSame
                || [self.tileCategory compare:@"Wine" options:NSCaseInsensitiveSearch] == NSOrderedSame
                || [self.tileCategory compare:@"Retail Shopping" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return @"shopping";
    }
    else if([self.tileCategory compare:Global_Offers options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return Global_Offers;
    }
    else
    {
        return @"";
    }
}
@end
