//
//  CCAGeographicManager.m
//  TestIOS
//
//  Created by Dai Pham on 7/12/17.
//  Copyright © 2017 Dai Pham. All rights reserved.
//

#import "CCAGeographicManager.h"

#pragma mark -
@implementation CCAGeographicObject

- (id) initWithObject:(NSDictionary*)dict {
    if(self = [super init]) {
        for (NSString* key in [dict allKeys]) {
            if([dict objectForKey:key] != nil || ![[dict objectForKey:key] isEqual:[NSNull null]]) {
                [self setValue:[dict objectForKey:key] forKey:key];
            }
        }
    }
    
    return self;
}

- (void) setValue:(id)value forKey:(NSString *)key {
    if([value isEqual:[NSNull null]]) return;
    if(value == nil) return;
    [super setValue:value forKey:key];
}
@end

#pragma mark -
@implementation CCAGeographicManager
static CCAGeographicManager *_shared;

+ (CCAGeographicManager*) startService {
    static dispatch_once_t onceInstance;
    if(!_shared) {
        dispatch_once(&onceInstance, ^{
            _shared = [CCAGeographicManager new];
        });
    }
    return _shared;
}

- (instancetype) init {
    if(self = [super init]) {
        [self getData];
    }
    return self;
}

#pragma mark - INTERFACE
+ (NSString *)getGeographicForCity:(NSString *)cityName {
    return [_shared getGeographicForCity:cityName];
}

+ (NSString *)getSubcategoryForCity:(NSString *)cityName {
    return [_shared getSubcategoryForCity:cityName];
}

+ (NSString *)getExceptionGeographicForCity:(NSString *)cityName {
    return [_shared getExceptionGeographicForCity:cityName];
}

#pragma mark - PRIVATE
- (NSString *)getGeographicForCity:(NSString *)cityName {
    NSMutableString* tmp = [NSMutableString stringWithString:@""];
    for (CCAGeographicObject* obj in data) {
        if([obj.City containsString:cityName]) {
            [tmp appendFormat:tmp.length>0?@"|%@":@"%@",obj.GeographicRegion];
        }
    }
//    if([cityName isEqualToString:@"Cancun"])
//    NSLog(@"Geo for %@: %@",cityName,[tmp componentsSeparatedByString:@"|"]);
    return tmp;
}

- (NSString *)getSubcategoryForCity:(NSString *)cityName {
    NSMutableString* tmp = [NSMutableString stringWithString:@""];
    for (CCAGeographicObject* obj in data) {
        if([obj.City containsString:cityName]) {
            [tmp appendFormat:tmp.length>0?@"|%@":@"%@",obj.Subcategory];
        }
    }
//    if([cityName isEqualToString:@"Cancun"])
//    NSLog(@"Sub for %@: %@",cityName,[tmp componentsSeparatedByString:@"|"]);
    return tmp;
}

-(NSString *) getExceptionGeographicForCity:(NSString *)cityName{
    NSMutableString* tmp = [NSMutableString stringWithString:@""];
    for (CCAGeographicObject* obj in data) {
        if([obj.City containsString:cityName]) {
            [tmp appendFormat:tmp.length>0?@"|%@":@"%@",obj.ExceptGeographicRegion];
        }
    }
    
    return tmp;
}

- (void) getData {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSString*  filepath = [[NSBundle mainBundle] pathForResource:@"CCAGeographicManager" ofType:@"json"];
        NSError* error;
        NSString*myJson = [[NSString alloc] initWithContentsOfFile:filepath encoding:NSUTF8StringEncoding error:&error];
        if(error != nil) {
            return;
        }
        
        NSArray* result = [NSJSONSerialization JSONObjectWithData:[myJson dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
        
        if(error != nil) {
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self convertDataToCCAGeographicObject:result];
        });
    });
}

- (void) convertDataToCCAGeographicObject:(NSArray*)result {
    if(result.count == 0)
        return;
    NSMutableArray* temp = [NSMutableArray new];
    for (NSDictionary* obj in result) {
        [temp addObject:[[CCAGeographicObject alloc] initWithObject:obj]];
    }
    data = temp;
    temp = nil;
    if(_onDone)
        _onDone();
}
@end
