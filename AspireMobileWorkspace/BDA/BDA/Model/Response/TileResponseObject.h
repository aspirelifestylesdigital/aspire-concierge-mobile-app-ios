//
//  TileResponseObject.h
//  MobileConcierge
//
//  Created by Chung Mai on 6/6/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "BaseResponseObject.h"

@interface TileResponseObject : BaseResponseObject

@property(nonatomic, strong) NSString *searchText;
@property(nonatomic, assign) NSInteger *totalSearchItem;

@end
