//
//  Personal_Concierge_ServiceUITests.swift
//  Personal Concierge ServiceUITests
//
//  Created by Dai on 8/13/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

import XCTest

class Personal_Concierge_ServiceUITests: XCTestCase {

    let app = XCUIApplication()
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = true

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testFotgotPassword() {
        
        sleep(2)
        
        // case 0: check enable submit button when one of fields is blank
        XCTContext.runActivity(named: "CHECK SUBMIT BUTTON") { (test) in
            XCTContext.runActivity(named: "CASE: DONT SELECT QUESTION") { (test) in
                XCTAssertFalse(checkSubmitButton(email: "you10@gmail.com", answer: "kimochi", password: "1234", cPassword: "1234",false))
            }
            
            XCTContext.runActivity(named: "CASE: EMAIL IS BLANK OR HAVE SPACE VALUE") { (test) in
                XCTAssertFalse(checkSubmitButton(email: "   ", answer: "kimochi", password: "1234", cPassword: "1234"))
            }
            
            XCTContext.runActivity(named: "CASE: ANSWER IS BLANK OR HAVE SPACE VALUE") { (test) in
                XCTAssertFalse(checkSubmitButton(email: "you10@gmail.com", answer: "   ", password: "1234", cPassword: "1234"))
            }
            
            XCTContext.runActivity(named: "CASE: PASSWORD IS BLANK OR HAVE SPACE VALUE") { (test) in
                XCTAssertFalse(checkSubmitButton(email: "you10@gmail.com", answer: "kimochi", password: "", cPassword: "1234"))
            }
            
            XCTContext.runActivity(named: "CASE: CONFIRM PASSWORD IS BLANK OR HAVE SPACE VALUE") { (test) in
                XCTAssertFalse(checkSubmitButton(email: "you10@gmail.com", answer: "kimochi", password: "1234", cPassword: ""))
            }
            
            XCTContext.runActivity(named: "CASE: INPUT VALID") { (test) in
                XCTAssertTrue(checkSubmitButton(email: "you10@gmail.com", answer: "kimochi", password: "1234", cPassword: "1234"))
            }
        }
        
        // case 1: Email not found,
        XCTContext.runActivity(named: "CASE 1: EMAIL NOT FOUND") { (test) in
            XCTContext.runActivity(named: "A: DISPLAY ALERT WHEN SELECT NO BUTTON, EXPECT BACK TO SIGNIN PAGE") { (test) in
                processInputForgotPassword(email: "abc@gmail.com", answer: "123", password: "2345", cPassword: "2345")
                XCTAssertTrue(checkResultForgotPassword(numberButtons: 2, titleButtons: ["YES","NO"], message: "A user account with this email address cannot be found. Would you like to try again?"))
                button(id: "btnSecond").tap()
                XCTAssert(button(id: "btnForgot").exists)
            }
            
            XCTContext.runActivity(named: "B: DISPLAY ALERT WHEN SELECT YES BUTTON, EXPECT ALL ENTRIES REMOVED") { (test) in
                processInputForgotPassword(email: "abc@gmail.com", answer: "123", password: "2345", cPassword: "2345")
                XCTAssertTrue(checkResultForgotPassword(numberButtons: 2, titleButtons: ["YES","NO"], message: "A user account with this email address cannot be found. Would you like to try again?"))
                button(id: "btnFirst").tap()
                XCTAssertTrue(checkEntriesForgotPasswordRemoved(except: []))
            }
        }
        
        // case 2: Answer wrong
        XCTContext.runActivity(named: "CASE 2: ANSWER IS WRONG") { (test) in
            XCTContext.runActivity(named: "A: DISPLAY ALERT WHEN SELECT NO BUTTON, EXPECT BACK TO SIGNIN PAGE") { (test) in
                processInputForgotPassword(email: "you18@gmail.com", answer: "test", password: "Admin@123;", cPassword: "Admin@123;")
                XCTAssertTrue(checkResultForgotPassword(numberButtons: 2, titleButtons: ["YES","NO"], message: "The answer to the Security question is not correct. Would you like to try again?"))
                button(id: "btnSecond").tap()
                XCTAssert(button(id: "btnForgot").exists)
            }
            
            XCTContext.runActivity(named: "B: DISPLAY ALERT WHEN SELECT YES BUTTON, EXPECT ALL ENTRIES REMOVED EXCEPT EMAIL") { (test) in
                processInputForgotPassword(email: "you18@gmail.com", answer: "test", password: "Admin@123;", cPassword: "Admin@123;")
                XCTAssertTrue(checkResultForgotPassword(numberButtons: 2, titleButtons: ["YES","NO"], message: "The answer to the Security question is not correct. Would you like to try again?"))
                button(id: "btnFirst").tap()
                XCTAssertTrue(checkEntriesForgotPasswordRemoved(except: ["email"]))
            }
        }
 
        // case 3: Answer wrong
        XCTContext.runActivity(named: "CASE 3: PASSWORD DOES NOT MEET ALL REQUIREMENTS, EXPECT DISPLAY ALERT WITH OK BUTTON & PASSWORD TEXTFIELD IS FOCUSED") { (test) in
            processInputForgotPassword(email: "you13@gmail.com", answer: "kimochi", password: "Admin", cPassword: "Admin")
            XCTAssertTrue(checkResultForgotPassword(numberButtons: 1, titleButtons: ["OK"], message: "At least 8 characters, a lowercase letter, an uppercase letter, a number, a symbol, no parts of your username, does not include your first name, does not include your last name. Your password cannot be any of your last 12 passwords. At least 1 day(s) must have elapsed since you last changed your password."))
            button(id: "btnFirst").tap()
            if let v = textField(id: "password", isSecure: true).value(forKey: "hasKeyboardFocus") as? Bool {
                XCTAssertTrue(v)
            } else {
                XCTAssertFalse(false)
            }
        }
        
        // case 4: email invalid form & password does not match
        XCTContext.runActivity(named: "CASE 4: EMAIL DOES NOT MATCH & PASSWORD DOES NOT MATCH") { (test) in
            XCTContext.runActivity(named: "A: EMAIL INVALID, EXPECT DISPLAY ALERT WITH OK BUTTON & EMAIL TEXTFIELD IS FOCUSED") { (test) in
                processInputForgotPassword(email: "gmail.com", answer: "kimochi", password: "Admin", cPassword: "Admin")
                XCTAssertTrue(checkResultForgotPassword(numberButtons: 1, titleButtons: ["OK"], message: " * Enter a valid email address. "))
                button(id: "btnFirst").tap()
                if let v = textField(id: "email", isSecure: false).value(forKey: "hasKeyboardFocus") as? Bool {
                    XCTAssertTrue(v)
                } else {
                    XCTAssertFalse(false)
                }
            }
            
            XCTContext.runActivity(named: "B: PASSWORD DOES NOT MATCH, EXPECT DISPLAY ALERT WITH OK BUTTON & CONFIRM PASSWORD TEXTFIELD IS FOCUSED") { (test) in
                processInputForgotPassword(email: "you13@gmail.com", answer: "kimochi", password: "Admin", cPassword: "Admin1")
                XCTAssertTrue(checkResultForgotPassword(numberButtons: 1, titleButtons: ["OK"], message: " * The password entries do not match. "))
                button(id: "btnFirst").tap()
                if let v = textField(id: "confirmPassword", isSecure: true).value(forKey: "hasKeyboardFocus") as? Bool {
                    XCTAssertTrue(v)
                } else {
                    XCTAssertFalse(false)
                }
            }
            
            XCTContext.runActivity(named: "C: PASSWORD DOES NOT MATCH & EMAIL INVALID, EXPECT DISPLAY ALERT WITH OK BUTTON & EMAIL TEXTFIELD IS FOCUSED") { (test) in
                processInputForgotPassword(email: "gmail.com", answer: "kimochi", password: "Admin", cPassword: "Admin1")
                XCTAssertTrue(checkResultForgotPassword(numberButtons: 1, titleButtons: ["OK"], message: " * Enter a valid email address. * The password entries do not match. "))
                button(id: "btnFirst").tap()
                if let v = textField(id: "email", isSecure: false).value(forKey: "hasKeyboardFocus") as? Bool {
                    XCTAssertTrue(v)
                } else {
                    XCTAssertFalse(false)
                }
            }
        }
        
    }

    func processInputForgotPassword(email:String, answer:String, password:String, cPassword:String) {
        
        XCTContext.runActivity(named: "Go to Forgot Password page") { activity in
            if button(id: "btnForgot").exists {
                button(id: "btnForgot").tap()
            }
        }
        
        XCTContext.runActivity(named: "Input fields forgot password page") { (activity) in
            input(id: "Email", value: email, isSecure: false)
            
            sleep(1)
            if checkCheckboxIsExist(id: "Security Question") {
                app.staticTexts["Security Question"].tap()
                app.tables/*@START_MENU_TOKEN@*/.staticTexts["Who is your favorite book/movie character?"]/*[[".cells.staticTexts[\"Who is your favorite book\/movie character?\"]",".staticTexts[\"Who is your favorite book\/movie character?\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
            }
            
            input(id: "Answer", value: answer, isSecure: false)
            input(id: "New Password", value: password, isSecure: true)
            input(id: "Retype New Password", value: cPassword, isSecure: true)
            
            app.buttons["SUBMIT"].tap()
        }
    }
    
    func checkResultForgotPassword(numberButtons:Int, titleButtons:[String], message:String, isForgotSucess:Bool = false) -> Bool {
        
        let predicate = NSPredicate(format: "exists == 1")
        
        let lblMessage = label(id: "message")
        
        let expect = expectation(for: predicate, evaluatedWith: lblMessage, handler: nil)
        
        let resultOK = XCTWaiter().wait(for: [expect], timeout: 40)
        
        if isForgotSucess {
            return resultOK != .completed
        }
        
        let resultBtnFirst = XCTWaiter().wait(for: [expectation(for: predicate, evaluatedWith: button(id: "btnFirst"), handler: nil)], timeout: 1)
        let resultBtnSecond = XCTWaiter().wait(for: [expectation(for: predicate, evaluatedWith: button(id: "btnSecond"), handler: nil)], timeout: 1)
        
        // check alert view only one button
        if numberButtons == 1 {
            //case: display two button alert displayed
            if resultBtnFirst == .completed && resultBtnSecond == .completed {
                return false
            }
            // case: wrong button alert displayed
            else if resultBtnFirst != .completed && resultBtnSecond == .completed {
                return false
            }
            // case: title button alert wrong
            if let a = titleButtons.first {
                if (a != button(id: "btnFirst").label) {
                    return false
                }
            } else {
                return false;
            }
        }
        // check alert view has two button
        else {
            //case: display only one button alert displayed
            if resultBtnFirst == .completed && resultBtnSecond != .completed {
                return false
            } else if resultBtnFirst != .completed && resultBtnSecond == .completed {
                return false
            }
            
            // case: title button alert wrong
            if let a = titleButtons.first {
                if (a != button(id: "btnFirst").label) {
                    return false
                }
            } else {
                return false;
            }
            if (titleButtons[1] != button(id: "btnSecond").label) {
                return false
            }
        }
        
        //case: message alert wrong expected
        if (message != lblMessage.label) {
            return false
        }
        return true;
    }
    
    func checkSubmitButton(email:String, answer:String, password:String, cPassword:String,_ isSelectQuestion:Bool = true)->Bool {

        XCTContext.runActivity(named: "Go to Forgot Password page") { activity in
            if button(id: "btnForgot").exists {
                button(id: "btnForgot").tap()
            }
        }
        
        XCTContext.runActivity(named: "Input fields forgot password page") { (activity) in
            input(id: "Email", value: email, isSecure: false)
            
            if checkCheckboxIsExist(id: "Security Question") && isSelectQuestion{
                app.staticTexts["Security Question"].tap()
                app.tables/*@START_MENU_TOKEN@*/.staticTexts["Who is your favorite book/movie character?"]/*[[".cells.staticTexts[\"Who is your favorite book\/movie character?\"]",".staticTexts[\"Who is your favorite book\/movie character?\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
            }
            
            input(id: "Answer", value: answer, isSecure: false)
            input(id: "New Password", value: password, isSecure: true)
            input(id: "Retype New Password", value: cPassword, isSecure: true)
        }
        
        return app.buttons["SUBMIT"].isEnabled
    }
    
    func input(id:String, value:String, isSecure:Bool) {
        if isSecure {
            app.secureTextFields[id].tap()
        } else {
            app.textFields[id].tap()
        }
        
        if isSecure {
            if let text = app.secureTextFields[id].value as? String {
                let deleteString = text.characters.map { _ in XCUIKeyboardKey.delete.rawValue }.joined(separator: "")
                app.secureTextFields[id].typeText(deleteString)
            }
            app.secureTextFields[id].typeText(value)
        } else {
            if let text = app.textFields[id].value as? String {
                let deleteString = text.characters.map { _ in XCUIKeyboardKey.delete.rawValue }.joined(separator: "")
                app.textFields[id].typeText(deleteString)
            }
            app.textFields[id].typeText(value)
        }
    }
    
    func checkButtonIsExist(id:String) -> Bool{
        let btn = app.buttons.element(matching: .button, identifier: id)
        
        let predicate = NSPredicate(format: "exists == 1")
        let expect = expectation(for: predicate, evaluatedWith: btn, handler: nil)
        
        let resultOK = XCTWaiter().wait(for: [expect], timeout: 1)
        
        return resultOK == .completed
    }
    
    func checkCheckboxIsExist(id:String) -> Bool {
        let btn = app.staticTexts[id]
        
        let predicate = NSPredicate(format: "exists == 1")
        let expect = expectation(for: predicate, evaluatedWith: btn, handler: nil)
        
        let resultOK = XCTWaiter().wait(for: [expect], timeout: 1)
        
        return resultOK == .completed
    }
    
    func checkEntriesForgotPasswordRemoved(except:[String]) -> Bool {
        var isOK = true
        
        for id in ["email","answer","password","confirmPassword"] {
            if let v = textField(id: id, isSecure: id.contains("assword")).value as? String {
                isOK = except.contains(id) ? v.characters.count > 0 : v.characters.count == 0
            } else {
                isOK = !except.contains(id)
            }
            if !isOK {return isOK}
        }
        
        isOK = checkCheckboxIsExist(id: "Security Question")
        
        return isOK
    }
    
    func button(id:String) ->XCUIElement {
        return app.buttons.element(matching: .button, identifier: id)
    }
    
    func label(id:String) ->XCUIElement {
        return app.staticTexts.element(matching: .staticText, identifier: id)
    }
    
    func textField(id:String,isSecure:Bool) ->XCUIElement {
        return isSecure ? app.secureTextFields.element(matching: .secureTextField, identifier: id) : app.textFields.element(matching: .textField, identifier: id)
    }
}
