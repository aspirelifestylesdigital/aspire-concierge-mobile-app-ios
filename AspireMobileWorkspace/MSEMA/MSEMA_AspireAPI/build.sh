#
#  build script
#  Increase build number -> generate IPA -> Upload to TestFlight
#
#  Created by Vuong Tran on 4/12/17.
#  Copyright © 2017 s3Corp. All rights reserved.
#

#!/bin/bash
set -ex

# This scripts allows you to upload a binary to the iTunes Connect Store and do it for a specific app_id

# Requires application loader to be installed
# See https://developer.apple.com/library/ios/documentation/LanguagesUtilities/Conceptual/iTunesConnect_Guide/Chapters/SubmittingTheApp.html
# Itunes Connect username & password
USER=tcvuong89@gmail.com
PASS=vgem-fseh-ynyy-rhoc
#USER=vuong.tran@s3corp.com.vn
#PASS='Zz1234!@#$'
TEAMID=ZR6QDY93JK

# App id as in itunes store create, not in your developer account
APP_ID='1297176219'

# SCHEME TARGET
SCHEME=MSEMA
WORKSPACE="MSEMA"
FOLDER=MSEMA

# Increase build version
buildnumber=$(/usr/libexec/PlistBuddy -c "Print :CFBundleVersion" "MSEMA/Supporting Files/Info.plist")
version=$(/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" "MSEMA/Supporting Files/Info.plist")
value=1
nextbuildnumber=$((buildnumber+value))
echo "Start build version $nextbuildnumber"

plutil -replace CFBundleVersion -string $nextbuildnumber "MSEMA/Supporting Files/Info.plist"

IPA_FILE=builds/$SCHEME.ipa

BUILDSDIR=builds
# Remove previous builds
test -d ${BUILDSDIR} && rm -rf ${BUILDSDIR}
mkdir ${BUILDSDIR}
cat <<EOM > builds/exportPlist.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
        <key>teamID</key>
        <string>$TEAMID</string>
        <key>method</key>
        <string>app-store</string>
        <key>uploadSymbols</key>
        <true/>
</dict>
</plist>
EOM

if [ "$WORKSPACE" == "" ]
then
	echo "Build CodeProj"
	xcodebuild -scheme $SCHEME -archivePath builds/$SCHEME.xcarchive archive
else
	echo "Build WorkSpace"
	xcodebuild -workspace $WORKSPACE.xcworkspace -scheme $SCHEME -archivePath builds/$SCHEME.xcarchive archive
fi

xcrun xcodebuild -exportArchive -exportOptionsPlist builds/exportPlist.plist -archivePath builds/$SCHEME.xcarchive -exportPath builds -allowProvisioningUpdates

# Start upload IPA file to iTunes
IPA_FILENAME=$(basename $IPA_FILE)
MD5=$(md5 -q $IPA_FILE)
BYTESIZE=$(stat -f "%z" $IPA_FILE)

TEMPDIR=builds/itsmp
# Remove previous temp
test -d ${TEMPDIR} && rm -rf ${TEMPDIR}
mkdir ${TEMPDIR}
mkdir ${TEMPDIR}/mybundle.itmsp

# You can see this debug info when you manually do an app upload with the Application Loader
# It's when you click activity

cat <<EOM > ${TEMPDIR}/mybundle.itmsp/metadata.xml
<?xml version="1.0" encoding="UTF-8"?>
<package version="software4.7" xmlns="http://apple.com/itunes/importer">
    <software_assets apple_id="$APP_ID">
        <asset type="bundle">
            <data_file>
                <file_name>$IPA_FILENAME</file_name>
                <checksum type="md5">$MD5</checksum>
                <size>$BYTESIZE</size>
            </data_file>
        </asset>
    </software_assets>
</package>
EOM

cp ${IPA_FILE} $TEMPDIR/mybundle.itmsp

/Applications/Xcode.app/Contents/Applications/Application\ Loader.app/Contents/itms/bin/iTMSTransporter -m upload -f ${TEMPDIR} -u "$USER" -p "$PASS" -v detailed

git add -A 
git commit -m"Built the $version($nextbuildnumber) internal release of the Morgan Stanley project."
git push origin head

cd builds
RELEASENAME="${WORKSPACE}_$version($nextbuildnumber).ipa"
mv $IPA_FILENAME.ipa $RELEASENAME

# test -d ${BUILDSDIR} && rm -rf ${BUILDSDIR}

osascript -e 'display notification "Build done!" with title "Hey"'

