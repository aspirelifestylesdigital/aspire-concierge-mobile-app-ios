//
//  AppColor.h
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 10/19/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppColor : NSObject

#pragma MARK - VIEW
+(UIColor *) backgroundColor;
+(UIColor *) lineViewBackgroundColor;
+(UIColor *) separatingLineColor;
+(UIColor *) selectedViewColor;

#pragma MARK - BUTTON
+(UIColor *) disableButtonColor;
+(UIColor *) normalButtonColor;
+(UIColor *) highlightButtonColor;

#pragma MARK - LABEL
+(UIColor *) titleViewControllerColor;
+(UIColor *) placeholderTextColor;
+(UIColor *) textColor;
+(UIColor *) disableTextColor;
+(UIColor *) normalTextColor;
+(UIColor *) highlightTextColor;
+(UIColor *) linkTextColor;

@end

