//
//  StyleTextField.h
//  MobileConcierge
//
//  Created by 😱 on 7/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "Common.h"

#define TEXTFIELD_TEXT_COLOR_STANDARD         @"#011627" //@"#272726"
#define TEXTFIELD_PLACEHOLDER_COLOR_STANDARD  @"#99A1A8" //@"#434342"

#define TEXTFIELD_FONT_SIZE_STANDARD           18
#define TEXTFIELD_FONT_NAME_STANDARD           FONT_MarkForMC_REGULAR


#define TEXTFIELD_TEXT_COLOR_SEARCH         @"#272726"
#define TEXTFIELD_PLACEHOLDER_COLOR_SEARCH  @"#434342"

#define TEXTFIELD_FONT_SIZE_SEARCH           18
#define TEXTFIELD_FONT_NAME_SEARCH           FONT_MarkForMC_REGULAR
