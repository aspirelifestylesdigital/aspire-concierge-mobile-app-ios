//
//  BaseComponentView.h
//  MobileConciergeUSDemo
//
//  Created by Dai on 8/8/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseComponentView : UIView

@property (weak,nonatomic) IBOutlet UIView* view;

@property (weak,nonatomic) UIViewController* vc;
@property (assign,nonatomic) BOOL didUpdateLayout;
@property (assign,nonatomic) BOOL isIgnoreScaleView;

- (void) config;
- (void) showErrorNetwork;
- (void) stopActivityIndicator;
- (void) startActivityIndicator;
- (void) showApiErrorWithMessage:(NSString*) message;
- (void) showAlertWithTitle:(NSString*)title
                    message:(NSString*)message
                    buttons:(NSArray*)buttons actions:(NSArray*)actions
           messageAlignment:(NSTextAlignment) textAlign;
@end
