//
//  CheckEmailView.h
//  MSEMA
//
//  Created by HieuNguyen on 10/5/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "BaseViewController.h"
#import "BaseComponentView.h"

@class CustomTextField;
@class AACCheckEmailController;

@interface CheckEmailView : BaseComponentView
@property (weak, nonatomic) IBOutlet UILabel *lblTop;
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet CustomTextField *emailTextField;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomButtonConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintHidenView;

@property (weak,nonatomic) AACCheckEmailController* controller;

@property (nonatomic, strong) UIPercentDrivenInteractiveTransition *interactivePopTransition;

@end
