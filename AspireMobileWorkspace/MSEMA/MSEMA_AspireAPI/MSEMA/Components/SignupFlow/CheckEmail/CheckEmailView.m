//
//  CheckEmailView.m
//  MSEMA
//
//  Created by HieuNguyen on 10/5/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "CheckEmailView.h"
#import "UITextField+Extensions.h"
#import "UILabel+Extension.h"
#import "UIView+Extension.h"
#import "UIButton+Extension.h"
#import "NSString+Utis.h"
#import "UtilStyle.h"
#import "AlertViewController.h"
#import "CustomTextField.h"
#import "Common.h"
#import "Constant.h"
#import "AppSize.h"
#import "AppColor.h"

#import "CreateProfileViewController.h"
#import "UDASignInViewController.h"

#import "AppDelegate.h"
@import AspireApiFramework;

@import AspireApiControllers;

@interface CheckEmailView ()<UITextFieldDelegate, UIGestureRecognizerDelegate> {
    CGFloat keyboardHeight;
    UITapGestureRecognizer *tappedOutsideKeyboards;
    UIPanGestureRecognizer *popRecognizer;
}
@end

@implementation CheckEmailView
- (void)setController:(AACCheckEmailController *)controller{
    _controller = controller;
    
    if (_controller) {
        self.vc = _controller;
        typeof(self) __weak _self = self;
        
        _controller.onViewWillAppear= ^{
            if (!_self) return;
            typeof(self) __self = _self;
            [_self viewWillAppear];
            [__self->_controller.navigationController setNavigationBarHidden:true];
        };
        
        _controller.onViewWillDisappear= ^{
            if (!_self) return;
            [_self viewWillDisappear];
        };
    }
}

-(void) viewWillAppear {
    [self setTextViewsDefaultBottomBolder];
    tappedOutsideKeyboards = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tappedOutsideKeyboards.delegate = self;
    [_controller.navigationController.view addGestureRecognizer:tappedOutsideKeyboards];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear {
    [_controller.navigationController.view removeGestureRecognizer:tappedOutsideKeyboards];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)config {
    
    if(!self.didUpdateLayout && !self.isIgnoreScaleView){
        self.didUpdateLayout = YES;
        resetScaleViewBaseOnScreen(self.view);
    }
    
    [self setUpData];
    [self setUIStyte];
    
//    popRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePopRecognizer:)];
//    //popRecognizer.delegate = self;
//    [self.view addGestureRecognizer:popRecognizer];
}

- (void) setUIStyte {
    
    [self.lblTop setLineSpacing:6];
    
    self.titleLable.attributedText = [UtilStyle setTextStyleForTitleViewControllerWithMessage:@"REGISTER"];
    
    self.view.backgroundColor = [AppColor backgroundColor];
    
    self.emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    
    self.emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    
    [self createAsteriskForTextField:self.emailTextField];
    
    self.emailTextField.delegate = self;
    
    self.emailTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.emailTextField.text];
    
    self.emailTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.emailTextField.placeholder];
    
    self.emailTextField.font =  [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.lblTop.font =  [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionTextSize]];
    
    [self.submitButton setBackgroundColorForDisableStatus];
    //    self.submitButton.backgroundColor = [AppColor normalButtonColor];
    [self.submitButton setBackgroundColorForNormalStatus];
    self.submitButton.enabled = false;
    [self.submitButton setBackgroundColorForTouchingStatus];
    [self.submitButton configureDecorationForButton];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:([AppSize titleTextSize])],
                                 NSKernAttributeName: @([AppSize descriptionLetterSpacing])};
    self.submitButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.submitButton.titleLabel.text attributes:attributes];
    
    [self.emailTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}

#pragma mark - Setup data

-(void) setUpData {
    
}

-(void) createAsteriskForTextField:(UITextField *)textField
{
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 8, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [textField setLeftView:leftView];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
}

#pragma mark - handle keyboard event

-(void)handleKeyboardWillShow:(NSNotification *)paramNotification
{
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         keyboardHeight = keyboardRect.size.height;
                         [self updateEdgeInsetForShowKeyboard];
                         [self layoutIfNeeded];
                     }
                     completion:nil];
}

-(void)handleKeyboardWillHide:(NSNotification *)paramNotification
{
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         [self updateEdgeInsetForHideKeyboard];
                         [self layoutIfNeeded];
                     }
                     completion:nil];
    
}

-(void)updateEdgeInsetForShowKeyboard
{
    _bottomButtonConstraint.constant =  keyboardHeight + 20;
    _heightConstraintHidenView.constant = 0;
    
}


-(void)updateEdgeInsetForHideKeyboard
{
    _bottomButtonConstraint.constant = 20;
    _heightConstraintHidenView.constant = 140;
}



-(void)setTextViewsDefaultBottomBolder
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.emailTextField setBottomBolderDefaultColor];
    });
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAction:(id)sender {
   [_controller successRedirect];
}


#pragma mark - Action for button

-(IBAction)submitButtonTapped:(UIButton*)sender {
    [self dismissKeyboard];
    [self verifyData];
}

-(void)dismissKeyboard {
    [self endEditing:true];
    [self updateEdgeInsetForHideKeyboard];
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    
    //    CGPoint touchPoint = [touch locationInView:touch.view];
    
    UIView *view = touch.view.superview;
    if ([view isKindOfClass:[UITableViewCell class]]) {
        return NO;
    }
    return YES;
}

#pragma mark - UITextField delegate
-(void) makeBecomeFirstResponderForTextField
{
    if(![self.emailTextField.text isValidEmail])
    {
        [self.emailTextField becomeFirstResponder];
    }
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.text = [textField.text removeRedudantWhiteSpaceInText];
    return YES;
}

- (void)textFieldDidChange:(UITextField *)textField {
    [self checkEnableSubmitButton];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return [self updateTextFiel:textField shouldChangeCharactersInRange:range replacementString:string];
}

-(BOOL)updateTextFiel:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(string.length > 1)
    {
        NSMutableString *newString = [[NSMutableString alloc] initWithString:[string removeRedudantWhiteSpaceInText]];
        NSString *resultText = [textField.text stringByReplacingCharactersInRange:range
                                                                       withString:string];
        resultText = [resultText removeRedudantWhiteSpaceInText];
        
        if(textField == self.emailTextField)
        {
            //textField.text = newString;
            textField.text = (resultText.length > 96)?[resultText substringWithRange:NSMakeRange(0, 96)] : resultText;
        }
        
        return NO;
    }
    
    else if(textField == self.emailTextField)
    {
        if([textField.text occurrenceCountOfCharacter:'@'] == 1 && [string isEqualToString:@"@"]){
            return NO;
        }
        if (textField.text.length == 96 && ![string isEqualToString:@""]){
            return NO;
        }
        
        return YES;
    }
    
    return YES;
}

-(void)checkEnableSubmitButton {
    bool is = [_emailTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0;
    [_submitButton setEnabled:is];
}


#pragma mark - Validate fields


-(void)verifyData {
    
    [_controller receiveInputWithInput:_emailTextField.text type:FPErrorInputTypeEmail];
    
    NSMutableString *message = [[NSMutableString alloc] init];
    [self setTextViewsDefaultBottomBolder];
    
    typeof(self) __weak _self = self;
    [_controller verifyDataWithResponse:^(NSArray<NSNumber *> * _Nullable listError) {
        if (!_self) return;
        
        typeof(self) __self = _self;
        if (listError.count == 0) {
            [_self updateEdgeInsetForHideKeyboard];
            [_self setTextViewsDefaultBottomBolder];
            [_self checkEmail];
        } else {
            NSMutableArray* listTextfieldError = [NSMutableArray new];
            [listError enumerateObjectsUsingBlock:^(NSNumber * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSString* errorMsg = nil;
                switch ([obj intValue]) {
                    case FPErrorInputTypeEmail:
                        errorMsg = NSLocalizedString(@"input_invalid_email_msg", nil);
                        [listTextfieldError addObject:__self->_emailTextField];
                        break;
                    default:
                        break;
                }
                (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
                [message appendString:errorMsg];
            }];
            
            NSMutableString *newMessage = [[NSMutableString alloc] init];
            [newMessage appendString:@"\n"];
            [newMessage appendString:message];
            [newMessage appendString:@"\n"];
            
            [_self showAlertWithTitle:@"Please confirm that the value entered is correct:"
                              message:newMessage
                              buttons:@[NSLocalizedString(@"ok_button_title", nil)]
                              actions:@[^(void){
                [_self makeBecomeFirstResponderForTextField];
                [listTextfieldError enumerateObjectsUsingBlock:^(UITextField*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    [obj setBottomBorderRedColor];
                }];
            }]
                     messageAlignment:NSTextAlignmentLeft];
        }
    }];
}

#pragma mark - API

-(void)checkEmail {
    [self startActivityIndicator];
    __weak typeof(self) _self = self;
    
    [_controller checkEmailOKTAWithEmail:_emailTextField.text complete:^(BOOL isExists) {
        if(!_self) return;
        typeof(self) __self = _self;
        if (isExists) {
            [_self stopActivityIndicator];
            [_self showAlertWithTitle:nil
                              message:@"A user account has been found for this email address."
                              buttons:@[NSLocalizedString(@"FORGOT PASSWORD", nil),NSLocalizedString(@"SIGN IN", nil)]
                              actions:@[^{
                [_self gotoSignIn:true];
            },^{
                [_self gotoSignIn:false];
            }]
                     messageAlignment:NSTextAlignmentCenter];
            
        } else {
            [__self->_controller getProfilePartyPMAWithEmail:__self->_emailTextField.text complete:^(PMAUser * _Nullable pmaUser, NSError * _Nullable error) {
                if (!_self) return;
                dispatch_async(dispatch_get_main_queue(), ^{
                    typeof(self) __self = _self;
                    [_self stopActivityIndicator];
                    if (pmaUser) {
                        [_self showAlertWithTitle:nil
                                          message:@"A user account has been found for this email address."
                                          buttons:@[NSLocalizedString(@"OK", nil)]
                                          actions:@[^{
                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                            CreateProfileViewController *createProfileViewController = [storyboard instantiateViewControllerWithIdentifier:@"CreateProfileViewController"];
                            
                            // set value
                            createProfileViewController.emailChecked = __self->_emailTextField.text;
                            createProfileViewController.partyId = pmaUser.partyID;
                            
                            if (pmaUser.partyPerson.firstName.length > 0) {
                                createProfileViewController.firstNameChecked = pmaUser.partyPerson.firstName;
                            }
                            
                            if (pmaUser.partyPerson.lastName.length > 0) {
                                createProfileViewController.lastNameChecked = pmaUser.partyPerson.lastName;
                            }
                            
                            if ([pmaUser.contacts.phones count] > 0) {
                                if (pmaUser.contacts.phones.firstObject.phoneNumber.length > 0) {
                                    createProfileViewController.phoneChecked = pmaUser.contacts.phones.firstObject.phoneNumber;
                                }
                            }
                            
                            [__self->_controller.navigationController pushViewController:createProfileViewController animated:YES];
                        }]
                                 messageAlignment:NSTextAlignmentCenter];
                    } else {
                        [_self onNotFoundEmailAccount:error];
                    }
                });
            }];
        }
    }];
}

- (void) onNotFoundEmailAccount:(NSError *) error {
    if (error != nil) {
        NSString* message = @"";
        switch ([AspireApiError getErrorTypeAPISignupFromError:error]) {
            case NETWORK_UNAVAILABLE_ERR:
                [self showErrorNetwork];
                return;
                break;
            default:
                message = NSLocalizedString(@"error_api_message", nil);
                break;
        }
        if (message.length == 0) return;
        [self showAlertWithTitle:NSLocalizedString(@"alert_error_title", nil)
                         message:message
                         buttons:@[NSLocalizedString(@"ok_button_title", nil)]
                         actions:@[]
                messageAlignment:NSTextAlignmentCenter];
    } else {
        [self showAlertWithTitle:nil
                         message:@"Please create your account."//@"An account with this email address has not been found. Click OK to create a new account."
                         buttons:@[NSLocalizedString(@"OK", nil)]
                         actions:@[^{
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            CreateProfileViewController *createProfileViewController = [storyboard instantiateViewControllerWithIdentifier:@"CreateProfileViewController"];
            createProfileViewController.emailChecked = self.emailTextField.text;
            [self.controller.navigationController pushViewController:createProfileViewController animated:YES];
        }]
                messageAlignment:NSTextAlignmentCenter];
    }
}

#pragma mark - PRIVATE
- (void) gotoSignIn:(BOOL) gotoForgotPasswordImmediately {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UDASignInViewController *signinViewController = [storyboard instantiateViewControllerWithIdentifier:@"UDASignInViewController"];
    signinViewController.shouldGotoForgotPasswordImmediately = gotoForgotPasswordImmediately;
    UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:signinViewController];
    AppDelegate* appDelegate= (AppDelegate*)[[UIApplication sharedApplication] delegate];
    appDelegate.window.rootViewController = navigationController;
    [appDelegate.window makeKeyAndVisible];
}

- (void)handlePopRecognizer:(UIPanGestureRecognizer*)recognizer {
    
    [self updateEdgeInsetForHideKeyboard];
    
    CGPoint velocity = [recognizer translationInView:self.view];
    if(velocity.x < 0)
    {
       
        if(self.interactivePopTransition)
        {
            [self.interactivePopTransition cancelInteractiveTransition];
            self.interactivePopTransition = nil;
        }
        
        return;
    }
    
#ifdef DEBUG
    NSLog(@"Location x: %f",velocity.x);
#endif
    // Calculate how far the user has dragged across the view
    CGFloat progress = [recognizer translationInView:self.view].x / (self.view.bounds.size.width * 1.0);
    progress = MIN(1.0, MAX(0.0, progress));
    
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        // Create a interactive transition and pop the view controller
        //        [self hidenKeyboardWhenSwipe];
        self.interactivePopTransition = [[UIPercentDrivenInteractiveTransition alloc] init];
        [self.controller.navigationController popViewControllerAnimated:YES];
    }
    else if (recognizer.state == UIGestureRecognizerStateChanged) {
        // Update the interactive transition's progress
        
#ifdef DEBUG
        NSLog(@"UIGestureRecognizerStateChanged :%f",progress);
#endif
        [self.interactivePopTransition updateInteractiveTransition:progress];
    }
    else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
        // Finish or cancel the interactive transition
        if (progress > 0.5) {
           
#ifdef DEBUG
            NSLog(@"finishInteractiveTransition");
#endif
            [self.interactivePopTransition finishInteractiveTransition];
        }
        else {
            
#ifdef DEBUG
            NSLog(@"cancelInteractiveTransition");
#endif
            [self.interactivePopTransition cancelInteractiveTransition];
            
        }
        self.interactivePopTransition = nil;
    }
}

@end
