//
//  PasswordTextField.m
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 8/17/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "PasswordTextField.h"

@interface PasswordTextField() {
}

@end

@implementation PasswordTextField

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender{
    return NO;
}

- (BOOL)becomeFirstResponder {
    
    BOOL became = [super becomeFirstResponder];
    if (became) {
        NSString *originalText = [self text];
        [self deleteBackward];
        [self setText:originalText];
        [self insertText:@""];
    }
    return became;
}

@end

