//
//  StoreUserDefaults.h
//  MobileConciergeUSDemo
//
//  Created by HieuNguyen on 10/31/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoreUserDefaults : NSObject
+ (void) saveEmail :(id ) email;
+ (NSString*) email ;
@end
