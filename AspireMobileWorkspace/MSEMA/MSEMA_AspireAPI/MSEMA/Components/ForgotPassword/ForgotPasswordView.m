//
//  ForgotPasswordViewController.m
//  MobileConciergeUSDemo
//
//  Created by Den on 8/3/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "ForgotPasswordView.h"
#import "UITextField+Extensions.h"
#import "UIView+Extension.h"
#import "UIButton+Extension.h"
#import "ChallengeQuestionObject.h"
#import "NSString+Utis.h"
#import "AppColor.h"
#import "UtilStyle.h"
#import "AlertViewController.h"
#import "CustomTextField.h"
#import "DropDownView.h"
#import "PasswordTextField.h"
#import "Common.h"
#import "Constant.h"
#import "AppSize.h"
#import "StyleConstant.h"
#import "UILabel+Extension.h"
#import "StoreUserDefaults.h"
@import AspireApiFramework;

@import AspireApiControllers;

@interface ForgotPasswordView () <DropDownViewDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate> {
    CGFloat keyboardHeight;
    UITapGestureRecognizer *tappedOutsideKeyboards;
    NSMutableArray *questionArray;
    NSMutableArray *questionTextArray;
    ChallengeQuestionObject *selectedChallengeQuestion;
    
}

@end

@implementation ForgotPasswordView

- (void)setController:(AACForgotPasswordController *)controller {
    _controller = controller;
    if (_controller) {
        self.vc = _controller;
        typeof(self) __weak _self = self;
    
        _controller.onViewWillAppear= ^{
            if (!_self) return;
            [_self viewWillAppear];
        };
        
        _controller.onViewWillDisappear= ^{
            if (!_self) return;
            [_self viewWillDisappear];
        };
    }
}
- (void)config {
    
    if(!self.didUpdateLayout && !self.isIgnoreScaleView){
        self.didUpdateLayout = YES;
        resetScaleViewBaseOnScreen(self.view);
    }
    
    [self setUpData];
    [self setUIStyte];
    
    
}

-(void) viewWillAppear {
    [self setTextViewsDefaultBottomBolder];
    tappedOutsideKeyboards = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tappedOutsideKeyboards.delegate = self;
    [_controller.navigationController.view addGestureRecognizer:tappedOutsideKeyboards];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}


- (void)viewWillDisappear {
    [_controller.navigationController.view removeGestureRecognizer:tappedOutsideKeyboards];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    
}

#pragma mark - Setup Layout



- (IBAction)backAction:(id)sender {
    [_controller successRedirect];
}


- (void) setUIStyte {
    
    [self.lblNote setLineSpacing:6];
    [self.lblTop setLineSpacing:6];
    
    self.titleLable.attributedText = [UtilStyle setTextStyleForTitleViewControllerWithMessage:@"FORGOT PASSWORD"];
    
    self.view.backgroundColor = [AppColor backgroundColor];
    
    self.emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    
    
    self.answerTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    self.emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    
    [self createAsteriskForTextField:self.emailTextField];
    //    [self createAsteriskForTextField:self.questionDropdown];
    [self createAsteriskForTextField:self.answerTextField];
    [self createAsteriskForTextField:self.passwordTextField];
    [self createAsteriskForTextField:self.confirmPasswordTextField];
    
    self.questionDropdown.leadingTitle = 8.0f;
    self.questionDropdown.titleFontSize = TEXTFIELD_FONT_SIZE_STANDARD * SCREEN_SCALE;
    self.questionDropdown.title = @"Security Question";
    self.questionDropdown.listItems = questionTextArray;
    self.questionDropdown.titleColor = [AppColor placeholderTextColor];
    self.questionDropdown.isBreakLine = true;
    self.questionDropdown.delegate = self;
    self.questionDropdown.itemLineBreakMode = NSLineBreakByWordWrapping;
    self.questionDropdown.itemsFont = [UIFont fontWithName:TEXTFIELD_FONT_NAME_STANDARD size:TEXTFIELD_FONT_SIZE_STANDARD * SCREEN_SCALE];
    self.questionDropdown.titleLabel.font = [UIFont fontWithName:TEXTFIELD_FONT_NAME_STANDARD size:TEXTFIELD_FONT_SIZE_STANDARD * SCREEN_SCALE];
    [self.questionDropdown setItemHeight:50];
    [self.questionDropdown setItemDisplay:5];
    
    self.emailTextField.delegate = self;
    self.answerTextField.delegate = self;
    self.passwordTextField.delegate = self;
    self.confirmPasswordTextField.delegate = self;
    
    
    self.emailTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.emailTextField.text];
    self.answerTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.answerTextField.text];
    self.passwordTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.passwordTextField.text];
    self.confirmPasswordTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.confirmPasswordTextField.text];
    
    
    self.emailTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.emailTextField.placeholder];
    self.answerTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.answerTextField.placeholder];
    self.passwordTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.passwordTextField.placeholder];
    self.confirmPasswordTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.confirmPasswordTextField.placeholder];
    
    self.submitButton.enabled = false;
    [self.submitButton setBackgroundColorForDisableStatus];
    [self.submitButton setBackgroundColorForNormalStatus];
    [self.submitButton setBackgroundColorForTouchingStatus];
    [self.submitButton configureDecorationForButton];
    
    [self.emailTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.answerTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.passwordTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.confirmPasswordTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}


-(void) createAsteriskForTextField:(UITextField *)textField
{
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 8, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [textField setLeftView:leftView];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
}

-(void)setTextViewsDefaultBottomBolder
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.emailTextField setBottomBolderDefaultColor];
        [self.answerTextField setBottomBolderDefaultColor];
        [self.passwordTextField setBottomBolderDefaultColor];
        [self.confirmPasswordTextField setBottomBolderDefaultColor];
        [self.questionDropdown setBottomBolderDefaultColor];
    });
}

#pragma mark - Setup data

-(void) setUpData {
    questionArray = [[NSMutableArray alloc] init];
    questionTextArray = [[NSMutableArray alloc] init];
    
    id allKeys = [ModelAspireApiManager getSecurityQuetions];
    
    for (int i=0; i<[allKeys count]; i++) {
        NSDictionary *arrayResult = [allKeys objectAtIndex:i];
        ChallengeQuestionObject *object = [[ChallengeQuestionObject alloc] initFromDict:arrayResult];
        [questionArray addObject:object];
        [questionTextArray addObject:object.questionText];
    }
}


#pragma mark - handle keyboard event

-(void)handleKeyboardWillShow:(NSNotification *)paramNotification
{
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         keyboardHeight = keyboardRect.size.height;
                         [self updateEdgeInsetForShowKeyboard];
                         [self layoutIfNeeded];
                     }
                     completion:nil];
    
    [self.questionDropdown didTapBackground];
}

-(void)handleKeyboardWillHide:(NSNotification *)paramNotification
{
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         [self updateEdgeInsetForHideKeyboard];
                         [self layoutIfNeeded];
                     }
                     completion:nil];
    
}


-(void)updateEdgeInsetForShowKeyboard
{
    _bottomButtonConstraint.constant =  keyboardHeight + 20;
    _heightConstraintHidenView.constant = 0;
    self.bottomScrollView.constant = 10;
    
}


-(void)updateEdgeInsetForHideKeyboard
{
    _bottomButtonConstraint.constant = 20;
    _heightConstraintHidenView.constant = 140;
    self.bottomScrollView.constant = 0;
}

-(void)dismissKeyboard {
    [self endEditing:true];
    [self.questionDropdown didTapBackground];
    [self updateEdgeInsetForHideKeyboard];
}


#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    
    //    CGPoint touchPoint = [touch locationInView:touch.view];
    
    UIView *view = touch.view.superview;
    if ([view isKindOfClass:[UITableViewCell class]]) {
        return NO;
    }
    return YES;
}

#pragma mark - Dropdown delegate
- (void)didSelectItem:(DropDownView *)dropDownView atIndex:(int)index {
    
    
    //    NSString *valueSelected = [dropDownView.listItems objectAtIndex:index];
    selectedChallengeQuestion = [questionArray objectAtIndex:index];
    self.answerTextField.text = @"";
    [self checkEnableSubmitButton];
    //    _questionDropdown.title = valueSelected;
    
    //    [self setButtonStatus:[self isChangeData]];
    
}

-(void)didShow:(DropDownView *)dropDownView {
    [self endEditing:true];
}

#pragma mark - UITextField delegate


-(void) makeBecomeFirstResponderForTextField
{
    if(![self.emailTextField.text isValidEmail])
    {
        [self.emailTextField becomeFirstResponder];
    }
    else if([self.passwordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0) //(![self.passwordTextField.text isValidStrongPassword])
    {
        [self.passwordTextField becomeFirstResponder];
    }
    else if(![self.confirmPasswordTextField.text isEqualToString:self.passwordTextField.text])
    {
        [self.confirmPasswordTextField becomeFirstResponder];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if ([self.questionDropdown isOpen]) {
        [self.questionDropdown didTapBackground];
    }
    
    if (self.scrollView.contentOffset.y < self.emailTextField.frame.origin.y) {
        self.scrollView.contentOffset = CGPointMake(0, self.emailTextField.frame.origin.y - 15);
    }
    
    return true;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.text = [textField.text removeRedudantWhiteSpaceInText];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.passwordTextField || textField == self.confirmPasswordTextField) {
        if ([string isEqualToString:@" "]) {
            return NO;
        }
    }
    return [self updateTextFiel:textField shouldChangeCharactersInRange:range replacementString:string];
}

-(void)checkTextFieldIsChange:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
}

- (void)textFieldDidChange:(UITextField *)textField {
    [self checkEnableSubmitButton];
}

-(BOOL)updateTextFiel:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(string.length > 1)
    {
        NSMutableString *newString = [[NSMutableString alloc] initWithString:[string removeRedudantWhiteSpaceInText]];
        NSString *resultText = [textField.text stringByReplacingCharactersInRange:range
                                                                       withString:string];
        resultText = [resultText removeRedudantWhiteSpaceInText];
        
        
        if(textField == self.passwordTextField || textField == self.confirmPasswordTextField)
        {
            if([newString isValidStrongPassword])//isValidStrongPassword
            {
                textField.text = newString;
            }
            else if(string.length > 25)
            {
                textField.text = [newString substringToIndex:25];
            }
            else
            {
                textField.text = newString;
            }
        }
        
        
        if(textField == self.emailTextField)
        {
            //textField.text = newString;
            textField.text = (resultText.length > 96)?[resultText substringWithRange:NSMakeRange(0, 96)] : resultText;
        }
        
        return NO;
    }
    
    
    if(textField == self.passwordTextField || textField == self.confirmPasswordTextField)
    {
        if(textField.text.length == 25 && ![string isEqualToString:@""])
        {
            return NO;
        }
        return YES;
    }
    
    else if(textField == self.emailTextField)
    {
        if([textField.text occurrenceCountOfCharacter:'@'] == 1 && [string isEqualToString:@"@"]){
            return NO;
        }
        if (textField.text.length == 96 && ![string isEqualToString:@""]){
            return NO;
        }
        
        return YES;
    }
    
    return YES;
}

-(void)checkEnableSubmitButton {
    bool is = [_emailTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0 && [_answerTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0 && [_passwordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0 && [_confirmPasswordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0 && selectedChallengeQuestion != nil;
    [_submitButton setEnabled:is];
    
//    self.submitButton.backgroundColor = is ? [AppColor highlightButtonColor] :[AppColor normalButtonColor];
}


#pragma mark - Action for button

-(IBAction)submitButtonTapped:(UIButton*)sender {
    [self dismissKeyboard];
    [self verifyData];
}



#pragma mark - Validate fields


-(void)verifyData {
    
    [_controller receiveInputWithInput:_emailTextField.text type:FPErrorInputTypeEmail];
    [_controller receiveInputWithInput:_answerTextField.text type:FPErrorInputTypeAnswer];
    [_controller receiveInputWithInput:_passwordTextField.text type:FPErrorInputTypePassword];
    [_controller receiveInputWithInput:_confirmPasswordTextField.text type:FPErrorInputTypeConfirmPassword];
    
    NSMutableString *message = [[NSMutableString alloc] init];
    [self setTextViewsDefaultBottomBolder];
    
    typeof(self) __weak _self = self;
    [_controller verifyDataWithResponse:^(NSArray<NSNumber *> * _Nullable listError) {
        if (!_self) return;
        
        typeof(self) __self = _self;
        if (listError.count == 0) {
            [_self updateEdgeInsetForHideKeyboard];
            [_self setTextViewsDefaultBottomBolder];
            [_self requestForgotPassword];
        } else {
            NSMutableArray* listTextfieldError = [NSMutableArray new];
            [listError enumerateObjectsUsingBlock:^(NSNumber * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSString* errorMsg = nil;
                switch ([obj intValue]) {
                    case FPErrorInputTypeEmail:
                        errorMsg = NSLocalizedString(@"input_invalid_email_msg", nil);
                        [listTextfieldError addObject:__self->_emailTextField];
                        break;
                    case FPErrorInputTypePassword:
                        errorMsg = NSLocalizedString(@"input_invalid_password_msg", nil);
                        [listTextfieldError addObject:__self->_passwordTextField];
                        break;
                    case FPErrorInputTypeConfirmPassword:
                        errorMsg = NSLocalizedString(@"input_invalid_confirm_password_msg", nil);
                        [listTextfieldError addObject:__self->_confirmPasswordTextField];
                        break;
                    case FPErrorInputTypeAnswer:
                        errorMsg = NSLocalizedString(@"input_invalid_answer_msg", nil);
                        [listTextfieldError addObject:__self->_answerTextField];
                        break;
                    default:
                        break;
                }
                (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
                [message appendString:errorMsg];
            }];
            
            NSMutableString *newMessage = [[NSMutableString alloc] init];
            [newMessage appendString:@"\n"];
            [newMessage appendString:message];
            [newMessage appendString:@"\n"];
            
            AlertViewController *alert = [[AlertViewController alloc] init];
            alert.titleAlert = @"Please confirm that the value entered is correct:";
            alert.msgAlert = newMessage;
            alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
            dispatch_async(dispatch_get_main_queue(), ^{
                alert.seconBtn.hidden = YES;
                alert.midView.alpha = 0.0f;
                alert.lblAlertMessage.textAlignment = NSTextAlignmentLeft;
                [alert.view layoutIfNeeded];
            });
            
            alert.blockFirstBtnAction = ^(void){
                [_self makeBecomeFirstResponderForTextField];
                [listTextfieldError enumerateObjectsUsingBlock:^(UITextField*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    [obj setBottomBorderRedColor];
                }];
            };
            
            [__self->_controller showWithAlert:alert forNavigation:false];
        }
    }];
}

#pragma mark - API

-(void)requestForgotPassword {
    [self startActivityIndicator];
    __weak typeof(self) _self = self;
    [_controller requestForgetPasswordWithEmail:_emailTextField.text
                                       question:[[AAFRecoveryQuestion alloc] initWithQuestion:selectedChallengeQuestion.questionText answer:_answerTextField.text]
                                    newPassword:[[AAFPassword alloc] initWithPassword:_passwordTextField.text]
                                       complete:^(NSError * error) {
                                           if(!_self) return;
                                           
                                           typeof(self) __self = _self;
                                           [_self stopActivityIndicator];
                                           if (!error) {
                                               AlertViewController *alert = [[AlertViewController alloc] init];
                                               alert.msgAlert = @"Your password has been successfully updated!";
                                               alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
                                               alert.blockFirstBtnAction = ^{
                                                   [StoreUserDefaults saveEmail:_emailTextField.text];
                                                   [__self->_controller successRedirect];
                                               };
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   alert.seconBtn.hidden = YES;
                                                   alert.midView.alpha = 0.0f;
                                               });
                                               
                                               [__self->_controller showWithAlert:alert forNavigation:false];
                                               
                                           } else {
                                               UITextField *currentTextField = nil;
                                               NSString* msg = @"Enter a valid email address.";
                                               NSString* title = @"Please confirm that the value entered is correct:";
                                               switch ([AspireApiError getErrorTypeAPIForgetPasswordFromError:error]) {
                                                   case NETWORK_UNAVAILABLE_ERR:
                                                       msg = @"";
                                                       [_self showErrorNetwork];
                                                       break;
                                                   case EMAIL_INVALID_ERR: {
                                                       msg = @"A user account with this email address cannot be found. Would you like to try again?";
                                                       [_self.emailTextField setBottomBorderRedColor];
                                                       currentTextField = _emailTextField;
                                                       
                                                       AlertViewController *alert = [[AlertViewController alloc] init];
                                                       alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);;
                                                       alert.msgAlert = msg;
                                                       alert.firstBtnTitle = NSLocalizedString(@"no_button", nil);
                                                       alert.secondBtnTitle = NSLocalizedString(@"yes_button", nil);
                                                       
                                                       alert.blockFirstBtnAction = ^{
                                                           [__self->_controller successRedirect];
                                                       };
                                                       
                                                       alert.blockSecondBtnAction = ^{
                                                           __self->_emailTextField.text = @"";
                                                           __self->_confirmPasswordTextField.text = @"";
                                                           __self->_answerTextField.text = @"";
                                                           __self->_passwordTextField.text = @"";
                                                           __self->_confirmPasswordTextField.text = @"";
                                                           __self->_questionDropdown.title = @"Security Question";
                                                           __self->_questionDropdown.titleColor = [AppColor placeholderTextColor];
                                                           __self->selectedChallengeQuestion = nil;
                                                           [_self setTextViewsDefaultBottomBolder];
                                                           [_self checkEnableSubmitButton];
                                                       };
                                                       
                                                       [__self->_controller showWithAlert:alert forNavigation:false];
                                                       msg = @"";
                                                       break;
                                                   }
                                                   case RECOVERYQUESTION_ERR:
                                                   case ANSWERQUESTION_ERR: {
                                                       currentTextField = _answerTextField;
                                                       msg = @"The answer to the Security question is not correct. Would you like to try again?";
                                                       [_questionDropdown setBottomBorderRedColor];
                                                       [_answerTextField setBottomBorderRedColor];
                                                       AlertViewController *alert = [[AlertViewController alloc] init];
                                                       alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);
                                                       alert.msgAlert = msg;
                                                       alert.firstBtnTitle = NSLocalizedString(@"no_button", nil);
                                                       alert.secondBtnTitle = NSLocalizedString(@"yes_button", nil);
                                                       
                                                       alert.blockFirstBtnAction = ^{
                                                           [__self->_controller successRedirect];
                                                       };
                                                       
                                                       alert.blockSecondBtnAction = ^{
                                                           __self->_confirmPasswordTextField.text = @"";
                                                           __self->_answerTextField.text = @"";
                                                           __self->_passwordTextField.text = @"";
                                                           __self->_confirmPasswordTextField.text = @"";
                                                           __self->_questionDropdown.title = @"Security Question";
                                                           __self->_questionDropdown.titleColor = [AppColor placeholderTextColor];
                                                           __self->selectedChallengeQuestion = nil;
                                                           [_self setTextViewsDefaultBottomBolder];
                                                           [_self checkEnableSubmitButton];
                                                       };
                                                       
                                                       [__self->_controller showWithAlert:alert forNavigation:false];
                                                       msg = @"";
                                                       break;
                                                   }
                                                   case FORGETPW_ERR: {
                                                       currentTextField = _passwordTextField;
                                                       title = @"Password does not meet all requirements";
                                                       msg = NSLocalizedString(@"input_invalid_change_password_msg", nil);
                                                       
                                                       AlertViewController *alert = [[AlertViewController alloc] init];
                                                       alert.titleAlert = title;
                                                       alert.msgAlert = msg;
                                                       alert.secondBtnTitle = NSLocalizedString(@"no_button", nil);
                                                       alert.firstBtnTitle = NSLocalizedString(@"alert_ok_button", nil);
                                                       
//                                                       alert.blockSecondBtnAction = ^{
//                                                           [__self->_controller successRedirect];
//                                                       };
                                                       
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           alert.seconBtn.hidden = YES;
                                                           alert.midView.alpha = 0.0f;
                                                           alert.lblAlertMessage.textAlignment = NSTextAlignmentCenter;
                                                           [alert.view layoutIfNeeded];
                                                       });
                                                       
                                                       alert.blockFirstBtnAction = ^{
//                                                           __self->_confirmPasswordTextField.text = @"";
//                                                           __self->_answerTextField.text = @"";
//                                                           __self->_passwordTextField.text = @"";
//                                                           __self->_confirmPasswordTextField.text = @"";
//                                                           __self->_questionDropdown.title = @"Security Question";
//                                                           __self->_questionDropdown.titleColor = [AppColor placeholderTextColor];
                                                           [currentTextField becomeFirstResponder];
                                                           [currentTextField setBottomBorderRedColor];
                                                           [_self checkEnableSubmitButton];
                                                       };
                                                       
                                                       [__self->_controller showWithAlert:alert forNavigation:false];
                                                       msg = @"";
                                                       break;
                                                   }
                                                    case FORGETPW_USER_NOT_ALLOWED_ERR:
                                                   {
                                                       currentTextField = __self->_emailTextField;
                                                       title = @"Password does not meet all requirements";
                                                       msg = @"Forgot password not allowed on specified user.";
                                                       
                                                       AlertViewController *alert = [[AlertViewController alloc] init];
                                                       alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);
                                                       alert.msgAlert = msg;
                                                       alert.firstBtnTitle = NSLocalizedString(@"arlet_ok_button", nil);

                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           alert.seconBtn.hidden = YES;
                                                           alert.midView.alpha = 0.0f;
                                                           alert.lblAlertMessage.textAlignment = NSTextAlignmentLeft;
                                                           [alert.view layoutIfNeeded];
                                                       });
                                                       
                                                       alert.blockFirstBtnAction = ^{
//                                                           __self->_emailTextField.text = @"";
//                                                           __self->_confirmPasswordTextField.text = @"";
//                                                           __self->_answerTextField.text = @"";
//                                                           __self->_passwordTextField.text = @"";
//                                                           __self->_confirmPasswordTextField.text = @"";
//                                                           __self->_questionDropdown.title = @"Security Question";
//                                                           __self->_questionDropdown.titleColor = [AppColor placeholderTextColor];
//                                                           __self->selectedChallengeQuestion = nil;
                                                           [currentTextField becomeFirstResponder];
                                                           [currentTextField setBottomBorderRedColor];
                                                           [_self checkEnableSubmitButton];
                                                       };
                                                       
                                                       [__self->_controller showWithAlert:alert forNavigation:false];
                                                       msg = @"";
                                                       break;
                                                   }
                                                       break;
                                                    case PW_ERR:
                                                   {
                                                       currentTextField = __self->_passwordTextField;
                                                       title = @"Password does not meet all requirements";
                                                       msg = NSLocalizedString(@"input_invalid_password_msg", nil);
                                                       
                                                       AlertViewController *alert = [[AlertViewController alloc] init];
                                                       alert.titleAlert = title;
                                                       alert.msgAlert = msg;
                                                       alert.secondBtnTitle = NSLocalizedString(@"no_button", nil);
                                                       alert.firstBtnTitle = NSLocalizedString(@"arlet_ok_button", nil);
                                                       
//                                                       alert.blockSecondBtnAction = ^{
//                                                           [__self->_controller successRedirect];
//                                                       };
                                                       
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           alert.seconBtn.hidden = YES;
                                                           alert.midView.alpha = 0.0f;
                                                           alert.lblAlertMessage.textAlignment = NSTextAlignmentLeft;
                                                           [alert.view layoutIfNeeded];
                                                       });
                                                       
                                                       alert.blockFirstBtnAction = ^{
//                                                           __self->_confirmPasswordTextField.text = @"";
//                                                           __self->_answerTextField.text = @"";
//                                                           __self->_passwordTextField.text = @"";
//                                                           __self->_confirmPasswordTextField.text = @"";
//                                                           __self->_questionDropdown.title = @"Security Question";
//                                                           __self->_questionDropdown.titleColor = [AppColor placeholderTextColor];
//                                                           __self->selectedChallengeQuestion = nil;
                                                           [currentTextField becomeFirstResponder];
                                                           [currentTextField setBottomBorderRedColor];
                                                           [_self checkEnableSubmitButton];
                                                       };
                                                       
                                                       [__self->_controller showWithAlert:alert forNavigation:false];
                                                       msg = @"";
                                                       break;
                                                   }
                                                   default:
                                                       msg = @"";
                                                       [_self showApiErrorWithMessage:@""];
                                                       break;
                                               }
                                           }
                                       }];
}
@end
