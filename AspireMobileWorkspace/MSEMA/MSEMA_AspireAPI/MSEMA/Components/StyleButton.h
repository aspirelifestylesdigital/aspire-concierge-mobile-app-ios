//
//  StyleButton.h
//  MobileConcierge
//
//  Created by 😱 on 7/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "Common.h"

#define BUTTON_TEXT_COLOR_NORMAL_STANDARD           @"#FFFFFF"
#define BUTTON_TEXT_COLOR_HIGHLIGHT_STANDARD        @"#FFFFFF"
#define BUTTON_BG_COLOR_NORMAL_STANDARD             @"#FF4000"
#define BUTTON_BG_COLOR_HIGHLIGHT_STANDARD          @"#FF8C56"

#define BUTTON_TEXT_COLOR_NORMAL_SMALL              @"#FFFFFF"
#define BUTTON_TEXT_COLOR_HIGHLIGHT_SMALL           @"#FFFFFF"
#define BUTTON_BG_COLOR_NORMAL_SMALL                @"#FF4000"
#define BUTTON_BG_COLOR_HIGHLIGHT_SMALL             @"#FF8C56"

#define BUTTON_TEXT_COLOR_NORMAL_LARGER              @"#FFFFFF"
#define BUTTON_TEXT_COLOR_HIGHLIGHT_LARGER           @"#FFFFFF"
#define BUTTON_BG_COLOR_NORMAL_LARGER                @"#FF4000"
#define BUTTON_BG_COLOR_HIGHLIGHT_LARGER             @"#FF8C56"

#define BUTTON_FONT_NAME_STANDARD           FONT_MarkForMC_MED
#define BUTTON_FONT_NAME_SMALL              FONT_MarkForMC_MED
#define BUTTON_FONT_NAME_LARGER             FONT_MarkForMC_REGULAR

#define BUTTON_FONT_SIZE_STANDARD           18
#define BUTTON_FONT_SIZE_SMALL              14
#define BUTTON_FONT_SIZE_LARGER             24

// HOME CONTROLLER
#define BUTTON_HOME_FONT_NAME               FONT_MarkForMC_BOLD
#define BUTTON_HOME_FONT_SIZE               16
#define BUTTON_HOME_TEXT_COLOR_NORMAL       @"#272726"
#define BUTTON_HOME_TEXT_COLOR_HIGHLIGHT    @"#FF4000"
