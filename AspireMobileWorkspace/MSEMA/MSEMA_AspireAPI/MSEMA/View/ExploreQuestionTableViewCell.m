//
//  ExploreQuestionTableViewCell.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/31/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "ExploreQuestionTableViewCell.h"
#import "Common.h"
#import "UtilStyle.h"
#import "StyleConstant.h"

@implementation ExploreQuestionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.nameLabel.textColor = colorFromHexString(BUTTON_BG_COLOR_NORMAL);
    self.addressLabel.textColor = [UIColor colorWithRed:77/255.0f green:107/255.0f blue:133/255.0f alpha:1.0f];//colorFromHexString(@"#253847");
    self.descriptionLabel.textColor = colorFromHexString(DEFAULT_BACKGROUND_COLOR);
    [self.nameLabel setFont:[UIFont fontWithName:FONT_MarkForMC_MED size:16.0 * SCREEN_SCALE]];
    [self.addressLabel setFont:[UIFont fontWithName:FONT_MarkForMC_MED size:16.0 * SCREEN_SCALE]];
    [self.descriptionLabel setFont:[UIFont fontWithName:FONT_MarkForMC_MED size:16.0 * SCREEN_SCALE]];
    [self.contentView setBackgroundColor:[UIColor whiteColor]];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    UIView *view = [self viewWithTag:1000];
    if(!view)
    {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - 1, self.bounds.size.width, 1.0f)];
        [view setBackgroundColor:[UtilStyle colorForSeparateLine]];
        view.tag = 1000;
        [self addSubview:view];
    }
    else
    {
        view.frame = CGRectMake(0, self.bounds.size.height - 1, self.bounds.size.width, 1.0f);
    }
    if(self.delegate) {
        if(self.tag == [self.delegate BaseTableViewCellWhatCellSelected:self]) {
            [self.contentView setBackgroundColor:colorFromHexString(@"#E5EBEE")];
            [view setBackgroundColor:[UtilStyle colorForSeparateLine]];
        }
    }
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    UIView *view = [self viewWithTag:1000];
    if(view)
    {
        [view setBackgroundColor:[UtilStyle colorForSeparateLine]];
    }
    if(highlighted) {
        [self.contentView setBackgroundColor:colorFromHexString(@"#E5EBEE")];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    // Configure the view for the selected state
    [super setSelected:selected animated:animated];
    if(selected)
    {
        UIView *view = [self viewWithTag:1000];
        if(view)
        {
            [view setBackgroundColor:[UtilStyle colorForSeparateLine]];
        }
        [self.contentView setBackgroundColor:colorFromHexString(@"#E5EBEE")];
    }
}
- (void)prepareForReuse {
    [super prepareForReuse];
    [self.contentView setBackgroundColor:[UIColor whiteColor]];
}
@end
