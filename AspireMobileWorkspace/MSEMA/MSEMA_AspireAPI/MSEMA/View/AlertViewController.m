//
//  AlertViewController.m
//  MobileConcierge
//
//  Created by Mac OS on 5/31/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "AlertViewController.h"
#import "UIButton+Extension.h"
#import "Constant.h"
#import "AppSize.h"

@interface AlertViewController () <UIScrollViewDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate>

- (IBAction)touchOk:(id)sender;
- (IBAction)touchCancel:(id)sender;

@end

@implementation AlertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.viewAlert.layer.cornerRadius = 8;
    self.lblAlertTitle.text = self.titleAlert;
    self.lblAlertMessage.text = self.msgAlert;
    
    [self.lblAlertMessage setFont:[UIFont fontWithName:FONT_MarkForMC_LIGHT size:18.0f*SCREEN_SCALE]];
    
    [self.firstBtn setTitle:self.firstBtnTitle forState:UIControlStateNormal];
    if(self.secondBtnTitle)
    {
        [self.seconBtn setTitle:self.secondBtnTitle forState:UIControlStateNormal];
    }
    [self setUpCustomizedPanGesturePopRecognizer];
    
    [self.seconBtn.titleLabel setAdjustsFontSizeToFitWidth:true];
    [self.firstBtn.titleLabel setAdjustsFontSizeToFitWidth:true];
    
    [self.lblAlertMessage setFont:[UIFont fontWithName:FONT_MarkForMC_LIGHT size:18.0f*SCREEN_SCALE]];
    [self.lblAlertTitle setFont:[UIFont fontWithName:FONT_MarkForMC_BOLD size:[AppSize titleTextSize]]];
}

-(void)setUpCustomizedPanGesturePopRecognizer
{
    self.navigationController.delegate = self;
    UIPanGestureRecognizer *popRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePopRecognizer:)];
    [self.view addGestureRecognizer:popRecognizer];
    
    UIPanGestureRecognizer *popNavigationBarRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePopRecognizer:)];
    [self.navigationController.navigationBar addGestureRecognizer:popNavigationBarRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)handlePopRecognizer:(UIPanGestureRecognizer*)recognizer{
    
}

- (IBAction)touchOk:(id)sender {
    if(self.blockFirstBtnAction){
        self.blockFirstBtnAction();
    }
    else{
        [self.alertViewControllerDelegate alertOkAction];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:NO completion:nil];
    });
}

- (IBAction)touchCancel:(id)sender {
    if(self.blockSecondBtnAction){
        self.blockSecondBtnAction();
    }
    else{
        [self.alertViewControllerDelegate alertCancelAction];
    }
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
