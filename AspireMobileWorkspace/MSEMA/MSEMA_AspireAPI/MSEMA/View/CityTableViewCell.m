//
//  CityTableViewCell.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "CityTableViewCell.h"
#import "CityItem.h"
#import "CategoryItem.h"
#import "Common.h"
#import "StyleConstant.h"

@interface CityTableViewCell(){
    
    __weak IBOutlet UIView *bottomLInt;
}

@end

@implementation CityTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.name.textColor = colorFromHexString(@"#002b51"); //#011627
    self.name.font = [UIFont fontWithName:FONT_MarkForMC_MED size:18.0f * SCREEN_SCALE];
    self.address.textColor =  colorFromHexString(@"#002b51");//#253847
    self.address.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:16.0f * SCREEN_SCALE];
    [self.contentView setBackgroundColor:[UIColor whiteColor]];
    
}

- (void) setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    [super setHighlighted:highlighted animated:animated];
    if(highlighted)
    {
        [self.contentView setBackgroundColor:colorFromHexString(@"#E5EBEE")]; //#E5EBEE
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    if(selected)
    {
       [self.contentView setBackgroundColor:colorFromHexString(@"#E5EBEE")];//#E5EBEE
    }
}

-(void) initCellWithData:(CityItem *)item
{
    self.name.text = [item.name uppercaseString];
    self.address.text = item.address;
    self.avartaImage.image = item.image;
}

-(void)initCellWithCategoryData:(CategoryItem *)item
{
    self.name.text = item.categoryName;
    self.address.text = nil;
    self.avartaImage.image = item.categoryImg;
    
//    self.address.textColor = [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1];
}

- (void)setHiddenBottomLine:(BOOL)isHide {
    bottomLInt.hidden = isHide;
}

- (void)prepareForReuse {
    bottomLInt.hidden = YES;
    [self.contentView setBackgroundColor:[UIColor whiteColor]];
}
@end
