//
//  VenueTableViewCell.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/11/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "VenueTableViewCell.h"

@implementation VenueTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self initView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)initView
{
    [self.heartBtn setImage:[UIImage imageNamed:@"heart_icon"] forState:UIControlStateNormal];
    [self.calendarBtn setImage:[UIImage imageNamed:@"calendar_icon"] forState:UIControlStateNormal];
    [self.conciergeBtn setImage:[UIImage imageNamed:@"concierge_icon"] forState:UIControlStateNormal];
    [self.shareBtn setImage:[UIImage imageNamed:@"share_icon"] forState:UIControlStateNormal];
}



@end
