//
//  TableHeaderView.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/21/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "TableHeaderView.h"
#import "Common.h"
#import "Constant.h"
#import "UIButton+Extension.h"
#import "StyleConstant.h"

@implementation TableHeaderView

-(void)setupViewWithMessage:(NSString *)message
{
    resetScaleViewBaseOnScreen(self);
    self.messageLbl.text = message == nil?NSLocalizedString(@"no_result_searching_msg", nil):message;
    self.messageLbl.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:20.0f * SCREEN_SCALE];
    self.messageLbl.textColor = colorFromHexString(@"#99A1A8");
    
    [self.askConciergeBtn setTitle:NSLocalizedString(@"ask_concierge_menu_title", nil) forState:UIControlStateNormal];
    [self.askConciergeBtn setBackgroundColorForNormalStatus];
    [self.askConciergeBtn setBackgroundColorForTouchingStatus];
    [self.askConciergeBtn configureDecorationForButton];
}

@end
