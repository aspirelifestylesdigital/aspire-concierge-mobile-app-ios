//
//  SearchHeaderView.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/11/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "SearchHeaderView.h"
#import "Common.h"
#import "Constant.h"
#import "StyleConstant.h"
#import "UIView+Extension.h"

@interface SearchHeaderView(){
    
    IBOutlet UIStackView *stBook;
    UIView *line;
    IBOutlet UIView *vwOffer;
}

@end

@implementation SearchHeaderView

- (void) awakeFromNib {
    [super awakeFromNib];
    for (NSLayoutConstraint* c in self.listConstraintsHaveScale) {
        c.constant = c.constant*SCREEN_SCALE;
    }
}

-(void)setupView
{
    [self.offerClear setImage:[UIImage imageNamed:@"clear_icon"] forState:UIControlStateNormal];
    [self.bookOnlineClear setImage:[UIImage imageNamed:@"clear_icon"] forState:UIControlStateNormal];
    
    [self.bookOnlineText setText:NSLocalizedString(@"can_book_search", nil)];
    [self.offerText setText:NSLocalizedString(@"offer_search", nil)];
    
    UIColor *color = colorFromHexString(DEFAULT_BACKGROUND_COLOR);
    self.searchText.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:18.0f * SCREEN_SCALE];
    self.searchText.textColor = color;
    self.searchText.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search1" attributes:@{NSForegroundColorAttributeName: colorFromHexString(@"#99A1A8")}];
    self.offerText.font = [UIFont fontWithName:FONT_MarkForMC_MED size:18.0f * SCREEN_SCALE];
    self.offerText.textColor = color;
//    [self.searchTextView setbackgroundColorForLineMenu];
    
    self.searchTextView.backgroundColor = colorFromHexString(DEFAULT_BACKGROUND_COLOR);
    
    UIButton *clearButton = [self.searchText valueForKey:@"_clearButton"];
    [clearButton setImage:[UIImage imageNamed:@"clear_icon"] forState:UIControlStateNormal];
    [clearButton setImage:[UIImage imageNamed:@"clear_icon"] forState:UIControlStateHighlighted];
    
    self.searchText.clearButtonMode = UITextFieldViewModeAlways;
    self.searchText.returnKeyType = UIReturnKeySearch;
    
    [vwOffer setHidden:!self.isSearchOffer];
    if(line) [line setHidden:!self.isSearchOffer];
}

- (void)resetState {
    self.isSearchOffer = NO;
    self.searchText.text = @"";
    [self setupView];
}

-(void)setBottomLine
{
    if(!line) {
        line = [[UIView alloc] initWithFrame:CGRectMake(0.0f, self.frame.size.height - 1, SCREEN_WIDTH, 1.0f)];
        
        [line setBackgroundColor:[UIColor colorWithRed:0.8f green:0.8f blue:0.8f alpha:1.f]];
        [self addSubview:line];
        line.translatesAutoresizingMaskIntoConstraints = NO;
        [self addConstraint:[NSLayoutConstraint constraintWithItem:line attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:line attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1 constant:0]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:line attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:1]];
        [line addConstraint:[NSLayoutConstraint constraintWithItem:line attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:1]];
    }
}

@end
