//
//  CityGuideTableViewCell.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"

@class CityItem;
@class CategoryItem;

@interface CityGuideTableViewCell : BaseTableViewCell

@property(nonatomic, weak) IBOutlet UILabel *name;
@property(nonatomic, weak) IBOutlet UILabel *address;
@property(nonatomic, weak) IBOutlet UIImageView *avartaImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageWidthConstraint;

-(void) initCellWithData:(CityItem *)item;
-(void)initCellWithCategoryData:(CategoryItem *)item;
- (void) setHiddenBottomLine:(BOOL) isHide;
@end
