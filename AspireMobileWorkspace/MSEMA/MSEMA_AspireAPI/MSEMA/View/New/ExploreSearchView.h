//
//  ExploreSearchView.h
//  Test
//
//  Created by 😱 on 7/17/17.
//  Copyright © 2017 dai.pham.s3corp.com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ExploreSearchView;
@protocol ExploreSearchViewProtocol <NSObject>

@optional

@end

@interface ExploreSearchView : UIView

@property (weak, nonatomic) IBOutlet UIStackView *stackView;

@property (weak,nonatomic) id <ExploreSearchViewProtocol> delegate;
@end
