//
//  ExploreSearchView.m
//  Test
//
//  Created by 😱 on 7/17/17.
//  Copyright © 2017 dai.pham.s3corp.com.vn. All rights reserved.
//

//Module
#import "MenuSearch.h"

#import "ExploreSearchView.h"

@interface ExploreSearchView()<MenuSearchProtocol> {
    MenuSearch* searchExplore;
}

@end

@implementation ExploreSearchView

#pragma mark - 🚸 INIT 🚸
- (id)init
{
    self = [super init];
    
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
    }
    return self;
}

- (void) awakeFromNib {
    [super awakeFromNib];
    
    // Declare Search Module
    searchExplore = [MenuSearch new];
    [searchExplore setOptionSearchType:OptionSearchTypeCheckbox];
    searchExplore.delegate = self;
    [searchExplore setAttributeTextSearch:nil withListOptionSearchs:@[@"",@"",@""]];
    
    [self addSubview: searchExplore];
    searchExplore.translatesAutoresizingMaskIntoConstraints = NO;
    [self addConstraint:[NSLayoutConstraint constraintWithItem:searchExplore attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:searchExplore attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:searchExplore attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1 constant:0]];
    
    [_stackView insertArrangedSubview:searchExplore atIndex:0];
    
}

#pragma mark - 🚸 SEARCH DELEGATE 🚸

- (void) MenuSearch:(MenuSearch *)view onClearOptionSearch:(id)optionSearch {
}
@end
