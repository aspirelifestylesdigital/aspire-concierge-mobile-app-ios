//
//  TableSimpleView.m
//  TestIOS
//
//  Created by Dai Pham on 7/17/17.
//  Copyright © 2017 Dai Pham. All rights reserved.
//

// cell
#import "CitiesListCell.h"

#import "TableSimpleView.h"

@interface TableSimpleView()<UITableViewDelegate, UITableViewDataSource> {
    NSArray* datas;
}

@end

@implementation TableSimpleView

#pragma mark - 🚸 INIT 🚸
- (instancetype)init {
    if(self = [super init]) {
        self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
    }
    
    return self;
}

- (void) awakeFromNib {
    [super awakeFromNib];

    _tblList.estimatedRowHeight = 100.f;
    _tblList.rowHeight = UITableViewAutomaticDimension;
    
    _tblList.delegate =self;
    _tblList.dataSource = self;
    
    // register cell for table
    [_tblList registerNib:[UINib nibWithNibName:NSStringFromClass([CitiesListCell class]) bundle:nil] forCellReuseIdentifier:@"cell"];
}

#pragma mark - 🚸 INTERFACE 🚸
- (void) reloadWithData:(NSArray *)data {
    
    datas = data;
    [_tblList reloadData];
}

#pragma mark - 🚸 TABLEVIEW DELEGATE 🚸
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return datas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CitiesListCell* cell = (CitiesListCell*)[tableView dequeueReusableCellWithIdentifier:@"cell"];
    [cell presentObject:[datas objectAtIndex:indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row > datas.count - 1)
        return;
    
    if(self.delegate)
       [self.delegate TableSimpleView:self onSelectItem:[datas objectAtIndex:indexPath.row]];
}
@end
