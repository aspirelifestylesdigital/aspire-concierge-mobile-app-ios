//
//  CollectSimpleView.h
//  Test
//
//  Created by Dai Pham on 7/19/17.
//  Copyright © 2017 dai.pham.s3corp.com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CollectSimpleView;

@protocol CollectSimpleViewProtocol <NSObject>

@optional
- (void) CollectSimpleView:(CollectSimpleView*)view onSelectItem:(id)item;

@end

@interface CollectSimpleView : UIView
@property (weak,nonatomic) id<CollectSimpleViewProtocol> delegate;
@end
