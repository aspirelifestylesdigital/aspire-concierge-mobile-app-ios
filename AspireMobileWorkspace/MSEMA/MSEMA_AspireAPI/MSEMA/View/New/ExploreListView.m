//
//  ExploreListView.m
//  Test
//
//  Created by 😱 on 7/14/17.
//  Copyright © 2017 dai.pham.s3corp.com.vn. All rights reserved.
//

//Module
#import "MenuExplore.h"
#import "MenuSearch.h"

#import "ExploreListView.h"

@interface ExploreListView()<UITableViewDelegate,UITableViewDataSource, MenuExploreProtocol, MenuSearchProtocol> {
    MenuExplore* menuExplore;
    MenuSearch* searchExplore;
}

@end

@implementation ExploreListView

#pragma mark - 🚸 INIT 🚸
- (id)init
{
    self = [super init];
    
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
    }
    return self;
}

- (void) awakeFromNib {
    [super awakeFromNib];
    
    // Declare Search Module
    searchExplore = [MenuSearch new];
    searchExplore.delegate = self;
    [searchExplore setAttributeTextSearch:nil withListOptionSearchs:@[@"",@"",@""]];
    
    // Declare Menu Module
    menuExplore = [MenuExplore new];
    menuExplore.delegate = self;
    
    for (UIView* view in @[searchExplore,menuExplore]) {
        [_stackView insertArrangedSubview:view atIndex:0];
    }
}

#pragma mark - 🚸 MENU DELEGATE 🚸
// get Interaction From Menu Button
- (void) MenuExplore:(MenuExplore *)view onSelectButton:(UIButton *)button atIndex:(NSInteger)index  {
    //do anything when user touch button on menu explore
    switch (index) {
        case 1:
        case 2:
            if(self.delegate)
                [self.delegate ExploreListView:self onSelectWithButtonIndex:index];
            break;
        case 3:
            if(self.delegate)
                [self.delegate ExploreListViewOnNavigateToViewSearch:self];
            break;
        default:
            break;
    }
}


#pragma mark - 🚸 SEARCH DELEGATE 🚸
- (BOOL) MenuSearchIsTextFieldShouldBeginEditing:(MenuSearch *)view {
    if([view isEqual:searchExplore]) {
        // send command navigation to search view controller
        if(self.delegate)
            [self.delegate ExploreListViewOnNavigateToViewSearch:self];
        return NO;
    }
    return YES;
}

- (void) MenuSearch:(MenuSearch *)view onClearOptionSearch:(id)optionSearch {
    // do anything when user clear a object option search
    [searchExplore setAttributeTextSearch:nil withListOptionSearchs:@[@"",@"",@"",@"",@""]];
}

@end
