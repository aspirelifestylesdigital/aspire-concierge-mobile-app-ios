//
//  LocationComponent.m
//  Test
//
//  Created by Dai Pham on 2/24/17.
//  Copyright © 2017 Dai Pham. All rights reserved.
//

#import "LocationComponent.h"
#import "SessionData.h"
#import "AnswerItem.h"
#import "AppData.h"
#import "SyncService.h"
#import "AppCoreData.h"

#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLGeocoder.h>
#import <math.h>

@interface LocationComponent()<CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
    CLGeocoder  *geocoder;
    CLLocation* location;
    BOOL isGettingDis;
    BOOL isGettingLocation;
    BOOL isDenied;
    
    NSMutableArray* listQueueWhileGetLocation;
    
    NSInteger numberItemsCheck;
}

@end

@implementation LocationComponent

static LocationComponent *_getLocation;

#pragma mark - INIT
+ (id) getLocation {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _getLocation = [LocationComponent new];
        printf("%s",[[NSString stringWithFormat:@"%s START LOCATION SERVICE\n",__PRETTY_FUNCTION__] UTF8String]);
    });
    
    if([[SessionData shareSessiondata] isUseLocation] && !_getLocation->isDenied) {
        [_getLocation startUpdatingLocation];
    }
    
    return _getLocation;
}

+ (void)startRequestLocation {
    if(!_getLocation) {
        [LocationComponent getLocation];
    }
    [_getLocation startUpdatingLocation];
}

- (instancetype) init {
    if(self == [super init]) {
        listWaitingGetDistances = [NSMutableArray new];
        againGetDistances = [NSMutableArray new];
        listQueueWhileGetLocation = [NSMutableArray new];
    }
    
    // listern location done then retry get location for all items
    self.onGetLocationDone = ^{
        printf("%s",[[NSString stringWithFormat:@"\nSTART GOT DISTANCE FOR ITEMS AFTER GOT LOCATION %@\n",_getLocation->listQueueWhileGetLocation] UTF8String]);
        if(_getLocation->listQueueWhileGetLocation.count > 0) {
            for (NSDictionary* item in _getLocation->listQueueWhileGetLocation) {
                [_getLocation getDistanceForObject:[item objectForKey:@"object"] fromAddress:[item objectForKey:@"address"] onDone:[item objectForKey:@"callback"]];
            }
            [_getLocation->listQueueWhileGetLocation removeAllObjects];
        }
    };
    
    return self;
}

#pragma mark - INTERFACE
+ (void) staticGetDistanceForObject:(id)object fromAddress:(NSString*) address onDone:(void (^)(id obj,CGFloat, double lng, double lati)) onDone {
    
    if(![[SessionData shareSessiondata] isUseLocation])
        return;
    
    if(!_getLocation && [[SessionData shareSessiondata] isUseLocation]) {
        [LocationComponent getLocation];
    }
    
    if(!_getLocation->location && !_getLocation->isDenied) {
        [_getLocation startUpdatingLocation];
    }
    
    [_getLocation getDistanceForObject:object fromAddress:address onDone:onDone];
}

+ (void)setNumbersItemCheck:(NSInteger)numberItems {
    if(!_getLocation)
        [LocationComponent getLocation];
    _getLocation->numberItemsCheck = numberItems;
}

+ (BOOL)isGettingDistance {
    if(!_getLocation)
        return NO;
    return _getLocation->isGettingDis;
}

#pragma mark - PRIVATE
- (BOOL) isQueueGettingDistance {
    return listWaitingGetDistances.count != 0;
}

- (void) callbackCheckingDistance {
    if(numberItemsCheck > 0)
        numberItemsCheck--;
    if(![self isQueueGettingDistance] && numberItemsCheck == 0) {
        
        printf("%s",[@"\nCompleted process get distance, involke onChecking block\n" UTF8String]);
        isGettingDis = NO;
        if(self.onChecking)
            self.onChecking();
        
    }
}

- (void) getDistanceForObject:(id)object fromAddress:(NSString*) address onDone:(void (^)(id obj,CGFloat, double lng, double lati))onDone {
    isGettingDis = YES;
    
    // add request to list, when request all list done then involke block resort data
    [listWaitingGetDistances addObject:object];
    
    [self geoCodeUsingAddress:address withObject:(id)object onCompleted:^(double longitude, double latitude){
        AnswerItem* item = (AnswerItem*)object;
        if(longitude == 0 && latitude == 0 && ![againGetDistances containsObject:object]) {
            [againGetDistances addObject:object];
            
            [listWaitingGetDistances removeObject:object];
            item.shouldUseNameInsteadAddress = YES;
            [[LocationComponent getLocation] getDistanceForObject:item fromAddress:[[item.name stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""] onDone:onDone];
            return;
        } else
            [againGetDistances removeObject:object];
        
        item.addressForMap = address;
        
        if(!location || ![[SessionData shareSessiondata] isUseLocation]) {
            item.distance = CGFLOAT_MAX;
            item.longitude = longitude;
            item.latitude = latitude;
            item.isWaitingGetDistance = NO;
            
            [listWaitingGetDistances removeObject:object];
            [self callbackCheckingDistance];
            return;
        }
        CLLocation *restaurantLoc = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
        CLLocationDistance meters = [restaurantLoc distanceFromLocation:location];
        
        item.distance = meters;
        item.longitude = longitude;
        item.latitude = latitude;
        item.isWaitingGetDistance = NO;
        
        if(listWaitingGetDistances.count > 0)
            [listWaitingGetDistances removeObject:object];
        [self callbackCheckingDistance];
    }];
}

- (void) geoCodeUsingAddress:(NSString *)address withObject:(id)object onCompleted:(void (^)(double longitude, double latitude))onCompleted
{
    // if address has got distance => return data
    NSDictionary* dataLocal = [AppCoreData getLocationForAddress:address];
    if(dataLocal) {
        NSString* longg = [dataLocal objectForKey:L_longitude_key];
        NSString* lat = [dataLocal objectForKey:L_latitude_key];
        if(onCompleted)
            onCompleted([longg doubleValue],[lat doubleValue]);
        return;
    }
    
    
    NSString *esc_addr =  [address stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    CLGeocoder* geocoder1 = [CLGeocoder new];
    [geocoder1 geocodeAddressString:address completionHandler:^(NSArray* placemarks, NSError* error){
        CLPlacemark *placemark=nil;
        if(placemarks.count > 0) {
            placemark = [placemarks objectAtIndex:0];
        }
        BOOL isCheckGoogle = NO;
        if(placemark) {
            if(![placemark.locality containsString:((BaseItem*)object).cityName] || ![placemark.postalCode isEqualToString:((BaseItem*)object).zipCode]) {
                isCheckGoogle = YES;
            }
        }
        if(error != nil || isCheckGoogle) {
            NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
            [SyncService getUrl:req withTimeout:10 withParams:nil OnDone:^(id response){
                __block double latitude = 0, longitude = 0;
                
                if(![response isKindOfClass:[NSDictionary class]]) {
                    if(onCompleted) {
                        onCompleted(longitude,latitude);
                    }
                    return;
                }
                
                NSDictionary* result = (NSDictionary*) response;
                if(![[result allKeys] containsObject:@"results"]) {
                    if(onCompleted) {
                        onCompleted(longitude,latitude);
                    }
                    return;
                }
                
                if(![[[[result objectForKey:@"results"] firstObject] allKeys] containsObject:@"geometry"]) {
                    if(onCompleted) {
                        onCompleted(longitude,latitude);
                    }
                    return;
                }
                
                @try {
                    NSDictionary* locat = [[[[result objectForKey:@"results"] firstObject] objectForKey:@"geometry"] objectForKey:@"location"];
                    
                    latitude = [[locat objectForKey:@"lat"] doubleValue];
                    longitude = [[locat objectForKey:@"lng"] doubleValue];
                    
                    // save location got to local db
                    [self saveLong:[locat objectForKey:@"lng"] andLatitude:[locat objectForKey:@"lat"] forAddress:address];
                    
                    if(onCompleted) {
                        onCompleted(longitude,latitude);
                    }
                    
                } @catch (NSException *exception) {
                    if(onCompleted) {
                        onCompleted(0,0);
                    }
                }
            } onError:^(NSError* err){
                if(onCompleted) {
                    onCompleted(0,0);
                }
            }];
        } else {
            if(placemarks.count > 0) {
                
                CLLocationCoordinate2D coordinates = placemark.location.coordinate;
                
                // save location got to local db
                [self saveLong:[NSString stringWithFormat:@"%f",coordinates.longitude] andLatitude:[NSString stringWithFormat:@"%f",coordinates.latitude] forAddress:address];
                
                if(coordinates.latitude != 0) {
                    if(onCompleted) {
                        onCompleted(coordinates.longitude,coordinates.latitude);
                    }
                    return;
                }
                
            }
            if(onCompleted) {
                onCompleted(0,0);
            }
        }
    }];
}

- (void) saveLong:(NSString*)longitude andLatitude:(NSString*)latitude forAddress:(NSString*) address {
    // save location got to local db
    [AppCoreData saveLongitude:longitude andLatitude:latitude forAddress:address];
}

- (void) startUpdatingLocation {
    // Create the location manager if this object does not
    // already have one.
    
    printf("%s",[@"\nStart UPDATE LOCATION\n" UTF8String]);
    
    isGettingLocation = YES;
    
    if (nil == locationManager) {
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
    }
    [locationManager requestWhenInUseAuthorization];
    [locationManager startUpdatingLocation];
}

- (void) stopUpdatingLocation {
    isGettingLocation = NO;
    [locationManager stopUpdatingLocation];
    if(_getLocation.onGetLocationDone)
        _getLocation.onGetLocationDone();
}

#pragma mark - CLLocationManagerDelegate protocol

// Delegate method from the CLLocationManagerDelegate protocol.
- (void) locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
            location = nil;
            [[NSNotificationCenter defaultCenter] postNotificationName:LOCATION_COMPONENT_ISAUTHORIZED_NOTIFICATION object:nil userInfo:@{LOCATION_COMPONENT_ISAUTHORIZED_NOTIFICATION:@(NO),@"ERROR_CODE":@2}];
            [[NSNotificationCenter defaultCenter] postNotificationName:LOCATION_COMPONENT_ISAUTHORIZED_NOTIFICATION_FOR_EXPLOREVIEW object:nil userInfo:@{LOCATION_COMPONENT_ISAUTHORIZED_NOTIFICATION:@(NO)}];
            _getLocation->isDenied = YES;
            printf("%s",[@"\nPERMISSION LOCATION DENIED\n" UTF8String]);
            [self stopUpdatingLocation];
            break;
        case kCLAuthorizationStatusAuthorizedAlways:
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [[NSNotificationCenter defaultCenter] postNotificationName:LOCATION_COMPONENT_ISAUTHORIZED_NOTIFICATION object:nil userInfo:@{LOCATION_COMPONENT_ISAUTHORIZED_NOTIFICATION:@(YES)}];
            [[NSNotificationCenter defaultCenter] postNotificationName:LOCATION_COMPONENT_ISAUTHORIZED_NOTIFICATION_FOR_EXPLOREVIEW object:nil userInfo:@{LOCATION_COMPONENT_ISAUTHORIZED_NOTIFICATION:@(YES)}];
            _getLocation->isDenied = NO;
            printf("%s",[@"\nPERMISSION LOCATION ALLOW\n" UTF8String]);
            [_getLocation startUpdatingLocation];
            break;
        default:
            break;
    }
}

- (void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    isGettingLocation = NO;
    getLocation = @"Unknown";
    if(_getLocation.onReturnLocation)
        _getLocation.onReturnLocation(@"Unknown");
    if (error.code == 1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:LOCATION_COMPONENT_ISAUTHORIZED_NOTIFICATION object:nil userInfo:@{LOCATION_COMPONENT_ISAUTHORIZED_NOTIFICATION:@(NO), @"ERROR_CODE":@1}];
    }
    [self stopUpdatingLocation];
    
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    // If it's a relatively recent event, turn off updates to save power.
    location = [locations lastObject];
    
    isGettingLocation = NO;
    
    if(!location) {
        getLocation = @"Unknown";
        if(_getLocation.onReturnLocation)
            _getLocation.onReturnLocation(@"Unknown");
        [self stopUpdatingLocation];
        return;
    }
    
    if (!geocoder)
        geocoder = [[CLGeocoder alloc] init];
    
    [geocoder reverseGeocodeLocation:location completionHandler:
     ^(NSArray* placemarks, NSError* error){
         if ([placemarks count] > 0)
         {
             CLPlacemark *placemark = [placemarks objectAtIndex:0];
             [AppData setUserCityLocationDevice:[placemark locality]];
             getLocation = [NSString stringWithFormat:@"%@, %@",NSLocalizedString([placemark locality], nil) ,NSLocalizedString(placemark.country, nil)];
         }
         
         if(getLocation.length == 0) getLocation = @"Unknown";
         if(_getLocation.onReturnLocation)
             _getLocation.onReturnLocation(getLocation);
         [self stopUpdatingLocation];
         
     }];
}
@end
