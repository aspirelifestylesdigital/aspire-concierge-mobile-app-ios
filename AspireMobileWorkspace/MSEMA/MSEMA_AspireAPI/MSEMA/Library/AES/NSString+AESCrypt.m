//
//  NSString+AESCrypt.m
//

#import "NSString+AESCrypt.h"

@implementation NSString (AESCrypt)

- (NSString *)AES256EncryptWithKey:(NSString *)key
{
   NSData *plainData = [self dataUsingEncoding:NSUTF8StringEncoding];
   NSData *encryptedData = [plainData AES256EncryptWithKey:key];
   
   return [encryptedData base64Encoding];
}

- (NSString *)AES256DecryptWithKey:(NSString *)key
{
   NSData *encryptedData = [NSData dataWithBase64EncodedString:self];
   NSData *plainData = [encryptedData AES256DecryptWithKey:key];
   
   return [[NSString alloc] initWithData:plainData encoding:NSUTF8StringEncoding];
}

@end
