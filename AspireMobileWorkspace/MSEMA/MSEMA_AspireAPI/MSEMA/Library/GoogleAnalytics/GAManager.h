//
//  GAManager.h
//  LuxuryCard
//
//  Created by Dai Pham on 9/7/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Google/Analytics.h>
#import <FirebaseCore/FirebaseCore.h>
@import FirebaseAnalytics;

@interface GAManager : NSObject

#pragma mark - 🚸 Config 🚸

/**
 Start Config FIREBase, should involke in AppDelegate

 @return ingore this return
 */
+ (GAManager*) configure;

#pragma mark - 🚸 EVENT 🚸

/**
 log event for googleanalytics

 @param nameEvent read FIREventNames.h from library FirebaseAnalytics
 @param itemInfo read FIREventNames.h base nameEvent to provide familar item info from library FirebaseAnalytics
 */
+ (void) trackingWithEventName:(NSString*const)nameEvent withItemInfo:(NSDictionary*)itemInfo;
@end
