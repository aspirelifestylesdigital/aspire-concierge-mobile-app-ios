//
//  LinkLabel.h
//  TestLink
//
//  Created by Nghia Dinh on 7/18/17.
//  Copyright © 2017 Nghia Dinh. All rights reserved.
//

#import "TTTAttributedLabel.h"


@protocol LinkLabelDelegate <NSObject>
@optional
- (void)didSelectLink:(NSURL*)link;
@end


@interface LinkLabel : TTTAttributedLabel

-(void)setHTMLString:(NSString*)html;
-(void)expandLabel:(BOOL)isExpand;



@end
