//
//  CitiesListCell.m
//  TestIOS
//
//  Created by Dai Pham on 7/17/17.
//  Copyright © 2017 Dai Pham. All rights reserved.
//

#import "CitiesListCell.h"

// components
#import "CitiesManager.h"

// extension
#import "UIView+Extension.h"

// Model
#import "CityItem.h"

// style
#import "ControlStyleHeader.h"

#pragma mark - 🚸 UILabelTitleCitiesListCell 🚸
@interface UILabelTitleCitiesListCell : UILabel@end
@implementation UILabelTitleCitiesListCell
- (void) awakeFromNib {
    [super awakeFromNib];
    
    // set style here
    [self setFont:[UIFont fontWithName:FONT_MarkForMC_MED size:18]];
    self.textColor = colorFromHexString(DEFAULT_BACKGROUND_COLOR);
}
@end

#pragma mark - 🚸 UILabelSubtitleCitiesListCell 🚸
@interface UILabelSubtitleCitiesListCell : UILabel@end
@implementation UILabelSubtitleCitiesListCell
- (void) awakeFromNib {
    [super awakeFromNib];
    
    // set style here
    [self setFont:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:16]];
    self.textColor = colorFromHexString(@"#99a1a8");
}
@end

#pragma mark - 🚸 CitiesListCell 🚸
@interface CitiesListCell(){
    
    __weak IBOutlet UILabel *title;
    __weak IBOutlet UILabel *subtitle;
    __weak IBOutlet UIImageView *imgView;
    __weak IBOutlet UIStackView *stackView;
    __weak IBOutlet UIView *separateLine;
    
    id object;
}

@end

@implementation CitiesListCell

#pragma mark - INIT
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [separateLine setbackgroundColorForLineMenu];
}

#pragma mark - INTERFACE
- (void)presentObject:(id)obj {
    
    object = obj;
    NSString* titleString = @"";
    NSString* subtitleString = @"";
    
    if([obj isKindOfClass:[CityObject class]]) {
        CityObject* o = (CityObject*)obj;
        titleString = o.name;
    } else if([obj isKindOfClass:[CityItem class]]) {
        CityItem* o = (CityItem*)obj;
        titleString = o.name;
        subtitleString = o.address;
        if(o.image) {
            [imgView setImage:o.image];
        }
    }
    title.text = [titleString uppercaseString];
    if(subtitleString.length == 0 || subtitle == nil) {
        subtitle.hidden = YES;
    } else
        subtitle.text = subtitleString;
}

- (void) hiddenSeparateLine {
    separateLine.hidden = YES;
}

#pragma mark - PRIVATE
- (void)prepareForReuse {
    subtitle.hidden = NO;
    separateLine.hidden = NO;
}
@end
