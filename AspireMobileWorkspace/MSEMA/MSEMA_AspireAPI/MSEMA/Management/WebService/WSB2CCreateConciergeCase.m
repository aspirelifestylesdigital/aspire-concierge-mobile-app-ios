//
//  WSB2CCreateNewConciergeCase.m
//  MobileConcierge
//
//  Created by user on 5/15/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSB2CCreateConciergeCase.h"
#import "CreateConciergeCaseResponseObject.h"
#import "UserObject.h"
#import "AskConciergeRequest.h"

@implementation WSB2CCreateConciergeCase
-(void) askConciergeWithMessage:(NSDictionary*)dict
{
//    NSString *editType = dict[@"editType"];
//    NSString* url = nil;
//
//    if([editType isEqualToString:UPDATE_EDIT_TYPE]){
//
//        url = [MCD_API_URL stringByAppendingString:UpdateConciergeRequest];
//    } else if([editType isEqualToString:CANCEL_EDIT_TYPE]){
//        url = [MCD_API_URL stringByAppendingString:UpdateConciergeRequest];
//        self.task = WS_CANCEL_CONCIERGE;
//        self.subTask = WS_ST_NONE;
//    }
//    else if ([editType isEqualToString:ADD_EDIT_TYPE]){
//        url = [MCD_API_URL stringByAppendingString:CreateConciergeRequestUrl];
//    }
//
//
//    [self POST:url withParams:[[NSMutableDictionary alloc] initWithDictionary:[AskConciergeRequest buildRequestMessageWithData:dict]]];
//
//    // return response
    NSString* url = [@"https://api.vipdesk.com/services/" stringByAppendingString:@"concierge/standard/v2.0/Requests/CreateNewConciergeCase"];
    
    [self POST:url withParams:[self buildRequestParamsWithMessage:dict]];
    
    // return response
}
-(NSMutableDictionary*) buildRequestParamsWithMessage:(NSDictionary*)dict{
    NSMutableDictionary *contentDict = [[NSMutableDictionary alloc] init];
    
    
    [contentDict setObject:[self configurationAccessTokenDict] forKey:@"forThisExampleImplementationOnly_ConfigurationAccessToken"];
    [contentDict setObject:[self newCaseRequestDictWithMessage:dict] forKey:@"newCaseRequest"];
#ifdef DEBUG
    NSLog(@"%@",contentDict);
#endif
    return contentDict;
}

- (NSMutableDictionary *)newCaseRequestDictWithMessage:(NSDictionary*)dict{
    
    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[self configurationAccessTokenDict] forKey:@"accessToken"];
    
    [params setValue:[NSNull null] forKey:@"programId"];
    [params setValue:[[SessionData shareSessiondata] OnlineMemberID] forKey:@"memberId"];
    
    if (profileDictionary) {
        NSString *salutation = [profileDictionary objectForKey:keySalutation];
        [params setValue:(salutation.length > 0)?salutation:@"N/A" forKey:@"salutation"];
        [params setValue:[profileDictionary objectForKey:keyFirstName] forKey:@"firstName"];
        [params setValue:[profileDictionary objectForKey:keyLastName] forKey:@"lastName"];
        [params setValue:[profileDictionary objectForKey:keyMobileNumber] forKey:@"phoneNumber"];
        [params setValue:[profileDictionary objectForKey:keyEmail] forKey:@"emailAddress"];
        NSString *requestCity = dict[@"requestCity"];
        NSString *requestType = dict[@"requestType"];
        [params setValue:(requestCity.length > 0)?requestCity:@"N/A" forKey:@"requestCity"];
        [params setValue:(requestType.length > 0)?requestType:@"N/A" forKey:@"requestType"];
        
        NSString *requestDetail = @"";
        
        if ([dict[@"phone"] boolValue] == YES && [dict[@"mail"] boolValue] == YES) {
            requestDetail = [requestDetail stringByAppendingString:[NSString stringWithFormat:@"Respond by %@ or Respond by %@\n\n",[profileDictionary objectForKey:keyMobileNumber],[profileDictionary objectForKey:keyEmail]]];
        } else {
            if ([dict[@"phone"] boolValue] == YES) {
                requestDetail = [requestDetail stringByAppendingString:[NSString stringWithFormat:@"Respond by %@\n\n",[profileDictionary objectForKey:keyMobileNumber]]];
            }else if ([dict[@"mail"] boolValue] == YES) {
                requestDetail = [requestDetail stringByAppendingString:[NSString stringWithFormat:@"Respond by %@\n\n",[profileDictionary objectForKey:keyEmail]]];
            }
        }
        requestDetail = [requestDetail stringByAppendingString:dict[@"message"]];
        [params setValue:requestDetail forKey:@"requestDetails"];
    }
    return params;
}

- (NSMutableDictionary*)configurationAccessTokenDict{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    NSDictionary *keyDict = @{@"key":@"F8C78EC5-E4A6-4D6A-918F-00AA2D7C24BA"}; //@{@"key":@"D8FE24E1-E0D6-4EED-BCE1-231D6CB7F183"};
    
    NSString* userPass = B2C_PW;
    [dict setObject:keyDict forKey:@"licenseKey"];
    [dict setValue:B2C_SUBDOMAIN forKey:@"applicationName"];
    [dict setValue:userPass forKey:@"programPassword"];
    
    return dict;
}

- (NSString*) getJSONStringFromArray:(NSMutableArray*)array{
    NSError* error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:&error];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}
-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        CreateConciergeCaseResponseObject *response = [[CreateConciergeCaseResponseObject alloc] initFromDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && response.isSuccess){
            self.data = [response data];
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.isSuccess];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}
@end
