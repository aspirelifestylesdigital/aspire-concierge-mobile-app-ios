//
//  WSB2CGetContentFullFromCCA.m
//  MobileConcierge
//
//  Created by user on 6/12/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSB2CGetContentFullFromCCA.h"
#import "QuestionResponseObject.h"
#import "SBJsonWriter.h"
#import "ContentFullCCAResponseObject.h"

@implementation WSB2CGetContentFullFromCCA
-(void) loadDataFromServer
{
    NSString* url = [B2C_IA_API_URL stringByAppendingString:GetContentFull];
    [self POST:url withParams:[self buildRequestParams]];
}

-(NSMutableDictionary*) buildRequestParams
{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    
    // cause of security scanning, not allow get and set password directly
    NSString* userPass = B2C_PW;
    [dictKeyValues setObject:self.itemID forKey:@"contentID"];
    [dictKeyValues setObject:B2C_SUBDOMAIN forKey:@"subDomain"];
    [dictKeyValues setObject:userPass forKey:@"password"];

    return dictKeyValues;
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        ContentFullCCAResponseObject *response = [[ContentFullCCAResponseObject alloc] initFromDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && response.isSuccess){
            self.data = [response data];
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:nil withErrorCode:0     errorMessage:response.message];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}

-(void)processDataResultWithError:(NSError *)error
{
    [self.delegate loadDataFailFrom:nil withErrorCode:error.code errorMessage:error.localizedDescription];
}
@end
