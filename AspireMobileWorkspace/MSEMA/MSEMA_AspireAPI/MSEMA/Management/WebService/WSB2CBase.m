//
//  WSB2CBase.m
//  ALC
//
//  Created by Anh Tran on 11/1/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"
#import "WSB2CRefreshToken.h"
#import "WSB2CGetRequestToken.h"
#import "WSB2CGetAccessToken.h"
#import "AppDelegate.h"
#import "UserObject.h"
#import "BaseResponseObject.h"
#import "Common.h"
#import "SBJson.h"
#import "Constant.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "Common.h"
#import "SyncService.h"

#import "AlertViewController.h"
#import "BaseViewController.h"

@interface WSB2CBase(){
    WSB2CRefreshToken *wsRefreshToken;
    WSB2CGetRequestToken *wsRequestToken;
    WSB2CGetAccessToken *wsGetAccessToken;
    UserObject *loggedInUser;
}

@end

@implementation WSB2CBase

-(void)checkAuthenticate{
//    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
//    if([appdele isLoggedIn]){
//        loggedInUser = [appdele getLoggedInUser];
//    }
//    if(appdele.ACCESS_TOKEN!=nil){
//        if ([appdele.B2C_ExpiredAt compare:[NSDate date]] == NSOrderedAscending)
//        {
//            [self refreshTokenAPI:self.task];
//        } else {
//            [self startAPIAfterAuthenticate];
//        }
//    } else if([appdele isLoggedIn]){
//        [self requestToken];
//    } else {
//        NSError *err = [[NSError alloc] init];
//        [self processDataResultWithError:err];
//    }
}

-(void)refreshTokenAPI:(enum WSTASK)aTask
{
    wsRefreshToken = [[WSB2CRefreshToken alloc] init];
    wsRefreshToken.delegate = self;
    [wsRefreshToken refreshAccessToken];
}

-(void)requestToken
{
    if(loggedInUser){
        wsRequestToken = [[WSB2CGetRequestToken alloc] init];
        wsRequestToken.delegate = self;
        [wsRequestToken getRequestToken];
    } else {
        [self userHaventLoggedIn];
    }
}

-(void)requestAccessToken:(NSString*) token{
    if(loggedInUser){
        wsGetAccessToken = [[WSB2CGetAccessToken alloc] init];
        wsGetAccessToken.delegate = self;
        [wsGetAccessToken requestAccessToken:token member:loggedInUser.userId];
    } else {
        [self userHaventLoggedIn];
     }
}

-(void) userHaventLoggedIn{
    NSError* err = [NSError errorWithDomain:@"Error" code:401 userInfo:nil];
    [self processDataResultWithError:err];

}

-(void) nextPage{
    if([self hasNextItem]){
        self.pageIndex++;
        [self checkAuthenticate];
    } else {
        [self.delegate loadDataDoneFrom:self];
    }
}

-(BOOL) hasNextItem{
    return self.isHasNextPage;
}


-(void)loadDataDoneFrom:(WSBase*)ws{
    
    if(ws.task == WS_GET_REFRESH_ACCESS_TOKEN){
        // after refresh token, call next request
        [self startAPIAfterAuthenticate];
    }else if(ws.task == WS_GET_REQUEST_TOKEN){
        [self requestAccessToken:wsRequestToken.requestToken];
    } else if(ws.task == WS_GET_ACCESS_TOKEN){
        [self startAPIAfterAuthenticate];
    }
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    
    NSError* err = [NSError errorWithDomain:@"Error" code:errorCode userInfo:nil];
    if(result && [result.errorCode isEqualToString:B2C_INVALID_ACCESS_TOKEN]){
        // access token is invalid, have to request new token, then request access token again
        [self requestToken];
    } else {
        [self processDataResultWithError:err];
    }
}

-(void)POST:(NSString*) url withParams:(NSMutableDictionary*)params{
    if(!isNetworkAvailable())
    {
        [self showErrorNetwork];
        if([self.delegate respondsToSelector:@selector(WSBaseNetworkUnavailable)]) {
            [self.delegate WSBaseNetworkUnavailable];
        }
        
    } else {
        self.isSyncing = YES;
//        typeof(self) __weak _self = self;
        [SyncService postUrl:url withParams:params OnDone:^(id responseObject){
            [self processDataResults:responseObject forTask:self.task forSubTask:self.subTask returnFormat:400];
            self.isSyncing = NO;
        } onError:^(NSError* error){
            [self processDataResultWithError:error];
            self.isSyncing = NO;
        }];
    }
    
}


-(void)POSTForSearch:(NSString*) url withParams:(NSMutableDictionary*)params{
    if(!isNetworkAvailable())
    {
        [self showErrorNetwork];
        if([self.delegate respondsToSelector:@selector(WSBaseNetworkUnavailable)]) {
            [self.delegate WSBaseNetworkUnavailable];
        }
        
    } else {
        //        typeof(self) __weak _self = self;
        [SyncService postUrl:url withParams:params OnDone:^(id responseObject){
            [self processDataResults:responseObject forTask:self.task forSubTask:self.subTask returnFormat:400];
        } onError:^(NSError* error){
            [self processDataResultWithError:error];
        }];
    }
}
-(void)GET:(NSString*) url withParams:(NSMutableDictionary*)params{
    
    if(!isNetworkAvailable())
    {
        
        [self showErrorNetwork];
        if([self.delegate respondsToSelector:@selector(WSBaseNetworkUnavailable)]) {
            [self.delegate WSBaseNetworkUnavailable];
        }
    } else {
        self.isSyncing = YES;
        [SyncService getUrl:url withParams:params OnDone:^(id responseObject){
            
            [self processDataResults:responseObject forTask:self.task forSubTask:self.subTask returnFormat:400];
            self.isSyncing = NO;
        } onError:^(NSError* error){
            
            [self processDataResultWithError:error];
            self.isSyncing = NO;
        }];
    }
    
}

-(void)cancelRequest{
    
    if(wsRequestToken){
        [wsRequestToken cancelRequest];
    }
    
    if(wsGetAccessToken){
        [wsGetAccessToken cancelRequest];
        
    }
    
    if(wsRefreshToken){
        [wsRefreshToken cancelRequest];
    }
    
    [super cancelRequest];
}

- (void)dealloc
{
    if(wsRequestToken){
        [wsRequestToken cancelRequest];
    }
    
    if(wsGetAccessToken){
        [wsGetAccessToken cancelRequest];
        
    }
    
    if(wsRefreshToken){
        [wsRefreshToken cancelRequest];
    }
    
    [super cancelRequest];
}

#pragma mark - PRIVATE
- (void) showErrorNetwork {
    
    // mark request is done
    self.isSyncing = NO;
    
    id rootViewController = [UIApplication sharedApplication].delegate.window.rootViewController;
    if([rootViewController isKindOfClass:[UINavigationController class]])
    {
        rootViewController = ((UINavigationController *)rootViewController).viewControllers.firstObject;
    }
    
    // return then popup already present.
    if(((UIViewController*)rootViewController).presentedViewController) {
        return;
    }
        
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"cannot_get_data", nil);
    alert.msgAlert = NSLocalizedString(@"no_network_connection", nil);
    alert.firstBtnTitle = NSLocalizedString(@"alert_ok_button", nil);
    alert.blockSecondBtnAction = ^(void){
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"prefs:root"]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root"]];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root"]];
        }
    };
    alert.secondBtnTitle = NSLocalizedString(@"settings_button", nil);

    alert.providesPresentationContextTransitionStyle = YES;
    alert.definesPresentationContext = YES;
    [alert setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    
    
    
    [rootViewController presentViewController:alert animated:YES completion:nil];
}
@end
