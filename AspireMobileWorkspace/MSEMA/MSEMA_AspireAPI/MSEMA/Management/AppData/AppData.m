//
//  AppData.m
//  MobileConcierge
//
//  Created by Home on 5/7/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "AppData.h"
#import <Foundation/Foundation.h>
#import "Constant.h"
#import "CityItem.h"
#import "CategoryItem.h"
#import "AnswerItem.h"
#import "AppDelegate.h"
@import AspireApiFramework;
#import "CCAMapCategories.h"
#import "CCAGeographicManager.h"
#import "PreferenceObject.h"

@interface AppData()
{
    NSArray *cityItemList;
    NSArray *categoryItemList;
    NSArray *cityGuideList;
    NSArray *subCategoryItemList;
    NSArray *defaultAnswers;
    
    NSDictionary* listFeatures;
}

@end

static AppData *sharedMyManager;

@implementation AppData

+ (AppData *)getInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

+ (void)startService {
    if(!sharedMyManager) {
        [AppData getInstance];
    }
    [sharedMyManager createSubCategoryItemList];
    [sharedMyManager createDefaultAnswers];
    [sharedMyManager createSelectionCityList];
    [sharedMyManager createCityGuideList];
}

- (instancetype) init {
    if(self = [super init]) {
        NSString * plistPath = [[NSBundle mainBundle] pathForResource:@"NewFeature" ofType:@"plist"];
        listFeatures = [NSDictionary dictionaryWithContentsOfFile:plistPath];
        [self createSelectionCategoryListForObject:self onDone:^(AppData* item,NSArray* list){
            item->categoryItemList = list;
        }];
        
    }
    return self;
}

+ (NSArray *)getDefaultAnswers
{
    return sharedMyManager->defaultAnswers;
}

-(void)createDefaultAnswers
{
    NSMutableArray *mutableDefaultAnswers = [[NSMutableArray alloc] init];
    AnswerItem *item = [[AnswerItem alloc] init];
    item.answerName = @"Charlie Palmer Steak (Midtown)";
    item.address3 = @"Midtown";
    item.offer2 = @"Complimentary Glass of Sparkling Wine for each guest";
    item.imageURL = @"https://assets.vipdesk.com/universal/images/diningnetwork/newyork/NYC_CHARLIE_PALMER_STEAK_FINAL.JPG";
    
    [mutableDefaultAnswers addObject:item];
    
    item = [[AnswerItem alloc] init];
    item.answerName = @"Megu (Chelsea)";
    item.address3 = @"Chelsea";
    item.offer2 = @"A Complimentary Appetizer or Dessert for the Table";
    item.imageURL = @"https://assets.vipdesk.com/universal/images/diningnetwork/newyork/NYC_MEGU_FINAL.JPG";
    
    [mutableDefaultAnswers addObject:item];
    
    defaultAnswers = mutableDefaultAnswers;
}

+(NSArray *)getSubCategoryItemList
{
    return sharedMyManager->subCategoryItemList;
}

-(void)createSubCategoryItemList
{
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSMutableArray *subCategories = [[NSMutableArray alloc] init];
    
        CategoryItem *category = [[CategoryItem alloc] init];
        category.categoryName = @"ACCOMMODATIONS";
        category.code = @"accommodations";
        category.categoryImg = [UIImage imageNamed:@"asset_cityguide"];
        category.categoryImgDetail = [UIImage imageNamed:@"asset_cityguide_detail"];
        [subCategories addObject:category];
    
        
        category = [[CategoryItem alloc] init];
        category.categoryName = @"BARS/CLUBS";
        category.code = @"bars/clubs";
        category.categoryImg = [UIImage imageNamed:@"bar_club_cityguide"];
        category.categoryImgDetail = [UIImage imageNamed:@"bar_club_cityguide_detail"];
        [subCategories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = @"CULTURE";
        category.code = @"culture";
        category.categoryImg = [UIImage imageNamed:@"culture_cityguide"];
        category.categoryImgDetail = [UIImage imageNamed:@"culture_cityguide_detail"];
        [subCategories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = @"DINING";
        category.code = @"dining";
        category.categoryImg = [UIImage imageNamed:@"dining_cityguide"];
        category.categoryImgDetail = [UIImage imageNamed:@"dining_cityguide_detail"];
        [subCategories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = @"SHOPPING";
        category.code = @"shopping";
        category.categoryImg = [UIImage imageNamed:@"shopping_cityguide"];
        category.categoryImgDetail = [UIImage imageNamed:@"shopping_cityguide_detail"];
        [subCategories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = @"SPAS";
        category.code = @"spas";
        category.categoryImg = [UIImage imageNamed:@"spa_wellness_cityguide"];
        category.categoryImgDetail = [UIImage imageNamed:@"spa_wellness_cityguide_detail"];
        [subCategories addObject:category];
        subCategoryItemList = subCategories;
}

+(NSArray *)getSelectionCityList
{
    for(CityItem *item in sharedMyManager->cityItemList)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cityCode == %@",item.name];
        NSArray *tempArray = [sharedMyManager->cityGuideList filteredArrayUsingPredicate:predicate];
        
        if(tempArray.count > 0)
        {
            item.ID = ((CityItem *)[tempArray objectAtIndex:0]).ID;
        }
        
    }
    
    return sharedMyManager-> cityItemList;
}

+ (NSArray *)getSelectionCategoryList
{
    return sharedMyManager->categoryItemList;
}

+(NSArray *)getCityGuideList
{
    return sharedMyManager->cityGuideList;
}


-(void)createCityGuideList
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSMutableArray *cityGuides = [[NSMutableArray alloc] init];
        //    CategoryItem *category = [[CategoryItem alloc] init];
        //    category.ID = @"";
        //    category.categoryName = @"Atlanta";
        //    category.code = @"Atlanta";
        //    category.categoryImg = [UIImage imageNamed:@"atlanta"];
        //    [cityGuides addObject:category];
        
        CityItem *city = [[CityItem alloc] init];
        city.ID = @"6311";
        city.name = @"Boston";
        city.cityCode = @"Boston";
        city.image = [UIImage imageNamed:@"boston"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6308";
        city.name = @"Chicago";
        city.cityCode = @"Chicago";
        city.image = [UIImage imageNamed:@"chicago"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6445";
        city.name = @"Dallas";
        city.cityCode = @"Dallas";
        city.image = [UIImage imageNamed:@"dallas"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6310";
        city.name = @"Las Vegas";
        city.cityCode = @"Las Vegas";
        city.image = [UIImage imageNamed:@"lasvegas"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6314";
        city.name = @"London";
        city.cityCode = @"London";
        city.image = [UIImage imageNamed:@"london"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6312";
        city.name = @"Los Angeles";
        city.cityCode = @"Los Angeles";
        city.image = [UIImage imageNamed:@"losangeles"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6307";
        city.name = @"Miami";
        city.cityCode = @"Miami";
        city.image = [UIImage imageNamed:@"miami"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6429";
        city.name = @"Montreal";
        city.cityCode = @"Montreal";
        city.image = [UIImage imageNamed:@"montreal"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6306";
        city.name = @"New York";
        city.cityCode = @"New York";
        city.image = [UIImage imageNamed:@"new_york"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6389";
        city.name = @"Orlando";
        city.cityCode = @"Orlando";
        city.image = [UIImage imageNamed:@"orlando"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6315";
        city.name = @"Paris";
        city.cityCode = @"Paris";
        city.image = [UIImage imageNamed:@"paris"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6458";
        city.name = @"Rome";
        city.cityCode = @"Rome";
        city.image = [UIImage imageNamed:@"rome"];
        [cityGuides addObject:city];
        
        //    category = [[CategoryItem alloc] init];
        //    category.ID = @"";
        //    category.categoryName = @"San Diego";
        //    category.code = @"San Diego";
        //    category.categoryImg = [UIImage imageNamed:@"sandiego"];
        //    [cityGuides addObject:category];
        
        city = [[CityItem alloc] init];
        city.ID = @"6313";
        city.name = @"San Francisco";
        city.cityCode = @"San Francisco";
        city.image = [UIImage imageNamed:@"sanfrancisco"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6444";
        city.name = @"Seattle";
        city.cityCode = @"Seattle";
        city.image = [UIImage imageNamed:@"seattle"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6428";
        city.name = @"Toronto";
        city.cityCode = @"Toronto";
        city.image = [UIImage imageNamed:@"toronto"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6459";
        city.name = @"Vancouver";
        city.cityCode = @"Vancouver";
        city.image = [UIImage imageNamed:@"vancouver"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6309";
        city.name = @"Washington, D.C.";
        city.cityCode = @"Washington, DC";
        city.image = [UIImage imageNamed:@"washington"];
        [cityGuides addObject:city];
        dispatch_async(dispatch_get_main_queue(), ^{
            cityGuideList = cityGuides;
        });
    });
}

-(void)createSelectionCategoryListForObject:(id)object onDone:(void(^)(id object,NSArray* list))onDone
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSMutableArray *categories = [[NSMutableArray alloc] init];
        
        CategoryItem *category = [[CategoryItem alloc] init];
        category.categoryName = @"";
        category.code = @"all";
        category.categoryImg = [UIImage imageNamed:@"all_category"];
        category.categoriesForService = LIST_ALL_TILES;
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.ID = @"2";
        category.categoryName = @"DINING";
        category.code = @"dining";
        category.categoryImg = [UIImage imageNamed:@"dining"];
        /*
        [CCAMapCategories getCategoriesIDsDining:category onDone:^(CategoryItem* item,NSArray* list){
            item.categoryIDs = list;
        }];
         */
        category.categoriesForService = @[@"Dining",@"Specialty Travel"];
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = @"HOTELS";
        category.code = @"hotels";
    category.isCheckGeographicRegion = YES;
        category.categoryImg = [UIImage imageNamed:@"hotel"];
        category.categoriesForService = @[@"Hotels"];
        /*
        [CCAMapCategories getCategoriesIDsHotel:category onDone:^(CategoryItem* item,NSArray* list){
            item.categoryIDs = list;
        }];
         */
        [categories addObject:category];
        
        /*
         category = [[CategoryItem alloc] init];
         category.ID = @"3";
         category.categoryName = @"MASTERCARD \nTRAVEL \nSERVICES";
         category.code = @"mastercard travel";
         category.categoryImg = [UIImage imageNamed:@"mastercard_travel"];
         [categories addObject:category];
         */
        
        category = [[CategoryItem alloc] init];
        category.categoryName = @"FLOWERS";
        category.code = @"flowers";
        category.categoryImg = [UIImage imageNamed:@"flowers"];
        category.categoriesForService = @[@"Flowers"];
        /*
        [CCAMapCategories getCategoriesIDsFlower:category onDone:^(CategoryItem* item,NSArray* list){
            item.categoryIDs = list;
        }];
         */
        [categories addObject:category];
        
        
        category = [[CategoryItem alloc] init];
        category.categoryName = [@"entertainment" uppercaseString];
        category.code = @"entertainment";
        category.categoryImg = [UIImage imageNamed:@"entertainment"];
        category.categoriesForService = @[@"Tickets",@"Specialty Travel"];
        /*
        [CCAMapCategories getCategoriesIDsEntertainment:category onDone:^(CategoryItem* item,NSArray* list){
            item.categoryIDs = list;
        }];
         */
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = @"GOLF";
        category.code = @"golf";
        category.ID = @"7";
        category.categoryImg = [UIImage imageNamed:@"golf"];
        category.categoriesForService = @[@"Golf",@"Golf Merchandise",@"Specialty Travel"];
        /*
        [CCAMapCategories getCategoriesIDsGolf:category onDone:^(CategoryItem* item,NSArray* list){
            item.categoryIDs = list;
        }];
         */
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = [@"Vacation \nPackages" uppercaseString];
        category.code = @"vacation packages";
    category.isCheckGeographicRegion = YES;
        category.categoryImg = [UIImage imageNamed:@"vacation"];
        category.categoriesForService = @[@"Vacation Packages"];
        /*
        [CCAMapCategories getCategoriesIDsVacationPackages:category onDone:^(CategoryItem* item,NSArray* list){
            item.categoryIDs = list;
        }];
         */
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = [@"cruise" uppercaseString];
        category.code = @"cruise";
        category.categoryImg = [UIImage imageNamed:@"cruise"];
        category.isCheckGeographicRegion = YES;
        category.categoriesForService = @[@"Cruises"];
        /*
        [CCAMapCategories getCategoriesIDsCruise:category onDone:^(CategoryItem* item,NSArray* list){
            item.categoryIDs = list;
        }];
         */
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = @"TOURS";
        category.code = @"tours";
        category.categoryImg = [UIImage imageNamed:@"tour"];
        category.categoriesForService = @[@"VIP Travel Services"];
        /*
        [CCAMapCategories getCategoriesIDsTour:category onDone:^(CategoryItem* item,NSArray* list){
            item.categoryIDs = list;
        }];
         */
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = @"TRAVEL";
        category.code = @"travel";
        category.categoryImg = [UIImage imageNamed:@"travel"];
        category.categoriesForService = @[@"Private Jet Travel"];
        /*
        [CCAMapCategories getCategoriesIDsTravel:category onDone:^(CategoryItem* item,NSArray* list){
            item.categoryIDs = list;
        }];
         */
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.ID = @"12";
        category.categoryName = [@"Airport \nservices" uppercaseString];
        category.code = @"airport services";
        category.categoryImg = [UIImage imageNamed:@"airport_service"];
        category.categoriesForService = @[@"VIP Travel Services"];
        /*
        [CCAMapCategories getCategoriesIDsAirportService:category onDone:^(CategoryItem* item,NSArray* list){
            item.categoryIDs = list;
        }];
         */
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = [@"travel \nservices" uppercaseString];
        category.code = @"travel services";
        category.categoryImg = [UIImage imageNamed:@"travel_service"];
        category.categoriesForService = @[@"VIP Travel Services"];
        /*
        [CCAMapCategories getCategoriesIDsTravelService:category onDone:^(CategoryItem* item,NSArray* list){
            item.categoryIDs = list;
        }];
         */
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = [@"transportation" uppercaseString];
        category.code = @"transportation";
        category.categoryImg = [UIImage imageNamed:@"transportation"];
        category.categoriesForService = @[@"Transportation"];
        /*
        [CCAMapCategories getCategoriesIDsTransportation:category onDone:^(CategoryItem* item,NSArray* list){
            item.categoryIDs = list;
        }];
         */
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = @"SHOPPING";
        category.code = @"shopping";
        category.categoryImg = [UIImage imageNamed:@"shopping"];
        category.categoriesForService = @[@"Golf Merchandise",@"Flowers",@"Wine",@"Retail Shopping"];
        /*
        [CCAMapCategories getCategoriesIDsShopping:category onDone:^(CategoryItem* item,NSArray* list){
            item.categoryIDs = list;
        }];
         */
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.ID = @"5";
        category.categoryName = @"CITY GUIDE";
        category.code = @"city guide";
        category.categoryImg = [UIImage imageNamed:@"city_guide"];
        [categories addObject:category];
        dispatch_async(dispatch_get_main_queue(), ^{
            if(onDone)
                onDone(object,categories);
            //    categoryItemList = categories;
        });
    });
}

-(void)createSubCategoriesWithIDs:(NSArray *)categoryIDs forObject:(id)object onDone:(void (^)(id obj, NSArray* list)) onDone
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSMutableArray *subCategories = [[NSMutableArray alloc] init];
        for(NSInteger i = 0; i< categoryIDs.count; i++)
        {
            CategoryItem *item = [[CategoryItem alloc] init];
            item.ID = [categoryIDs objectAtIndex:i];
            item.supperCategoryID = @"80";
            switch (i) {
                 /*
                case 0:
                    item.categoryName = @"ACCOMMODATIONS";
                    item.code = @"accommodation";
                    item.categoryImg =  [UIImage imageNamed:@"asset_cityguide"];
                    item.categoryImgDetail =  [UIImage imageNamed:@"asset_cityguide_detail"];
                    break;
                  */
                    
                case 0:
                    item.categoryName = @"DINING";
                    item.code = @"dining";
                    item.categoryImg =  [UIImage imageNamed:@"dining_cityguide"];
                    item.categoryImgDetail =  [UIImage imageNamed:@"dining_cityguide_detail"];
                    break;
                case 1:
                    item.categoryName = @"BARS/CLUBS";
                    item.code = @"bars";
                    item.categoryImg =  [UIImage imageNamed:@"bar_club_cityguide"];
                    item.categoryImgDetail =  [UIImage imageNamed:@"bar_club_cityguide_detail"];
                    break;
                case 2:
                    item.categoryName = @"SPAS";
                    item.code = @"spas";
                    item.categoryImg =  [UIImage imageNamed:@"spa_wellness_cityguide"];
                    item.categoryImgDetail =  [UIImage imageNamed:@"spa_wellness_cityguide_detail"];
                    break;
                case 3:
                    item.categoryName = @"SHOPPING";
                    item.code = @"shopping";
                    item.categoryImg =  [UIImage imageNamed:@"shopping_cityguide"];
                    item.categoryImgDetail =  [UIImage imageNamed:@"shopping_cityguide_detail"];
                    break;
                case 4:
                    item.categoryName = @"CULTURE";
                    item.code = @"culture";
                    item.categoryImg =  [UIImage imageNamed:@"culture_cityguide"];
                    item.categoryImgDetail =  [UIImage imageNamed:@"culture_cityguide_detail"];
                    break;
                    
                    
                default:
                    break;
            }
            [subCategories addObject:item];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if(onDone)
                onDone(object,subCategories);
        });
    });
}

-(void) createSelectionCityList
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSMutableArray *cityLst = [[NSMutableArray alloc] init];
        
        CityItem*city = [[CityItem alloc] init];
        city.name = @"Atlanta";
        city.cityCode = @"Atlanta";
        city.stateCode = @"Georgia";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.countryCode = @"USA";
        city.image = [UIImage imageNamed:@"atlanta"];
        city.diningID = @"6514";
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Boston";
        city.cityCode = @"Boston";
        city.stateCode = @"Massachusetts";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.countryCode = @"USA";
        city.image = [UIImage imageNamed:@"boston"];
        city.diningID = @"6422";
        NSArray *subCategoryId = @[/*@"6478",*/@"6357",@"6358",@"6359",@"6360",@"6362"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Cancun";
        city.cityCode = @"Cancun";
        city.countryCode = @"Mexico";
        city.image = [UIImage imageNamed:@"cancun"];
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.diningID = @"6586";
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Chicago";
        city.cityCode = @"Chicago";
        city.stateCode = @"Illinois";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.countryCode = @"USA";
        city.image = [UIImage imageNamed:@"chicago"];
        city.diningID = @"6317";
        subCategoryId = @[/*@"6478",*/@"6339",@"6340",@"6341",@"6342",@"6344"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        
        [cityLst addObject:city];
        
        
        city = [[CityItem alloc] init];
        city.name = @"Cleveland";
        city.cityCode = @"Cleveland";
        city.stateCode = @"Ohio";
        city.countryCode = @"USA";
        city.image = [UIImage imageNamed:@"cleveland"];
        city.diningID = @"6318";
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Dallas";
        city.cityCode = @"Dallas";
        city.stateCode = @"Texas";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.countryCode = @"USA";
        city.image = [UIImage imageNamed:@"dallas"];
        city.diningID = @"6423";
        subCategoryId = @[/*@"6478",*/@"6452",@"6453",@"6454",@"6455",@"6457"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Hawaii";
        city.cityCode = @"Honolulu";
        city.countryCode = @"USA";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.image = [UIImage imageNamed:@"hawai"];
        city.diningID = @"6473";
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Las Vegas";
        city.cityCode = @"Las Vegas";
        city.stateCode = @"Nevada";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.countryCode = @"USA";
        city.image = [UIImage imageNamed:@"lasvegas"];
        city.diningID = @"6319";
        subCategoryId = @[/*@"6478",*/@"6351",@"6352",@"6353",@"6354",@"6356"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        
        city = [[CityItem alloc] init];
        city.name = @"London";
        city.cityCode = @"London";
        city.countryCode = @"United Kingdom";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.image = [UIImage imageNamed:@"london"];
        city.diningID = @"6443";
        subCategoryId = @[/*@"6479",*/@"6375",@"6376",@"6377",@"6378",@"6380"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Los Angeles";
        city.cityCode = @"Los Angeles";
        city.stateCode = @"California";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.countryCode = @"USA";
        city.image = [UIImage imageNamed:@"losangeles"];
        city.diningID = @"6320";
        subCategoryId = @[/*@"6478",*/@"6363",@"6364",@"6365",@"6366",@"6368"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Miami";
        city.cityCode = @"Miami";
        city.stateCode = @"Florida";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.countryCode = @"USA";
        city.image = [UIImage imageNamed:@"miami"];
        city.diningID = @"6321";
        subCategoryId = @[/*@"6478",*/@"6333",@"6334",@"6335",@"6336",@"6338"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        /*
         city = [[CityItem alloc] init];
         city.name = @"Middle East";
         city.regionCode = @"Middle East";
         city.image = [UIImage imageNamed:@"middle_east"];
         city.subCategories = [self createSubCategoriesWithIDs:@[@"6482"]];
         city.diningID = @"-1";
         [cityLst addObject:city];
         */
        
        
        city = [[CityItem alloc] init];
        city.name = @"Milwaukee";
        city.cityCode = @"Milwaukee";
        city.stateCode = @"Wisconsin";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.countryCode = @"USA";
        city.image = [UIImage imageNamed:@"milwaukee"];
        city.diningID = @"6322";
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Montreal";
        city.cityCode = @"Montreal";
        city.stateCode = @"Quebec";
        city.countryCode = @"Canada";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.image = [UIImage imageNamed:@"montreal"];
        city.diningID = @"6424";
        subCategoryId = @[/*@"6478",*/@"6436",@"6438",@"6439",@"6440",@"6437"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Napa/Sonoma";
        city.stateCode = @"California";
        city.countryCode = @"USA";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.image = [UIImage imageNamed:@"napa_sonoma"];
        city.diningID = @"6483";
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"New Orleans";
        city.cityCode = @"New Orleans";
        city.stateCode = @"Louisiana";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.countryCode = @"USA";
        city.image = [UIImage imageNamed:@"new_orlean"];
        city.diningID = @"6515";
        [cityLst addObject:city];
        
        
        city = [[CityItem alloc] init];
        city.name = @"New York";
        city.cityCode = @"New York";
        city.stateCode = @"New York";
        city.countryCode = @"USA";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.image = [UIImage imageNamed:@"new_york"];
        city.diningID = @"6323";
        subCategoryId = @[/*@"6478",*/@"6326",@"6327",@"6328",@"6329",@"6331"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Orlando";
        city.cityCode = @"Orlando";
        city.stateCode = @"Florida";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.countryCode = @"USA";
        city.image = [UIImage imageNamed:@"orlando"];
        city.diningID = @"6477";
        subCategoryId = @[/*@"6478",*/@"6395",@"6398",@"6405",@"6408",@"6418"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Paris";
        city.cityCode = @"Paris";
        city.countryCode = @"France";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.image = [UIImage imageNamed:@"paris"];
        city.diningID = @"6476";
        subCategoryId = @[/*@"6479",*/@"6381",@"6382",@"6383",@"6384",@"6386"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Philadelphia";
        city.cityCode = @"Philadelphia";
        city.stateCode = @"Pennsylvania";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.countryCode = @"USA";
        city.image = [UIImage imageNamed:@"philadelphia"];
        city.diningID = @"6425";
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Rome";
        city.cityCode = @"Rome";
        city.countryCode = @"Italy";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.image = [UIImage imageNamed:@"rome"];
        city.diningID = @"6475";
        subCategoryId = @[/*@"6479",*/@"6460",@"6461",@"6462",@"6471",@"6472"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        /*
        city = [[CityItem alloc] init];
        city.name = @"San Diego";
        city.cityCode = @"San Diego";
        city.stateCode = @"California";
        city.countryCode = @"USA";
        city.diningID = @"-1";
        city.image = [UIImage imageNamed:@"sandiego"];
        [self createSubCategoriesWithIDs:@[@"6478"] forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
         */
        
        
        city = [[CityItem alloc] init];
        city.name = @"San Francisco";
        city.cityCode = @"San Francisco";
        city.stateCode = @"California";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.countryCode = @"USA";
        city.image = [UIImage imageNamed:@"sanfrancisco"];
        city.diningID = @"6324";
        subCategoryId = @[/*@"6478",*/@"6369",@"6370",@"6371",@"6372",@"6374"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Seattle";
        city.cityCode = @"Seattle";
        city.stateCode = @"Washington";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.countryCode = @"USA";
        city.image = [UIImage imageNamed:@"seattle"];
        city.diningID = @"6426";
        subCategoryId = @[/*@"6478",*/@"6446",@"6447",@"6448",@"6449",@"6451"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Toronto";
        city.cityCode = @"Toronto";
        city.stateCode = @"Ontario";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.countryCode = @"Canada";
        city.image = [UIImage imageNamed:@"toronto"];
        city.diningID = @"6427";
        subCategoryId = @[/*@"6478",*/@"6430",@"6432",@"6433",@"6434",@"6431"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Vancouver";
        city.cityCode = @"Montreal";
        city.stateCode = @"British Columbia";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.countryCode = @"Canada";
        city.image = [UIImage imageNamed:@"vancouver"];
        city.diningID = @"6442";
        subCategoryId = @[/*@"6478",*/@"6464",@"6468",@"6470",@"6465",@"6469"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Washington, D.C.";
        city.cityCode = @"Washington DC";
        city.countryCode = @"USA";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.image = [UIImage imageNamed:@"washington"];
        city.diningID = @"6325";
        subCategoryId = @[/*@"6478",*/@"6345",@"6346",@"6347",@"6348",@"6350"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        NSArray *sortedArray = [cityLst sortedArrayUsingComparator:^NSComparisonResult(CityItem *obj1, CityItem *obj2){ return [obj1.name compare:obj2.name];}];
        
        [cityLst removeAllObjects];
        [cityLst addObjectsFromArray:sortedArray];
        
        city = [[CityItem alloc] init];
        city.name = @"Africa/Middle East";
        city.regionCode = @"Africa";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.image = [UIImage imageNamed:@"africa"];
//        city.diningID = @"-1";
//        [self createSubCategoriesWithIDs:@[@"6482"] forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Asia Pacific";
        city.regionCode = @"Asia Pacific";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.image = [UIImage imageNamed:@"asia_pacific"];
        city.diningID = @"6523";
//        [self createSubCategoriesWithIDs:@[@"6480"] forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Canada";
        city.regionCode = @"Canada";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.image = [UIImage imageNamed:@"canada"];
//        city.diningID = @"-1";
        //    city.subCategories = [self createSubCategoriesWithIDs:@[@"6480"]];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Caribbean";
        city.regionCode = @"Caribbean";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.image = [UIImage imageNamed:@"caribbean"];
//        city.diningID = @"-1";
//        [self createSubCategoriesWithIDs:@[@"6542"] forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Europe";
        city.regionCode = @"Europe";
        city.image = [UIImage imageNamed:@"europe"];
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
//        city.diningID = @"-1";
//        [self createSubCategoriesWithIDs:@[@"6479"] forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Latin America";
        city.regionCode = @"Latin America";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.name];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.name];
        city.image = [UIImage imageNamed:@"latin_america"];
//        city.diningID = @"-1";
//        [self createSubCategoriesWithIDs:@[@"6481"] forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
        [cityLst addObject:city];
        
        //    CityItem *allCity = [[CityItem alloc] init];
        //    allCity.name = @"All";
        //    allCity.regionCode = @"all";
        //    allCity.image = [UIImage imageNamed:@"all_cityselection"];
        
        //    NSMutableArray *allItems = [[NSMutableArray alloc] init];
        //    [allItems addObject:allCity];
        //    [allItems addObjectsFromArray:sortedArray];
        dispatch_async(dispatch_get_main_queue(), ^{
            cityItemList = cityLst;
        });
    });
}
+(NSDictionary *)getMenuItems
{
    return @{@"0":@[NSLocalizedString(@"home_menu_title", nil),
                    NSLocalizedString(@"ask_concierge_menu_title", nil),
                    NSLocalizedString(@"explore_menu_title", nil)],
             @"1":@[NSLocalizedString(@"my_profile_menu_title", nil),
//                    NSLocalizedString(@"preferences", nil),
//                    @"CHANGE PASSWORD",
//                    NSLocalizedString(@"my_requests_menu_title", nil)
                    NSLocalizedString(@"preferences_menu_title", nil),
                    NSLocalizedString(@"password_menu_title", nil)
                    ],
             @"2":@[NSLocalizedString(@"about_this_app_menu_title", nil),
                    NSLocalizedString(@"privacy_policy_menu_title", nil),
                    NSLocalizedString(@"terms_of_use_menu_title", nil),
//                    NSLocalizedString(@"GENERATE FAKE BIN", nil),
                    NSLocalizedString(@"sign_out", nil)],
             };
    
}

+(void)storedTimeAcceptedPolicy
{
    [[NSUserDefaults standardUserDefaults] setDouble:[[NSDate date] timeIntervalSince1970] forKey:@"TimeAcceptedPolicy"];
    [[NSUserDefaults standardUserDefaults]  synchronize];
}

+(void)setCreatedProfileSuccessful
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ProfileCreatedSuccessful"];
    [[NSUserDefaults standardUserDefaults]  synchronize];
}


+ (BOOL) isCreatedProfile
{
    return ([[NSUserDefaults standardUserDefaults] dictionaryForKey:keyProfile] != nil);
}

+(NSTimeInterval)getTimeAcceptedPolicy
{
    return [[NSUserDefaults standardUserDefaults] doubleForKey:@"TimeAcceptedPolicy"];
}

+(BOOL)isAcceptedPolicyOverSixMonths
{
    return (([[NSDate date] timeIntervalSince1970] - [AppData getTimeAcceptedPolicy]) > SIX_MONTH_SECONDS_TIME);
}


+(void)setCurrentCityWithCode:(NSString *)cityName
{
    [[NSUserDefaults standardUserDefaults] setValue:cityName forKey:CURRENT_CITY_CODE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(BOOL)isCurrentCity
{
    if([[NSUserDefaults standardUserDefaults] valueForKey:CURRENT_CITY_CODE])
    {
        return YES;
    }
    return NO;
}

+(CityItem *)getSelectedCity
{
    AppDelegate* dl = (AppDelegate*)[UIApplication sharedApplication].delegate;
    //    NSString *currentCityCode = [[NSUserDefaults standardUserDefaults] valueForKey:CURRENT_CITY_CODE];
    NSString *currentCityCode;
    if ([User isValid] && ![dl isUpdatingProfile]) {
        UserObject *userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
        currentCityCode = userObject.selectedCity;
    } else {
        currentCityCode = [[NSUserDefaults standardUserDefaults] valueForKey:CURRENT_CITY_CODE];
    }
    if(currentCityCode)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@",currentCityCode];
        NSArray *cities = sharedMyManager-> cityItemList;
        NSArray *tempArray = [cities filteredArrayUsingPredicate:predicate];
        if(tempArray.count > 0)
        {
            return [tempArray objectAtIndex:0];
        }
    }
    return nil;
}

+ (BOOL) isApplyFeatureWithKey:(NSString *)key {
    BOOL result = NO;
    if(!sharedMyManager)
        return result;
    if([[sharedMyManager->listFeatures allKeys] containsObject:key]) {
        result = [[sharedMyManager->listFeatures objectForKey:key] boolValue];
    }
    return result;
}

+ (void)setUserCityLocationDevice:(NSString *)city {
    if(city.length > 0)
        [[NSUserDefaults standardUserDefaults] setValue:city forKey:USER_CITY_LOCATION_DEVICE];
    else
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_CITY_LOCATION_DEVICE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)getUserCityLocationDevice {
    return [[NSUserDefaults standardUserDefaults] valueForKey:USER_CITY_LOCATION_DEVICE];
}

+ (BOOL) isApplyNewFeatureWithKey:(NSString*)keyFeature {
//    !!!: Apply New Feature
    return [AppData isApplyFeatureWithKey:keyFeature];
}

+(NSDictionary *)getPreferencesItems {
    return @{
             @"DINING":@[@"None", @"African", @"Amalfitano", @"American", @"Arabic", @"Argentinian", @"Asian", @"Australian", @"Austrian", @"Barbeque",@"Brazilian",@"British",@"Burmese", @"Cajun/Creole",@"Californian" ,@"Canadian" ,@"Cantonese" ,@"Caribbean" ,@"Chicken" ,@"Chinese",@"Classic Italian/Traditional",@"Cuban",@"Cucina Povera",@"Dutch",@"European",@"French",@"German",@"Greek",@"Hawaii Regional",@"Indian",@"Indonesian",@"International/Global",@"Italian",@"Jamaican",@"Japanese",@"Korean",@"Latin",@"Lebanese",@"Malaysian",@"Mandarin",@"Mediterranean",@"Mexican",@"Middle Eastern",@"Milanese",@"Modern/Contempo American",@"Modern/Contempo French",@"Modern/Contempo German",@"Modern/Contempo Italian",@"Modern/Contempo Mexican",@"Moroccan",@"Pacific Rim",@"Persian",@"Peruvian",@"Pizza",@"Polish",@"Roman",@"Rortuguese",@"Russian",@"Scandinavian",@"Seafood",@"Singaporean",@"South American",@"Southern American",@"Spanish",@"Sri Lankan",@"Steak/Steakhouse",@"Sushi",@"Swedish",@"Swiss",@"Szechuan",@"Taiwanese",@"Thai",@"Turkish",@"Tuscan",@"Umbrian",@"Venetian",@"Vietnamese"],
             @"HOTEL":@[@"None", @"1 Star", @"2 Stars", @"3 Stars", @"4 Stars", @"5 Stars"],
             @"TRANSPORTATION":@[@"None", @"Luxury Sedan", @"Standard (Sedan)", @"Van"],
             };
    
}

+ (BOOL)shouldGetDistanceForListDiningFromCity:(CityItem *)city {
    BOOL have = NO;
    NSString* deviceCity = [[AppData getUserCityLocationDevice] lowercaseString];
    NSString* selectCity = [city.name lowercaseString];
    have = [deviceCity containsString:selectCity];
    if([LIST_REGIONS containsObject:city.regionCode]) {
        have = YES;
    }
    return have;
}



@end
