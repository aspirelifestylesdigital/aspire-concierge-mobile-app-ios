//
//  UITextField+Extensions.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/4/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "UITextField+Extensions.h"
#import "Common.h"
#import "Constant.h"
#import "UtilStyle.h"
#import "NSString+Utis.h"
#import <objc/runtime.h>

static char* const BORDER = "border_for_textfield";
static char* const ORIGIN_TEXT = "origin_text_textfield";
static char* const IS_TEXT_MASKED = "IS_TEXT_MASKED";

@implementation UITextField (Extensions)

-( void)setBottomBorderRedColor
{
    CALayer *borderLayer = [CALayer layer];
    CGFloat borderWidth = 1.0f;
    borderLayer.borderColor = [UIColor redColor].CGColor;
//    borderLayer.borderColor = colorFromHexString(@"#ff4000").CGColor;
    borderLayer.frame = CGRectMake(0.0f, self.frame.size.height - borderWidth, self.frame.size.width, 1.0f);
    borderLayer.borderWidth = borderWidth;
    [self.layer addSublayer:borderLayer];
    self.layer.masksToBounds = YES;
}


-(void)setBottomBolderDefaultColor
{
    CALayer *borderLayer = [CALayer layer];
    CGFloat borderWidth = 1.0f;
    borderLayer.borderColor = colorFromHexString(@"#99A1A8").CGColor; //5B5B5A
    borderLayer.frame = CGRectMake(0.0f, self.frame.size.height - borderWidth, self.frame.size.width, 1.0f);
    borderLayer.borderWidth = borderWidth;
    [self.layer addSublayer:borderLayer];
    self.layer.masksToBounds = YES;
}

-(void)updateCursorPositionAtRange:(NSRange)range {
    UITextPosition *start = [self positionFromPosition:[self beginningOfDocument]
                                                 offset:range.location];
    UITextPosition *end = [self positionFromPosition:start
                                               offset:range.length];
    [self setSelectedTextRange:[self textRangeFromPosition:start toPosition:end]];
}

-(void)setTextStyleForText:(NSString *)text WithFontName:(NSString *)fontName WithColor:(UIColor *)color WithTextSize:(float)textSize WithTextCenter:(BOOL)isCenter WithSpacing:(float)spacing ForPlaceHolder:(BOOL)isPlaceHolder;
{
    NSRange range = NSMakeRange(0, text.length);
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:text];
    [attributeString addAttribute:NSKernAttributeName value:@(spacing) range:range];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont fontWithName:fontName size:textSize*SCREEN_SCALE] range:range];
    [attributeString addAttribute:NSForegroundColorAttributeName value:color range:range];
    
    if(isCenter)
    {
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init] ;
        [paragraphStyle setAlignment:NSTextAlignmentCenter];
        [attributeString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    }
    
    if(isPlaceHolder){
       self.attributedPlaceholder = attributeString;
    }
    else{
        self.attributedText = attributeString;
    }
}

-(void) setMarginLeftRight:(CGFloat)margin {
    
    if (self.leftView) self.leftView = nil;
    if (self.rightView) self.rightView = nil;
    
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, margin, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    UIView *rView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, margin, 30)];
    [rView setBackgroundColor:[UIColor clearColor]];
    [self setLeftView:leftView];
    [self setLeftViewMode:UITextFieldViewModeAlways];
    [self setRightView:rView];
    [self setRightViewMode:UITextFieldViewModeAlways];
}

- (void)setOriginText:(NSString *)text {
#if DEBUG
    NSLog(@"%s => %@",__PRETTY_FUNCTION__,text);
#endif
    objc_setAssociatedObject(self, ORIGIN_TEXT, text, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)showTextMask:(BOOL)isEmail {
    //    if ([self isTextMasked]) return;
    if (self.text.length > 0) {
        self.text = [self.text createMaskForText:isEmail];
        //        objc_setAssociatedObject(self, IS_TEXT_MASKED, @(true), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
}

- (void)showTextMaskPhone {
    //    if ([self isTextMasked]) return;
    if (self.text.length > 0) {
        self.text = [self.text createMaskForText:false];
        //        objc_setAssociatedObject(self, IS_TEXT_MASKED, @(true), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
}


-(void) showAsterisk:(CGFloat)margin {
    
    if (self.leftView) self.leftView = nil;
    if (self.rightView) self.rightView = nil;
    
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, margin, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [self setLeftView:leftView];
    [self setLeftViewMode:UITextFieldViewModeAlways];
    UIImageView* asteriskImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"asterisk_icon"]];
    asteriskImage.frame = CGRectMake(0.0f, 0.0f, 20.0f, 20.0f);
    asteriskImage.contentMode = UIViewContentModeCenter;
    [self setRightView:asteriskImage];
    [self setRightViewMode:UITextFieldViewModeAlways];
}
@end
