//
//  UIView+Extension.m
//  MobileConcierge
//
//  Created by Home on 5/7/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "UIView+Extension.h"
#import "Common.h"
#import "Constant.h"

@implementation UIView (Extension)
-(void)setBackgroundColorForView
{
    [self setBackgroundColor:colorFromHexString(DEFAULT_BACKGROUND_COLOR)];
}

-(void)setBackgroundRedColorForView
{
    [self setBackgroundColor:[UIColor colorWithRed:210.0f/255.0f green:10.0f/255.0f blue:59.0f/255.0f alpha:1.0f]];
}

-(void)setBackgroundColoreForNavigationBar
{
    [self setBackgroundColor:[UIColor colorWithRed:1.0f/255.0f green:22.0f/255.0f blue:39.0f/255.0f alpha:1.0f]];
}

-(void)setbackgroundColorForLineMenu
{
    [self setBackgroundColor:[UIColor colorWithRed:77/255.0f green:107/255.0f blue:133/255.0f alpha:1.0f]];//colorFromHexString(@"#253847")
}
@end
