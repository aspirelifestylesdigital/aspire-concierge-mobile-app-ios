//
//  Constant.m
//  ALC
//
//  Created by Anh Tran on 8/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "Constant.h"

/***
 /// Define Font
 ***/
NSString *const FONT_MarkForMC_BOLD = @"Arial-BoldMT"; //@"ProximaNova-Bold";
NSString *const FONT_MarkForMC_MED = @"Arial-BoldMT"; //@"ProximaNova-Semibold";
NSString *const FONT_MarkForMC_LIGHT = @"Arial"; //@"ProximaNova-Light";
NSString *const FONT_MarkForMC_REGULAR = @"ArialMT"; //@"ProximaNova-Regular";
NSString *const FONT_MarkForMC_REGULAR_It = @"Arial-ItalicMT"; //@"ProximaNova-RegularIt";
NSString *const FONT_MarkForMC_Nrw_BOLD = @"Arial-BoldMT"; //@"ProximaNovaAExCn-Bold";
NSString *const DEFAULT_BACKGROUND_COLOR = @"#002b51"; //@"#000000";
NSString *const DEFAULT_PLACEHOLDER_COLOR = @"#99A1A8";
NSString *const DEFAULT_HIGHLIGHT_COLOR = @"#0F8CE7"; //@"#e1cb78";

@implementation Constant

@end
/*
Arial-BoldItalicMT
Arial-BoldMT
Arial-ItalicMT


TimesNewRomanPSMT
TimesNewRomanPS-BoldItalicMT
TimesNewRomanPS-ItalicMT
TimesNewRomanPS-BoldMT
TimesNewRomanPSMT
TimesNewRomanPS-BoldItalicMT
TimesNewRomanPS-ItalicMT
TimesNewRomanPS-BoldMT
 */
