//
//  SessionData.h
//  MobileConcierge
//
//  Created by user on 6/21/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserObject.h"
#import "NSDictionary+SBJSONHelper.h"

#define keyUUID                  @"UUID"
#define keyOnlineMemberDetailIDs @"OnlineMemberDetailIDs"
#define keyOnlineMemberID        @"OnlineMemberID"
#define keyBINNumber             @"binNumber"
#define keyFirstName             @"FirstName"
#define keyLastName              @"LastName"
#define keyConsumerKey           @"ConsumerKey"
#define keyEmail                 @"Email"
#define keyPassword              @"Password"
#define keyMobileNumber          @"MobileNumber"
#define keyZipCode               @"ZipCode"
#define keyProgram               @"Program"
#define keySalutation            @"Salutation"
#define keyCountryCode           @"CountryCode"
#define keyRequestToken          @"RequestToken"
#define keyAccessToken           @"AccessToken"
#define keyRefreshToken          @"RefreshToken"
#define keyExprirationTime       @"ExprirationTime"
#define keyOptStatus             @"OptStatus"
#define keyCurrentPolicyVersion  @"POLICYVERSION"
#define keyIsUseLocation         @"ISUSELOCATION"
#define keyHasForgotAccount      @"HASFORGOTPACCOUNT"
#define keyTransactionID         @"TRANSACTIONID"
#define keyProfile               @"PROFILE"
#define keyFullName              @"FullName"
#define keyAppName               @"AppName"
#define keyMember                @"Member"
#define keyMemberDetails         @"MemberDetails"

#define keyCurrentSecret        @"l3JdK3PT09XfoxS3LKPwGA=="
#define keyOldSecret            @"vB3SKWrVJjSOj33KrnZdRg=="
#define keyNewSecret            @"N+hLLmBHaouoNNKVvOMUFg=="

@interface SessionData : NSObject

+(instancetype)shareSessiondata;

//@property (strong, nonatomic) NSString *UUID;
//@property (strong, nonatomic) NSString *OnlineMemberDetailIDs;
//@property (strong, nonatomic) NSString *OnlineMemberID;
//
@property (strong, nonatomic) NSString *userFirstName;
@property (strong, nonatomic) NSString *userLastName;
@property (strong, nonatomic) NSString *userMobileNumber;
@property (strong, nonatomic) NSString *userEmail;
@property (strong, nonatomic) NSString *userConsumerKey;
@property (strong, nonatomic) NSString *userSalutation;
@property (strong, nonatomic) NSString *userZipCode;
@property (strong, nonatomic) NSString *userProgram;

@property (strong, nonatomic) NSMutableArray *arrayPreferences;
//
- (void) setUUID:(NSString*)uuid;
- (NSString*) UUID;

- (void) setOnlineMemberDetailIDs:(NSString*)onlineMemberDetailIDs;
- (NSString*) OnlineMemberDetailIDs;

- (void) setOnlineMemberID:(NSString*)onlineMemberID;
- (NSString*) OnlineMemberID;

- (void) setBINNumber:(NSString*)bin;
- (NSString*) BINNumber;

- (void)setRequestToken:(NSString*)requestToken;
- (NSString*)RequestToken;

- (void) setAccessToken:(NSString*)accessToken;
- (NSString*)AccessToken;

- (void) setRefreshToken:(NSString*)refreshToken;
- (NSString*) RefreshToken;

- (void) setExpirationTime:(NSNumber*)exprirationTime;
- (NSNumber*) ExpirationTime;

- (void) setCurrentPolicyVersion:(NSString*)currentPolicyVersion;
- (NSString*) CurrentPolicyVersion;

- (void) setIsUseLocation:(BOOL)isLocation;
- (BOOL) isUseLocation;

- (void) setHasForgotAccount:(BOOL)isForgot;
- (BOOL) hasForgotAccount;

- (void)setUserObjectWithDict:(NSDictionary*)dict;
- (void)setUserObject:(UserObject*)userObject;
- (UserObject*)UserObject;

- (void)setTransactionID:(NSString*)transactionID;
- (NSString*)TransactionID;

- (NSDictionary*)getUserInfo;

- (void)setIsShownLocationServiceAlert:(BOOL)isLocation;
- (BOOL)isShownLocationServiceAlert;

@end
