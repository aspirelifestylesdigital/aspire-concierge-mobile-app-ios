//
//  UITableView+Extensions.m
//  FoodyMobile
//
//  Created by Duc Trinh on 8/23/12.
//  Copyright (c) 2012 Foody Corp. All rights reserved.
//

#import "UITableView+Extensions.h"
#import "Constant.h"

@interface UITableView(PrivateExt)
 
@end

@implementation UITableView (Extensions)

- (void)setDummyFooterView
{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 1)];
    footerView.backgroundColor= [UIColor clearColor];
    self.tableFooterView = footerView;
}

- (void)addDummyTableHeaderWithHeight:(float)height
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, height)];
    self.tableHeaderView = headerView;
}

- (void)setDummyFooterViewWithText:(NSString *)text
{
    //CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:13]];
    CGSize size = [text sizeWithAttributes:
                       @{NSFontAttributeName:
                             [UIFont systemFontOfSize:13]}];
    
    UILabel *footerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, size.height + 30)] ;
    footerLabel.backgroundColor = [UIColor clearColor];
    footerLabel.textColor = [UIColor darkGrayColor];
    footerLabel.font = [UIFont systemFontOfSize:13];
    footerLabel.textAlignment = NSTextAlignmentCenter;
    footerLabel.text = text;
    footerLabel.numberOfLines = 0;
    
    self.tableFooterView = footerLabel;
}

// New dummy footer
- (void)setDummyFooterViewWithText:(NSString *)text height:(CGFloat)footerHeight button:(UIButton *)button {
    
    CGFloat imageWidth = 56.0;
    CGFloat imageHeight = 56.0;
    CGFloat labelWidth = SCREEN_WIDTH*2/3;
    CGSize textSize = [text sizeWithFont:[UIFont systemFontOfSize:13.0f] constrainedToSize:CGSizeMake(labelWidth, 1000) lineBreakMode: NSLineBreakByWordWrapping];
    
    
    CGFloat totalContentHeight = imageHeight + textSize.height + 10;
    if (button) {
        totalContentHeight += (button.bounds.size.height) + 10;
    }
    
    
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, footerHeight)];
    [footerView setBackgroundColor:[UIColor clearColor]];
    
    
    UIImageView *imv = [[UIImageView alloc] initWithFrame:CGRectMake( (SCREEN_WIDTH - imageWidth)/2.0 , (footerHeight - totalContentHeight) /2, imageWidth, imageHeight)];
    [imv setImage:[UIImage imageNamed:@"no_data_icon"]];
    [imv setBackgroundColor:[UIColor clearColor]];
    [footerView addSubview:imv];
    
    UILabel *footerLabel = [[UILabel alloc] initWithFrame:CGRectMake( (SCREEN_WIDTH - labelWidth)/2 , CGRectGetMaxY(imv.frame) + 10, labelWidth, textSize.height)] ;
    footerLabel.backgroundColor = [UIColor clearColor];
    footerLabel.textColor = [UIColor lightGrayColor];
    footerLabel.font = [UIFont systemFontOfSize:13];
    footerLabel.textAlignment = NSTextAlignmentCenter;
    footerLabel.text = text;
    footerLabel.numberOfLines = 0;
    footerLabel.tag = 222; // use for outside
    [footerView addSubview:footerLabel];
    
    if (button) {
        [footerView addSubview:button];
        
        CGRect btRect = button.frame;
        btRect.origin.x = (SCREEN_WIDTH - btRect.size.width)/2;
        btRect.origin.y = CGRectGetMaxY(footerLabel.frame) + 10;
        button.frame = btRect;
    }
    self.tableFooterView = footerView;
}

- (void)setDummyFooterViewWithImageAndText:(NSString *)text height:(CGFloat)footerHeight isShowReload:(BOOL)isReload
{
    CGFloat imageWidth = 56.0;
    CGFloat imageHeight = 56.0;
    //CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:13]];
    //CGSize sizeBtn = [NSLocalizedString(@"RELOAD_DATA_BUTTON_TITLE", @"") sizeWithFont:[UIFont systemFontOfSize:13]];
    CGSize size = [text sizeWithAttributes:
                   @{NSFontAttributeName:
                         [UIFont systemFontOfSize:13]}];
    CGSize sizeBtn = [NSLocalizedString(@"RELOAD_DATA_BUTTON_TITLE", @"") sizeWithAttributes:
                   @{NSFontAttributeName:
                         [UIFont systemFontOfSize:13]}];
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, footerHeight)];
    [footerView setBackgroundColor:[UIColor clearColor]];
    
    
    // add image
    UIImageView *imv = [[UIImageView alloc] initWithFrame:CGRectMake( (SCREEN_WIDTH - imageWidth)/2.0 , (footerHeight - imageHeight - size.height -sizeBtn.height - 50)/2.0, imageWidth, imageHeight)];
    if (!isReload) {
        [imv setFrame:CGRectMake( (SCREEN_WIDTH - imageWidth)/2.0 , (footerHeight - imageHeight - size.height - 40)/2.0, imageWidth, imageHeight)];
    }
    [imv setImage:[UIImage imageNamed:@"no_data_icon"]];
    [imv setBackgroundColor:[UIColor clearColor]];
    [footerView addSubview:imv];
    
    // add text
    UILabel *footerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,  imv.frame.origin.y + 10.0 + imv.frame.size.height, SCREEN_WIDTH, size.height + 30)] ;
    footerLabel.backgroundColor = [UIColor clearColor];
    footerLabel.textColor = [UIColor lightGrayColor];
    footerLabel.font = [UIFont systemFontOfSize:13];
    footerLabel.textAlignment = NSTextAlignmentCenter;
    footerLabel.text = text;
    footerLabel.numberOfLines = 0;
    footerLabel.tag = 222; // use for outside
    [footerView addSubview:footerLabel];
    
    // add reload button
    if (isReload) {
        UIButton *reloadBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [reloadBtn setBackgroundColor:[UIColor clearColor]];
        [reloadBtn setFrame:CGRectMake( (SCREEN_WIDTH - sizeBtn.width - 20.0)/2.0, footerLabel.frame.origin.y + 10.0 + footerLabel.frame.size.height, sizeBtn.width + 20.0, sizeBtn.height)];
        [reloadBtn setTitle:NSLocalizedString(@"RELOAD_DATA_BUTTON_TITLE", @"") forState:UIControlStateNormal];
        [reloadBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [reloadBtn.titleLabel setFont:[UIFont systemFontOfSize:13]];
        [reloadBtn addTarget:self action:@selector(didTouchReloadData:) forControlEvents:UIControlEventTouchUpInside];
        [footerView addSubview:reloadBtn];
    }
    
    self.tableFooterView = footerView;
}

- (void)didTouchReloadData:(id)sender
{
//    [[NSNotificationCenter defaultCenter] postNotificationName:RELOAD_DATA_NOTI object:nil];
//    [Utility postNotificationInMainThreadWithName:RELOAD_DATA_NOTI object:nil userInfo:nil];

}

- (void)removeDummyFooterView
{
    self.tableFooterView = nil;
}
#pragma mark -
#pragma mark load more
- (void)addLoadMoreFooterView
{
    CGRect footer = [self rectForFooterInSection:self.numberOfSections - 1];
    UIView *tableFooter = self.tableFooterView;
    if (tableFooter) {
        footer = tableFooter.frame;
    }
    
    UIImage *downArrowImg = [UIImage imageNamed:@""];
    UIImageView *downArrow = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH/2.0 - downArrowImg.size.width - 40, footer.origin.y + footer.size.height + TABLE_PULL_HEIGHT - 15 + 3, downArrowImg.size.width, downArrowImg.size.height)];
    downArrow.image = downArrowImg;
    downArrow.tag = 20000;
    [self addSubview:downArrow];
    
    UILabel *loadMoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(downArrow.frame.origin.x + downArrow.frame.size.width + 3, footer.origin.y + footer.size.height + TABLE_PULL_HEIGHT - 15, 130, 15)] ;
    loadMoreLabel.backgroundColor = [UIColor clearColor];
    loadMoreLabel.font = [UIFont systemFontOfSize:13.0];
    loadMoreLabel.textAlignment = NSTextAlignmentLeft;
    loadMoreLabel.textColor = [UIColor colorWithRed:227.0/255.0 green:31.0/255.0 blue:31.0/255.0 alpha:1.0];
    loadMoreLabel.tag = 20001;
    [self addSubview:loadMoreLabel];
    
    UIActivityIndicatorView *loadMoreSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] ;
    loadMoreSpinner.frame = CGRectMake((self.frame.size.width - 20)/2, footer.origin.y + footer.size.height + 5, 20, 20);
    loadMoreSpinner.hidesWhenStopped = YES;
    loadMoreSpinner.tag = 20002;
    [self addSubview:loadMoreSpinner];
}

- (void)setLoadMoreText:(NSString *)text isDown:(BOOL)isArrowDown padding:(float)paddingNum
{
    UIImageView *downArrow = (UIImageView *)[self viewWithTag:20000];
    UILabel *loadMoreLabel = (UILabel *)[self viewWithTag:20001];
    UIActivityIndicatorView *spinner = (UIActivityIndicatorView *)[self viewWithTag:20002];
    if (!downArrow) {
        [self addLoadMoreFooterView];
        downArrow = (UIImageView *)[self viewWithTag:20000];
        loadMoreLabel = (UILabel *)[self viewWithTag:20001];
        spinner = (UIActivityIndicatorView *)[self viewWithTag:20002];
    }
    else
    {

    }
    
    loadMoreLabel.hidden = NO;
    downArrow.hidden = NO;
    
    
    CGRect footer = [self rectForFooterInSection:self.numberOfSections - 1];
    UIView *tableFooter = self.tableFooterView;
    if (tableFooter) {
        footer = tableFooter.frame;
    }
    
    CGRect downArrowFrame = downArrow.frame;
    downArrow.frame = CGRectMake(SCREEN_WIDTH/2.0 - downArrowFrame.size.width - 40, footer.origin.y + footer.size.height + TABLE_PULL_HEIGHT - 15 + paddingNum + 3, downArrowFrame.size.width, downArrowFrame.size.height);
    loadMoreLabel.frame = CGRectMake(downArrow.frame.origin.x + downArrow.frame.size.width + 3, footer.origin.y + footer.size.height + TABLE_PULL_HEIGHT - 15 + paddingNum, 130, 15);
    spinner.frame = CGRectMake((self.frame.size.width - 20)/2, footer.origin.y + footer.size.height, 20, 20);
    if (isArrowDown) {
        downArrow.transform = CGAffineTransformIdentity;
    }
    else
        downArrow.transform = CGAffineTransformMakeRotation(M_PI);
    
    loadMoreLabel.text = text;
}

- (void)hideLoadMoreText
{
    UIImageView *downArrow = (UIImageView *)[self viewWithTag:20000];
    UILabel *loadMoreLabel = (UILabel *)[self viewWithTag:20001];
 
    if (!downArrow) {
        [self addLoadMoreFooterView];
        downArrow = (UIImageView *)[self viewWithTag:20000];
        loadMoreLabel = (UILabel *)[self viewWithTag:20001];
        
    }
    else{
 
    }
 
    downArrow.hidden = YES;
    loadMoreLabel.hidden = YES;
}

- (void)startLoadMoreIndicator
{
    UIImageView *downArrow = (UIImageView *)[self viewWithTag:20000];
    UILabel *loadMoreLabel = (UILabel *)[self viewWithTag:20001];
    UIActivityIndicatorView *loadMoreSpinner = (UIActivityIndicatorView *)[self viewWithTag:20002];
    if (!downArrow) {
        [self addLoadMoreFooterView];
        downArrow = (UIImageView *)[self viewWithTag:20000];
        loadMoreLabel = (UILabel *)[self viewWithTag:20001];
        loadMoreSpinner = (UIActivityIndicatorView *)[self viewWithTag:20002];
    }
    else{
        CGRect footer = [self rectForFooterInSection:self.numberOfSections - 1];
        UIView *tableFooter = self.tableFooterView;
        if (tableFooter) {
            footer = tableFooter.frame;
        }
        loadMoreSpinner.frame = CGRectMake((self.frame.size.width - 20)/2, footer.origin.y + footer.size.height + 5, 20, 20);
    }
    
    [loadMoreSpinner startAnimating];
    downArrow.hidden = YES;
    loadMoreLabel.hidden = YES;
}

- (void)startLoadMoreIndicatorWithYPos:(double)YPos
{
    UIImageView *downArrow = (UIImageView *)[self viewWithTag:20000];
    UILabel *loadMoreLabel = (UILabel *)[self viewWithTag:20001];
    UIActivityIndicatorView *loadMoreSpinner = (UIActivityIndicatorView *)[self viewWithTag:20002];
    if (!downArrow) {
        [self addLoadMoreFooterView];
        downArrow = (UIImageView *)[self viewWithTag:20000];
        loadMoreLabel = (UILabel *)[self viewWithTag:20001];
        loadMoreSpinner = (UIActivityIndicatorView *)[self viewWithTag:20002];
    }
    loadMoreSpinner.frame = CGRectMake((self.frame.size.width - 20)/2, YPos, 20, 20);
    [loadMoreSpinner startAnimating];
    downArrow.hidden = YES;
    loadMoreLabel.hidden = YES;
}

- (void)removeLoadMoreIndicator
{
    UIImageView *downArrow = (UIImageView *)[self viewWithTag:20000];
    UILabel *loadMoreLabel = (UILabel *)[self viewWithTag:20001];
    UIActivityIndicatorView *loadMoreSpinner = (UIActivityIndicatorView *)[self viewWithTag:20002];
    
    [downArrow removeFromSuperview];
    [loadMoreLabel removeFromSuperview];
    [loadMoreSpinner removeFromSuperview];
}

- (void)stopLoadMoreIndicator
{
    UIImageView *downArrow = (UIImageView *)[self viewWithTag:20000];
    UILabel *loadMoreLabel = (UILabel *)[self viewWithTag:20001];
    UIActivityIndicatorView *loadMoreSpinner = (UIActivityIndicatorView *)[self viewWithTag:20002];
    if (downArrow) {
        [loadMoreSpinner stopAnimating];
        downArrow.hidden = NO;
        loadMoreLabel.hidden = NO;
    }
}
 
#pragma mark -
#pragma mark refresh
- (void)addRefreshHeaderView
{
    [self addRefreshHeaderView:NO];
}

- (void)addRefreshHeaderView:(BOOL)isWhite
{
    UIImage *downArrowImg = nil;
    if (isWhite) {
        downArrowImg = [UIImage imageNamed:@"arrow_down_white.png"];
    }
    else
    {
        downArrowImg = [UIImage imageNamed:@"arrow_down.png"];
    }
    
    UIImageView *downArrow = [[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.size.width/2 - downArrowImg.size.width - 40, -TABLE_PULL_HEIGHT +10, downArrowImg.size.width, downArrowImg.size.height)] ;
    downArrow.image = downArrowImg;
    downArrow.tag = 10000;
    [self addSubview:downArrow];
    
    UILabel *refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(downArrow.frame.origin.x + downArrow.frame.size.width + 3, -TABLE_PULL_HEIGHT, 130, 30)] ;
    refreshLabel.backgroundColor = [UIColor clearColor];
    refreshLabel.font = [UIFont systemFontOfSize:13.0];
    refreshLabel.textAlignment = NSTextAlignmentLeft;
    if (isWhite) {
        refreshLabel.textColor = [UIColor whiteColor];
    }
    else{
         refreshLabel.textColor = [UIColor colorWithRed:227.0/255.0 green:31.0/255.0 blue:31.0/255.0 alpha:1.0];
    }
   
    refreshLabel.tag = 10001;
    [self addSubview:refreshLabel];
    
    UIActivityIndicatorView *refreshSpinner = nil;
    if (isWhite) {
        refreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite] ;
    }
    else
    {
        refreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] ;
    }
    refreshSpinner.frame = CGRectMake((self.bounds.size.width - 20)/2, -25, 20, 20);
    refreshSpinner.hidesWhenStopped = YES;
    refreshSpinner.tag = 10002;
    [self addSubview:refreshSpinner];
}

- (void)setRefreshText:(NSString *)text isDown:(BOOL)isDown
{
    [self setRefreshText:text isDown:isDown isWhite:NO];
}

- (void)setRefreshText:(NSString *)text isDown:(BOOL)isDown isWhite:(BOOL)isWhiteColor
{
    UIImageView *downArrow = (UIImageView *)[self viewWithTag:10000];
    UILabel *refreshLabel = (UILabel *)[self viewWithTag:10001];
    
    if (!refreshLabel) {
        [self addRefreshHeaderView:isWhiteColor];
        downArrow = (UIImageView *)[self viewWithTag:10000];
        refreshLabel = (UILabel *)[self viewWithTag:10001];
    }
    if (!isDown) {
        downArrow.transform = CGAffineTransformMakeRotation(M_PI);
    }
    else
    {
        downArrow.transform = CGAffineTransformIdentity;
    }
    refreshLabel.text = text;
}

- (void)startRefreshIndicator
{
    [self startRefreshIndicator:NO];
}

- (void)startRefreshIndicator:(BOOL)isWhite
{
    UIImageView *downArrow = (UIImageView *)[self viewWithTag:10000];
    UILabel *refreshLabel = (UILabel *)[self viewWithTag:10001];
    UIActivityIndicatorView *refreshSpinner = (UIActivityIndicatorView *)[self viewWithTag:10002];
    
    if (!downArrow) {
        [self addRefreshHeaderView:isWhite];
        downArrow = (UIImageView *)[self viewWithTag:10000];
        refreshLabel = (UILabel *)[self viewWithTag:10001];
        refreshSpinner = (UIActivityIndicatorView *)[self viewWithTag:10002];
    }
    [refreshSpinner startAnimating];
    downArrow.hidden = YES;
    refreshLabel.hidden = YES;
}

- (void)stopRefreshIndicator
{
    UIImageView *downArrow = (UIImageView *)[self viewWithTag:10000];
    UILabel *refreshLabel = (UILabel *)[self viewWithTag:10001];
    UIActivityIndicatorView *refreshSpinner = (UIActivityIndicatorView *)[self viewWithTag:10002];
    
    if (downArrow) {
        [refreshSpinner stopAnimating];
        downArrow.hidden = NO;
        refreshLabel.hidden = NO;
    }
}

- (void)removeRefreshIndicator
{
    UIImageView *downArrow = (UIImageView *)[self viewWithTag:10000];
    UILabel *loadMoreLabel = (UILabel *)[self viewWithTag:10001];
    UIActivityIndicatorView *loadMoreSpinner = (UIActivityIndicatorView *)[self viewWithTag:10002];
    
    [downArrow removeFromSuperview];
    [loadMoreLabel removeFromSuperview];
    [loadMoreSpinner removeFromSuperview];
}

@end
