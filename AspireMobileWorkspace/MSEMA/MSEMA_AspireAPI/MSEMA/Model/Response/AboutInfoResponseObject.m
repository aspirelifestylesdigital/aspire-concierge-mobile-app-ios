//
//  AboutInfoResponseObject.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/19/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "AboutInfoResponseObject.h"
#import "NSDictionary+SBJSONHelper.h"
#import "AboutInfoItem.h"

@implementation AboutInfoResponseObject

-(id)initFromDict:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        dict = [dict dictionaryForKey:@"GetClientCopyResult"];
        self.data = [self parseJson:dict toObject:NSStringFromClass([AboutInfoItem class])];
        [self parseCommonResponse:dict];
    }
    return self;
}

-(id)parseJson:(NSDictionary *)dict toObject:(NSString *)className
{
    if([className isEqualToString:NSStringFromClass([AboutInfoItem class])])
    {
        AboutInfoItem *item = [[AboutInfoItem alloc] init];
        item.textInfo = [dict stringForKey:@"Text"];
        return [[NSArray alloc] initWithObjects:item, nil];
    }
    
    return nil;
}

-(void)parseCommonResponse:(NSDictionary *)dict
{
    self.status = [dict boolForKey:@"Success"];
    NSArray* mesArr = [dict arrayForKey:@"message"];
    if(mesArr && mesArr.count > 0){
        NSDictionary * error = [mesArr objectAtIndex:0];
        self.message = [error stringForKey:@"message"];
        self.errorCode = [error stringForKey:@"code"];
    }
    
    self.message = [dict objectForKey:@"message"];
}

@end
