//
//  SearchResponseObject.h
//  MobileConcierge
//
//  Created by Chung Mai on 6/21/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseResponseObject.h"
#import "CityItem.h"

@interface SearchResponseObject : BaseResponseObject

@property(nonatomic, assign) BOOL hasOffer;
@property(nonatomic, assign) NSInteger offerNumber;
//@property(nonatomic, strong) CityItem *currentCity;
@property(nonatomic, strong) NSString *searchText;
@property (nonatomic,strong) NSArray* categories;
@property (nonatomic,assign) NSUInteger totalResultItemDontFilter;
@property(nonatomic, assign) NSInteger searchItemNumber;

@end
