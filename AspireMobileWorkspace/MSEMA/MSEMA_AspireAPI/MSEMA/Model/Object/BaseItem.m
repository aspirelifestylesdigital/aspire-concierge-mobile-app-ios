//
//  ExploreItem.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/9/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "BaseItem.h"
#import "LocationComponent.h"
#import "AppData.h"

@implementation BaseItem
- (id) init {
    if(self = [super init]) {
        self.distance = CGFLOAT_MAX;
    }
    return self;
}

- (void) getDistance {
    
    NSMutableString* temp = [NSMutableString new];
    
    NSString* addres1 = [[self.address1 componentsSeparatedByString:@","] firstObject];
    if([[self.cityName lowercaseString] isEqualToString:@"london"]) {
        addres1 = self.address1;
    }
    if([self.state isEqualToString:@"HI"] || [self.state isEqualToString:@"NV"]){
        addres1 = [[self.address1 componentsSeparatedByString:@","] lastObject];
        if([self.address1 componentsSeparatedByString:@","].count > 1) {
            addres1 = [[self.address1 componentsSeparatedByString:@","] objectAtIndex:1];
        }
    }
    if([addres1 containsString:@"St."])
        addres1 = [addres1 stringByReplacingOccurrencesOfString:@"St." withString:@"St"];
    if([self.state isEqualToString:@"GA"]) {
        addres1 = [addres1 stringByReplacingOccurrencesOfString:@"NW" withString:@"Northwest"];
    }
        if([self.address1 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0) {
            [temp appendFormat:temp.length > 0?@", %@":@"%@",[self.state isEqualToString:@"HI"]?addres1:[addres1 stringByReplacingOccurrencesOfString:@" St" withString:@" Street"]];
        }
    
    if([self.cityName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0) {
        [temp appendFormat:temp.length > 0?@" %@":@"%@",self.cityName];
    }
    
    
    if([self.state stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0) {
        [temp appendFormat:temp.length > 0?@", %@":@"%@",self.state];
    }

    if(temp.length == 0) return;

#ifdef DEBUG
    NSLog(@"%@",temp);
#endif
    self.addressForMap = temp;
    self.isWaitingGetDistance = YES;
    [LocationComponent staticGetDistanceForObject:self fromAddress:[self strimStringFrom:temp withPatterns:@[@"\\(.*\\),",@"^(\\+\\d{1,9}\\s)?\\(?\\d{1,9}\\)?[\\s.-]?\\d{1,9}[\\s.-]?\\d{1,9},"]] onDone:nil];
    temp = nil;
}

- (NSString*) strimStringFrom:(NSString*) from withPatterns:(NSArray*) pattern {
    NSString* temp = from;
    for (NSString* p in pattern) {
        temp = [temp stringByReplacingOccurrencesOfString:p withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0,temp.length)];
    }
    return temp;
}
@end
