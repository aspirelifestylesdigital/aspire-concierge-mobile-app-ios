//
//  CreateNewConciergeCaseResponseItem.h
//  MobileConcierge
//
//  Created by Mac OS on 5/16/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CreateConciergeCaseItem : NSObject
@property (nonatomic, strong) NSString *transactionId;
@end
