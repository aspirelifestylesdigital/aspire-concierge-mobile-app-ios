//
//  ExploreItem.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/9/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface BaseItem : NSObject

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *descriptionContent;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *offer;
@property (nonatomic, assign) BOOL     isOffer;
@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) UIImage *imageDetail;
@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic, strong) NSString *categoryCode;

@property (nonatomic, assign) BOOL shouldUseNameInsteadAddress;
@property (nonatomic, strong) NSString * addressForMap;

@property(nonatomic, strong) NSString * address1;
@property(nonatomic, strong) NSString * address2;
@property(nonatomic, strong) NSString * address3;
@property (nonatomic, strong) NSString *state;
@property(nonatomic, strong) NSString * cityName;
@property(nonatomic, strong) NSString * countryName;
@property (nonatomic, strong) NSString *zipCode;
@property(nonatomic, strong) NSString * userDefined1;
@property(assign,nonatomic) double distance;
@property(assign,nonatomic) double longitude;
@property(assign,nonatomic) double latitude;
@property (nonatomic, assign) BOOL isWaitingGetDistance;

- (void) getDistance;
@end
