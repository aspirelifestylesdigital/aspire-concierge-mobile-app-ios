//
//  Country.m
//  LuxuryCard
//
//  Created by Viet Vo on 9/8/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "Country.h"

@implementation Country
@synthesize countryName, countryCode, countryNumber, countryDescription;


- (id)init:(NSString*)name withCode:(NSString*)code withNumber:(NSString*)number withDesciption:(NSString*)description{
    countryName = name;
    countryCode = code;
    countryNumber = number;
    countryDescription = description;
    return self;
}

@end
