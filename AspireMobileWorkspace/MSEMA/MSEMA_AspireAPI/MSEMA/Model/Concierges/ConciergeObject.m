//
//  ConciergeObject.m
//  LuxuryCard
//
//  Created by Viet Vo on 8/29/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//


/*
 - Dining
 - Recommend a Dining
 - Hotel
 - Recommend a Hotel
 - Others
 - Golf
 - Golf recommend
 - Car Rental
 - Transfer
 - Entertainment
 - Book a Tour
 - Recommend a Tour
 - Flight
 - Cruise
 - FlowerDelivery
 - PrivateJet
 */

#import "ConciergeObject.h"
#import "Constant.h"

@implementation ConciergeObject

@synthesize rowNumber, ePCCaseID, transactionID, prefResponse, phoneNumber, emailAddress1, requestStatus, requestType, city, country, eventDate, pickupDate, startDate, endDate, createdDate, requestMode, requestDetails, contentString, myRequestType, functionality, situation, numberOfAdults, numberOfChildrens, numberOfPassengers, dietrestrict, requestDetailObject;

- (id)initFromDict:(NSDictionary*)dict{
    rowNumber = [[dict objectForKey:@"RowNumber"] integerValue];
    ePCCaseID = [dict objectForKey:@"EPCCaseID"];
    transactionID = [dict objectForKey:@"TransactionID"];
    prefResponse = [dict objectForKey:@"PREFRESPONSE"];
    phoneNumber = [dict objectForKey:@"PHONENUMBER"];
    emailAddress1 = [dict objectForKey:@"EMAILADDRESS1"];
    requestStatus = [dict objectForKey:@"REQUESTSTATUS"];
    requestType = [dict objectForKey:@"REQUESTTYPE"];
    city = [dict objectForKey:@"CITY"];
    country = [dict objectForKey:@"COUNTRY"];
    eventDate = [dict objectForKey:@"EVENTDATE"];
    pickupDate = [dict objectForKey:@"PICKUPDATE"];
    startDate = [dict objectForKey:@"STARTDATE"];
    endDate = [dict objectForKey:@"ENDDATE"];
    createdDate = [dict objectForKey:@"CREATEDDATE"];
    requestMode = [dict objectForKey:@"REQUESTMODE"];
    requestDetails = [dict objectForKey:@"REQUESTDETAILS"];
    
    
    functionality = [dict objectForKey:@"FUNCTIONALITY"];
    situation = [dict objectForKey:@"SITUATION"];
    numberOfAdults = [dict objectForKey:@"NUMBEROFADULTS"];
    numberOfChildrens = [dict objectForKey:@"NUMBEROFCHILDRENS"];
    numberOfPassengers = [dict objectForKey:@"NUMBEROFPASSENGERS"];
    dietrestrict = [dict objectForKey:@"DIETRESTRICT"];
    
    
    myRequestType = [self parsedRequestType:[dict objectForKey:@"REQUESTTYPE"]];

    requestDetailObject = [self getObjectByString:[self stringValueForDict:[dict objectForKey:@"REQUESTDETAILS"]]];
    
    return self;
}

-(NSString*)getStringName {
    return [self getNameByType:self.requestType];
}

-(NSString *)getStringType {
    switch (self.myRequestType) {
        case MY_REQUEST_TYPE_DINING:
            return  @"Restaurant Request";
            break;
        case MY_REQUEST_TYPE_HOTEL:
            return  @"HOTEL AND B&B Request";
            break;
        case MY_REQUEST_TYPE_GOLF:
            return  @"Golf Request";
            break;
        case MY_REQUEST_TYPE_CAR_RENTAL:
            return  @"Limo and Sedan Request";
            break;
        case MY_REQUEST_TYPE_ENTERTAIMENT:
            return  @"Entertainment";
            break;
        case MY_REQUEST_TYPE_BOOK_A_TOUR:
            return  @"Sightseeing/Tours Request";
            break;
        case MY_REQUEST_TYPE_FLIGHT:
            return  @"Airport Services Request";
            break;
        case MY_REQUEST_TYPE_RECOMMEND_CRUISE:
            return  @"Cruise Request";
            break;
        case MY_REQUEST_TYPE_FLOWER_DELIVERY:
            return  @"Flowers/Gift Basket Request";
            break;
        case MY_REQUEST_TYPE_PRIVATE_JET:
            return  @"Private Jet Request";
            break;
        case MY_REQUEST_TYPE_OTHERS:
            return  @"Other Request";
            break;
        default:
            return @"";
            break;
    }
}

-(NSString*)stringValueForDict:(NSString*)string{
    if (![string isKindOfClass:[NSNull class]] && string != nil && string) {
        return string;
    }
    return @"";
}


- (NSDictionary *)getObjectByString:(NSString *)item
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    NSArray *requestDetailsArray = item ? [item componentsSeparatedByString:@"|"] : nil;
    if(requestDetailsArray.count > 0)
    {
        for (int i = 0; i< requestDetailsArray.count; i++) {
            if ([requestDetailsArray[i] rangeOfString:@":"].location != NSNotFound) {
                NSString *key = [requestDetailsArray[i] substringToIndex:[requestDetailsArray[i] rangeOfString:@":"].location];
                key = [key stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                
                NSString *value = [requestDetailsArray[i] substringFromIndex:[requestDetailsArray[i] rangeOfString:@":"].location+1];
                value = [value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                
                [dict setValue:value forKey:key];
            }
        }
    }
    return dict;
}


- (NSString*)getNameByType:(NSString*)type{
    NSString *name = @"";
    if(self.requestDetailObject)
    {
        if([type isEqualToString:DINING_REQUEST_TYPE])
        {
            name = [self stringValueForDict:[self.requestDetailObject objectForKey:@"RestaurantName"]];
        }
        else if([type isEqualToString:HOTEL_REQUEST_TYPE])
        {
            name = [self stringValueForDict:[self.requestDetailObject objectForKey:@"HotelName"]];
        }
        else if([type isEqualToString:GOLF_REQUEST_TYPE] || [type isEqualToString:GOLF_RED_REQUEST_TYPE])
        {
            name = [self stringValueForDict:[self.requestDetailObject objectForKey:@"ReservationName"]];
            if ([name isEqualToString:@""]) {
                name = [self stringValueForDict:[self.requestDetailObject objectForKey:@"GolfCourseName"]];
            }
        }
//        else if([type isEqualToString:TRANSPORTATION_REQUEST_TYPE])
//        {
//            name = [self stringValueForDict:[self.requestDetailObject objectForKey:@"DriverName"]];
//        }
        else if([type isEqualToString:ENTERTAINMENT_REQUEST_TYPE_SERVICE] || [type isEqualToString:ENTERTAINMENT_REQUEST_TYPE])
        {
            name = [self stringValueForDict:[self.requestDetailObject objectForKey:@"EventName"]];
        }
        else if([type isEqualToString:TOUR_REQUEST_TYPE] || [type isEqualToString:AIRPORT_SERVICE_REQUEST_TYPE])
        {
            name = [self stringValueForDict:[self.requestDetailObject objectForKey:@"TourName"]];
        }else if ([type isEqualToString:CRUISE_REQUEST_TYPE]){
            name = [self stringValueForDict:[self.requestDetailObject objectForKey:@"CruiseName"]];
        }
    }
    
    return [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}




-(enum MY_REQUEST_TYPE) parsedRequestType:(NSString*) type{
    if ([type isEqualToString:DINING_REQUEST_TYPE]) {
        return MY_REQUEST_TYPE_DINING;
    }
    if ([type isEqualToString:HOTEL_REQUEST_TYPE]) {
        return MY_REQUEST_TYPE_HOTEL;
    }
    if ([type isEqualToString:GOLF_REQUEST_TYPE] || [type isEqualToString:GOLF_RED_REQUEST_TYPE]) {
        return MY_REQUEST_TYPE_GOLF;
    }
    if ([type isEqualToString:TRANSPORTATION_REQUEST_TYPE] || [type isEqualToString:TRANSFER_REQUEST_TYPE]) {
        return MY_REQUEST_TYPE_CAR_RENTAL;
    }
    if ([type isEqualToString:ENTERTAINMENT_REQUEST_TYPE]) {
        return MY_REQUEST_TYPE_ENTERTAIMENT;
    }
    if ([type isEqualToString:ENTERTAINMENT_REQUEST_TYPE_SERVICE]) {
        return MY_REQUEST_TYPE_ENTERTAIMENT;
    }
    if ([type isEqualToString:TOUR_REQUEST_TYPE]) {
        return MY_REQUEST_TYPE_BOOK_A_TOUR;
    }
    if ([type isEqualToString:AIRPORT_SERVICE_REQUEST_TYPE]) {
        return MY_REQUEST_TYPE_FLIGHT;
    }
    if ([type isEqualToString:CRUISE_REQUEST_TYPE]) {
        return MY_REQUEST_TYPE_RECOMMEND_CRUISE;
    }
    if ([type isEqualToString:FLOWERS_REQUEST_TYPE]) {
        return MY_REQUEST_TYPE_FLOWER_DELIVERY;
    }
    if ([type isEqualToString:TRAVEL_REQUEST_TYPE]) {
        return MY_REQUEST_TYPE_PRIVATE_JET;
    }
    return MY_REQUEST_TYPE_OTHERS;
}





@end
