//
//  PreferenceObject.h
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 7/26/17.
//  Copyright © 2017 Sunrixe Software Solutions Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    DiningPreferenceType,
    HotelPreferenceType,
    TransportationPreferenceType,
} PreferenceType;

@interface PreferenceObject : NSObject

@property (strong, nonatomic) NSString* preferenceID;
@property (strong, nonatomic) NSString* value;
@property (nonatomic) PreferenceType type;

- (id)initFromDict:(NSDictionary*)dict;

@end
