//
//  PreferenceObject.m
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 7/26/17.
//  Copyright © 2017 Sunrixe Software Solutions Corporation. All rights reserved.
//

#import "PreferenceObject.h"
#import "AppData.h"

@implementation PreferenceObject
@synthesize preferenceID, value, type;

- (id)initFromDict:(NSDictionary*)dict{
    
    NSDictionary *preferenceData =  [AppData getPreferencesItems];
    preferenceID = [dict objectForKey:@"MYPREFERENCESID"];
    NSString *tempValue = [dict objectForKey:@"VALUE"];
    value = ([tempValue.uppercaseString isEqualToString:@"NA"]) ? @"" : tempValue;
    NSString *typeString = [dict objectForKey:@"TYPE"];
    if ([typeString.uppercaseString isEqualToString:@"DINING"]) {
        NSArray *diningData = [preferenceData objectForKey:@"DINING"];
        if(![diningData containsObject:value]){
            value = @"";
        }
        type = DiningPreferenceType;
    }else if ([typeString.uppercaseString isEqualToString:@"HOTEL"]) {
        NSArray *hotelData = [preferenceData objectForKey:@"HOTEL"];
        if(![hotelData containsObject:value]){
            value = @"";
        }
        type = HotelPreferenceType;
    }else if ([typeString.uppercaseString isEqualToString:@"CAR RENTAL"]) {
        NSArray *transportationData = [preferenceData objectForKey:@"TRANSPORTATION"];
        if(![transportationData containsObject:value]){
            value = @"";
        }
        type = TransportationPreferenceType;
    }
    return self;
}

@end
