//
//  CitiesManager.h
//  TestIOS
//
//  Created by Dai Pham on 7/12/17.
//  Copyright © 2017 Dai Pham. All rights reserved.
//

#define vShopping               @"Shopping"
#define vGolfMerchandise        @"Golf Merchandise"
#define vFlowers                @"Flowers"
#define vWine                   @"Wine"
#define vVIPTravelServices      @"VIP Travel Services"
#define vTravelServices         @"Travel Services"
#define vAirportServices        @"Airport Services"
#define vPrivateJetTravel       @"Private Jet Travel"
#define vTravel                 @"Travel"
#define vSightseeing            @"Sightseeing"
#define vEntertainment          @"Entertainment"
#define vGolf                   @"Golf"
#define vSpecialtyTravel        @"Specialty Travel"
#define vGolfExperiences        @"Golf Experiences"
#define vTickets                @"Tickets"
#define vCulinaryExperiences    @"Culinary Experiences"
#define vMajorSportsEvents      @"Major Sports Events"
#define vHotel                  @"Hotels"
#define vVacationPackage        @"Vacation Packages"
#define vCruise                 @"Cruises"
#define vTransportation         @"Transportation"
#define vEntertainmentExperiences @"Entertainment Experiences"
#define vDining                 @"Dining"
#define vRetailShopping         @"Retail Shopping"

#import <Foundation/Foundation.h>

@interface TempObject: NSObject
@property (strong,nonatomic) NSString* Category;
@property (strong,nonatomic) NSString* SubCategory;
@property (strong,nonatomic) NSString* ParentCategory;
@property (assign,nonatomic) NSUInteger ID;

- (id) initWithObject:(NSDictionary*)dict;
@end

@interface CCAMapCategories : NSObject {
    NSArray* listObjects;
}

@property (copy) void (^onDone)();

+ (CCAMapCategories*) shared;
+ (void) startService;

/*
+ (void) getCategoriesIDsShopping:(id)object onDone:(void(^)(id,NSArray*))onDone;
+ (void) getCategoriesIDsTravelService:(id)object onDone:(void(^)(id,NSArray*))onDone;
+ (void) getCategoriesIDsAirportService:(id)object onDone:(void(^)(id,NSArray*))onDone;
+ (void) getCategoriesIDsTravel:(id)object onDone:(void(^)(id,NSArray*))onDone;
+ (void) getCategoriesIDsTour:(id)object onDone:(void(^)(id,NSArray*))onDone;
+ (void) getCategoriesIDsGolf:(id)object onDone:(void(^)(id,NSArray*))onDone;
+ (void) getCategoriesIDsEntertainment:(id)object onDone:(void(^)(id,NSArray*))onDone;
+ (void) getCategoriesIDsDining:(id)object onDone:(void(^)(id,NSArray*))onDone;
+ (void) getCategoriesIDsHotel:(id)object onDone:(void(^)(id,NSArray*))onDone;
+ (void) getCategoriesIDsFlower:(id)object onDone:(void(^)(id,NSArray*))onDone;
+ (void) getCategoriesIDsVacationPackages:(id)object onDone:(void(^)(id,NSArray*))onDone;
+ (void) getCategoriesIDsCruise:(id)object onDone:(void(^)(id,NSArray*))onDone;
+ (void) getCategoriesIDsTransportation:(id)object onDone:(void(^)(id,NSArray*))onDone;
*/

//+ (void) getNameCategoryItemFromID:(NSString*) itemID onDone:(void(^)(NSString*))onDone;

// get CategoriesIDsMutilCities
+ (NSArray*) getCategoriesIDsMutilCities;

//+ (NSArray*) getCategoriesIDsForCategoryHasGeographic;
@end
