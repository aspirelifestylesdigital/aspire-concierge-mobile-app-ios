//
//  CCAMapCategories.m
//  TestIOS
//
//  Created by Dai Pham on 7/12/17.
//  Copyright © 2017 Dai Pham. All rights reserved.
//

#import "Constant.h"
#import "CCAMapCategories.h"

#define kParentCategory @"ParentCategory"
#define kCategory       @"Category"
#define kSubCategory    @"SubCategory"

@interface TempObject()@end

@implementation TempObject
- (id) initWithObject:(NSDictionary*)dict {
    if(self = [super init]) {
        for (NSString* key in [dict allKeys]) {
            if([dict objectForKey:key] != nil || ![[dict objectForKey:key] isEqual:[NSNull null]]) {
                [self setValue:[dict objectForKey:key] forKey:key];
            }
        }
    }
    
    return self;
}

- (void) setValue:(id)value forKey:(NSString *)key {
    if([value isEqual:[NSNull null]]) return;
    if(value == nil) return;
    [super setValue:value forKey:key];
}
@end

#pragma mark -
@interface CCAMapCategories() {
    NSArray* listIDSMutilCities;
    NSMutableArray* listQueues;
    BOOL isGetting;
}

@property (copy) void (^onGetListCCAMapCategoriesDone)();

@end

@implementation CCAMapCategories
static CCAMapCategories *_shared;

+ (CCAMapCategories*) shared {
    static dispatch_once_t onceInstance;
    if(!_shared) {
        dispatch_once(&onceInstance, ^{
            _shared = [CCAMapCategories new];
        });
    }
    
    _shared.onGetListCCAMapCategoriesDone = ^{
        if(_shared->listQueues.count > 0) {
            for (NSDictionary* item in _shared->listQueues) {
                [_shared getCategoriesIDsForObject:[item objectForKey:@"object"] withKey:[item objectForKey:@"condition"] onDone:[item objectForKey:@"callback"]];
            }
            [_shared->listQueues removeAllObjects];
        }
    };
    return _shared;
}

+ (void)startService {
    if(!_shared) {
        [CCAMapCategories shared];
    }
}

- (instancetype) init {
    if(self = [super init]) {
        [self getData];
        listQueues = [NSMutableArray new];
    }
    return self;
}

#pragma mark - INTERFACE
/*
+ (void) getCategoriesIDsShopping:(id)object onDone:(void (^)(id, NSArray *))onDone{
    if(!_shared) {
        [CCAMapCategories shared];
    }
    NSArray* condition = @[[[TempObject alloc] initWithObject:@{kParentCategory:vShopping,
                                                                kCategory:vGolfMerchandise}],
                           
                           [[TempObject alloc] initWithObject:@{kParentCategory:vShopping,
                                                                kCategory:vRetailShopping}],
                           
                           [[TempObject alloc] initWithObject:@{kParentCategory:vShopping,
                                                                kCategory:vFlowers}],
                           
                           [[TempObject alloc] initWithObject:@{kParentCategory:vEntertainment,
                                                                kCategory:vGolf}],
                           
                           [[TempObject alloc] initWithObject:@{kParentCategory:vShopping,
                                                                kCategory:vWine}]];
    if(_shared->isGetting) {
        [_shared->listQueues addObject:@{@"object":object,@"condition":condition,@"callback":onDone}];
    } else {
        if(onDone) {
            onDone(object,[_shared getCategoriesIDsWithKey:condition]);
        }
    }
}

+ (void) getCategoriesIDsTravelService:(id)object onDone:(void (^)(id, NSArray *))onDone {
    if(!_shared) {
        [CCAMapCategories shared];
    }
    NSArray* condition = @[[[TempObject alloc] initWithObject:@{kCategory:vVIPTravelServices,
                                                                kSubCategory:vTravelServices}]];
    
    if(_shared->isGetting) {
        [_shared->listQueues addObject:@{@"object":object,@"condition":condition,@"callback":onDone}];
    } else {
        if(onDone) {
            onDone(object,[_shared getCategoriesIDsWithKey:condition]);
        }
    }
}

+ (void) getCategoriesIDsAirportService:(id)object onDone:(void (^)(id, NSArray *))onDone {
    if(!_shared) {
        [CCAMapCategories shared];
    }
    
    NSArray* condition = @[[[TempObject alloc] initWithObject:@{kCategory:vVIPTravelServices,
                                                                kSubCategory:vAirportServices}]];
    if(_shared->isGetting) {
        [_shared->listQueues addObject:@{@"object":object,@"condition":condition,@"callback":onDone}];
    } else {
        if(onDone) {
            onDone(object,[_shared getCategoriesIDsWithKey:condition]);
        }
    }
}

+ (void) getCategoriesIDsTravel:(id)object onDone:(void (^)(id, NSArray *))onDone {
    if(!_shared) {
        [CCAMapCategories shared];
    }
    
    NSArray* condition = @[[[TempObject alloc] initWithObject:@{kParentCategory:vTravel,
                                                                kCategory:vPrivateJetTravel}]];
    if(_shared->isGetting) {
        [_shared->listQueues addObject:@{@"object":object,@"condition":condition,@"callback":onDone}];
    } else {
        if(onDone) {
            onDone(object,[_shared getCategoriesIDsWithKey:condition]);
        }
    }
}

+ (void) getCategoriesIDsTour:(id)object onDone:(void (^)(id, NSArray *))onDone {
    if(!_shared) {
        [CCAMapCategories shared];
    }
    
    NSArray* condition = @[[[TempObject alloc] initWithObject:@{kCategory:vVIPTravelServices,
                                                                kSubCategory:vSightseeing}]];
    if(_shared->isGetting) {
        [_shared->listQueues addObject:@{@"object":object,@"condition":condition,@"callback":onDone}];
    } else {
        if(onDone) {
            onDone(object,[_shared getCategoriesIDsWithKey:condition]);
        }
    }
}

+ (void) getCategoriesIDsGolf:(id)object onDone:(void (^)(id, NSArray *))onDone {
    if(!_shared) {
        [CCAMapCategories shared];
    }
    
    NSArray* condition = @[[[TempObject alloc] initWithObject:@{kParentCategory:vEntertainment,
                                                                kCategory:vGolf}],
                           
                           [[TempObject alloc] initWithObject:@{kParentCategory:vShopping,
                                                                kCategory:vGolfMerchandise}],
                           
                           [[TempObject alloc] initWithObject:@{kCategory:vSpecialtyTravel,
                                                                kSubCategory:vGolfExperiences}]];
    if(_shared->isGetting) {
        [_shared->listQueues addObject:@{@"object":object,@"condition":condition,@"callback":onDone}];
    } else {
        if(onDone) {
            onDone(object,[_shared getCategoriesIDsWithKey:condition]);
        }
    }
}

+ (void) getCategoriesIDsEntertainment:(id)object onDone:(void (^)(id, NSArray *))onDone {
    if(!_shared) {
        [CCAMapCategories shared];
    }
    
    NSArray* condition = @[[[TempObject alloc] initWithObject:@{kParentCategory:vEntertainment,
                                                                kCategory:vTickets}],
                           
                           [[TempObject alloc] initWithObject:@{kCategory:vSpecialtyTravel,
                                                                kSubCategory:vMajorSportsEvents}],
                           
                           [[TempObject alloc] initWithObject:@{kCategory:vSpecialtyTravel,
                                                                kSubCategory:vEntertainmentExperiences}]];
    if(_shared->isGetting) {
        [_shared->listQueues addObject:@{@"object":object,@"condition":condition,@"callback":onDone}];
    } else {
        if(onDone) {
            onDone(object,[_shared getCategoriesIDsWithKey:condition]);
        }
    }
}

+ (void) getCategoriesIDsDining:(id)object onDone:(void (^)(id, NSArray *))onDone {
    if(!_shared) {
        [CCAMapCategories shared];
    }
    
    NSArray* condition = @[[[TempObject alloc] initWithObject:@{kCategory:vSpecialtyTravel,
                                                                kSubCategory:vCulinaryExperiences}],
                           
                           [[TempObject alloc] initWithObject:@{kParentCategory:vDining,
                                                                kCategory:vDining}]];
    if(_shared->isGetting) {
        [_shared->listQueues addObject:@{@"object":object,@"condition":condition,@"callback":onDone}];
    } else {
        if(onDone) {
            onDone(object,[_shared getCategoriesIDsWithKey:condition]);
        }
    }
}

+ (void)getCategoriesIDsHotel:(id)object onDone:(void (^)(id, NSArray *))onDone {
    if(!_shared) {
        [CCAMapCategories shared];
    }
    
    NSArray* condition = @[[[TempObject alloc] initWithObject:@{kParentCategory:vTravel,
                                                                kCategory:vHotel}]];
    if(_shared->isGetting) {
        [_shared->listQueues addObject:@{@"object":object,@"condition":condition,@"callback":onDone}];
    } else {
        if(onDone) {
            onDone(object,[_shared getCategoriesIDsWithKey:condition]);
        }
    }
}

+ (void)getCategoriesIDsFlower:(id)object onDone:(void (^)(id, NSArray *))onDone{
    if(!_shared) {
        [CCAMapCategories shared];
    }
    
    NSArray* condition = @[[[TempObject alloc] initWithObject:@{kParentCategory:vShopping,
                                                                kCategory:vFlowers}]];
    if(_shared->isGetting) {
        [_shared->listQueues addObject:@{@"object":object,@"condition":condition,@"callback":onDone}];
    } else {
        if(onDone) {
            onDone(object,[_shared getCategoriesIDsWithKey:condition]);
        }
    }
}

+ (void)getCategoriesIDsVacationPackages:(id)object onDone:(void (^)(id, NSArray *))onDone {
    if(!_shared) {
        [CCAMapCategories shared];
    }
    
    NSArray* condition = @[[[TempObject alloc] initWithObject:@{kParentCategory:vTravel,
                                                                kCategory:vVacationPackage}]];
    if(_shared->isGetting) {
        [_shared->listQueues addObject:@{@"object":object,@"condition":condition,@"callback":onDone}];
    } else {
        if(onDone) {
            onDone(object,[_shared getCategoriesIDsWithKey:condition]);
        }
    }
}

+ (void)getCategoriesIDsCruise:(id)object onDone:(void (^)(id, NSArray *))onDone {
    if(!_shared) {
        [CCAMapCategories shared];
    }
    
    NSArray* condition = @[[[TempObject alloc] initWithObject:@{kParentCategory:vTravel,
                                                                kCategory:vCruise}]];
    if(_shared->isGetting) {
        [_shared->listQueues addObject:@{@"object":object,@"condition":condition,@"callback":onDone}];
    } else {
        if(onDone) {
            onDone(object,[_shared getCategoriesIDsWithKey:condition]);
        }
    }
}

+ (void)getCategoriesIDsTransportation:(id)object onDone:(void (^)(id, NSArray *))onDone {
    if(!_shared) {
        [CCAMapCategories shared];
    }
    
    NSArray* condition = @[[[TempObject alloc] initWithObject:@{kParentCategory:vTravel,
                                                                kCategory:vTransportation}]];
    if(_shared->isGetting) {
        [_shared->listQueues addObject:@{@"object":object,@"condition":condition,@"callback":onDone}];
    } else {
        if(onDone) {
            onDone(object,[_shared getCategoriesIDsWithKey:condition]);
        }
    }
}
 */

/*
+ (void) getNameCategoryItemFromID:(NSString *)itemID onDone:(void (^)(NSString *))onDone{
    if(itemID.length == 0) {
        if(onDone)
            onDone(nil);
        return;
    }
    
    [self getCategoriesIDsGolf:nil onDone:^(id a, NSArray* lG){
        if([lG containsObject:itemID]) {
            if(onDone)
                onDone(vGolf);
        } else
            [self getCategoriesIDsTour:nil onDone:^(id a,NSArray* lTou) {
                if([lG containsObject:itemID]) {
                    if(onDone)
                        onDone(@"Tour");
                } else
                    [self getCategoriesIDsHotel:nil onDone:^(id a,NSArray* lG1){
                        if([lG1 containsObject:itemID]) {
                            if(onDone)
                                onDone(@"Hotels");
                        } else
                            [self getCategoriesIDsCruise:nil onDone:^(id a,NSArray* lG2){
                                if([lG2 containsObject:itemID]) {
                                    if(onDone)
                                        onDone(@"Cruise");
                                } else
                                    [self getCategoriesIDsDining:nil onDone:^(id a,NSArray* lTou1){
                                        if([lTou1 containsObject:itemID]) {
                                            if(onDone)
                                                onDone(@"Dining");
                                        } else
                                            [self getCategoriesIDsFlower:nil onDone:^(id a,NSArray* lTou2){
                                                if([lTou2 containsObject:itemID]) {
                                                    if(onDone)
                                                        onDone(@"Flower");
                                                } else
                                                    [self getCategoriesIDsTravel:nil onDone:^(id a,NSArray* lTou3){
                                                        if([lTou3 containsObject:itemID]) {
                                                            if(onDone)
                                                                onDone(@"Travel");
                                                        } else
                                                            [self getCategoriesIDsShopping:nil onDone:^(id a,NSArray* lTou4){
                                                                if([lTou4 containsObject:itemID]) {
                                                                    if(onDone)
                                                                        onDone(@"Shopping");
                                                                } else
                                                                    [self getCategoriesIDsEntertainment:nil onDone:^(id a,NSArray* lTou5){
                                                                        if([lTou5 containsObject:itemID]) {
                                                                            if(onDone)
                                                                                onDone(@"Entertaiment");
                                                                        } else
                                                                            [self getCategoriesIDsTravelService:nil onDone:^(id a,NSArray* lTou6){
                                                                                if([lTou6 containsObject:itemID]) {
                                                                                    if(onDone)
                                                                                        onDone(@"Travel Services");
                                                                                } else
                                                                                    [self getCategoriesIDsAirportService:nil onDone:^(id a,NSArray* lTou7){
                                                                                        if([lTou7 containsObject:itemID]) {
                                                                                            if(onDone)
                                                                                                onDone(@"Airport Service");
                                                                                        } else
                                                                                            [self getCategoriesIDsTransportation:nil onDone:^(id a,NSArray* lTou8){
                                                                                                if([lTou8 containsObject:itemID]) {
                                                                                                    if(onDone)
                                                                                                        onDone(@"Transportation");
                                                                                                } else
                                                                                                    if(onDone)
                                                                                                        onDone(nil);
                                                                                            }];
                                                                                    }];
                                                                            }];
                                                                    }];
                                                            }];
                                                    }];
                                            }];
                                    }];
                            }];
                    }];
            }];
    }];
}
*/

+ (NSArray*)getCategoriesIDsMutilCities {
    if(!_shared) {
        [CCAMapCategories shared];
    }
    if(!_shared->listIDSMutilCities) {
        _shared->listIDSMutilCities = [_shared getCategoriesIDsWithKey:@[
                                              [[TempObject alloc] initWithObject:@{kParentCategory:vEntertainment,
                                                                                   kCategory:vGolf}],
                                              
                                              [[TempObject alloc] initWithObject:@{kCategory:vGolf,
                                                                                   kParentCategory:vShopping}],
                                              
                                              [[TempObject alloc] initWithObject:@{kCategory:vPrivateJetTravel,
                                                                                   kParentCategory:vTravel}],
                                              
                                              [[TempObject alloc] initWithObject:@{kParentCategory:vEntertainment,
                                                                                   kCategory:vTickets}],
                                              
                                              [[TempObject alloc] initWithObject:@{kParentCategory:vTravel,
                                                                                   kCategory:vTransportation}],
                                              
                                              [[TempObject alloc] initWithObject:@{kCategory:vVIPTravelServices,
                                                                                   kParentCategory:vTravel}],
                                              
                                              [[TempObject alloc] initWithObject:@{kParentCategory:vShopping,
                                                                                   kCategory:vGolfMerchandise}],
                                              
                                              [[TempObject alloc] initWithObject:@{kParentCategory:vShopping,
                                                                                   kCategory:vFlowers}],
                                              
                                              [[TempObject alloc] initWithObject:@{kParentCategory:vShopping,
                                                                                   kCategory:vWine}],
                                              
                                              [[TempObject alloc] initWithObject:@{kCategory:vVIPTravelServices,
                                                                                   kSubCategory:vAirportServices}]
                                              ]];
    }
    return _shared->listIDSMutilCities;
}

/*
+ (NSArray *)getCategoriesIDsForCategoryHasGeographic {
    NSMutableArray* temp = [NSMutableArray new];
    [CCAMapCategories getCategoriesIDsHotel:temp onDone:^(NSMutableArray* t,NSArray* list){
        [t addObjectsFromArray:list];
    }];
    [CCAMapCategories getCategoriesIDsCruise:temp onDone:^(NSMutableArray* t,NSArray* list){
        [t addObjectsFromArray:list];
    }];
    [CCAMapCategories getCategoriesIDsVacationPackages:temp onDone:^(NSMutableArray* t,NSArray* list){
        [t addObjectsFromArray:list];
    }];
    
    return temp;
}
 */

#pragma mark - PRIVATE
- (void) getCategoriesIDsForObject:(id)object withKey:(NSArray*)condition onDone:(void (^)(id, NSArray *))onDone {
    if(onDone)
        onDone(object,[self getCategoriesIDsWithKey:condition]);
}

- (NSArray *)getCategoriesIDsWithKey:(NSArray<TempObject*> *)key {
    if(key.count == 0 || listObjects.count == 0)
        return nil;
    
    NSMutableArray* temp = [NSMutableArray new];
    for (TempObject* obj in key) {
        
        if(obj.ParentCategory.length > 0 && obj.Category.length > 0) {
            
            for (TempObject* item in listObjects) {
                BOOL isOk = NO;
                if([obj.ParentCategory isEqualToString:item.ParentCategory] && [obj.Category isEqualToString:item.Category]) {
                    isOk = YES;
                    
                }
                
                if(isOk)
                    [temp addObject:[NSString stringWithFormat:@"%lu",(unsigned long)item.ID]];
            }
        } else if(obj.Category.length > 0 && obj.SubCategory.length > 0) {
            for (TempObject* item in listObjects) {
                BOOL isOk = NO;
                if([obj.Category isEqualToString:item.Category] && [obj.SubCategory isEqualToString:item.SubCategory]) {
                    isOk = YES;
                    
                }
                
                if(isOk)
                    [temp addObject:[NSString stringWithFormat:@"%lu",(unsigned long)item.ID]];
            }
        }
    }
    return temp;
}

- (void) getData {
    isGetting = YES;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSString*  filepath = [[NSBundle mainBundle] pathForResource:@"CCA" ofType:@"json"];
        NSError* error;
        NSString*myJson = [[NSString alloc] initWithContentsOfFile:filepath encoding:NSUTF8StringEncoding error:&error];
        if(error != nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self involkeBlock];
            });
            return;
        }
        
        NSArray* data = [NSJSONSerialization JSONObjectWithData:[myJson dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
        
        if(error != nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self involkeBlock];
            });
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self convertDataToTempObject:data];
        });
    });
}

- (void) involkeBlock {
    isGetting = NO;
    if(self.onGetListCCAMapCategoriesDone)
        self.onGetListCCAMapCategoriesDone();
    if(_onDone)
        _onDone();
}

- (void) convertDataToTempObject:(NSArray*)data {
    if(data.count == 0)
        return;
    NSMutableArray* temp = [NSMutableArray new];
    for (NSDictionary* obj in data) {
        [temp addObject:[[TempObject alloc] initWithObject:obj]];
    }
    listObjects = temp;
    temp = nil;
    [self involkeBlock];
}
@end
