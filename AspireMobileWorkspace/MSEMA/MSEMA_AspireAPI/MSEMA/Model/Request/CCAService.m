//
//  CCAService.m
//  MobileConciergeUSDemo
//
//  Created by Dai Pham on 8/5/17.
//  Copyright © 2017 Sunrixe Software Solutions Corporation. All rights reserved.
//
#import "Constant.h"

// services
#import "SyncService.h"

// models
#import "BaseItem.h"

#import "CCAService.h"

@interface CCAService() {
    id response;
    NSDate* timeToRefresh;
}

@end

static CCAService* service;

@implementation CCAService

#pragma mark - SERVICE
+ (CCAService*) startService {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [CCAService new];
    });
    return service;
}

+ (CCAService *)service {
    return service;
}

+ (id)responseFromCCA {
    
    // checking if after 10 minutes
    if(service.isGetCCADone && [service->timeToRefresh timeIntervalSinceNow] > -10*60) {
        return service->response;
    }
    
    // start get service again if service is freetime
    if(!service) {
        [CCAService startService];
    } else
    if(service.isGetCCADone) {
        [service getDataCCA];
    }
    return nil;
}

#pragma mark - INIT
- (id) init {
    if(self = [super init]) {
        _isGetCCADone = NO;
    }
    
    // start get all data for CCA
    [self getDataCCA];
    
    return self;
}

#pragma mark - PRIVATE
- (void) getDataCCA {
    
    // mark last time get CCA
    timeToRefresh = [NSDate date];
    
    [SyncService postUrl:[B2C_IA_API_URL stringByAppendingString:GetTiles] withParams:[self buildRequestParams] OnDone:^(id res) {
        _isGetCCADone = YES;
        response = res;
    } onError:^(NSError* error){
#ifdef DEBUG
        NSLog(@"%s: CCA error",__PRETTY_FUNCTION__);
#endif
    }];
}

- (NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    
    // cause of security scanning, not allow get and set password directly
    NSString* userPass = B2C_PW;
    [dictKeyValues setObject:B2C_SUBDOMAIN forKey:@"subDomain"];
    [dictKeyValues setObject:userPass forKey:@"password"];
    NSData *json = [NSJSONSerialization dataWithJSONObject:self.categories options:0 error:nil];
    NSArray *array = [NSJSONSerialization JSONObjectWithData:json options:0 error:nil];
    [dictKeyValues setObject:array forKey:@"categories"];
    return dictKeyValues;
}

- (NSArray*) categories {
    return LIST_ALL_TILES;
}
@end
