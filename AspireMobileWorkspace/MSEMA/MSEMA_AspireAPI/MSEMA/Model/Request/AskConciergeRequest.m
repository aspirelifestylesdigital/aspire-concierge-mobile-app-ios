//
//  AskConciergeRequest.m
//  LuxuryCard
//
//  Created by Chung Mai on 9/11/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "AskConciergeRequest.h"
#import "Constant.h"
#import "EnumConstant.h"
#import "ConciergeDetailObject.h"

#define OTHER_FUNCTIONALITY  @"Others"
#define OTHER_REQUEST_FUNCTIONALITY  @"Other Request"
#define SOURCE @"Mobile App"

#define kAccessToken    @"AccessToken"
#define kConsumerKey    @"ConsumerKey"
#define kEPCClientCode  @"EPCClientCode"
#define kEPCProgramCode @"EPCProgramCode"
#define kSalutation     @"Salutation"
#define kFirstName      @"FirstName"
#define kLastName       @"LastName"
#define kVeriCode       @"VeriCode"
#define kEditType       @"EditType"
#define kSource         @"Source"
#define kFunctionality  @"Functionality"
#define kRequestType    @"RequestType"
#define kPrefResponse   @"PrefResponse"
#define kEmailAddress1  @"EmailAddress1"
#define kPhoneNumber    @"PhoneNumber"
#define kRequestDetails @"RequestDetails"
#define kAttachmentPath @"AttachmentPath"
#define kOnlineMemberId @"OnlineMemberId"
#define kTransactionID  @"TransactionID"


@implementation AskConciergeRequest

+(NSDictionary *) buildRequestMessageWithData:(NSDictionary *)data
{
    NSMutableDictionary* dictKeyValues = nil;
    NSString *editType = data[@"editType"];
    
   if([editType isEqualToString:ADD_EDIT_TYPE])
   {
       
       dictKeyValues = [AskConciergeRequest buildParametersForNewRequestWithEditType:editType WithData:data];
   }
   else if([editType isEqualToString:UPDATE_EDIT_TYPE])
   {
       dictKeyValues = [AskConciergeRequest buildParametersForUpdatingRequestWithEditType:editType WithData:data];
   }else if([editType isEqualToString:CANCEL_EDIT_TYPE])
   {
       dictKeyValues = [AskConciergeRequest buildParametersForUpdatingRequestWithEditType:editType WithData:data];
   }
    return dictKeyValues;
}

+(NSMutableDictionary *)buildParametersForUpdatingRequestWithEditType:(NSString *)editType WithData:(NSDictionary *)data
{
    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
    
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc] init];
    [dictKeyValues setObject:[[SessionData shareSessiondata] AccessToken] forKey:kAccessToken];
    [dictKeyValues setObject:[[SessionData shareSessiondata] OnlineMemberID]  forKey:kOnlineMemberId];
    [dictKeyValues setObject:B2C_VERIFICATION_CODE forKey:kVeriCode];
    
    [dictKeyValues setObject:B2C_ConsumerKey forKey:kConsumerKey];
    [dictKeyValues setObject:B2C_EPCClientCode forKey:kEPCClientCode];
    [dictKeyValues setObject:B2C_EPCProgramCode forKey:kEPCProgramCode];
    
    [dictKeyValues setObject:SOURCE forKey:kSource];
    [dictKeyValues setObject:OTHER_FUNCTIONALITY forKey:kFunctionality];
    [dictKeyValues setObject:OTHER_REQUEST_TYPE forKey:kRequestType];
    
    [dictKeyValues setObject:data[kTransactionID] forKey:kTransactionID];
    //    [dictKeyValues setObject:EDIT_TYPE forKey:kVeriCode];
    [dictKeyValues setObject:editType forKey:kEditType];
    
    if (profileDictionary) {
        NSString *salutation = [profileDictionary objectForKey:keySalutation];
        [dictKeyValues setValue:(salutation.length > 0)?salutation:@"N/A" forKey:kSalutation];
        [dictKeyValues setValue:[profileDictionary objectForKey:keyFirstName] forKey:kFirstName];
        [dictKeyValues setValue:[profileDictionary objectForKey:keyLastName] forKey:kLastName];
        [dictKeyValues setValue:[profileDictionary objectForKey:keyMobileNumber] forKey:kPhoneNumber];
        [dictKeyValues setValue:[profileDictionary objectForKey:keyEmail] forKey:kEmailAddress1];
        NSString *requestDetail = @"";
        
        if ([data[@"phone"] boolValue] == YES && [data[@"mail"] boolValue] == YES) {
            [dictKeyValues setValue:@"Email, Mobile" forKey:kPrefResponse];
        } else {
            if ([data[@"phone"] boolValue] == YES) {
                [dictKeyValues setValue:@"Mobile" forKey:kPrefResponse];
            }else if ([data[@"mail"] boolValue] == YES) {
                [dictKeyValues setValue:@"Email" forKey:kPrefResponse];
            }
        }
        requestDetail = [requestDetail stringByAppendingString:data[kRequestDetails]];
        [dictKeyValues setValue:requestDetail forKey:kRequestDetails];
    }
    
    //[dictKeyValues setObject:[data objectForKey:kAttachmentPath] forKey:kAttachmentPath];
    
    return dictKeyValues;
}

+(NSMutableDictionary *)buildParametersForNewRequestWithEditType:(NSString *)editType WithData:(NSDictionary *)data
{
    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
    
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc] init];
    [dictKeyValues setObject:[[SessionData shareSessiondata] AccessToken] forKey:kAccessToken];
    [dictKeyValues setObject:[[SessionData shareSessiondata] OnlineMemberID]  forKey:kOnlineMemberId];
    [dictKeyValues setObject:B2C_VERIFICATION_CODE forKey:kVeriCode];
    
    [dictKeyValues setObject:B2C_ConsumerKey forKey:kConsumerKey];
    [dictKeyValues setObject:B2C_EPCClientCode forKey:kEPCClientCode];
    [dictKeyValues setObject:B2C_EPCProgramCode forKey:kEPCProgramCode];
    
    [dictKeyValues setObject:SOURCE forKey:kSource];
    [dictKeyValues setObject:OTHER_FUNCTIONALITY forKey:kFunctionality];
    [dictKeyValues setObject:OTHER_REQUEST_TYPE forKey:kRequestType];

//    [dictKeyValues setObject:EDIT_TYPE forKey:kVeriCode];
    [dictKeyValues setObject:editType forKey:kEditType];
    
    if (profileDictionary) {
        NSString *salutation = [profileDictionary objectForKey:keySalutation];
        [dictKeyValues setValue:(salutation.length > 0)?salutation:@"N/A" forKey:kSalutation];
        [dictKeyValues setValue:[profileDictionary objectForKey:keyFirstName] forKey:kFirstName];
        [dictKeyValues setValue:[profileDictionary objectForKey:keyLastName] forKey:kLastName];
        [dictKeyValues setValue:[profileDictionary objectForKey:keyMobileNumber] forKey:kPhoneNumber];
        [dictKeyValues setValue:[profileDictionary objectForKey:keyEmail] forKey:kEmailAddress1];
        NSString *requestDetail = @"";
        
        if ([data[@"phone"] boolValue] == YES && [data[@"mail"] boolValue] == YES) {
            [dictKeyValues setValue:@"Email, Mobile" forKey:kPrefResponse];
        } else {
            if ([data[@"phone"] boolValue] == YES) {
                [dictKeyValues setValue:@"Mobile" forKey:kPrefResponse];
            }else if ([data[@"mail"] boolValue] == YES) {
                [dictKeyValues setValue:@"Email" forKey:kPrefResponse];
            }
        }
        requestDetail = [requestDetail stringByAppendingString:data[kRequestDetails]];
        [dictKeyValues setValue:requestDetail forKey:kRequestDetails];
    }
    
    //[dictKeyValues setObject:[data objectForKey:kAttachmentPath] forKey:kAttachmentPath];
    
    return dictKeyValues;
}


+(NSString *)buildAskConciergeTextWithConciergeData:(ConciergeDetailObject *)item
{
    NSString *message = @"";
    if([item.requestType isEqualToString:DINING_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildAskConciergeTextForDiningRequestType:item];
    }
    else if([item.requestType isEqualToString:HOTEL_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildAskConciergeTextForHotelRequestType:item];
    }
    else if([item.requestType isEqualToString:GOLF_REQUEST_TYPE] || [item.requestType isEqualToString:GOLF_RED_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildAskConciergeTextForGolfRequestType:item];
    }
    else if([item.requestType isEqualToString:TRANSPORTATION_REQUEST_TYPE] || [item.requestType isEqualToString:TRANSFER_REQUEST_TYPE])
    {
        if([item.requestDetail containsString:@"DriverName"]){
            message = [AskConciergeRequest buildAskConciergeTextForCarRentalRequestType:item];
        }
        else{
           message = [AskConciergeRequest buildAskConciergeTextForTransferRequestType:item];
        }
        
    }
    else if([item.requestType isEqualToString:ENTERTAINMENT_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildAskConciergeTextForEntertainmentRequestType:item];
    }
    else if([item.requestType isEqualToString:TOUR_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildAskConciergeTextForTourRequestType:item];
    }
    else if([item.requestType isEqualToString:AIRPORT_SERVICE_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildAskConciergeTextForAirPortRequestType:item];
    }
    else if([item.requestType isEqualToString:FLOWERS_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildAskConciergeTextForFlowerRequestType:item];
    }
    else if([item.requestType isEqualToString:TRAVEL_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildAskConciergeTextForPrivateJetRequestType:item];
    }
    else if([item.requestType isEqualToString:CRUISE_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildAskConciergeTextForCruiseRequestType:item];
    }
    else if([item.requestType isEqualToString:VACATION_PACKAGE_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildAskConciergeTextForVacationPackageRequestType:item];
    }
    else
    {
        message = [AskConciergeRequest buildAskConciergeTextForOtherRequestType:item];
    }
    return message;
}


+ (NSString *)buildAskConciergeTextForDiningRequestType:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"Name: %@\n",(item.name ? item.name : @"")];
    [message appendFormat:@"Date/time: %@\n",item.eventDate ? item.eventDate : @""];
    [message appendFormat:@"# of adults: %@\n",item.numberOfAdults ? item.numberOfAdults : @""];
    [message appendFormat:@"# of children: %@\n",item.numberOfChildrens ?  : @""];
    [message appendFormat:@"City: %@\n",item.city ? item.city : @""];
    [message appendFormat:@"State: %@\n",item.state ? item.state : @""];
    [message appendFormat:@"Country: %@\n",item.country ? item.country : @""];
    [message appendFormat:@"Other comments: %@\n",item.commonMessage ? item.commonMessage : @""];
    return message;
}

+(NSString *)buildAskConciergeTextForHotelRequestType:(ConciergeDetailObject *)item
{
    
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"Name: %@\n",item.name ? item.name : @""];
    [message appendFormat:@"Check in: %@\n",item.startDateWithoutTime ? item.startDateWithoutTime : @""];
    [message appendFormat:@"Check out: %@\n",item.endDateWithoutTime ? item.endDateWithoutTime : @""];
    [message appendFormat:@"# of adults: %@\n",item.numberOfAdults ? item.numberOfAdults : @""];
    [message appendFormat:@"# of children: %@\n",item.numberOfChildrens ? item.numberOfChildrens : @""];
    [message appendFormat:@"City: %@\n",item.city ? item.city : @""];
    [message appendFormat:@"State: %@\n",item.state ? item.state : @""];
    [message appendFormat:@"Country: %@\n",item.country ? item.country : @""];
    [message appendFormat:@"Other comments: %@\n",item.commonMessage ? item.commonMessage : @""];
    return message;
}

+(NSString *)buildAskConciergeTextForOtherRequestType:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    if(![item.name isEqualToString:@""])
    {
        [message appendFormat:@"Name: %@\n",item.name];
    }
//    [message appendFormat:@"Other comments: %@\n",item.commonMessage ? item.commonMessage : @""] ;
    [message appendFormat:@"Other comments: Common Message"] ;
    return message;
}

+(NSString *)buildAskConciergeTextForGolfRequestType:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"Name: %@\n",item.name ? item.name : @""];
    [message appendFormat:@"Date: %@\n",item.startDate ? item.startDate : @""];
    [message appendFormat:@"# of golfers: %@\n",item.numberOfAdults ? item.numberOfAdults : @""];
    [message appendFormat:@"City: %@\n",item.city ? item.city : @""];
    [message appendFormat:@"State: %@\n",item.state ? item.state : @""];
    [message appendFormat:@"Country: %@\n",item.country ? item.country : @""];
    [message appendFormat:@"Other comments: %@\n",item.commonMessage ? item.commonMessage : @""];
    return message;
}

+(NSString *)buildAskConciergeTextForCarRentalRequestType:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"Driver: %@\n",item.name ? item.name : @""];
    [message appendFormat:@"Pickup date: %@\n",item.pickUpDate ? item.pickUpDate : @""];
    [message appendFormat:@"Pick up location: %@\n",item.pickUpLocation ? item.pickUpLocation : @""];
    [message appendFormat:@"Drop off location: %@\n",item.dropOffLocation ? item.dropOffLocation : @""];
    [message appendFormat:@"# in party: %@\n",item.numberOfAdults ? item.numberOfAdults : @""];
    [message appendFormat:@"Other comments: %@\n",item.commonMessage ? item.commonMessage : @""];
    return message;
}

+(NSString *)buildAskConciergeTextForTransferRequestType:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    
    [message appendFormat:@"Pickup date: %@\n",item.pickUpDate ? item.pickUpDate : @""];
    [message appendFormat:@"Pick up location: %@\n",item.pickUpLocation ? item.pickUpLocation : @""];
    [message appendFormat:@"Drop off location: %@\n",item.dropOffLocation ? item.dropOffLocation : @""];
    [message appendFormat:@"# in party: %@\n",item.numberOfAdults ? item.numberOfAdults : @""];
    [message appendFormat:@"Other comments: %@\n",item.commonMessage ? item.commonMessage : @""];
    return message;
}

+(NSString *)buildAskConciergeTextForEntertainmentRequestType:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    
    [message appendFormat:@"Name: %@\n",item.name ? item.name : @""];
    [message appendFormat:@"Date: %@\n",item.startDate ? item.startDate : @""];
    [message appendFormat:@"# of tickets: %@\n", item.numberOfAdults ? item.numberOfAdults : @""];
    [message appendFormat:@"City: %@\n",item.city ? item.city : @""];
    [message appendFormat:@"State: %@\n",item.state ? item.state : @""];
    [message appendFormat:@"Country: %@\n",item.country ? item.country : @""];
    [message appendFormat:@"Other comments: %@\n",item.commonMessage ? item.commonMessage : @""];
    return message;
}


// Build for Tour, Flight and Cruise Request Type
+(NSString *)buildAskConciergeTextForTourRequestType:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    if (![item.name isEqualToString:@""]) {
        [message appendFormat:@"Name: %@\n",item.name ? item.name : @""];
    }
    [message appendFormat:@"Start date: %@\n",item.startDate ? item.startDate : @""];
    [message appendFormat:@"End date: %@\n",item.endDate ? item.endDate : @""];
    [message appendFormat:@"Start location: %@\n",item.startLocation ? item.startLocation : @""];
    [message appendFormat:@"End location: %@\n",item.endLocation ? item.endLocation : @""];
    [message appendFormat:@"# of adults: %@\n",item.numberOfAdults ? item.numberOfAdults : @""];
    [message appendFormat:@"# of children: %@\n",item.numberOfChildrens ? item.numberOfChildrens : @""];
    [message appendFormat:@"# of infants: %@\n",item.numberOfInfants ? item.numberOfInfants : @""];
    [message appendFormat:@"Other comments: %@\n",item.commonMessage ? item.commonMessage : @""];
    return message;
}

+(NSString *)buildAskConciergeTextForAirPortRequestType:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"Start date: %@\n",item.startDate ? item.startDate : @""];
    [message appendFormat:@"End date: %@\n",item.endDate ? item.endDate : @""];
    [message appendFormat:@"Start location: %@\n",item.startLocation ? item.startLocation : @""];
    [message appendFormat:@"End location: %@\n",item.endLocation ? item.endLocation : @""];
    [message appendFormat:@"# of adults: %@\n",item.numberOfAdults ? item.numberOfAdults : @""];
    [message appendFormat:@"# of children: %@\n",item.numberOfChildrens ? item.numberOfChildrens : @""];
    [message appendFormat:@"# of infants: %@\n",item.numberOfInfants ? item.numberOfInfants : @""];
    [message appendFormat:@"Other comments: %@\n",item.commonMessage ? item.commonMessage : @""];
    return message;
}

+(NSString *)buildAskConciergeTextForFlowerRequestType:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"Arrangement: %@\n",item.flowerType ? item.flowerType : @""];
    [message appendFormat:@"Delivery date: %@\n",item.deliveryDate ? item.deliveryDate : @""];
    [message appendFormat:@"Delivery address: %@\n",item.dAddress ? item.dAddress : @""];
    [message appendFormat:@"Quantity: %@\n",item.bouquetQuantity ? item.bouquetQuantity : @""];
    [message appendFormat:@"Other comments: %@\n",item.commonMessage ? item.commonMessage : @""];
    return message;
}

+(NSString *)buildAskConciergeTextForPrivateJetRequestType:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"Start date: %@\n",item.startDate ? item.startDate : @""];
    [message appendFormat:@"Start location: %@\n",item.startLocation ? item.startLocation : @""];
    [message appendFormat:@"End location: %@\n",item.endLocation ? item.endLocation : @""];
    [message appendFormat:@"# of passengers: %@\n",item.numberOfPassengers ? item.numberOfPassengers : @""];
    [message appendFormat:@"Other comments: %@\n",item.commonMessage ? item.commonMessage : @""];
    return message;
}

+(NSString *)buildAskConciergeTextForCruiseRequestType:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    if (![item.name isEqualToString:@""]) {
        [message appendFormat:@"Name: %@\n",item.name ? item.name : @""];
    }
    [message appendFormat:@"Start date: %@\n",item.startDateWithoutTime ? item.startDateWithoutTime : @""];
    [message appendFormat:@"End date: %@\n",item.endDateWithoutTime ? item.endDateWithoutTime : @""];
    [message appendFormat:@"Start location: %@\n",item.startLocation ? item.startLocation : @""];
    [message appendFormat:@"End location: %@\n",item.endLocation ? item.endLocation : @""];
    [message appendFormat:@"# of adults: %@\n",item.numberOfAdults ? item.numberOfAdults : @""];
    [message appendFormat:@"# of children: %@\n",item.numberOfChildrens ? item.numberOfChildrens : @""];
     [message appendFormat:@"# of infants: %@\n",item.numberOfInfants ? item.numberOfInfants : @""];
    [message appendFormat:@"Other comments: %@\n",item.commonMessage ? item.commonMessage : @""];
    return message;
}

+(NSString *)buildAskConciergeTextForVacationPackageRequestType:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"Name: %@\n",item.name ? item.name : @""];
    [message appendFormat:@"# of adults: %@\n",item.numberOfAdults ? item.numberOfAdults : @""];
    [message appendFormat:@"# of children: %@\n",item.numberOfChildrens ? item.numberOfChildrens : @""];
    [message appendFormat:@"Other comments: %@\n",item.commonMessage ? item.commonMessage : @""];
    return message;
}

+(NSString *)convertCategoryCodeToMyRequestType:(NSString *)categoryCode
{
    if([categoryCode isEqualToString:@"dining"])
    {
        return DINING_REQUEST_TYPE;
    }
    else if([categoryCode isEqualToString:@"hotels"])
    {
        return HOTEL_REQUEST_TYPE;
    }
    else if([categoryCode isEqualToString:@"flowers"])
    {
        return FLOWERS_REQUEST_TYPE;
    }
    else if([categoryCode isEqualToString:@"entertainment"])
    {
        return ENTERTAINMENT_REQUEST_TYPE;
    }
    else if([categoryCode isEqualToString:@"golf"])
    {
        return GOLF_REQUEST_TYPE;
    }
    else if([categoryCode isEqualToString:@"vacation packages"])
    {
        return VACATION_PACKAGE_REQUEST_TYPE;
    }
    else if([categoryCode isEqualToString:@"cruise"])
    {
        return CRUISE_REQUEST_TYPE;
    }
    else if([categoryCode isEqualToString:@"tours"])
    {
        return TOUR_REQUEST_TYPE;
    }
    else if([categoryCode isEqualToString:@"travel"])
    {
        return TRAVEL_REQUEST_TYPE;
    }
    else if([categoryCode isEqualToString:@"airport services"])
    {
        return AIRPORT_SERVICE_REQUEST_TYPE;
    }
    else if([categoryCode isEqualToString:@"travel services"])
    {
        return TRAVEL_REQUEST_TYPE;
    }
    else if([categoryCode isEqualToString:@"transportation"])
    {
        return TRANSPORTATION_REQUEST_TYPE;
    }
    
    //Shopping and City Guide
    return OTHER_REQUEST_TYPE;
}

+(NSString *)convertCategoryNameToMyRequestType:(NSString *)categoryName andSubcategory:(NSString *)subCategory
{
    if([categoryName isEqualToString:@"Dining"] || ([categoryName isEqualToString:@"Specialty Travel"] && [subCategory isEqualToString:@"Culinary Experiences"]))
    {
        return DINING_REQUEST_TYPE;
    }
    else if([categoryName isEqualToString:@"Hotels"])
    {
        return HOTEL_REQUEST_TYPE;
    }
    else if([categoryName isEqualToString:@"Flowers"])
    {
        return FLOWERS_REQUEST_TYPE;
    }
    else if([categoryName isEqualToString:@"Tickets"] || ([categoryName isEqualToString:@"Specialty Travel"] && [subCategory isEqualToString:@"Entertainment Experiences"]))
    {
        return ENTERTAINMENT_REQUEST_TYPE;
    }
    else if([categoryName isEqualToString:@"Golf"] || [categoryName isEqualToString:@"Golf Merchandise"] || ([categoryName isEqualToString:@"Specialty Travel"] && [subCategory isEqualToString:@"Golf Experiences"]))
    {
        return GOLF_REQUEST_TYPE;
    }
    else if([categoryName isEqualToString:@"Vacation Packages"])
    {
        return VACATION_PACKAGE_REQUEST_TYPE;
    }
    else if([categoryName isEqualToString:@"Cruises"])
    {
        return CRUISE_REQUEST_TYPE;
    }
    else if([categoryName isEqualToString:@"VIP Travel Services"] && [subCategory isEqualToString:@"Sightseeing"])
    {
        return TOUR_REQUEST_TYPE;
    }
    else if([categoryName isEqualToString:@"Private Jet Travel"])
    {
        return TRAVEL_REQUEST_TYPE;
    }
    else if([categoryName isEqualToString:@"VIP Travel Services"] && [subCategory isEqualToString:@"Airport Services"])
    {
        return AIRPORT_SERVICE_REQUEST_TYPE;
    }
    else if([categoryName isEqualToString:@"VIP Travel Services"] && [subCategory isEqualToString:@"Travel Services"])
    {
        return @"";
    }
    else if([categoryName isEqualToString:@"Transportation"])
    {
        return TRANSPORTATION_REQUEST_TYPE;
    }
    // Shopping, City Guide and Travel Services
    return OTHER_REQUEST_TYPE;
}
@end
