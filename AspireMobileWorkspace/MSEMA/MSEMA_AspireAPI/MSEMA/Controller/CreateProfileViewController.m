//
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/13/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "CreateProfileViewController.h"
#import "MenuViewController.h"
#import "SWRevealViewController.h"
#import "UDASignInViewController.h"
#import "AppDelegate.h"
#import "WSCreateUser.h"
#import "WSB2CGetAccessToken.h"
#import "WSB2CGetUserDetails.h"
#import "WSB2CGetRequestToken.h"
#import "WSPreferences.h"
#import "NSString+Utis.h"
#import "UITextField+Extensions.h"
#import "UIButton+Extension.h"
#import "UIView+Extension.h"
#import "UILabel+Extension.h"
#import "HomeViewController.h"
#import "Constant.h"
#import "MyProfileViewController.h"
#import "AppData.h"
#import "AppDelegate.h"
#import "UserObject.h"
#import "UtilStyle.h"
#import "UserRegistrationItem.h"
#import "LocationComponent.h"
#import "PreferenceObject.h"
#import "Country.h"
#import "NSString+AESCrypt.h"
#import "AppColor.h"
#import "CreateSecureView.h"
#import "TermsOfUseViewController.h"
#import "PolicyViewController.h"

@import AspireApiFramework;
@import AspireApiControllers;

#define TEXT_FIELD_FONT_SIZE 18

@interface CreateProfileViewController ()<DataLoadDelegate, UITextFieldDelegate, DropDownViewDelegate, UIGestureRecognizerDelegate>
{
    WSB2CGetAccessToken* wsB2CGetAccessToek;
    WSB2CGetRequestToken* wsB2CGetRequestToken;
    WSB2CGetUserDetails* wsGetUser;
    WSPreferences *wsPreferences;
    AppDelegate* appdelegate;
    UITapGestureRecognizer *tappedOutsideKeyboards;
    NSArray *countries;
    UILabel *termOfUseLbl;
    UILabel *policyLbl;
}

@property (nonatomic, strong) WSCreateUser *wsCreateUser;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnSC;

@end

@implementation CreateProfileViewController
@synthesize partyId;
#pragma mark - LIFECYCLE
- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [_phoneNumberText addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIcont];
    
    // isShouldHideKeyboardWhenSwipe = NO;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateStatusLocation:)
                                                 name:LOCATION_COMPONENT_ISAUTHORIZED_NOTIFICATION
                                               object:nil];
    
    UITapGestureRecognizer *tab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkBox:)];

    self.commitmentLabel.userInteractionEnabled = YES;
    [self.commitmentLabel addGestureRecognizer:tab];
    
    countries = getCountryList();
    [self setUIStyte];
    if (_isUpdateProfile == NO || !_isUpdateProfile) {
        [self trackingScreenByName:@"Sign up"];
    }else{
        [self trackingScreenByName:@"My profile"];
    }
    [self buildAgreeTextViewFromString:NSLocalizedString(@"message_confirm_policy", nil)];
    
    [self setEmail:_emailChecked];
    [self setPhone:_phoneChecked];
    [self setLastName:_lastNameChecked];
    [self setFirstName:_firstNameChecked];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    if (![self isKindOfClass:[MyProfileViewController class]]) {
        [self.navigationController setNavigationBarHidden:YES animated:NO];
    }
    
    [super viewWillAppear:animated];
    [self setTextViewsDefaultBottomBolder];
    tappedOutsideKeyboards = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tappedOutsideKeyboards.delegate = self;
    [self.navigationController.view addGestureRecognizer:tappedOutsideKeyboards];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidChange:) name:UITextFieldTextDidChangeNotification object:self.phoneNumberText];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController.view removeGestureRecognizer:tappedOutsideKeyboards];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:self.phoneNumberText];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - SETUPVIEW
- (void) setUIStyte {
    
    self.phoneNumberText.keyboardType = UIKeyboardTypePhonePad;
    self.phoneNumberText.tag = PHONE_NUMBER_TEXTFIELD_TAG;
    self.emailText.keyboardType = UIKeyboardTypeEmailAddress;
    self.firstNameText.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    self.lastNameText.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    
    self.emailText.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailText.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.emailText.textColor = [AppColor placeholderTextColor];
    
    self.numberCardText.autocorrectionType = UITextAutocorrectionTypeNo;
    self.numberCardText.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    self.salutationDropDown.leadingTitle = 8.0f;
    self.salutationDropDown.title = @"Please select a salutation.";
    self.salutationDropDown.listItems = @[@"Dr", @"Miss", @"Mr", @"Mrs", @"Ms"];
    self.salutationDropDown.titleColor = colorFromHexString(TEXTFIELD_PLACEHOLDER_COLOR_STANDARD);
    self.salutationDropDown.delegate = self;
    
    self.firstNameText.delegate = self;
    self.lastNameText.delegate = self;
    self.emailText.delegate = self;
    self.phoneNumberText.delegate = self;
    self.numberCardText.delegate = self;
    self.passwordText.delegate = self;
    self.confirmPasswordText.delegate = self;
    
//    [self createAsteriskForTextField:self.firstNameText];
//    [self createAsteriskForTextField:self.lastNameText];
//    [self createAsteriskForTextField:self.emailText];
//    [self createAsteriskForTextField:self.phoneNumberText];
//    [self createAsteriskForTextField:self.numberCardText];
//    [self createAsteriskForTextField:self.passwordText];
//    [self createAsteriskForTextField:self.confirmPasswordText];
    
    
    if ([User isValid] && [self isKindOfClass:[MyProfileViewController class]]) {
        UserObject *profileObject= (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
        NSString *salutation = profileObject.salutation;
        if (salutation.length > 1) {
            self.salutationDropDown.titleColor = [UIColor blackColor];
            self.salutationDropDown.title = salutation;
        }
        self.firstNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:profileObject.firstName];
        self.lastNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:profileObject.lastName];
        self.emailText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:profileObject.email];
        self.phoneNumberText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:profileObject.mobileNumber];
        
        isUseLocation = [[SessionData shareSessiondata] isUseLocation];
        [self setImageLocation:isUseLocation];
        
        currentFirstName = profileObject.firstName;
        currentEmail = profileObject.email;
        NSString *tempPhone = profileObject.mobileNumber;
        currentPhone = (tempPhone.length < 15) && ![tempPhone containsString:@"+"] ? [NSString stringWithFormat:@"+%@",tempPhone] : tempPhone;
        currentLastName = profileObject.lastName;
        
        self.firstNameText.text = [currentFirstName createMaskForText:NO];
        self.emailText.text = [currentEmail createMaskForText:YES];
        self.lastNameText.text = [currentLastName createMaskForText:NO];
        
        self.phoneNumberText.text = [currentPhone createMaskForText:NO];
        self.emailText.enabled = NO;
        
        [self.answerTextField setText:@"*******"];
        
    }else{
        currentFirstName = @"";
        currentEmail = @"";
        currentPhone = @"";
        currentLastName = @"";
        self.firstNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.firstNameText.text];
        self.lastNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.lastNameText.text];
        self.emailText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.emailText.text];
        self.phoneNumberText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.phoneNumberText.text];
        self.numberCardText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.numberCardText.text];
        self.passwordText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.passwordText.text];
        self.confirmPasswordText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.confirmPasswordText.text];
        
        self.firstNameText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.firstNameText.placeholder];
        self.lastNameText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.lastNameText.placeholder];
        self.emailText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.emailText.placeholder];
        self.phoneNumberText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.phoneNumberText.placeholder];
        self.numberCardText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.numberCardText.placeholder];
        self.passwordText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.passwordText.placeholder];
        self.confirmPasswordText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.confirmPasswordText.placeholder];
    }
    
    [self.firstNameText showTextMask:false];
    [self.lastNameText showTextMask:false];
    [self.emailText showTextMask:true];
    [self.phoneNumberText showTextMask:false];
    
    [self.firstNameText showAsterisk:5];
    [self.lastNameText showAsterisk:5];
    [self.emailText showAsterisk:5];
    [self.phoneNumberText showAsterisk:5];
    
    [self.myView setBackgroundColorForView];
    [self.scrollView setBackgroundColorForView];
    [self.signInButton setBackgroundColorForNormalStatus];
    [self.signInButton setBackgroundColorForTouchingStatus];
    [self.signInButton configureDecorationForButton];
    
    [self.submitButton setBackgroundColorForNormalStatus];
    self.submitButton.enabled = NO;
    [self.submitButton setBackgroundColorForTouchingStatus];
    [self.submitButton configureDecorationForButton];
    
    [self.cancelButton setBackgroundColorForNormalStatus];
    [self.cancelButton setBackgroundColorForTouchingStatus];
    [self.cancelButton configureDecorationForButton];
    
    [self.submitButton setBackgroundColorForNormalStatus];
    self.submitButton.enabled = NO;
    [self.submitButton setBackgroundColorForTouchingStatus];
    [self.submitButton configureDecorationForButton];
    
    
    self.answerTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.answerTextField.text];
    self.answerTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.answerTextField.placeholder];
    self.answerTextField.font =  self.firstNameText.font;
    self.answerTextField.delegate = self;
    [self.answerTextField setMarginLeftRight:5];
    
//    [self.phoneNumberText setMarginLeftRight:5];
//    [self.emailText setMarginLeftRight:5];
//    [self.firstNameText setMarginLeftRight:5];
//    [self.lastNameText setMarginLeftRight:5];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.checkBoxImageTopConstraint.constant = (IPAD) ? 12.0f : 2.0f;
    });
    
}

- (void) setEmail:(NSString *)email {
    if (email != nil) {
        _emailText.text = email;
        currentEmail = email;
        [_emailText setOriginText:email];
        [_emailText showTextMask:true];
    }
}

- (void) setPhone:(NSString *)phone {
    if (phone != nil) {
        NSMutableString* newString = [NSMutableString stringWithString:@""];
        if(![phone isValidPhoneNumber]) {
            NSString* temp = phone;
            NSMutableArray *characters = [NSMutableArray array];
            for (int i=0; i<temp.length; i++) {
                [characters addObject:[temp substringWithRange:NSMakeRange(i, 1)]];
            }
            [newString insertString:@"1-" atIndex:0];
            
            NSInteger start = characters.count > 10 ? characters.count - 10 : 0;
            for (NSInteger i = start; i < characters.count; i++) {
                [newString appendString:characters[i]];
                if (i - start == 2 || i - start == 5)
                    [newString appendString:@"-"];
            }
        } else {
            newString = [NSMutableString stringWithString:phone];
        }
        //        [self updateTextFiel:_phoneNumberText shouldChangeCharactersInRange:NSMakeRange(0, 0) replacementString:newString];
        _phoneNumberText.text = newString;
        currentPhone = newString;
        [_phoneNumberText setOriginText:newString];
        [_phoneNumberText showTextMaskPhone];
    }
}

- (void) setFirstName:(NSString*) firstName {
    if (firstName != nil) {
        _firstNameText.text = firstName;
        currentFirstName = firstName;
        [_firstNameText setOriginText:firstName];
        [_firstNameText showTextMask:false];
    }
}

- (void) setLastName:(NSString*) lastName {
    if (lastName != nil) {
        currentLastName = lastName;
        _lastNameText.text = lastName;
        [_lastNameText setOriginText:lastName];
        [_lastNameText showTextMask:false];
    }
}

- (void)backgroundColorForDisableStatusButton
{
    UIGraphicsBeginImageContext(CGSizeMake(1.0f, 1.0f));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, colorFromHexString(@"#b32d00").CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 1));
    UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.submitButton setBackgroundImage:colorImage forState:UIControlStateNormal];
}

- (void)initView{
    
    // Text Style For Title
    self.titleLable.attributedText = [UtilStyle setTextStyleForTitleViewControllerWithMessage:@"REGISTER"];
    [self.titleLable setTextColor:[UIColor whiteColor]];
    
    // Text Style For Greet Message
    NSString *message = NSLocalizedString(@"commitment_profile_message", nil);
    self.commitmentLabel.attributedText = [UtilStyle setLargeSizeStyleForLabelWithMessage:message];
    //[self setCheckBoxState:isCheck];
}


- (void) getUserInfo{
    if ([User isValid]) {
        UserObject* user = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
        NSString *salutation = user.salutation;
        self.salutationDropDown.titleColor = (salutation.length > 1)?colorFromHexString(TEXTFIELD_TEXT_COLOR_STANDARD):colorFromHexString(TEXTFIELD_PLACEHOLDER_COLOR_STANDARD);
        self.salutationDropDown.title = (salutation.length > 1)?salutation:@"Please select a salutation.";
        currentFirstName = user.firstName;
        currentEmail = user.email ;
        NSString *tempPhone = user.mobileNumber;
        currentPhone = (tempPhone.length < 15) && ![tempPhone containsString:@"+"] ? [NSString stringWithFormat:@"+%@",tempPhone] : tempPhone;
        currentLastName = user.lastName;
        self.firstNameText.text = [currentFirstName createMaskForText:NO];
        self.emailText.text = [currentEmail createMaskForText:YES];
        self.lastNameText.text = [currentLastName createMaskForText:NO];
        self.phoneNumberText.text = [currentPhone createMaskForText:NO];
        
        //        isUseLocation = false;
        if ([User current].locations.count > 0)
            isUseLocation = user.isUseLocation;
        [self setImageLocation:isUseLocation];
    }
    /*
     NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
     if (profileDictionary) {
     
     NSString *salutation = [profileDictionary objectForKey:keySalutation];
     self.salutationDropDown.titleColor = (salutation.length > 1)?colorFromHexString(TEXTFIELD_TEXT_COLOR_STANDARD):colorFromHexString(TEXTFIELD_PLACEHOLDER_COLOR_STANDARD);
     self.salutationDropDown.title = (salutation.length > 1)?salutation:@"Please select a salutation.";
     
     currentFirstName = [profileDictionary objectForKey:keyFirstName];
     currentEmail = [profileDictionary objectForKey:keyEmail] ;
     NSString *tempPhone = [profileDictionary objectForKey:keyMobileNumber];
     currentPhone = (tempPhone.length >= 20)?tempPhone:[NSString stringWithFormat:@"+%@",tempPhone];
     currentLastName = [profileDictionary objectForKey:keyLastName];
     
     self.firstNameText.text = [currentFirstName createMaskForText:NO];
     self.emailText.text = [currentEmail createMaskForText:YES];
     self.lastNameText.text = [currentLastName createMaskForText:NO];
     //self.phoneNumberText.text = [NSString stringWithFormat:@"*-%@", [[currentPhone substringFromIndex:2] createMaskStringBeforeNumberCharacter:3]];
     //self.phoneNumberText.text = (currentPhone.length > 3) ? [currentPhone createMaskStringBeforeNumberCharacter:3]: currentPhone;
     self.phoneNumberText.text = [currentPhone createMaskForText:NO];
     isUseLocation = [[SessionData shareSessiondata] isUseLocation];
     [self setImageLocation:isUseLocation];
     }
     */
}

-(void) createAsteriskForTextField:(UITextField *)textField
{
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 8, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [textField setLeftView:leftView];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    
    asteriskImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"asterisk_icon"]];
    
    asteriskImage.frame = CGRectMake(0.0f, 0.0f, 20.0f, 20.0f);
    asteriskImage.contentMode = UIViewContentModeCenter;
    [textField setRightView:asteriskImage];
    [textField setRightViewMode:UITextFieldViewModeAlways];
    
}

- (void) changeUseLocation {
    
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

-(void)setTextViewsDefaultBottomBolder
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.firstNameText setBottomBolderDefaultColor];
        [self.lastNameText setBottomBolderDefaultColor];
        [self.emailText setBottomBolderDefaultColor];
        [self.phoneNumberText setBottomBolderDefaultColor];
        [self.passwordText setBottomBolderDefaultColor];
        [self.confirmPasswordText setBottomBolderDefaultColor];
        [self.salutationDropDown setBottomBolderDefaultColor];
        [self.numberCardText setBottomBolderDefaultColor];
        [self.answerTextField setBottomBolderDefaultColor];
    });
}

- (void) setCheckBoxState:(BOOL)check
{
    if (!check) {
        check = NO;
    }
    (check == YES) ? [self.checkBoxButton setImage:[UIImage imageNamed:@"checkbox_check"] forState:UIControlStateNormal] : [self.checkBoxButton setImage:[UIImage imageNamed:@"checkbox_uncheck"] forState:UIControlStateNormal];
}
- (IBAction)checkBox:(id)sender {
    isCheck = !isCheck;
    [self setCheckBoxState:isCheck];
}

- (IBAction)actionSwitchLocation:(id)sender {
    
    isUseLocation = !isUseLocation;
    [self setImageLocation:isUseLocation];
    if (![self isKindOfClass:[MyProfileViewController class]]) {
        [[SessionData shareSessiondata] setIsUseLocation:isUseLocation];
        if (isUseLocation) {
            [LocationComponent startRequestLocation];
        }
    }
    else{
        [[SessionData shareSessiondata] setIsUseLocation:isUseLocation];
        if (isUseLocation) {
            [LocationComponent startRequestLocation];
        }
    }
    [self changeUseLocation];
}

- (void)setImageLocation:(BOOL)isLocation {
    self.locationStatusImage.image = [UIImage imageNamed:(isLocation) ? @"location_on" : @"location_off"];
}

-(void)updateStatusLocation:(NSNotification *)notification {
    
    NSDictionary* dictNoti = notification.userInfo;
    BOOL isLocation = [dictNoti boolForKey:LOCATION_COMPONENT_ISAUTHORIZED_NOTIFICATION];
    if (!isLocation) {
        if ([dictNoti intForKey:@"ERROR_CODE"] == 2) {
            //            isUseLocation = NO;
            //            [self setImageLocation:isUseLocation];
            //            [[SessionData shareSessiondata] setIsUseLocation:isUseLocation];
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
                [self setRequestLocation:YES];
            });
        }else if ([dictNoti intForKey:@"ERROR_CODE"] == 1) {
            if([[NSUserDefaults standardUserDefaults] boolForKey:@"IsRequestLocation"]) {
                [self setRequestLocation:NO];
            }else{
                [self showErrorLocationService];
            }
        }
    }
    [self changeUseLocation];
}

- (void)setRequestLocation:(BOOL)isRequest{
    [[NSUserDefaults standardUserDefaults] setBool:isRequest forKey:@"IsRequestLocation"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)showErrorLocationService {
    
    isUseLocation = NO;
    [self setImageLocation:isUseLocation];
    [[SessionData shareSessiondata] setIsUseLocation:isUseLocation];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Location Service Disabled" message:@"To enable, please go to Settings and turn on Location Service for this app." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction*  action) {
        
    }];
    
    UIAlertAction *settingAction = [UIAlertAction actionWithTitle:@"Setting" style:UIAlertActionStyleDefault handler:^(UIAlertAction  *action) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }];
    
    [alertController addAction:okAction];
    [alertController addAction:settingAction];
    alertController.preferredAction = settingAction;
    
    [self presentViewController:alertController animated: YES completion: nil];
}

- (void)updateProfileButtonStatus{
    
}

-(void) enableSignInButton:(UIButton *)button
{
    [button setHighlighted:NO];
    self.checkBoxButton.selected = !(button.selected);
    [button setBackgroundColor:[UIColor clearColor]];
    [self.submitButton setEnabled:button.selected];
}

#pragma mark - ACTIONS
- (IBAction)CheckboxAction:(id)sender {
    if (isCheck) {
        self.checkBoxImage.image = [UIImage imageNamed:@"checkbox_uncheck"];
        isCheck = NO;
    }else{
        self.checkBoxImage.image = [UIImage imageNamed:@"checkbox_check"];
        isCheck = YES;
    }
    [self enableSubmitButton:isCheck];
}

- (void)enableSubmitButton:(BOOL)isEnable {
    self.submitButton.enabled = isEnable;
}

- (IBAction)SubmitAction:(id)sender {
    [self.view endEditing:YES];
    [self verifyAccountData:NO];
}

- (IBAction)CancelAction:(id)sender {
    if ([AppData isCreatedProfile]) {
        SWRevealViewController *revealViewController = self.revealViewController;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UDASignInViewController *signInViewController = [storyboard instantiateViewControllerWithIdentifier:@"UDASignInViewController"];
        [revealViewController pushFrontViewController:signInViewController animated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - API PROCESS

- (void)updateProfileWithAPI{
    //[self startActivityIndicator];
    NSMutableDictionary *dictDai = [[NSMutableDictionary alloc] init];
    [dictDai setValue:(self.salutationDropDown.title.length > 1) ? self.salutationDropDown.title : @" " forKey:@"Salutation"];
    [dictDai setValue:currentFirstName forKey:@"FirstName"];
    [dictDai setValue:currentLastName forKey:@"LastName"];
    [dictDai setValue:[currentPhone stringByReplacingOccurrencesOfString:@"+" withString:@""] forKey:@"MobileNumber"];
    [dictDai setValue:currentEmail forKey:@"Email"];
    [dictDai setValue:currentPassword forKey:@"Password"];
    //    [dictDai setValue:[[SessionData shareSessiondata] passcode] forKey:@"Other Preferences"];
    [dictDai setValue:isUseLocation? @"ON": @"OFF" forKey:APP_USER_PREFERENCE_Location];
    [dictDai setValue:@"USA" forKey:@"Country"];
    [dictDai setValue:APP_NAME forKey:@"referenceName"];
    if (partyId) {
        [dictDai setValue:partyId forKey:@"partyId"];
    }
    
    //    [dictDai setValue:@"na" forKey:@"Dining Preference"];
    //    [dictDai setValue:@"na" forKey:@"Hotel Preference"];
    //    [dictDai setValue:@"na" forKey:@"Car Type Preference"];
    [dictDai setValue:@"Unknown" forKey:@"City"];
    //    [dictDai setValue:@"Unknown" forKey:APP_USER_PREFERENCE_Selected_City];
    
    CreateSecureView* v = [[CreateSecureView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    AACForgotPasswordController* vc = [AACForgotPasswordController new];
    vc.subview = v;
    v.controller = vc;
    [v updateUserInfor:dictDai];
    [self.navigationController pushViewController:vc animated:true];
    
//    [ModelAspireApiManager createProfileWithUserInfo:dictDai completion:^(User *user, NSError *error) {
//        [_self stopActivityIndicator];
//        if (user) {
//            dispatch_sync(dispatch_get_main_queue(), ^{
//                [_self trackingEventByName:@"Sign up" withAction:ClickActionType withCategory:AuthenticationCategoryType];
//                [AppData setCreatedProfileSuccessful];
//
//                AlertViewController *alert = [[AlertViewController alloc] init];
//                alert.titleAlert = nil;
//                alert.msgAlert = @"Profile created successfully.";
//                alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    alert.seconBtn.hidden = YES;
//                    alert.midView.alpha = 0.0f;;
//                    alert.lblAlertMessage.textAlignment = NSTextAlignmentCenter;
//                    [alert.view layoutIfNeeded];
//                });
//
//                alert.blockFirstBtnAction = ^(void){
//                    [_self updateSuccessRedirect];
//                };
//
//                [_self showAlert:alert forNavigation:NO];
//            });
//        }else{
//            dispatch_sync(dispatch_get_main_queue(), ^{
//                switch ([AspireApiError getErrorTypeAPISignupFromError:error]) {
//                    case UNKNOWN_ERR:
//                        [_self showApiErrorWithMessage:@""];
//                        break;
//                    case SIGNUP_EXIST_ERR:
//                        [self showAlertExistUser];
//                        break;
//                    case NETWORK_UNAVAILABLE_ERR:
//                        [self showErrorNetwork];
//                        break;
//                    default:
//                        [_self showApiErrorWithMessage:@""];
//                        break;
//                }
//
//            });
//        }
//    }];
    
}

-(void)showAlertExistUser {
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);;
    alert.msgAlert = @"An account with this email address already exists.";
    alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        alert.seconBtn.hidden = YES;
        alert.midView.alpha = 0.0f;;
    });
    
    [self showAlert:alert forNavigation:NO];
}




-(void)loadDataDoneFrom:(WSBase*)ws {
    
    if (ws.task == WS_CREATE_USER) {
        if (ws.data) {
            [self setUserDefaultInfoWithDict:ws.data[0]];
            [self trackingEventByName:@"Sign up" withAction:ClickActionType withCategory:AuthenticationCategoryType];
        }
        //        [self performSelector:@selector(setUserObjectWithDict:) withObject:((WSCreateUser*)ws).user afterDelay:1];
        
        //        if (((WSCreateUser*)ws).user) {
        //            [self setUserObjectWithDict:((WSCreateUser*)ws).user];
        //        }
        [AppData setCreatedProfileSuccessful];
        
        [self addPreferences];
        
    } else if (ws.task == WS_GET_MY_PREFERENCE){
        [self stopActivityIndicator];
        if (![self isKindOfClass:[MyProfileViewController class]]) {
            AlertViewController *alert = [[AlertViewController alloc] init];
            alert.titleAlert = nil;
            alert.msgAlert = @"Profile created successfully.";
            alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
            dispatch_async(dispatch_get_main_queue(), ^{
                alert.seconBtn.hidden = YES;
                alert.midView.alpha = 0.0f;;
                alert.lblAlertMessage.textAlignment = NSTextAlignmentCenter;
                [alert.view layoutIfNeeded];
            });
            
            alert.blockFirstBtnAction = ^(void){
                [self updateSuccessRedirect];
            };
            
            [self showAlert:alert forNavigation:NO];
        }else{
            [self updateSuccessRedirect];
        }
        
    }else if(ws.task == WS_ADD_MY_PREFERENCE || ws.task == WS_UPDATE_MY_PREFERENCE){
        
        [self getPreference];
        //        if (![self isKindOfClass:[MyProfileViewController class]]) {
        //            [self getPreference];
        //        }else{
        //           [self stopActivityIndicator];
        //            getPreferencesFromAPI(NO, self);
        //            [self updateSuccessRedirect];
        //        }
    }
    
}

- (void)addPreferences{
    
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    NSMutableArray *arraySubDictValues = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *hotelDict = [[NSMutableDictionary alloc] init];
    [hotelDict setValue:@"Hotel" forKey:@"Type"];
    
    PreferenceObject *hotelPref = [self getPreferenceByType:HotelPreferenceType];
    NSString *value = ([[SessionData shareSessiondata] isUseLocation])?@"YES":@"NO";
    
    NSString *userName = [[NSString alloc] initWithFormat:@"%@ %@", currentFirstName, currentLastName];
    
    if ([SessionData shareSessiondata].arrayPreferences.count > 0 && hotelPref.preferenceID.length > 0){
        [hotelDict setValue:hotelPref.preferenceID forKey:@"MyPreferencesId"];
        [hotelDict setValue:hotelPref.value forKey:@"Preferredstarrating"];
        [hotelDict setValue:value forKey:@"SmokingPreference"];
        [arraySubDictValues addObject:hotelDict];
        [dataDict setValue:arraySubDictValues forKey:@"PreferenceDetails"];
        
        wsPreferences = [[WSPreferences alloc] init];
        wsPreferences.delegate = self;
        [wsPreferences updatePreference:dataDict];
        
    }else{
        [hotelDict setValue:@"" forKey:@"Preferredstarrating"];
        [hotelDict setValue:value forKey:@"SmokingPreference"];
        //        [hotelDict setValue:[[SessionData shareSessiondata] passcode] forKey:@"RoomPreference"];
        [hotelDict setValue:userName forKey:@"BedPreference"];
        
        [arraySubDictValues addObject:hotelDict];
        
        //Begin: support for android.
        NSMutableDictionary *diningDict = [[NSMutableDictionary alloc] init];
        [diningDict setValue:@"Dining" forKey:@"Type"];
        [diningDict setValue:@"" forKey:@"CuisinePreferences"];
        [arraySubDictValues addObject:diningDict];
        
        NSMutableDictionary *transportationDict = [[NSMutableDictionary alloc] init];
        [transportationDict setValue:@"Car Rental" forKey:@"Type"];
        [transportationDict setValue:@"" forKey:@"PreferredRentalVehicle"];
        [arraySubDictValues addObject:transportationDict];
        //End: support for android.
        
        [dataDict setValue:arraySubDictValues forKey:@"PreferenceDetails"];
        wsPreferences = [[WSPreferences alloc] init];
        wsPreferences.delegate = self;
        [wsPreferences addPreference:dataDict];
    }
    
}

- (void)getPreference{
    
    [self startActivityIndicator];
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    [dataDict setValue:B2C_ConsumerKey forKey:@"ConsumerKey"];
    [dataDict setValue:@"GetPreference" forKey:@"Functionality"];
    [dataDict setValue:[[SessionData shareSessiondata] OnlineMemberID] forKey:@"OnlineMemberId"];
    
    wsPreferences = [[WSPreferences alloc] init];
    wsPreferences.delegate = self;
    [wsPreferences getPreference];
    
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode {
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }
}

-(void)loadDataFailFrom:(WSBase *)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message
{
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }else{
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);
        if ([message isEqualToString:@"ENR68-2"]) {
            alert.msgAlert =  @"An account with this email address already exists.";
        }else if ([message isEqualToString:@"ENR61-1"]) {
            alert.msgAlert =  @"Please confirm that the value entered is correct.";
        }else {
            alert.msgAlert = message;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.lblAlertMessage.textAlignment = NSTextAlignmentLeft;
            [alert.view layoutIfNeeded];
        });
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
        });
        
        [self showAlert:alert forNavigation:NO];
    }
}

- (void)setUserObjectWithDict:(NSDictionary*)dict{
    [[SessionData shareSessiondata] setUserObjectWithDict:dict];
}

- (void)setUserDefaultInfoWithDict:(UserRegistrationItem*)item{
    [[SessionData shareSessiondata] setOnlineMemberID:item.OnlineMemberID];
    [[SessionData shareSessiondata] setOnlineMemberDetailIDs:item.OnlineMemberDetailIDs];
}

#pragma mark KEYBOARD PROCESS
-(void)handleKeyboardWillShow:(NSNotification *)paramNotification
{
    [self.salutationDropDown didTapBackground];
    
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         keyboardHeight = keyboardRect.size.height;
                         [self updateEdgeInsetForShowKeyboard];
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

-(void)handleKeyboardWillHide:(NSNotification *)paramNotification
{
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    //    isAnimatingHideKeyboard = NO;
    //    if(isShouldHideKeyboardWhenSwipe)
    //        [UIView setAnimationsEnabled:NO];
    
    [self updateEdgeInsetForHideKeyboard];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:0
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL isFinished){
                         //                         isAnimatingHideKeyboard = !isFinished;
                     }];
    
    
}


-(void)updateEdgeInsetForShowKeyboard
{
    
    //    float heightInset = 0.0;
    //    heightInset = ([self isKindOfClass:[MyProfileViewController class]])?80.0f:120.0f; //80.0f:60.0f
    //
    //    self.scrollView.contentInset = UIEdgeInsetsMake(- heightInset,  0.0f, 0.0f, 0.0f);
    //    self.scrollBottom.constant = 150.0f + heightInset;
    //    if (!IPAD) {
    //        self.viewActionBottomConstraint.constant = (keyboardHeight + MARGIN_KEYBOARD);
    //    }
    //    [self.view layoutIfNeeded];
    _btnSC.constant =  keyboardHeight + 80;
    
}




-(void)updateEdgeInsetForHideKeyboard
{
    _btnSC.constant = 80;
    //    self.scrollView.contentInset = UIEdgeInsetsMake(0.0f,  0.0f, 0.0f, 0.0f);
    [self resignFirstResponderForAllTextField];
    //    self.scrollBottom.constant = 0.0f;
    //    self.viewActionBottomConstraint.constant = backupBottomConstraint;
    //    [self.view layoutIfNeeded];
}

//- (void)hidenKeyboardWhenSwipe{
//    //    [self.view.layer removeAllAnimations];
//    [self updateEdgeInsetForHideKeyboard];
//    [UIView setAnimationsEnabled:YES];
//}


-(void)resignFirstResponderForAllTextField
{
    if(self.firstNameText.isFirstResponder)
    {
        [self.firstNameText resignFirstResponder];
    }
    else if(self.lastNameText.isFirstResponder)
    {
        [self.lastNameText resignFirstResponder];
    }
    else if(self.emailText.isFirstResponder)
    {
        [self.emailText resignFirstResponder];
    }
    else if(self.phoneNumberText.isFirstResponder)
    {
        [self.phoneNumberText resignFirstResponder];
    }
    else if(self.passwordText.isFirstResponder)
    {
        [self.passwordText resignFirstResponder];
    }
    else if(self.confirmPasswordText.isFirstResponder)
    {
        [self.confirmPasswordText resignFirstResponder];
    }
    else if (self.numberCardText.isFirstResponder)
    {
        [self.numberCardText resignFirstResponder];
    }else if(self.answerTextField.isFirstResponder)
    {
        [self.answerTextField resignFirstResponder];
    }
}

-(void)dismissKeyboard
{
    [self updateEdgeInsetForHideKeyboard];
    [self.salutationDropDown didTapBackground];
    
}

-(BOOL)verifyValueForTextField:(UITextField *)textFied andMessageError:(NSMutableString *)message
{
    NSString *currentText = nil;
    NSString *errorMsg = nil;
    BOOL isValid = NO;
    if(textFied == self.firstNameText && textFied.text.length > 0)
    {
        currentText = currentFirstName;
        
        NSMutableString *tempError = [[NSMutableString alloc] init];
        if (![(textFied.isFirstResponder ? [textFied.text removeRedudantWhiteSpaceInText] :currentText) isValidName]) {
            isValid = NO;
            if (tempError.length > 0) {
                [tempError appendString:@"\n* "];
            }
            [tempError appendString:NSLocalizedString(@"input_invalid_first_name_msg", nil)];
        }else if (![(textFied.isFirstResponder ? textFied.text :currentText) isValidNameWithSpecialCharacter]) {
            isValid = NO;
            if (tempError.length > 0) {
                [tempError appendString:@"\n* "];
            }
            [tempError appendString:@"Please enter a valid first name. Acceptable special characters are -, ' and space."]; //Please enter a valid first name.
        }else{
            isValid = YES;
        }
        
        errorMsg = tempError;
        //errorMsg = NSLocalizedString(@"input_invalid_first_name_msg", nil);
        //isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidName];
    }
    else if(textFied == self.lastNameText && textFied.text.length > 0)
    {
        currentText = currentLastName;
        
        NSMutableString *tempError = [[NSMutableString alloc] init];
        if (![(textFied.isFirstResponder ? [textFied.text removeRedudantWhiteSpaceInText] :currentText) isValidName]) {
            isValid = NO;
            if (tempError.length > 0) {
                [tempError appendString:@"\n* "];
            }
            [tempError appendString:NSLocalizedString(@"input_invalid_last_name_msg", nil)];
        }else if (![(textFied.isFirstResponder ? textFied.text :currentText) isValidNameWithSpecialCharacter]) {
            isValid = NO;
            if (tempError.length > 0) {
                [tempError appendString:@"\n* "];
            }
            [tempError appendString:@"Please enter a valid last name. Acceptable special characters are -, ' and space."]; //Please enter a valid last name.
        }else{
            isValid = YES;
        }
        
        errorMsg = tempError;
        //errorMsg = NSLocalizedString(@"input_invalid_last_name_msg", nil);
        //isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidName];
    }
    else if(textFied == self.emailText && textFied.text.length > 0)
    {
        currentText = currentEmail;
        errorMsg = NSLocalizedString(@"input_invalid_email_msg", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidEmail];
    }
    else if(textFied == self.phoneNumberText && textFied.text.length > 0)
    {
        currentText = (textFied.isFirstResponder) ? textFied.text : currentPhone;
        errorMsg = NSLocalizedString(@"input_invalid_phone_number_msg", nil);
        
        if (![currentText isValidPhoneNumber]) {
            isValid = NO;
            errorMsg = NSLocalizedString(@"input_invalid_phone_number_msg", nil);
        }else{
            if ([currentText isValidCountryCode]) {
                currentText = [currentText stringByReplacingOccurrencesOfString:@"+" withString:@""];
                NSString *countryCode = [currentText getCoutryCode];
                if (currentText.length <= countryCode.length) {
                    isValid = NO;
                    errorMsg = NSLocalizedString(@"input_invalid_phone_number_msg", nil);
                }else {
                    isValid = YES;
                }
            }else {
                isValid = NO;
                errorMsg = NSLocalizedString(@"input_invalid_country_code_msg", nil);
            }
        }
        
        //        currentText = currentPhone;
        //        errorMsg = NSLocalizedString(@"input_invalid_phone_number_msg", nil);
        //        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidPhoneNumber];
    }
    else if (textFied == self.numberCardText)
    {
        currentText = currentCardNumber;
        errorMsg = @"Enter a valid program passcode.";
        isValid = [(textFied.isFirstResponder ? textFied.text : currentText) isValidBinNumber];
    }
    else if(textFied == self.passwordText && textFied.text.length > 0)
    {
        currentText = currentPassword;
        errorMsg = NSLocalizedString(@"input_invalid_password_msg", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidStrongPassword];//isValidStrongPassword
    }
    else if(textFied == self.confirmPasswordText)
    {
        currentText = currentConfirmPassword;
        errorMsg = NSLocalizedString(@"input_invalid_confirm_password_msg", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isEqualToString:currentPassword];
    }else if(textFied == self.answerTextField)
    {
        errorMsg = NSLocalizedString(@"input_invalid_answer_msg", nil);
        if (self.answerTextField.text.length == 0) {
            isValid = NO;
        }else
        {
            if (self.answerTextField.text.length > 0 && currentAnswer.length == 0)
            {
                isValid = YES;
            }
            else
            {
                NSString *answer = [currentAnswer stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
                isValid = answer.length > 3;
            }
        }
    }
    
    if((textFied.isFirstResponder ? textFied.text :currentText).length > 0)
    {
        if(!isValid)
        {
            
            [textFied setBottomBorderRedColor];
            
            if (errorMsg != nil) {
                (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
                [message appendString:errorMsg];
            }
        }
        else
        {
            [textFied setBottomBolderDefaultColor];
        }
    }
    else
    {
        if (!isValid) {
            [textFied setBottomBorderRedColor];
            if (errorMsg != nil) {
                (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
                [message appendString:errorMsg];
            }
            
        }
       
    }
    
    return isValid;
}
- (BOOL)verifyValueForDropdown:(DropDownView*)dropdown andMessageError:(NSMutableString *)message{
    
    BOOL isValid = NO;
    
    if (dropdown == self.salutationDropDown) {
        if (self.salutationDropDown.valueStatus == DefaultValueStatus) {
            (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
            [message appendString:@"Select a salutation."]; //All fields are required.
            isValid = NO;
            [self.salutationDropDown setBottomBorderRedColor];
        }else {
            isValid = YES;
            [self.salutationDropDown setBottomBolderDefaultColor];
        }
    }
    return isValid;
}
#pragma mark LOCAL PROCESS
-(void)verifyAccountData:(BOOL)isUpdate
{
    NSMutableString *message = [[NSMutableString alloc] init];
    if (isUpdate) {
        if(self.firstNameText.text.length == 0 && self.lastNameText.text.length == 0 && self.phoneNumberText.text.length == 0)
        {
            [message appendString:@"* "];
            [message appendString:@"All fields are required."];
            
            [self.firstNameText setBottomBorderRedColor];
            [self.lastNameText setBottomBorderRedColor];
            [self.phoneNumberText setBottomBorderRedColor];
            
            //[self verifyValueForDropdown:self.salutationDropDown andMessageError:message];
            
            [self createProfileInforWithMessage:message isCreatedSuccessful:NO];
        }else if(self.firstNameText.text.length == 0 || self.lastNameText.text.length == 0 || self.emailText.text.length == 0 || self.phoneNumberText.text.length == 0)
        {
            [message appendString:@"* "];
            [message appendString:@"All fields are required."];
            if (self.salutationDropDown.title.length > 10) {
                [self verifyValueForDropdown:self.salutationDropDown andMessageError:message];
            }
            [self verifyValueForTextField:self.firstNameText andMessageError:message];
            [self verifyValueForTextField:self.lastNameText andMessageError:message];
            [self verifyValueForTextField:self.emailText andMessageError:message];
            [self verifyValueForTextField:self.phoneNumberText andMessageError:message];
            [self verifyValueForTextField:self.answerTextField andMessageError:message];
//
            [self createProfileInforWithMessage:message isCreatedSuccessful:NO];
        }
        else{
            if (self.salutationDropDown.title.length > 10) {
                [self verifyValueForDropdown:self.salutationDropDown andMessageError:message];
            }
            [self verifyValueForTextField:self.firstNameText andMessageError:message];
            [self verifyValueForTextField:self.lastNameText andMessageError:message];
            [self verifyValueForTextField:self.emailText andMessageError:message];
            [self verifyValueForTextField:self.phoneNumberText andMessageError:message];
            [self verifyValueForTextField:self.answerTextField andMessageError:message];
            [self createProfileInforWithMessage:message isCreatedSuccessful:(message.length == 0)];
        }
    }else{
        if(self.firstNameText.text.length == 0 && self.lastNameText.text.length == 0 && self.phoneNumberText.text.length == 0)
        {
            [message appendString:@"* "];
            [message appendString:@"All fields are required."];
            
            [self.firstNameText setBottomBorderRedColor];
            [self.lastNameText setBottomBorderRedColor];
            [self.phoneNumberText setBottomBorderRedColor];
    
            [self verifyValueForDropdown:self.salutationDropDown andMessageError:message];
            
            [self createProfileInforWithMessage:message isCreatedSuccessful:NO];
        }
        else if(self.firstNameText.text.length == 0 || self.lastNameText.text.length == 0 || self.emailText.text.length == 0 || self.phoneNumberText.text.length == 0)
        {
            [message appendString:@"* "];
            [message appendString:@"All fields are required."];
            
            [self verifyValueForDropdown:self.salutationDropDown andMessageError:message];

            [self verifyValueForTextField:self.firstNameText andMessageError:message];
            [self verifyValueForTextField:self.lastNameText andMessageError:message];
            [self verifyValueForTextField:self.emailText andMessageError:message];
            [self verifyValueForTextField:self.phoneNumberText andMessageError:message];
            //[self verifyValueForTextField:self.numberCardText andMessageError:message];
            //[self verifyValueForTextField:self.passwordText andMessageError:message];
            //[self verifyValueForTextField:self.confirmPasswordText andMessageError:message];
//
            [self createProfileInforWithMessage:message isCreatedSuccessful:NO];
        }
        else{
            
            [self verifyValueForDropdown:self.salutationDropDown andMessageError:message];
            
            [self verifyValueForTextField:self.firstNameText andMessageError:message];
            [self verifyValueForTextField:self.lastNameText andMessageError:message];
            [self verifyValueForTextField:self.emailText andMessageError:message];
            [self verifyValueForTextField:self.phoneNumberText andMessageError:message];
            //[self verifyValueForTextField:self.numberCardText andMessageError:message];
            //[self verifyValueForTextField:self.passwordText andMessageError:message];
            //[self verifyValueForTextField:self.confirmPasswordText andMessageError:message];
            
            [self createProfileInforWithMessage:message isCreatedSuccessful:(message.length == 0)];
        }
    }
}

-(void)createProfileInforWithMessage:(NSString *)message isCreatedSuccessful:(BOOL)isSuccessful
{
    if(isSuccessful)
    {
        [self updateEdgeInsetForHideKeyboard];
        [self setTextViewsDefaultBottomBolder];
        
        //save profile is create
        [self updateProfileWithAPI];
        
    }
    else
    {
        [self.view endEditing:YES];
        NSMutableString *newMessage = [[NSMutableString alloc] init];
        [newMessage appendString:@"\n"];
        [newMessage appendString:message];
        [newMessage appendString:@"\n"];
        
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = @"Please confirm that the value entered is correct:";
        alert.msgAlert = newMessage;
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
            alert.lblAlertMessage.textAlignment = NSTextAlignmentLeft;
            [alert.view layoutIfNeeded];
        });
        
        alert.blockFirstBtnAction = ^(void){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self makeBecomeFirstResponderForTextField];
            });
        };
        
        [self showAlert:alert forNavigation:NO];
    }
}

- (void) updateSuccessRedirect{
    
    appdelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    [UIView transitionWithView:appdelegate.window
                      duration:0.5
                       options:UIViewAnimationOptionPreferredFramesPerSecond60
                    animations:^{
                        
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        
                        MenuViewController *menuViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
                        UINavigationController *menuNavigationViewController = [[UINavigationController alloc] initWithRootViewController:menuViewController];
                        UIViewController *fontViewController = [[HomeViewController alloc] init];
                        
                        SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:menuNavigationViewController frontViewController:fontViewController];
                        
                        revealController.delegate = appdelegate;
                        revealController.rearViewRevealWidth = SCREEN_WIDTH;
                        revealController.rearViewRevealOverdraw = 0.0f;
                        revealController.rearViewRevealDisplacement = 0.0f;
                        appdelegate.viewController = revealController;
                        appdelegate.window.rootViewController = appdelegate.viewController;
                        [appdelegate.window makeKeyWindow];
                        
                        UIViewController *newFrontController = [[HomeViewController alloc] init];
                        UINavigationController *newNavigationViewController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                        [revealController pushFrontViewController:newNavigationViewController animated:YES];
                        
                    }
                    completion:nil];
}
-(void) makeBecomeFirstResponderForTextField
{
    [self resignFirstResponderForAllTextField];
    if(![currentFirstName isValidName] || ![currentFirstName isValidNameWithSpecialCharacter])
    {
        [self.firstNameText becomeFirstResponder];
    }
    else if(![currentLastName isValidName] || ![currentLastName isValidNameWithSpecialCharacter])
    {
        [self.lastNameText becomeFirstResponder];
    }
    else if(![currentPhone isValidPhoneNumber] || ![currentPhone isValidCountryCode])
    {
        [self.phoneNumberText becomeFirstResponder];
    }
    
    else if(!(self.answerTextField.text.length > 3)){
        [self.answerTextField becomeFirstResponder];
    }
    
    else if(![self.passwordText.text isValidPassword])
    {
        [self.passwordText becomeFirstResponder];
    }
    else if(![self.confirmPasswordText.text isEqualToString:currentPassword])
    {
        [self.confirmPasswordText becomeFirstResponder];
    }
    
    
    else if([currentPhone isValidCountryCode])
    {
        currentPhone = [currentPhone stringByReplacingOccurrencesOfString:@"+" withString:@""];
        NSString *countryCode = [currentPhone getCoutryCode];
        if (currentPhone.length <= countryCode.length) {
            [self.phoneNumberText becomeFirstResponder];
        }
        
        else if(self.answerTextField.text.length == 0){
            [self.answerTextField becomeFirstResponder];
        }
        else if(self.answerTextField.text.length == currentAnswer.length){
            NSString *answer = [currentAnswer stringByTrimmingCharactersInSet:
                                [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if(answer.length < 4){
                [self.answerTextField becomeFirstResponder];
            }
        }
        
    }
    
    else if(self.answerTextField.text.length == 0){
        [self.answerTextField becomeFirstResponder];
    }
    else if(self.answerTextField.text.length == currentAnswer.length){
        if([currentAnswer stringByTrimmingCharactersInSet:
            [NSCharacterSet whitespaceAndNewlineCharacterSet]].length < 4){
            [self.answerTextField becomeFirstResponder];
        }
    }
}

- (void) handleAsterickIcon{
    
}
#pragma mark 1
//-(void)textFieldChanged:(id)sender {

//}

-(void)textDidChange:(NSNotification *)paramNotification
{
        if (![self.phoneNumberText.text containsString:@"+"] && self.phoneNumberText.text.length > 0) {
            self.phoneNumberText.text = [NSString stringWithFormat:@"+%@",self.phoneNumberText.text];
        }
    
        if ([self.phoneNumberText.text containsString:@"+"] && self.phoneNumberText.text.length == 1) {
            self.phoneNumberText.text = @"";
        }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField == self.emailText) {
        [self dismissKeyboard];
        self.emailText.text = currentEmail;
        return NO;
    }
    else{
        [self.emailText showTextMask:true];
    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.text = [textField.text removeRedudantWhiteSpaceInText];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == self.firstNameText)
    {
        currentFirstName = textField.text;
        textField.text = [textField.text createMaskForText:NO];
        
    }
    else if(textField == self.lastNameText)
    {
        currentLastName = textField.text;
        textField.text =  [textField.text createMaskForText:NO];
        
    }
    else if(textField == self.phoneNumberText)
    {
        //        if(textField.text.length == 2 && [[textField.text substringToIndex:2] isEqualToString:@"1-"]) {
        //            textField.text = nil;
        //        }
    
        currentPhone = textField.text;
        
        textField.text =  [textField.text createMaskForText:NO];
        
    }
    else if(textField == self.emailText)
    {
        currentEmail = textField.text;
        if([textField.text containsString:@"@"])
        {
            textField.text = [textField.text createMaskForText:YES];
        }
    }
    else if (textField == self.numberCardText)
    {
        currentCardNumber = textField.text;
        //textField.text = [textField.text createMaskForText:NO];
    }
    else if(textField == self.passwordText)
    {
        currentPassword = textField.text;
    }
    else if(textField == self.confirmPasswordText)
    {
        currentConfirmPassword = textField.text;
    }
    else if(textField == self.answerTextField){
        currentAnswer=textField.text;
        
        if (currentAnswer.length > 0) {
            NSMutableString* temp = [NSMutableString stringWithString:@""];
            for (int i=0; i < currentAnswer.length; i++) {
                [temp appendString:@"*"];
            }
            self.answerTextField.text = temp;
        }else{
            self.answerTextField.text = @"";
            [self handleMaskAnswer];
        }
    }
}


- (void)handleMaskAnswer
{
    
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == self.firstNameText)
    {
        textField.text = currentFirstName;
    }
    
    else if(textField == self.lastNameText)
    {
        textField.text = currentLastName;
    }
    
    else if(textField == self.phoneNumberText)
    {
//        textField.text = currentPhone;
        if (currentPhone.length == 0) return;
        NSString* temp = currentPhone;
        
    
        if (![[currentPhone substringToIndex:1] isEqualToString:@"+"]) {
            temp = [NSString stringWithFormat:@"+%@",temp];
        }
        
        
        
        textField.text = temp;
        
        //        if(textField.text.length > 0)
        //        {
        //            if(![[textField.text substringWithRange:NSMakeRange(0, 2)] isEqualToString:@"1-"])
        //            {
        //                NSMutableString *insertCountryCode = [[NSMutableString alloc] initWithString:textField.text];
        //                [insertCountryCode insertString:@"1-" atIndex:0];
        //                textField.text = insertCountryCode;
        //            }
        //        }
        //        else
        //        {
        //            textField.text = @"1-";
        //        }
    }
    else if(textField == self.emailText)
    {
        textField.text = currentEmail;
    }
    else if(textField == self.numberCardText)
    {
        textField.text = currentCardNumber;
    }
    else if(textField == self.passwordText)
    {
        textField.text = currentPassword;
    }
    else if (textField == self.answerTextField)
    {
        textField.text = currentAnswer;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([self isKindOfClass:[MyProfileViewController class]]) {
        if (textField == self.emailText) {
            return NO;
        }
    }
    if (textField == self.passwordText || textField == self.confirmPasswordText) {
        if ([string isEqualToString:@" "]) {
            return NO;
        }
    }
    
    NSString *inputString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (textField == self.firstNameText || textField == self.lastNameText) {
        if (inputString.length <= 25) {
            [self checkTextFieldIsChange:textField shouldChangeCharactersInRange:range replacementString:string];
        }
    }else if (textField == self.phoneNumberText) {
        if (inputString.length <= 19) {
            [self checkTextFieldIsChange:textField shouldChangeCharactersInRange:range replacementString:string];
        }
    }else if(textField == self.answerTextField){
        if (inputString.length <= 100) {
           [self checkTextFieldIsChange:textField shouldChangeCharactersInRange:range replacementString:string];
        }
    }else{
        [self checkTextFieldIsChange:textField shouldChangeCharactersInRange:range replacementString:string];
    }
    
    return [self updateTextFiel:textField shouldChangeCharactersInRange:range replacementString:string];
    
}

-(void)checkTextFieldIsChange:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
}

-(BOOL)updateTextFiel:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(string.length > 1)
    {
        NSString *stringContent = (textField == self.emailText) ? string : [string removeRedudantWhiteSpaceInText];
        NSMutableString *newString = [[NSMutableString alloc] initWithString:stringContent];
        
        NSString *resultText = [textField.text stringByReplacingCharactersInRange:range
                                                                       withString:string];
        resultText = [resultText removeRedudantWhiteSpaceInText];
        
        if(textField == self.firstNameText || textField == self.lastNameText)
        {
            //            if([newString isValidName])
            //            {
            //                textField.text = newString;
            //            }
            //            else if(string.length > 19)
            //            {
            //                textField.text = [newString substringToIndex:19];
            //            }
            //            else
            //            {
            //                textField.text = newString;
            //            }
            textField.text = (resultText.length > 25) ? [resultText substringWithRange:NSMakeRange(0, 25)] : resultText;
        }
        if(textField == self.passwordText || textField == self.confirmPasswordText)
        {
            if([newString isValidStrongPassword]) //isValidStrongPassword
            {
                textField.text = newString;
            }
            else if(string.length > 25)
            {
                textField.text = [newString substringToIndex:25];
            }
            else
            {
                textField.text = newString;
            }
        }
        if(textField == self.phoneNumberText)
        {
            if([newString isValidPhoneNumber] && [newString isValidCountryCode])
            {
                textField.text = newString;
            }
            /*
             else{
             if(newString.length <= 10)
             {
             [newString insertString:@"1-" atIndex:0];
             }
             else{
             if(![[newString substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"-"])
             {
             [newString insertString:@"-" atIndex:1];
             }
             }
             
             if(newString.length > 5 && ![[newString substringWithRange:NSMakeRange(5, 1)] isEqualToString:@"-"])
             {
             [newString insertString:@"-" atIndex:5];
             }
             
             if(newString.length > 9 && ![[newString substringWithRange:NSMakeRange(9, 1)] isEqualToString:@"-"])
             {
             [newString insertString:@"-" atIndex:9];
             }
             
             textField.text = (newString.length > 14) ? [newString substringWithRange:NSMakeRange(0, 14)] : newString;
             }
             */
        }
        
        if(textField == self.emailText)
        {
            textField.text = (resultText.length > 100)?[resultText substringWithRange:NSMakeRange(0, 100)] : resultText;
        }
        
        return NO;
    }
    
    if(textField == self.firstNameText || textField == self.lastNameText)
    {
        if(textField.text.length == 25 && ![string isEqualToString:@""])
        {
            return NO;
        }
        return YES;
    }
    if(textField == self.passwordText || textField == self.confirmPasswordText)
    {
        if(textField.text.length == 25 && ![string isEqualToString:@""])
        {
            return NO;
        }
        return YES;
    }
    if (textField == self.numberCardText) {
        if (textField.text.length > 24 && ![string isEqualToString:@""]) {
            return NO;
        }
        return YES;
    }
    
    else if(textField == self.phoneNumberText)
    {
        
        textField.text =  [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"+%@",[textField.text stringByReplacingOccurrencesOfString:@"+" withString:@""]]];
        
        //            if (textField.text.length > 0 && ![inputString containsString:@"+"]) {
        //                textField.text = [NSString stringWithFormat:@"+%@",textField.text];
        //            }
        //
        //            if (textField.text.length == 1 && ![inputString containsString:@"+"]) {
        //                textField.text = @"";
        //            }
    
        
//        NSString * plus = @"+";
//        NSString * editChar = [textField.text substringWithRange:range];
//        if ([editChar isEqualToString:plus] && range.location == 0) {
//            return false;
//        }
       
        
        if(textField.text.length == 19 && ![string isEqualToString:@""])
        {
            return NO;
        }
        /*
         if(textField.text.length == 14 && ![string isEqualToString:@""])
         {
         return NO;
         }
         
         if(range.location < 2 && [string isEqualToString:@""])
         {
         return NO;
         }
         
         if(range.location < textField.text.length)
         {
         textField.text = [self reformatForText:textField WithNewString:string WithRange:range];
         NSRange newRange;
         if([string isEqualToString:@""])
         {
         newRange = (textField.text.length < range.location) ? NSMakeRange(textField.text.length, 0) : NSMakeRange(range.location, 0);
         }
         else
         {
         newRange = (range.location == 5 || range.location == 9) ? NSMakeRange(range.location + 2, 0) : NSMakeRange(range.location + 1, 0);
         
         }
         [textField updateCursorPositionAtRange:newRange];
         return NO;
         }
         
         if(textField.text.length == 5 && ![string isEqualToString:@""])
         {
         textField.text = [NSString stringWithFormat:@"%@-%@",textField.text,string];
         return NO;
         }
         
         if(textField.text.length == 7 && [string isEqualToString:@""])
         {
         textField.text = [textField.text substringToIndex:5];
         return NO;
         }
         
         if(textField.text.length == 9 && ![string isEqualToString:@""])
         {
         textField.text = [NSString stringWithFormat:@"%@-%@",textField.text,string];
         return NO;
         }
         
         if(textField.text.length == 11 && [string isEqualToString:@""])
         {
         textField.text = [textField.text substringToIndex:9];
         return NO;
         }
         */
        
        return YES;
    }
    else if(textField == self.emailText)
    {
        if(textField.text.length == 100 && ![string isEqualToString:@""])
        {
            return NO;
        }
        
        if([textField.text occurrenceCountOfCharacter:'@'] == 1 && [string isEqualToString:@"@"]){
            return NO;
        }
        
        return YES;
    }
    
    else if(textField == self.answerTextField){
        if (textField.text.length==100 && ![string isEqualToString:@""]) {
            return NO;
        }
        return YES;
    }
    
    return YES;
}
- (void)WSBaseNetworkUnavailable{
    [self stopActivityIndicator];
}

#pragma mark LOGICAL FUNCTION

-(NSString *)reformatForText:(UITextField *)textField WithNewString:(NSString *)newString WithRange:(NSRange)range
{
    NSMutableString *mutableString = [[NSMutableString alloc] initWithString:textField.text];
    NSRange newRange = range;
    if([newString isEqualToString:@""] && (range.location == 5 ||  range.location == 9))
    {
        newRange = NSMakeRange(range.location - 1, 1);
    }
    [mutableString replaceCharactersInRange:newRange withString:newString];
    mutableString = [[NSMutableString alloc] initWithString:[mutableString stringByReplacingOccurrencesOfString:@"-" withString:@""]];
    
    if(mutableString.length >= 1 && textField.tag == PHONE_NUMBER_TEXTFIELD_TAG)
    {
        [mutableString insertString:@"-" atIndex:1];
    }
    
    if(mutableString.length > 5)
    {
        [mutableString insertString:@"-" atIndex:5];
    }
    
    if(mutableString.length > 9 && textField.tag == PHONE_NUMBER_TEXTFIELD_TAG)
    {
        [mutableString insertString:@"-" atIndex:9];
    }
    
    return mutableString;
}
#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    
    CGPoint touchPoint = [touch locationInView:touch.view];
    
    UIView *viewTouch = touch.view.superview;
    if ([viewTouch isKindOfClass:[UITableViewCell class]]) {
        return NO;
    }else {
        if ([self isKindOfClass:[MyProfileViewController class]]) {
            for (UIView* view in touch.view.subviews) {
                if ((UITextField*)view == self.emailText && CGRectContainsPoint(self.emailText.frame, touchPoint)) {
                    if ([User isValid]) {
                        UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
                        self.emailText.text = userObject.email;
                        break;
                    }
                }else{
                    if([self.emailText.text containsString:@"@"])
                    {
                        self.emailText.text = [self.emailText.text createMaskForText:YES];
                    }
                }
            }
        }
        
    }
    
    return YES;
    
}

#pragma mark - DropDownViewDelegate
- (void)didSelectItem:(DropDownView *)dropDownView atIndex:(int)index{
    [self changeValueDropDown];
}
- (void)didShow:(DropDownView *)dropDownView {
    [self.view endEditing:YES];
}

- (void)didHiden:(DropDownView *)dropDownView {
    
}

- (void)changeValueDropDown{
    
}

#pragma mark - Links Tapped

- (void)buildAgreeTextViewFromString:(NSString *)localizedString
{
    // 1. Split the localized string on the # sign:
    NSArray *localizedStringPieces = [localizedString componentsSeparatedByString:@"#"];
    self.confirmMessageView.userInteractionEnabled = YES;
    // 2. Loop through all the pieces:
    NSUInteger msgChunkCount = localizedStringPieces ? localizedStringPieces.count : 0;
    CGPoint wordLocation = CGPointMake(0.0, 0.0);
    for (NSUInteger i = 0; i < msgChunkCount; i++)
    {
        NSString *chunk = [localizedStringPieces objectAtIndex:i];
        if ([chunk isEqualToString:@""])
        {
            continue;     // skip this loop if the chunk is empty
        }
        
        // 3. Determine what type of word this is:
        BOOL isTermsOfServiceLink = [chunk hasPrefix:@"<ts>"];
        BOOL isPrivacyPolicyLink  = [chunk hasPrefix:@"<pp>"];
        BOOL isLink = (BOOL)(isTermsOfServiceLink || isPrivacyPolicyLink);
        
        // 4. Create label, styling dependent on whether it's a link:
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont fontWithName:FONT_MarkForMC_MED size:18.0];
        
        label.text = chunk;
        label.userInteractionEnabled = isLink;
        
        if (isLink)
        {
            // 5. Set tap gesture for this clickable text:
            SEL selectorAction = isTermsOfServiceLink ? @selector(tapOnTermsOfServiceLink:) : @selector(tapOnPrivacyPolicyLink:);
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                         action:selectorAction];
            [label addGestureRecognizer:tapGesture];
            
            // Trim the markup characters from the label:
            if (isTermsOfServiceLink)
            {
                label.text = [label.text stringByReplacingOccurrencesOfString:@"<ts>" withString:@""];
                termOfUseLbl = label;
            }
            
            
            if (isPrivacyPolicyLink)
            {
                label.text = [label.text stringByReplacingOccurrencesOfString:@"<pp>" withString:@""];
                policyLbl = label;
            }
            
            
            NSArray * objects = [[NSArray alloc] initWithObjects:[UIColor whiteColor], [NSNumber numberWithInt:NSUnderlineStyleSingle], nil];
            NSArray * keys = [[NSArray alloc] initWithObjects:NSForegroundColorAttributeName, NSUnderlineStyleAttributeName, nil];
            
            NSDictionary * linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
            
            NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:label.text attributes:linkAttributes];
            
            [label setAttributedText:attributedString];
            
        }
        else
        {
            UITapGestureRecognizer *tappedOutsideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enableSubmitButtonByConfirmation)];
            
            [label addGestureRecognizer:tappedOutsideKeyboard];
            label.userInteractionEnabled = YES;
            [label setTextColor:[UIColor whiteColor]];
            label.tag = 1000;
        }
        
        // 6. Lay out the labels so it forms a complete sentence again:
        
        // If this word doesn't fit at end of this line, then move it to the next
        // line and make sure any leading spaces are stripped off so it aligns nicely:
        
        [label sizeToFit];
        
        if (self.confirmMessageView.frame.size.width < wordLocation.x + label.bounds.size.width)
        {
            wordLocation.x = 0.0;                       // move this word all the way to the left...
            wordLocation.y += label.frame.size.height;  // ...on the next line
            
            // And trim of any leading white space:
            NSRange startingWhiteSpaceRange = [label.text rangeOfString:@"^\\s*"
                                                                options:NSRegularExpressionSearch];
            if (startingWhiteSpaceRange.location == 0)
            {
                label.text = [label.text stringByReplacingCharactersInRange:startingWhiteSpaceRange
                                                                 withString:@""];
                [label sizeToFit];
            }
        }
        
        // Set the location for this label:
        label.frame = CGRectMake(wordLocation.x,
                                 wordLocation.y,
                                 label.frame.size.width,
                                 label.frame.size.height);
        
        // Show this label:
        [self.confirmMessageView addSubview:label];
        
        // Update the horizontal position for the next word:
        wordLocation.x += label.frame.size.width;
    }
    
    if (self.confirmMessageView.subviews.count > 0) {
        UILabel* label = (UILabel*)self.confirmMessageView.subviews.lastObject;
        for (NSLayoutConstraint* c in self.confirmMessageView.constraints) {
            if ([c.firstItem isEqual:self.confirmMessageView] && c.firstAttribute == NSLayoutAttributeHeight) {
                c.constant = CGRectGetMaxY(label.frame);
                NSMutableArray* temp = [NSMutableArray new];
                if (termOfUseLbl) [temp addObject:termOfUseLbl];
                if (policyLbl) [temp addObject:policyLbl];
                [temp enumerateObjectsUsingBlock:^(UITextField*  _Nonnull label, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSArray * objects = [[NSArray alloc] initWithObjects:[UIColor whiteColor], [NSNumber numberWithInt:NSUnderlineStyleSingle], nil];
                    NSArray * keys = [[NSArray alloc] initWithObjects:NSForegroundColorAttributeName, NSUnderlineStyleAttributeName, nil];
                    
                    NSDictionary * linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
                    
                    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:label.text attributes:linkAttributes];
                    
                    [label setAttributedText:attributedString];
                }];
            }
        }
    }
}

- (void)enableSubmitButtonByConfirmation {
    self.submitButton.enabled = !self.submitButton.enabled;
    if (self.submitButton.enabled) {
        self.checkBoxImage.image = [UIImage imageNamed:@"checkbox_check"];
    }else{
        self.checkBoxImage.image = [UIImage imageNamed:@"checkbox_uncheck"];
    }
}

- (void)tapOnTermsOfServiceLink:(UITapGestureRecognizer *)tapGesture
{
    if([self isDisableSubmitTapped:tapGesture])
    {
        return;
    }
    
    [self dismissKeyboard];
    
    if (tapGesture.state == UIGestureRecognizerStateEnded)
    {
        TermsOfUseViewController *vc = [[TermsOfUseViewController alloc] init];
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        [self.navigationController pushViewController:vc animated:NO];
    }
}

- (void)tapOnPrivacyPolicyLink:(UITapGestureRecognizer *)tapGesture
{
    if([self isDisableSubmitTapped:tapGesture])
    {
        return;
    }
    
    [self dismissKeyboard];
    
    if (tapGesture.state == UIGestureRecognizerStateEnded)
    {
        PolicyViewController *vc = [[PolicyViewController alloc] init];
        //vc.delegate = self;
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        [self.navigationController pushViewController:vc animated:NO];
    }
}

-(BOOL)isDisableSubmitTapped:(id)sender
{
    if([sender isKindOfClass:[UITapGestureRecognizer class]])
    {
        UITapGestureRecognizer *tapRecognization = (UITapGestureRecognizer *)sender;
        CGPoint location = [tapRecognization locationInView:self.view];
        CGRect fingerRect = CGRectMake(location.x-5, location.y-5, 10, 10);
        
        if(CGRectIntersectsRect(fingerRect, self.submitButton.frame) && !self.submitButton.enabled)
        {
            [self dismissKeyboard];
            return YES;
        }
    }
    
    return NO;
}

@end
