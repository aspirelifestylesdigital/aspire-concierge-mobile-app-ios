//
//  AskConciergeConfirmationViewController.h
//  MobileConcierge
//
//  Created by Mac OS on 5/29/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "BaseViewController.h"

@interface AskConciergeConfirmationViewController : BaseViewController
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintHeaderView;
@property (weak, nonatomic) IBOutlet UIView *viewOverlay;
@property (weak, nonatomic) IBOutlet UIView *viewHeader;
@property (weak, nonatomic) IBOutlet UIView *viewAnotherRequest;
@property (weak, nonatomic) IBOutlet UIView *viewExplore;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraintHeaderView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraintThanksView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraintExploreView;

@property (weak, nonatomic) IBOutlet UILabel *lblThanks;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;

@property (weak, nonatomic) IBOutlet UIButton *btnAnotherRequest;
@property (weak, nonatomic) IBOutlet UIButton *btnExplore;

@property BOOL isUpdatedRequest;

- (IBAction)touchAnotherRequest:(id)sender;
- (IBAction)touchExplore:(id)sender;
@end
