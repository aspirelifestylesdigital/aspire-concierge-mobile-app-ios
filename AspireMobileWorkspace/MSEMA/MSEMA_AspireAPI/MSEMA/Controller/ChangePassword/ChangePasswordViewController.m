//
//  ChangePasswordViewController.m
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 8/23/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "UIView+Extension.h"
#import "UITextField+Extensions.h"
#import "UIButton+Extension.h"
#import "WSChangePassword.h"
#import "NSString+Utis.h"

#import "AppDelegate.h"
#import "MenuViewController.h"
#import "HomeViewController.h"
#import "NSString+AESCrypt.h"
#import "UILabel+Extension.h"
@import AspireApiFramework;

#define BOTTOM_SPACE  30.0f

@interface ChangePasswordViewController ()<DataLoadDelegate, UITextFieldDelegate>{
    CGFloat keyboardHeight;
    AppDelegate* appdelegate;
    UIImageView *asteriskImage;
    IBOutlet UILabel *titleTemp;
    IBOutlet NSLayoutConstraint *topConstraintStackView;
    UITapGestureRecognizer *tappedOutsideKeyboard;
}

@property (nonatomic, strong) WSChangePassword *wsChangePassword;

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    isNotAskConciergeBarButton = YES;
    [super viewDidLoad];
    [self backNavigationItem];
    [self setNavigationBarWithDefaultColorAndTitle:@"Change Password"];
    [self setUIStyle];
    
    if(!self.navigationController) {
        NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                     NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:(14.0f * SCREEN_SCALE)],
                                     NSKernAttributeName: @1.6};
        titleTemp.attributedText = [[NSAttributedString alloc] initWithString:@"Change Password".uppercaseString attributes:attributes];
        titleTemp.numberOfLines = 0;
        //    titleLabel.adjustsFontSizeToFitWidth = YES;
        titleTemp.textAlignment = NSTextAlignmentCenter;
    } else {
        titleTemp.hidden = YES;
        topConstraintStackView.constant = 50;
    }
    [self trackingScreenByName:@"Change password"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    tappedOutsideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tappedOutsideKeyboard];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view removeGestureRecognizer:tappedOutsideKeyboard];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUIStyle{
    
    [self setStatusSubmitButton];
    [self.scrollView setBackgroundColorForView];
    [self.contentView setBackgroundColorForView];
    
    [self.submitButton setBackgroundColorForNormalStatus];
    [self.submitButton setBackgroundColorForTouchingStatus];
    [self.submitButton configureDecorationForButton];
    
    [self.cancelButton setBackgroundColorForNormalStatus];
    [self.cancelButton setBackgroundColorForTouchingStatus];
    [self.cancelButton configureDecorationForButton];
    
    self.oldPWTextField.delegate = self;
    self.nPWTextField.delegate = self;
    self.confirmNewPWTextField.delegate = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setTextFieldDefaultBottomBolder];
        self.topConstraintContentView.constant = CGRectGetHeight(self.contentView.frame) / 9;
    });
    [self createAsteriskForTextField:self.oldPWTextField];
    [self createAsteriskForTextField:self.nPWTextField];
    [self createAsteriskForTextField:self.confirmNewPWTextField];
    
    [self.lblNote setLineSpacing:6];
}

#pragma mark - Configure UI
-(void) createAsteriskForTextField:(UITextField *)textField {
    
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 8, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [textField setLeftView:leftView];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    asteriskImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"asterisk_icon"]];
    asteriskImage.frame = CGRectMake(0.0f, 0.0f, 20.0f, 20.0f);
    asteriskImage.contentMode = UIViewContentModeCenter;
    [textField setRightView:asteriskImage];
    [textField setRightViewMode:UITextFieldViewModeAlways];
}
-(void) makeBecomeFirstResponderForTextField {
    [self resignFirstResponderForAllTextField];
    
    if(self.oldPWTextField.text.length == 0){
        [self.oldPWTextField becomeFirstResponder];
    }
    else if(self.nPWTextField.text.length == 0){
        [self.nPWTextField becomeFirstResponder];
    }
    else if(self.confirmNewPWTextField.text.length == 0){
        [self.confirmNewPWTextField becomeFirstResponder];
    }
    else if(![self.nPWTextField.text isEqualToString:self.confirmNewPWTextField.text])
    {
        [self.confirmNewPWTextField becomeFirstResponder];
    }
}
- (void)resignFirstResponderForAllTextField {
    
    if(self.oldPWTextField.isFirstResponder) {
        [self.oldPWTextField resignFirstResponder];
    }else if (self.nPWTextField.isFirstResponder) {
        [self.nPWTextField resignFirstResponder];
    }else if (self.confirmNewPWTextField.isFirstResponder) {
        [self.confirmNewPWTextField resignFirstResponder];
    }
}
- (void)setTextFieldDefaultBottomBolder {
    [self.oldPWTextField setBottomBolderDefaultColor];
    [self.nPWTextField setBottomBolderDefaultColor];
    [self.confirmNewPWTextField setBottomBolderDefaultColor];
}
- (void)setStatusSubmitButton{
    self.submitButton.enabled = self.cancelButton.enabled = (self.oldPWTextField.text.length > 0 || self.nPWTextField.text.length > 0 || self.confirmNewPWTextField.text.length > 0);
}

- (void)setEnableActionButtonsWithValueOldPW:(NSString *)oldPW withNewPW:(NSString*)newPW withConfirmNPW:(NSString*)confirmNPW{
    self.submitButton.enabled = self.cancelButton.enabled = (oldPW.length > 0 || newPW.length > 0 || confirmNPW.length > 0);
}

#pragma mark - Selector
- (void)dismissVC {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)dismissKeyboard {
    [self resignFirstResponderForAllTextField];
}

#pragma mark TEXT FIELD DELEGATE
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == self.oldPWTextField || textField == self.nPWTextField || textField == self.confirmNewPWTextField) {
        if ([string isEqualToString:@" "]) {
            return NO;
        }
        if (textField.text.length >= 25 && range.length == 0){
            return NO;
        }
        NSString *resultText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if (textField == self.oldPWTextField) {
            [self setEnableActionButtonsWithValueOldPW:resultText withNewPW:self.nPWTextField.text withConfirmNPW:self.confirmNewPWTextField.text];
        }else if (textField == self.nPWTextField) {
            [self setEnableActionButtonsWithValueOldPW:self.oldPWTextField.text withNewPW:resultText withConfirmNPW:self.confirmNewPWTextField.text];
        }else if (textField == self.confirmNewPWTextField) {
            [self setEnableActionButtonsWithValueOldPW:self.oldPWTextField.text withNewPW:self.nPWTextField.text withConfirmNPW:resultText];
        }
    }
    
    return YES;
}

#pragma mark - Validation
-(void)verifyData{
    NSString *errorAll = @"All fields are required.\n";
    NSMutableString *message = [[NSMutableString alloc] init];
    
    [self.oldPWTextField setBottomBolderDefaultColor];
    [self.nPWTextField setBottomBolderDefaultColor];
    [self.confirmNewPWTextField setBottomBolderDefaultColor];
    
    
    if (self.oldPWTextField.text.length == 0 || self.nPWTextField.text.length == 0 || self.confirmNewPWTextField.text.length == 0) {
        [message appendString:@"* "];
        [message appendString:errorAll];
        
        if(self.oldPWTextField.text.length == 0) {
            [self.oldPWTextField setBottomBorderRedColor];
        }
        if(self.nPWTextField.text.length == 0) {
            [self.nPWTextField setBottomBorderRedColor];
        }
        if(self.confirmNewPWTextField.text.length == 0) {
            [self.confirmNewPWTextField setBottomBorderRedColor];
        }
    }
    
//    if(self.oldPWTextField.text.length > 0) {
//        if([self verifyValueForTextField:self.oldPWTextField].length > 0) {
//            [message appendString:@"* "];
//            [message appendString:[self verifyValueForTextField:self.oldPWTextField]];
//            [message appendString:@"\n"];
//        }
//    }
//    if(self.nPWTextField.text.length > 0) {
//        if([self verifyValueForTextField:self.nPWTextField].length > 0) {
//            [message appendString:@"* "];
//            [message appendString:[self verifyValueForTextField:self.nPWTextField]];
//            [message appendString:@"\n"];
//        }
//    }
    if(self.confirmNewPWTextField.text.length > 0) {
        if([self verifyValueForTextField:self.confirmNewPWTextField].length > 0) {
            [message appendString:@"* "];
            [message appendString:[self verifyValueForTextField:self.confirmNewPWTextField]];
            [message appendString:@"\n"];
        }
    }
    
    [self showError:message];
}

-(void)showError:(NSString *)message
{
    if(message.length > 0){
        
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = @"Please confirm that the value entered is correct:";
        alert.msgAlert = message;
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;
            alert.lblAlertMessage.textAlignment = NSTextAlignmentLeft;
            [alert.view layoutIfNeeded];
        });
        
        alert.blockFirstBtnAction = ^(void){
            [self makeBecomeFirstResponderForTextField];
        };
        
        [self showAlert:alert forNavigation:NO];
    }
    else{
        if(!isNetworkAvailable()) {
            [self showErrorNetwork];
            
        }else{
            [self resignFirstResponderForAllTextField];
            [self setTextFieldDefaultBottomBolder];
            [self changePasswordWithAPI];
        }
    }
}
-(NSString*)verifyValueForTextField:(UITextField *)textFied{
    
    NSString *errorMsg = @"";
   if(textFied == self.oldPWTextField && ![(textFied.isFirstResponder ? textFied.text :self.oldPWTextField.text) isValidWeakPassword]){ //isValidWeakPassword
        errorMsg = @"The password you entered is incorrect.";
        [textFied setBottomBorderRedColor];
   }else if (textFied == self.nPWTextField && ![(textFied.isFirstResponder ? textFied.text :self.nPWTextField.text) isValidStrongPassword]) { //isValidStrongPassword
       errorMsg = NSLocalizedString(@"input_invalid_password_msg", nil);
       [textFied setBottomBorderRedColor];
   }else if (textFied == self.confirmNewPWTextField && ![(textFied.isFirstResponder ? textFied.text :self.confirmNewPWTextField.text) isEqualToString:self.nPWTextField.text]) {
       errorMsg = NSLocalizedString(@"input_invalid_confirm_password_msg", nil);
       [textFied setBottomBorderRedColor];
   }
    else{
        [textFied setBottomBolderDefaultColor];
    }
    
    return errorMsg;
}

#pragma mark - Delegate from API
- (void)WSBaseNetworkUnavailable{
    [self stopActivityIndicator];
}
-(void)loadDataDoneFrom:(WSBase*)ws
{
    [self stopActivityIndicator];
    if (((WSChangePassword*)ws).isSuccesful) {
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.msgAlert = @"You have successfully updated your password.";
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        alert.blockFirstBtnAction = ^{
            [[SessionData shareSessiondata] setHasForgotAccount:NO];
            if (self.navigationController) {
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                appdelegate = appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
                
                [UIView transitionWithView:appdelegate.window
                                  duration:0.5
                                   options:UIViewAnimationOptionPreferredFramesPerSecond60
                                animations:^{
                                    
                                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                    MenuViewController *menuViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
                                    UINavigationController *menuNavigationViewController = [[UINavigationController alloc] initWithRootViewController:menuViewController];
                                    UIViewController *fontViewController = [[HomeViewController alloc] init];
                                    
                                    SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:menuNavigationViewController frontViewController:fontViewController];
                                    
                                    revealController.delegate = appdelegate;
                                    revealController.rearViewRevealWidth = SCREEN_WIDTH;
                                    revealController.rearViewRevealOverdraw = 0.0f;
                                    revealController.rearViewRevealDisplacement = 0.0f;
                                    appdelegate.viewController = revealController;
                                    appdelegate.window.rootViewController = appdelegate.viewController;
                                    [appdelegate.window makeKeyWindow];
                                    
                                    UIViewController *newFrontController = [[HomeViewController alloc] init];
                                    UINavigationController *newNavigationViewController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                                    [revealController pushFrontViewController:newNavigationViewController animated:YES];
                                }
                                completion:nil];

                 //[self performSelector:@selector(dismissVC) withObject:nil afterDelay:0.0];
            }
            
        };
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
        });
        
        [self showAlert:alert forNavigation:NO];
    }
    
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode
{
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }
}

-(void)loadDataFailFrom:(WSBase *)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message {
    
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }else{
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = @"Please confirm that the value entered is correct:";
        alert.msgAlert = [NSString stringWithFormat:@"* %@",message];
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
            alert.lblAlertMessage.textAlignment = NSTextAlignmentLeft;

        });
        
        alert.blockFirstBtnAction = ^{
            [self.oldPWTextField setBottomBorderRedColor];
            [self.oldPWTextField becomeFirstResponder];
        };
        
        [self showAlert:alert forNavigation:NO];
    }
    
}

#pragma mark - Actions

- (void)changePasswordWithAPI{
    
    [self startActivityIndicator];
    __weak typeof (self) _self = self;
    //    if ([[AccountAuthentication password] isEqualToString:self.oldPWTextField.text]) {
    //
    //    }else{
    //
    //    }
    [ModelAspireApiManager changePasswordWithNewPassword:self.confirmNewPWTextField.text andOldPassword:self.oldPWTextField.text completion:^(NSError *error) {
        [_self stopActivityIndicator];
        if (!error) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                [_self changePasswordSuccessRedirect];
            });
        }else{
            //            [_self showApiErrorWithMessage:@"Please confirm that the value entered is correct."];
            dispatch_sync(dispatch_get_main_queue(), ^{
                
                //            [_self showApiErrorWithMessage:@"Please confirm that the value entered is correct."];
                //            [_self showApiErrorWithMessage:[AspireApiError getMessageAPIChangePasswordFromError:error]];
                if ([AspireApiError getErrorTypeAPIChangePasswordFromError:error] == CHANGEPW_ERR) {
                    AlertViewController *alert = [[AlertViewController alloc] init];
                    alert.titleAlert = @"Password requirements were not met";
                    alert.msgAlert = NSLocalizedString(@"input_invalid_change_password_msg", nil);
                    alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        alert.seconBtn.hidden = YES;
                        alert.midView.alpha = 0.0f;;
                        alert.lblAlertMessage.textAlignment = NSTextAlignmentCenter;
                        
                    });
                    
                    alert.blockFirstBtnAction = ^{
                        [self.nPWTextField setBottomBorderRedColor];
                        [self.nPWTextField becomeFirstResponder];
                    };
                    
                    [self showAlert:alert forNavigation:NO];
                }else if([AspireApiError getErrorTypeAPIChangePasswordFromError:error] == OLDPW_ERR)
                {
                    AlertViewController *alert = [[AlertViewController alloc] init];
                    alert.titleAlert = @"Please confirm that the value entered is correct:";
                    alert.msgAlert = @"The password you entered do not match.",
                    alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        alert.seconBtn.hidden = YES;
                        alert.midView.alpha = 0.0f;;
                        alert.lblAlertMessage.textAlignment = NSTextAlignmentCenter;
                        
                    });
                    
                    alert.blockFirstBtnAction = ^{
                        [self.oldPWTextField setBottomBorderRedColor];
                        [self.oldPWTextField becomeFirstResponder];
                    };
                    
                    [self showAlert:alert forNavigation:NO];
                }else if([AspireApiError getErrorTypeAPIChangePasswordFromError:error] == NETWORK_UNAVAILABLE_ERR)
                {
                    [_self showErrorNetwork];
                }else {
                    [_self showApiErrorWithMessage:@""];
                }
            });
        }
    }];
    /*
    [self startActivityIndicator];
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    
    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo]; 
    if (profileDictionary) {
        [dataDict setValue:[profileDictionary objectForKey:keyEmail] forKey:@"Email"];
    }

    [dataDict setValue:[self.oldPWTextField.text AES256EncryptWithKey:EncryptKey] forKey:[keyOldSecret AES256DecryptWithKey:EncryptKey]];
    [dataDict setValue:[self.nPWTextField.text AES256EncryptWithKey:EncryptKey] forKey:[keyNewSecret AES256DecryptWithKey:EncryptKey]];
    
    self.wsChangePassword = [[WSChangePassword alloc] init];
    self.wsChangePassword.delegate = self;
    [self.wsChangePassword submitChangePassword:dataDict];
     */
}

- (IBAction)SubmitChangePassword:(id)sender {
    [self updateEdgeInsetForHideKeyboard];
    [self verifyData];
}

- (IBAction)CancelAction:(id)sender {
    [self.view endEditing:YES];
    self.oldPWTextField.text = @"";
    self.nPWTextField.text = @"";
    self.confirmNewPWTextField.text = @"";
    [self setStatusSubmitButton];
    [self setTextFieldDefaultBottomBolder];
}

#pragma mark Keyboard
-(void)handleKeyboardWillShow:(NSNotification *)paramNotification
{
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         keyboardHeight = keyboardRect.size.height;
                         [self updateEdgeInsetForShowKeyboard];
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

-(void)handleKeyboardWillHide:(NSNotification *)paramNotification
{
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         [self updateEdgeInsetForHideKeyboard];
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
    
}

-(void)changePasswordSuccessRedirect{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.msgAlert = @"You have successfully updated your password.";
    alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
    alert.blockFirstBtnAction = ^{
        [[SessionData shareSessiondata] setHasForgotAccount:NO];
        if (self.navigationController) {
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            appdelegate = appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
            
            [UIView transitionWithView:appdelegate.window
                              duration:0.5
                               options:UIViewAnimationOptionPreferredFramesPerSecond60
                            animations:^{
                                
                                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                MenuViewController *menuViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
                                UINavigationController *menuNavigationViewController = [[UINavigationController alloc] initWithRootViewController:menuViewController];
                                UIViewController *fontViewController = [[HomeViewController alloc] init];
                                
                                SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:menuNavigationViewController frontViewController:fontViewController];
                                
                                revealController.delegate = appdelegate;
                                revealController.rearViewRevealWidth = SCREEN_WIDTH;
                                revealController.rearViewRevealOverdraw = 0.0f;
                                revealController.rearViewRevealDisplacement = 0.0f;
                                appdelegate.viewController = revealController;
                                appdelegate.window.rootViewController = appdelegate.viewController;
                                [appdelegate.window makeKeyWindow];
                                
                                UIViewController *newFrontController = [[HomeViewController alloc] init];
                                UINavigationController *newNavigationViewController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                                [revealController pushFrontViewController:newNavigationViewController animated:YES];
                            }
                            completion:nil];
            
            //[self performSelector:@selector(dismissVC) withObject:nil afterDelay:0.0];
        }
        
    };
    dispatch_async(dispatch_get_main_queue(), ^{
        alert.seconBtn.hidden = YES;
        alert.midView.alpha = 0.0f;;
    });
    
    [self showAlert:alert forNavigation:NO];
}



-(void)updateEdgeInsetForShowKeyboard {

    self.bottomConstraintViewAction.constant = (keyboardHeight + MARGIN_KEYBOARD);
    
}


-(void)updateEdgeInsetForHideKeyboard
{
    [self resignFirstResponderForAllTextField];
    self.bottomConstraintViewAction.constant = BOTTOM_SPACE;
}

@end
