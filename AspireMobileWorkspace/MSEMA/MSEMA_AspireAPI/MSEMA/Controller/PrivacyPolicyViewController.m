//
//  PrivacyPolicyViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 4/26/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "PrivacyPolicyViewController.h"
#import "Constant.h"
#import "SWRevealViewController.h"
#import "CreateProfileViewController.h"
#import "UIButton+Extension.h"
#import "UILabel+Extension.h"
#import "AppData.h"
#import "HomeViewController.h"
#import "Common.h"
#import "WSB2CGetPolicyInfo.h"
#import "PolicyInfoItem.h"

@interface PrivacyPolicyViewController ()<DataLoadDelegate,UIWebViewDelegate,UIScrollViewDelegate>
@end

@implementation PrivacyPolicyViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTextStyleForView];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkButtonAndEnableSubmit)];
    [self.confirmMessageLabel addGestureRecognizer:tap];
    
    [self.checkBoxButton setImage:[UIImage imageNamed:@"checkbox_uncheck"] forState:UIControlStateNormal];
    [self.checkBoxButton setImage:[UIImage imageNamed:@"checkbox_check"] forState:UIControlStateSelected];
    [self.checkBoxButton addTarget:self action:@selector(enableSubmitButton:) forControlEvents:UIControlEventTouchUpInside];
    
    self.webView.delegate = self;
    self.webView.scrollView.delegate = self;
    [self.webView setBackgroundColor:[UIColor clearColor]];
    [self.webView setOpaque:NO];
    
    if(self.isRecheckPolicy)
    {
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = nil;
        alert.msgAlert = @"The Privacy Policy has been updated. Please confirm that you have accepted it.";
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.midView.alpha = 0.0f;;
            alert.seconBtn.hidden = YES;
        });
        
        [self showAlert:alert forNavigation:NO];
    }
   
//    self.textView.text = @"MAKING EACH EXPERIENCE A PRICELESS ONE, NO MATTER WHERE YOU ARE.<br/><br/>With the Mastercard Concierge app, we\\U2019ll help you book and arrange those memorable experiences that you won\\U2019t get anywhere else.  Whether you\\U2019re interested in travel planning, shopping services, dining reservations, or access to a once-in-a-lifetime event, Mastercard Concierge\\U2019s dedicated team will work diligently to fulfill your every request.<br/><br/>We look forward to making your experiences priceless, 24/7/365, everywhere and anywhere.  Just call us toll free at 1-877-288-6503 or email us at MastercardAppConcierge@vipdeskservice.com to book your experience now.  This is a free service for all Mastercard World or World Elite card holders.";
    
    if (self.policyText) {
        content = self.policyText;
        
        [self getContentHeight];
        
        [self.webView loadHTMLString:centerHTMLString(content, FONT_MarkForMC_REGULAR, (16.0 * SCREEN_SCALE), @"#FFFFFF") baseURL:nil];
    }else{
        [self startActivityIndicator];
        WSB2CGetPolicyInfo *ws = [[WSB2CGetPolicyInfo alloc] init];
        ws.delegate = self;
        [ws retrieveDataFromServer];
    }
    
    
}

-(void)setTextStyleForView
{
    [self setConfirmMessageLabelStatus:NO];
    [self enableCheckButton:NO];
    // Text Style For Title
    self.titleViewController.attributedText = [self attributedStringWithString:NSLocalizedString(@"privacy_policy_title_message", nil) andFont:FONT_MarkForMC_MED andSize:14.0*SCREEN_SCALE andColor:@"#FFFFFF"];
    
    
    // Text Style For Submit Button
    [self.submitButton setTitle:NSLocalizedString(@"submit_button_title", nil) forState:UIControlStateNormal];
    [self.submitButton setBackgroundColorForNormalStatus];
    [self.submitButton setBackgroundColorForTouchingStatus];
    [self.submitButton configureDecorationForButton];
    [self.submitButton setBackgroundColorForDisableStatus];
}

- (NSAttributedString*)attributedStringWithString:(NSString*)string andFont:(NSString*)fontString andSize:(float)size andColor:(NSString*)color{
    
    NSRange range = NSMakeRange(0, string.length);
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:string];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont fontWithName:fontString size:size] range:range];
    [attributeString addAttribute:NSForegroundColorAttributeName value:colorFromHexString(color) range:range];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init] ;
    paragraphStyle.lineSpacing = 1.25;
    [attributeString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    return attributeString;
}

-(void)checkButtonAndEnableSubmit
{
    
    if(self.checkBoxButton.selected)
    {
        self.checkBoxButton.selected = NO;
    }
    else{
        self.checkBoxButton.selected = YES;
    }

    [self.checkBoxButton setBackgroundColor:[UIColor clearColor]];
    [self.submitButton setEnabled:self.checkBoxButton.selected];
    if(self.checkBoxButton.selected)
    {
        [AppData storedTimeAcceptedPolicy];
    }
}

- (void)setConfirmMessageLabelStatus:(BOOL)status{
    if (status == YES) {
        // Text Style For Greet Message
        self.confirmMessageLabel.attributedText = [self attributedStringWithString:NSLocalizedString(@"privacy_policy_confirm_message", nil) andFont:FONT_MarkForMC_REGULAR andSize:18.0*SCREEN_SCALE andColor:@"#FFFFFF"];
        self.confirmMessageLabel.userInteractionEnabled = YES;
    }else{
        // Text Style For Greet Message
        self.confirmMessageLabel.attributedText = [self attributedStringWithString:NSLocalizedString(@"privacy_policy_confirm_message", nil) andFont:FONT_MarkForMC_REGULAR andSize:18.0*SCREEN_SCALE andColor:@"#E5EBEE"];
        self.confirmMessageLabel.userInteractionEnabled = NO;
    }
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void) enableCheckButton:(BOOL)status{
    [self.checkBoxButton setEnabled:status];
    self.checkBoxButton.userInteractionEnabled = status;
}


-(void) enableSubmitButton:(UIButton *)button
{
    [button setHighlighted:NO];
    self.checkBoxButton.selected = !(button.selected);
    [button setBackgroundColor:[UIColor clearColor]];
    [self.submitButton setEnabled:button.selected];
    
    if(self.checkBoxButton.selected)
    {
        [AppData storedTimeAcceptedPolicy];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getContentHeight{
    UITextView *tempTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, self.webView.frame.size.width, self.webView.frame.size.height)];
    
    tempTextView.attributedText = [[NSAttributedString alloc]
                                   initWithData: [content dataUsingEncoding:NSUnicodeStringEncoding]
                                   options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                   documentAttributes: nil
                                   error: nil
                                   ];
    textHeight = tempTextView.contentSize.height;
}


- (IBAction)submitButtonTapped:(id)sender {
    if(self.isRecheckPolicy || [AppData isCreatedProfile])
    {
        if (self.policyVersion) {
            [[SessionData shareSessiondata] setCurrentPolicyVersion:self.policyVersion];
        }
        SWRevealViewController *revealViewController = self.revealViewController;
        HomeViewController *homeViewConroller = [[HomeViewController alloc] init];
        UINavigationController *fontViewController = [[UINavigationController alloc] initWithRootViewController:homeViewConroller];
        [revealViewController pushFrontViewController:fontViewController animated:YES];
    }
    else
    {
        SWRevealViewController *revealViewController = self.revealViewController;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        CreateProfileViewController *createProfileVC = [storyboard instantiateViewControllerWithIdentifier:@"CreateProfileViewController"];
        [revealViewController pushFrontViewController:createProfileVC animated:YES];
    }
}

- (NSString*) replaceWebLinkInString:(NSString*)string{
    
    NSArray *tempArr = getMatchesFromString(string,@"href+=+\"(?!#+)(.*?)+\"",NSRegularExpressionCaseInsensitive);
    
    NSMutableArray* vvv = [NSMutableArray new];
    for (NSString* test in tempArr) {
        NSString* t = strimStringFrom(test,@[@"href=|\"?"]);
        if(![t containsString:@"http"]) {
            [vvv addObject:@{test:[[@"href=\"http://" stringByAppendingString:t] stringByAppendingString:@"\""]}];
        }
    }
    NSMutableString* tttt = [NSMutableString stringWithString:string];
    for (NSDictionary* t in vvv) {
        if([string containsString:[[t allKeys] firstObject]]) {
#ifdef DEBUG
            NSLog(@"%@",[[t allKeys] firstObject]);
#endif
            
            string = [tttt stringByReplacingOccurrencesOfString:[[t allKeys] firstObject] withString:[t objectForKey:[[t allKeys] firstObject]]];
        }
    }
    return string;
}

-(void)loadDataDoneFrom:(WSB2CGetPolicyInfo *)wsAboutInfo
{
    [self stopActivityIndicator];
    if(wsAboutInfo.data.count > 0)
    {
        [[SessionData shareSessiondata] setCurrentPolicyVersion:((PolicyInfoItem *)[wsAboutInfo.data objectAtIndex:0]).CurrentVersion];
        //content = [self replaceWebLinkInString:((PolicyInfoItem *)[wsAboutInfo.data objectAtIndex:0]).textInfo];
        content = ((PolicyInfoItem *)[wsAboutInfo.data objectAtIndex:0]).textInfo;
        
        [self getContentHeight];
        NSRange bodyHtmlRange = [content rangeOfString:@"font-family"];
        content = [[content stringByReplacingOccurrencesOfString:@"MarkForMC_Med" withString:FONT_MarkForMC_MED] stringByReplacingOccurrencesOfString:@"MarkForMC" withString:FONT_MarkForMC_REGULAR];
        if (bodyHtmlRange.location == NSNotFound) {
            [self.webView loadHTMLString:/*centerHTMLString(content, FONT_MarkForMC_REGULAR, (16.0 * SCREEN_SCALE), @"#FFFFFF")*/content baseURL:nil];
        }else{
            [self.webView loadHTMLString:content baseURL:nil];
        }
    }
    else{
        [self stopActivityIndicator];
    }
    
}


-(void)loadDataFailFrom:(id<BaseResponseObjectProtocol>) result withErrorCode:(NSInteger)errorCode
{
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }
}

-(void)loadDataFailFrom:(id<WSBaseProtocol>)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message
{
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }else{
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);;
        alert.msgAlert = message;
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
        });
        
        [self showAlert:alert forNavigation:YES];
    }

}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self stopActivityIndicator];
    UITextView *tempTextView = [[UITextView alloc] initWithFrame:self.webView.frame];
    tempTextView.text = content;
    
    if(textHeight < self.webView.bounds.size.height)
    {
        [self didScrollToEnd];
    }
    
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
#ifdef DEBUG
    NSLog(@"%@", request.URL);
#endif
    if (navigationType==UIWebViewNavigationTypeLinkClicked) {
        if ([request.URL.absoluteString containsString:@"http"]) {
            
            AlertViewController *alert = [[AlertViewController alloc] init];
            alert.titleAlert = NSLocalizedString(@"alert_title", nil);
            alert.msgAlert = NSLocalizedString(@"category_more_info_msg", nil);
            alert.secondBtnTitle = NSLocalizedString(@"yes_button", nil);
            alert.firstBtnTitle = NSLocalizedString(@"no_button", nil);
            alert.blockSecondBtnAction = ^(void){
                [[UIApplication sharedApplication] openURL:request.URL];
            };
            
            alert.blockFirstBtnAction  = ^(void){};
            [self showAlert:alert forNavigation:YES];
            // It was a link
            
            return NO;
        }
    }
    
    return YES;
}
- (void)openEmailBox{
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height)
    {
        [self didScrollToEnd];
        
    }
}
- (void) didScrollToEnd{
    [self setConfirmMessageLabelStatus:YES];
    [self enableCheckButton:YES];
}
- (void)WSBaseNetworkUnavailable{
    [self stopActivityIndicator];
}
@end
