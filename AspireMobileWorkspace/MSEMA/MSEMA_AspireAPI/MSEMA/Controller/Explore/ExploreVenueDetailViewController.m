//
//  ExploreVenueDetailViewController.m
//  MobileConciergeUSDemo
//
//  Created by Nghia Dinh on 7/19/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "ExploreVenueDetailViewController.h"
#import "AskConciergeViewController.h"
// Utils
#import "Constant.h"
// cell
#import "ExploreDetailHeaderImageView.h"
#import "ExploreDetailBenifitsView.h"
#import "ExploreDetailPriceView.h"
#import "ExploreDetailDescriptionView.h"

// item
#import "AnswerItem.h"
#import "WSB2CGetContentFullFromCCA.h"
#import "ContentFullCCAResponseObject.h"
#import "ContentFullCCAItem.h"
#import "TileItem.h"
#import "SearchItem.h"
#import "WSB2CGetQuestionAndAnswers.h"
#import "AppData.h"
#import "CityItem.h"
#import "CategoryItem.h"
#import "UIImageView+AFNetworking.h"
#import "MCActivityItemSource.h"
#import "AlertViewController.h"
#import "MCImageActivityItemSource.h"
#import "MCTitleActivityItemSource.h"
#import "ImageUtilities.h"

#import "NSString+Utis.h"
#import "LinkLabel.h"
#import "MapVenueDetailViewController.h"
#import "ConciergeDetailObject.h"
#import "AskConciergeRequest.h"
#import "EnumConstant.h"

@interface ExploreVenueDetailViewController () <DataLoadDelegate, ExploreDetailHeaderImageViewDelegate, ExploreDetailDescriptionDelegate, ExploreDetailBenifitsDelegate>
@property (weak, nonatomic) IBOutlet UIStackView *detailStackView;
@property (nonatomic) AnswerItem *answerItem;

@end

@implementation ExploreVenueDetailViewController {
    ContentFullCCAItem *contentFullCCAItem;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //[self registTableView];
    [self setNavigationBarWithDefaultColorAndTitle:self.item.name.uppercaseString];
    [self backNavigationItem];
    [self createConciergeBarButton];
    [self setupDataForView];
    [self trackingScreenByName:@"Venue detail"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setUpCustomizedPanGesturePopRecognizer];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)loadDataForView {
    [self.view layoutSubviews];
    switch (self.detailType) {
        case DetailType_Dining:
            [self addHeaderView:_item];
            [self addInsiderView:_item];
            [self addBenifitsView:_item];
            [self addPriceView:_item];
            [self addDescriptionView:_item withType:ViewDescription];
            [self addDescriptionView:_item withType:ViewOperatorHours];
            [self addDescriptionView:_item withType:ViewDefaultTerm];
            break;
        case DetailType_CityGuide:
            [self addHeaderView:_item];
            [self addInsiderView:_item];
            [self addDescriptionView:_item withType:ViewDescription];
            break;
        case DetailType_Other:
            [self addHeaderView:_item];
            [self addBenifitsView:_item];
            [self addDescriptionView:_item withType:ViewDescription];
            //[self addDescriptionView:_item withType:ViewTerm];
            break;
        default:
            break;
    }
    [self.view layoutIfNeeded];
}



//MARK: Add SubView To StackView
-(void)addInsiderView:(BaseItem*)item {
    if ([item isKindOfClass:[AnswerItem class]]) {
        if (((AnswerItem*)item).insiderTip.length != 0) {
            [_detailStackView addArrangedSubview:[self generateInsiderView:item withIsBenifits:NO]];
        }
    }
}
-(void)addHeaderView:(BaseItem*)item{
    [_detailStackView addArrangedSubview:[self generateHeader:item]];
}
-(void)addBenifitsView:(BaseItem*)item {
    [_detailStackView addArrangedSubview:[self generateInsiderView:item withIsBenifits:YES]];
}
-(void)addPriceView:(BaseItem*)item {
    [_detailStackView addArrangedSubview:[self generatePriceView:item]];
}
-(void)addDescriptionView:(BaseItem*)item withType:(TypeDescriptionView)type{
    [_detailStackView addArrangedSubview:[self generateDesView:item withType:type]];
}


//MARK: generate View
-(ExploreDetailHeaderImageView *)generateHeader:(BaseItem*)item {
    ExploreDetailHeaderImageView *headerView = [[ExploreDetailHeaderImageView alloc] initWithFrame:self.view.frame];
    [headerView setupDataAddress:self.item withType:self.detailType withContentFullCA:contentFullCCAItem];
    headerView.delegate = self;
    return headerView;
}
-(ExploreDetailBenifitsView*)generateInsiderView:(BaseItem*)item withIsBenifits:(BOOL)benifits {
    ExploreDetailBenifitsView *benifitsView = [[ExploreDetailBenifitsView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
    benifitsView.isBenifits = benifits;
    benifitsView.delegate = self;
    [benifitsView setupDataBenefits:_item withType:self.detailType withContentFullCA:contentFullCCAItem];
    return benifitsView;
}
-(ExploreDetailPriceView*)generatePriceView:(BaseItem*)item {
    ExploreDetailPriceView *priceView = [[ExploreDetailPriceView alloc] initWithFrame:self.view.frame];
    [priceView setupData:item];
    return priceView;
}
-(ExploreDetailDescriptionView*)generateDesView:(BaseItem*)item withType:(TypeDescriptionView)type {
    ExploreDetailDescriptionView *desView = [[ExploreDetailDescriptionView alloc] initWithFrame:self.view.frame];
    [desView setupData:item withDetailType:_detailType withFullContent:contentFullCCAItem withType:type];
    desView.delegate = self;
    return desView;
}

-(void)setupDataForView
{

    if([self.item isKindOfClass:[TileItem class]])
    {
        self.detailType = DetailType_Other;
        [self loadDataFromCCAServerWithItemId:self.item.ID];
    }
    else if([self.item isKindOfClass:[AnswerItem class]] && [self.item.categoryCode isEqualToString:@"dining"])
    {
        self.detailType = DetailType_Dining;
        [self loadDataForView];
    }
    else if([self.item isKindOfClass:[AnswerItem class]] && [self.item.categoryCode isEqualToString:@"cityguide"])
    {
        self.detailType = DetailType_CityGuide;
        [self loadDataForView];
    }
    else if([self.item isKindOfClass:[SearchItem class]])
    {
        SearchItem *searchItem = (SearchItem *)self.item;
        UIImage *headImage = [self getImageForCityGuideItem:searchItem];
        
        if([searchItem.product isEqualToString:@"CCA"])
        {
            self.detailType = DetailType_Other;
            [self loadDataFromCCAServerWithItemId:searchItem.ID];
        }
        else if([searchItem.product isEqualToString:@"IA"])
        {
            self.item.image = headImage;
            self.detailType = DetailType_Other;
            [self loadDataFromIAServerWithSearchItem:searchItem];
        }
    }
}
-(void) loadDataFromIAServerWithSearchItem:(SearchItem *)item;
{
    [self startActivityIndicatorWithoutMask];
    WSB2CGetQuestionAndAnswers *wsAnswer = [[WSB2CGetQuestionAndAnswers alloc] init];
    wsAnswer.delegate = self;
    wsAnswer.categoryId = item.secondaryID;
    wsAnswer.questionId = item.ID;
    wsAnswer.task = WS_B2C_GET_SEARCH_DETAIL;
    [wsAnswer retrieveDataFromServer];
}

- (void) loadDataFromCCAServerWithItemId:(NSString *)itemID
{
    [self startActivityIndicatorWithoutMask];
    WSB2CGetContentFullFromCCA *wSB2CGetContentFullFromCCA = [[WSB2CGetContentFullFromCCA alloc] init];
    wSB2CGetContentFullFromCCA.delegate = self;
    wSB2CGetContentFullFromCCA.itemID = itemID;
    wSB2CGetContentFullFromCCA.categoryCode = self.item.categoryCode;
    wSB2CGetContentFullFromCCA.categoryName = self.item.categoryName;
    [wSB2CGetContentFullFromCCA loadDataFromServer];
}

#pragma mark PROCESS VIEW LOGIC

-(UIImage *)getImageForCityGuideItem:(SearchItem *)searchItem
{
    NSArray *citySelections = [AppData getSelectionCityList];
    for(CityItem *cityItem  in citySelections)
    {
        for(CategoryItem *categoryItem in cityItem.subCategories)
        {
            if([searchItem.secondaryID isEqualToString:categoryItem.ID])
            {
                return categoryItem.categoryImgDetail;
            }
        }
    }
    
    return nil;
}



#pragma mark DATA LOAD DELEGATE


- (void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_B2C_GET_SEARCH_DETAIL && ws.data.count > 0)
    {
        UIImage *headImage = self.item.image;
        self.item = [ws.data objectAtIndex:0];
        
        if(headImage){
            self.detailType = DetailType_CityGuide;
            self.item.image = headImage;
        }
        else{
            self.detailType = DetailType_Dining;
        }
    }
    else if (ws.data.count > 0) {
        contentFullCCAItem = (ContentFullCCAItem*)[ws.data objectAtIndex:0];
    }
    //[self.tableView reloadData];
    [self loadDataForView];
    [self stopActivityIndicatorWithoutMask];
}


- (void)loadDataFailFrom:(id<BaseResponseObjectProtocol>)result withErrorCode:(NSInteger)errorCode{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self stopActivityIndicatorWithoutMask];
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);;
        alert.msgAlert = @"Failed to send request";
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
        });
        
        [self showAlert:alert forNavigation:YES];
    });
}


- (void)loadDataFailFrom:(id<WSBaseProtocol>)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message{
    [self stopActivityIndicatorWithoutMask];
    if (errorCode == 1005 || errorCode == -1009) {
         [self showErrorNetwork];
    } else {
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);;
        alert.msgAlert = message;
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
        });
        [self showAlert:alert forNavigation:YES];
    }
}


- (void) showErrorNetwork {
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"cannot_get_data", nil);
    alert.msgAlert = NSLocalizedString(@"no_network_connection", nil);
    alert.firstBtnTitle = NSLocalizedString(@"alert_ok_button", nil);
    alert.blockSecondBtnAction = ^(void){
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"prefs:root"]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root"]];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root"]];
        }
    };
    alert.secondBtnTitle = NSLocalizedString(@"settings_button", nil);
    alert.providesPresentationContextTransitionStyle = YES;
    alert.definesPresentationContext = YES;
    [alert setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)WSBaseNetworkUnavailable {
    [self stopActivityIndicator];
}

#pragma mark HEADER IMAGE DELEGATE

-(void)touchAskConcierge {
    /*
    AskConciergeViewController *vc = [[AskConciergeViewController alloc] init];
    ConciergeDetailObject *conciergeItem = [[ConciergeDetailObject alloc] init];
    conciergeItem.isCreated = NO;
    if (self.detailType != DetailType_Other) {
        vc.itemName = self.item.name;
        vc.categoryName = self.item.categoryName;
        conciergeItem.name = self.item.name;
        conciergeItem.requestType = [AskConciergeRequest convertCategoryCodeToMyRequestType:self.item.categoryCode];
        
        if([conciergeItem.requestType isEqualToString:DINING_REQUEST_TYPE])
        {
            conciergeItem.city = self.item.cityName;
            conciergeItem.state = self.item.state;
            conciergeItem.country = self.item.countryName;
        }
        if (self.detailType == DetailType_CityGuide) {
            vc.cityName = self.currentCity.name;
        }
    } else {
        vc.itemName = contentFullCCAItem.title;
        vc.categoryName = contentFullCCAItem.category;
        conciergeItem.name = contentFullCCAItem.title;
        conciergeItem.requestType = [AskConciergeRequest convertCategoryNameToMyRequestType:contentFullCCAItem.category andSubcategory:contentFullCCAItem.subCategory];
    }
    
   
    vc.conciergeDetail = conciergeItem;
     */
    AskConciergeViewController *vc = [[AskConciergeViewController alloc] init];
    if (self.detailType != DetailType_Other) {
        vc.itemName = self.item.name;
        vc.categoryName = self.item.categoryName;
        
        if (self.detailType == DetailType_CityGuide) {
            vc.cityName = self.currentCity.name;
        }
    } else {
        vc.itemName = contentFullCCAItem.title;
        vc.categoryName = contentFullCCAItem.category;
    }
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)shareUrl:(NSArray *)data {
    MCImageActivityItemSource *imageItemSource = [[MCImageActivityItemSource alloc] init];
    imageItemSource.data = data;
    
    MCTitleActivityItemSource *titleItemSource = [[MCTitleActivityItemSource alloc] init];
    imageItemSource.data = data;
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:@[titleItemSource,[data objectAtIndex:1],imageItemSource] applicationActivities:nil];
    [self presentViewController:activityVC animated:YES completion:^{}];
}

-(void)gotoWebBrowser:(NSURL *)url {
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"alert_title", nil);
    alert.msgAlert = NSLocalizedString(@"category_more_info_msg", nil);
    alert.secondBtnTitle = NSLocalizedString(@"yes_button", nil);
    alert.firstBtnTitle = NSLocalizedString(@"no_button", nil);
    alert.blockSecondBtnAction = ^(void){
        [[UIApplication sharedApplication] openURL:url];
    };
    alert.blockFirstBtnAction  = ^(void){};
    [self showAlert:alert forNavigation:YES];
}
-(void)seeDetailMapWithAddress:(NSString *)address {
    [self gotoAppleMapWith:address];

//    AlertViewController *alert = [[AlertViewController alloc] init];
//    alert.titleAlert = NSLocalizedString(@"alert_title", nil);
//    alert.msgAlert = NSLocalizedString(@"category_more_info_msg", nil);
//    alert.firstBtnTitle = NSLocalizedString(@"yes_button", nil);
//    alert.blockFirstBtnAction = ^(void){
//        [self gotoAppleMapWith:address];
//    };
//    alert.blockSecondBtnAction  = ^(void){};
//    [self showAlert:alert forNavigation:YES];
}


- (void) gotoAppleMapWith:(NSString*)address{
    MapVenueDetailViewController *vc = [[MapVenueDetailViewController alloc] initWithNibName:@"MapVenueDetailViewController" bundle:nil];
    vc.address = address;
    vc.item = self.item;
    vc.cityName = self.currentCity.name;
    [self.navigationController pushViewController:vc animated:true];
    
    // Check for iOS 6
    /*
    Class mapItemClass = [MKMapItem class];
    if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)])
    {
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder geocodeAddressString:address
                     completionHandler:^(NSArray *placemarks, NSError *error) {
                         
                         // Convert the CLPlacemark to an MKPlacemark
                         // Note: There's no error checking for a failed geocode
                         CLPlacemark *geocodedPlacemark = [placemarks objectAtIndex:0];
                         MKPlacemark *placemark = [[MKPlacemark alloc]
                                                   initWithCoordinate:geocodedPlacemark.location.coordinate
                                                   addressDictionary:geocodedPlacemark.addressDictionary];
                         
                         // Create a map item for the geocoded address to pass to Maps app
                         MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
                         [mapItem setName:((BaseItem*)self.item).name];
                         [MKMapItem openMapsWithItems:@[mapItem] launchOptions:nil];
                         
                     }];
    }
    */
    
}


#pragma mark TERM/DESCRIPTION DELEGATE

-(void)didSelectURL:(NSURL *)url {
    [self gotoWebBrowser:url];
}




@end
