//
//  MyRequestTableViewCell.h
//  LuxuryCard
//
//  Created by Nghia Dinh on 9/8/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyRequestViewController.h"
#import "ConciergeObject.h"

@interface MyRequestTableViewCell : UITableViewCell

-(void)setupDataForCell:(ConciergeObject*)data;

@end
