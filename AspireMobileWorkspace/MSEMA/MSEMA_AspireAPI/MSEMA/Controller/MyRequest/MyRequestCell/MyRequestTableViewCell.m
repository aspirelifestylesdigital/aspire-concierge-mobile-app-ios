//
//  MyRequestTableViewCell.m
//  LuxuryCard
//
//  Created by Nghia Dinh on 9/8/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "MyRequestTableViewCell.h"
#import "Common.h"
#import "Constant.h"



@interface MyRequestTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *indicatorImageView;
@property (weak, nonatomic) IBOutlet UILabel *requestTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *openLabel;
@property (weak, nonatomic) IBOutlet UILabel *closedLabel;
@property (weak, nonatomic) IBOutlet UILabel *openTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *closedTimeLabel;

@end

@implementation MyRequestTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.requestTypeLabel.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:18.0 * SCREEN_SCALE];
     self.openLabel.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:18.0 * SCREEN_SCALE];
    self.openTimeLabel.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:18.0 * SCREEN_SCALE];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setupDataForCell:(ConciergeObject*)data {
    NSString* requestTypeString = [NSString stringWithFormat:@"%@",  [data getStringType]];
    if (![[data getStringName] isEqualToString:@""]) {
        requestTypeString = [requestTypeString stringByAppendingString:[NSString stringWithFormat:@" - %@", [data getStringName]]];
    }
    _requestTypeLabel.text = requestTypeString;
    _openTimeLabel.text = data.createdDate != nil ? stringDateWithFormat(@"MM/dd/yyyy | h:mm aa", dateFromISO8601Time(data.createdDate)) : nil;
    if ([data.requestStatus isEqualToString:CLOSE_STATUS] || [data.requestMode isEqualToString:CANCEL_MODE]) {
        _openLabel.text = @"Closed:";
        [_indicatorImageView setHidden:NO];
    } else {
        NSDate *date = dateFromISO8601Time(data.createdDate);
        NSDate *currentDate = [[NSDate alloc] init];
        NSTimeInterval distanceBetweenDates = [currentDate timeIntervalSinceDate:date];
        if (distanceBetweenDates <= 5 * 60) {
            _openLabel.text = @"Pending:";
            [_indicatorImageView setHidden:YES];
        } else {
            _openLabel.text = @"Last Updated:";
            [_indicatorImageView setHidden:NO];
        }
    }

}


@end
