//
//  MyRequestDetailViewController.h
//  LuxuryCard
//
//  Created by Nghia Dinh on 9/8/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "BaseViewController.h"

@interface MyRequestDetailViewController : BaseViewController
@property (nonatomic, strong) NSString *epcCaseID;
@property (nonatomic, strong) NSString *transactionID;

-(IBAction)changeRequestAction:(id)sender;
-(IBAction)createRequestAction:(id)sender;
@end
