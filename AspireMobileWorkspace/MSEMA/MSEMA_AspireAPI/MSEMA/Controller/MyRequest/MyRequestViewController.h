//
//  MyRequestViewController.h
//  LuxuryCard
//
//  Created by Nghia Dinh on 9/8/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "BaseViewController.h"
#import "MyRequestPagerViewController.h"
#import "EnumConstant.h"



@interface MyRequestViewController : BaseViewController

@property (nonatomic) MyRequestType requestType;
@property (weak, nonatomic) MyRequestPagerViewController *parrentVC;
@property NSMutableArray *requestArray;

-(void)reloadData;
-(void)loadMoreIndicatorIcon:(BOOL)loading;

@end
