//
//  BaseViewController.h
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/12/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/Analytics.h>
#import "AlertViewController.h"
#import "ControlStyleHeader.h"
#import "TouchDownGestureRecognizer.h"
#import "PreferenceObject.h"

typedef enum : NSUInteger {
    ClickActionType,
    OpenActionType,
    LeaveActionType,
    SubmitActionType,
    SelectActionType
} ActionType;

typedef enum : NSUInteger {
    AuthenticationCategoryType,
    UserInteractivityCategoryType,
    RequestCategoryType,
    CitySelectionCategoryType,
    CategorySelectionCategoryType,
    SignOutCategoryType
} CategoryType;

@interface BaseViewController : UIViewController<UIScrollViewDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate>
{
    @public
    UIView                              *spinnerBackground;
    UIActivityIndicatorView             *spinner;
    UIImageView                         *indicatorImageView;
    BOOL                                showingSpinner;
    UIView                              *maskViewForSpinner;
    float                               translationPointY;
    BOOL                                isLoading;
    BOOL                                isNotChangeNavigationBarColor;
    BOOL                                isNotAskConciergeBarButton;
    BOOL                                isIgnoreScaleView;
    BOOL                                isShouldHideKeyboardWhenSwipe;
    BOOL                                isAnimatingHideKeyboard;
    TouchDownGestureRecognizer          *touchdown;
    UIPanGestureRecognizer *popRecognizer;
    UIPanGestureRecognizer *popNavigationBarRecognizer;
}

@property (nonatomic, strong) UIPercentDrivenInteractiveTransition *interactivePopTransition;
@property (nonatomic, strong) UIPanGestureRecognizer *popNavigationBarRecognizer;

-(void)createMenuBarButton;
-(void)createBackBarButtonWithIconName:(NSString*)iconName;
-(void)showMenu;
-(void)decorateForButton:(UIButton *)button;
-(void)startActivityIndicator;
-(void)stopActivityIndicator;
-(void)createConciergeBarButton;
-(void)initData;
-(void)initView;
- (void)touchBack;
-(void)setNavigationBarColor;
-(void) setNavigationBarWithDefaultColorAndTitle:(NSString*)title;
-(void)backNavigationItem;
-(void)alterWithMessage:(NSString *)message;
-(void) showAlertMCStyleWithTitle:(NSString*)title andMessage:(NSString*)message andOkButtonTitle:(NSString*)okTitle;
-(void) showAlert:(AlertViewController *)alert forNavigation:(BOOL)isNavigationView;
-(void)handlePopRecognizer:(UIPanGestureRecognizer*)recognizer;
-(void) returnNormalState;
-(void) alertOkAction;
-(void)alertCancelAction;
-(void)startActivityIndicatorWithoutMask;
-(void)stopActivityIndicatorWithoutMask;
-(void)setNavigationBarWithColor:(UIColor *)color;
-(void)changePositionForView;
-(void)setUpCustomizedPanGesturePopRecognizer;
-(void)removeSetupForCustomizedPanGesturePopRecognizer;
-(void)askConciergeAction:(id)sender;
-(void)showErrorNetwork;
-(void)hidenKeyboardWhenSwipe;

- (void) forceRelease;
-(void)showApiErrorWithMessage:(NSString*)message;
//#Tracking
- (void)trackingEventByName:(NSString*)eventName withAction:(ActionType)action withCategory:(CategoryType)category;
- (void)trackingScreenByName:(NSString*)screenName;
- (void)backNavigationAction:(id)sender;
-(void)showAlertForRetryPasscodeWithFirstBlock:(void (^)(void))actFirstBtn withSecondBlock:(void (^)(void))actSecondBtn;
- (void) handleInvalidCredentials;
- (void)signOutCurrentUser;
-(PreferenceObject*)getPreferenceByType:(PreferenceType)type;

- (void) showAlertWithTitle:(NSString*)title
                    message:(NSString*)message
                    buttons:(NSArray*)buttons actions:(NSArray*)actions
           messageAlignment:(NSTextAlignment) textAlign;

@end
