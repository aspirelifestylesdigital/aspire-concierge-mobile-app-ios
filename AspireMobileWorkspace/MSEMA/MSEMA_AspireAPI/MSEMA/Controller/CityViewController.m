//
//  CityViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "CityViewController.h"
#import "CityItem.h"
#import "CityTableViewCell.h"
#import "AppData.h"
#import "SWRevealViewController.h"
#import "UtilStyle.h"
#import "HomeViewController.h"
#import "ExploreViewController.h"
@import AspireApiFramework;
#import "AppDelegate.h"
#import "PreferenceObject.h"

#define CENTER_TAG 1
#define LEFT_PANEL_TAG 2
#define RIGHT_PANEL_TAG 3

#define CORNER_RADIUS 4

#define SLIDE_TIMING .25
#define PANEL_WIDTH 60

@interface CityViewController ()<UITableViewDelegate, UITableViewDataSource,SWRevealViewControllerDelegate>
{
    BOOL isFirstSelectCity;
}

@end

@implementation CityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.separatorColor = [UtilStyle colorForSeparateLine];
    [self trackingScreenByName:@"City list"];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"city_title", nil)];
    

    NSInteger index = self.currentCity ?  [self.cityLst indexOfObject:self.currentCity] : 0;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    if(index > self.cityLst.count - 3){
        [self.view layoutIfNeeded];
        CGPoint offset = CGPointMake(0, self.tableView.contentSize.height);
        [self.tableView setContentOffset:offset animated:YES];
    }
    else{
        [self.tableView scrollToRowAtIndexPath:indexPath
                              atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
}


-(void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    // Stop being the navigation controller's delegate
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //  Set up Pop pan gesture
//    [self setUpCustomizedPanGesturePopRecognizer];
    if(self.navigationController.viewControllers.count == 1)
    {
        isFirstSelectCity = YES;
        HomeViewController *homeView = [[HomeViewController alloc] init];
        [self.navigationController setViewControllers:@[homeView,self]];
    }
    else{
        UIViewController *vc = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count - 2];
        if(![vc isKindOfClass:[ExploreViewController class]])
        {
            isFirstSelectCity = YES;
        }
    }
}


-(void)initView
{
    [self.tableView registerNib:[UINib nibWithNibName:@"CityTableViewCell" bundle:nil] forCellReuseIdentifier:@"CityTableViewCell"];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100.f;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self backNavigationItem];
    self.navigationItem.title = NSLocalizedString(@"city_title", nil);
}

-(void)initData
{
    self.cityLst = [AppData getSelectionCityList];
}

#pragma mark TABLEVIEW DELEGATE
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.cityLst.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CityTableViewCell"];
    [cell initCellWithData:[self.cityLst objectAtIndex:indexPath.row]];
//    if()
        [cell setHiddenBottomLine:indexPath.row == self.cityLst.count-1];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CityTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setBackgroundColor:[UtilStyle colorForSeparateLine]];
    CityItem *currentCity = [self.cityLst objectAtIndex:indexPath.row];
    
    if (currentCity) {
        [self trackingEventByName:currentCity.name withAction:SelectActionType withCategory:CitySelectionCategoryType];
    }
    UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
    if ([userObject.currentCity isEqualToString:currentCity.name]) {
        if (isFirstSelectCity) {
            [self successRedirectWithCity:currentCity];
        }
        if(self.delegate)
            [self.delegate getDataBaseOnCity:currentCity];
    }else{
        [self updateCityToAPIWithCity:currentCity];
    }
}

- (void)successRedirectWithCity:(CityItem*)city{
    ExploreViewController *exploreViewController = [[ExploreViewController alloc] init];
    exploreViewController.currentCity = city;
    [AppData setCurrentCityWithCode:city.name];
    [self.navigationController setViewControllers:@[exploreViewController,self]];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)updateCityToAPIWithCity:(CityItem*)city{
    UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
    NSMutableDictionary *dictDai = [[NSMutableDictionary alloc] init];
    [dictDai setValue:userObject.salutation forKey:@"Salutation"];
    [dictDai setValue:userObject.firstName forKey:@"FirstName"];
    [dictDai setValue:userObject.lastName forKey:@"LastName"];
    [dictDai setValue:[userObject.mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""] forKey:@"MobileNumber"];
    [dictDai setValue:userObject.email forKey:@"Email"];
//    [dictDai setValue:[[SessionData shareSessiondata] passcode]  forKey:@"Other Preferences"];
    [dictDai setValue:userObject.isUseLocation ? @"ON" : @"OFF" forKey:APP_USER_PREFERENCE_Location];
    [dictDai setValue:[self getPreferenceByType:DiningPreferenceType].value forKey:@"Dining Preference"];
    [dictDai setValue:[self getPreferenceByType:HotelPreferenceType].value forKey:@"Hotel Preference"];
    [dictDai setValue:[self getPreferenceByType:TransportationPreferenceType].value forKey:@"Car Type Preference"];
    if (city.name.length > 0) {
        [dictDai setValue:city.name forKey:APP_USER_PREFERENCE_Selected_City];
    }
    [dictDai setValue:@"Unknown" forKey:@"City"];
    [dictDai setValue:@"USA" forKey:@"Country"];
    [dictDai setValue:APP_NAME forKey:@"referenceName"];
    
    // update profile
    AppDelegate* appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appdelegate updateUserFromUserInfo:dictDai];
    
    if (isFirstSelectCity) {
        [self successRedirectWithCity:city];
    }
    if(self.delegate)
        [self.delegate getDataBaseOnCity:city];
    
    //    [self startActivityIndicator];
    //
    //    __weak typeof (self) _self = self;
    //    [ModelAspireApiManager updateProfileCurrentUserWithUserInfo:dictDai completion:^(NSError *error) {
    //        [_self stopActivityIndicator];
    //        dispatch_async(dispatch_get_main_queue(), ^{
    //            if (!error) {
    //
    //            }else{
    //                [_self showApiErrorWithMessage:@""];
    //            }
    //        });
    //
    //    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}
@end
