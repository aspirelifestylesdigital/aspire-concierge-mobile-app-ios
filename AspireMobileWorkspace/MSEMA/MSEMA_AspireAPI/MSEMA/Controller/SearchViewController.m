//
//  SearchViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "SearchViewController.h"
#import "UIView+Extension.h"
#import "UIButton+Extension.h"
#import "Constant.h"
#import "Common.h"

@interface SearchViewController ()<UITextFieldDelegate>
{
    CGFloat backupBottomConstraint;
    UITapGestureRecognizer *tappedOutsideKeyboard;
    BOOL isCheckOffer;
}
@end

@implementation SearchViewController

#pragma mark - LifeCyle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    backupBottomConstraint = self.searchBtnBottomConstraint.constant;
    [self trackingScreenByName:@"Search"];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    tappedOutsideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tappedOutsideKeyboard.cancelsTouchesInView = YES;
    [self.navigationController.view addGestureRecognizer:tappedOutsideKeyboard];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWill:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController.view removeGestureRecognizer:tappedOutsideKeyboard];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SetupView

-(void)initView
{
    [self backNavigationItem];
    [self.offerCheckBox setImage:[UIImage imageNamed:@"checkbox_uncheck"] forState:UIControlStateNormal];
    [self.offerCheckBox setImage:[UIImage imageNamed:@"checkbox_check"] forState:UIControlStateSelected];
    [self.offerCheckBox addTarget:self action:@selector(enableSubmitButton:) forControlEvents:UIControlEventTouchUpInside];
    self.offerCheckBox.selected = isCheckOffer;
    [self.offerCheckBox setBackgroundColor:[UIColor clearColor]];
    
    if(self.currentKeyWord.length == 0)
    {
        self.offerCheckBox.enabled = NO;
    }
    
    [self.bookCheckBox setImage:[UIImage imageNamed:@"checkbox_uncheck"] forState:UIControlStateNormal];
    [self.bookCheckBox setImage:[UIImage imageNamed:@"checkbox_check"] forState:UIControlStateSelected];
    [self.bookCheckBox addTarget:self action:@selector(enableSubmitButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.bookCheckBox setBackgroundColor:[UIColor clearColor]];
    self.bookCheckBox.enabled = NO;
    
    [self.bookText setText:NSLocalizedString(@"can_book_search", nil)];
    [self.offerText setText:NSLocalizedString(@"offer_search", nil)];
    self.offerText.font = [UIFont fontWithName:FONT_MarkForMC_MED size:18.0f * SCREEN_SCALE];
    self.offerText.textColor = colorFromHexString(DEFAULT_BACKGROUND_COLOR);
    
    UITapGestureRecognizer *offerTextGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enableSubmitButton:)];
    [self.offerText setUserInteractionEnabled:YES];
    [self.offerText addGestureRecognizer:offerTextGesture];
    
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"search_title", nil)];
    
    [self.searchButton setBackgroundColorForNormalStatus];
    [self.searchButton setBackgroundColorForTouchingStatus];
    //[self.searchButton setBackgroundColorForDisableStatus];
    
    if(self.currentKeyWord.length == 0)
    {
        self.searchButton.enabled = NO;
    }
    
    self.searchButton.titleLabel.font = [UIFont fontWithName:FONT_MarkForMC_MED size:18.0f * SCREEN_SCALE];
    self.searchButton.titleLabel.textColor = [UIColor whiteColor];
    [self.searchButton setTitle:NSLocalizedString(@"search_title", nil) forState:UIControlStateNormal];
    
    UIImageView *searchIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"search_icon"]];
    searchIcon.frame = CGRectMake(0.0f, 0.0f, 30.0f, 30.0f);
    self.searchText.leftView = searchIcon;
    self.searchText.leftViewMode = UITextFieldViewModeAlways;
    self.searchText.delegate = self;
    self.searchText.textColor = colorFromHexString(DEFAULT_BACKGROUND_COLOR);
    self.searchText.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search" attributes:@{NSForegroundColorAttributeName:colorFromHexString(@"#99A1A8")}];
    
    UIFont *font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:18.0 * SCREEN_SCALE];
    self.searchText.font = font;
    
    if(self.currentKeyWord.length == 0)
    {
        NSString *placeHolderMsg = NSLocalizedString(@"search_text", nil);
        NSMutableAttributedString *placeHolderAttribute = [[NSMutableAttributedString alloc] initWithString:placeHolderMsg];
        [placeHolderAttribute addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, placeHolderMsg.length)];
        [placeHolderAttribute addAttribute:NSForegroundColorAttributeName value: colorFromHexString(DEFAULT_PLACEHOLDER_COLOR) range:NSMakeRange(0, placeHolderMsg.length)];
        self.searchText.attributedPlaceholder = placeHolderAttribute;
    }
    else{
        self.searchText.text = self.currentKeyWord;
    }
    [self.searchButtonView setBackgroundColor:colorFromHexString(DEFAULT_BACKGROUND_COLOR)];
    //[self.searchTextView setbackgroundColorForLineMenu];
    [self.searchTextView setBackgroundColor:colorFromHexString(DEFAULT_BACKGROUND_COLOR)];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.stackViewTopConstraint.constant = (IPAD) ? 8.0f : 20.0f;
    });
}

#pragma mark - Keyboard Process

-(void)handleKeyboardWillShow:(NSNotification *)paramNotification
{
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         self.searchBtnBottomConstraint.constant = keyboardRect.size.height + MARGIN_KEYBOARD;
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

-(void)handleKeyboardWill:(NSNotification *)paramNotification
{
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         self.searchBtnBottomConstraint.constant =  backupBottomConstraint;
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
    
}


-(void)dismissKeyboard
{
    if([self.searchText becomeFirstResponder])
    {
        [self.searchText resignFirstResponder];
    }
}

#pragma mark - Actions

-(void) enableSubmitButton:(id)sender
{
    if([self.searchText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0)
    {
        [self.offerCheckBox setHighlighted:NO];
        self.offerCheckBox.selected = !(self.offerCheckBox.selected);
    }
}

- (IBAction)searchTappedAction:(id)sender {
    [self searchAction];
}

- (void) checkOffer:(BOOL)ischeck {
    isCheckOffer = ischeck;
}
#pragma mark - TextField

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return [self searchAction];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL isPressedBackspaceAfterSingleSpaceSymbol = range.length==1 && string.length==0;
    NSString* textNeedCheck = textField.text;
    if(isPressedBackspaceAfterSingleSpaceSymbol)
        textNeedCheck = [[textNeedCheck substringToIndex:range.location] stringByAppendingString:[textNeedCheck substringWithRange:NSMakeRange(range.location+range.length, textNeedCheck.length - range.location - range.length)]];
    else
        textNeedCheck = [textNeedCheck stringByAppendingString:string];
    if([textNeedCheck stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length && ![self checkIsAllIsWhiteSpaceForText:textNeedCheck])
    {
        self.searchButton.enabled = YES;
        self.bookCheckBox.enabled = YES;
        self.offerCheckBox.enabled = YES;
    }
    else{
        if([textNeedCheck stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0){
            self.searchButton.enabled = NO;
            self.bookCheckBox.enabled = NO;
            self.bookCheckBox.selected = NO;
            self.offerCheckBox.enabled = NO;
            self.offerCheckBox.selected = NO;
        }
        else{
            self.searchButton.enabled = YES;
            self.bookCheckBox.enabled = YES;
            self.offerCheckBox.enabled = YES;
        }
    }
    return YES;
}

-(BOOL)searchAction
{
    if([self.searchText.text length] > 0 || self.offerCheckBox.selected)
    {
        if(self.delegate)
        {
            [self.searchText resignFirstResponder];
            [self.delegate getDataBaseOnSearchText:[self strimStringFrom:self.searchText.text withPatterns:@[@"^ *"]] withOffer:self.offerCheckBox.selected withBookOnline:self.bookCheckBox.selected];
        }
        return YES;
    }
    else{
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"alert_title", nil);;
        alert.msgAlert = @"Enter a search term or select a check box.";
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
        });
        
        [self showAlert:alert forNavigation:YES];
        return NO;
    }
}

#pragma mark - Logical Function

- (BOOL) checkIsAllIsWhiteSpaceForText:(NSString*) text {
    BOOL isYES = YES;
    NSArray* arr = [text componentsSeparatedByString:@""];
    for (NSString* t in arr) {
        if([t stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0) {
            isYES = NO;
            break;
        }
    }
    return isYES;
}

- (NSString*) strimStringFrom:(NSString*) from withPatterns:(NSArray*) pattern {
    NSString* temp = from;
    for (NSString* p in pattern) {
        temp = [temp stringByReplacingOccurrencesOfString:p withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0,temp.length)];
    }
    return temp;
}

@end
