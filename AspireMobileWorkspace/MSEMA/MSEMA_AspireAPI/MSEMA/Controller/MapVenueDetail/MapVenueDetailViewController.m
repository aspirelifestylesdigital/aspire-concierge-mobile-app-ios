//
//  MapVenueDetailViewController.m
//  MobileConciergeUSDemo
//
//  Created by Nghia Dinh on 7/26/17.
//  Copyright © 2017 Sunrixe Software Solutions Corporation. All rights reserved.
//

#import "MapVenueDetailViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "AnswerItem.h"

@interface MapVenueDetailViewController () <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation MapVenueDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBarWithDefaultColorAndTitle:self.item.name.uppercaseString];
    [self backNavigationItem];
    if(!isNetworkAvailable()) {
        [self showErrorNetwork];
    } else {
        [self loadMapFromAddress];
    }
}


-(void)dealloc {
    [_webView loadHTMLString:@"" baseURL:nil];
    [_webView stopLoading];
    [_webView setDelegate:nil];
    [_webView removeFromSuperview];
    
    [[NSURLCache sharedURLCache]removeAllCachedResponses];
    [[NSURLCache sharedURLCache] setDiskCapacity:0];
    [[NSURLCache sharedURLCache] setMemoryCapacity:0];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    // Dispose of any resources that can be recreated.
}


-(void)loadMapFromAddress {
    [self startActivityIndicator];
    AnswerItem *answerItem = (AnswerItem *)_item;
    NSString *queryString = @"";
    if (answerItem.name.length != 0) {
        NSString *newName = [NSString stringWithFormat:@"%@", _item.name];
        newName = [self trimName:newName withSymbol:@"at"];
        newName = [self trimName:newName withSymbol:@"-"];
        //newName = [newName stringByReplacingOccurrencesOfString:@"(" withString:@", "];
        //newName = [newName stringByReplacingOccurrencesOfString:@")" withString:@""];
        queryString = [queryString stringByAppendingString:newName];
    }
    
    if ([self.cityName isEqualToString:@"Asia Pacific"]) {
        if (answerItem.address1.length != 0) {
            queryString = [queryString stringByAppendingString:[NSString stringWithFormat:@", %@", answerItem.address1]];
        }
        if (answerItem.cityName.length != 0) {
            if ([answerItem.cityName containsString:@"Beijing"]) {
                queryString = [queryString stringByAppendingString:[NSString stringWithFormat:@", Beijing"]];
            } else {
                queryString = [queryString stringByAppendingString:[NSString stringWithFormat:@", %@", answerItem.cityName]];
            }
        }
    } else {
        if (answerItem.address1.length != 0) {
            queryString = [queryString stringByAppendingString:[NSString stringWithFormat:@", %@", answerItem.address1]];
        }
        if (answerItem.state.length != 0) {
            queryString = [queryString stringByAppendingString:[NSString stringWithFormat:@", %@", answerItem.state]];
        }
        if (answerItem.zipCode.length != 0) {
            queryString = [queryString stringByAppendingString:[NSString stringWithFormat:@", %@", answerItem.zipCode]];
        }
    }
    if (answerItem.countryName.length != 0) {
        queryString = [queryString stringByAppendingString:[NSString stringWithFormat:@", %@", answerItem.countryName]];
    }
  
    NSMutableCharacterSet * URLQueryPartAllowedCharacterSet;
    URLQueryPartAllowedCharacterSet = [[NSCharacterSet URLQueryAllowedCharacterSet] mutableCopy];
    [URLQueryPartAllowedCharacterSet removeCharactersInString:@"&"];
    NSString * escapedQueryString = [queryString stringByAddingPercentEncodingWithAllowedCharacters:URLQueryPartAllowedCharacterSet];
    NSString *requestString = [[NSString alloc] initWithFormat:@"http://maps.google.com/maps?q=%@", escapedQueryString];
    NSURLRequest *requestURL = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:requestString]];
    _webView.delegate = self;
    
    [[NSURLCache sharedURLCache] removeCachedResponseForRequest:requestURL];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [_webView loadRequest:requestURL];
}


-(NSString*)trimName:(NSString*)name withSymbol:(NSString*)symbol {
    NSString *newName = [NSString stringWithFormat:@"%@",name];
    NSRange range = [_item.name rangeOfString:[NSString stringWithFormat:@" %@ ",symbol]];
    if (range.location != NSNotFound) {
        newName = [_item.name substringToIndex:range.location];
    }
    return newName;
}





/*

-(void)loadwebFromStringURL:(NSString*)urlString {
    [self startActivityIndicator];
    NSString *add;
    if (_item.shouldUseNameInsteadAddress) {
        add = [NSString stringWithFormat:@"%@",(AnswerItem *)_item.name];
    } else {
        add = [NSString stringWithFormat:@"%@, %@", _address, ((AnswerItem *)_item).countryName];
    }
    NSString *newAddress = [self strimStringFrom:add withPatterns:@[@"\\(.*\\),",@"^(\\+\\d{1,9}\\s)?\\(?\\d{1,9}\\)?[\\s.-]?\\d{1,9}[\\s.-]?\\d{1,9},"]];
    NSString *filteredName = [self strimStringFrom:_item.name withPatterns:@[@"\\(.*\\)"]];
    
    double latitude = 0;
    double longitude = 0;

    if ([_item isKindOfClass:[AnswerItem class]]) {
        AnswerItem *diningItem = (AnswerItem*)_item;
        if ([self isValidLocation:diningItem]) {
            latitude = diningItem.latitude;
            longitude = diningItem.longitude;
            NSString *latlongString = [NSString stringWithFormat:@"@%f,%f",latitude, longitude];
            NSString *requestString = [NSString stringWithFormat:@"https://www.google.com/maps/place/%@ %@/%@,20z",filteredName, diningItem.addressForMap?:newAddress, latlongString];
            if(diningItem.addressForMap.length > 0) {
                requestString = [NSString stringWithFormat:@"https://www.google.com/maps/place/%@ (%@)/%@,20z", diningItem.addressForMap?:newAddress, filteredName, latlongString];
            }
            requestString = [requestString  stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            NSURLRequest *requestURL = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:requestString]];
            _webView.delegate = self;
            [_webView loadRequest:requestURL];
            
        } else {
            NSString *requestString = [NSString stringWithFormat:@"https://www.google.com/maps/place/%@ %@",filteredName, newAddress];
            requestString = [requestString  stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            NSURLRequest *requestURL = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:requestString]];
            _webView.delegate = self;
            [_webView loadRequest:requestURL];
        }
    }
}
*/

-(NSString*)strimStringFrom:(NSString*)from withPatterns:(NSArray*)pattern {
    NSString* temp = from;
    for (NSString* p in pattern) {
        temp = [temp stringByReplacingOccurrencesOfString:p withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0,temp.length)];
    }
    return temp;
}


-(void)webViewDidFinishLoad:(UIWebView *)webView {
    [self stopActivityIndicator];
}


-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self stopActivityIndicator];
    [self showErrorNetwork];
}


-(void)webViewDidStartLoad:(UIWebView *)webView{
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}





- (void) showErrorNetwork {
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"cannot_get_data", nil);
    alert.msgAlert = NSLocalizedString(@"no_network_connection", nil);
    alert.firstBtnTitle = NSLocalizedString(@"alert_ok_button", nil);
    alert.blockSecondBtnAction = ^(void){
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"prefs:root"]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root"]];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root"]];
        }
    };
    alert.secondBtnTitle = NSLocalizedString(@"settings_button", nil);
    alert.providesPresentationContextTransitionStyle = YES;
    alert.definesPresentationContext = YES;
    [alert setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self presentViewController:alert animated:YES completion:nil];
}


-(BOOL)isValidLocation:(AnswerItem*)item {
    return (item.latitude != 0 && item.longitude != 0);
}



@end
