//
//  MapVenueDetailViewController.h
//  MobileConciergeUSDemo
//
//  Created by Nghia Dinh on 7/26/17.
//  Copyright © 2017 Sunrixe Software Solutions Corporation. All rights reserved.
//

#import "BaseViewController.h"
#import "BaseItem.h"

@interface MapVenueDetailViewController : BaseViewController

@property (weak, nonatomic) NSString *address;
@property (weak, nonatomic) BaseItem *item;
@property (weak, nonatomic) NSString *cityName;

@end
