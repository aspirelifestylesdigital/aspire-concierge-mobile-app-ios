//
//  SplashScreenViewController.m
//  MobileConciergeUSDemo
//
//  Created by Nghia Dinh on 7/13/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "SplashScreenViewController.h"
#import "SWRevealViewController.h"
#import "UDASignInViewController.h"
#import "Constant.h"
#import "EnumConstant.h"
#import "HomeViewController.h"
#import "UIView+Extension.h"
#import "AppData.h"
#import "PrivacyPolicyViewController.h"
#import "CreateProfileViewController.h"
#import "Common.h"
#import "WSB2CGetRequestToken.h"
#import "WSB2CGetAccessToken.h"
#import "WSB2CGetUserDetails.h"
#import "WSB2CGetPolicyInfo.h"
#import "WSB2CVerifyBIN.h"
#import "PolicyInfoItem.h"
#import "BINItem.h"
#import "WSSignIn.h"
#import "MenuViewController.h"
#import "ChangePasswordViewController.h"

@import AspireApiFramework;
#import "PreferenceObject.h"

#import "AppDelegate.h"

@interface SplashScreenViewController () <DataLoadDelegate>
{
    AppDelegate* appdelegate;
}
@end

@implementation SplashScreenViewController
{
    NSInteger currentTask;
    BOOL isLoadPolicy;
    WSB2CGetRequestToken *wsRequestToken;
    WSB2CGetAccessToken *wsAccessToken;
    WSB2CGetUserDetails *wsGetUser;
    WSB2CGetPolicyInfo *wsPolicy;
    
    NSNumber *currentPolicy;
    dispatch_group_t group;
    dispatch_queue_t queue;
    float policyVersion;
    
}

- (void)viewDidLoad {
    isIgnoreScaleView = YES;
    [super viewDidLoad];
    [self trackingScreenByName:@"Splash"];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.view layoutIfNeeded];
    
    [self getUUID];
    
    group = dispatch_group_create();
    queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    dispatch_group_notify(group, queue, ^{
        NSLog(@"All tasks done");
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self goToNextViewController];
    });
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getUUID{
    if (![[SessionData shareSessiondata] UUID]) {
        NSString *uuidString = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [[SessionData shareSessiondata] setUUID:uuidString];
    }
}

- (void)appWillEnterForeground:(NSNotification*) noti {
    if(!isNetworkAvailable()) {
        [self showNetWorkingStatusArlet];
    }
}

- (void) CheckBINNumber{
    
    if(!isNetworkAvailable()) {
        [self showNetWorkingStatusArlet];
    }
}

-(void)goToNextViewController {
    
    if(isJailbroken())
    {
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = nil;
        alert.msgAlert = NSLocalizedString(@"jailbroken_message", nil);
        alert.firstBtnTitle = @"OK";
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
        });
        
        alert.blockFirstBtnAction = ^(void){
            //Quit app
            exit(0);
        };
        [self showAlert:alert forNavigation:NO];
        
        return;
    }
    
    if ([User isValid]) {
//        __block NSDate* startTime = [NSDate date];
        __weak typeof (self) _self = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [_self navigationToHomeViewController];
        });
        
        AppDelegate *appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
        [appdelegate retriveProfile];
        
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self navigationToUDASignInViewController];
        });
    }
    //                if(passcodeText.length > 0)
    //                {
    //                    wsPasscodeVerification = [[WSB2CPasscodeVerfication alloc] init];
    //                    wsPasscodeVerification.delegate = _self;
    //                    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    //                    [dataDict setValue:passcodeText forKey:keyPasscode];
    //                    [wsPasscodeVerification verifyPasscode:dataDict];
    //                }else{
    //                    dispatch_async(dispatch_get_main_queue(), ^{
    //                        NSInteger wait = [[NSDate date] timeIntervalSinceDate:startTime];
    //                        if (wait < 0) wait = 0;
    //                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(wait * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    //                            if(error.code == ERR_USER_TOKEN){
    //                                [_self handleInvalidCredentials];
    //                            }else{
    //                                [_self handleInvalidPasscode];
    //                            }
    //                        });
    //                    });
    //                }
    //            }else{
    //                dispatch_async(dispatch_get_main_queue(), ^{
    //                    if(error.code == ERR_USER_TOKEN){
    //                        [_self handleInvalidCredentials];
    //                    }else{
    //                        AlertViewController *alert = [[AlertViewController alloc] init];
    //                        alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);
    //                        alert.msgAlert = NSLocalizedString(@"api_common_error_message", nil);
    //                        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
    //                        alert.blockFirstBtnAction = ^{
    //                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
    //                            [[NSUserDefaults standardUserDefaults] synchronize];
    //                            [ModelAspireApiManager logout];
    //                            //        [self navigationToUDASignInViewController];
    //                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //                            UDASignInViewController *signinViewController = [storyboard instantiateViewControllerWithIdentifier:@"UDASignInViewController"];
    //                            UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:signinViewController];
    //                            AppDelegate *appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    //                            appdelegate.window.rootViewController = navigationController;
    //                            [appdelegate.window makeKeyAndVisible];
    //                        };
    //
    //                        dispatch_async(dispatch_get_main_queue(), ^{
    //                            alert.seconBtn.hidden = YES;
    //                            alert.midView.alpha = 0.0f;
    //                        });
    //
    //                        [_self showAlert:alert forNavigation:NO];
    //                    }
    //                });
    //            }
    //        }];
    
    //    }else{
    //        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    //            [self goToNextViewController];
    //        });
    //    }
    //
    
    //
    //    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
    //
    //    if (profileDictionary) {
    //        [self navigationToHomeViewController];
    //    }else{
    //        [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
    //        [[NSUserDefaults standardUserDefaults] synchronize];
    //        [self navigationToUDASignInViewController];
    //    }
    
}

-(void)showAlertError {
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);
    alert.msgAlert = NSLocalizedString(@"api_common_error_message", nil);
    alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
    alert.blockFirstBtnAction = ^{
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [ModelAspireApiManager logout];
        //        [self navigationToUDASignInViewController];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UDASignInViewController *signinViewController = [storyboard instantiateViewControllerWithIdentifier:@"UDASignInViewController"];
        UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:signinViewController];
        AppDelegate *appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
        appdelegate.window.rootViewController = navigationController;
        [appdelegate.window makeKeyAndVisible];
    };
    
    dispatch_async(dispatch_get_main_queue(), ^{
        alert.seconBtn.hidden = YES;
        alert.midView.alpha = 0.0f;
    });
    
    [self showAlert:alert forNavigation:NO];
}


- (void) loadPolicy{
    // load policy privacy
    wsPolicy = [[WSB2CGetPolicyInfo alloc] init];
    wsPolicy.delegate = self;
    isLoadPolicy = YES;
    [wsPolicy retrieveDataFromServer];
}

- (void) getRequestToken{
    wsRequestToken = [[WSB2CGetRequestToken alloc] init];
    wsRequestToken.delegate = self;
    currentTask = WS_GET_REQUEST_TOKEN;
    [wsRequestToken getRequestToken];
}


- (void)loadDataDoneFrom:(id<WSBaseProtocol>)ws {
    if ([ws isKindOfClass:[WSSignIn class]]) {
        [self navigationToHomeViewController];
    }
}

- (void)loadDataFailFrom:(id<BaseResponseObjectProtocol>)result withErrorCode:(NSInteger)errorCode{
#ifdef DEBUG
    NSLog(@"faillllllllll lllll");
#endif
    
}

- (void)loadDataFailFrom:(id<WSBaseProtocol>)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);;
    alert.msgAlert = message;
    alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        alert.seconBtn.hidden = YES;
        alert.midView.alpha = 0.0f;;
    });
    
    [self showAlert:alert forNavigation:NO];
}


- (void) showNetWorkingStatusArlet{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = nil;
    alert.msgAlert = NSLocalizedString(@"no_network_connection", nil);
    alert.firstBtnTitle = NSLocalizedString(@"arlet_cancel_button", nil);
    //alert.secondBtnTitle = NSLocalizedString(@"arlet_retry_button", nil);
    
    alert.secondBtnTitle = NSLocalizedString(@"home_button_title", nil);
    alert.blockSecondBtnAction = ^(void){
        [self CheckBINNumber];
    };
    
    [self showAlert:alert forNavigation:NO];
}

-(void)navigationToHomeViewController
{
    if ([[SessionData shareSessiondata] hasForgotAccount]) {
        //        Remove old data user.
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
        [[NSUserDefaults standardUserDefaults] synchronize];
        if ([SessionData shareSessiondata].arrayPreferences.count > 0) {
            [[SessionData shareSessiondata].arrayPreferences removeAllObjects];
        }
        
        [self navigationToUDASignInViewController];
        //        ChangePasswordViewController *changePasswordVC = [[ChangePasswordViewController alloc] init];
        //        [self presentViewController:changePasswordVC animated:YES completion:nil];
    }else{
        appdelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
        
        [UIView transitionWithView:appdelegate.window
                          duration:0.5
                           options:UIViewAnimationOptionPreferredFramesPerSecond60
                        animations:^{
                            
                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                            
                            MenuViewController *menuViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
                            UINavigationController *menuNavigationViewController = [[UINavigationController alloc] initWithRootViewController:menuViewController];
                            UIViewController *fontViewController = [[HomeViewController alloc] init];
                            
                            SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:menuNavigationViewController frontViewController:fontViewController];
                            
                            revealController.delegate = appdelegate;
                            revealController.rearViewRevealWidth = SCREEN_WIDTH;
                            revealController.rearViewRevealOverdraw = 0.0f;
                            revealController.rearViewRevealDisplacement = 0.0f;
                            appdelegate.viewController = revealController;
                            appdelegate.window.rootViewController = appdelegate.viewController;
                            [appdelegate.window makeKeyWindow];
                            
                            UIViewController *newFrontController = [[HomeViewController alloc] init];
                            UINavigationController *newNavigationViewController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                            [revealController pushFrontViewController:newNavigationViewController animated:YES];
                        }
                        completion:nil];
        
    }
}

-(void)navigationToPrivacyPolicyViewController
{
    SWRevealViewController *revealViewController = self.revealViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PrivacyPolicyViewController *privacyPolicyViewController = [storyboard instantiateViewControllerWithIdentifier:@"PrivacyPolicyViewController"];
    privacyPolicyViewController.isRecheckPolicy = YES;
    NSString *str = ((PolicyInfoItem *)[wsPolicy.data objectAtIndex:0]).textInfo;
    privacyPolicyViewController.policyText = str;
#ifdef DEBUG
    NSLog(@"return policy version: %@", ((PolicyInfoItem *)[wsPolicy.data objectAtIndex:0]).CurrentVersion);
#endif
    privacyPolicyViewController.policyVersion = ((PolicyInfoItem *)[wsPolicy.data objectAtIndex:0]).CurrentVersion;
    [revealViewController pushFrontViewController:privacyPolicyViewController animated:YES];
}

-(void)navigationToUDASignInViewController
{
    [UIView transitionWithView:appdelegate.window
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        UDASignInViewController *createProfileViewController = [storyboard instantiateViewControllerWithIdentifier:@"UDASignInViewController"];
                        UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:createProfileViewController];
                        appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
                        appdelegate.window.rootViewController = navigationController;
                    }
                    completion:^(BOOL a){
                        [appdelegate.window makeKeyAndVisible];
                    }];
    
}

-(void)navigationToCreateProfileViewController
{
    SWRevealViewController *revealViewController = self.revealViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CreateProfileViewController *createProfileViewController = [storyboard instantiateViewControllerWithIdentifier:@"CreateProfileViewController"];
    [revealViewController pushFrontViewController:createProfileViewController animated:YES];
}

- (void) crashApp{
    //[self performSelector:@selector(die_die)];
}

@end
