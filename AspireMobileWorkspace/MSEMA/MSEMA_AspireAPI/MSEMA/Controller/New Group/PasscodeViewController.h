//
//  PasscodeViewController.h
//  MSEMA
//
//  Created by HieuNguyen on 10/4/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "PasswordTextField.h"

@interface PasscodeViewController :BaseViewController{
    NSString *currentCardNumber;
}
    @property (weak, nonatomic) IBOutlet UIButton *btnBack;
    @property(weak, nonatomic) IBOutlet UILabel *titleLbl;
    @property(weak, nonatomic) IBOutlet PasswordTextField *passcodeTxtView;
    @property(weak, nonatomic) IBOutlet UIButton *submitBtn;
    @property(weak, nonatomic) IBOutlet UILabel *headerLbl;
    @property (weak, nonatomic) IBOutlet NSLayoutConstraint *submitBtnBottomConstraint;
    @property (assign,nonatomic) BOOL needInputNewPasscode;

    -(IBAction)submitPasscode:(id)sender;
    -(IBAction)setValidPasscode:(id)sender;
@end
