//
//  PasscodeViewController.m
//  MSEMA
//
//  Created by HieuNguyen on 10/4/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "PasscodeViewController.h"
#import "UIButton+Extension.h"
#import "EnumConstant.h"
#import "WSBase.h"
#import "SWRevealViewController.h"
#import "WSB2CGetUserDetails.h"
#import "AppDelegate.h"
#import "HomeViewController.h"
#import "MenuViewController.h"
#import "WSPreferences.h"
#import "PreferenceObject.h"
#import "CreateProfileViewController.h"
#import "AppData.h"
#import "ChangePasswordViewController.h"
@import AspireApiFramework;
#import "NSDictionary+SBJSONHelper.h"
#import "AppColor.h"
#import "UtilStyle.h"
#import "CheckEmailView.h"
#import "NSString+Utis.h"
#import "UITextField+Extensions.h"
#import "UDASignInViewController.h"

@import AspireApiControllers;

#define LIMITCHARACTERS     25

@interface PasscodeViewController ()<UITextFieldDelegate>
{
    CGFloat backupBottomConstraint;
    WSPreferences *wsPreferences;
    WSB2CGetUserDetails *wsGetUser;
    //WSB2CPasscodeVerfication *wsPasscodeVerification;
}
@end

@implementation PasscodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view from its nib.
    self.headerLbl.attributedText = [UtilStyle setTextStyleForTitleViewControllerWithMessage:NSLocalizedString(@"concierge", nil)];
    
    UITapGestureRecognizer *dismiss = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:dismiss];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWill:) name:UIKeyboardWillHideNotification object:nil];
    
    [self.submitBtn setTitle:NSLocalizedString(@"submit_button_title", nil) forState:UIControlStateNormal];
    
    [self.submitBtn setBackgroundColorForNormalStatus];
    [self.submitBtn setBackgroundColorForTouchingStatus];
    [self.submitBtn configureDecorationForButton];
    [self.submitBtn setEnabled:NO];
    
    [self.titleLbl setTextColor:[AppColor normalTextColor]];
    self.titleLbl.text = NSLocalizedString(@"passcode_title_msg", @"");
    
    self.passcodeTxtView.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.passcodeTxtView.text];
    
    self.passcodeTxtView.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:NSLocalizedString(@"passcode_holder_text_msg", @"")];
    [self.passcodeTxtView setDelegate:self];
    
    backupBottomConstraint = self.submitBtnBottomConstraint.constant;
    
    if (self.needInputNewPasscode) {
        [self.btnBack removeFromSuperview];
    }
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)submitPasscode:(id)sender{
    NSMutableString *message = [[NSMutableString alloc] init];
    [self.passcodeTxtView setBottomBolderDefaultColor];
    
    bool isValid = [self verifyValueForTextField:self.passcodeTxtView andMessageError:message];
    
    if (!isValid) {
        [self showAlertInvalidBin];
    }else{
        [self.passcodeTxtView endEditing:YES];
        
        CheckEmailView* v = [[CheckEmailView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        AACCheckEmailController* vc = [AACCheckEmailController new];
        vc.subview = v;
        v.controller = vc;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(BOOL)verifyValueForTextField:(UITextField *)textFied andMessageError:(NSMutableString *)message
{
    NSString *errorMsg = nil;
    BOOL isValid = NO;
    if (textFied == self.passcodeTxtView)
    {
        errorMsg = @"Enter a valid program passcode.";
        isValid = [textFied.text isValidBinNumber];
    }
    
    if(!isValid)
    {
        //(message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
        [message appendString:errorMsg];
//        [textFied setBottomBorderRedColor];
//        [textFied becomeFirstResponder];
    }
//    else
//    {
//        [textFied setBottomBolderDefaultColor];
//    }
    
    return isValid;
}



- (void)setValidPasscode:(id)sender
{
    
}

-(void)handleKeyboardWillShow:(NSNotification *)paramNotification
{
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         self.submitBtnBottomConstraint.constant = keyboardRect.size.height + MARGIN_KEYBOARD;
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

-(void)handleKeyboardWill:(NSNotification *)paramNotification
{
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         self.submitBtnBottomConstraint.constant =  backupBottomConstraint;
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.passcodeTxtView) {
        
        NSString *resultText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        (resultText.length > 0) ? [self.submitBtn setEnabled:YES] :  [self.submitBtn setEnabled:NO];
        
        return !([textField.text length] >= LIMITCHARACTERS && [string length] > range.length);
    }
    return YES;
}

- (void)dismissKeyboard{
    if([self.passcodeTxtView becomeFirstResponder])
    {
        [self.passcodeTxtView resignFirstResponder];
    }
}

- (void) showAlertInvalidBin {
    void(^gotoSignin)(void) = ^{
        [self stopActivityIndicator];
        if (self.navigationController ) {
            [self.navigationController popViewControllerAnimated:true];
            return;
        }
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [ModelAspireApiManager logout];
        //        [self navigationToUDASignInViewController];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UDASignInViewController *signinViewController = [storyboard instantiateViewControllerWithIdentifier:@"UDASignInViewController"];
        UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:signinViewController];
        AppDelegate *appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
        appdelegate.window.rootViewController = navigationController;
        [appdelegate.window makeKeyAndVisible];
    };
    
    [self stopActivityIndicator];
    [self showAlertWithTitle:NSLocalizedString(@"alert_error_title", nil)
                     message:NSLocalizedString(@"check_invalid_passcode_msg", nil)
                     buttons:@[NSLocalizedString(@"arlet_cancel_button", nil),NSLocalizedString(@"ok_button_title", nil)]
                     actions:@[gotoSignin,^{
        self.passcodeTxtView.text = @"";
        [self.passcodeTxtView becomeFirstResponder];
        [self.passcodeTxtView setBottomBorderRedColor];
        [self.submitBtn setEnabled:NO];
    },]
            messageAlignment:NSTextAlignmentCenter];
}
@end
