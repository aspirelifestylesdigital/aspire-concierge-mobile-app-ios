//
//  CategoryViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "CategoryViewController.h"
#import "CategoryItem.h"
#import "CategoryCollectionViewCell.h"
#import "Constant.h"
#import "AppData.h"
#import "SWRevealViewController.h"
#import "CustomPopTransition.h"
#import "SelectingCategoryDelegate.h"
#import "SubCategoryViewController.h"
#import "CategoryCityGuideViewController.h"
#import "Common.h"
#import "AlertViewController.h"

@interface CategoryViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{
    CGFloat verticalSpaceDistance;
    CGFloat horizontalSpaceDistance;
    CGPoint subViewCenter;
    
    
    IBOutlet UICollectionViewFlowLayout *flowLayoutCollection;
}

@end

@implementation CategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    flowLayoutCollection = [UICollectionViewFlowLayout new];
//    [self.collectionView setCollectionViewLayout:flowLayoutCollection];
    [self trackingScreenByName:@"Category list"];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //  Set up Pop pan gesture
    
    [self setUpCustomizedPanGesturePopRecognizer];
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}

-(void)initView
{
    //    self.navigationItem.title = NSLocalizedString(@"category_title", nil);
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"category_title", nil)];
    [self backNavigationItem];
    [self.collectionView registerNib:[UINib nibWithNibName:@"CategoryCollectionViewCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"CategoryCollectionViewCell"];
    
    //  Update view base on Device size
    [self.view setBackgroundColor:[UIColor whiteColor]];
//    self.topConstraint.constant = self.topConstraint.constant * SCREEN_SCALE;
//    self.bottomConstraint.constant = self.bottomConstraint.constant * SCREEN_SCALE;
//    self.leftConstraint.constant = self.leftConstraint.constant * SCREEN_SCALE;
//    self.rightConstraint.constant = self.rightConstraint.constant * SCREEN_SCALE;
    horizontalSpaceDistance = 9.0f;// * SCREEN_SCALE;
    verticalSpaceDistance = 9.0f;// * SCREEN_SCALE;
    
    
    // force main queue to get real size when use autolayout
//    dispatch_async(dispatch_get_main_queue(), ^{
//
//        [flowLayoutCollection setSectionInset:UIEdgeInsetsMake(0, 0, 0, 0)];
//        NSInteger width = round((self.collectionView.frame.size.width-(9*3))/3);
//        flowLayoutCollection.itemSize = CGSizeMake(width, width);
//    });
}

-(void)checkCityGuideEnable
{
    
}
-(void)initData
{
    self.categoryLst = [AppData getSelectionCategoryList];
    if(self.currentCity.subCategories.count == 0) {
        NSMutableArray* temp = [NSMutableArray arrayWithArray:self.categoryLst];
        [temp removeLastObject];
        self.categoryLst = temp;
    }
}

#pragma mark COLLECTION DELEGATE

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.categoryLst.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryCollectionViewCell *cell = (CategoryCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"CategoryCollectionViewCell" forIndexPath:indexPath];
    CategoryItem *item = [self.categoryLst objectAtIndex:indexPath.row];
    ;
    BOOL isDisableItem = (!(self.currentCity.subCategories.count > 0) && [item.ID isEqualToString:@"5"]);

    [cell initCellWithData:item withDisableItem:isDisableItem];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger width = round((self.collectionView.frame.size.width-(horizontalSpaceDistance*3))/3);
    return CGSizeMake(width, width);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return verticalSpaceDistance;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return horizontalSpaceDistance;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryCollectionViewCell *cell = (CategoryCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
    CategoryItem *item = [self.categoryLst objectAtIndex:indexPath.row];
    
    if (![item.code isEqualToString:@"city guide"]) {
        [self trackingEventByName:[item.categoryName capitalizedString] withAction:SelectActionType withCategory:CategorySelectionCategoryType];
    }
    
    if(![item.code isEqualToString:@"city guide"] || ([item.code isEqualToString:@"city guide"] && self.currentCity.subCategories.count > 0))
    {
        cell.name.textColor = colorFromHexString(@"#FF8C66");
        cell.name.shadowColor = [UIColor clearColor];
    }
    
    if([item.code isEqualToString:@"city guide"])
    {
        if(self.currentCity.subCategories.count > 0)
        {
            // wait for text-color change
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [NSThread sleepForTimeInterval:(0.1)];
                dispatch_async(dispatch_get_main_queue(), ^{
                    cell.name.shadowColor = [UIColor blackColor];
                    CategoryCityGuideViewController *cityGuideViewController = [[CategoryCityGuideViewController alloc] init];
                    cityGuideViewController.delegate = self.delegate;
                    cityGuideViewController.subCategories = self.currentCity.subCategories;
                    [self.navigationController pushViewController:cityGuideViewController animated:NO];
                    [cell setCellToDefault];
                });
            });
        }
    }
//    else  if([item.code isEqualToString:@"mastercard travel"])
//    {
//        [self showAlertForMoreInformationWithURL:[NSURL URLWithString:MASTERCARD_TRAVEL_URL] ForCell:cell];
//    }
//    else if([item.code isEqualToString:@"golf"])
//    {
//        [self showAlertForMoreInformationWithURL:[NSURL URLWithString:GOLF_URL] ForCell:cell];
//    }
//    else if([item.code isEqualToString:@"priceless city"])
//    {
//        [self showAlertForMoreInformationWithURL:[NSURL URLWithString:PRICELESS_URL] ForCell:cell];
//    }
    else
    {
        CategoryItem *itemForData = item;
        
        if([item.code isEqualToString:@"dining"])
        {
            itemForData.ID = self.currentCity.diningID;
        }
        
        if(self.delegate)
            [self.delegate Explore:self onSelectCategory:itemForData];
    }
    
}

#pragma mark LOGICAL FUNCTION
-(void) showAlertForMoreInformationWithURL:(NSURL*)url ForCell:(CategoryCollectionViewCell *)cell
{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"alert_title", nil);
    alert.msgAlert = NSLocalizedString(@"category_more_info_msg", nil);
    alert.secondBtnTitle = NSLocalizedString(@"yes_button", nil);
    alert.firstBtnTitle = NSLocalizedString(@"no_button", nil);
    alert.blockSecondBtnAction = ^(void){
        [[UIApplication sharedApplication] openURL:url];
        cell.name.textColor = [UIColor whiteColor];
        cell.name.shadowColor = [UIColor blackColor];
        [cell.maskView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.4]];
    };
    
    alert.blockFirstBtnAction  = ^(void){
        cell.name.textColor = [UIColor whiteColor];
        cell.name.shadowColor = [UIColor blackColor];
        [cell.maskView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.4]];
    };
    
    [self showAlert:alert forNavigation:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
