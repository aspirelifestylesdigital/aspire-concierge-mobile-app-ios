//
//  MyProfileViewController.m
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/13/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "MyProfileViewController.h"
#import "ImageUtilities.h"
#import "NSString+Utis.h"
#import "Constant.h"
#import "SWRevealViewController.h"
#import "UIButton+Extension.h"
#import "HomeViewController.h"

#import "AppDelegate.h"
#import "UserObject.h"
#import "UITextField+Extensions.h"
#import "UtilStyle.h"

#import "WSBase.h"
#import "WSB2CGetUserDetails.h"
#import "LocationComponent.h"
#import "WSPreferences.h"

@import AspireApiFramework;
#import "PreferenceObject.h"
#import "AppData.h"
#import "ChallengeQuestionObject.h"
#import "AppSize.h"
#import "AppColor.h"

@interface MyProfileViewController ()<DataLoadDelegate, DropDownViewDelegate> {
    WSPreferences *wsPreferences;
    WSB2CGetUserDetails *wsGetUser;
}

@end

@implementation MyProfileViewController
{
    NSString *firstNameOnTyping;
    NSString *lastNameOnTyping;
    NSString *emailOnTyping;
    NSString *phoneOnTyping;
    NSString *passwordOnTyping;
    NSString *confirmPasswordOnTyping;
    BOOL isTempUseLocation;
    IBOutlet NSLayoutConstraint *btSC;
    
    NSMutableArray *questionArray;
    NSMutableArray *questionTextArray;
    ChallengeQuestionObject *selectedChallengeQuestion;
    BOOL isFirstLoad;
    
    int indexQuestionFirstTime;
    int currentIndexQuestion;
}
- (void)viewDidLoad
{
    isTempUseLocation = [[SessionData shareSessiondata] isUseLocation];
    isNotAskConciergeBarButton = YES;
    self.isUpdateProfile = YES;
    isFirstLoad = true;
    [super viewDidLoad];
    [self backNavigationItem];
    [self setNavigationBarWithDefaultColorAndTitle:@"My Profile"];
    backupBottomConstraint = self.viewActionBottomConstraint.constant;
    [self setButtonStatus:NO];
    [self trackingScreenByName:@"My profile"];
    [self setUpData];
    [self getUserProfile];
    if([[SessionData shareSessiondata] isUseLocation]) {
        [[SessionData shareSessiondata] setIsShownLocationServiceAlert:NO];
        [LocationComponent startRequestLocation];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //  Set up Pop pan gesture
    //[self setUpCustomizedPanGesturePopRecognizer];
    [@[self.firstNameText,self.lastNameText,self.answerTextField,self.phoneNumberText] enumerateObjectsUsingBlock:^(UITextField*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(textFieldChanged:)
                                                     name:UITextFieldTextDidChangeNotification
                                                   object:obj];
    }];
}

-(void)textFieldChanged:(NSNotification*)notification {
    UITextField* txt = (UITextField*)[notification object];
    [txt setOriginText:txt.text];
    if (txt == self.firstNameText) {
        firstNameOnTyping = txt.text;
    } else if (txt == self.phoneNumberText) {
        phoneOnTyping = txt.text;
    } else if (txt == self.lastNameText) {
        lastNameOnTyping = txt.text;
    }
    [self setButtonStatus:([self checkFieldIsChangeString] || [self.answerTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0)];
    if (self.answerTextField.text.length == 0 ||
        [self.answerTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 3) {
        [self.answerTextField setBottomBolderDefaultColor];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    //  Set up Pop pan gesture
    [self setUpCustomizedPanGesturePopRecognizer];
    
    if(self.navigationController.viewControllers.count == 1)
    {
        HomeViewController *homeView = [[HomeViewController alloc] init];
        [self.navigationController setViewControllers:@[homeView,self]];
    }
    
    self.submitButton.hidden = YES;
    //self.cancelButton.hidden = YES;
    self.commitmentLabel.hidden = YES;
    
    [self setTextViewsDefaultBottomBolder];
    [self.view layoutIfNeeded];
    
//    [self setDefaultData];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (isTempUseLocation != isUseLocation) {
        [[SessionData shareSessiondata] setIsUseLocation:isTempUseLocation];
    }
    [@[self.firstNameText,self.lastNameText,self.answerTextField,self.phoneNumberText] enumerateObjectsUsingBlock:^(UITextField*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:obj];
    }];
}
-(void)viewDidDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}

-(void) setUpData {
    questionArray = [[NSMutableArray alloc] init];
    questionTextArray = [[NSMutableArray alloc] init];
    
    
    id allKeys = [ModelAspireApiManager getSecurityQuetions];
    
    for (int i=0; i<[allKeys count]; i++) {
        NSDictionary *arrayResult = [allKeys objectAtIndex:i];
        ChallengeQuestionObject *object = [[ChallengeQuestionObject alloc] initFromDict:arrayResult];
        [questionArray addObject:object];
        [questionTextArray addObject:object.questionText];
    }
    [self.questionDropdown setItemDisplay:4];
    self.questionDropdown.listItems = questionTextArray;
}

-(void)getUserProfile {
    [self startActivityIndicator];
    __weak typeof (self) _self = self;
    [ModelAspireApiManager retrieveProfileCurrentUser:^(User *user, NSError *error) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error) {
                [_self setDefaultData];
                
                if ([User isValid]) {
                    UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
                    
                    [[SessionData shareSessiondata] setUserObject:userObject];
                    
                    NSString *salutation = userObject.salutation;
                    if (salutation.length > 1) {
                        _self.salutationDropDown.titleColor = colorFromHexString(TEXTFIELD_TEXT_COLOR_STANDARD);
                        _self.salutationDropDown.title = salutation;
                    }
                    NSString *tempPhone = userObject.mobileNumber;
                    _self.firstNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:userObject.firstName];
                    _self.lastNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:userObject.lastName];
                    _self.emailText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:userObject.email];
                    _self.phoneNumberText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:(tempPhone.length < 19) && ![tempPhone containsString:@"+"] ? [NSString stringWithFormat:@"+%@",tempPhone] : tempPhone];
                    
                    isUseLocation = [[SessionData shareSessiondata] isUseLocation];
                    if ([User current].locations.count > 0)
                        isUseLocation = userObject.isUseLocation;
                    isTempUseLocation = isUseLocation;
                    [self setImageLocation:isUseLocation];
                    
                    currentFirstName = userObject.firstName;
                    currentEmail = userObject.email;
                    currentPhone = (tempPhone.length < 19) && ![tempPhone containsString:@"+"] ? [NSString stringWithFormat:@"+%@",tempPhone] : tempPhone;
                    currentLastName = userObject.lastName;
                    
                    _self.firstNameText.text = [currentFirstName createMaskForText:NO];
                    _self.emailText.text = [currentEmail createMaskForText:YES];
                    _self.lastNameText.text = [currentLastName createMaskForText:NO];
                    _self.phoneNumberText.text = (currentPhone.length > 3) ? [currentPhone createMaskStringBeforeNumberCharacter:3]:currentPhone;
                    _self.emailText.enabled = NO;
                    
                    // get sercure question
                    [ModelAspireApiManager getProfileFromEmail:[[SessionData shareSessiondata] UserObject].email completion:^(OKTAUserProfile * _Nullable OKTAuser, User * _Nullable user, NSError *error) {
                        [_self stopActivityIndicator];
                        if (!_self || !OKTAuser) return;
                        [_self stopActivityIndicator];
                        typeof(self) __self = _self;
                        int i = 0;
                        for (ChallengeQuestionObject* obj in __self->questionArray) {
                            if ([OKTAuser.credentials.recoveryQuestion.question isEqualToString:obj.questionText]) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [__self->_questionDropdown setSelectedIndex:i];
                                });
                                break;
                            }
                            i++;
                        }
                    }];
                }
            }else{
                [_self stopActivityIndicator];
                if(error.code == ERR_USER_TOKEN){
                    [_self handleInvalidCredentials];
                }else{
                    [_self showApiErrorWithMessage:@""];
                }
            }
        });
    }];
}

- (void)getPreference{
    wsPreferences = [[WSPreferences alloc] init];
    wsPreferences.delegate = self;
    [wsPreferences getPreference];
}

- (void)setDefaultData {
    UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
    if (userObject) {
        firstNameOnTyping = userObject.firstName;
        lastNameOnTyping = userObject.lastName;
        emailOnTyping = userObject.email;
        NSString *tempPhone = userObject.mobileNumber;
        phoneOnTyping = (tempPhone.length < 19) && ![tempPhone containsString:@"+"] ? [NSString stringWithFormat:@"+%@",tempPhone] : tempPhone;
        //passwordOnTyping = [profileDictionary objectForKey:keyPassword];
        
        NSString *salutation = userObject.salutation;
        if (salutation.length > 1) {
            self.salutationDropDown.titleColor = colorFromHexString(TEXTFIELD_TEXT_COLOR_STANDARD);
            self.salutationDropDown.title = salutation;
        }
        self.firstNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:userObject.firstName];
        self.lastNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:userObject.lastName];
        self.emailText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:userObject.email];
        self.phoneNumberText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:userObject.mobileNumber];
        
        isUseLocation = [[SessionData shareSessiondata] isUseLocation];
        [self setImageLocation:isUseLocation];
        
        currentFirstName = userObject.firstName;
        currentEmail = userObject.email ;
        currentPhone = (tempPhone.length < 19) && ![tempPhone containsString:@"+"] ? [NSString stringWithFormat:@"+%@",tempPhone] : tempPhone;
        currentLastName = userObject.lastName;
        currentAnswer = @"";
        
        self.firstNameText.text = [currentFirstName createMaskForText:NO];
        self.emailText.text = [currentEmail createMaskForText:YES];
        self.lastNameText.text = [currentLastName createMaskForText:NO];
        self.phoneNumberText.text = [currentPhone createMaskForText:NO];
        self.emailText.enabled = NO;
        [self.answerTextField setText:@"*******"];

    }
}

- (void)initView{
    self.phoneNumberText.tag = PHONE_NUMBER_TEXTFIELD_TAG;
    [self setButtonFrame];
    
    self.emailText.textColor = colorFromHexString(TEXTFIELD_PLACEHOLDER_COLOR_STANDARD);
    
    [self.updateButton setBackgroundColorForNormalStatus];
    [self.updateButton setBackgroundColorForTouchingStatus];
    [self.updateButton configureDecorationForButton];
    
    [self.cancelButtonMyProfile setBackgroundColorForNormalStatus];
    [self.cancelButtonMyProfile setBackgroundColorForTouchingStatus];
    [self.cancelButtonMyProfile configureDecorationForButton];
    
    // Text Style For Greet Message
    NSString *message = NSLocalizedString(@"commitment_my_profile_message", nil);
    self.commitmentLabel.attributedText = [UtilStyle setLargeSizeStyleForLabelWithMessage:message];
    
    [self setCheckBoxState:isCheck];
    self.commitmentLabel.text = NSLocalizedString(@"commitment_my_profile_message", nil);
    [self.updateButton setTitle:NSLocalizedString(@"update_button", nil) forState:UIControlStateNormal];
    [self.cancelButtonMyProfile setTitle:NSLocalizedString(@"cancel_button", nil) forState:UIControlStateNormal];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:BUTTON_FONT_NAME_STANDARD size:(BUTTON_FONT_SIZE_STANDARD * SCREEN_SCALE)],
                                 NSKernAttributeName: @1.4};
    self.updateButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.updateButton.titleLabel.text attributes:attributes];
    self.cancelButtonMyProfile.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.cancelButtonMyProfile.titleLabel.text attributes:attributes];
    
    [self.updateButtonView setBackgroundColor:colorFromHexString(DEFAULT_BACKGROUND_COLOR)];
    [self.cancelButtonView setBackgroundColor:colorFromHexString(DEFAULT_BACKGROUND_COLOR)];
    
    self.questionDropdown.leadingTitle = 3.0f;
    self.questionDropdown.titleFontSize = [AppSize titleTextSize];
    self.questionDropdown.title = @"Security Question";
    self.questionDropdown.titleColor = [AppColor placeholderTextColor];
    self.questionDropdown.isBreakLine = true;
    self.questionDropdown.delegate = self;
    self.questionDropdown.itemsFont = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.questionDropdown.titleLabel.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    [self.questionDropdown setItemHeight:50];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.answerTextField setBottomBolderDefaultColor];
        [self.questionDropdown setBottomBolderDefaultColor];
    });
}

- (void) setButtonFrame{
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)handleKeyboardWillHide:(NSNotification *)paramNotification {
    btSC.constant = 80;
//    self.viewActionBottomConstraint.constant = backupBottomConstraint;
}

- (void)handleKeyboardWillShow:(NSNotification *)paramNotification {
    
    [self.salutationDropDown didTapBackground];
    [self.questionDropdown didTapBackground];
    
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    keyboardHeight = keyboardRect.size.height;
    btSC.constant =  keyboardHeight + 80;
//    self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight + 250, 0);
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

-(void) createAsteriskForTextField:(UITextField *)textField{
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [textField setLeftView:leftView];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    
    asteriskImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"asterisk_icon"]];
    
    asteriskImage.frame = CGRectMake(0.0f, 0.0f, 20.0f, 20.0f);
    asteriskImage.contentMode = UIViewContentModeCenter;
    [textField setRightView:asteriskImage];
    [textField setRightViewMode:UITextFieldViewModeAlways];
}

- (void)changeUseLocation{
    [self setButtonStatus:[self checkFieldIsChangeString]];
}

- (void)updateSuccessRedirect{    
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert =nil;
    alert.msgAlert = NSLocalizedString(@"update_profile_success_message", nil);
    alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
    dispatch_async(dispatch_get_main_queue(), ^{
        alert.seconBtn.hidden = YES;
        alert.midView.alpha = 0.0f;;
        [alert.view layoutIfNeeded];
    });
    
    alert.blockFirstBtnAction = ^(void){
        isTempUseLocation = isUseLocation;
        [self getUserInfo];
        [self setTextViewsDefaultBottomBolder];
        [self setButtonStatus:NO];
    };
    
    [self showAlert:alert forNavigation:NO];
}


#pragma mark API PROCESS
- (void)updateProfileWithAPI{
    [self startActivityIndicator];
    
    UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
    NSMutableDictionary *dictDai = [[NSMutableDictionary alloc] init];
    [dictDai setValue:(self.salutationDropDown.title.length > 1) ? self.salutationDropDown.title : @" " forKey:@"Salutation"];
    [dictDai setValue:currentFirstName forKey:@"FirstName"];
    [dictDai setValue:currentLastName forKey:@"LastName"];
    [dictDai setValue:[currentPhone stringByReplacingOccurrencesOfString:@"+" withString:@""] forKey:@"MobileNumber"];
    [dictDai setValue:currentEmail forKey:@"Email"];
    [dictDai setValue:currentPassword forKey:@"Password"];
//    [dictDai setValue:[[SessionData shareSessiondata] passcode] forKey:@"Other Preferences"];
    [dictDai setValue:isUseLocation? @"ON": @"OFF" forKey:APP_USER_PREFERENCE_Location];
    [dictDai setValue:@"USA" forKey:@"Country"];
    [dictDai setValue:APP_NAME forKey:@"referenceName"];
    [dictDai setValue:[self getPreferenceByType:DiningPreferenceType].value forKey:@"Dining Preference"];
    [dictDai setValue:[self getPreferenceByType:HotelPreferenceType].value forKey:@"Hotel Preference"];
    [dictDai setValue:[self getPreferenceByType:TransportationPreferenceType].value forKey:@"Car Type Preference"];
    if (userObject.selectedCity) {
        [dictDai setValue:userObject.selectedCity forKey:APP_USER_PREFERENCE_Selected_City];
    }
    [dictDai setValue:@"Unknown" forKey:@"City"];
    
    __block NSString*answer = currentAnswer;
    
    typeof(self) __weak _self = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self startActivityIndicator];
    });
    
    __block BOOL isChangeString = [self checkFieldIsChangeString];
    __block BOOL isChangeAnswer = [currentAnswer stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0;
    __block NSError* errorTotal = nil;
    
    dispatch_queue_t queue = dispatch_queue_create("com.s3corp.update", DISPATCH_QUEUE_SERIAL);
    
    // update profile
    dispatch_async(queue, ^{
        if (isChangeString) {
            dispatch_suspend(queue);
            [ModelAspireApiManager updateProfileCurrentUserWithUserInfo:dictDai completion:^(NSError *error) {
                errorTotal = error;
                dispatch_resume(queue);
            }];
        }
    });
    
    // change answer
    dispatch_async(queue, ^{
        if(errorTotal == nil && isChangeAnswer) {
            dispatch_suspend(queue);
            [ModelAspireApiManager changeRecoveryQuestionForEmail:[[SessionData shareSessiondata] UserObject].email password:[[AAFPassword alloc] initWithPassword:[AccountAuthentication password]] question:[[AAFRecoveryQuestion alloc] initWithQuestion:selectedChallengeQuestion.questionText answer:answer] completion:^(NSError * _Nullable error) {
                errorTotal = error;
                dispatch_resume(queue);
            }];
        }
    });
    
    // result
    dispatch_barrier_async(queue, ^{
        typeof(self) __self = _self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [_self stopActivityIndicator];
            if (!errorTotal) {
                __self.answerTextField.text = @"*******";
                currentAnswer = @"";
                indexQuestionFirstTime = currentIndexQuestion;
                
                [[SessionData shareSessiondata] setUserObject:(UserObject *)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([UserObject class])]];
                [_self updateSuccessRedirect];
                return;
            }
            
            NSString* message = @"";
            switch ([AspireApiError getErrorTypeAPIUpdateProfileFromError:errorTotal]) {
                case NETWORK_UNAVAILABLE_ERR:{
                    [_self showErrorNetwork];
                }
                    break;
                case UNKNOWN_ERR:
                    message = NSLocalizedString(@"error_api_message", nil);
                    break;
                    
                default:
                    message = NSLocalizedString(@"error_api_message", nil);
                    break;
            }
            
            switch ([AspireApiError getErrorTypeAPIChangeRecoveryQuestionOKTAFromError:errorTotal]) {
                case NETWORK_UNAVAILABLE_ERR:{
                    [_self showErrorNetwork];
                }
                    break;
                case UNKNOWN_ERR:
                    message = NSLocalizedString(@"error_api_message", nil);
                    break;
                case ANSWER_ERR:
                    message = NSLocalizedString(@"input_invalid_answer_msg", nil);
                    [self.answerTextField setBottomBorderRedColor];
                    break;
                case EMAIL_INVALID_ERR:
                    message = NSLocalizedString(@"error_api_message", nil);
                    break;
                case CHANGE_RECOVERY_QUESTION_ERR:
                    message = NSLocalizedString(@"error_api_message", nil);
                    break;
                default:
                    message = NSLocalizedString(@"error_api_message", nil);
                    break;
            }
            
            if (message.length == 0) return;
            AlertViewController *alert = [[AlertViewController alloc] init];
            alert.titleAlert = NSLocalizedString(@"confirm_value_title", nil);
            alert.msgAlert = message;
            alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
            alert.lblAlertMessage.textAlignment = NSTextAlignmentCenter;
            dispatch_async(dispatch_get_main_queue(), ^{
                alert.seconBtn.hidden = YES;
                alert.midView.alpha = 0.0f;;
                [alert.view layoutIfNeeded];
            });
            
            alert.blockFirstBtnAction = ^(void){
                [self.answerTextField becomeFirstResponder];
            };
            
//            alert.seconBtn.hidden = YES;
//            alert.midView.hidden = YES;
//
//            [alert.view layoutIfNeeded];
            [_self showAlert:alert forNavigation:NO];
        });
    });
    
//    [ModelAspireApiManager updateProfileCurrentUserWithUserInfo:dictDai completion:^(NSError *error) {
//        [_self stopActivityIndicator];
//        dispatch_sync(dispatch_get_main_queue(), ^{
//            if (!error) {
//
//                [_self updateSuccessRedirect];
//            }else{
//                if(error.code == ERR_USER_TOKEN){
//                    [_self handleInvalidCredentials];
//                }else{
//                    [_self showApiErrorWithMessage:@""];
//                }
//            }
//        });
//    }];
}


- (IBAction)touchUpdate:(id)sender {
    [self.view endEditing:YES];
    [self verifyAccountData:YES];
}

- (void)dismissKeyboard
{
    [super dismissKeyboard];
    [self.questionDropdown didTapBackground];
}

- (IBAction)touchCancel:(id)sender {
    [self.phoneNumberText endEditing:YES];
    [self dismissKeyboard];
    [self setDefaultData];
    [self setTextViewsDefaultBottomBolder];
    [self setButtonStatus:NO];
    [[SessionData shareSessiondata] setIsUseLocation:isTempUseLocation];
    [self getUserInfo];
    
    currentAnswer = @"";
    self.answerTextField.text = @"*******";
    [self.questionDropdown setSelectedIndex:indexQuestionFirstTime];
    selectedChallengeQuestion = [questionArray objectAtIndex:indexQuestionFirstTime];
}

- (void)updateProfileButtonStatus{
    [self setButtonStatus:[self checkFieldIsChangeString]];
}

- (void)checkTextFieldIsChange:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *resultText = [textField.text stringByReplacingCharactersInRange:range
                                                                   withString:string];
    
    if (textField == self.firstNameText) {
        firstNameOnTyping = resultText;
    }else if (textField == self.lastNameText){
        lastNameOnTyping = resultText;
    }else if (textField == self.emailText){
        emailOnTyping = resultText;
    }else if (textField == self.phoneNumberText){
        resultText = [resultText stringByReplacingOccurrencesOfString:@"+" withString:@""];
        phoneOnTyping = resultText;
    }else if (textField == self.passwordText){
        passwordOnTyping = resultText;
    }
    [self setButtonStatus:[self checkFieldIsChangeString]];
}

- (void)changeValueDropDown{
    [self setButtonStatus:([self checkFieldIsChangeString] || [self.answerTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0)];
    
}

- (BOOL)checkFieldIsChangeString{

    if (isUseLocation != isTempUseLocation) {
        return YES;
    }
    if (firstNameOnTyping.length == 0 && lastNameOnTyping.length == 0 && emailOnTyping.length == 0 && phoneOnTyping.length == 0 && self.salutationDropDown.valueStatus == DefaultValueStatus) {
        return NO;
    }
    UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
    if (![self.salutationDropDown.title isEqualToString:userObject.salutation]) {
        return YES;
    }else if (![userObject.firstName isEqualToString:firstNameOnTyping]) {
        return YES;
    }else if (![userObject.lastName isEqualToString:lastNameOnTyping]){
        return YES;
    }
    else if (![userObject.email isEqualToString:emailOnTyping]){
        return YES;
    }
    else if (![userObject.mobileNumber isEqualToString:[phoneOnTyping stringByReplacingOccurrencesOfString:@"+" withString:@""]]){
        return YES;
    }
    return NO;
}

- (void)setButtonStatus:(BOOL)status{
    self.updateButton.enabled = status;
    self.cancelButtonMyProfile.enabled = status;
}
#pragma mark - API Delegate

-(void)loadDataDoneFrom:(WSBase*)ws
{
    if (ws.task == WS_CREATE_USER)
    {
        [self addPreferences];
    }
    else if(ws.task == WS_UPDATE_MY_PREFERENCE){
        [self stopActivityIndicator];
        [self updateSuccessRedirect];
    }
    else if(ws.task == WS_GET_MY_PREFERENCE)
    {
        wsGetUser = [[WSB2CGetUserDetails alloc] init];
        wsGetUser.delegate = self;
        [wsGetUser getUserDetails];
    }
    else if(ws.task == WS_B2C_GET_USER_DETAILS)
    {
        [self setDefaultData];
        
        
        UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
        if (userObject != nil) {
            
            NSString *salutation = userObject.salutation;
            if (salutation.length > 1) {
                self.salutationDropDown.titleColor = colorFromHexString(@"#011627");
                self.salutationDropDown.title = salutation;
            }
            NSString *tempPhone = userObject.mobileNumber;
            self.firstNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:userObject.firstName];
            self.lastNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:userObject.lastName];
            self.emailText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:userObject.email];
            self.phoneNumberText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:(tempPhone.length >= 15) && ![tempPhone containsString:@"+"] ?tempPhone:[NSString stringWithFormat:@"+%@",tempPhone]];
            
            isUseLocation = [[SessionData shareSessiondata] isUseLocation];
            isTempUseLocation = isUseLocation;
            [self setImageLocation:isUseLocation];
            
            currentFirstName = userObject.firstName;
            currentEmail = userObject.email;
            currentPhone = (tempPhone.length < 19) && ![tempPhone containsString:@"+"] ? [NSString stringWithFormat:@"+%@",tempPhone] : tempPhone;
            currentLastName = userObject.lastName;
            
            self.firstNameText.text = [currentFirstName createMaskForText:NO];
            self.emailText.text = [currentEmail createMaskForText:YES];
            self.lastNameText.text = [currentLastName createMaskForText:NO];
            self.phoneNumberText.text = (currentPhone.length > 3) ? [currentPhone createMaskStringBeforeNumberCharacter:3]:currentPhone;
            self.emailText.enabled = NO;
            
        }
        [self stopActivityIndicator];
    }
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode {
    
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }
}

-(void)loadDataFailFrom:(WSBase *)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message
{
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }
}

- (void)WSBaseNetworkUnavailable{
    [self stopActivityIndicator];
}

#pragma mark - Dropdown delegate
- (void)didSelectItem:(DropDownView *)dropDownView atIndex:(int)index {
    currentIndexQuestion = index;
    
    if (![dropDownView isEqual:self.questionDropdown]) {
        [self changeValueDropDown];
        return;
    }
    
    if (isFirstLoad) {
        isFirstLoad = false;
        indexQuestionFirstTime = index;
    }else{
        currentAnswer = @"";
        if (index != indexQuestionFirstTime) {
            self.answerTextField.text = @"";
        }else{
            self.answerTextField.text = @"*******";
        }
        
    }
    
    selectedChallengeQuestion = [questionArray objectAtIndex:index];
    if ([dropDownView.titleLabel.text isEqualToString:@"Security Question"]) {
        dropDownView.titleColor = [AppColor placeholderTextColor];
    } else {
        dropDownView.titleColor = [AppColor textColor];
    }
    
    [self updateProfileButtonStatus];
}

- (void)handleMaskAnswer
{
    if (indexQuestionFirstTime == currentIndexQuestion) {
        [self.answerTextField setText:@"*******"];
    }
}

-(void)didShow:(DropDownView *)dropDownView {
    [self.view endEditing:true];
}

@end
