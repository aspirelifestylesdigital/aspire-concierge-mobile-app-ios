//
//  UDASignInViewController.m
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 7/14/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "UDASignInViewController.h"
#import "SWRevealViewController.h"
#import "CreateProfileViewController.h"
#import "UDAForgotPasswordViewController.h"
#import "HomeViewController.h"
#import "WSSignIn.h"
#import "WSB2CGetAccessToken.h"
#import "WSB2CGetUserDetails.h"
#import "WSB2CGetRequestToken.h"
#import "UserRegistrationItem.h"
#import "UtilStyle.h"
#import "UIButton+Extension.h"
#import "NSString+Utis.h"
#import "UITextField+Extensions.h"
#import "AppDelegate.h"
#import "MenuViewController.h"
#import "ChangePasswordViewController.h"
#import "NSString+AESCrypt.h"
#import "AppData.h"
#import "SessionData.h"

#import "PreferenceObject.h"
//#import "WSB2CPasscodeVerfication.h"
//#import "PasscodeViewController.h"
#import "MenuViewController.h"
#import "WSPreferences.h"
@import AspireApiFramework;
@import AspireApiControllers;
#import "ForgotPasswordView.h"
#import "PasscodeViewController.h"
#import "StoreUserDefaults.h"


@interface UDASignInViewController () <DataLoadDelegate, UITextFieldDelegate>{
    
    BOOL isInputEmail, isInputPassword;
    WSB2CGetAccessToken* wsB2CGetAccessToek;
    WSB2CGetRequestToken* wsB2CGetRequestToken;
    WSB2CGetUserDetails* wsGetUser;
    WSPreferences *wsPreferences;
//    WSB2CPasscodeVerfication *wsPasscodeVerification;

    
    AppDelegate* appdelegate;
}

@property (nonatomic, strong) WSSignIn *wsSignIn;

@end

@implementation UDASignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *tappedOutsideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tappedOutsideKeyboard];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldChanged:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:self.passwordTextField];
    
    [self setUIStyte];
    [self trackingScreenByName:@"Sign in"];
    
    if (self.shouldGotoForgotPasswordImmediately)
        [self ForgetPasswordAction:nil];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setTextViewsDefaultBottomBolder];
//    self.emailTextField.text = @"";
//    self.passwordTextField.text = @"";
    
//    isInputEmail = self.emailTextField.text.length > 0;
//    isInputPassword = self.passwordTextField.text.length > 0;
    
    if ([StoreUserDefaults email] != nil) {
        [self.emailTextField setText:[StoreUserDefaults email]];
        isInputEmail = TRUE;
        [StoreUserDefaults saveEmail:nil];
    }
    
    [self setStatusSignInButton];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setTextViewsDefaultBottomBolder {
     dispatch_async(dispatch_get_main_queue(), ^{
         [self.emailTextField setBottomBolderDefaultColor];
         [self.passwordTextField setBottomBolderDefaultColor];
     });
}

-(void)resignFirstResponderForAllTextField{
    if(self.emailTextField.isFirstResponder){
        [self.emailTextField resignFirstResponder];
    }
    else if(self.passwordTextField.isFirstResponder){
        [self.passwordTextField resignFirstResponder];
    }
    
}

-(void)dismissKeyboard
{
    [self resignFirstResponderForAllTextField];
}

-(void)verifyData{
    NSString *errorAll = @"All fields are required.";
    NSMutableString *message = [[NSMutableString alloc] init];
    if (self.emailTextField.text.length == 0 && self.passwordTextField.text.length == 0) {
        [message appendString:@"* "];
        [message appendString:errorAll];
        [self.emailTextField setBottomBorderRedColor];
        [self.passwordTextField setBottomBorderRedColor];
        
    }else{
        if(self.emailTextField.text.length == 0){
            [message appendString:@"* "];
            [message appendString:[NSString stringWithFormat:@"%@ \n",errorAll]];
            [self.emailTextField setBottomBorderRedColor];
        }
        if (self.passwordTextField.text.length == 0){
            [message appendString:@"* "];
            [message appendString:[NSString stringWithFormat:@"%@ \n",errorAll]];
            [self.passwordTextField setBottomBorderRedColor];
        }
        if([self verifyValueForTextField:self.emailTextField].length > 0) {
            [message appendString:@"* "];
            [message appendString:[self verifyValueForTextField:self.emailTextField]];
            [message appendString:@"\n"];
        }
        if([self verifyValueForTextField:self.passwordTextField].length > 0) {
            [message appendString:@"* "];
            [message appendString:[self verifyValueForTextField:self.passwordTextField]];
            [message appendString:@"\n"];
        }
    }
    
    [self showError:message];
}

-(void)showError:(NSString *)message
{
    if(message.length > 0){
        
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = @"Please confirm that the value entered is correct:";
        alert.msgAlert = message;
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;
            alert.lblAlertMessage.textAlignment = NSTextAlignmentLeft;
            [alert.view layoutIfNeeded];
        });
        
        alert.blockFirstBtnAction = ^(void){
            [self makeBecomeFirstResponderForTextField];
        };
        
        [self showAlert:alert forNavigation:NO];
    }
    else{
        [self resignFirstResponderForAllTextField];
        [self setTextViewsDefaultBottomBolder];
        [self signInWithEmail:self.emailTextField.text withPassword:self.passwordTextField.text];
    }
}

-(void) makeBecomeFirstResponderForTextField
{
    [self resignFirstResponderForAllTextField];
    if(![self.emailTextField.text isValidEmail]){
        [self.emailTextField becomeFirstResponder];
    }
    else if(![self.passwordTextField.text isValidWeakPassword]){//isValidWeakPassword
        [self.passwordTextField becomeFirstResponder];
    }
}

-(NSString*)verifyValueForTextField:(UITextField *)textFied{
    
    NSString *errorMsg = @"";
    if(textFied == self.emailTextField && ![(textFied.isFirstResponder ? textFied.text :self.emailTextField.text) isValidEmail]){
        errorMsg = NSLocalizedString(@"input_invalid_email_msg", nil);
        [textFied setBottomBorderRedColor];
    }
    else if(textFied == self.passwordTextField && ![(textFied.isFirstResponder ? textFied.text :self.passwordTextField.text) isValidWeakPassword]){ //isValidWeakPassword
        errorMsg = NSLocalizedString(@"input_invalid_weak_password_msg", nil);
        [textFied setBottomBorderRedColor];
    }
    else{
        [textFied setBottomBolderDefaultColor];
    }
    
    return errorMsg;
}
- (void)WSBaseNetworkUnavailable{
    [self stopActivityIndicator];
}

- (void)signInWithEmail:(NSString*)email withPassword:(NSString*)password {
    [self startActivityIndicator];
    __weak typeof(self) _self = self;
    [ModelAspireApiManager loginWithUsername:email password:password completion:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            if (!error) {
                UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
                [[SessionData shareSessiondata] setUserObject:userObject];
                
                [_self stopActivityIndicator];
                [_self updateSuccessRedirect];
                
                
                
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_self stopActivityIndicator];
                    NSString* message = @"";
                    switch ([AspireApiError getErrorTypeAPISignInFromError:error]) {
                        case SIGNIN_INVALID_USER_ERR:
                            message = @"We're sorry. Your username or password do not match what we have in our system. Click OK to try again.";
                            break;
                        case OKTA_LOCKED_ERR:
                            message = @"We're sorry. Please contact us at 877-288-6503 for assistance accessing this account.";
                            break;
                        case UNKNOWN_ERR:
                            break;
                        default:
                            break;
                    }
                    
                    switch ([AspireApiError getErrorTypeAPIGetProfileOKTAFromError:error]) {
                        case OKTA_PROFILE_EXIST_ERR:
                            message = @"We're sorry. Your username or password do not match what we have in our system. Click OK to try again.";
                            break;
                        case NETWORK_UNAVAILABLE_ERR:
                            [_self showErrorNetwork];
                            return;
                            break;
                        default:
                            break;
                    }
                    
                    [_self handleAPIErrorWithCode:message];
                });
            }
        });
    }];

    
//    NSMutableDictionary *userDict = [[NSMutableDictionary alloc] init];
//    [userDict setValue:B2C_ConsumerKey forKey:@"ConsumerKey"];
//    [userDict setValue:@"Login" forKey:@"Functionality"];
//    [userDict setValue:email forKey:@"Email"];
//    [userDict setValue:[password AES256EncryptWithKey:EncryptKey] forKey:[keyCurrentSecret AES256DecryptWithKey:EncryptKey]];
//    [userDict setValue:B2C_DeviceId forKey:@"MemberDeviceID"];
//    self.wsSignIn = [[WSSignIn alloc] init];
//    self.wsSignIn.delegate = self;
//    [self.wsSignIn signIn:userDict];
    
}


- (void) handleAPIErrorWithCode:(NSString*)errorCode{
    [self.emailTextField setBottomBorderRedColor];
    [self.passwordTextField setBottomBorderRedColor];
    if([errorCode isEqualToString:NSLocalizedString(@"no_network_connection", nil)])
    {
        [self showApiErrorWithMessage:NSLocalizedString(@"no_network_connection", nil)];
    }else if (![errorCode isEqualToString:@""]) {
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);
        alert.msgAlert = errorCode;
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        alert.blockFirstBtnAction = ^{
            [self makeBecomeFirstResponderForTextField];
        };
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;
        });
        
        [self showAlert:alert forNavigation:NO];
        [self.emailTextField setBottomBorderRedColor];
        [self.passwordTextField setBottomBorderRedColor];
    }else{
        [self showApiErrorWithMessage:@""];
    }
    
}

- (void) setUIStyte {
    
    isInputEmail = isInputPassword = NO;
    self.emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    self.passwordTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    self.passwordTextField.secureTextEntry = YES;
    self.emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    self.emailTextField.delegate = self;
    self.passwordTextField.delegate = self;
    
    [self createAsteriskForTextField:self.emailTextField];
    [self createAsteriskForTextField:self.passwordTextField];
    
    self.emailTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.emailTextField.text];
    self.passwordTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.passwordTextField.text];
    self.emailTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.emailTextField.placeholder];
    self.passwordTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.passwordTextField.placeholder];
    
    [self setStatusSignInButton];
    
    [self.backgroundSignInView setBackgroundColor:colorFromHexString(DEFAULT_BACKGROUND_COLOR)];
    
    [self.signInButton setBackgroundColorForNormalStatus];
    [self.signInButton setBackgroundColorForTouchingStatus];
    [self.signInButton configureDecorationForButton];
    
    [self.forgotButton configureDecorationForButton];
    [self.signUpButton configureDecorationForButton];
}

- (void)setStatusSignInButton{
    self.signInButton.enabled = (isInputEmail && isInputPassword);
}

-(void) createAsteriskForTextField:(UITextField *)textField
{
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 8, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [textField setLeftView:leftView];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    
}
#pragma mark TEXT FIELD DELEGATE
-(void)textFieldChanged:(NSNotification*)notification {
    isInputPassword = self.passwordTextField.text.length > 0;
    [self setStatusSignInButton];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
    NSString *inputString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (textField == self.passwordTextField) {
        if ([string isEqualToString:@" "]) {
            return NO;
        }
        isInputPassword = (inputString.length > 0);
        if (textField.text.length >= 25 && range.length == 0){
            return NO;
        }
    }else if (textField == self.emailTextField){
        inputString = [inputString removeRedudantWhiteSpaceInText];
        isInputEmail = inputString.length > 0;
    }
    
    [self setStatusSignInButton];
    
    return [self updateTextFiel:textField shouldChangeCharactersInRange:range replacementString:string];
}


-(BOOL)updateTextFiel:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(string.length > 1)
    {
        NSMutableString *newString = [[NSMutableString alloc] initWithString:[string removeRedudantWhiteSpaceInText]];
        NSString *resultText = [textField.text stringByReplacingCharactersInRange:range
                                                                       withString:string];
        resultText = [resultText removeRedudantWhiteSpaceInText];
        
        if(textField == self.emailTextField)
        {
            //textField.text = newString;
            textField.text = (resultText.length > 96)?[resultText substringWithRange:NSMakeRange(0, 96)] : resultText;
        }
        
        return NO;
    }
    
    else if(textField == self.emailTextField)
    {
        if([textField.text occurrenceCountOfCharacter:'@'] == 1 && [string isEqualToString:@"@"]){
            return NO;
        }
        if (textField.text.length == 96 && ![string isEqualToString:@""]){
            return NO;
        }
        
        return YES;
    }
    
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.text = [textField.text removeRedudantWhiteSpaceInText];
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == self.emailTextField)
    {
        self.emailTextField.text = textField.text;
    }
    else if(textField == self.passwordTextField)
    {
        self.passwordTextField.text = textField.text;
    }
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if(textField == self.emailTextField){
        textField.text = self.emailTextField.text;
    }
    else if(textField == self.passwordTextField){
        textField.text = self.passwordTextField.text;
    }
}


#pragma mark - Delegate from API

-(void)loadDataDoneFrom:(WSBase*)ws
{
    if (ws.task == WS_AUTHENTICATION_LOGIN) {
        if (ws.data) {
            [self setUserDefaultInfoWithDict:ws.data[0]];
        }
        [[SessionData shareSessiondata] setIsUseLocation: NO];
        getPreferencesFromAPI(YES, self);
    }else if (ws.task == WS_GET_MY_PREFERENCE){
        [self updateSuccessRedirect];
        [self stopActivityIndicator];
    }
}


-(PreferenceObject*)getPreferenceByType:(PreferenceType)type{
    if ([SessionData shareSessiondata].arrayPreferences.count > 0) {
        for (PreferenceObject *preference in [SessionData shareSessiondata].arrayPreferences) {
            if (preference.type == type) {
                return preference;
            }
        }
    }
    return nil;
}

- (void) updateSuccessRedirect{
    
    //    if ([[SessionData shareSessiondata] hasForgotAccount]) {
    //        ChangePasswordViewController *changePasswordVC = [[ChangePasswordViewController alloc] init];
    //        [self presentViewController:changePasswordVC animated:YES completion:nil];
    //    }else{
    appdelegate = appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    [UIView transitionWithView:appdelegate.window
                      duration:0.5
                       options:UIViewAnimationOptionPreferredFramesPerSecond60
                    animations:^{
                        
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        MenuViewController *menuViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
                        UINavigationController *menuNavigationViewController = [[UINavigationController alloc] initWithRootViewController:menuViewController];
                        UIViewController *fontViewController = [[HomeViewController alloc] init];
                        
                        SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:menuNavigationViewController frontViewController:fontViewController];
                        
                        revealController.delegate = appdelegate;
                        revealController.rearViewRevealWidth = SCREEN_WIDTH;
                        revealController.rearViewRevealOverdraw = 0.0f;
                        revealController.rearViewRevealDisplacement = 0.0f;
                        appdelegate.viewController = revealController;
                        appdelegate.window.rootViewController = appdelegate.viewController;
                        [appdelegate.window makeKeyWindow];
                        
                        UIViewController *newFrontController = [[HomeViewController alloc] init];
                        UINavigationController *newNavigationViewController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                        [revealController pushFrontViewController:newNavigationViewController animated:YES];
                    }
                    completion:nil];
    //    }
    [self trackingEventByName:@"Sign in" withAction:ClickActionType withCategory:AuthenticationCategoryType];
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }
}

-(void)loadDataFailFrom:(WSBase *)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message {
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }else{
        AlertViewController *alert = [[AlertViewController alloc] init];
        
        if ([message isEqualToString:@"ENR70-1"]) {
            alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);;
            [self.emailTextField setBottomBorderRedColor];
            [self.passwordTextField setBottomBorderRedColor];
            
            alert.msgAlert = @"Please confirm that the value entered is correct.";
            
        }else{
            alert.titleAlert = NSLocalizedString(@"alert_error_title", nil);;
            alert.msgAlert = message;
        }
        alert.firstBtnTitle = NSLocalizedString(@"ok_button_title", nil);
        alert.blockFirstBtnAction = ^{
            [self makeBecomeFirstResponderForTextField];
        };
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;
        });
        
        [self showAlert:alert forNavigation:NO];
    }
}


- (void)setUserDefaultInfoWithDict:(UserRegistrationItem*)item{
    [[SessionData shareSessiondata] setOnlineMemberID:item.OnlineMemberID];
    [[SessionData shareSessiondata] setOnlineMemberDetailIDs:item.OnlineMemberDetailIDs];
}

#pragma mark - Action
- (IBAction)ForgetPasswordAction:(id)sender {
    self.emailTextField.text = @"";
    self.passwordTextField.text = @"";
    isInputEmail = isInputPassword = NO;
    [self.view endEditing:YES];
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UDAForgotPasswordViewController *forgotPasswordViewController = [storyboard instantiateViewControllerWithIdentifier:@"UDAForgotPasswordViewController"];
//    [self.navigationController pushViewController:forgotPasswordViewController animated:YES];
    
    ForgotPasswordView* v = [[ForgotPasswordView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    AACForgotPasswordController* vc = [AACForgotPasswordController new];
    vc.subview = v;
    v.controller = vc;
    [self.navigationController pushViewController:vc animated:true];
    
}

- (IBAction)SignInAction:(id)sender {
    [self verifyData];
}

- (IBAction)SignUpAction:(id)sender {
    self.emailTextField.text = @"";
    self.passwordTextField.text = @"";
    isInputEmail = isInputPassword = NO;
    [self.emailTextField endEditing:YES];
    [self.passwordTextField endEditing:YES];
    [self navigationToCreateProfileViewController];
}

-(void)navigationToCreateProfileViewController {
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    CreateProfileViewController *createProfileViewController = [storyboard instantiateViewControllerWithIdentifier:@"CreateProfileViewController"];
//
//    [self.navigationController pushViewController:createProfileViewController animated:YES];
    
    PasscodeViewController *passcodeViewController = [[PasscodeViewController alloc] init];
    [self.navigationController pushViewController:passcodeViewController animated:true];
    
}

@end
