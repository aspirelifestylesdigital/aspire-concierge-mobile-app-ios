//
//  BaseViewController.h
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/12/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ControlStyleHeader.h"
#import "TouchDownGestureRecognizer.h"
#import "WSBase.h"
#import <Google/Analytics.h>

typedef enum : NSUInteger {
    ClickActionType,
    OpenActionType,
    LeaveActionType,
    SubmitActionType,
    SelectActionType
} ActionType;

typedef enum : NSUInteger {
    AuthenticationCategoryType,
    UserInteractivityCategoryType,
    RequestCategoryType,
    CitySelectionCategoryType,
    CategorySelectionCategoryType,
    SignOutCategoryType
} CategoryType;

@interface BaseViewController : UIViewController<UIScrollViewDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate, DataLoadDelegate>
{
    @public
    UIView                              *spinnerBackground;
    UIActivityIndicatorView             *spinner;
    UIImageView                         *indicatorImageView;
    BOOL                                showingSpinner;
    UIView                              *maskViewForSpinner;
    float                               translationPointY;
    BOOL                                isLoading;
    BOOL                                isNotChangeNavigationBarColor;
    BOOL                                isShowAskConciergeBarButton;
    BOOL                                isShowAskConciergeButton;
    BOOL                                isIgnoreScaleView;
    BOOL                                isShouldHideKeyboardWhenSwipe;
    BOOL                                isAnimatingHideKeyboard;
    BOOL                                isHiddenTabBar;
    BOOL                                isHiddenNavigationBar;
    TouchDownGestureRecognizer          *touchdown;
    UIPanGestureRecognizer *popRecognizer;
    UIPanGestureRecognizer *popNavigationBarRecognizer;
}

@property (nonatomic, strong) UIPercentDrivenInteractiveTransition *interactivePopTransition;
@property (nonatomic, strong) UIPanGestureRecognizer *popNavigationBarRecognizer;

-(void)showAlertWithTitle:(NSString*)title
              andMessage:(NSString*)message
    andBtnTitle:(NSString*)btnTitle;

-(void)showActiveAlertWithTitle:(NSString*)title
              withMessage:(NSString*)message
         withFistBtnTitle:(NSString*)fstTit
           forFirstAction:(void (^)())fstAction
       withSecondBtnTitle:(NSString*)sndTit
           forSeconAction:(void (^)())sndAction;
-(void)showActiveAndDismissableAlertWithTitle:(NSString*)title
                                  withMessage:(NSString*)message
                             withFistBtnTitle:(NSString*)fstTit
                               forFirstAction:(void (^)())fstAction
                           withSecondBtnTitle:(NSString*)sndTit
                               forSeconAction:(void (^)())sndAction;

-(void)createBackBarButton;
-(void)createBackBarButtonWithIconName:(NSString*)iconName;
-(void)showMenu;
-(void)decorateForButton:(UIButton *)button;
-(void)startActivityIndicator;
-(void)stopActivityIndicator;
-(void)createConciergeBarButton;
-(void)callConcierge;
-(void)initData;
-(void)initView;
-(void)touchBack;
-(void) setNavigationBarWithDefaultColorAndTitle:(NSString*)title;
-(void)handlePopRecognizer:(UIPanGestureRecognizer*)recognizer;
-(void) returnNormalState;
-(void)startActivityIndicatorWithoutMask;
-(void)stopActivityIndicatorWithoutMask;
-(void)setNavigationBarWithColor:(UIColor *)color;
- (void)isChangeNavigationBackgroundColor:(BOOL)isChange;
-(void)changePositionForView;
-(void)setUpCustomizedPanGesturePopRecognizer;
-(void)removeSetupForCustomizedPanGesturePopRecognizer;
-(void)askConciergeAction:(id)sender;
-(void)showErrorNetwork;
-(void)hidenKeyboardWhenSwipe;

- (void) forceRelease;
-(void) createTabBarController;
-(void) changeBackgroundForSelectedTabbarItem;
-(void) setBackgroundColorForRequestTabBarItem;
- (void) setTabBarNormalStyle;
-(void) showToolTipAtView:(id)view inView:(UIView *)inView withKey:(NSString*)keyMessage;
- (void)WSBaseNetworkUnavailable;
-(void) showCommonAlertForFaildLoodingData;
- (void) pushVCFromBottom:(UIViewController *)viewControllerName;
- (void) popToRootVCFromBottom:(BOOL)popToRoot;
-(void) dismistTooltip;
//#Tracking
- (void)trackingEventByName:(NSString*)eventName withAction:(ActionType)action withCategory:(CategoryType)category;
- (void)trackingScreenByName:(NSString*)screenName;
@end
