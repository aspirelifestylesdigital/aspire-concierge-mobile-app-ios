//
//  MenuProfileViewController.m
//  LuxuryCard
//
//  Created by Viet Vo on 9/7/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "MenuProfileViewController.h"

#import "UtilStyle.h"
#import "PreferencesViewController.h"
#import "ChangePasswordViewController.h"
#import "MyProfileLXViewController.h"
#import "MenuTableViewCell.h"

#define rowHeight (float)70.0

@interface MenuProfileViewController () <UITableViewDelegate, UITableViewDataSource> {
    NSArray *contentList;
}

@end

@implementation MenuProfileViewController

- (void)viewDidLoad {
    isShowAskConciergeBarButton = NO;
    [super viewDidLoad];
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"My Settings", nil)];
    [self createBackBarButton];
    [self setupTableView];

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.contentTableView reloadData];
    isHiddenTabBar = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup UI
- (void)setupTableView{
    contentList = @[@"My Profile",@"Preferences", @"Change Password"];
    self.contentTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.contentTableView.alwaysBounceVertical = NO;
    [self.contentTableView setDelegate:self];
    [self.contentTableView setDataSource:self];
    [self.contentTableView setBackgroundColor:[UIColor blackColor]];
    [self.contentTableView registerNib:[UINib nibWithNibName:@"MenuTableViewCell" bundle:nil] forCellReuseIdentifier:@"MenuTableViewCell"];
}


#pragma mark - UITableView DataSource & Delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return rowHeight * SCREEN_SCALE;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return contentList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MenuTableViewCell"];
    cell.menuItemName.textColor = [UIColor whiteColor];
    
    CGFloat textSize = FONT_SIZE_28 * SCREEN_SCALE_BASE_WIDTH_375;
    NSString *fontName = FONT_MarkForMC_BLACK;
    if(indexPath.section == 1)
    {
        textSize = FONT_SIZE_16 * SCREEN_SCALE_BASE_WIDTH_375;
        fontName = FONT_MarkForMC_REGULAR;
    }
    cell.menuItemName.attributedText = [[NSAttributedString alloc] initWithString:[contentList objectAtIndex:indexPath.row] attributes:@{NSFontAttributeName:[UIFont fontWithName:fontName size:textSize]}];
    [cell.contentView setBackgroundColor:[UIColor blackColor]];
    UIImageView *accessoryView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40*SCREEN_SCALE, 40*SCREEN_SCALE)];
    [accessoryView setImage:[UIImage imageNamed:@"arrow_pro_white"]];
    [cell setAccessoryView:accessoryView];
    cell.contentView.superview.backgroundColor = [UIColor blackColor];
    [cell.accessoryView setHidden:NO];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    /*
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.textColor = [UIColor grayColor];
    cell.textLabel.textColor = colorFromHexString(BUTTON_BG_COLOR_HIGHLIGHT);
    
    for (UIView *view in cell.contentView.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            UIImageView *imageView = (UIImageView*)view;
            imageView.image = [UIImage imageNamed:@"right_arrow_red"];
        }
    }
     */
    
    switch (indexPath.row) {
        case 0:
        {
            MyProfileLXViewController *myProfile = [[MyProfileLXViewController alloc] init];
            [self.navigationController pushViewController:myProfile animated:YES];
        }
            break;
        case 1:
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            PreferencesViewController *preferenceViewController = [storyboard instantiateViewControllerWithIdentifier:@"PreferencesViewController"];
            [self.navigationController pushViewController:preferenceViewController animated:YES];
            
        }
            break;
        case 2:
        {
            ChangePasswordViewController *changePassword = [[ChangePasswordViewController alloc] init];
            [self.navigationController pushViewController:changePassword animated:YES];
        }
            break;
        default:
            break;
    }

}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    /*
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.textColor = [UIColor grayColor];
    cell.textLabel.textColor = colorFromHexString(BUTTON_BG_COLOR_HIGHLIGHT);
    
    for (UIView *view in cell.contentView.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            UIImageView *imageView = (UIImageView*)view;
            imageView.image = [UIImage imageNamed:@"right_arrow_red"];
        }
    }
     */
}

- (void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    /*
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.textColor = [UIColor grayColor];
    
    for (UIView *view in cell.contentView.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            UIImageView *imageView = (UIImageView*)view;
            imageView.image = [UIImage imageNamed:@"right_arrow_black"];
        }
    }
     */
}

@end
