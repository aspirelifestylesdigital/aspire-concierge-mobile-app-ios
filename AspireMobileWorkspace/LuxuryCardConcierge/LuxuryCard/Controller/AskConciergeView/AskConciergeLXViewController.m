//
//  AskConciergeLXViewController.m
//  LuxuryCard
//
//  Created by user on 3/27/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "AskConciergeLXViewController.h"
#import "UIButton+Extension.h"
#import "WSB2CCreateConciergeCase.h"
#import "AskConciergeRequest.h"
#import "ConciergeObject.h"
#import "ExploreVenueDetailViewController.h"
#import "ExploreViewController.h"
#define kOFFSET_FOR_KEYBOARD 80.0
#define DISABLE_ALPHA 0.4


@interface AskConciergeLXViewController () <UITextViewDelegate,DataLoadDelegate>

@end

@implementation AskConciergeLXViewController
{
    float keyboardHeight;
    UILabel *lblPlaceholder;
    ConciergeObject *conciergeObject;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isHiddenNavigationBar = YES;
    // Do any additional setup after loading the view from its nib.
    self.tvAskConcierge.delegate =  self;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (!lblPlaceholder) {
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            //load your data here.
            dispatch_async(dispatch_get_main_queue(), ^{
                //update UI in main thread.
                [self createAskConciergeTextViewPlaceHolder];
                [self checkShowHidePlaceHolder];
            });
        });
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self trackingScreenByName:@"Ask concierge"];
    // register for keyboard notifications
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleKeyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleKeyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [self.tvAskConcierge becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}

-  (void)initData{
    
}

- (void)initView{
    [self hideAlert:YES];
    
    
    
    self.contentView.layer.cornerRadius =  BUTTON_CORNER_RADIUS + 2;
    UIFont *callButtonFont = [UIFont fontWithName:FONT_MarkForMC_BLACK size:FONT_SIZE_20*SCREEN_SCALE_BASE_WIDTH_375];
    UIFont *mailButtonFont = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:FONT_SIZE_12*SCREEN_SCALE_BASE_WIDTH_375];
    
    self.bCallViewBackground.layer.cornerRadius = BUTTON_CORNER_RADIUS;
    self.bCallViewBackground.backgroundColor = [UIColor whiteColor];
    self.bCall.layer.cornerRadius = BUTTON_CORNER_RADIUS;
    [self.bCall setBackgroundColor:colorFromHexString(ASKCONCIERGE_BUTTON_BACKGROUND_COLOR)];
    [self.bCall setAttributedTitle:[self stringWithImageName:@"phone_call"
                                                   andString:NSLocalizedString(@"Or, call us. Available 24/7.", nil)
                                                     forFont:callButtonFont
                                                    andColor:WHITE_COLOR]
                          forState:UIControlStateNormal];
    
    self.bCall.titleEdgeInsets = UIEdgeInsetsMake(0, -4*SCREEN_SCALE, 0, 0);
    [self.bCheckPhone setAttributedTitle:[self stringWithImageName:@"checkbox_uncheck"
                                                         andString:NSLocalizedString(@"checkbox_phone", nil)
                                                           forFont:mailButtonFont
                                                          andColor:GRAY_COLOR]
                                forState:UIControlStateNormal];
    
    [self.bCheckPhone setAttributedTitle:[self stringWithImageName:@"checkbox_check"
                                                         andString:NSLocalizedString(@"checkbox_phone", nil)
                                                           forFont:mailButtonFont
                                                          andColor:BLACK_COLOR]
                                forState:UIControlStateSelected];
    
    [self.bCheckMail setAttributedTitle:[self stringWithImageName:@"checkbox_uncheck"
                                                        andString:NSLocalizedString(@"checkbox_email", nil)
                                                          forFont:mailButtonFont
                                                         andColor:GRAY_COLOR]
                               forState:UIControlStateNormal];
    
    [self.bCheckMail setAttributedTitle:[self stringWithImageName:@"checkbox_check"
                                                        andString:NSLocalizedString(@"checkbox_email", nil)
                                                          forFont:mailButtonFont
                                                         andColor:BLACK_COLOR]
                               forState:UIControlStateSelected];
    
    [self.bRespone.titleLabel setFont:mailButtonFont];
    [self.bRespone setTitleColor:colorFromHexString(BLACK_COLOR) forState:UIControlStateNormal];
    [self.bRespone setTitle:NSLocalizedString(@"respond_to_me", nil) forState:UIControlStateNormal];
    
    [self.bCancel.titleLabel setFont:mailButtonFont];
    [self.bCancel setTitleColor:colorFromHexString(BLACK_COLOR) forState:UIControlStateNormal];
    [self.bCancel setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
    
    [self.bSend.titleLabel setFont:mailButtonFont];
    [self.bSend setTitleColor:colorFromHexString(WHITE_COLOR) forState:UIControlStateNormal];
    [self.bSend setTitle:NSLocalizedString(@"SEND", nil) forState:UIControlStateNormal];
    self.bSend.layer.cornerRadius = BUTTON_CORNER_RADIUS;
    [self.lbTitleHeader setFont:[UIFont fontWithName:FONT_MarkForMC_BLACK size:FONT_SIZE_18*SCREEN_SCALE_BASE_WIDTH_375]];
    self.lbTitleHeader.textColor = colorFromHexString(BLACK_COLOR);

    [self.tvAskConcierge setFont:[UIFont fontWithName:FONT_MarkForMC_LIGHT size:FONT_SIZE_24*SCREEN_SCALE_BASE_WIDTH_375]];
    self.tvAskConcierge.textColor = colorFromHexString(ASKCONCIERGE_TEXTVIEW_COLOR);
    self.tvAskConcierge.autocorrectionType = UITextAutocorrectionTypeNo;
    if (self.conciergeDetail && self.conciergeDetail.requestDetail.length > 0) {
        NSString *caseID = (self.conciergeDetail.ePCCaseID ? self.conciergeDetail.ePCCaseID : @"");
        
        NSString *fullTitle = self.isDuplicateRequest ? [NSString stringWithFormat:@"Duplicate Request"] : [NSString stringWithFormat:@"Edit Request #%@", caseID];
        
        self.lbTitleHeader.text = fullTitle;
        
        
        if ([self.conciergeDetail.prefresponse.lowercaseString isEqualToString:@"mobile"]) {
            self.bCheckPhone.selected = YES;
        }else{
            self.bCheckMail.selected = YES;
        }
        
        
        [self enableSendButton: self.isDuplicateRequest ? NO : YES];
    }else{
        self.lbTitleHeader.text = NSLocalizedString(@"New Request", nil);
        self.bCheckMail.selected = YES;
        [self enableSendButton:NO];
    }
}

- (void) checkShowHidePlaceHolder{
    if(self.conciergeDetail)
    {
        if(!self.conciergeDetail.isCreated && self.conciergeDetail.requestDetail.length == 0){
            self.tvAskConcierge.text = [AskConciergeRequest buildAskConciergeTextWithConciergeData:self.conciergeDetail];
        }
        else{
            self.tvAskConcierge.text = [AskConciergeRequest editAskConciergeTextWithConciergeData:self.conciergeDetail];
        }
        
        lblPlaceholder.hidden = YES;
    }else{
        lblPlaceholder.hidden = NO;
    }
}

-(void)createAskConciergeTextViewPlaceHolder{
    NSMutableParagraphStyle *style  = [[NSMutableParagraphStyle alloc] init];
    style.minimumLineHeight = 30.f*SCREEN_SCALE;
    style.maximumLineHeight = 30.f*SCREEN_SCALE;
    
    
    lblPlaceholder = [[UILabel alloc] init];
    [lblPlaceholder setText:NSLocalizedString(@"ask_concierge_placeholder", nil)];
    [lblPlaceholder setBackgroundColor:[UIColor clearColor]];
//    NSMutableAttributedString *atrStr = [[NSMutableAttributedString alloc] initWithString:@""];
    NSMutableAttributedString *atrStr1 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"How can we help you today? \n\n", nil) attributes:@{NSParagraphStyleAttributeName:style, NSForegroundColorAttributeName : colorFromHexString(@"#484848"),NSFontAttributeName:[UIFont fontWithName:FONT_MarkForMC_LIGHT size:FONT_SIZE_20*SCREEN_SCALE_BASE_WIDTH_375]}];
    
    NSMutableAttributedString *atrStr2 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"Ideas?\n", nil) attributes:@{NSParagraphStyleAttributeName:style, NSForegroundColorAttributeName : colorFromHexString(ASKCONCIERGE_TEXTVIEW_COLOR),NSFontAttributeName:[UIFont fontWithName:FONT_MarkForMC_LIGHT size:FONT_SIZE_12*SCREEN_SCALE_BASE_WIDTH_375]}];
    
    NSMutableAttributedString *atrStr3 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"Arrange dinner reservations for tonight\nBook a hotel near me\nArrange a car to pick me up\nBook tickets to a local concert", nil) attributes:@{NSParagraphStyleAttributeName:style, NSForegroundColorAttributeName : colorFromHexString(@"#A8A8A8"),NSFontAttributeName:[UIFont fontWithName:FONT_MarkForMC_LIGHT size:FONT_SIZE_16*SCREEN_SCALE_BASE_WIDTH_375]}];
    
    lblPlaceholder.numberOfLines = 0;
    
    
    [atrStr1 appendAttributedString:atrStr2];
    [atrStr1 appendAttributedString:atrStr3];
//    [atrStr appendAttributedString:atrStr3];
    lblPlaceholder.attributedText = atrStr1;

    [self.tvAskConcierge addSubview:lblPlaceholder];

    [lblPlaceholder setFrame:CGRectMake(self.tvAskConcierge.bounds.origin.x + 7*SCREEN_SCALE, self.tvAskConcierge.bounds.origin.y + 5*SCREEN_SCALE*SCREEN_SCALE, self.tvAskConcierge.frame.size.width, self.tvAskConcierge.frame.size.height)];
    [lblPlaceholder sizeToFit];
    
    self.lbMessageAlert.textColor = [UIColor redColor];
    [self.lbMessageAlert setFont:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:FONT_SIZE_14]];
    self.lbMessageAlert.text = NSLocalizedString(@"Please select one of the following obtions", nil);
}

- (void)hideAlert:(BOOL)hide{
    [UIView animateWithDuration:0.1
                     animations:^{
                         self.lbMessageAlert.hidden = hide;
                         self.alertHeightConstraint.constant = hide == YES ? 0 : 26;
                     }];
    
}

- (void)enableSendButton:(BOOL)enable{
    self.bSend.enabled = enable;
    [self.bSend setBackgroundColor:colorFromHexString(enable == YES ? BLACK_COLOR : ASKCONCIERGE_BUTTON_BACKGROUND_COLOR)];
}

//-(void)keyboardWillHide {
//    [self setViewMovedUp:NO];
//}
//
//- (void)keyboardWasShown:(NSNotification *)notification
//{
//    NSDictionary* keyboardInfo = [notification userInfo];
//    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
//    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
//
//    // Get the size of the keyboard.
////    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//
//    //Given size may not account for screen rotation
//    keyboardHeight = keyboardFrameBeginRect.size.height;
//
//
//    [self setViewMovedUp:YES];
//    //your other code here..........
//}

#pragma mark KEYBOARD PROCESS
-(void)handleKeyboardWillShow:(NSNotification *)paramNotification
{
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         keyboardHeight = keyboardRect.size.height;
                         [self setViewMovedUp:YES];
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

-(void)handleKeyboardWillHide:(NSNotification *)paramNotification
{
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [self setViewMovedUp:NO];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:0
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL isFinished){
                         //                         isAnimatingHideKeyboard = !isFinished;
                     }];
    
    
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    self.bottomConstraint.constant = (movedUp == YES) ? keyboardHeight - self.tabBarController.tabBar.frame.size.height : 0 ;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSAttributedString*) stringWithImageName:(NSString*)imageName andString:(NSString*)string forFont:(UIFont*)font andColor:(NSString*)colorString{
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    attachment.image = [UIImage imageNamed:imageName];
    attachment.bounds = CGRectMake(0, -(attachment.image.size.height/4)*SCREEN_SCALE, attachment.image.size.width, attachment.image.size.height);
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    
    NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithAttributedString:attachmentString];
    
    
    [myString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"   %@", string]]];
    [myString addAttribute:NSFontAttributeName value:font range:NSMakeRange(4, string.length)];
    [myString addAttribute:NSForegroundColorAttributeName value:colorFromHexString(colorString) range:NSMakeRange(4, string.length)];
//    [myString addAttribute:NSBaselineOffsetAttributeName value:@8 range:NSMakeRange(0, string.length)];
    return myString;
}

- (IBAction)checkPhone:(id)sender {
    
//    if (self.bCheckPhone.selected == YES) {
//        self.bCheckPhone.selected = NO;
//    }else{
//        self.bCheckPhone.selected = YES;
//        if (self.bCheckMail.selected == YES) {
//            self.bCheckMail.selected = NO;
//        }
//    }
    self.bCheckPhone.selected = YES;
    self.bCheckMail.selected = NO;
}

- (IBAction)checkMail:(id)sender {
//    if (self.bCheckMail.selected == YES) {
//        self.bCheckMail.selected = NO;
//    }else{
//        self.bCheckMail.selected = YES;
//        if (self.bCheckPhone.selected == YES) {
//            self.bCheckPhone.selected = NO;
//        }
//    }
    
    self.bCheckMail.selected = YES;
    self.bCheckPhone.selected = NO;
}
- (IBAction)callConcierge:(id)sender {
    
//    maskView.providesPresentationContextTransitionStyle = YES;
//    maskView.definesPresentationContext = YES;
//    [maskView setModalPresentationStyle:UIModalPresentationOverCurrentContext];
//    maskView.alertViewControllerDelegate = self;
//    if(self.navigationController.presentedViewController == nil)
//        (isNavigationView)?[self.navigationController presentViewController:alert animated:NO completion:nil] : [self presentViewController:alert animated:NO completion:^{}];
    
    [self callConcierge];
}

- (IBAction)touchBack:(id)sender {
    _alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    _alertWindow.rootViewController = [UIViewController new];
    _alertWindow.windowLevel = 10000001;
    _alertWindow.hidden = NO;
    _alertWindow.tintColor = [[UIWindow valueForKey:@"keyWindow"] tintColor];
    
    __weak __typeof(self) weakSelf = self;
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"Are you sure you want to cancel?", nil) preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        weakSelf.alertWindow.hidden = YES;
        weakSelf.alertWindow = nil;
        [self backToNavigationController];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        weakSelf.alertWindow.hidden = YES;
        weakSelf.alertWindow = nil;
    }]];
    
    [_alertWindow.rootViewController presentViewController:alert animated:YES completion:nil];
    
    
//    if(self.tvAskConcierge.isFirstResponder)
//    {
//        [self.tvAskConcierge resignFirstResponder];
//    }
//    [self showActiveAlertWithTitle:@"" withMessage:NSLocalizedString(@"Are you sure you want to cancel?", nil) withFistBtnTitle:NSLocalizedString(@"Yes", nil) forFirstAction:^{
//        [self backToNavigationController];
//    } withSecondBtnTitle:NSLocalizedString(@"NO", nil) forSeconAction:^{
//        [self.tvAskConcierge becomeFirstResponder];
//    }];
    
    
}

- (IBAction)touchSend:(id)sender {
    if (self.bCheckPhone.selected == NO && self.bCheckMail.selected == NO) {
        [self stopActivityIndicator];
        
//        [self showAlertWithTitle:@"" andMessage:NSLocalizedString(@"noncheck_respond_to_me_alert", nil) andBtnTitle:NSLocalizedString(@"OK", nil)];
        [self hideAlert:NO];
    }else{
        [self startActivityIndicator];
        
        WSB2CCreateConciergeCase *wSB2CCreateConciergeCase = [[WSB2CCreateConciergeCase alloc] init];
        wSB2CCreateConciergeCase.delegate = self;
        if (!self.categoryName) {
            self.categoryName = @"";
        }
        if (!self.cityName) {
            self.cityName = @"";
        }
        
        if(self.conciergeDetail.isCreated){
            self.editType = UPDATE_EDIT_TYPE;
        }
        else{
            self.editType = ADD_EDIT_TYPE;
        }
        
        
        NSString *requestDetail;
        ConciergeDetailObject *tempObject = self.conciergeDetail;
        if (self.categoryName.length > 0 && ![tempObject.requestDetail containsString:@"CategoryName:"]) {
            requestDetail = [NSString stringWithFormat:@"CategoryName: %@ ##%@",[tempObject getStringType:tempObject.requestType], [self.tvAskConcierge.text stringByReplacingOccurrencesOfString:@"\n" withString:@"##"]];
        }
        else if([tempObject.requestDetail containsString:@"CategoryName:"])
        {
            requestDetail = [NSString stringWithFormat:@"CategoryName: %@ ##%@",tempObject.requestCategory, [self.tvAskConcierge.text stringByReplacingOccurrencesOfString:@"\n" withString:@"##"]];
        }
        else{
            requestDetail = [self.tvAskConcierge.text stringByReplacingOccurrencesOfString:@"\n" withString:@"##"];
        }
        
        NSDictionary *dict = @{@"RequestDetails": requestDetail,
                               @"TransactionID":(self.conciergeDetail.transactionId ? self.conciergeDetail.transactionId : @""),
                               //@"requestType":self.categoryName,
                               //@"requestCity":self.cityName,
                               @"phone":@(self.bCheckPhone.selected),
                               @"mail":@(self.bCheckMail.selected),
                               @"editType":self.editType};
        
        
        
        [wSB2CCreateConciergeCase askConciergeWithMessage:dict];
    }
}

/*
 // UITextViewDelegate
 */
- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (![textView hasText]) {
        lblPlaceholder.hidden = NO;
        [self enableSendButton:NO];
    }
}


- (void) textViewDidBeginEditing:(UITextView *)textView{
//    lblPlaceholder.hidden = YES;
}



-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    NSString *resultText = [textView.text stringByReplacingCharactersInRange:range
                                                                  withString:text];
    NSCharacterSet *charSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    resultText = [resultText stringByTrimmingCharactersInSet:charSet];
    
    if ([resultText isEqualToString:@""]) {
        if ([text isEqualToString:@""]) {
            lblPlaceholder.hidden = NO;
        }else{
            lblPlaceholder.hidden = YES;
        }

        [self enableSendButton:NO];
    }else{
        lblPlaceholder.hidden = YES;
        [self enableSendButton:YES];
    }
    return YES;
}

- (ConciergeObject*) createRequestItem{
    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
    
    conciergeObject = [[ConciergeObject alloc] init];
    conciergeObject.requestType = @"O Client Specific";
    conciergeObject.prefResponse = self.bCheckPhone.selected == YES ? @"Phone" : @"Email";
    conciergeObject.requestDetail = self.tvAskConcierge.text;
    conciergeObject.requestStatus = @"Open";
    conciergeObject.requestMode = @"Add";
    conciergeObject.createdDate = [self toStringFromDateTime:[NSDate new]];
    if (profileDictionary) {
        conciergeObject.phoneNumber = [profileDictionary objectForKey:keyMobileNumber];
        conciergeObject.emailAddress1 = [profileDictionary objectForKey:keyEmail];
    }
    return conciergeObject;
}

- (NSString*)toStringFromDateTime:(NSDate*)datetime
{
    // Purpose: Return a string of the specified date-time in UTC (Zulu) time zone in ISO 8601 format.
    // Example: 2013-10-25T06:59:43.431Z
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SS"];
    NSString* dateTimeInIsoFormatForZuluTimeZone = [dateFormatter stringFromDate:datetime];
    return dateTimeInIsoFormatForZuluTimeZone;
}

- (void)loadDataDoneFrom:(id<WSBaseProtocol>)ws{
    [self trackingEventByName:@"Request accepted" withAction:SubmitActionType withCategory:RequestCategoryType];

    [self stopActivityIndicator];
    [self backToNavigationController];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadMyRequest" object:nil];
}

- (void)backToNavigationController{
    
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    
    if (numberOfViewControllers > 1)
    {
         UIViewController *previousViewcontroller = [self.navigationController.viewControllers objectAtIndex:numberOfViewControllers - 2];
        if ([previousViewcontroller isKindOfClass:[ExploreVenueDetailViewController class]] || [previousViewcontroller isKindOfClass:[ExploreViewController class]]) {
            [self popToRootVCFromBottom:NO];
        }else{
            [self popToRootVCFromBottom:YES];
        }
    }
}

- (void)loadDataFailFrom:(id<BaseResponseObjectProtocol>)result withErrorCode:(NSInteger)errorCode{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self stopActivityIndicator];
        if(errorCode == 1005 || errorCode == -1009){
            [self showErrorNetwork];
        }else{
            [self trackingEventByName:@"Request denied" withAction:SubmitActionType withCategory:RequestCategoryType];
            [self showActiveAlertWithTitle:@"" withMessage:NSLocalizedString(@"askconcierge_fail_message", nil) withFistBtnTitle:NSLocalizedString(@"Call Concierge", nil) forFirstAction:^{
                [self callConcierge];
            } withSecondBtnTitle:NSLocalizedString(@"OK", nil) forSeconAction:nil];
        }
    });
}

@end
