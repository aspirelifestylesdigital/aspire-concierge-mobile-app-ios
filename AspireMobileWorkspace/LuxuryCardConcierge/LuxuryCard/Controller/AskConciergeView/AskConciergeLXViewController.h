//
//  AskConciergeLXViewController.h
//  LuxuryCard
//
//  Created by user on 3/27/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "BaseViewController.h"
#import "ConciergeDetailObject.h"

@interface AskConciergeLXViewController : BaseViewController
@property (strong, nonatomic) UIWindow* alertWindow;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIButton *bCancel;
@property (weak, nonatomic) IBOutlet UIButton *bSend;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleHeader;

@property (weak, nonatomic) IBOutlet UITextView *tvAskConcierge;

@property (weak, nonatomic) IBOutlet UILabel *lbMessageAlert;
@property (weak, nonatomic) IBOutlet UIButton *bRespone;
@property (weak, nonatomic) IBOutlet UIButton *bCheckPhone;
@property (weak, nonatomic) IBOutlet UIButton *bCheckMail;
@property (weak, nonatomic) IBOutlet UIButton *bCall;
@property (weak, nonatomic) IBOutlet UIView *bCallViewBackground;
- (IBAction)checkPhone:(id)sender;
- (IBAction)checkMail:(id)sender;
- (IBAction)callConcierge:(id)sender;
- (IBAction)touchBack:(id)sender;
- (IBAction)touchSend:(id)sender;

@property(nonatomic,strong) NSString *itemName;
@property(nonatomic,strong) NSString *categoryName;
@property(nonatomic,strong) NSString *cityName;
@property(nonatomic,strong) NSString *editType;
@property(nonatomic,strong) ConciergeDetailObject *conciergeDetail;
@property(nonatomic,assign) BOOL isDuplicateRequest;

@end
