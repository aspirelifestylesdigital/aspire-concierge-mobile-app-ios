//
//  ExploreDetailBenifitsView.m
//  MobileConciergeUSDemo
//
//  Created by Nghia Dinh on 7/19/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "ExploreDetailBenifitsView.h"
#import "LinkLabel.h"
#import "Common.h"
#import "Constant.h"
#import "AnswerItem.h"
#import "NSString+Utis.h"
#import "UILabel+Extension.h"

@interface ExploreDetailBenifitsView() <TTTAttributedLabelDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintImageView;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet LinkLabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *decoverImageView;
@property (nonatomic) NSString *benefits;
@property (nonatomic) BOOL isOffer;
@property (nonatomic) NSString *insiderTip;

@end

@implementation ExploreDetailBenifitsView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self customInit];
    }
    return self;
}

-(void)customInit {
    [[NSBundle mainBundle] loadNibNamed:@"ExploreDetailBenifitsView" owner:self options:nil];
    [self addSubview:_contentView];
    self.contentView.frame = self.frame;
    _contentLabel.delegate = self;
    
    //self.leadingConstraint.constant = self.leadingConstraint.constant*SCREEN_SCALE;
    //self.trailingConstraint.constant = self.trailingConstraint.constant*SCREEN_SCALE;
}

-(void)setupDataBenefits:(BaseItem*)item withType:(DetailType)type withContentFullCA:(ContentFullCCAItem*)content {
    
    if (type != DetailType_Other) {
        _benefits = ((AnswerItem*)item).offer2;
        //self.isOffer = ((AnswerItem*)item).isOffer;
        
    }else{
        _benefits = content.offerText;
        //self.isOffer = content.isOffer;
    }
    
    //cell.diningDetailContentBenefitsTableViewCellDelegate = self;
    if (!_isBenifits) {
        self.insiderTip = ((AnswerItem*)item).insiderTip;
    }
    [self loadData];
}

-(void)layoutSubviews {
    [super layoutSubviews];
    CGFloat heihgtLabel = _titleLabel.frame.size.height;
    _heightConstraintImageView.constant = heihgtLabel * 0.6;
    [self layoutIfNeeded];
}

-(void)loadData {
    NSDictionary *atts = @{
                           NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_BLACK size:16.0*SCREEN_SCALE]
                           };
    NSAttributedString *titleString = [[NSAttributedString alloc] initWithString:_isBenifits ? @"Your Benefits" : @"Insider Tips" attributes:atts];
    _titleLabel.attributedText = titleString;
    _decoverImageView.image = _isBenifits ? [UIImage imageNamed:@"star_icon"] : [UIImage imageNamed:@"insider_icon"];
    
    if (_isBenifits) {
        self.benefits = [self.benefits removeRedudantNewLineInText];
                if(self.benefits.length > 0)
        {NSRange range = [self.benefits rangeOfString:@"Click here"];
        if (range.location != NSNotFound) {
            self.benefits = [self.benefits substringToIndex:range.location];
            self.benefits = [NSString stringWithFormat:@"%@ </p>",self.benefits];
        }
        range = [self.benefits rangeOfString:@"Contact us"];
        if (range.location != NSNotFound) {
            self.benefits = [self.benefits substringToIndex:range.location];
            self.benefits = [NSString stringWithFormat:@"%@ </p>",self.benefits];
        }
        range = [self.benefits rangeOfString:@"Contact&nbsp;us"];
        if (range.location != NSNotFound) {
            self.benefits = [self.benefits substringToIndex:range.location];
            self.benefits = [NSString stringWithFormat:@"%@ </p>",self.benefits];
        }
        range = [self.benefits rangeOfString:@"<p><b> </p>"];
        if (range.location != NSNotFound) {
            self.benefits = [self.benefits substringToIndex:range.location];
            self.benefits = [NSString stringWithFormat:@"%@",self.benefits];
        }

            //self.benefits = [self.benefits stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%fpx; color:#011627; font-weight:normal;line-height: %fpx;}</style>",FONT_MarkForMC_LIGHT, 18.0f*SCREEN_SCALE, 22.0 * SCREEN_SCALE]];
            
            self.benefits = [NSString stringWithFormat:@"<html><head><style>body{font-family: '%@'; font-size:%fpx; color:#FFFFFF; font-weight:normal;line-height: %fpx;}</style></head><body>%@</body></html>",FONT_MarkForMC_REGULAR, FONT_SIZE_14*SCREEN_SCALE_BASE_WIDTH_375, 22.0 * SCREEN_SCALE, self.benefits];
        }
        [_contentLabel setHTMLString:_benefits];
        if(((NSString *)_contentLabel.text).length == 0){
            _contentLabel.text = nil;
            _heightTitleView.constant = 0;
            _titleView.hidden = YES;
            [self layoutIfNeeded];
            [self.heightAnchor constraintEqualToConstant:10].active = YES;
        }
        [_contentLabel sizeToFit];
    } else {
        if ([self.insiderTip containsString:@"<"]) {
            
        }else{
            self.insiderTip  = [NSString stringWithFormat:@"<p>%@</p>",self.insiderTip];
        }
        //self.insiderTip = [self.insiderTip stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%fpx; color:#011627; font-weight:normal;line-height: %fpx;}</style>",FONT_MarkForMC_LIGHT, 18.0f*SCREEN_SCALE, 22.0 * SCREEN_SCALE]];
        self.insiderTip = [NSString stringWithFormat:@"<html><head><style>body{font-family: '%@'; font-size:%fpx; color:#FFFFFF; font-weight:normal;line-height: %fpx;}</style></head><body>%@</body></html>",FONT_MarkForMC_REGULAR, FONT_SIZE_14*SCREEN_SCALE_BASE_WIDTH_375, 22.0 * SCREEN_SCALE, self.insiderTip];
         [_contentLabel setHTMLString:_insiderTip];
    }
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    if ([self.delegate respondsToSelector:@selector(didSelectURL:)]) {
        [self.delegate didSelectURL:url];
    }
}

-(void)attributedLabel:(TTTAttributedLabel *)label didLongPressLinkWithURL:(NSURL *)url atPoint:(CGPoint)point {
    if ([self.delegate respondsToSelector:@selector(didSelectURL:)]) {
        [self.delegate didSelectURL:url];
    }
}





@end
