//
//  ExploreDetailHeaderImageView.m
//  TestLink
//
//  Created by Nghia Dinh on 7/18/17.
//  Copyright © 2017 Nghia Dinh. All rights reserved.
//

#import "ExploreDetailHeaderImageView.h"
#import "UIImageView+AFNetworking.h"
#import "Common.h"
#import "Constant.h"
#import "NSString+Utis.h"
#import "ImageUtilities.h"
#import "LinkLabel.h"
#import "BaseViewController.h"
#import "AppData.h"
#import "UILabel+Extension.h"

@interface ExploreDetailHeaderImageView() <TTTAttributedLabelDelegate>
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *btnGotoWebsite;

@property (weak, nonatomic) IBOutlet UIButton *seeMapButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightImageConstraint;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet LinkLabel *addressLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topImageConstraint;
- (IBAction)gotoWebSite:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *seeMapBottomConstraint;

@property NSString *address;

@end

@implementation ExploreDetailHeaderImageView


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self customInit];
    }
    return self;
}


-(void)customInit {
    [[NSBundle mainBundle] loadNibNamed:@"ExploreDetailHeaderImageView" owner:self options:nil];
    [self addSubview:_contentView];
    self.contentView.frame = self.frame;
    
    
    [self.nameLabel setFont:[UIFont fontWithName:FONT_MarkForMC_BLACK size:FONT_SIZE_19*SCREEN_SCALE_BASE_WIDTH_375]];
    [self.nameLabel setTextColor:colorFromHexString(WHITE_COLOR)];
    
    [self.btnAskConcierge setImage:[UIImage imageNamed:@"concierge_icon"] forState:UIControlStateNormal];
    [self.btnAskConcierge setBackgroundColor:colorFromHexString(WHITE_COLOR)];
    self.btnAskConcierge.layer.cornerRadius = BUTTON_CORNER_RADIUS;
    self.heightConstraintButton.constant = 30 * SCREEN_SCALE;

   
    [self.addressLabel setFont:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:FONT_SIZE_12*SCREEN_SCALE_BASE_WIDTH_375]];
    [self.addressLabel setTextColor:colorFromHexString(WHITE_COLOR)];
    
    [self.seeMapButton setTitle:NSLocalizedString(@"See Map", nil) forState:UIControlStateNormal];
    [self.seeMapButton.titleLabel setFont:[UIFont fontWithName:FONT_MarkForMC_BOLD size:FONT_SIZE_14*SCREEN_SCALE_BASE_WIDTH_375]];
    [self.seeMapButton setTitleColor:colorFromHexString(WHITE_COLOR) forState:UIControlStateNormal];

    [self.btnGotoWebsite setTitle:NSLocalizedString(@"Go to website", nil) forState:UIControlStateNormal];
    [self.btnGotoWebsite.titleLabel setFont:[UIFont fontWithName:FONT_MarkForMC_BOLD size:FONT_SIZE_14*SCREEN_SCALE_BASE_WIDTH_375]];
    [self.btnGotoWebsite setTitleColor:colorFromHexString(WHITE_COLOR) forState:UIControlStateNormal];
    

    [self layoutIfNeeded];
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    if([self.delegate isKindOfClass:[BaseViewController class]])
    {
        BaseViewController *viewController = (BaseViewController*)self.delegate;
        [viewController showToolTipAtView:self.btnAskConcierge inView:viewController.view withKey:@"BellButton"];
    }
}


- (IBAction)touchAskConcierge:(id)sender {
    self.btnAskConcierge.highlighted = YES;
    self.btnAskConcierge.selected = YES;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.btnAskConcierge.highlighted = NO;
        self.btnAskConcierge.selected = NO;
        if ([self.delegate respondsToSelector:@selector(touchAskConcierge:)]){
            [self.delegate touchAskConcierge:sender];
        }
    });
}
- (IBAction)touchShare:(id)sender {
    NSMutableArray *data = [[NSMutableArray alloc] init];
    if(self.nameLabel.text.length > 0) {
        [data addObject:[NSString stringWithFormat:@"%@\n",self.nameLabel.text]];
    }
    [data addObject:[NSURL URLWithString:MASTERCARD_SHARING]];
    if(self.headImg.image) {
        [data addObject:self.headImg.image];
    }
    if ([self.delegate respondsToSelector:@selector(shareUrl:)]){
        [self.delegate shareUrl:data];
    }
}

- (void)loadData{
    if(self.imageURL)
    {
        __weak typeof(self) weakCell = self;
        //__weak typeof(self.tableView) weakTable = self.tableView;
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[self.imageURL removeBlankFromURL]]] ;
        [weakCell removeConstraint:_topImageConstraint];
        [weakCell.headImg.topAnchor constraintEqualToAnchor:weakCell.topAnchor constant:-5].active = YES;
        [self.headImg setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"placehoder_explore_detail"] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
            float imgWidth = image.size.width;
            image = [ImageUtilities imageWithImage:image convertToSize:CGSizeMake(SCREEN_WIDTH, (SCREEN_WIDTH/imgWidth)*image.size.height)];
            weakCell.heightImageConstraint.constant = image.size.height;
                [UIView animateWithDuration:0.1 animations:^{
                    [self.superview.superview.superview layoutIfNeeded];
                } completion:^(BOOL finished) {
                    weakCell.headImg.image = image;
                    [self.superview.superview.superview layoutIfNeeded];
//                    if([self.delegate isKindOfClass:[BaseViewController class]])
//                    {
//                        BaseViewController *viewController = (BaseViewController*)self.delegate;
//                        [viewController showToolTipAtView:self.btnAskConcierge inView:self.contentView withKey:@"BellButton"];
//                    }
                }];
        } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
        }];
    }
    
}

-(void)setupDataAddress:(BaseItem*)item withType:(DetailType)type withContentFullCA:(ContentFullCCAItem*)content; {
    
    if (type != DetailType_Other) {
        self.questionName = item.name;
        
        if(type == DetailType_CityGuide){
            __weak typeof(self) weakCell = self;
            float imgWidth = item.image.size.width;
            UIImage *image = [ImageUtilities imageWithImage:item.image convertToSize:CGSizeMake(SCREEN_WIDTH, (SCREEN_WIDTH/imgWidth)*item.image.size.height)];
            self.heightImageConstraint.constant = image.size.height;
            //dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:0.1 animations:^{
                [self.superview.superview.superview layoutIfNeeded];
            } completion:^(BOOL finished) {
                weakCell.headImg.image = image;
                [self.superview.superview.superview layoutIfNeeded];
                
            }];
            
        }
        else{
            self.imageURL = item.imageURL;
        }
        
        //headerView.diningDetailHeaderTableViewCellDelegate = self;
    }else{
        self.questionName = content.title;
        self.imageURL = [content.imgURL isEqualToString:@""] ? item.imageURL : content.imgURL;

        //headerView.diningDetailHeaderTableViewCellDelegate = self;
    }
    
    self.nameLabel.text = self.questionName;
    
    if (type == DetailType_CityGuide || type == DetailType_Dining) {
        NSString *address1;
        if (type == DetailType_CityGuide) {
            address1 = [NSString stringWithFormat:@"%@ %@, %@ %@ %@ %@",((AnswerItem*)item).address1, ((AnswerItem*)item).cityName, ((AnswerItem*)item).state, ((AnswerItem*)item).zipCode, ((AnswerItem*)item).address3, ((AnswerItem*)item).address2];
            //        cell.address3 = ((AnswerItem*)item).address2;
        }else{
            address1 = [NSString stringWithFormat:@"%@, %@, %@, %@ %@",((AnswerItem*)item).address1, ((AnswerItem*)item).cityName, ((AnswerItem*)item).state, ((AnswerItem*)item).zipCode, ((AnswerItem*)item).address3];
            //        cell.address3 = ((AnswerItem*)item).address3;
        }
        
        address1 = [address1 stringByReplacingOccurrencesOfString:@", ," withString:@","];
        if ([address1 hasSuffix:@","]) {
            address1 = [address1 substringToIndex:[address1 length]-1];
        }
        address1 = [NSString stringWithFormat:@"%@", address1];
        _address = address1;
        
//        NSString *webLink = ((AnswerItem*)item).URL;
       
        _addressLabel.text = _address;
        
    } else {
        _addressLabel.text = nil;
        [_btnGotoWebsite setTitle:nil forState:UIControlStateNormal];
        [_seeMapButton removeFromSuperview];
        [_btnGotoWebsite removeFromSuperview];
        [_addressLabel removeFromSuperview];
        if(_nameLabel.lineCount > 2){
            [_nameLabel.bottomAnchor constraintEqualToAnchor:self.bottomAnchor constant:-5.0].active = true;
        }
        else{
            [_btnAskConcierge.bottomAnchor constraintEqualToAnchor:self.bottomAnchor constant:-5.0].active = true;
        }
    }
    [self loadData];
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    if ([self.delegate respondsToSelector:@selector(gotoWebBrowser:)]){
         [self.delegate gotoWebBrowser:url];
    }
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithTransitInformation:(NSDictionary *)components {
    NSString *googleAdress = [components valueForKey:@"GoogleAddress"];
    if (googleAdress.length > 0) {
         if ([self.delegate respondsToSelector:@selector(seeDetailMapWithAddress:)]){
             [self.delegate seeDetailMapWithAddress:googleAdress];
         }
    }
}

-(void)attributedLabel:(TTTAttributedLabel *)label didLongPressLinkWithURL:(NSURL *)url atPoint:(CGPoint)point {
    if ([self.delegate respondsToSelector:@selector(gotoWebBrowser:)]){
        [self.delegate gotoWebBrowser:url];
    }
}
-(void)attributedLabel:(TTTAttributedLabel *)label didLongPressLinkWithTransitInformation:(NSDictionary *)components atPoint:(CGPoint)point {
    NSString *googleAdress = [components valueForKey:@"GoogleAddress"];
    if (googleAdress.length > 0) {
        if ([self.delegate respondsToSelector:@selector(seeDetailMapWithAddress:)]){
            [self.delegate seeDetailMapWithAddress:googleAdress];
        }
    }
}
-(IBAction)seeMapButtonTapped:(id)sender {
    if ([self.delegate respondsToSelector:@selector(seeDetailMapWithAddress:)]){
        [self.delegate seeDetailMapWithAddress:_address];
    }
}







- (IBAction)gotoWebSite:(id)sender {
     [self.delegate gotoWebBrowser:[NSURL URLWithString:self.shareLink]];
}
@end
