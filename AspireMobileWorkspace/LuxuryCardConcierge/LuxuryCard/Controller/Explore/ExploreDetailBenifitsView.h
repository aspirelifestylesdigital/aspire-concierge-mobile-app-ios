//
//  ExploreDetailBenifitsView.h
//  MobileConciergeUSDemo
//
//  Created by Nghia Dinh on 7/19/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "BaseItem.h"
#import "EnumConstant.h"
#import "ContentFullCCAItem.h"

@protocol  ExploreDetailBenifitsDelegate <NSObject>
-(void)didSelectURL:(NSURL*)url;
@end

@interface ExploreDetailBenifitsView : UIView

@property (weak, nonatomic) id<ExploreDetailBenifitsDelegate> delegate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTitleView;
@property (weak, nonatomic) IBOutlet UIView *titleView;
@property (nonatomic) BOOL isBenifits;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trailingConstraint;

-(void)setupDataBenefits:(BaseItem*)item withType:(DetailType)type withContentFullCA:(ContentFullCCAItem*)content;

@end
