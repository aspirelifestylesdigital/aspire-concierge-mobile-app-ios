//
//  ExploreVenueDetailViewController.h
//  MobileConciergeUSDemo
//
//  Created by Nghia Dinh on 7/19/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "BaseViewController.h"
#import "BaseItem.h"
#import "CityItem.h"



@interface ExploreVenueDetailViewController : BaseViewController

@property (nonatomic, strong) BaseItem *item;
//@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (assign, nonatomic) BOOL itemCCA;
@property (nonatomic, assign) NSInteger detailType;

@property (nonatomic, strong) NSString *contentID;
@property (nonatomic, strong) CityItem *currentCity;

@end
