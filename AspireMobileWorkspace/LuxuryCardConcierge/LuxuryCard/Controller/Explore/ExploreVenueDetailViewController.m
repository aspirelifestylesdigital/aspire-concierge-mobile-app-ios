//
//  ExploreVenueDetailViewController.m
//  MobileConciergeUSDemo
//
//  Created by Nghia Dinh on 7/19/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "ExploreVenueDetailViewController.h"
#import "AskConciergeLXViewController.h"
// Utils
#import "Constant.h"
// cell
#import "ExploreDetailHeaderImageView.h"
#import "ExploreDetailBenifitsView.h"
#import "ExploreDetailPriceView.h"
#import "ExploreDetailDescriptionView.h"

// item
#import "AnswerItem.h"
#import "WSB2CGetContentFullFromCCA.h"
#import "ContentFullCCAResponseObject.h"
#import "ContentFullCCAItem.h"
#import "TileItem.h"
#import "SearchItem.h"
#import "WSB2CGetQuestionAndAnswers.h"
#import "AppData.h"
#import "CityItem.h"
#import "CategoryItem.h"
#import "UIImageView+AFNetworking.h"
#import "MCActivityItemSource.h"
#import "MCImageActivityItemSource.h"
#import "MCTitleActivityItemSource.h"
#import "ImageUtilities.h"

#import "NSString+Utis.h"
#import "UILabel+Extension.h"
#import "LinkLabel.h"
#import "MapVenueDetailViewController.h"
#import "ConciergeDetailObject.h"
#import "AskConciergeRequest.h"

@interface ExploreVenueDetailViewController () <DataLoadDelegate, ExploreDetailHeaderImageViewDelegate, ExploreDetailDescriptionDelegate, ExploreDetailBenifitsDelegate>
@property (weak, nonatomic) IBOutlet UIStackView *detailStackView;
@property (nonatomic) AnswerItem *answerItem;

@end

@implementation ExploreVenueDetailViewController {
    ContentFullCCAItem *contentFullCCAItem;
}

- (void)viewDidLoad {
    isShowAskConciergeBarButton = YES;
    [super viewDidLoad];
    
    //[self registTableView];
    [self setupNavigationTitle];
    [self createBackBarButton];
    [self setupDataForView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadMyRequest)
                                                 name:@"ReloadMyRequest"
                                               object:nil];
}

-(void)reloadMyRequest {
    [self showActiveAlertWithTitle:NSLocalizedString(@"Your request has been received", nil) withMessage:NSLocalizedString(@"If it is time-sensitive, we recommend that you call the Concierge.", nil) withFistBtnTitle:NSLocalizedString(@"OK", nil) forFirstAction:^{
    } withSecondBtnTitle:NSLocalizedString(@"Call Concierge", nil) forSeconAction:^{
        [self callConcierge];
    }];
    
}

- (void)setupNavigationTitle{
    NSString *categoryNameTrimed = [self.item.categoryName stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    UILabel *calculateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width*0.75, 40)];
    calculateLabel.text = [NSString stringWithFormat:@"%@ > %@",self.currentCity.name, categoryNameTrimed];
    NSInteger lineCount = calculateLabel.lineCount;
    calculateLabel.alpha = 0;
    [self.navigationItem.titleView addSubview:calculateLabel];
    NSString *titleString;
    if (lineCount > 1) {
        titleString = [NSString stringWithFormat:@"%@ >\n%@",self.currentCity.name, categoryNameTrimed];
    }else{
        titleString = [NSString stringWithFormat:@"%@ > %@",self.currentCity.name, categoryNameTrimed];
    }
    [self setNavigationBarWithDefaultColorAndTitle:titleString.capitalizedString];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self trackingScreenByName:@"Venue detail"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
   
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self dismistTooltip];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)loadDataForView {
    [self.view layoutSubviews];
    switch (self.detailType) {
        case DetailType_Dining:
            [self addHeaderView:_item];
            [self addInsiderView:_item];
            [self addBenifitsView:_item];
            [self addPriceView:_item];
            [self addDescriptionView:_item withType:ViewDescription];
            [self addDescriptionView:_item withType:ViewOperatorHours];
            [self addDescriptionView:_item withType:ViewDefaultTerm];
            break;
        case DetailType_CityGuide:
            [self addHeaderView:_item];
            [self addInsiderView:_item];
            [self addDescriptionView:_item withType:ViewDescription];
            break;
        case DetailType_Other:
            [self addHeaderView:_item];
            [self addBenifitsView:_item];
            [self addDescriptionView:_item withType:ViewDescription];
            //[self addDescriptionView:_item withType:ViewTerm];
            break;
        default:
            break;
    }
    [self.view layoutIfNeeded];
}



//MARK: Add SubView To StackView
-(void)addInsiderView:(BaseItem*)item {
    if ([item isKindOfClass:[AnswerItem class]]) {
        if (((AnswerItem*)item).insiderTip.length != 0) {
            [_detailStackView addArrangedSubview:[self generateInsiderView:item withIsBenifits:NO]];
        }
    }
}
-(void)addHeaderView:(BaseItem*)item{
    [_detailStackView addArrangedSubview:[self generateHeader:item]];
}
-(void)addBenifitsView:(BaseItem*)item {
    [_detailStackView addArrangedSubview:[self generateInsiderView:item withIsBenifits:YES]];
}
-(void)addPriceView:(BaseItem*)item {
    [_detailStackView addArrangedSubview:[self generatePriceView:item]];
}
-(void)addDescriptionView:(BaseItem*)item withType:(TypeDescriptionView)type{
    [_detailStackView addArrangedSubview:[self generateDesView:item withType:type]];
}


//MARK: generate View
-(ExploreDetailHeaderImageView *)generateHeader:(BaseItem*)item {
    ExploreDetailHeaderImageView *headerView = [[ExploreDetailHeaderImageView alloc] initWithFrame:self.view.frame];
    headerView.delegate = self;
    [headerView setupDataAddress:self.item withType:self.detailType withContentFullCA:contentFullCCAItem];
    if ([self.item isKindOfClass:[AnswerItem class]]) {
        headerView.shareLink = ((AnswerItem*)self.item).URL;
    }
    return headerView;
}
-(ExploreDetailBenifitsView*)generateInsiderView:(BaseItem*)item withIsBenifits:(BOOL)benifits {
    ExploreDetailBenifitsView *benifitsView = [[ExploreDetailBenifitsView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
    benifitsView.isBenifits = benifits;
    benifitsView.delegate = self;
    [benifitsView setupDataBenefits:_item withType:self.detailType withContentFullCA:contentFullCCAItem];
    return benifitsView;
}
-(ExploreDetailPriceView*)generatePriceView:(BaseItem*)item {
    ExploreDetailPriceView *priceView = [[ExploreDetailPriceView alloc] initWithFrame:self.view.frame];
    [priceView setupData:item];
    return priceView;
}
-(ExploreDetailDescriptionView*)generateDesView:(BaseItem*)item withType:(TypeDescriptionView)type {
    ExploreDetailDescriptionView *desView = [[ExploreDetailDescriptionView alloc] initWithFrame:self.view.frame];
    [desView setupData:item withDetailType:_detailType withFullContent:contentFullCCAItem withType:type];
    desView.delegate = self;
    return desView;
}

-(void)setupDataForView
{

    if([self.item isKindOfClass:[TileItem class]])
    {
        self.detailType = DetailType_Other;
        [self loadDataFromCCAServerWithItemId:self.item.ID];
    }
    else if([self.item isKindOfClass:[AnswerItem class]] && [self.item.categoryCode isEqualToString:@"dining"])
    {
        self.detailType = DetailType_Dining;
        [self loadDataForView];
    }
    else if([self.item isKindOfClass:[AnswerItem class]] && [self.item.categoryCode isEqualToString:@"cityguide"])
    {
        self.detailType = DetailType_CityGuide;
        [self loadDataForView];
    }
    else if([self.item isKindOfClass:[SearchItem class]])
    {
        SearchItem *searchItem = (SearchItem *)self.item;
        UIImage *headImage = [self getImageForCityGuideItem:searchItem];
        
        if([searchItem.product isEqualToString:@"CCA"])
        {
            self.detailType = DetailType_Other;
            [self loadDataFromCCAServerWithItemId:searchItem.ID];
        }
        else if([searchItem.product isEqualToString:@"IA"])
        {
            self.item.image = headImage;
            self.detailType = DetailType_Other;
            [self loadDataFromIAServerWithSearchItem:searchItem];
        }
    }
}
-(void) loadDataFromIAServerWithSearchItem:(SearchItem *)item;
{
    [self startActivityIndicatorWithoutMask];
    WSB2CGetQuestionAndAnswers *wsAnswer = [[WSB2CGetQuestionAndAnswers alloc] init];
    wsAnswer.delegate = self;
    wsAnswer.categoryId = item.secondaryID;
    wsAnswer.questionId = item.ID;
    wsAnswer.task = WS_B2C_GET_SEARCH_DETAIL;
    [wsAnswer retrieveDataFromServer];
}

- (void) loadDataFromCCAServerWithItemId:(NSString *)itemID
{
    [self startActivityIndicatorWithoutMask];
    WSB2CGetContentFullFromCCA *wSB2CGetContentFullFromCCA = [[WSB2CGetContentFullFromCCA alloc] init];
    wSB2CGetContentFullFromCCA.delegate = self;
    wSB2CGetContentFullFromCCA.itemID = itemID;
    wSB2CGetContentFullFromCCA.categoryCode = self.item.categoryCode;
    wSB2CGetContentFullFromCCA.categoryName = self.item.categoryName;
    [wSB2CGetContentFullFromCCA loadDataFromServer];
}

#pragma mark PROCESS VIEW LOGIC

-(UIImage *)getImageForCityGuideItem:(SearchItem *)searchItem
{
    NSArray *citySelections = [AppData getSelectionCityList];
    for(CityItem *cityItem  in citySelections)
    {
        for(CategoryItem *categoryItem in cityItem.subCategories)
        {
            if([searchItem.secondaryID isEqualToString:categoryItem.ID])
            {
                return categoryItem.categoryImgDetail;
            }
        }
    }
    
    return nil;
}



#pragma mark DATA LOAD DELEGATE


- (void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_B2C_GET_SEARCH_DETAIL && ws.data.count > 0)
    {
        UIImage *headImage = self.item.image;
        self.item = [ws.data objectAtIndex:0];
        
        if(headImage){
            self.detailType = DetailType_CityGuide;
            self.item.image = headImage;
        }
        else{
            self.detailType = DetailType_Dining;
        }
    }
    else if (ws.data.count > 0) {
        contentFullCCAItem = (ContentFullCCAItem*)[ws.data objectAtIndex:0];
    }
    //[self.tableView reloadData];
    [self loadDataForView];
    [self stopActivityIndicatorWithoutMask];
}


#pragma mark HEADER IMAGE DELEGATE

-(void)touchAskConcierge:(id)sender {
    AskConciergeLXViewController *vc = [[AskConciergeLXViewController alloc] init];
    ConciergeDetailObject *conciergeItem = [[ConciergeDetailObject alloc] init];
    conciergeItem.isCreated = NO;
    if (self.detailType != DetailType_Other) {
        vc.itemName = self.item.name;
        vc.categoryName = self.item.categoryName;
        conciergeItem.name = self.item.name;
        conciergeItem.requestType = [AskConciergeRequest convertCategoryCodeToMyRequestType:self.item.categoryCode];
        
        if([conciergeItem.requestType isEqualToString:DINING_REQUEST_TYPE])
        {
            conciergeItem.city = self.item.cityName;
            conciergeItem.state = self.item.state;
            conciergeItem.country = self.item.countryName;
        }
        if (self.detailType == DetailType_CityGuide) {
            vc.cityName = self.currentCity.name;
        }
    } else {
        vc.itemName = contentFullCCAItem.title;
        vc.categoryName = contentFullCCAItem.category;
        conciergeItem.name = contentFullCCAItem.title;
        conciergeItem.requestType = [AskConciergeRequest convertCategoryNameToMyRequestType:contentFullCCAItem.category andSubcategory:contentFullCCAItem.subCategory];
    }
    
   
    vc.conciergeDetail = conciergeItem;
    [self pushVCFromBottom:vc];
}
-(void)shareUrl:(NSArray *)data {
    MCImageActivityItemSource *imageItemSource = [[MCImageActivityItemSource alloc] init];
    imageItemSource.data = data;
    
    MCTitleActivityItemSource *titleItemSource = [[MCTitleActivityItemSource alloc] init];
    imageItemSource.data = data;
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:@[titleItemSource,[data objectAtIndex:1],imageItemSource] applicationActivities:nil];
    [self presentViewController:activityVC animated:YES completion:^{}];
}

-(void)gotoWebBrowser:(NSURL *)url {
    [self showActiveAlertWithTitle:NSLocalizedString(@"alert_title", nil) withMessage:NSLocalizedString(@"category_more_info_msg", nil) withFistBtnTitle: NSLocalizedString(@"Yes", nil) forFirstAction:^{
        [[UIApplication sharedApplication] openURL:url];
    } withSecondBtnTitle: NSLocalizedString(@"CANCEL", nil) forSeconAction:nil];
}

-(void)seeDetailMapWithAddress:(NSString *)address {
    [self gotoAppleMapWith:address];
}


- (void) gotoAppleMapWith:(NSString*)address{
    MapVenueDetailViewController *vc = [[MapVenueDetailViewController alloc] initWithNibName:@"MapVenueDetailViewController" bundle:nil];
    vc.address = address;
    vc.item = self.item;
    vc.cityName = self.currentCity.name;
    [self.navigationController pushViewController:vc animated:true];
    
    // Check for iOS 6
    /*
    Class mapItemClass = [MKMapItem class];
    if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)])
    {
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder geocodeAddressString:address
                     completionHandler:^(NSArray *placemarks, NSError *error) {
                         
                         // Convert the CLPlacemark to an MKPlacemark
                         // Note: There's no error checking for a failed geocode
                         CLPlacemark *geocodedPlacemark = [placemarks objectAtIndex:0];
                         MKPlacemark *placemark = [[MKPlacemark alloc]
                                                   initWithCoordinate:geocodedPlacemark.location.coordinate
                                                   addressDictionary:geocodedPlacemark.addressDictionary];
                         
                         // Create a map item for the geocoded address to pass to Maps app
                         MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
                         [mapItem setName:((BaseItem*)self.item).name];
                         [MKMapItem openMapsWithItems:@[mapItem] launchOptions:nil];
                         
                     }];
    }
    */
    
}


#pragma mark TERM/DESCRIPTION DELEGATE

-(void)didSelectURL:(NSURL *)url {
    [self gotoWebBrowser:url];
}




@end
