//
//  MyRequestViewController.m
//  LuxuryCard
//
//  Created by Nghia Dinh on 9/8/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "MyRequestViewController.h"
#import "WSRequestConcierge.h"
#import "MyRequestDetailViewController.h"
#import "UITableView+Extensions.h"
#import "MenuListView.h"
#import "Common.h"
#import "UIButton+Extension.h"
#import "AskConciergeLXViewController.h"
#import "WSB2CCreateConciergeCase.h"
#import "AskConciergeRequest.h"
#import "ConciergeObject.h"
#import "TableHeaderView.h"
#import "MyRequestTableHeaderView.h"
#import "MyRequestTableViewCell.h"


@interface MyRequestViewController () <UITableViewDelegate, UITableViewDataSource, MenuListViewProtocol, DataLoadDelegate, MyRequestTableViewDelegate> {
    WSB2CCreateConciergeCase *wSB2CCreateConciergeCase;
    ConciergeObject *canceledRequest;
}
@property (nonatomic, strong) MyRequestTableHeaderView *tableHeaderView;
@property (nonatomic, strong) NSIndexPath *cancelIndex;
@end



@implementation MyRequestViewController
@synthesize requestType, requestArray, parrentVC;

- (void)viewDidLoad {
    [super viewDidLoad];
    isHiddenNavigationBar = YES;
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(receiveNotification:)
//                                                 name:@"MyRequestNotification"
//                                               object:nil];
    
    
    [self setupTableView];
}


//- (void) receiveNotification:(NSNotification *) notification
//{
//    if ([[notification name] isEqualToString:@"MyRequestNotification"])
//    {
//        [self showActiveAlertWithTitle:NSLocalizedString(@"Your request has been received.", nil) withMessage:NSLocalizedString(@"If it is time-sensitive, we recommend that you call the Concierge.", nil) withFistBtnTitle:NSLocalizedString(@"OK", nil) forFirstAction:nil withSecondBtnTitle:NSLocalizedString(@"Call Concierge", nil) forSeconAction:^{
//            [self callConcierge];
//        }];
//
//        if ([notification.object isKindOfClass:[ConciergeObject class]]) {
//            NSMutableArray *mutableArray = [[NSMutableArray alloc] init];
//            [mutableArray addObject:notification.object];
//            [mutableArray addObjectsFromArray:requestArray];
//            requestArray = mutableArray;
//        }
//    }
//    [self.myRequestTableView reloadData];
//
//}

-(void)setupTableView {
    [self.myRequestTableView registerNib:[UINib nibWithNibName:@"MyRequestTableViewCell" bundle:nil] forCellReuseIdentifier:@"MyRequestTableViewCell"];
    self.myRequestTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.myRequestTableView.rowHeight = UITableViewAutomaticDimension;
    self.myRequestTableView.estimatedRowHeight = 100.f;
    self.myRequestTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.myRequestTableView.allowsSelection = NO;
}

-(void)initData {
    
    requestArray = [[NSMutableArray alloc] init];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void) initView
{
    [self.myRequestBtn setDefaultStyle];
    [self.myRequestBtn setTitle:NSLocalizedString(@"New Request", nil) forState:UIControlStateNormal];
}


-(void)reloadDataForViewController:(NSInteger) index {
    NSLog(@"reloadData");
    if(self.requestArray.count == 0)
    {
        self.tableHeaderView = [self.myRequestTableView.tableHeaderView viewWithTag:3001];
        if(!self.tableHeaderView)
        {
            self.tableHeaderView = [[[NSBundle mainBundle] loadNibNamed:@"MyRequestTableHeaderView" owner:self options:nil] objectAtIndex:0];
            self.tableHeaderView.tag = 3001;
            [self.tableHeaderView setBackgroundColor:[UIColor blackColor]];
            self.tableHeaderView.frame = self.myRequestTableView.bounds;
            if(index == 0){
                [self.tableHeaderView setupViewWithMessage:NSLocalizedString(@"You have no open requests.\n Tap the \"New Request\" button above to make a request.", nil)];
            }
            else{
                [self.tableHeaderView setupViewWithMessage:NSLocalizedString(@"You have no closed requests.\n Tap the \"New Request\" button above to make a request.", nil)];
            }
//            [self.tableHeaderView.askConciergeBtn addTarget:self action:@selector(askConciergeAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        [self.myRequestTableView setTableHeaderView:self.tableHeaderView];
    }
    else
    {
        self.tableHeaderView = [self.myRequestTableView.tableHeaderView viewWithTag:3001];
        if(self.tableHeaderView)
        {
            [self.myRequestTableView setTableHeaderView:nil];
        }
        [_myRequestTableView reloadData];
    }
}


#pragma mark - UITableView DataSource & Delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  requestArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MyRequestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyRequestTableViewCell" forIndexPath:indexPath];
    cell.delegate = self;
    if(requestArray.count > 0)
    {
        [cell setupDataForCell:[requestArray objectAtIndex:indexPath.row]];
        cell.index = indexPath;
    }
    
    return cell;
}

/*
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(!isNetworkAvailable()) {
//        [self showErrorNetwork];
    }else{
        ConciergeObject *object = [requestArray objectAtIndex:indexPath.row];
        NSDate *date = dateFromISO8601Time(object.createdDate);
        NSDate *currentDate = [[NSDate alloc] init];
        NSTimeInterval distanceBetweenDates = [currentDate timeIntervalSinceDate:date];
        if (distanceBetweenDates <= 5 * 60) {
            [tableView deselectRowAtIndexPath:indexPath animated:FALSE];
            if (requestType == OpenRequest) {
                return;
            }
        }
        self.parrentVC.navigationController.delegate = nil;
        MyRequestDetailViewController *vc = [[MyRequestDetailViewController alloc] init];
        vc.epcCaseID = object.ePCCaseID;
        vc.transactionID = object.transactionID;
        
        [self.navigationController pushViewController:vc animated:YES];
    }
}
 */

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!parrentVC.isFullItem) {
        if([self isLastRowVisible])
        {
            if(parrentVC.isLoadMore)
            {
                [self loadMoreIndicatorIcon:parrentVC.isLoadMore];
                return;
            } else {
                [parrentVC loadMoreData];
            }
        }
        NSInteger lastRowIndex = [tableView numberOfRowsInSection:0] - 1;
        if ((indexPath.row == lastRowIndex))
        {
            [self loadMoreIndicatorIcon:isLoading];
        }
    }
}

-(void)loadMoreIndicatorIcon:(BOOL)loading
{
    if(loading)
    {
        [self.myRequestTableView startLoadMoreIndicator];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        self.myRequestTableView.contentInset = UIEdgeInsetsMake(0, 0, 30, 0);
        [UIView commitAnimations];;
    } else {
        [self.myRequestTableView removeLoadMoreIndicator];
        self.myRequestTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    }
}

- (IBAction)createNewRequestAction:(id)sender {
    AskConciergeLXViewController *askConciergeVC = [[AskConciergeLXViewController alloc] init];
    [self pushVCFromBottom:askConciergeVC];
}


-(BOOL)isLastRowVisible {
    NSArray *indexes = [self.myRequestTableView indexPathsForVisibleRows];
    // ignore this case
    if(indexes.count < 2)
        return YES;
    
    for (NSIndexPath *index in indexes) {
        if (index.row == requestArray.count - 1) {
            return YES;
        }
    }
    return NO;
}


#pragma mark - Data Load Delegate

-(void)loadDataDoneFrom:(WSBase*)ws {
    [self stopActivityIndicatorWithoutMask];
    if (ws.task == WS_CANCEL_CONCIERGE) {
//        [requestArray removeObject:canceledRequest];
//        [self.parrentVC updateData:canceledRequest];
        [self trackingEventByName:@"Request accepted" withAction:SubmitActionType withCategory:RequestCategoryType];
        canceledRequest.requestMode = CANCEL_MODE;
        [self.myRequestTableView reloadRowsAtIndexPaths:@[self.cancelIndex] withRowAnimation:UITableViewRowAnimationNone];
    }
}

-(void)loadDataFailFrom:(id<WSBaseProtocol>)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message{
    [self stopActivityIndicator];
    NSString *title;
    if (!message || [message isEqualToString:@""]) {
        message = NSLocalizedString(@"An error has occurred. Check your Internet settings or try again.", nil);
        title = NSLocalizedString(@"Unable to get data", nil);
    }else{
        title = NSLocalizedString(@"ERROR!", nil);
    }
    
    [self showActiveAlertWithTitle:title withMessage:message withFistBtnTitle:NSLocalizedString(@"Settings", nil) forFirstAction:^{
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"prefs:root"]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root"]];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root"]];
        }
    } withSecondBtnTitle:NSLocalizedString(@"OK", nil) forSeconAction:^{
        
    }];
    
    if (ws.task == WS_CANCEL_CONCIERGE) {
        [self trackingEventByName:@"Request denied" withAction:SubmitActionType withCategory:RequestCategoryType];
    }
}

- (void)loadDataFailFrom:(id<BaseResponseObjectProtocol>)result withErrorCode:(NSInteger)errorCode{
    [self stopActivityIndicator];
    [self showCommonAlertForFaildLoodingData];
    
    if (result.task == WS_CANCEL_CONCIERGE) {
        [self trackingEventByName:@"Request denied" withAction:SubmitActionType withCategory:RequestCategoryType];
    }
}

#pragma mark MyRequestTableViewDelegate
-(void)cancelRequestAction:(BaseConciergeObject *)data withIndex:(NSIndexPath *)index
{
    ConciergeObject* request = (ConciergeObject *)data;
    [self showActiveAndDismissableAlertWithTitle:@"" withMessage:NSLocalizedString(@"cancel_request_confirmation", nil) withFistBtnTitle:NSLocalizedString(@"Call Concierge", nil) forFirstAction:^{
        [self callConcierge];
    } withSecondBtnTitle:NSLocalizedString(@"Cancel anyway", nil) forSeconAction:^{
        [self startActivityIndicator];
        
        NSString *requestDetail;
        ConciergeObject *tempObject = request;
        if (request.requestType && request.requestType != nil && ![request.requestType isEqualToString:@""]) {
            requestDetail = [NSString stringWithFormat:@"CategoryName: %@ - %@",[tempObject getStringType:request.requestType], request.requestDetail];
        }else{
            requestDetail = request.requestDetail;
        }
        
        NSDictionary *dict = @{@"RequestDetails": request.requestDetail,
                               @"TransactionID":(request.transactionID ? request.transactionID : @""),
                               //@"requestType":self.categoryName,
                               //@"requestCity":self.cityName,
                               @"phone":@"false",
                               @"mail":@"true",
                               @"editType":CANCEL_EDIT_TYPE};
        wSB2CCreateConciergeCase = [[WSB2CCreateConciergeCase alloc] init];
        wSB2CCreateConciergeCase.delegate = self;
        
        [wSB2CCreateConciergeCase askConciergeWithMessage:dict];
        canceledRequest = request;
        self.cancelIndex = index;
    }];
}

-(void) seeDetailRequestAction:(BaseConciergeObject *)data withIndex:(NSIndexPath *)index
{
    ConciergeObject* request = (ConciergeObject *)data;
    MyRequestDetailViewController *vc = [[MyRequestDetailViewController alloc] init];
    vc.epcCaseID = request.ePCCaseID;
    vc.transactionID = request.transactionID;
    vc.resquestStatus = request.requestStatus;
    vc.resquestMode = request.requestMode;
    
    [self.navigationController pushViewController:vc animated:YES];
}


-(void) refreshUI
{
    [self.myRequestTableView beginUpdates];
    [self.myRequestTableView endUpdates];
}
@end
