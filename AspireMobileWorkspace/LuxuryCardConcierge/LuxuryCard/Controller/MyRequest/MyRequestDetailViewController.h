//
//  MyRequestDetailViewController.h
//  LuxuryCard
//
//  Created by Nghia Dinh on 9/8/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "BaseViewController.h"

@interface MyRequestDetailViewController : BaseViewController
@property (nonatomic, strong) NSString *epcCaseID;
@property (nonatomic, strong) NSString *transactionID;
@property (nonatomic, strong) NSString *resquestStatus;
@property (nonatomic, strong) NSString *resquestMode;

-(IBAction)changeRequestAction:(id)sender;
-(IBAction)createRequestAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *duplicateBtn;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
