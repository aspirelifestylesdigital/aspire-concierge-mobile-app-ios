//
//  MyRequestPagerViewController.m
//  LuxuryCard
//
//  Created by Nghia Dinh on 9/13/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "MyRequestPagerViewController.h"
#import "LJViewPager.h"
#import "MyRequestViewController.h"
#import "MenuListView.h"
#import "WSRequestConcierge.h"
#import "MyRequestDetailViewController.h"
#import "ConciergeObject.h"
#import "Constant.h"
#import "UILabel+Extension.h"
#import "AskConciergeLXViewController.h"
#import "UIButton+Extension.h"
#import "AppData.h"

#define OpenViewControllerIndex (int)0
#define ClosedViewControllerIndex (int)1

@interface MyRequestPagerViewController () <LJViewPagerDelegate, LJViewPagerDataSource, MenuListViewProtocol, DataLoadDelegate> {
    MyRequestViewController *openRequestVC;
    MyRequestViewController *closeRequestVC;
    __weak IBOutlet UIView *pagerView;
    MenuListView *menuListView;
    __weak IBOutlet UIStackView *stackView;
    
    NSMutableArray *allRequestArray;
    int requestPage;
    int requestPerPage;
    
    WSRequestConcierge *wsRequestConcierge;
    MyRequestType currentRequestType;
    
    LJViewPager *viewPager;
}

@end

@implementation MyRequestPagerViewController
@synthesize isLoadMore, isFullItem;

- (void)viewDidLoad {
    isShowAskConciergeButton = YES;
    [super viewDidLoad];
    
    openRequestVC = [[MyRequestViewController alloc] init];
    openRequestVC.parrentVC = self;
    openRequestVC.requestType = OpenRequest;
    closeRequestVC = [[MyRequestViewController alloc] init];
    closeRequestVC.parrentVC = self;
    closeRequestVC.requestType = ClosedRequest;
    
    currentRequestType = OpenRequest;
    
    menuListView = [MenuListView new];
    [stackView insertArrangedSubview:menuListView atIndex:0];

    menuListView.delegate = self;
    [menuListView reload];
    [self setupViewPage];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadMyRequest)
                                                 name:@"ReloadMyRequest"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadMyRequestWithoutMsg)
                                                 name:@"ReloadMyRequestWithoutMsg"
                                               object:nil];
    
    
    [self isChangeBackgroundTitle:NO];
    
}

- (void)isChangeBackgroundTitle:(BOOL)change{
    self.vBackGround.backgroundColor = colorFromHexString( change == YES ? DEFAULT_BACKGROUND_COLOR :GRAY_BACKGROUND_COLOR);
    pagerView.backgroundColor = colorFromHexString( change == YES ? DEFAULT_BACKGROUND_COLOR :GRAY_BACKGROUND_COLOR);
    viewPager.backgroundColor = colorFromHexString( change == YES ? DEFAULT_BACKGROUND_COLOR :GRAY_BACKGROUND_COLOR);
    menuListView.backgroundColor = colorFromHexString( change == YES ? DEFAULT_BACKGROUND_COLOR :GRAY_BACKGROUND_COLOR);
}

- (void)viewWillAppear:(BOOL)animated {
    isHiddenNavigationBar = YES;
    [super viewWillAppear:animated];
    
    if (self.isReaload == true) {
        self.isReaload = false;
        [self trackingScreenByName:@"Open Requests"];
        openRequestVC.requestArray = [[NSMutableArray alloc] init];
        [openRequestVC.myRequestTableView reloadData];
        closeRequestVC.requestArray = [[NSMutableArray alloc] init];
        [closeRequestVC.myRequestTableView reloadData];
//        [openRequestVC reloadDataForViewController:OpenViewControllerIndex];
//        [closeRequestVC reloadDataForViewController:ClosedViewControllerIndex];
        [self initData];
        
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    BOOL isShown = [AppData isShownTooltip:@"NewRequestButton"];
    if(isShown){
        [self showToolTipAtView:self.myRequestBtn inView:self.view withKey:@"NewRequestButton"];
    }
    
    CGPoint bottomOffset = CGPointMake(0, menuListView.scrollView.contentSize.height - menuListView.scrollView.bounds.size.height);
    [menuListView.scrollView setContentOffset:bottomOffset animated:YES];
    menuListView.scrollView.scrollEnabled = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self dismistTooltip];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reloadMyRequestWithoutMsg{
    self.isReaload = true;
}

-(void)reloadMyRequest {
    self.isReaload = true;
    [self isChangeBackgroundTitle:YES];
    [self showActiveAlertWithTitle:NSLocalizedString(@"Your request has been received", nil) withMessage:NSLocalizedString(@"If it is time-sensitive, we recommend that you call the Concierge.", nil) withFistBtnTitle:NSLocalizedString(@"OK", nil) forFirstAction:^{
        [self isChangeBackgroundTitle:NO];
    } withSecondBtnTitle:NSLocalizedString(@"Call Concierge", nil) forSeconAction:^{
        [self callConcierge];
        [self isChangeBackgroundTitle:NO];
    }];
}


-(void)initData {
    self.isReaload = false;
    isFullItem = false;
    requestPage = 0;
    requestPerPage = 20;
    isLoading = false;
    // openRequestArray = [[NSMutableArray alloc] init];
    //closedRequestArray = [[NSMutableArray alloc] init];
    //requestType = OpenRequest;
    wsRequestConcierge = [[WSRequestConcierge alloc] init];
    wsRequestConcierge.delegate = self;
    wsRequestConcierge.task = WS_GET_RECENT_CONCIERGE;
    [self getData];
    
}

-(void) initView
{
    NSString *titleText = NSLocalizedString(@"My Requests", nil);
    NSRange range = NSMakeRange(0, titleText.length);
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:titleText];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_MarkForMC_BOLD size:32.0*SCREEN_SCALE_BASE_WIDTH_375] range:range];
    [attributeString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:range];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init] ;
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    paragraphStyle.lineSpacing = 1.4;
    [attributeString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    
    self.myrequestTitle.attributedText = attributeString;
    
    [self.myRequestBtn setDefaultStyle];
    [self.myRequestBtn setTitle:NSLocalizedString(@"New Request", nil) forState:UIControlStateNormal];
}
-(void)getData {
    if(!isNetworkAvailable()) {
        [self showErrorNetwork];
    }else{
        if (!isLoadMore) {
            [self startActivityIndicator];
        } else {
            if ((currentRequestType == OpenRequest && openRequestVC.requestArray.count == 0) || (currentRequestType == ClosedRequest && closeRequestVC.requestArray.count == 0)) {
                [self startActivityIndicator];
            }
        }
        NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
        [param setValue:[NSString stringWithFormat:@"%d", requestPage * requestPerPage + 1] forKey:@"RowStart"];
        [param setValue:[NSString stringWithFormat:@"%d", (requestPage + 1) * requestPerPage] forKey:@"RowEnd"];
        [wsRequestConcierge getRecentConcierge:param];
    }
}



-(void)setupViewPage {
    viewPager = [[LJViewPager alloc] initWithFrame:CGRectZero];
    viewPager.viewPagerDateSource = self;
    viewPager.viewPagerDelegate = self;
    [pagerView addSubview:viewPager];
    viewPager.translatesAutoresizingMaskIntoConstraints = false;
    
    [viewPager.topAnchor constraintEqualToAnchor:pagerView.topAnchor constant:0].active = true;
    [viewPager.bottomAnchor constraintEqualToAnchor:pagerView.bottomAnchor constant:0].active = true;
    [viewPager.trailingAnchor constraintEqualToAnchor:pagerView.trailingAnchor constant:0].active = true;
    [viewPager.leadingAnchor constraintEqualToAnchor:pagerView.leadingAnchor constant:0].active = true;
    
}



#pragma mark - Pager View Delegate

- (UIViewController *)viewPagerInViewController {
    return self;
}

- (NSInteger)numbersOfPage {
    return 2;
}

- (UIViewController *)viewPager:(LJViewPager *)viewPager controllerAtPage:(NSInteger)page {
    switch (page) {
        case 0:
            return openRequestVC;
            break;
        case 1:
            return closeRequestVC;
            break;
        default:
            return nil;
            break;
    }
}

-(void)viewPager:(LJViewPager *)viewPager didScrollToPage:(NSInteger)page {
    [self cancelAllRequest];
    switch (page) {
        case 0:
            currentRequestType = OpenRequest;
            [openRequestVC reloadDataForViewController:OpenViewControllerIndex];
            [self trackingScreenByName:@"Open Requests"];
            break;
        case 1:
            currentRequestType = ClosedRequest;
            [closeRequestVC reloadDataForViewController:ClosedViewControllerIndex];
            [self trackingScreenByName:@"Closed Requests"];
            break;
        default:
            break;
    }
    [menuListView setDefaultIndex:page];
    
    if ((currentRequestType == OpenRequest && isFullItem == false && openRequestVC.requestArray.count == 0) || (currentRequestType == ClosedRequest && isFullItem == false && closeRequestVC.requestArray.count == 0))  {
        if (!isLoadMore) {
            [self getData];
        } else {
            [self startActivityIndicator];
        }
    }
}


#pragma mark - Menu List View Delegate

- (NSArray*) MenuListViewListItemsForView:(MenuListView*) view {
    return  @[@"Open Requests",@"Closed Requests"];
}


-(void)loadMoreData {
    isLoadMore =  true;
    [self getData];
}


- (void) MenuListView:(MenuListView*)view didSelectedItem:(id) item atIndex:(NSInteger) index {
    [self cancelAllRequest];
    switch (index) {
        case 0:
            currentRequestType = OpenRequest;
            [openRequestVC reloadDataForViewController:OpenViewControllerIndex];
            [self trackingScreenByName:@"Open Requests"];
            break;
        case 1:
            currentRequestType = ClosedRequest;
            [closeRequestVC reloadDataForViewController:ClosedViewControllerIndex];
            [self trackingScreenByName:@"Closed Requests"];
            break;
        default:
            break;
    }
    [viewPager scrollToPage:index];
    
    
    if ((currentRequestType == OpenRequest && isFullItem == false && openRequestVC.requestArray.count == 0) || (currentRequestType == ClosedRequest && isFullItem == false && closeRequestVC.requestArray.count == 0))  {
        if (!isLoadMore) {
            [self getData];
        } else {
            [self startActivityIndicator];
        }
    }
}

-(void)cancelAllRequest {
    //[wsRequestConcierge cancelRequest];
    //isLoadMore = false;
    [openRequestVC loadMoreIndicatorIcon:false];
    [closeRequestVC loadMoreIndicatorIcon:false];
    
}

-(void)dealloc {
    [wsRequestConcierge cancelRequest];
}


#pragma mark - Data Load Delegate
-(void)loadDataDoneFrom:(WSRequestConcierge*)ws {
    [self stopActivityIndicator];
    requestPage += 1;
    if (ws.data.count == 0){
        isFullItem = true;
        isLoadMore = false;
        [openRequestVC loadMoreIndicatorIcon:false];
        [closeRequestVC loadMoreIndicatorIcon:false];
        
//      Update No Data Message
        [openRequestVC reloadDataForViewController:OpenViewControllerIndex];
    }
    if (ws.data.count > 0) {
        if (ws.data.count < requestPerPage) {
            isFullItem = true;
        }
        NSArray *filteredOpenRequestArray = [ws.data filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
            ConciergeObject *obj = (ConciergeObject*)evaluatedObject;
            return ([obj.requestStatus isEqualToString:OPEN_STATUS] || [obj.requestStatus isEqualToString:INPROGRESS_STATUS]);
        }]];
        NSArray *filteredClosedRequestArray = [ws.data filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
            ConciergeObject *obj = (ConciergeObject*)evaluatedObject;
            return [obj.requestStatus isEqualToString:CLOSE_STATUS];
        }]];
        
        
        [openRequestVC.requestArray addObjectsFromArray:filteredOpenRequestArray];
        [closeRequestVC.requestArray addObjectsFromArray:filteredClosedRequestArray];
        
        
        if ((currentRequestType == OpenRequest && isFullItem == false && filteredOpenRequestArray.count == 0) || (currentRequestType == ClosedRequest && isFullItem == false && filteredClosedRequestArray.count == 0))  {
            [self getData];
        } else {
            //[self stopActivityIndicator];
            isLoadMore = false;
            [openRequestVC loadMoreIndicatorIcon:false];
            [closeRequestVC loadMoreIndicatorIcon:false];
            [openRequestVC reloadDataForViewController:0];
            [closeRequestVC reloadDataForViewController:1];
            
            //[self loadMoreIndicatorIcon:false];
            //[_myRequestTableView removeLoadMoreIndicator];
            //[_myRequestTableView reloadData];
        }
    }
}


-(void)updateData:(BaseConciergeObject*)data
{
    [closeRequestVC.requestArray insertObject:data atIndex:0];
}
- (IBAction)createNewRequestAction:(id)sender {
    AskConciergeLXViewController *askConciergeVC = [[AskConciergeLXViewController alloc] init];
    [self pushVCFromBottom:askConciergeVC];
}
@end
