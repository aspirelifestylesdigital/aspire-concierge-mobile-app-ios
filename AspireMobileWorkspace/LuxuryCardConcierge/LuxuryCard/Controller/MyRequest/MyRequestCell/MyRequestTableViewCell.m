//
//  MyRequestTableViewCell.m
//  LuxuryCard
//
//  Created by Nghia Dinh on 9/8/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "MyRequestTableViewCell.h"
#import "Common.h"
#import "Constant.h"
#import "UIButton+Extension.h"
#import "MyRequestDetailViewController.h"
#import "AskConciergeRequest.h"



@interface MyRequestTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *indicatorImageView;
@property (weak, nonatomic) IBOutlet UILabel *requestTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *openLabel;
@property (weak, nonatomic) IBOutlet UILabel *closedLabel;
@property (weak, nonatomic) IBOutlet UILabel *openTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *closedTimeLabel;
@property (strong, nonatomic) ConciergeObject* data;

@end

@implementation MyRequestTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.requestStatusLbl.font = [UIFont fontWithName:FONT_MarkForMC_BOLD size:10.0 * SCREEN_SCALE_BASE_WIDTH_375];
    self.requestStatusLbl.textColor = [UIColor whiteColor];
    
    self.requestNumberLbl.font = [UIFont fontWithName:FONT_MarkForMC_BOLD size:18.0 * SCREEN_SCALE_BASE_WIDTH_375];
    self.requestNumberLbl.textColor = [UIColor whiteColor];
    
    self.requestDetailTitleLbl.font = [UIFont fontWithName:FONT_MarkForMC_BOLD size:10.0 * SCREEN_SCALE_BASE_WIDTH_375];
    self.requestDetailTitleLbl.textColor = [UIColor whiteColor];
    self.requestDetailTitleLbl.text = NSLocalizedString(@"REQUEST DETAILS", nil);
    
    self.requestDetailMessageLbl.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:14.0 * SCREEN_SCALE_BASE_WIDTH_375];
    self.requestDetailMessageLbl.textColor = colorFromHexString(@"#A7A7A7");
    
    [self.seeDetailBtn.layer setCornerRadius:3.0f];
    [self.seeDetailBtn setBackgroundColor:colorFromHexString(@"#2B2B2B")];
    [self.seeDetailBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.seeDetailBtn.titleLabel.font =[UIFont fontWithName:FONT_MarkForMC_REGULAR size:14.0 * SCREEN_SCALE_BASE_WIDTH_375];
    [self.seeDetailBtn setTitle:NSLocalizedString(@"See details", nil) forState:UIControlStateNormal];
    
    [self.cancelRequestBtn.layer setCornerRadius:3.0f];
    [self.cancelRequestBtn setBackgroundColor:colorFromHexString(@"#2B2B2B")];
    [self.cancelRequestBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.cancelRequestBtn.titleLabel.font =[UIFont fontWithName:FONT_MarkForMC_REGULAR size:14.0 * SCREEN_SCALE_BASE_WIDTH_375];
    [self.cancelRequestBtn setTitle:NSLocalizedString(@"Cancel Request", nil) forState:UIControlStateNormal];
    
    [self.headerView setBackgroundColor: colorFromHexString(@"#3E3E3E")];
//    [self.headerView.layer setCornerRadius:3.0f];
    
    [self.detailView setBackgroundColor: colorFromHexString(@"#3E3E3E")];
//    [self.detailView.layer setCornerRadius:3.0f];
    
//    self.requestTypeLabel.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:18.0 * SCREEN_SCALE];
//     self.openLabel.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:18.0 * SCREEN_SCALE];
//    self.openTimeLabel.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:18.0 * SCREEN_SCALE];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setupDataForCell:(ConciergeObject*)data {
    /*
    NSString* requestTypeString = [NSString stringWithFormat:@"%@",  [data getStringType]];
    if (![[data getStringName] isEqualToString:@""]) {
        requestTypeString = [requestTypeString stringByAppendingString:[NSString stringWithFormat:@" - %@", [data getStringName]]];
    }
    _requestTypeLabel.text = requestTypeString;
    _openTimeLabel.text = data.createdDate != nil ? stringDateWithFormat(@"MM/dd/yyyy | h:mm aa", dateFromISO8601Time(data.createdDate)) : nil;
     */
    self.data = data;
    self.requestNumberLbl.text = [NSString stringWithFormat:@"Request #%@", (data.ePCCaseID ? data.ePCCaseID : @"")];
    
    self.requestDetailMessageLbl.text = [AskConciergeRequest buildRequestWithConciergeData:data];
    
    if ([data.requestStatus isEqualToString:CLOSE_STATUS]) {
        self.requestStatusLbl.text = [NSString stringWithFormat:@"STATUS: CLOSED"];
        self.cancelRequestBtn.hidden = YES;
    } else {
        NSDate *date = dateFromStringWithFormat(@"MMMM dd, yyyy h:mm aa", data.createdDate);
        NSDate *currentDate = [[NSDate alloc] init];
        NSTimeInterval distanceBetweenDates = [currentDate timeIntervalSinceDate:date];
        if (distanceBetweenDates <= 5 * 60 && ![data.requestStatus isEqualToString:CANCEL_MODE]) {
            self.requestNumberLbl.text = [NSString stringWithFormat:@"New Pending Request"];
            self.requestStatusLbl.text = [NSString stringWithFormat:@"STATUS: PENDING"];
            self.buttonsView.hidden = YES;
            self.detailView.hidden = YES;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (300 - distanceBetweenDates) * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                self.requestNumberLbl.text = [NSString stringWithFormat:@"Request #%@", (data.ePCCaseID ? data.ePCCaseID : @"")];
                self.requestStatusLbl.text = [NSString stringWithFormat:@"STATUS: OPEN"];
                self.detailView.hidden = NO;
                self.buttonsView.hidden = NO;
                if(self.delegate != nil){
                    if([self.delegate respondsToSelector:@selector(refreshUI)])
                        [self.delegate refreshUI];
                }
            });
        }
        else if([data.requestStatus isEqualToString:INPROGRESS_STATUS])
        {
            self.requestStatusLbl.text = [NSString stringWithFormat:@"STATUS: IN PROGRESS"];
            self.buttonsView.hidden = NO;
            self.detailView.hidden = NO;
            
            self.cancelRequestBtn.hidden = [data.requestMode isEqualToString:CANCEL_MODE] ? YES : NO;
        }
        else {
            self.requestStatusLbl.text = [NSString stringWithFormat:@"STATUS: OPEN"];
            self.buttonsView.hidden = NO;
            self.detailView.hidden = NO;
            
            self.cancelRequestBtn.hidden = [data.requestMode isEqualToString:CANCEL_MODE] ? YES : NO;
        }
    }

}

- (IBAction)seeRequestDetail:(id)sender {
    if(self.delegate != nil){
        if([self.delegate respondsToSelector:@selector(seeDetailRequestAction:withIndex:)])
            [self.delegate seeDetailRequestAction:self.data withIndex:self.index];
    }
    
}

- (IBAction)cancelRequestDetail:(id)sender {
    if(self.delegate != nil){
        if([self.delegate respondsToSelector:@selector(cancelRequestAction:withIndex:)])
            [self.delegate cancelRequestAction:self.data withIndex:self.index];
    }
}



@end
