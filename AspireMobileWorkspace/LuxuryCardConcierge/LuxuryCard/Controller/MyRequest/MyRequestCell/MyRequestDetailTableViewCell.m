//
//  MyRequestDetailTableViewCell.m
//  LuxuryCard
//
//  Created by Chung Mai on 3/29/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "MyRequestDetailTableViewCell.h"
#import "Constant.h"
#import "Common.h"

@implementation MyRequestDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.requestBackgroundView.layer setCornerRadius:3.0f];
    [self.requestBackgroundView setBackgroundColor:colorFromHexString(@"#3E3E3E")];
    
    self.titleLbl.font = [UIFont fontWithName:FONT_MarkForMC_BOLD size:10.0 * SCREEN_SCALE_BASE_WIDTH_375];
    self.titleLbl.textColor = [UIColor whiteColor];
    
    self.contentLbl.font = [UIFont fontWithName:FONT_MarkForMC_BOLD size:18.0 * SCREEN_SCALE_BASE_WIDTH_375];
    self.contentLbl.textColor = [UIColor whiteColor];
    
    self.moreInfoLbl.font = [UIFont fontWithName:FONT_MarkForMC_BOLD size:18.0 * SCREEN_SCALE_BASE_WIDTH_375];
    self.moreInfoLbl.textColor = [UIColor whiteColor];
    
    self.moreInfoLbl.hidden = YES;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setupDataForCellWithData:(PropertyItem *)item
{
    
    self.titleLbl.text = item.title;
    self.contentLbl.text = item.value;
    self.moreInfoLbl.text =@"";
}
@end
