//
//  MyRequestTableHeaderView.m
//  LuxuryCard
//
//  Created by Chung Mai on 4/2/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "MyRequestTableHeaderView.h"
#import "Constant.h"

@implementation MyRequestTableHeaderView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)setupViewWithMessage:(NSString *)message
{
    self.messageLbl.font = [UIFont fontWithName:FONT_MarkForMC_LIGHT size:19.0 * SCREEN_SCALE_BASE_WIDTH_375];
    self.messageLbl.textColor = [UIColor whiteColor];
    self.messageLbl.text = message;
}

@end
