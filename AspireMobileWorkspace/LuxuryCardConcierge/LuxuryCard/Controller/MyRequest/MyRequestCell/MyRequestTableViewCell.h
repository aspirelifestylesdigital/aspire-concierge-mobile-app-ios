//
//  MyRequestTableViewCell.h
//  LuxuryCard
//
//  Created by Nghia Dinh on 9/8/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyRequestViewController.h"
#import "ConciergeObject.h"
#import "BaseConciergeObject.h"

@protocol MyRequestTableViewDelegate <NSObject>

@optional
-(void) cancelRequestAction:(BaseConciergeObject *) data withIndex:(NSIndexPath *) index;
-(void) seeDetailRequestAction: (BaseConciergeObject *) data withIndex:(NSIndexPath *) index;
-(void) refreshUI;

@end

@interface MyRequestTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *requestStatusLbl;
@property (weak, nonatomic) IBOutlet UILabel *requestNumberLbl;
@property (weak, nonatomic) IBOutlet UILabel *requestDetailTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *requestDetailMessageLbl;
@property (weak, nonatomic) IBOutlet UIButton *seeDetailBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelRequestBtn;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *spacingView;
@property (weak, nonatomic) IBOutlet UIView *detailView;
@property (weak, nonatomic) IBOutlet UIView *buttonsView;
@property (weak, nonatomic) id<MyRequestTableViewDelegate> delegate;
@property (strong, nonatomic) NSIndexPath *index;

-(void)setupDataForCell:(ConciergeObject*)data;
- (IBAction)seeRequestDetail:(id)sender;
- (IBAction)cancelRequestDetail:(id)sender;


@end
