//
//  MyRequestTableHeaderView.h
//  LuxuryCard
//
//  Created by Chung Mai on 4/2/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyRequestTableHeaderView : UIView
@property (weak, nonatomic) IBOutlet UILabel *messageLbl;
-(void)setupViewWithMessage:(NSString *)message;
@end
