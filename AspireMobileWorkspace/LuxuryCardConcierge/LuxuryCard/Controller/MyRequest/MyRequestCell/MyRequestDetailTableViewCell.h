//
//  MyRequestDetailTableViewCell.h
//  LuxuryCard
//
//  Created by Chung Mai on 3/29/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PropertyItem.h"
@interface MyRequestDetailTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *contentLbl;
@property (weak, nonatomic) IBOutlet UILabel *moreInfoLbl;
@property (weak, nonatomic) IBOutlet UIView *requestBackgroundView;

-(void)setupDataForCellWithData:(PropertyItem *)item;
@end
