//
//  MyRequestDetailViewController.m
//  LuxuryCard
//
//  Created by Nghia Dinh on 9/8/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "MyRequestDetailViewController.h"
#import "WSRequestConcierge.h"
#import "UIButton+Extension.h"
#import "ConciergeDetailObject.h"
#import "AskConciergeRequest.h"
#import "WSB2CCreateConciergeCase.h"
#import "MyRequestPagerViewController.h"
#import "MyRequestDetailTableViewCell.h"
#import "AskConciergeLXViewController.h"

@interface MyRequestDetailViewController () <DataLoadDelegate, UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UITextView *contentTextView;
    
    __weak IBOutlet UILabel *lbCloseDate;
    __weak IBOutlet UILabel *lbStatus;
    __weak IBOutlet UILabel *lbRequestName;
    __weak IBOutlet UILabel *lblClose;
    __weak IBOutlet UILabel *lbStatusTitle;
    
    WSRequestConcierge *wsRequestConcierge;
    ConciergeDetailObject *conciergeDetail ;
    WSB2CCreateConciergeCase *wSB2CCreateConciergeCase;
    NSArray *displayingData;
}

@end

@implementation MyRequestDetailViewController

- (void)viewDidLoad {
    isShowAskConciergeBarButton = YES;
    [super viewDidLoad];
    [self setNavigationBarWithDefaultColorAndTitle:@" "];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    [self createBackBarButton];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"MyRequestDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"MyRequestDetailTableViewCell"];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100.f;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.allowsSelection = NO;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView setBackgroundColor:[UIColor blackColor]];
    // Do any additional setup after loading the view from its nib.
}

-(void)setNavigationBarTitleForRequest:(ConciergeDetailObject *)request{
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBarTintColor:colorFromHexString(GRAY_BACKGROUND_COLOR)];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 100, 50)];

    NSString *statusStr;
    if ([request.requestStatus isEqualToString:CLOSE_STATUS]) {
        statusStr = @"Closed";
    }
    else if([request.requestStatus isEqualToString:INPROGRESS_STATUS])
    {
        statusStr = @"In Progress";
    }
    else{
        statusStr = @"Open";
    }
    NSString *caseID = (request.ePCCaseID ? request.ePCCaseID : @"");
    NSString *fullTitle = [NSString stringWithFormat:@"Request #%@\nStatus: %@", caseID,statusStr];
    NSString *requestText = [NSString stringWithFormat:@"Request #%@", caseID];
    NSString *statusText = [NSString stringWithFormat:@"Status: %@",statusStr];
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:fullTitle];
    
     [str addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0,fullTitle.length)];/// Define Range here and also TextColor color which you want
     [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_MarkForMC_BLACK size:FONT_SIZE_18 * SCREEN_SCALE_BASE_WIDTH_375] range:NSMakeRange(0, requestText.length)];
     [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_MarkForMC_BLACK size:FONT_SIZE_14 * SCREEN_SCALE_BASE_WIDTH_375] range:NSMakeRange(requestText.length + 1, statusText.length)];
    
    titleLabel.attributedText = str;
    titleLabel.numberOfLines = 0;
    //    titleLabel.adjustsFontSizeToFitWidth = YES;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    self.navigationItem.titleView = titleLabel;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self trackingScreenByName:@"Request Details"];
//    [self setUpCustomizedPanGesturePopRecognizer];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated{
    //[self setUpCustomizedPanGesturePopRecognizer];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    //[self removeSetupForCustomizedPanGesturePopRecognizer];
}


-(void)initView {
    contentTextView.textContainerInset = UIEdgeInsetsMake(18, 18, 18, 18);
    if([self.resquestStatus isEqualToString:CLOSE_STATUS] || [self.resquestMode isEqualToString:CANCEL_MODE])
    {
        self.editBtn.enabled = NO;
        self.cancelBtn.enabled = NO;
        [self.editBtn setDisableStyle];
        [self.cancelBtn setDisableStyle];
    }
    else{
        [self.editBtn setDefaultStyle];
        [self.cancelBtn setDefaultStyle];
    }
    [self.duplicateBtn setDefaultStyle];
   
    
    [self.editBtn setTitle:NSLocalizedString(@"Edit", nil) forState:UIControlStateNormal];
    [self.cancelBtn setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
    [self.duplicateBtn setTitle:NSLocalizedString(@"Duplicate", nil) forState:UIControlStateNormal];
    
}

-(void)initData {

    wsRequestConcierge = [[WSRequestConcierge alloc] init];
    wsRequestConcierge.delegate = self;
    wsRequestConcierge.task = WS_GET_DETAIL_CONCIERGE;
    
    wSB2CCreateConciergeCase = [[WSB2CCreateConciergeCase alloc] init];
    wSB2CCreateConciergeCase.delegate = self;
    [self getData];
    
}

-(void)getData {
    if (self.transactionID) {
        [self startActivityIndicator];
        NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
//        [param setValue:self.epcCaseID forKey:@"EPCCaseID"];
        [param setValue:self.transactionID forKey:@"TransactionID"];
        [wsRequestConcierge getConciergeDetail:param];
    }
    
}

- (NSMutableAttributedString*) textViewLineSpacing:(NSString*)string{
    
    NSString *textViewText = string;
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:textViewText];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 10;
    NSDictionary *dict = @{NSParagraphStyleAttributeName : paragraphStyle , NSFontAttributeName : [UIFont fontWithName:FONT_MarkForMC_REGULAR size:(18.0f * SCREEN_SCALE_BASE_WIDTH_375)]};
    [attributedString addAttributes:dict range:NSMakeRange(0, [textViewText length])];
    return attributedString;
}

-(void)reloadMyRequest {
//    UIViewController *vc = [self.navigationController.viewControllers objectAtIndex:1];
//    if ([vc isKindOfClass:[MyRequestPagerViewController class]]) {
//        MyRequestPagerViewController *requestVC = (MyRequestPagerViewController*)vc;
//        requestVC.isReaload = true;
//    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadMyRequestWithoutMsg" object:nil];
    [self.navigationController popViewControllerAnimated:true];
    
    
}

#pragma mark - Data Load Delegate
-(void)loadDataDoneFrom:(WSBase*)ws {
    [self stopActivityIndicator];
    if (ws.task == WS_CANCEL_CONCIERGE) {
        [self reloadMyRequest];
        [self trackingEventByName:@"Request accepted" withAction:SubmitActionType withCategory:RequestCategoryType];
    }else {
        if ([ws isKindOfClass:[WSB2CCreateConciergeCase class]]) {
            [self reloadMyRequest];
        }else{
            conciergeDetail = ws.data[0];
    
            [self setNavigationBarTitleForRequest:conciergeDetail];
            displayingData = [AskConciergeRequest buildRequestDetailForConciergeData:conciergeDetail];
            [self.tableView reloadData];
        }
    }
}

-(void)loadDataFailFrom:(id<WSBaseProtocol>)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message{
    [self stopActivityIndicator];
    NSString *title;
    if (!message || [message isEqualToString:@""]) {
        message = NSLocalizedString(@"An error has occurred. Check your Internet settings or try again.", nil);
        title = NSLocalizedString(@"Unable to get data", nil);
    }else{
        title = NSLocalizedString(@"ERROR!", nil);
    }
    
    [self showActiveAlertWithTitle:title withMessage:message withFistBtnTitle:NSLocalizedString(@"Settings", nil) forFirstAction:^{
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"prefs:root"]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root"]];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root"]];
        }
    } withSecondBtnTitle:NSLocalizedString(@"OK", nil) forSeconAction:^{
        
    }];
    
    if (ws.task == WS_CANCEL_CONCIERGE) {
        [self trackingEventByName:@"Request denied" withAction:SubmitActionType withCategory:RequestCategoryType];
    }
}

- (void)loadDataFailFrom:(id<BaseResponseObjectProtocol>)result withErrorCode:(NSInteger)errorCode{
    [self stopActivityIndicator];
    [self showCommonAlertForFaildLoodingData];
    
    if (result.task == WS_CANCEL_CONCIERGE) {
        [self trackingEventByName:@"Request denied" withAction:SubmitActionType withCategory:RequestCategoryType];
    }
}

- (IBAction)cancelRequestAction:(id)sender {
    [self showActiveAndDismissableAlertWithTitle:@"" withMessage:NSLocalizedString(@"cancel_request_confirmation", nil) withFistBtnTitle:NSLocalizedString(@"Call Concierge", nil) forFirstAction:^{
        [self callConcierge];
    } withSecondBtnTitle:NSLocalizedString(@"Cancel anyway", nil) forSeconAction:^{
        [self startActivityIndicator];
        NSString *requestDetail;
        ConciergeDetailObject *tempObject = conciergeDetail;
        if (conciergeDetail.requestType && conciergeDetail.requestType != nil && ![conciergeDetail.requestType isEqualToString:@""]) {
            requestDetail = [NSString stringWithFormat:@"CategoryName: %@ - %@",[tempObject getStringType:conciergeDetail.requestType], conciergeDetail.requestDetail];
        }else{
            requestDetail = conciergeDetail.requestDetail;
        }
        
        NSDictionary *dict = @{@"RequestDetails": requestDetail,
                               @"TransactionID":(conciergeDetail.transactionId ? conciergeDetail.transactionId : @""),
                               //@"requestType":self.categoryName,
                               //@"requestCity":self.cityName,
                               @"phone":@"false",
                               @"mail":@"true",
                               @"editType":CANCEL_EDIT_TYPE};
        [wSB2CCreateConciergeCase askConciergeWithMessage:dict];
    }];
}


-(IBAction)changeRequestAction:(id)sender
{
    AskConciergeLXViewController *askConciergeVC = [[AskConciergeLXViewController alloc] init];
    askConciergeVC.conciergeDetail = conciergeDetail;
    askConciergeVC.categoryName = conciergeDetail.requestType;
    [self pushVCFromBottom:askConciergeVC];
}

-(IBAction)createRequestAction:(id)sender
{
    AskConciergeLXViewController *askConciergeVC = [[AskConciergeLXViewController alloc] init];
    ConciergeDetailObject *copyConconciergeDetail = [conciergeDetail copy];
    
    copyConconciergeDetail.transactionId = @"";
    copyConconciergeDetail.isCreated = NO;
//    copyConconciergeDetail.createdDate = @"";
//    copyConconciergeDetail.eventDate = @"";
//    copyConconciergeDetail.pickUpDate = @"";
//    copyConconciergeDetail.startDate = @"";
//    copyConconciergeDetail.endDate = @"";
//    copyConconciergeDetail.startDateWithoutTime = @"";
//    copyConconciergeDetail.endDateWithoutTime = @"";
    
    askConciergeVC.conciergeDetail = copyConconciergeDetail;
    askConciergeVC.categoryName = copyConconciergeDetail.requestType;
    askConciergeVC.isDuplicateRequest = YES;
    
    [self pushVCFromBottom:askConciergeVC];
}

#pragma mark - UITableView DataSource & Delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  displayingData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MyRequestDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyRequestDetailTableViewCell" forIndexPath:indexPath];
    [cell setupDataForCellWithData:displayingData[indexPath.row]];
    return cell;
}
@end
