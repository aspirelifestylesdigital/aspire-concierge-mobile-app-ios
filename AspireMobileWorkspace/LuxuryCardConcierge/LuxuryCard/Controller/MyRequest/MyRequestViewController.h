//
//  MyRequestViewController.h
//  LuxuryCard
//
//  Created by Nghia Dinh on 9/8/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "BaseViewController.h"
#import "MyRequestPagerViewController.h"
#import "EnumConstant.h"
#import "ConciergeObject.h"


@interface MyRequestViewController : BaseViewController

@property (nonatomic) MyRequestType requestType;
@property (weak, nonatomic) MyRequestPagerViewController *parrentVC;
@property NSMutableArray *requestArray;
@property (weak, nonatomic) IBOutlet UIButton *myRequestBtn;
@property (weak, nonatomic) IBOutlet UITableView *myRequestTableView;

-(void)reloadDataForViewController:(NSInteger) index;
-(void)loadMoreIndicatorIcon:(BOOL)loading;
- (IBAction)createNewRequestAction:(id)sender;
@end
