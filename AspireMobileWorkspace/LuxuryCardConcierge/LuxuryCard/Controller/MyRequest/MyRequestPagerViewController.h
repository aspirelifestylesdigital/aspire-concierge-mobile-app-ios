//
//  MyRequestPagerViewController.h
//  LuxuryCard
//
//  Created by Nghia Dinh on 9/13/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "BaseViewController.h"
#import "EnumConstant.h"
#import "BaseConciergeObject.h"

@interface MyRequestPagerViewController : BaseViewController

@property BOOL isFullItem;
@property BOOL isLoadMore;
@property BOOL isReaload;
@property (weak, nonatomic) IBOutlet UILabel *myrequestTitle;
@property (weak, nonatomic) IBOutlet UIButton *myRequestBtn;
@property (weak, nonatomic) IBOutlet UIView *vBackGround;

-(void)loadMoreData;
-(void)updateData:(BaseConciergeObject*)data;
- (IBAction)createNewRequestAction:(id)sender;
@end
