//
//  MyRequestThanksViewController.h
//  LuxuryCard
//
//  Created by user on 11/9/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "BaseViewController.h"

@interface MyRequestThanksViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIImageView *imgThanks;
@property (weak, nonatomic) IBOutlet UILabel *lbThanks;
@property (weak, nonatomic) IBOutlet UILabel *lbDesThanks;
@property (weak, nonatomic) IBOutlet UIButton *btnCall;
@property (weak, nonatomic) IBOutlet UIButton *btnNewRequest;
@property (weak, nonatomic) IBOutlet UIButton *btnMyRequest;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@end
