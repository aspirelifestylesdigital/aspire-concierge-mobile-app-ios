//
//  PreferencesViewController.m
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 7/24/17.
//  Copyright © 2017 Sunrixe Software Solutions Corporation. All rights reserved.
//

#import "PreferencesViewController.h"
#import "AppData.h"
#import "UtilStyle.h"
#import "UIButton+Extension.h"
#import "ControlStyleHeader.h"
#import "UILabel+Extension.h"

#import "WSPreferences.h"
#import "PreferenceObject.h"

#define DEFAULT_STRING_PLACEHOLDER_DINING          @"Select your preferred cuisine"
#define DEFAULT_STRING_PLACEHOLDER_HOTEL           @"Please select a rating"
#define DEFAULT_STRING_PLACEHOLDER_TRASPORTATION   @"Select a preferred vehicle type"


@interface PreferencesViewController () <DropDownViewDelegate, DataLoadDelegate> {
    NSMutableArray *arrayPreferences, *listToAdd, *listToUpdate;
    BOOL isAddingPreference;
}

@property (nonatomic, strong) WSPreferences *wsPreferences;

@end

@implementation PreferencesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self getPreferenceData];
    [self createBackBarButton];
    [self setNavigationBarWithDefaultColorAndTitle:@"My Preferences"];
    [self setupView];
    isHiddenTabBar = YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self trackingScreenByName:@"Preferences"];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.diningDropDownView setBottomBolderDefaultColor];
        [self.hotelDropDownView setBottomBolderDefaultColor];
        [self.transportationDropDownView setBottomBolderDefaultColor];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Process Data

- (void)getPreferenceData {
    
    isAddingPreference = NO;
    arrayPreferences = [[NSMutableArray alloc] init];
    arrayPreferences = [SessionData shareSessiondata].arrayPreferences;
    [self setPreferenceData];
}

- (void)setPreferenceData{
    self.diningDropDownView.title = DEFAULT_STRING_PLACEHOLDER_DINING;
    self.hotelDropDownView.title = DEFAULT_STRING_PLACEHOLDER_HOTEL;
    self.transportationDropDownView.title = DEFAULT_STRING_PLACEHOLDER_TRASPORTATION;
    
    if (arrayPreferences.count > 0) {
        for (PreferenceObject *preferece in arrayPreferences) {
            switch (preferece.type) {
                case DiningPreferenceType:
                {
                    if (preferece.value.length > 0 && ![preferece.value isEqualToString:@"na"]) {
                        self.diningDropDownView.title = preferece.value;
                        self.diningDropDownView.titleColor = colorFromHexString(TEXTFIELD_TEXT_COLOR_STANDARD);
                        //[self.diningDropDownView layoutSubviews];
                    }
                }
                    break;
                case HotelPreferenceType:
                {
                    if (preferece.value.length > 0 && ![preferece.value isEqualToString:@"na"]) {
                        self.hotelDropDownView.title = preferece.value;
                        self.hotelDropDownView.titleColor = colorFromHexString(TEXTFIELD_TEXT_COLOR_STANDARD);
                        //[self.hotelDropDownView layoutSubviews];
                    }
                }
                    break;
                case TransportationPreferenceType:
                {
                    if (preferece.value.length > 0 && ![preferece.value isEqualToString:@"na"]) {
                        self.transportationDropDownView.title = preferece.value;
                        self.transportationDropDownView.titleColor = colorFromHexString(TEXTFIELD_TEXT_COLOR_STANDARD);
                        //[self.transportationDropDownView layoutSubviews];
                    }
                }
                    break;
                default:
                    break;
            }
        }
    }
    self.diningDropDownView.valueStatus = DefaultValueStatus;
    self.hotelDropDownView.valueStatus = DefaultValueStatus;
    self.transportationDropDownView.valueStatus = DefaultValueStatus;
}

#pragma mark - Setup
- (void)setupView {

    [self.dinningTitleLbl setDefaultStyleForTextFieldTitle];
    [self.hotelTitleLbl setDefaultStyleForTextFieldTitle];
    [self.transportationTitleLbl setDefaultStyleForTextFieldTitle];
    
    self.diningDropDownView.itemTextColor = [UIColor blackColor];
    self.hotelDropDownView.itemTextColor = [UIColor blackColor];
    self.transportationDropDownView.itemTextColor = [UIColor blackColor];
    
    self.diningDropDownView.leadingTitle = 0.0f;
    self.hotelDropDownView.leadingTitle = 0.0f;
    self.transportationDropDownView.leadingTitle = 0.0f;
    
    UIFont *titleFont = [UIFont fontWithName:FONT_MarkForMC_MED size:FONT_SIZE_18*SCREEN_SCALE_BASE_WIDTH_375];
    [self.diningDropDownView setTitleFont:titleFont];
    [self.hotelDropDownView setTitleFont:titleFont];
    [self.transportationDropDownView setTitleFont:titleFont];
    
    UITapGestureRecognizer *tappedBackground = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapBackground)];
    tappedBackground.delegate = self;
    [self.view addGestureRecognizer:tappedBackground];
    
    self.line1View.backgroundColor = [UIColor clearColor];
    self.line2View.backgroundColor = [UIColor clearColor];
    
    self.diningDropDownView.listItems = [[AppData getPreferencesItems] objectForKey:@"DINING"];
    self.diningDropDownView.delegate = self;
    
    
    self.hotelDropDownView.listItems = [[AppData getPreferencesItems] objectForKey:@"HOTEL"];
    self.hotelDropDownView.delegate = self;
    
    self.transportationDropDownView.listItems = [[AppData getPreferencesItems] objectForKey:@"TRANSPORTATION"];
    self.transportationDropDownView.delegate = self;
    
    
    [self.updateButton setDefaultStyle];
    [self.cancelButton setDefaultStyle];

    
    [self.updateButton setTitle:NSLocalizedString(@"Update", nil) forState:UIControlStateNormal];
    [self.cancelButton setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];

    
    [self setButtonStatus:NO];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.topLayoutConstraintDiningView.constant = (IPAD) ? 10.f : 30.0f;
    });

}
- (void)tapBackground {
    [self.diningDropDownView didTapBackground];
    [self.hotelDropDownView didTapBackground];
    [self.transportationDropDownView didTapBackground];
}

- (void)setButtonStatus:(BOOL)status {
    self.updateButton.enabled = status;
    self.cancelButton.enabled = status;
    
    if(status){
        [self.updateButton setDefaultStyle];
        [self.cancelButton setDefaultStyle];
    }
    else{
        [self.updateButton setDisableStyle];
        [self.cancelButton setDisableStyle];
    }
}

- (BOOL)isChangeData{
    if (arrayPreferences > 0) {
        for (PreferenceObject *preference in arrayPreferences) {
            switch (preference.type) {
                case DiningPreferenceType:
                {
                    NSString *valueTitle = ([self.diningDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_DINING]) ? @"" : self.diningDropDownView.title;
                    if ([valueTitle.uppercaseString isEqualToString:preference.value.uppercaseString]) {
                        self.diningDropDownView.valueStatus = DefaultValueStatus;
                    }else{
                        self.diningDropDownView.valueStatus = UpdateValueStatus;
                    }
                }
                    break;
                case HotelPreferenceType:
                {
                    NSString *valueTitle = ([self.hotelDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_HOTEL]) ? @"" : self.hotelDropDownView.title;
                    if ([valueTitle.uppercaseString isEqualToString:preference.value.uppercaseString]) {
                        self.hotelDropDownView.valueStatus = DefaultValueStatus;
                    }else{
                        self.hotelDropDownView.valueStatus = UpdateValueStatus;
                    }
                }
                    break;
                case TransportationPreferenceType:
                {
                    NSString *valueTitle = ([self.transportationDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_TRASPORTATION]) ? @"" : self.transportationDropDownView.title;
                    if ([valueTitle.uppercaseString isEqualToString:preference.value.uppercaseString]) {
                        self.transportationDropDownView.valueStatus = DefaultValueStatus;
                    }else{
                        self.transportationDropDownView.valueStatus = UpdateValueStatus;
                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
    }
    return (self.diningDropDownView.valueStatus != DefaultValueStatus || self.hotelDropDownView.valueStatus != DefaultValueStatus || self.transportationDropDownView.valueStatus != DefaultValueStatus);
}

#pragma mark - DropDownView Delegate
- (void)didSelectItem:(DropDownView *)dropDownView atIndex:(int)index{
    
    NSString *valueSelected = [dropDownView.listItems objectAtIndex:index];
    if ([valueSelected.uppercaseString isEqualToString:@"NONE"]) {
        
        if (dropDownView == self.diningDropDownView) {
            self.diningDropDownView.titleColor = colorFromHexString(TEXTFIELD_PLACEHOLDER_COLOR_STANDARD);
            self.diningDropDownView.title = DEFAULT_STRING_PLACEHOLDER_DINING;
            self.diningDropDownView.valueStatus = DefaultValueStatus;
        }else if (dropDownView == self.hotelDropDownView) {
            self.hotelDropDownView.titleColor = colorFromHexString(TEXTFIELD_PLACEHOLDER_COLOR_STANDARD);
            self.hotelDropDownView.title = DEFAULT_STRING_PLACEHOLDER_HOTEL;
            self.hotelDropDownView.valueStatus = DefaultValueStatus;
        }else if (dropDownView == self.transportationDropDownView) {
            self.transportationDropDownView.titleColor = colorFromHexString(TEXTFIELD_PLACEHOLDER_COLOR_STANDARD);
            self.transportationDropDownView.title = DEFAULT_STRING_PLACEHOLDER_TRASPORTATION;
            self.transportationDropDownView.valueStatus = DefaultValueStatus;
        }
        
    }
    [self setButtonStatus:[self isChangeData]];
}
- (void)didShow:(DropDownView *)dropDownView {
    if (dropDownView == self.diningDropDownView) {
        [self.hotelDropDownView didTapBackground];
        [self.transportationDropDownView didTapBackground];
    }else if (dropDownView == self.hotelDropDownView){
        [self.diningDropDownView didTapBackground];
        [self.transportationDropDownView didTapBackground];
    }else if (dropDownView == self.transportationDropDownView) {
        [self.diningDropDownView didTapBackground];
        [self.hotelDropDownView didTapBackground];
    }
        
}
#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    
    UIView *viewTouch = touch.view.superview;
    if ([viewTouch isKindOfClass:[UITableViewCell class]]) {
        return NO;
    }
    
    return YES;
    
}

#pragma mark - Actions Button
- (IBAction)UpdateAction:(id)sender {
    
    if(!isNetworkAvailable()) {
        [self showErrorNetwork];
        
    }else{
        [self setButtonStatus:NO];
        [self startActivityIndicator];
        
        listToAdd = [[NSMutableArray alloc] init];
        listToUpdate = [[NSMutableArray alloc] init];
        
        for (UIView *item in self.view.subviews) {
            if ([item isKindOfClass:[DropDownView class]]) {
                if (((DropDownView*)item).valueStatus == NewValueStatus) {
                    [listToAdd addObject:item];
                }else if(((DropDownView*)item).valueStatus == UpdateValueStatus){
                    [listToUpdate addObject:item];
                }
            }
        }
        if (listToAdd.count > 0 && listToUpdate.count > 0) {
            isAddingPreference = YES;
            [self addPreference];
        }else{
            if (listToAdd.count > 0) {
                [self addPreference];
            }else if (listToUpdate.count > 0){
                [self updatePreference];
            }
        }
    }

}

- (IBAction)CancelAction:(id)sender {
    [self setPreferenceData];
    [self setButtonStatus:NO];
}
#pragma mark - Private Methods
- (void)addPreference{
    
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    NSMutableArray *arraySubDictValues = [[NSMutableArray alloc] init];
    for (DropDownView *item in listToAdd) {
        if (item == self.diningDropDownView) {
            NSMutableDictionary *diningDict = [[NSMutableDictionary alloc] init];
            [diningDict setValue:@"Dining" forKey:@"Type"];
            [diningDict setValue:([self.diningDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_DINING])?@"":self.diningDropDownView.title forKey:@"CuisinePreferences"];
            [arraySubDictValues addObject:diningDict];
            
        }else if (item == self.hotelDropDownView) {
            NSMutableDictionary *hotelDict = [[NSMutableDictionary alloc] init];
            [hotelDict setValue:@"Hotel" forKey:@"Type"];
            [hotelDict setValue:([self.hotelDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_HOTEL])?@"":self.hotelDropDownView.title forKey:@"Preferredstarrating"];
            [arraySubDictValues addObject:hotelDict];
        }else if (item == self.transportationDropDownView) {
            NSMutableDictionary *transportationDict = [[NSMutableDictionary alloc] init];
            [transportationDict setValue:@"Car Rental" forKey:@"Type"];
            [transportationDict setValue:([self.transportationDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_TRASPORTATION])?@"":self.transportationDropDownView.title forKey:@"PreferredRentalVehicle"];
            [arraySubDictValues addObject:transportationDict];
        }
    }
    
    [dataDict setValue:arraySubDictValues forKey:@"PreferenceDetails"];
    self.wsPreferences = [[WSPreferences alloc] init];
    self.wsPreferences.delegate = self;
    [self.wsPreferences addPreference:dataDict];
    
}

- (void)updatePreference{
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    NSMutableArray *arraySubDictValues = [[NSMutableArray alloc] init];
    for (DropDownView *item in listToUpdate) {
        if (item == self.diningDropDownView) {
            NSMutableDictionary *diningDict = [[NSMutableDictionary alloc] init];
            [diningDict setValue:[self getPreferenceIDByType:DiningPreferenceType] forKey:@"MyPreferencesId"];
            [diningDict setValue:@"Dining" forKey:@"Type"];
            [diningDict setValue:([self.diningDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_DINING])?@"":self.diningDropDownView.title forKey:@"CuisinePreferences"];
            [arraySubDictValues addObject:diningDict];
            
        }else if (item == self.hotelDropDownView) {
            NSMutableDictionary *hotelDict = [[NSMutableDictionary alloc] init];
            [hotelDict setValue:[self getPreferenceIDByType:HotelPreferenceType] forKey:@"MyPreferencesId"];
            [hotelDict setValue:@"Hotel" forKey:@"Type"];
            [hotelDict setValue:([self.hotelDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_HOTEL])?@"":self.hotelDropDownView.title forKey:@"Preferredstarrating"];
            [arraySubDictValues addObject:hotelDict];
        }else if (item == self.transportationDropDownView) {
            NSMutableDictionary *transportationDict = [[NSMutableDictionary alloc] init];
            [transportationDict setValue:[self getPreferenceIDByType:TransportationPreferenceType] forKey:@"MyPreferencesId"];
            [transportationDict setValue:@"Car Rental" forKey:@"Type"];
            [transportationDict setValue:([self.transportationDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_TRASPORTATION])?@"":self.transportationDropDownView.title forKey:@"PreferredRentalVehicle"];
            [arraySubDictValues addObject:transportationDict];
        }
    }
    [dataDict setValue:arraySubDictValues forKey:@"PreferenceDetails"];
    
    self.wsPreferences = [[WSPreferences alloc] init];
    self.wsPreferences.delegate = self;
    [self.wsPreferences updatePreference:dataDict];
}

- (void)getPreference{
    [self startActivityIndicator];
    self.wsPreferences = [[WSPreferences alloc] init];
    self.wsPreferences.delegate = self;
    [self.wsPreferences getPreference];
    
}
-(NSString*)getPreferenceIDByType:(PreferenceType)type{
    for (PreferenceObject *preference in arrayPreferences) {
        if (preference.type == type) {
            return preference.preferenceID;
        }
    }
    return @"";
}

#pragma mark - Delegate from API
-(void)loadDataDoneFrom:(WSBase*)ws{
    if (ws.task == WS_GET_MY_PREFERENCE) {
        arrayPreferences = [SessionData shareSessiondata].arrayPreferences;
        [self setPreferenceData];
        [self stopActivityIndicator];
        
        [self showAlertWithTitle:@"" andMessage:NSLocalizedString(@"You have successfully updated your preference(s).", nil) andBtnTitle:NSLocalizedString(@"OK", nil)];
    }else if (ws.task == WS_ADD_MY_PREFERENCE){
        if (isAddingPreference) {
            [self updatePreference];
            isAddingPreference = NO;
        }else{
            [self getPreference];
        }
    }else if (ws.task == WS_UPDATE_MY_PREFERENCE){
        [self getPreference];
    }
}


-(void)loadDataFailFrom:(WSBase *)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message {
    
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
         [self showErrorNetwork];
    }else{
        
        [self showActiveAlertWithTitle:NSLocalizedString(@"ERROR!", nil) withMessage:NSLocalizedString(@"An error has occurred. Check your Internet settings or try again.", nil) withFistBtnTitle:NSLocalizedString(@"alert_error_title", nil) forFirstAction:^{
            [self setButtonStatus:YES];
        } withSecondBtnTitle:@"" forSeconAction:nil];
    }
}

@end
