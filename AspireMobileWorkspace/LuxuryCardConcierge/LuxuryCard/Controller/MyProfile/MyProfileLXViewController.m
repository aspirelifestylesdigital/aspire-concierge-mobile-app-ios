//
//  MyProfileLXViewController.m
//  LuxuryCard
//
//  Created by user on 3/22/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "MyProfileLXViewController.h"
//
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/13/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "ImageUtilities.h"
#import "NSString+Utis.h"
#import "Constant.h"
#import "UIButton+Extension.h"
#import "HomeLXViewController.h"

#import "AppDelegate.h"
#import "UserObject.h"
#import "UITextField+Extensions.h"
#import "UtilStyle.h"
#import "WSB2CGetUserDetails.h"
#import "NSString+AESCrypt.h"


@interface MyProfileLXViewController()<DataLoadDelegate>


@end

@implementation MyProfileLXViewController
{
    NSString *firstNameOnTyping;
    NSString *lastNameOnTyping;
    NSString *emailOnTyping;
    NSString *phoneOnTyping;
    NSString *zipCodeTyping;
    NSString *passwordOnTyping;
    NSString *confirmPasswordOnTyping;
    BOOL isTempUseLocation;
    IBOutlet NSLayoutConstraint *btSC;
    UIButton *saveButton;
    WSB2CGetUserDetails *wsGetUser;
    
}
- (void)viewDidLoad
{
    isTempUseLocation = [[SessionData shareSessiondata] isUseLocation];
    self.isUpdateProfile = YES;
    [super viewDidLoad];
    
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"My Profile", nil)];
    [self createSaveButton];
    backupBottomConstraint = self.viewActionBottomConstraint.constant;
    [self setButtonStatus:NO];
    
//  Hidden Tab Bar
    isHiddenTabBar = YES;
    btSC.constant = 0;//80 * SCREEN_SCALE;
    self.emailText.textColor = colorFromHexString(@"#99A1A8");
    [self getUserProfile];
    [self setTextViewsDefaultBottomBolder];
    [self.view layoutIfNeeded];
    
    [self setDefaultData];
    isHiddenTabBar = YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //  Set up Pop pan gesture
//    [self setUpCustomizedPanGesturePopRecognizer];
    dispatch_async(dispatch_get_main_queue(), ^{
        /*
        UIImageView *dropDownImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.salutationDropDown.frame.size.width - (16.0f + 15.0f), (self.salutationDropDown.frame.size.height - 30.0f)/2, 30.0f, 30.0f)];
        
        [dropDownImage setImage:[UIImage imageNamed:@"back_white_icon"]];
        dropDownImage.transform = CGAffineTransformMakeRotation(-M_PI_2);
        self.salutationDropDown.dropDownImage = dropDownImage;
        [self.salutationDropDown addSubview:dropDownImage];
        
        UIImageView *dropDownImage1 = [[UIImageView alloc] initWithFrame:CGRectMake(self.countryCodeDropDown.frame.size.width - (16.0f + 15.0f), (self.countryCodeDropDown.frame.size.height - 30.0f)/2, 30.0f, 30.0f)];
        [dropDownImage1 setImage:[UIImage imageNamed:@"back_white_icon"]];
        dropDownImage1.transform = CGAffineTransformMakeRotation(-M_PI_2);
        self.countryCodeDropDown.dropDownImage = dropDownImage1;
        [self.countryCodeDropDown addSubview:dropDownImage1];*/
        
    });
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self trackingScreenByName:@"My profile"];
//    self.salutationDropDown.dropDownImage.frame = CGRectMake(self.salutationDropDown.frame.size.width - (16.0f + 15.0f), (self.salutationDropDown.frame.size.height - 30.0f)/2, 30.0f, 30.0f);
//    self.countryCodeDropDown.dropDownImage.frame = CGRectMake(self.countryCodeDropDown.frame.size.width - (16.0f + 15.0f), (self.countryCodeDropDown.frame.size.height - 30.0f)/2, 30.0f, 30.0f);
    
    //  Set up Pop pan gesture
//    [self setUpCustomizedPanGesturePopRecognizer];
    
    /*
    if(self.navigationController.viewControllers.count == 1)
    {
        HomeLXViewController *homeView = [[HomeLXViewController alloc] init];
        [self.navigationController setViewControllers:@[homeView,self]];
    }
    */
    
    self.submitButton.hidden = YES;
    //self.cancelButton.hidden = YES;
    self.commitmentLabel.hidden = YES;
}

-(void) createSaveButton
{
    saveButton = [[UIButton alloc] init];
    [saveButton setFrame:CGRectMake(0, 0, 65.0f*SCREEN_SCALE, 60.0f*SCREEN_SCALE)];
    [saveButton addTarget:self action:@selector(saveProfile) forControlEvents:UIControlEventTouchUpInside];
    [saveButton setTitle:NSLocalizedString(@"Save", nil) forState:UIControlStateNormal];
    UIBarButtonItem *conciergeBarItem = [[UIBarButtonItem alloc] initWithCustomView:saveButton];
    self.navigationItem.rightBarButtonItem = conciergeBarItem;
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (isTempUseLocation != isUseLocation) {
        [[SessionData shareSessiondata] setIsUseLocation:isTempUseLocation];
    }
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}

-(void)getUserProfile {
    [self startActivityIndicator];
    wsGetUser = [[WSB2CGetUserDetails alloc] init];
    wsGetUser.delegate = self;
    [wsGetUser getUserDetails];
}


- (void)setDefaultData {
    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
    if (profileDictionary) {
        firstNameOnTyping = [profileDictionary objectForKey:keyFirstName];
        lastNameOnTyping = [profileDictionary objectForKey:keyLastName];
        emailOnTyping = [profileDictionary objectForKey:keyEmail];
        phoneOnTyping = [profileDictionary objectForKey:keyMobileNumber];
        passwordOnTyping = [profileDictionary objectForKey:[pssWrd_EncryptedKey AES256DecryptWithKey:EncryptKey]];
        zipCodeTyping = [profileDictionary objectForKey:keyZipCode];
    }
}

- (void)initView{
    self.phoneNumberText.tag = PHONE_NUMBER_TEXTFIELD_TAG;
    [self setButtonFrame];
    
    // Text Style For Greet Message
    NSString *message = NSLocalizedString(@"commitment_my_profile_message", nil);
    self.commitmentLabel.attributedText = [UtilStyle setLargeSizeStyleForLabelWithMessage:message];
    
    [self setCheckBoxState:isCheck];
    self.commitmentLabel.text = NSLocalizedString(@"commitment_my_profile_message", nil);
    [self.updateButton setTitle:NSLocalizedString(@"update_button", nil) forState:UIControlStateNormal];
    [self.cancelButtonMyProfile setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:BUTTON_FONT_NAME_STANDARD size:(BUTTON_FONT_SIZE_STANDARD * SCREEN_SCALE)]};
    self.updateButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.updateButton.titleLabel.text attributes:attributes];
    self.cancelButtonMyProfile.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.cancelButtonMyProfile.titleLabel.text attributes:attributes];
    
//    [self.updateButtonView setBackgroundColor:colorFromHexString(DEFAULT_BACKGROUND_COLOR)];
//    [self.cancelButtonView setBackgroundColor:colorFromHexString(DEFAULT_BACKGROUND_COLOR)];
    
}

- (void) setButtonFrame{
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)handleKeyboardWillHide:(NSNotification *)paramNotification {
    btSC.constant = 0;//80 * SCREEN_SCALE;
    //    self.viewActionBottomConstraint.constant = backupBottomConstraint;
}

- (void)handleKeyboardWillShow:(NSNotification *)paramNotification {
    
    [self.salutationDropDown didTapBackground];
    [self.countryCodeDropDown didTapBackground];
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    keyboardHeight = keyboardRect.size.height;
    btSC.constant =  keyboardHeight;
    //    self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight + 250, 0);
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

-(void) createAsteriskForTextField:(UITextField *)textField{
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [textField setLeftView:leftView];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    
    asteriskImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"asterisk_icon"]];
    
    asteriskImage.frame = CGRectMake(0.0f, 0.0f, 20.0f, 20.0f);
    asteriskImage.contentMode = UIViewContentModeCenter;
    [textField setRightView:asteriskImage];
    [textField setRightViewMode:UITextFieldViewModeAlways];
}

- (void)changeUseLocation{
    [self setButtonStatus:[self checkFieldIsChangeString]];
}




- (void)updateSuccessRedirect{
    [self isChangeNavigationBackgroundColor:YES];
    [self showActiveAlertWithTitle:@"" withMessage:NSLocalizedString(@"You have successfully updated your profile.", nil) withFistBtnTitle:NSLocalizedString(@"Ok", nil) forFirstAction:^{
        isTempUseLocation = isUseLocation;
        [self getUserInfo];
        [self setTextViewsDefaultBottomBolder];
        [self setButtonStatus:NO];
         [self isChangeNavigationBackgroundColor:NO];
    } withSecondBtnTitle:@"" forSeconAction:nil];
}

- (void) saveProfile{
    [self.view endEditing:YES];
    [self verifyAccountData:YES];
}

- (IBAction)touchUpdate:(id)sender {
    [self.view endEditing:YES];
    [self verifyAccountData:YES];
}

- (IBAction)touchCancel:(id)sender {
    [self.phoneNumberText endEditing:YES];
    [self dismissKeyboard];
    [self setDefaultData];
    [self setTextViewsDefaultBottomBolder];
    [self setButtonStatus:NO];
    [[SessionData shareSessiondata] setIsUseLocation:isTempUseLocation];
    [self getUserInfo];
}

- (void)updateProfileButtonStatus{
    [self setButtonStatus:[self checkFieldIsChangeString]];
}

- (void)checkTextFieldIsChange:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *resultText = [textField.text stringByReplacingCharactersInRange:range
                                                                   withString:string];
    
    if (textField == self.firstNameText) {
        firstNameOnTyping = resultText;
    }else if (textField == self.lastNameText){
        lastNameOnTyping = resultText;
    }else if (textField == self.emailText){
        emailOnTyping = resultText;
    }else if (textField == self.phoneNumberText){
        phoneOnTyping = resultText;
    }else if (textField == self.passwordText){
        passwordOnTyping = resultText;
    }else if (textField == self.zipCodeText){
        zipCodeTyping = resultText;
    }
    [self setButtonStatus:[self checkFieldIsChangeString]];
}

- (void)changeValueDropDown{
    [self setButtonStatus:[self checkFieldIsChangeString]];
}

- (BOOL)checkFieldIsChangeString{
    
    if (isUseLocation != isTempUseLocation) {
        return YES;
    }
    if (firstNameOnTyping.length == 0 && lastNameOnTyping.length == 0 && emailOnTyping.length == 0 && phoneOnTyping.length == 0 && self.salutationDropDown.valueStatus == DefaultValueStatus && zipCodeTyping.length == 0 && self.countryCodeDropDown.valueStatus == DefaultValueStatus) {
        return NO;
    }
    
    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
    if (profileDictionary) {
        Country *currentCountry = [[Country alloc] init];
        NSString *zipCode = [profileDictionary objectForKey:keyZipCode];
        NSString *countryCode = [profileDictionary objectForKey:keyCountryCode];
        if ([countryCode isEqualToString:@"1"]) {
            if (zipCode.length > 0) {
                currentCountry = ([[zipCode substringWithRange:NSMakeRange(0, 1)] isNumber]) ? getCountryByCodeName(@"USA") : getCountryByCodeName(@"CAN");
            }else {
                currentCountry = getCountryByCodeName(@"USA");
            }
        }else{
            currentCountry = getCountryByNumber(countryCode);
        }
        if (![self.salutationDropDown.title isEqualToString:[profileDictionary objectForKey:keySalutation]]) {
            return YES;
        }else if (![[profileDictionary objectForKey:keyFirstName] isEqualToString:firstNameOnTyping]) {
            return YES;
        }else if (![[profileDictionary objectForKey:keyLastName] isEqualToString:lastNameOnTyping]){
            return YES;
        }
        else if (![[profileDictionary objectForKey:keyEmail] isEqualToString:emailOnTyping]){
            return YES;
        }
        else if (![[profileDictionary objectForKey:keyMobileNumber] isEqualToString:phoneOnTyping]){
            return YES;
        }
        else if (![[profileDictionary objectForKey:keyZipCode] isEqualToString:zipCodeTyping]){
            return YES;
        }
        else if (![self.countryCodeDropDown.title isEqualToString:[NSString stringWithFormat:@"%@ (%@)",currentCountry.countryCode, currentCountry.countryNumber]]){
            return YES;
        }
    }
    
    return NO;
}

- (void)setButtonStatus:(BOOL)status{
    saveButton.enabled = status;
    saveButton.alpha = status == YES ? 1 : 0.8f;
    self.updateButton.enabled = status;
    self.cancelButtonMyProfile.enabled = status;
    if(status){
        [self.updateButton setDefaultStyle];
        [self.cancelButtonMyProfile setDisableStyle];
    }
    else{
        [self.updateButton setDisableStyle];
        [self.cancelButtonMyProfile setDisableStyle];
    }
}

#pragma mark - API Delegate

-(void)loadDataDoneFrom:(WSBase*)ws
{
    if (ws.task == WS_UPDATE_USER) {
        if (ws.data) {
            [self setUserDefaultInfoWithDict:ws.data[0]];
        }
        
        [self stopActivityIndicator];
        [self updateSuccessRedirect];
        
    }
    else if(ws.task == WS_B2C_GET_USER_DETAILS)
    {
        [self setDefaultData];
        
        NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
        if (profileDictionary) {
            
            NSString *salutation = [profileDictionary objectForKey:keySalutation];
            if (salutation.length > 1) {
                self.salutationDropDown.titleColor = colorFromHexString(TEXTFIELD_TEXT_COLOR_STANDARD);
                self.salutationDropDown.title = salutation;
            }
            NSString *countryCode = [profileDictionary objectForKey:keyCountryCode];
            if (countryCode.length > 0) {
                Country *currentCountry = [[Country alloc] init];
                NSString *zipCode = [profileDictionary objectForKey:keyZipCode];
                if ([countryCode isEqualToString:@"1"]) {
                    if (zipCode.length > 0) {
                        currentCountry = ([[zipCode substringWithRange:NSMakeRange(0, 1)] isNumber]) ? getCountryByCodeName(@"USA") : getCountryByCodeName(@"CAN");
                    }else {
                        currentCountry = getCountryByCodeName(@"USA");
                    }
                }else{
                    currentCountry = getCountryByNumber(countryCode);
                }
                self.countryCodeDropDown.titleColor = colorFromHexString(TEXTFIELD_TEXT_COLOR_STANDARD);
                self.countryCodeDropDown.title = [NSString stringWithFormat:@"%@ (%@)",currentCountry.countryCode, currentCountry.countryNumber];
                self.countryCodeDropDown.titleOther = currentCountry.countryDescription;
            }
            
            self.firstNameText.text = [profileDictionary objectForKey:keyFirstName];
            self.lastNameText.text = [profileDictionary objectForKey:keyLastName];
            self.emailText.text = [profileDictionary objectForKey:keyEmail];
            self.phoneNumberText.text = [profileDictionary objectForKey:keyMobileNumber];
            self.zipCodeText.text = [profileDictionary objectForKey:keyZipCode];
            
//            isUseLocation = [[SessionData shareSessiondata] isUseLocation];
//            [self setImageLocation:isUseLocation];
            
            currentFirstName = [profileDictionary objectForKey:keyFirstName];
            currentEmail = [profileDictionary objectForKey:keyEmail] ;
            currentPhone = [profileDictionary objectForKey:keyMobileNumber];
            currentLastName = [profileDictionary objectForKey:keyLastName];
            currentZipCode = [profileDictionary objectForKey:keyZipCode];
            
            self.firstNameText.text = [currentFirstName createMaskForText:NO];
            self.emailText.text = [currentEmail createMaskForText:YES];
            self.lastNameText.text = [currentLastName createMaskForText:NO];
            self.phoneNumberText.text = [currentPhone createMaskForText:NO];
            self.emailText.enabled = NO;
            self.zipCodeText.text = currentZipCode;
            
        }
        [self stopActivityIndicator];
    }
}

@end

