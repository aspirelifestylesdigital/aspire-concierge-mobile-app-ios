//
//  MyProfileLXViewController.h
//  LuxuryCard
//
//  Created by user on 3/22/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "BaseViewController.h"
#import "CreateProfileViewController.h"

@interface MyProfileLXViewController : CreateProfileViewController
@property (weak, nonatomic) IBOutlet UIView *viewAction;

@property (weak, nonatomic) IBOutlet UIButton *updateButton;

@property (weak, nonatomic) IBOutlet UIButton *cancelButtonMyProfile;
//@property (weak, nonatomic) IBOutlet UIView *updateButtonView;
//@property (weak, nonatomic) IBOutlet UIView *cancelButtonView;


- (IBAction)touchUpdate:(id)sender;
- (IBAction)touchCancel:(id)sender;
@end
