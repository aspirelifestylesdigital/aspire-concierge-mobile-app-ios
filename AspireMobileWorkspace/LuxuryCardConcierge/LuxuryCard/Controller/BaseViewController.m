//
//  BaseViewController.m
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/12/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "BaseViewController.h"
#import "Constant.h"
#import "Common.h"
#import "UIView+Extension.h"
#import "CustomPopTransition.h"
#import "CategoryViewController.h"
#import "ExploreVenueDetailViewController.h"
#import "AppDelegate.h"
#import "HomeLXViewController.h"
#import "ExploreViewController.h"
#import "WebformViewController.h"
#import "AskConciergeRequest.h"
#import "MenuViewController.h"
#import "UITabBarController+HideTabBar.h"
#import "ImageCacheList.h"
#import "MyRequestPagerViewController.h"
#import<CoreTelephony/CTTelephonyNetworkInfo.h>
#import<CoreTelephony/CTCarrier.h>
#import "CMPopTipView.h"
#import "AppData.h"
#import "MyRequestPagerViewController.h"

// views
#import "ViewLog.h"

// library
#import "NSTimer+Block.h"

@interface BaseViewController ()
{
    BOOL didUpdateLayout;
    
    ViewLog* vwLog;
    CMPopTipView *popTipView;
}
@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    didUpdateLayout = NO;
    if(!didUpdateLayout && !isIgnoreScaleView){
        didUpdateLayout = YES;
        resetScaleViewBaseOnScreen(self.view);
    }
    
    if(isShowAskConciergeBarButton)
    {
        [self createConciergeBarButton];
    }
    
    if (isShowAskConciergeButton) {
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            //load your data here.
            dispatch_async(dispatch_get_main_queue(), ^{
                //update UI in main thread.
                [self createConciergeButton];
            });
        });
        
    }
    
    [self.view setBackgroundColorForView];
    if(!isNotChangeNavigationBarColor)
    {
       [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"concierge", nil)];
    }
    [self initView];
    [self initData];

//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    [self isChangeNavigationBackgroundColor:NO];
    
//    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
//    self.navigationController.interactivePopGestureRecognizer.delegate = self;
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(isHiddenTabBar)
    {
        [appDelegate.tabBarController setTabBarHidden:YES animated:YES];
    }
    else
    {
        if(appDelegate.tabBarController.isTabBarHidden)
        {
            [appDelegate.tabBarController setTabBarHidden:NO animated:YES];
        }
    }
    
    isHiddenNavigationBar ? [self.navigationController setNavigationBarHidden:YES] : [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
//{
//    return YES;
//}

- (void) touchDown:(UIGestureRecognizer *)gesture {
    [self hidenKeyboardWhenSwipe];
}

-(void)hidenKeyboardWhenSwipe{

}

- (UIImageView*) createIndicator{
    if (!indicatorImageView) {
        indicatorImageView = [[UIImageView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - SPINNER_BACKGROUND_WIDTH)/2.0,
                                                                           (SCREEN_HEIGHT - SPINNER_BACKGROUND_HEIGHT-NAVIGATION_HEIGHT)/2.0,
                                                                           SPINNER_BACKGROUND_WIDTH*SCREEN_SCALE, SPINNER_BACKGROUND_HEIGHT*SCREEN_SCALE)];
        
        [indicatorImageView setImage:[UIImage imageNamed:@"activity_indicator_icon_1"]];
        //Add more images which will be used for the animation
        indicatorImageView.animationImages =  [NSArray arrayWithObjects:
                                               [UIImage imageNamed:@"activity_indicator_icon_1"],
                                               [UIImage imageNamed:@"activity_indicator_icon_2"],
                                               [UIImage imageNamed:@"activity_indicator_icon_3"],
                                               [UIImage imageNamed:@"activity_indicator_icon_4"],
                                               [UIImage imageNamed:@"activity_indicator_icon_5"],
                                               [UIImage imageNamed:@"activity_indicator_icon_6"],
                                               [UIImage imageNamed:@"activity_indicator_icon_7"],
                                               [UIImage imageNamed:@"activity_indicator_icon_8"],
                                               [UIImage imageNamed:@"activity_indicator_icon_9"],
                                               [UIImage imageNamed:@"activity_indicator_icon_10"],
                                               [UIImage imageNamed:@"activity_indicator_icon_11"],
                                               [UIImage imageNamed:@"activity_indicator_icon_12"],
                                               nil];
        indicatorImageView.animationDuration = 1.0f;
    }
    
    return indicatorImageView;
}

//- (void)appWillEnterForeground:(NSNotification*) noti{
//    [self checkNetworkStatus];
//}
//
//- (void) checkNetworkStatus{
//    if(!isNetworkAvailable())
//    {
//        [self showNetWorkingStatusArlet];
//    }
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initData{
}

- (void)initView{
}
- (void)changePositionForView{
    
}
-(void)dealloc
{
    #ifdef DEBUG
    NSLog(@"dealloc view controller: %@",NSStringFromClass([self class]));
    #endif
}


-(void) createConciergeBarButton
{
//    UIButton *conciergeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 45.0f, 45.0f)];
//    [conciergeButton addTarget:self action:@selector(callConcierge) forControlEvents:UIControlEventTouchUpInside];
//    [conciergeButton setImage:[UIImage imageNamed:@"phone_call"] forState:UIControlStateNormal];
//    UIBarButtonItem *conciergeBarItem = [[UIBarButtonItem alloc] initWithCustomView:conciergeButton];
//    self.navigationItem.rightBarButtonItem = conciergeBarItem;
    
//    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 65.0f*SCREEN_SCALE, 65.0f*SCREEN_SCALE)];
    UIButton *conciergeButton = [[UIButton alloc] init];
    [conciergeButton setFrame:CGRectMake(0, 0, 65.0f*SCREEN_SCALE, 60.0f*SCREEN_SCALE)];
//    [rightView addSubview:conciergeButton];
    [conciergeButton addTarget:self action:@selector(callConcierge) forControlEvents:UIControlEventTouchUpInside];
    [conciergeButton setImage:[UIImage imageNamed:@"phone_call"] forState:UIControlStateNormal];
    UIBarButtonItem *conciergeBarItem = [[UIBarButtonItem alloc] initWithCustomView:conciergeButton];
    self.navigationItem.rightBarButtonItem = conciergeBarItem;
    
}

-(void) createConciergeButton
{
    float marginTop = isIphoneX() == YES ? 50 : 28.0f*SCREEN_SCALE ;
    UIButton *conciergeButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 60, marginTop, 50.0f, 50.0f)];
    [conciergeButton setBackgroundColor:[UIColor clearColor]];
    [conciergeButton addTarget:self action:@selector(callConcierge) forControlEvents:UIControlEventTouchUpInside];
    [conciergeButton setImage:[UIImage imageNamed:@"phone_call"] forState:UIControlStateNormal];
//    [conciergeButton setBackgroundColor:colorFromHexString(DEFAULT_BACKGROUND_COLOR)];
    [self.view addSubview:conciergeButton];
}

- (void)callConcierge{
    if([self isKindOfClass:[ExploreViewController class]] || [self isKindOfClass:[ExploreVenueDetailViewController class]] || [self isKindOfClass:[MyRequestPagerViewController class]] || [self isKindOfClass:[HomeLXViewController class]]){
        [self dismistTooltip];
    }
    [self isChangeNavigationBackgroundColor:YES];
    NSString *message = NSLocalizedString(@"Luxury Card specialists are available 24 hours a day. Please use the International Collect number if you're calling from outside the United States (roaming charges may apply).",nil);
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *callDomestic = [UIAlertAction actionWithTitle:NSLocalizedString(@"(Domestic) 844-724-2500", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self callWithPhoneString:DOMESTIC_PHONE_NUMBER];
        [self isChangeNavigationBackgroundColor:NO];
    }];
    UIAlertAction *callInter = [UIAlertAction actionWithTitle:NSLocalizedString(@"(International Collect) +1 302-255-7444", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self callWithPhoneString:INTERNATIONAL_PHONE_NUMBER];
        [self isChangeNavigationBackgroundColor:NO];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self isChangeNavigationBackgroundColor:NO];
    }];
    
    [alert addAction:callDomestic];
    [alert addAction:callInter];
    [alert addAction:cancel];
    if (IPAD) {
        [alert setModalPresentationStyle:UIModalPresentationPopover];
        
        UIPopoverPresentationController *popPresenter = [alert
                                                         popoverPresentationController];
        popPresenter.sourceView = self.view;
        popPresenter.sourceRect = CGRectMake(self.view.bounds.size.width*0.1, self.view.bounds.size.height - 300, self.view.frame.size.width*0.8 ,300 );
    }else{
        NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
        paraStyle.alignment = NSTextAlignmentCenter;
        
        NSMutableAttributedString *atrStr = [[NSMutableAttributedString alloc] initWithString:message attributes:@{NSParagraphStyleAttributeName:paraStyle,NSForegroundColorAttributeName : colorFromHexString(CALL_CONCIERGE_TITLE_TEXT_COLOR),NSFontAttributeName:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:FONT_SIZE_16*SCREEN_SCALE_BASE_WIDTH_375]}];
        
        [alert setValue:atrStr forKey:@"attributedMessage"];
    }
    
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void) callWithPhoneString:(NSString*)phoneString{
    CTTelephonyNetworkInfo* info = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier* carrier = info.subscriberCellularProvider;
    NSString *isoCountryCode = carrier.isoCountryCode;
    
    if (!isoCountryCode && iosVersion() < 10.0) {
        [self isChangeNavigationBackgroundColor:YES];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:phoneString message:@"" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *call = [UIAlertAction actionWithTitle:@"Call" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSString *phoneStr = [[NSString alloc] initWithFormat:@"tel:%@",phoneString];
            NSURL *phoneURL = [[NSURL alloc] initWithString:phoneStr];
            [[UIApplication sharedApplication] openURL:phoneURL];
            [self isChangeNavigationBackgroundColor:NO];
        }];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self isChangeNavigationBackgroundColor:NO];
        }];
        
        [alert addAction:call];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        NSString *phoneStr = [[NSString alloc] initWithFormat:@"tel:%@",phoneString];
        NSURL *phoneURL = [[NSURL alloc] initWithString:phoneStr];
        [[UIApplication sharedApplication] openURL:phoneURL];
        
    }
    
}

-(void)createBackBarButton
{
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [menuButton addTarget:self action:@selector(touchBack) forControlEvents:UIControlEventTouchUpInside];
    
    [menuButton setImage:[UIImage imageNamed:@"back_white_icon"] forState:UIControlStateNormal];
//    [menuButton setTitleColor:colorFromHexString(GRAY_COLOR) forState:UIControlStateHighlighted];
    
    UIBarButtonItem *menuButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    self.navigationItem.leftBarButtonItem = menuButtonItem;
    
    //  Set up Pop pan gesture
    //[self setUpCustomizedPanGesturePopRecognizer];
}

-(void)createBackBarButtonWithIconName:(NSString*)iconName
{
    UIButton *menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [menuButton addTarget:self action:@selector(touchBack) forControlEvents:UIControlEventTouchUpInside];
    [menuButton setBackgroundImage:[UIImage imageNamed:iconName] forState:UIControlStateNormal];
//    [menuButton setBackgroundImage:[UIImage imageNamed:iconName] forState:UIControlStateHighlighted];
    UIBarButtonItem *menuButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    self.navigationItem.leftBarButtonItem = menuButtonItem;
    
    //  Set up Pop pan gesture
//    [self setUpCustomizedPanGesturePopRecognizer];
}

- (void) touchBack{
    [self.navigationController popViewControllerAnimated:YES];
}

//-(void)revealToggle:(id)sender
//{
//    SWRevealViewController *revealController = [self revealViewController];
//    [revealController setFrontViewPosition:FrontViewPositionRightMostRemoved animated:YES];
//}

-(void)decorateForButton:(UIButton *)button
{
    [button.layer setBorderWidth:1.0f];
    [UIColor colorWithRed:1 green:1 blue:1 alpha:0];
}

- (void)rotateLayerInfinite:(CALayer *)layer
{
    CABasicAnimation *rotation;
    rotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    rotation.fromValue = [NSNumber numberWithFloat:0];
    rotation.toValue = [NSNumber numberWithFloat:(2 * M_PI)];
    rotation.duration = 0.7f; // Speed
    rotation.repeatCount = HUGE_VALF; // Repeat forever. Can be a finite number.
    [layer removeAllAnimations];
    [layer addAnimation:rotation forKey:@"Spin"];
}

#pragma mark LOADING
- (void) checkCreateMaskIndicator {
    
    BOOL isExistMask = [self.view.subviews containsObject:maskViewForSpinner];
    
    if(!maskViewForSpinner) {
        UIView *maskView = [[UIView alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT)];
        maskView.backgroundColor = [UIColor clearColor];
        maskView.alpha = 1;
        maskViewForSpinner = maskView;
    }
    
    if(!isExistMask)
        [self.view addSubview:maskViewForSpinner];
    
    BOOL isExist = [self.view.subviews containsObject:indicatorImageView];
    
    if(!isExist) {
        [self.view addSubview:[self createIndicator]];
        indicatorImageView.translatesAutoresizingMaskIntoConstraints = NO;
        [indicatorImageView.superview addConstraint:[NSLayoutConstraint constraintWithItem:indicatorImageView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:indicatorImageView.superview attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
        [indicatorImageView.superview addConstraint:[NSLayoutConstraint constraintWithItem:indicatorImageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:indicatorImageView.superview attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        [indicatorImageView addConstraint:[NSLayoutConstraint constraintWithItem:indicatorImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:SPINNER_BACKGROUND_WIDTH*SCREEN_SCALE]];
        [indicatorImageView addConstraint:[NSLayoutConstraint constraintWithItem:indicatorImageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:SPINNER_BACKGROUND_HEIGHT*SCREEN_SCALE]];
    }
    
    [self.view bringSubviewToFront:maskViewForSpinner];
    [self.view bringSubviewToFront:indicatorImageView];
}

- (void)startActivityIndicator
{
    printf("%s",[[NSString stringWithFormat:@"%s\n",__PRETTY_FUNCTION__] UTF8String]);
    dispatch_async(dispatch_get_main_queue(), ^{
        if (showingSpinner) {
            return;
        }
        
        [self checkCreateMaskIndicator];
        
        [maskViewForSpinner setHidden:NO];
        [indicatorImageView setHidden:NO];
        
        //Start the animation
        [indicatorImageView startAnimating];

        showingSpinner = YES;
    });
}

- (void)startActivityIndicatorWithoutMask
{
    printf("%s",[[NSString stringWithFormat:@"%s\n",__PRETTY_FUNCTION__] UTF8String]);
    dispatch_async(dispatch_get_main_queue(), ^{
        if (showingSpinner) {
            return;
        }
        
        [self checkCreateMaskIndicator];
        
        [maskViewForSpinner setHidden:YES];
        [indicatorImageView setHidden:NO];
        
        [indicatorImageView startAnimating];
        showingSpinner = YES;
    });
}

- (void)stopActivityIndicatorWithoutMask
{
    [self stopActivityIndicator];
}

- (void)stopActivityIndicator
{
    printf("%s",[[NSString stringWithFormat:@"%s\n",__PRETTY_FUNCTION__] UTF8String]);
    dispatch_async(dispatch_get_main_queue(), ^{
//        [indicatorImageView.layer removeAllAnimations];
        [maskViewForSpinner setHidden:YES];
        [indicatorImageView setHidden:YES];
        showingSpinner = NO;
    });
    
}

#pragma mark NAVIGATION
-(void) setNavigationBarWithDefaultColorAndTitle:(NSString*)title{
    if (!title || [title isEqualToString:@""]) {
        title = @"Luxury Card";
    }
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBarTintColor:colorFromHexString(GRAY_BACKGROUND_COLOR)];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.navigationItem.titleView.frame.size.width, 50)];
    
    NSDictionary *attributes = @{NSForegroundColorAttributeName:colorFromHexString(NAVIGATION_BAR_TITLE_TEXT_COLOR),
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_BLACK size:(FONT_SIZE_18 * SCREEN_SCALE_BASE_WIDTH_375)]};
    titleLabel.attributedText = [[NSAttributedString alloc] initWithString:title attributes:attributes];
    titleLabel.numberOfLines = 2;
//    titleLabel.adjustsFontSizeToFitWidth = YES;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [titleLabel adjustsFontSizeToFitWidth];
    self.navigationItem.titleView = titleLabel;
}

- (void)isChangeNavigationBackgroundColor:(BOOL)isChange{
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBarTintColor:colorFromHexString(isChange == YES ? DEFAULT_BACKGROUND_COLOR : GRAY_BACKGROUND_COLOR)];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];

    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        
        statusBar.backgroundColor = colorFromHexString(isChange == YES ? DEFAULT_BACKGROUND_COLOR : GRAY_BACKGROUND_COLOR);//set whatever color you like
    }
}

-(void)setNavigationBarWithColor:(UIColor *)color
{
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBarTintColor:color];
    [self.navigationController.navigationBar setTranslucent:NO];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : colorFromHexString(DEFAULT_HIGHLIGHT_COLOR), NSFontAttributeName:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:(18.0f * SCREEN_SCALE_BASE_WIDTH_375)]};
}

-(void)backNavigationAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)showAlertWithTitle:(NSString*)title
               andMessage:(NSString*)message
              andBtnTitle:(NSString*)btnTitle{
    [self isChangeNavigationBackgroundColor:YES];
    UIAlertController *alert = [self baseAlertViewWithTitle:title withMessage:message withFistBtnTitle:btnTitle forFirstAction:nil withSecondBtnTitle:@"" forSeconAction:nil];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)showActiveAlertWithTitle:(NSString*)title
                    withMessage:(NSString*)message
               withFistBtnTitle:(NSString*)fstTit
                 forFirstAction:(void (^)())fstAction
             withSecondBtnTitle:(NSString*)sndTit
                 forSeconAction:(void (^)())sndAction{
    [self isChangeNavigationBackgroundColor:YES];
    UIAlertController *alert = [self baseAlertViewWithTitle:title withMessage:message withFistBtnTitle:fstTit forFirstAction:fstAction withSecondBtnTitle:sndTit forSeconAction:sndAction];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)showActiveAndDismissableAlertWithTitle:(NSString*)title
                    withMessage:(NSString*)message
               withFistBtnTitle:(NSString*)fstTit
                 forFirstAction:(void (^)())fstAction
             withSecondBtnTitle:(NSString*)sndTit
                 forSeconAction:(void (^)())sndAction{
    [self isChangeNavigationBackgroundColor:YES];
    UIAlertController *alert = [self baseAlertViewWithTitle:title withMessage:message withFistBtnTitle:fstTit forFirstAction:fstAction withSecondBtnTitle:sndTit forSeconAction:sndAction];

    [self presentViewController:alert animated:YES completion:^{
        alert.view.superview.userInteractionEnabled = YES;
        [alert.view.superview addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(DismissAlertByTab)]];
        
    }];
}

- (void)DismissAlertByTab
{
    [self dismissViewControllerAnimated: YES completion: nil];
}

- (UIAlertController*)baseAlertViewWithTitle:(NSString*)title
                                 withMessage:(NSString*)message
                            withFistBtnTitle:(NSString*)fstTit
                              forFirstAction:(void (^)())fstAction
                          withSecondBtnTitle:(NSString*)sndTit
                              forSeconAction:(void (^)())sndAction{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *fistBtn = [UIAlertAction actionWithTitle:fstTit style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (fstAction) {
            fstAction();
            [self isChangeNavigationBackgroundColor:NO];
            
        }
    }];
    UIAlertAction *sndBtn = [UIAlertAction actionWithTitle:sndTit style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (sndAction) {
            sndAction();
            [self isChangeNavigationBackgroundColor:NO];
            
        }
    }];
    
    [alert addAction:fistBtn];
    if (![sndTit isEqualToString:@""]) {
        [alert addAction:sndBtn];
    }
    
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.alignment = NSTextAlignmentLeft;
    
    NSMutableAttributedString *atrStr = [[NSMutableAttributedString alloc] initWithString:message attributes:@{NSParagraphStyleAttributeName:paraStyle,NSForegroundColorAttributeName : colorFromHexString(DEFAULT_ALERT_MESSAGE_COLOR),NSFontAttributeName:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:FONT_SIZE_14*SCREEN_SCALE_BASE_WIDTH_375]}];
    
    if (!IPAD) {
        [alert setValue:atrStr forKey:@"attributedMessage"];
    }
    
    return alert;
}




#pragma mark CHANGE VIEW WHEN SCROLLVIEW
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if( [scrollView.panGestureRecognizer translationInView:self.view].y  < translationPointY) {
        [self changePositionForView:YES];
        translationPointY = [scrollView.panGestureRecognizer translationInView:self.view].y;
    } else if ([scrollView.panGestureRecognizer translationInView:self.view].y  > translationPointY) {
        [self changePositionForView:NO];
        translationPointY = [scrollView.panGestureRecognizer translationInView:self.view].y;
    }
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    translationPointY = [scrollView.panGestureRecognizer translationInView:self.view].y;
}

-(void)changePositionForView:(BOOL)isHide
{
}


#pragma mark CUSTOMIZED POP ACTION

-(void)setUpCustomizedPanGesturePopRecognizer
{
    self.navigationController.delegate = self;
    
//    if(isShouldHideKeyboardWhenSwipe) {
//        touchdown = [[TouchDownGestureRecognizer alloc] initWithTarget:self action:@selector(touchDown:)];
//        touchdown.delegate = self;
//        [self.view addGestureRecognizer:touchdown];
//    }
//
    if(!popRecognizer) {
        popRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePopRecognizer:)];
        //popRecognizer.delegate = self;
        [self.view addGestureRecognizer:popRecognizer];
    }
    
    if(!popNavigationBarRecognizer) {
        popNavigationBarRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePopRecognizer:)];
        //popNavigationBarRecognizer.delegate = self;
        [self.navigationController.navigationBar addGestureRecognizer:popNavigationBarRecognizer];
    }
}

-(void)removeSetupForCustomizedPanGesturePopRecognizer
{
    if (self.navigationController.delegate == self) {
        self.navigationController.delegate = nil;
    }
}


- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC {
    
    // Check if we're transitioning from this view controller to a DSLSecondViewController
    if (fromVC == self) //&& [toVC isKindOfClass:[ExploreViewController class]])
    {
        return [[CustomPopTransition alloc] init];
    }
    else {
        return nil;
    }
}

- (id<UIViewControllerInteractiveTransitioning>)navigationController:(UINavigationController *)navigationController interactionControllerForAnimationController:(id<UIViewControllerAnimatedTransitioning>)animationController {
    
    // Check if this is for our custom transition
    if ([animationController isKindOfClass:[CustomPopTransition class]]) {
        return self.interactivePopTransition;
    }
    else {
        return nil;
    }
}

- (void) returnNormalState{
}

- (void)handlePopRecognizer:(UIPanGestureRecognizer*)recognizer {
    
//    if(isAnimatingHideKeyboard) {
//        [self hidenKeyboardWhenSwipe];
//    }
    
    [self hidenKeyboardWhenSwipe];
    
    CGPoint velocity = [recognizer translationInView:self.view];
    if(velocity.x < 0)
    {
        [self returnNormalState];
        if(self.interactivePopTransition)
        {
            [self.interactivePopTransition cancelInteractiveTransition];
            self.interactivePopTransition = nil;
        }
        
        return;
    }
    
#ifdef DEBUG
    NSLog(@"Location x: %f",velocity.x);
#endif
    // Calculate how far the user has dragged across the view
    CGFloat progress = [recognizer translationInView:self.view].x / (self.view.bounds.size.width * 1.0);
    progress = MIN(1.0, MAX(0.0, progress));
    
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        // Create a interactive transition and pop the view controller
//        [self hidenKeyboardWhenSwipe];
        self.interactivePopTransition = [[UIPercentDrivenInteractiveTransition alloc] init];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (recognizer.state == UIGestureRecognizerStateChanged) {
        // Update the interactive transition's progress
        [self returnNormalState];
#ifdef DEBUG
        NSLog(@"UIGestureRecognizerStateChanged :%f",progress);
#endif
        [self.interactivePopTransition updateInteractiveTransition:progress];
    }
    else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
        // Finish or cancel the interactive transition
        if (progress > 0.5) {
            [self returnNormalState];
#ifdef DEBUG
            NSLog(@"finishInteractiveTransition");
#endif
            
            [self.interactivePopTransition finishInteractiveTransition];
            
            
        }
        else {
            [self returnNormalState];
#ifdef DEBUG
            NSLog(@"cancelInteractiveTransition");
#endif
            [self.interactivePopTransition cancelInteractiveTransition];
            
        }
        self.interactivePopTransition = nil;
    }
}
 

#pragma mark - Show error network
- (void)showErrorNetwork{
    [self showActiveAlertWithTitle:NSLocalizedString(@"Unable to get data", nil) withMessage:NSLocalizedString(@"Turn on cellular data or use Wi-Fi to access Luxury Card.", nil) withFistBtnTitle:NSLocalizedString(@"Settings", nil) forFirstAction:^{
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"prefs:root"]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root"]];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root"]];
        }
    } withSecondBtnTitle:NSLocalizedString(@"OK", nil) forSeconAction:^{
        [self stopActivityIndicator];
    }];
}

- (void) forceRelease {
    
}

-(void) createTabBarController
{
    AppDelegate *appdelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    appdelegate.tabBarController  = [[UITabBarController alloc] init];
    appdelegate.tabBarController.tabBar.barTintColor = [UIColor blackColor];
    appdelegate.tabBarController.tabBar.translucent = NO;
    appdelegate.tabBarController.delegate = appdelegate;
    appdelegate.tabBarController.tabBar.tintColor = [UIColor blackColor];
    appdelegate.tabBarController.tabBar.layer.borderWidth = 1.0f;
    appdelegate.tabBarController.tabBar.layer.borderColor = [UIColor blackColor].CGColor;
    appdelegate.tabBarController.tabBar.clipsToBounds = YES;
    
    HomeLXViewController *homeLXViewController = [[HomeLXViewController alloc] init];
    UINavigationController *homeLXViewControllerNVC = [[UINavigationController alloc] initWithRootViewController:homeLXViewController];
    homeLXViewController.tabBarItem.image = [UIImage imageNamed:@"home_ic"];
    homeLXViewController.tabBarItem.selectedImage = [UIImage imageNamed:@"home_ic_inter"];
    homeLXViewController.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    
    MyRequestPagerViewController *askConciergeViewController = [[MyRequestPagerViewController alloc] init];
    UINavigationController *askConciergeNVC = [[UINavigationController alloc] initWithRootViewController:askConciergeViewController];
    askConciergeNVC.tabBarItem.image = [UIImage imageNamed:@"ask_concierge_bell_ic"];
    askConciergeNVC.tabBarItem.selectedImage = [UIImage imageNamed:@"ask_concierge_bell_ic_inter"];
    askConciergeNVC.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    
    WebformViewController *webformViewController = [[WebformViewController alloc] init];
    webformViewController.navigationTitle = NSLocalizedString(@"Account Management", nil);
    webformViewController.urlString = ACCOUNT_MANAGEMENT_LINK;
    UINavigationController *accountNVC = [[UINavigationController alloc] initWithRootViewController:webformViewController];
    webformViewController.tabBarItem.image = [UIImage imageNamed:@"account_ic"];
    webformViewController.tabBarItem.selectedImage = [UIImage imageNamed:@"account_ic_inter"];
    webformViewController.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MenuViewController *menuViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
    UINavigationController *menuNavigationViewController = [[UINavigationController alloc] initWithRootViewController:menuViewController];
    menuViewController.tabBarItem.image = [UIImage imageNamed:@"menu_ic"];
    menuViewController.tabBarItem.selectedImage = [UIImage imageNamed:@"menu_ic_inter"];
    menuViewController.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    
    
    [UIView transitionWithView:appdelegate.window
                      duration:0.5
                       options:UIViewAnimationOptionPreferredFramesPerSecond60
                    animations:^{
                        
                        appdelegate.tabBarController.viewControllers = @[homeLXViewControllerNVC, askConciergeNVC,accountNVC,menuNavigationViewController];
                        
                        appdelegate.window.rootViewController = appdelegate.tabBarController;
                        [appdelegate.window makeKeyAndVisible];
                        
                    }
                    completion:
     ^(BOOL finshed){
         
         for(UIViewController *vc in self.navigationController.viewControllers)
         {
             [vc dismissViewControllerAnimated:NO completion:nil];
         }
         [self.navigationController dismissViewControllerAnimated:NO completion:nil];
     }];
    
//    appdelegate.tabBarController.viewControllers = @[homeLXViewControllerNVC, askConciergeNVC,accountNVC,menuNavigationViewController];
    
//    appdelegate.window.rootViewController = appdelegate.tabBarController;
//    [appdelegate.window makeKeyWindow];
    
//    [self presentViewController:appdelegate.tabBarController animated:NO completion:nil];
}

-(void) setBackgroundColorForRequestTabBarItem
{
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    UIColor* bgColor = [UIColor colorWithRed:(255/255.f) green:(255/255.f) blue:(255/255.f) alpha:0.5f];
    appDelegate.tabBarController.tabBar.tintColor = bgColor;
    UIImage *blackBackground = [UIImage imageNamed:@"black_bg"];
    appDelegate.tabBarController.tabBar.selectionIndicatorImage = blackBackground;
}

- (void) setTabBarNormalStyle{
    AppDelegate *appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    UIColor* bgColor = [UIColor blackColor];
    appDelegate.tabBarController.tabBar.tintColor = bgColor;
    [self changeBackgroundForSelectedTabbarItem];
}

-(void) changeBackgroundForSelectedTabbarItem
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.tabbarItemBGView = [appDelegate.tabBarController.tabBar viewWithTag:10000];
    
    if(!appDelegate.tabbarItemBGView){
        appDelegate.tabbarItemBGView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/4.0, 50)];
        appDelegate.tabbarItemBGView.tag = 10000;
        [appDelegate.tabbarItemBGView setBackgroundColor:[UIColor whiteColor]];
        [appDelegate.tabBarController.tabBar insertSubview:appDelegate.tabbarItemBGView belowSubview:(UIView *)appDelegate.tabBarController.tabBarItem];
    }
}

-(void) showToolTipAtView:(id)view inView:(UIView *)inView withKey:(NSString*)keyMessage
{
    NSString *messageShown = [AppData showTooltip:keyMessage];
    if(messageShown)
    {
        popTipView  = [[CMPopTipView alloc] initWithMessage:messageShown];
        popTipView.dismissTapAnywhere = YES;
        popTipView.has3DStyle = NO;
        popTipView.hasGradientBackground = NO;
        popTipView.hasShadow = NO;
        popTipView.cornerRadius = 2.0;
        popTipView.bubblePaddingX = 10.0f;
        popTipView.bubblePaddingY = 10.0f;
        popTipView.borderColor = colorFromHexString(@"#007AFF");
        popTipView.backgroundColor = colorFromHexString(@"#007AFF");
        popTipView.textColor = [UIColor whiteColor];
        popTipView.dismissAlongWithUserInteraction = YES;
        if([keyMessage isEqualToString:@"BellButton"]){
           popTipView.preferredPointDirection = PointDirectionDown;
        }
        else if([keyMessage isEqualToString:@"CategoryButton"]){
            popTipView.offsetY = 25.0 * SCREEN_SCALE;
        }
    
        if([view isKindOfClass:[UIBarButtonItem class]] || [view isKindOfClass:[UITabBarItem class]])
        {
            UIBarButtonItem *barButtonItem = (UIBarButtonItem *)view;
            [popTipView presentPointingAtBarButtonItem:barButtonItem animated:YES];
        }
        else if([view isKindOfClass:[UIButton class]])
        {
            UIButton *button = (UIButton *)view;
            [popTipView presentPointingAtView:button inView:inView animated:YES];
        }
    }
    else{
        if(popTipView){
            [popTipView dismissAnimated:NO];
            
            if([view isKindOfClass:[UIBarButtonItem class]] || [view isKindOfClass:[UITabBarItem class]])
            {
                UIBarButtonItem *barButtonItem = (UIBarButtonItem *)view;
                [popTipView presentPointingAtBarButtonItem:barButtonItem animated:YES];
            }
            else if([view isKindOfClass:[UIButton class]])
            {
                UIButton *button = (UIButton *)view;
                [popTipView presentPointingAtView:button inView:inView animated:YES];
            }
        }
    }
}

-(void) dismistTooltip{
    if(popTipView){
        [popTipView dismissAnimated:NO];
    }
}

#pragma mark Data Loading Delegate
-(void)loadDataFailFrom:(id<WSBaseProtocol>)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message{
    [self stopActivityIndicator];
    NSString *title;
    if (!message || [message isEqualToString:@""]) {
        message = NSLocalizedString(@"An error has occurred. Check your Internet settings or try again.", nil);
        title = NSLocalizedString(@"Unable to get data", nil);
    }else{
        title = NSLocalizedString(@"ERROR!", nil);
    }
    
    [self showActiveAlertWithTitle:title withMessage:message withFistBtnTitle:NSLocalizedString(@"Settings", nil) forFirstAction:^{
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"prefs:root"]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root"]];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root"]];
        }
    } withSecondBtnTitle:NSLocalizedString(@"OK", nil) forSeconAction:^{
        
    }];
}

- (void)loadDataFailFrom:(id<BaseResponseObjectProtocol>)result withErrorCode:(NSInteger)errorCode{
    [self stopActivityIndicator];
    [self showCommonAlertForFaildLoodingData];
}

-(void) showCommonAlertForFaildLoodingData
{
    [self showActiveAlertWithTitle:NSLocalizedString(@"Unable to get data", nil) withMessage:NSLocalizedString(@"An error has occurred. Check your Internet settings or try again.", nil) withFistBtnTitle:NSLocalizedString(@"Settings", nil) forFirstAction:^{
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"prefs:root"]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root"]];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root"]];
        }
    } withSecondBtnTitle:NSLocalizedString(@"OK", nil) forSeconAction:^{
    }];
}


- (void)WSBaseNetworkUnavailable{
    [self stopActivityIndicator];
}

- (void) pushVCFromBottom:(UIViewController *)viewControllerName {
//    CATransition *transition = [CATransition animation];
//    transition.duration = 0.5;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionMoveIn;
//    transition.subtype = kCATransitionFromTop;
//    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
//    [self.navigationController pushViewController:viewControllerName animated:NO];
    [self.navigationController pushViewController:viewControllerName animated:YES];
}

- (void) popToRootVCFromBottom:(BOOL)popToRoot {
//    CATransition *transition = [CATransition animation];
//    transition.duration = 0.5;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionReveal;
//    transition.subtype = kCATransitionFromBottom;
//    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
//    if (popToRoot == YES) {
//        [self.navigationController popToRootViewControllerAnimated:NO];
//    }else{
//        [self.navigationController popViewControllerAnimated:NO];
//    }
        if (popToRoot == YES) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }else{
            [self.navigationController popViewControllerAnimated:YES];
        }
}

#pragma mark - TRACKING
- (void)trackingEventByName:(NSString*)eventName withAction:(ActionType)action withCategory:(CategoryType)category{
    NSString *actionName = @"";
    switch (action) {
        case ClickActionType:
            actionName = @"Click";
            break;
        case OpenActionType:
            actionName = @"Open";
            break;
        case LeaveActionType:
            actionName = @"Leave";
            break;
        case SubmitActionType:
            actionName = @"Submit";
            break;
        case SelectActionType:
            actionName = @"Select";
            break;
        default:
            break;
    }
    NSString *categoryName = @"";
    switch (category) {
        case AuthenticationCategoryType:
            categoryName = @"Authentication";
            break;
        case UserInteractivityCategoryType:
            categoryName = @"User interactivity";
            break;
        case RequestCategoryType:
            categoryName = @"Request";
            break;
        case CitySelectionCategoryType:
            categoryName = @"City selection";
            break;
        case CategorySelectionCategoryType:
            categoryName = @"Category selection";
            break;
        case SignOutCategoryType:
            categoryName = @"Sign out";
            break;
            
        default:
            break;
    }
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:categoryName
                                                          action:actionName
                                                           label:eventName
                                                           value:nil] build]];
    
}

- (void)trackingScreenByName:(NSString*)screenName {
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:screenName];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker set:kGAIScreenName
           value:nil];
}
@end
