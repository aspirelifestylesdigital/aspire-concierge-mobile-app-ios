//
//  SearchViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "SearchViewController.h"
#import "UIView+Extension.h"
#import "UIButton+Extension.h"
#import "Constant.h"
#import "Common.h"

@interface SearchViewController ()<UITextFieldDelegate>
{
    CGFloat backupBottomConstraint;
    
    BOOL isCheckOffer;
}
@end

@implementation SearchViewController

- (void)viewDidLoad {
    isShowAskConciergeBarButton = YES;
    [super viewDidLoad];
   
    // Do any additional setup after loading the view from its nib.
    UITapGestureRecognizer *tappedOutsideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tappedOutsideKeyboard.cancelsTouchesInView = YES;
    [self.view addGestureRecognizer:tappedOutsideKeyboard];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWill:) name:UIKeyboardWillHideNotification object:nil];
    backupBottomConstraint = self.searchBtnBottomConstraint.constant;
    [self createBackBarButton];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     [self trackingScreenByName:@"Search"];
}
-(void)initView
{
    [self.offerCheckBox setImage:[UIImage imageNamed:@"radio_unchecked"] forState:UIControlStateNormal];
    [self.offerCheckBox setImage:[UIImage imageNamed:@"radio_checked"] forState:UIControlStateSelected];
    [self.offerCheckBox addTarget:self action:@selector(enableSubmitButton:) forControlEvents:UIControlEventTouchUpInside];
    self.offerCheckBox.selected = isCheckOffer;
    [self.offerCheckBox setBackgroundColor:[UIColor clearColor]];
    
    if(self.currentKeyWord.length == 0)
    {
        self.offerCheckBox.enabled = NO;
    }
    
    [self.bookCheckBox setImage:[UIImage imageNamed:@"radio_unchecked"] forState:UIControlStateNormal];
    [self.bookCheckBox setImage:[UIImage imageNamed:@"radio_checked"] forState:UIControlStateSelected];
    [self.bookCheckBox addTarget:self action:@selector(enableSubmitButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.bookCheckBox setBackgroundColor:[UIColor clearColor]];
    self.bookCheckBox.enabled = NO;
    
    [self.bookText setText:NSLocalizedString(@"Can Book Online", nil)];
    [self.offerText setText:NSLocalizedString(@"Only show results with Luxury Card Offers", nil)];
    self.offerText.font = [UIFont fontWithName:FONT_MarkForMC_MED size:FONT_SIZE_14 * SCREEN_SCALE_BASE_WIDTH_375];
    self.offerText.textColor = [UIColor whiteColor];
    
    UITapGestureRecognizer *offerTextGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enableSubmitButton:)];
    [self.offerText setUserInteractionEnabled:YES];
    [self.offerText addGestureRecognizer:offerTextGesture];
    
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"Search", nil)];
    
    [self.searchButton setDefaultStyle];
    
    if(self.currentKeyWord.length == 0)
    {
        [self setEnableSearchButton:NO];
    }
    
//    self.searchButton.titleLabel.font = [UIFont fontWithName:FONT_MarkForMC_MED size:18.0f * SCREEN_SCALE];
//    self.searchButton.titleLabel.textColor = [UIColor whiteColor];
    [self.searchButton setTitle:NSLocalizedString(@"Search", nil) forState:UIControlStateNormal];
    
    UIImageView *searchIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"search_icon"]];
    searchIcon.frame = CGRectMake(0.0f, 0.0f, 30.0f*SCREEN_SCALE, 20.0f*SCREEN_SCALE);
    searchIcon.contentMode = UIViewContentModeScaleAspectFit;
    self.searchText.leftView = searchIcon;
    self.searchText.leftViewMode = UITextFieldViewModeAlways;
    self.searchText.delegate = self;
    self.searchText.textColor = colorFromHexString(@"#011627");
    self.searchText.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search" attributes:@{NSForegroundColorAttributeName:colorFromHexString(@"#99A1A8")}];
    
    UIFont *font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:18.0 * SCREEN_SCALE_BASE_WIDTH_375];
    self.searchText.font = font;
    
    if(self.currentKeyWord.length == 0)
    {
        NSString *placeHolderMsg = NSLocalizedString(@"Search", nil);
        NSMutableAttributedString *placeHolderAttribute = [[NSMutableAttributedString alloc] initWithString:placeHolderMsg];
        [placeHolderAttribute addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, placeHolderMsg.length)];
        [placeHolderAttribute addAttribute:NSForegroundColorAttributeName value: colorFromHexString(DEFAULT_PLACEHOLDER_COLOR) range:NSMakeRange(0, placeHolderMsg.length)];
        self.searchText.attributedPlaceholder = placeHolderAttribute;
    }
    else{
        self.searchText.text = self.currentKeyWord;
    }
    [self.searchButtonView setBackgroundColor:colorFromHexString(DEFAULT_BACKGROUND_COLOR)];
    //[self.searchTextView setbackgroundColorForLineMenu];
    [self.searchTextView setBackgroundColor:[UIColor blackColor]];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.stackViewTopConstraint.constant = (IPAD) ? 8.0f : 20.0f;
    });
}

- (void) checkOffer:(BOOL)ischeck {
    isCheckOffer = ischeck;
}

-(void)setEnableSearchButton:(BOOL)enable
{
    self.searchButton.enabled = enable;
    if(enable){
        [self.searchButton setDefaultStyle];
    }
    else{
        [self.searchButton setDisableStyle];
    }
}

-(void)handleKeyboardWillShow:(NSNotification *)paramNotification
{
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         self.searchBtnBottomConstraint.constant = keyboardRect.size.height + MARGIN_KEYBOARD;
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

-(void)handleKeyboardWill:(NSNotification *)paramNotification
{
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         self.searchBtnBottomConstraint.constant =  backupBottomConstraint;
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
    
}


-(void)dismissKeyboard
{
    if([self.searchText becomeFirstResponder])
    {
        [self.searchText resignFirstResponder];
    }
}

-(void) enableSubmitButton:(id)sender
{
    if([self.searchText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0)
    {
        [self.offerCheckBox setHighlighted:NO];
        self.offerCheckBox.selected = !(self.offerCheckBox.selected);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)searchTappedAction:(id)sender {
    [self searchAction];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return [self searchAction];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL isPressedBackspaceAfterSingleSpaceSymbol = range.length==1 && string.length==0;
    NSString* textNeedCheck = textField.text;
    if(isPressedBackspaceAfterSingleSpaceSymbol)
        textNeedCheck = [[textNeedCheck substringToIndex:range.location] stringByAppendingString:[textNeedCheck substringWithRange:NSMakeRange(range.location+range.length, textNeedCheck.length - range.location - range.length)]];
    else
        textNeedCheck = [textNeedCheck stringByAppendingString:string];
    if([textNeedCheck stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length && ![self checkIsAllIsWhiteSpaceForText:textNeedCheck])
    {
        self.bookCheckBox.enabled = YES;
        self.offerCheckBox.enabled = YES;
        [self setEnableSearchButton:YES];
    }
    else{
        if([textNeedCheck stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0){
            self.bookCheckBox.enabled = NO;
            self.bookCheckBox.selected = NO;
            self.offerCheckBox.enabled = NO;
            self.offerCheckBox.selected = NO;
            [self setEnableSearchButton:NO];
        }
        else{
            self.bookCheckBox.enabled = YES;
            self.offerCheckBox.enabled = YES;
            [self setEnableSearchButton:YES];
        }
    }
    return YES;
}

-(BOOL)searchAction
{
    if([self.searchText.text length] > 0 || self.offerCheckBox.selected)
    {
        if(self.delegate)
        {
            [self.searchText resignFirstResponder];
            [self.delegate getDataBaseOnSearchText:[self strimStringFrom:self.searchText.text withPatterns:@[@"^ *"]] withOffer:self.offerCheckBox.selected withBookOnline:self.bookCheckBox.selected];
        }
        return YES;
    }
    else{
        [self showAlertWithTitle:NSLocalizedString(@"alert_title", nil) andMessage:NSLocalizedString(@"Enter a search term or select a check box.", nil) andBtnTitle:NSLocalizedString(@"OK", nil)];
        return NO;
    }
}

- (BOOL) checkIsAllIsWhiteSpaceForText:(NSString*) text {
    BOOL isYES = YES;
    NSArray* arr = [text componentsSeparatedByString:@""];
    for (NSString* t in arr) {
        if([t stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0) {
            isYES = NO;
            break;
        }
    }
    return isYES;
}

- (NSString*) strimStringFrom:(NSString*) from withPatterns:(NSArray*) pattern {
    NSString* temp = from;
    for (NSString* p in pattern) {
        temp = [temp stringByReplacingOccurrencesOfString:p withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0,temp.length)];
    }
    return temp;
}

@end
