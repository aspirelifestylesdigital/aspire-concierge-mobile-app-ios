//
//  ChangePasswordViewController.m
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 8/23/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "UIView+Extension.h"
#import "UITextField+Extensions.h"
#import "UILabel+Extension.h"
#import "UIButton+Extension.h"
#import "WSChangePassword.h"
#import "NSString+Utis.h"

#import "AppDelegate.h"
#import "MenuViewController.h"
#import "HomeLXViewController.h"
#import "UDASignInViewController.h"
#import "NSString+AESCrypt.h"

#define BOTTOM_SPACE  30.0f

@interface ChangePasswordViewController ()<DataLoadDelegate, UITextFieldDelegate>{
    CGFloat keyboardHeight;
    AppDelegate* appdelegate;
    UIImageView *asteriskImage;
    IBOutlet UILabel *titleTemp;
    IBOutlet NSLayoutConstraint *topConstraintStackView;
}

@property (nonatomic, strong) WSChangePassword *wsChangePassword;

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createBackBarButton];
    [self setNavigationBarWithDefaultColorAndTitle:@"Change Password"];
    [self setUIStyle];
    UITapGestureRecognizer *tappedOutsideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tappedOutsideKeyboard];
    
    
    if(!self.navigationController) {
        NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                     NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:(14.0f * SCREEN_SCALE_BASE_WIDTH_375)]};
        titleTemp.attributedText = [[NSAttributedString alloc] initWithString:@"Change Password".uppercaseString attributes:attributes];
        titleTemp.numberOfLines = 0;
        //    titleLabel.adjustsFontSizeToFitWidth = YES;
        titleTemp.textAlignment = NSTextAlignmentCenter;
    } else {
        titleTemp.hidden = YES;
        topConstraintStackView.constant = 50;
        
        
        NSInteger  nViewCtr = self.navigationController.viewControllers.count;
        if (nViewCtr > 2) {
            NSString * fileToEqual = NSStringFromClass([UDASignInViewController class]);
            NSString * pushFrom =  NSStringFromClass([self.navigationController.viewControllers[nViewCtr -2] class]);
            if ([pushFrom isEqualToString:fileToEqual]) {
                self.navigationItem.leftBarButtonItem = nil;
                self.navigationItem.hidesBackButton = true;
            }
        }
    }
    isHiddenTabBar = YES;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self trackingScreenByName:@"Change password"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldChanged:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:self.oldPWTextField];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUIStyle{
    
    [self setStatusSubmitButton:NO];
    [self.scrollView setBackgroundColorForView];
    [self.contentView setBackgroundColorForView];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.oldPWTextField setDefaultStyleForTextField];
        [self.nPWTextField setDefaultStyleForTextField];
        //      [self.confirmNewPWTextField setDefaultStyleForTextField];
    });
    
    
    [self.currentPasswordLbl setDefaultStyleForTextFieldTitle];
    [self.nPasswordLbl setDefaultStyleForTextFieldTitle];
    [self.hintForPasswordLbl setDefaultStyleForTextFieldTitle];
    [self.hintForPasswordLbl setText:NSLocalizedString(@"Password must be between 10 and 25 characters, including at least 1 uppercase, 1 lowercase, 1 special character and 1 number.", nil)];
    
    self.oldPWTextField.delegate = self;
    self.nPWTextField.delegate = self;
    //    self.confirmNewPWTextField.delegate = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setTextFieldDefaultBottomBolder];
        self.topConstraintContentView.constant = CGRectGetHeight(self.contentView.frame) / 9;
    });
    
    /*
     [self createAsteriskForTextField:self.oldPWTextField];
     [self createAsteriskForTextField:self.nPWTextField];
     [self createAsteriskForTextField:self.confirmNewPWTextField];
     */
}

#pragma mark - Configure UI
-(void) createAsteriskForTextField:(UITextField *)textField {
    
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 8, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [textField setLeftView:leftView];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    asteriskImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"asterisk_icon"]];
    asteriskImage.frame = CGRectMake(0.0f, 0.0f, 20.0f, 20.0f);
    asteriskImage.contentMode = UIViewContentModeCenter;
    [textField setRightView:asteriskImage];
    [textField setRightViewMode:UITextFieldViewModeAlways];
}
-(void) makeBecomeFirstResponderForTextField {
    [self resignFirstResponderForAllTextField];
    
    if(![self.oldPWTextField.text isValidWeakPassword]){
        [self.oldPWTextField becomeFirstResponder];
    }
    else if(![self.nPWTextField.text isValidPassword]){
        [self.nPWTextField becomeFirstResponder];
    }
}
- (void)resignFirstResponderForAllTextField {
    
    if(self.oldPWTextField.isFirstResponder) {
        [self.oldPWTextField resignFirstResponder];
    }else if (self.nPWTextField.isFirstResponder) {
        [self.nPWTextField resignFirstResponder];
    }
    //    else if (self.confirmNewPWTextField.isFirstResponder) {
    //        [self.confirmNewPWTextField resignFirstResponder];
    //    }
}
- (void)setTextFieldDefaultBottomBolder {
    [self.oldPWTextField setBottomBolderDefaultColor];
    [self.nPWTextField setBottomBolderDefaultColor];
    //    [self.confirmNewPWTextField setBottomBolderDefaultColor];
}
- (void)setStatusSubmitButton:(BOOL)isEnable{
    //self.submitButton.enabled = (self.oldPWTextField.text.length > 0 && self.nPWTextField.text.length > 0 && self.confirmNewPWTextField.text.length > 0);
    self.submitButton.enabled = self.cancelButton.enabled = isEnable;
    
    self.submitButton.enabled ? [self.submitButton setDefaultStyle] : [self.submitButton setDisableStyle];
    self.cancelButton.enabled ? [self.cancelButton setDefaultStyle] : [self.cancelButton setDisableStyle];
}

#pragma mark - Selector
- (void)dismissVC {
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)textFieldChanged:(NSNotification*)notification {
    //    [self setStatusSubmitButton];
}
-(void)dismissKeyboard {
    [self resignFirstResponderForAllTextField];
}

#pragma mark TEXT FIELD DELEGATE
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == self.oldPWTextField || textField == self.nPWTextField /*|| textField == self.confirmNewPWTextField*/) {
        if ([string isEqualToString:@" "]) {
            return NO;
        }
        if (textField.text.length >= 25 && range.length == 0){
            return NO;
        }
    }
    if(textField == self.oldPWTextField && string == nil  && self.nPWTextField.text.length == 0)
    {
        [self setStatusSubmitButton:NO];
    }
    else if(textField == self.nPWTextField && string == nil  && self.oldPWTextField.text.length == 0)
    {
        [self setStatusSubmitButton:NO];
    }
    else{
        if(string.length > 0){
            [self setStatusSubmitButton:YES];
        }
        else{
            [self setStatusSubmitButton:(self.oldPWTextField.text.length > 0 && self.nPWTextField.text.length > 0)];
        }
    }
    
    
    return YES;
}

#pragma mark - Validation
-(void)verifyData{
    [self setTextFieldDefaultBottomBolder];
    NSString *errorAll = @"All fields are required.\n";
    NSMutableString *message = [[NSMutableString alloc] init];
    if (self.oldPWTextField.text.length == 0 || self.nPWTextField.text.length == 0 /*|| self.confirmNewPWTextField.text.length == 0*/) {
        [message appendString:@"* "];
        [message appendString:errorAll];
        if (self.oldPWTextField.text.length == 0) {
            [self.oldPWTextField setBottomBorderRedColor];
        }
        if (self.nPWTextField.text.length == 0) {
            [self.nPWTextField setBottomBorderRedColor];
        }
        /*
         if (self.confirmNewPWTextField.text.length == 0) {
         [self.confirmNewPWTextField setBottomBorderRedColor];
         }
         */
    }else {
        if([self verifyValueForTextField:self.oldPWTextField].length > 0 && self.oldPWTextField.text.length > 0) {
            [message appendString:@"* "];
            [message appendString:[self verifyValueForTextField:self.oldPWTextField]];
            [message appendString:@"\n"];
        }
        if([self verifyValueForTextField:self.nPWTextField].length > 0 && self.nPWTextField.text.length > 0) {
            [message appendString:@"* "];
            [message appendString:[self verifyValueForTextField:self.nPWTextField]];
            [message appendString:@"\n"];
        }
        /*
         else {
         if([self verifyValueForTextField:self.confirmNewPWTextField].length > 0 && self.confirmNewPWTextField.text.length > 0) {
         [message appendString:@"* "];
         [message appendString:[self verifyValueForTextField:self.confirmNewPWTextField]];
         [message appendString:@"\n"];
         }
         }
         */
    }
    
    [self showError:message];
}

-(void)showError:(NSString *)message
{
    if(message.length > 0){
        [self showActiveAlertWithTitle:NSLocalizedString(@"Please confirm that the value entered is correct:", nil) withMessage:message withFistBtnTitle:NSLocalizedString(@"OK", nil) forFirstAction:^{
            [self makeBecomeFirstResponderForTextField];
        } withSecondBtnTitle:@""forSeconAction:nil];
    }
    else{
        if(!isNetworkAvailable()) {
            [self showErrorNetwork];
            
        }else{
            [self resignFirstResponderForAllTextField];
            [self setTextFieldDefaultBottomBolder];
            [self changePasswordWithAPI];
        }
    }
}
-(NSString*)verifyValueForTextField:(UITextField *)textFied{
    
    NSString *errorMsg = @"";
    if(textFied == self.oldPWTextField && ![(textFied.isFirstResponder ? textFied.text :self.oldPWTextField.text) isValidWeakPassword]){
        errorMsg = NSLocalizedString(@"The password you entered is incorrect.", nil);
        [textFied setBottomBorderRedColor];
    }else if (textFied == self.nPWTextField && ![(textFied.isFirstResponder ? textFied.text :self.nPWTextField.text) isValidPassword]) {
        errorMsg = NSLocalizedString(@"Password must be between 10 and 25 characters, including at least 1 uppercase, 1 lowercase, 1 special character and 1 number.", nil);
        [textFied setBottomBorderRedColor];
    }
    /*
     else if (textFied == self.confirmNewPWTextField && ![(textFied.isFirstResponder ? textFied.text :self.confirmNewPWTextField.text) isEqualToString:self.nPWTextField.text]) {
     errorMsg = NSLocalizedString(@"input_invalid_confirm_password_msg", nil);
     [textFied setBottomBorderRedColor];
     }
     */
    else{
        [textFied setBottomBolderDefaultColor];
    }
    
    return errorMsg;
}

- (void)backToNavigationController{
    
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    
    if (numberOfViewControllers > 1)
    {
        UIViewController *previousViewcontroller = [self.navigationController.viewControllers objectAtIndex:numberOfViewControllers - 2];
        if (![previousViewcontroller isKindOfClass:[UDASignInViewController class]]) {
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [self createTabBarController];
        }
    }
}


#pragma mark - Delegate from API
-(void)loadDataDoneFrom:(WSBase*)ws
{
    [self stopActivityIndicator];
    if (((WSChangePassword*)ws).isSuccesful) {
        [self showActiveAlertWithTitle:@"" withMessage:NSLocalizedString(@"You have successfully updated your password.", nil) withFistBtnTitle:NSLocalizedString(@"OK", nil) forFirstAction:^{
            [[SessionData shareSessiondata] setHasForgotPassword:NO];
            [self backToNavigationController];
        } withSecondBtnTitle:@""forSeconAction:nil];
    }
    
}

-(void)loadDataFailFrom:(id<WSBaseProtocol>)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message{
    [self stopActivityIndicator];
    NSString *title;
    if (!message || [message isEqualToString:@""]) {
        message = NSLocalizedString(@"An error has occurred. Check your Internet settings or try again.", nil);
        title = NSLocalizedString(@"Unable to get data", nil);
        
        [self showActiveAlertWithTitle:title withMessage:message withFistBtnTitle:NSLocalizedString(@"Settings", nil) forFirstAction:^{
            if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"prefs:root"]]) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root"]];
            } else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root"]];
            }
        } withSecondBtnTitle:NSLocalizedString(@"OK", nil) forSeconAction:^{
            
        }];
    }else {
        if([message isEqualToString:@"The password you entered is incorrect."]){
            message = @"* The password you entered is incorrect.";
            title = NSLocalizedString(@"Please confirm that the value entered is correct:", nil);
            [self showActiveAlertWithTitle:title withMessage:message withFistBtnTitle:NSLocalizedString(@"OK", nil) forFirstAction:nil withSecondBtnTitle:@"" forSeconAction:nil];
        }else{
            title = NSLocalizedString(@"ERROR!", nil);
            
            [self showActiveAlertWithTitle:title withMessage:message withFistBtnTitle:NSLocalizedString(@"Settings", nil) forFirstAction:^{
                if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"prefs:root"]]) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root"]];
                } else {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root"]];
                }
            } withSecondBtnTitle:NSLocalizedString(@"OK", nil) forSeconAction:^{
                
            }];
        }
    }
    
    if (errorCode == 432) {
        [self.oldPWTextField setBottomBorderRedColor];
    }else{
        [self.oldPWTextField setBottomBolderDefaultColor];
    }
    
}

#pragma mark - Actions

- (void)changePasswordWithAPI{
    
    [self startActivityIndicator];
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    
    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
    if (profileDictionary) {
        [dataDict setValue:[profileDictionary objectForKey:keyEmail] forKey:@"Email"];
    }
    [dataDict setValue:self.oldPWTextField.text forKey:[OPSS_EncryptedKey AES256DecryptWithKey:EncryptKey]];
    [dataDict setValue:self.nPWTextField.text forKey:[NPSS_EncryptedKey AES256DecryptWithKey:EncryptKey]];
    
    self.wsChangePassword = [[WSChangePassword alloc] init];
    self.wsChangePassword.delegate = self;
    [self.wsChangePassword submitChangePassword:dataDict];
}

- (IBAction)SubmitChangePassword:(id)sender {
    [self verifyData];
}

- (IBAction)CancelAction:(id)sender {
    self.oldPWTextField.text = @"";
    self.nPWTextField.text = @"";
    //    self.confirmNewPWTextField.text = @"";
    [self setStatusSubmitButton:NO];
    [self setTextFieldDefaultBottomBolder];
}

#pragma mark Keyboard
-(void)handleKeyboardWillShow:(NSNotification *)paramNotification
{
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         keyboardHeight = keyboardRect.size.height;
                         [self updateEdgeInsetForShowKeyboard];
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

-(void)handleKeyboardWillHide:(NSNotification *)paramNotification
{
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         [self updateEdgeInsetForHideKeyboard];
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
    
}


-(void)updateEdgeInsetForShowKeyboard {
    
    self.bottomConstraintViewAction.constant = (keyboardHeight + MARGIN_KEYBOARD);
    
}


-(void)updateEdgeInsetForHideKeyboard
{
    [self resignFirstResponderForAllTextField];
    self.bottomConstraintViewAction.constant = BOTTOM_SPACE;
}

@end
