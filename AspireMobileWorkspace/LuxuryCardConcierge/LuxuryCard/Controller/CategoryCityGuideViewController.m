

//
//  CityGuideViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/26/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "CategoryCityGuideViewController.h"
#import "AppData.h"
#import "CategoryItem.h"
#import "CityItem.h"
#import "ExploreViewController.h"
#import "WSBase.h"
#import "WSB2CGetSubCategory.h"
#import "SubCategoryItem.h"
#import "WSB2CGetQuestions.h"
#import "CityTableViewCell.h"

@interface CategoryCityGuideViewController ()<UITableViewDelegate, UITableViewDataSource>

@end

@implementation CategoryCityGuideViewController

- (void)viewDidLoad {
    isShowAskConciergeBarButton = YES;
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [self setUpCustomizedPanGesturePopRecognizer];
    [self setNavigationBarWithDefaultColorAndTitle:@"City Guides"];
    [self createBackBarButton];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)initView
{
    [self.tableView setBackgroundColor:colorFromHexString(DEFAULT_BACKGROUND_COLOR)];
    [self.tableView registerNib:[UINib nibWithNibName:@"CityTableViewCell" bundle:nil] forCellReuseIdentifier:@"CityTableViewCell"];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100.f;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

-(void)initData
{
//    [self loadData];
    self.subCategories = [self.subCategories sortedArrayUsingComparator:^NSComparisonResult(CategoryItem *obj1, CategoryItem *obj2){ return [obj1.categoryName compare:obj2.categoryName]; }];
    
}

#pragma mark TABLEVIEW DELEGATE
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.subCategories.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CityTableViewCell"];
    [cell initCellWithCategoryData:[self.subCategories objectAtIndex:indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryItem *categoryItem = [self.subCategories objectAtIndex:indexPath.row];
    [AppData setCurrentCategoryWithCode:categoryItem];
    if(self.delegate)
        [self.delegate Explore:self onSelectCategory:categoryItem];
    
}

@end
