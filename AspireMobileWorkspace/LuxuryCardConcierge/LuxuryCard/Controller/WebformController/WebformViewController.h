//
//  WebformViewController.h
//  LuxuryCard
//
//  Created by user on 3/22/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "BaseViewController.h"

@interface WebformViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) NSString *navigationTitle;
@property (strong, nonatomic) NSString *urlString;
@end
