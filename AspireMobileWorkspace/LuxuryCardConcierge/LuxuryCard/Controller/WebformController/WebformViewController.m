//
//  WebformViewController.m
//  LuxuryCard
//
//  Created by user on 3/22/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "WebformViewController.h"
#import "AppDelegate.h"

@interface WebformViewController ()<UIWebViewDelegate>

@end

@implementation WebformViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
     [self createBackBarButton];
    
    [self setNavigationBarWithDefaultColorAndTitle:self.navigationTitle];
    if (self.urlString || ![self.urlString isEqualToString:@""]) {
        self.webView.delegate = self;
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.urlString]]];
//        [self startActivityIndicator];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    isHiddenTabBar = YES;
    if ([self.navigationTitle isEqualToString:NSLocalizedString(@"Account Management", nil)]) {
        self.navigationItem.leftBarButtonItem = nil;
        isHiddenTabBar = NO;
        [self trackingScreenByName:@"Account Management"];
    }
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
//    [self stopActivityIndicator];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (![self.navigationTitle isEqualToString:NSLocalizedString(@"Account Management", nil)]) {
        isHiddenTabBar = NO;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
