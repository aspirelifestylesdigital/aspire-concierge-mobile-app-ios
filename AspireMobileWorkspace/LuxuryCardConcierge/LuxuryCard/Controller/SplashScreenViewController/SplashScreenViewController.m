//
//  SplashScreenViewController.m
//  MobileConciergeUSDemo
//
//  Created by Nghia Dinh on 7/13/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "SplashScreenViewController.h"
//#import "UDASignInViewController.h"
#import "IntroductionViewController.h"
#import "Constant.h"
#import "EnumConstant.h"
#import "HomeLXViewController.h"
#import "UIView+Extension.h"
#import "AppData.h"
#import "PrivacyPolicyViewController.h"
#import "CreateProfileViewController.h"
#import "Common.h"
#import "WSB2CGetRequestToken.h"
#import "WSB2CGetAccessToken.h"
#import "WSB2CGetUserDetails.h"
#import "WSB2CGetPolicyInfo.h"
#import "WSB2CVerifyBIN.h"
#import "PolicyInfoItem.h"
#import "BINItem.h"
#import "WSSignIn.h"
#import "MenuViewController.h"
#import "ChangePasswordViewController.h"
#import "ExploreViewController.h"

#import "AppDelegate.h"

@interface SplashScreenViewController () <DataLoadDelegate>
{
    AppDelegate* appdelegate;
    NSInteger currentTask;
    BOOL isLoadPolicy;
    WSB2CGetRequestToken *wsRequestToken;
    WSB2CGetAccessToken *wsAccessToken;
    WSB2CGetUserDetails *wsGetUser;
    WSB2CGetPolicyInfo *wsPolicy;
    
    NSNumber *currentPolicy;
    dispatch_group_t group;
    dispatch_queue_t queue;
    float policyVersion;
}
@end

@implementation SplashScreenViewController

- (void)viewDidLoad {
    isIgnoreScaleView = YES;
    isNotChangeNavigationBarColor = YES;
    [super viewDidLoad];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self trackingScreenByName:@"Splash"];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.view layoutIfNeeded];
    
    [self getUUID];
    
    group = dispatch_group_create();
    queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    dispatch_group_notify(group, queue, ^{
        NSLog(@"All tasks done");
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self goToNextViewController];
    });
    [self isChangeNavigationBackgroundColor:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self isChangeNavigationBackgroundColor:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getUUID{
    if (![[SessionData shareSessiondata] UUID]) {
        NSString *uuidString = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [[SessionData shareSessiondata] setUUID:uuidString];
    }
}

- (void)appWillEnterForeground:(NSNotification*) noti {
    if(!isNetworkAvailable()) {
        [self showNetWorkingStatusArlet];
    }
}

- (void) CheckBINNumber{
    
    if(!isNetworkAvailable()) {
        [self showNetWorkingStatusArlet];
    }
}

-(void)goToNextViewController {
    
    if(isJailbroken())
    {
        [self showActiveAlertWithTitle:@"" withMessage:NSLocalizedString(@"jailbroken_message", nil) withFistBtnTitle:NSLocalizedString(@"OK", nil) forFirstAction:^{
            exit(0);
        } withSecondBtnTitle:@"" forSeconAction:nil];
        return;
    }
    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
    
    if (profileDictionary) {
        [self navigationToHomeLXViewController];
    }else{
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self navigationToIntroductionViewController];
    }

}

- (void) loadPolicy{
    // load policy privacy
    wsPolicy = [[WSB2CGetPolicyInfo alloc] init];
    wsPolicy.delegate = self;
    isLoadPolicy = YES;
    [wsPolicy retrieveDataFromServer];
}

- (void) getRequestToken{
    wsRequestToken = [[WSB2CGetRequestToken alloc] init];
    wsRequestToken.delegate = self;
    currentTask = WS_GET_REQUEST_TOKEN;
    [wsRequestToken getRequestToken];
}


- (void)loadDataDoneFrom:(id<WSBaseProtocol>)ws {
    if ([ws isKindOfClass:[WSSignIn class]]) {
        [self navigationToHomeLXViewController];
    }
}


- (void) showNetWorkingStatusArlet{
    [self showActiveAlertWithTitle:@"" withMessage:NSLocalizedString(@"Turn on cellular data or use Wi-Fi to access Luxury Card.", nil) withFistBtnTitle:NSLocalizedString(@"arlet_retry_button", nil) forFirstAction:^{
        [self CheckBINNumber];
    } withSecondBtnTitle:NSLocalizedString(@"home_button_title", nil) forSeconAction:nil];
}


-(void)navigationToHomeLXViewController
{
    if ([[SessionData shareSessiondata] hasForgotPassword]) {
//        Remove old data user.
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
        [[NSUserDefaults standardUserDefaults] synchronize];
        if ([SessionData shareSessiondata].arrayPreferences.count > 0) {
            [[SessionData shareSessiondata].arrayPreferences removeAllObjects];
        }

        [self navigationToIntroductionViewController];
    }
    else{
        [self createTabBarController];
    }
}


-(void)setHiddenTabBar:(BOOL)isHidden
{
    
}

-(void)navigationToIntroductionViewController
{
    [UIView transitionWithView:appdelegate.window
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{
                        /*
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        UDASignInViewController *createProfileViewController = [storyboard instantiateViewControllerWithIdentifier:@"UDASignInViewController"];
                        UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:createProfileViewController];
                        appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
                        appdelegate.window.rootViewController = navigationController;
                         */
                        IntroductionViewController *introductionVC = [[IntroductionViewController alloc] init];
                        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:introductionVC];
                        appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                        appdelegate.window.rootViewController = navigationController;
                    }
                    completion:^(BOOL a){
                    [appdelegate.window makeKeyAndVisible];
                    }];
    
}

- (void) crashApp{
    //[self performSelector:@selector(die_die)];
}

@end
