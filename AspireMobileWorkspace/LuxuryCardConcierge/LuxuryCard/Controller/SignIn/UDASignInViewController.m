//
//  UDASignInViewController.m
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 7/14/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "UDASignInViewController.h"
#import "CreateProfileViewController.h"
#import "UDAForgotPasswordViewController.h"
#import "HomeLXViewController.h"
#import "WSSignIn.h"
#import "WSB2CGetAccessToken.h"
#import "WSB2CGetUserDetails.h"
#import "WSB2CGetRequestToken.h"
#import "UserRegistrationItem.h"
#import "UtilStyle.h"
#import "UIButton+Extension.h"
#import "NSString+Utis.h"
#import "UITextField+Extensions.h"
#import "AppDelegate.h"
#import "MenuViewController.h"
#import "ChangePasswordViewController.h"
#import "AppData.h"
#import "UDAForgotPasswordViewController.h"
#import "UILabel+Extension.h"
#import "WSPreferences.h"
#import "NSString+AESCrypt.h"

@interface UDASignInViewController () <DataLoadDelegate, UITextFieldDelegate>{
    
    BOOL isInputEmail, isInputPassword;
    WSB2CGetAccessToken* wsB2CGetAccessToek;
    WSB2CGetRequestToken* wsB2CGetRequestToken;
    WSB2CGetUserDetails* wsGetUser;
    WSPreferences *wsPreferences;
    AppDelegate* appdelegate;
    UITapGestureRecognizer *tappedOutsideKeyboard;
}

@property (nonatomic, strong) WSSignIn *wsSignIn;

@end

@implementation UDASignInViewController

#pragma mark - LIFECYCLE
- (void)viewDidLoad {
    [super viewDidLoad];
   
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldChanged:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:self.passwordTextField];
    
    isInputEmail = isInputPassword = NO;
    [self createBackBarButton];
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"Sign In", nil)];
    [self setUIStyte];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self trackingScreenByName:@"Sign in"];
    self.emailTextField.text = @"";
    self.passwordTextField.text = @"";
    [self setTextViewsDefaultBottomBolder];
    
    if (!tappedOutsideKeyboard) {
        tappedOutsideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    }
    [self.view addGestureRecognizer:tappedOutsideKeyboard];
    [self.navigationController.view addGestureRecognizer:tappedOutsideKeyboard];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    if (tappedOutsideKeyboard) {
        [self.navigationController.view removeGestureRecognizer:tappedOutsideKeyboard];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SETUP UI
- (void) setUIStyte {
    isInputEmail = isInputPassword = NO;
    self.emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    self.passwordTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    self.passwordTextField.secureTextEntry = YES;
    self.emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    self.emailTextField.delegate = self;
    self.passwordTextField.delegate = self;
    
    [self.emailTextField setDefaultStyleForTextField];
    [self.passwordTextField setDefaultStyleForTextField];
    
    [self.emailTitle setDefaultStyleForTextFieldTitle];
    [self.emailTitle setFont:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:FONT_SIZE_14*SCREEN_SCALE_BASE_WIDTH_375]];
    self.emailTitle.text = NSLocalizedString(@"Email Address", nil);
    [self.passwordTitle setDefaultStyleForTextFieldTitle];
    [self.passwordTitle setFont:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:FONT_SIZE_14*SCREEN_SCALE_BASE_WIDTH_375]];
    self.passwordTitle.text = NSLocalizedString(@"Password", nil);
    
    
    self.emailTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:NSLocalizedString(@"Enter your email", nil)];
    self.passwordTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:NSLocalizedString(@"Enter your password", nil)];
    
    [self setStatusSignInButton];
    [self.signUpButton setTitle:NSLocalizedString(@"Don't have an account? Sign up.", nil) forState:UIControlStateNormal];
    self.signUpButton.titleLabel.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:12 * SCREEN_SCALE_BASE_WIDTH_375];
    
    [self.forgotButton setTitle:NSLocalizedString(@"Forgot Password?", nil) forState:UIControlStateNormal];
    self.forgotButton.titleLabel.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:12 * SCREEN_SCALE_BASE_WIDTH_375];
    
    [self.signInButton setTitle:NSLocalizedString(@"Sign In", nil) forState:UIControlStateNormal];
    self.signInButton.titleLabel.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:14 * SCREEN_SCALE_BASE_WIDTH_375];
}


- (void)setStatusSignInButton {
    if(isInputEmail && isInputPassword) {
        self.signInButton.enabled = YES;
        [self.signInButton setDefaultStyle];
    }
    else{
        self.signInButton.enabled = NO;
        [self.signInButton setDisableStyle];
    }
}


-(void)setTextViewsDefaultBottomBolder {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.emailTextField setBottomBolderDefaultColor];
        [self.passwordTextField setBottomBolderDefaultColor];
    });
}

-(void)dismissKeyboard
{
    [self resignFirstResponderForAllTextField];
}

#pragma mark - LOGICAL FUNCTION
- (void)OnBack {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)verifyData{
    NSString *errorAll = NSLocalizedString(@"All fields are required.", nil);
    NSMutableString *message = [[NSMutableString alloc] init];
    if (self.emailTextField.text.length == 0 && self.passwordTextField.text.length == 0) {
        [message appendString:@"* "];
        [message appendString:errorAll];
        [self.emailTextField setBottomBorderRedColor];
        [self.passwordTextField setBottomBorderRedColor];
        
    }else{
        if(self.emailTextField.text.length == 0){
            [message appendString:@"* "];
            [message appendString:[NSString stringWithFormat:@"%@ \n",errorAll]];
            [self.emailTextField setBottomBorderRedColor];
        }
        if (self.passwordTextField.text.length == 0){
            [message appendString:@"* "];
            [message appendString:[NSString stringWithFormat:@"%@ \n",errorAll]];
            [self.passwordTextField setBottomBorderRedColor];
        }
        if([self verifyValueForTextField:self.emailTextField].length > 0) {
            [message appendString:@"* "];
            [message appendString:[self verifyValueForTextField:self.emailTextField]];
            [message appendString:@"\n"];
        }
        if([self verifyValueForTextField:self.passwordTextField].length > 0) {
            [message appendString:@"* "];
            [message appendString:[self verifyValueForTextField:self.passwordTextField]];
            [message appendString:@"\n"];
        }
    }
    
    [self showError:message];
}

-(void)showError:(NSString *)message
{
    [self resignFirstResponderForAllTextField];
    
    if(message.length > 0){
        [self showActiveAlertWithTitle: NSLocalizedString(@"Please confirm that the value entered is correct:", nil) withMessage:message withFistBtnTitle: NSLocalizedString(@"OK", nil) forFirstAction:^{
            [self makeBecomeFirstResponderForTextField];
        } withSecondBtnTitle:@"" forSeconAction:nil];
    }
    else{
        [self setTextViewsDefaultBottomBolder];
        [self signInWithEmail:self.emailTextField.text withPassword:self.passwordTextField.text];
    }
}

-(NSString*)verifyValueForTextField:(UITextField *)textFied{
    
    NSString *errorMsg = @"";
    if(textFied == self.emailTextField && ![(textFied.isFirstResponder ? textFied.text :self.emailTextField.text) isValidEmail]){
        errorMsg = NSLocalizedString(@"input_invalid_email_msg", nil);
        [textFied setBottomBorderRedColor];
    }
    else if(textFied == self.passwordTextField && ![(textFied.isFirstResponder ? textFied.text :self.passwordTextField.text) isValidWeakPassword]){
        errorMsg = NSLocalizedString(@"The password you entered is incorrect.", nil);
        [textFied setBottomBorderRedColor];
    }
    else{
        [textFied setBottomBolderDefaultColor];
    }
    
    return errorMsg;
}

#pragma mark TEXT FIELD
-(void)resignFirstResponderForAllTextField{
    if(self.emailTextField.isFirstResponder){
        [self.emailTextField resignFirstResponder];
    }
    else if(self.passwordTextField.isFirstResponder){
        [self.passwordTextField resignFirstResponder];
    }
    
}

-(void) makeBecomeFirstResponderForTextField
{
    if(![self.emailTextField.text isValidEmail]){
        [self.emailTextField becomeFirstResponder];
    }
    else if(![self.passwordTextField.text isValidWeakPassword]){
        [self.passwordTextField becomeFirstResponder];
    }
}
-(void)textFieldChanged:(NSNotification*)notification {
    isInputPassword = self.passwordTextField.text.length > 0;
    [self setStatusSignInButton];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *inputString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (textField == self.passwordTextField) {
        if ([string isEqualToString:@" "]) {
            return NO;
        }
        isInputPassword = (inputString.length > 0);
        if (textField.text.length >= 25 && range.length == 0){
            return NO;
        }
    }else if (textField == self.emailTextField){
        isInputEmail = inputString.length > 0;
    }
    [self setStatusSignInButton];
    
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.text = [textField.text removeRedudantWhiteSpaceInText];
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == self.emailTextField)
    {
//        [self.emailTextField setBottomBolderDefaultColor];
        self.emailTextField.text = textField.text;
    }
    else if(textField == self.passwordTextField)
    {
//        [self.passwordTextField setBottomBolderDefaultColor];
        self.passwordTextField.text = textField.text;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if(textField == self.emailTextField){
//        [self.emailTextField setBottomBorderHightlightColor];
        textField.text = self.emailTextField.text;
    }
    else if(textField == self.passwordTextField){
//        [self.passwordTextField setBottomBorderHightlightColor];
        textField.text = self.passwordTextField.text;
    }
}

#pragma mark -  API

- (void)signInWithEmail:(NSString*)email withPassword:(NSString*)password {
    
    [self startActivityIndicator];
    NSMutableDictionary *userDict = [[NSMutableDictionary alloc] init];
    [userDict setValue:B2C_ConsumerKey forKey:@"ConsumerKey"];
    [userDict setValue:@"Login" forKey:@"Functionality"];
    [userDict setValue:email forKey:@"Email"];
    [userDict setValue:password forKey:@"Password"];
    [userDict setValue:B2C_DeviceId forKey:@"MemberDeviceID"];
    
    self.wsSignIn = [[WSSignIn alloc] init];
    self.wsSignIn.delegate = self;
    [self.wsSignIn signIn:userDict];
    
}
-(void)loadDataDoneFrom:(WSBase*)ws
{
    if (ws.task == WS_AUTHENTICATION_LOGIN) {
        if (ws.data) {
            [self setUserDefaultInfoWithDict:ws.data[0]];
        }
        [[SessionData shareSessiondata] setIsUseLocation: NO];
        wsPreferences = [[WSPreferences alloc] init];
        wsPreferences.delegate = self;
        [wsPreferences getPreference];
    }else if (ws.task == WS_GET_MY_PREFERENCE){
        [self updateSuccessRedirect];
        [self stopActivityIndicator];
    }
}

- (void) updateSuccessRedirect{
    
    
    if ([[SessionData shareSessiondata] hasForgotPassword]) {
        if([AppData isTempPasswordOver24hForEmail:self.emailTextField.text]){
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UDAForgotPasswordViewController *forgotPasswordViewController = [storyboard instantiateViewControllerWithIdentifier:@"UDAForgotPasswordViewController"];
            
            [self.navigationController pushViewController:forgotPasswordViewController animated:YES];
        }
        else{
            ChangePasswordViewController *changePasswordVC = [[ChangePasswordViewController alloc] init];
            [self.navigationController pushViewController:changePasswordVC animated:YES];
        }
    }else{
        [self createTabBarController];
    }
    
    [self trackingEventByName:@"Sign in" withAction:ClickActionType withCategory:AuthenticationCategoryType];
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:[pssWrd_EncryptedKey AES256DecryptWithKey:EncryptKey]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }
    else{
        [self showCommonAlertForFaildLoodingData];
    }
}

-(void)loadDataFailFrom:(WSBase *)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message {
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }else{
        if ([message isEqualToString:@"ENR70-1"]) {
            [self.emailTextField setBottomBorderRedColor];
            [self.passwordTextField setBottomBorderRedColor];
            message = NSLocalizedString(@"Please confirm that the value entered is correct.", nil);
            [self showActiveAlertWithTitle:NSLocalizedString(@"alert_error_title", nil) withMessage:message withFistBtnTitle:NSLocalizedString(@"OK", nil) forFirstAction:^{
                [self makeBecomeFirstResponderForTextField];
            } withSecondBtnTitle:@"" forSeconAction:nil];
        }
        else{
            [self showCommonAlertForFaildLoodingData];
        }
    }
}


- (void)setUserDefaultInfoWithDict:(UserRegistrationItem*)item{
    [[SessionData shareSessiondata] setOnlineMemberID:item.OnlineMemberID];
    [[SessionData shareSessiondata] setOnlineMemberDetailIDs:item.OnlineMemberDetailIDs];
}

#pragma mark - ACTIONS
- (IBAction)ForgetPasswordAction:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UDAForgotPasswordViewController *forgotPasswordViewController = [storyboard instantiateViewControllerWithIdentifier:@"UDAForgotPasswordViewController"];
    
    [self.navigationController pushViewController:forgotPasswordViewController animated:YES];
    
}

- (IBAction)SignInAction:(id)sender {
    [self verifyData];
}

- (IBAction)SignUpAction:(id)sender {
    [self.emailTextField endEditing:YES];
    [self.passwordTextField endEditing:YES];
    [self navigationToCreateProfileViewController];
}

-(void)navigationToCreateProfileViewController {
    
    NSInteger countVC = self.navigationController.viewControllers.count - 1;
    if([self.navigationController.viewControllers[countVC - 1] isMemberOfClass:[CreateProfileViewController class]])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        CreateProfileViewController *signinVC = [storyboard instantiateViewControllerWithIdentifier:@"CreateProfileViewController"];
        
        [self.navigationController pushViewController:signinVC animated:YES];
    }
}

@end
