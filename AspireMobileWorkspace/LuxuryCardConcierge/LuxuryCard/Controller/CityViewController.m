//
//  CityViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "CityViewController.h"
#import "CityItem.h"
#import "CityTableViewCell.h"
#import "AppData.h"
#import "UtilStyle.h"
#import "HomeLXViewController.h"
#import "ExploreViewController.h"
#import "WSPreferences.h"
#import "PreferenceObject.h"

#define CENTER_TAG 1
#define LEFT_PANEL_TAG 2
#define RIGHT_PANEL_TAG 3

#define CORNER_RADIUS 4

#define SLIDE_TIMING .25
#define PANEL_WIDTH 60

@interface CityViewController ()<UITableViewDelegate, UITableViewDataSource, DataLoadDelegate>
{
    BOOL isFirstSelectCity;
    WSPreferences *wsPreferences;
    CityItem *selectedCity;
}

@end

@implementation CityViewController

- (void)viewDidLoad {
    isShowAskConciergeBarButton = YES;
    [super viewDidLoad];
    

    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"Choose a Destination", nil)];
    [self createBackBarButton];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self trackingScreenByName:@"City list"];
    NSInteger index = self.currentCity ?  [self.cityLst indexOfObject:self.currentCity] : 0;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    if(index > self.cityLst.count - 3){
        [self.view layoutIfNeeded];
        CGPoint offset = CGPointMake(0, self.tableView.contentSize.height);
        [self.tableView setContentOffset:offset animated:YES];
    }
    else{
        //        [self.tableView reloadData];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView scrollToRowAtIndexPath:indexPath
                                  atScrollPosition:UITableViewScrollPositionTop animated:NO];
        });
        
    }
    
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}
-(void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    // Stop being the navigation controller's delegate
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


-(void)initView
{
    self.tableView.separatorColor = [UtilStyle colorForSeparateLine];
    [self.tableView registerNib:[UINib nibWithNibName:@"CityTableViewCell" bundle:nil] forCellReuseIdentifier:@"CityTableViewCell"];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 70.f;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

-(void)initData
{
    self.cityLst = [AppData getSelectionCityList];
}

#pragma mark TABLEVIEW DELEGATE
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.cityLst.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CityTableViewCell"];
    [cell initCellWithData:[self.cityLst objectAtIndex:indexPath.row]];
//    if()
        [cell setHiddenBottomLine:indexPath.row == self.cityLst.count-1];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CityTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setBackgroundColor:[UtilStyle colorForSeparateLine]];
    selectedCity = [self.cityLst objectAtIndex:indexPath.row];
    if (selectedCity) {
        [self trackingEventByName:selectedCity.name withAction:SelectActionType withCategory:CitySelectionCategoryType];
    }
    [AppData setCurrentCityWithCode:selectedCity.cityCode == nil ? selectedCity.regionCode : selectedCity.cityCode];
    
    [self addPreferences];
    if(self.delegate){
        [self.delegate getDataBaseOnCity:selectedCity];
//        [self.delegate reloadDataHome];
    }
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"HomeReloadNoti"
     object:self];
    [tableView deselectRowAtIndexPath:indexPath animated:false];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70*SCREEN_SCALE;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

#pragma mark LOCAL PROCESS
- (void)addPreferences{
    [self startActivityIndicator];
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    NSMutableArray *arraySubDictValues = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *hotelDict = [[NSMutableDictionary alloc] init];
    [hotelDict setValue:@"Hotel" forKey:@"Type"];
    
    PreferenceObject *hotelPref = [self getPreferenceByType:HotelPreferenceType];
    NSString *value = ([[SessionData shareSessiondata] isUseLocation])?@"YES":@"NO";
    NSString *currentCityCode;
    CityItem *item = [AppData getSelectedCity];
    if (item) {
        currentCityCode = item.cityCode == nil? item.regionCode : item.cityCode;
    }
    
    if ([SessionData shareSessiondata].arrayPreferences.count > 0 && hotelPref.preferenceID.length > 0){
        [hotelDict setValue:hotelPref.preferenceID forKey:@"MyPreferencesId"];
        [hotelDict setValue:hotelPref.value forKey:@"Preferredstarrating"];
        [hotelDict setValue:value forKey:@"SmokingPreference"];
        if (currentCityCode.length > 0) {
            [hotelDict setValue:currentCityCode forKey:@"NameofLoyaltyProgram"];
        }
        [arraySubDictValues addObject:hotelDict];
        [dataDict setValue:arraySubDictValues forKey:@"PreferenceDetails"];
        
        wsPreferences = [[WSPreferences alloc] init];
        wsPreferences.delegate = self;
        [wsPreferences updatePreference:dataDict];
        
    }else{
        [hotelDict setValue:@"" forKey:@"Preferredstarrating"];
        [hotelDict setValue:value forKey:@"SmokingPreference"];
        if (currentCityCode.length > 0) {
            [hotelDict setValue:currentCityCode forKey:@"NameofLoyaltyProgram"];
        }
        [arraySubDictValues addObject:hotelDict];
        
        //        //Begin: support for android.
        //        NSMutableDictionary *diningDict = [[NSMutableDictionary alloc] init];
        //        [diningDict setValue:@"Dining" forKey:@"Type"];
        //        [diningDict setValue:@"" forKey:@"CuisinePreferences"];
        //        [arraySubDictValues addObject:diningDict];
        //
        //        NSMutableDictionary *transportationDict = [[NSMutableDictionary alloc] init];
        //        [transportationDict setValue:@"Car Rental" forKey:@"Type"];
        //        [transportationDict setValue:@"" forKey:@"PreferredRentalVehicle"];
        //        [arraySubDictValues addObject:transportationDict];
        //        //End: support for android.
        
        [dataDict setValue:arraySubDictValues forKey:@"PreferenceDetails"];
        wsPreferences = [[WSPreferences alloc] init];
        wsPreferences.delegate = self;
        [wsPreferences addPreference:dataDict];
    }
    
}

-(PreferenceObject*)getPreferenceByType:(PreferenceType)type{
    if ([SessionData shareSessiondata].arrayPreferences.count > 0) {
        for (PreferenceObject *preference in [SessionData shareSessiondata].arrayPreferences) {
            if (preference.type == type) {
                return preference;
            }
        }
    }
    return nil;
}

-(void)loadDataDoneFrom:(WSBase*)ws {
    [self stopActivityIndicator];
    ExploreViewController *exploreViewController = [[ExploreViewController alloc] init];
    exploreViewController.currentCity = selectedCity;
    [self.navigationController pushViewController:exploreViewController animated:YES];
}

@end
