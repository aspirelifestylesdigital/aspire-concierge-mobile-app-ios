//
//  AboutAppViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/16/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "AboutAppViewController.h"
#import "WSB2CBase.h"
#import "WSB2CGetAboutInfo.h"
#import "AboutInfoItem.h"
#import "AppDelegate.h"

@interface AboutAppViewController ()<DataLoadDelegate, UIScrollViewDelegate, UIWebViewDelegate>
{
    NSString *content;
    UIBarButtonItem *backButtonItem;
}


@end

@implementation AboutAppViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createBackBarButton];
    [self setNavigationBarWithDefaultColorAndTitle:@"Experience More"];
    
    self.webView.delegate = self;
    self.webView.scrollView.delegate = self;
    [self.webView setBackgroundColor:[UIColor clearColor]];
    [self.webView setOpaque:NO];
    
    isHiddenTabBar = YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self trackingScreenByName:@"About this app"];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
//    [self setTabBarNormalStyle];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)initData
{
    [self startActivityIndicator];
    WSB2CGetAboutInfo *ws = [[WSB2CGetAboutInfo alloc] init];
    ws.delegate = self;
    [ws retrieveDataFromServer];
}


#pragma mark DATALOADDELEGATE


-(void)loadDataDoneFrom:(WSB2CGetAboutInfo *)wsAboutInfo
{
    if(wsAboutInfo.data.count > 0)
    {
        content = [self replaceWebLinkInString:((AboutInfoItem *)[wsAboutInfo.data objectAtIndex:0]).textInfo];
        NSRange bodyHtmlRange = [content rangeOfString:@"font-family"];
        if (bodyHtmlRange.location == NSNotFound) {
            [self.webView loadHTMLString:centerHTMLString(content, FONT_MarkForMC_REGULAR, (16.0 * SCREEN_SCALE_BASE_WIDTH_375), @"#FFFFFF") baseURL:nil];
        }else{
            [self.webView loadHTMLString:content baseURL:nil];
        }
    }
    else{
       [self stopActivityIndicator];
    }
}

- (NSString*) replaceWebLinkInString:(NSString*)string{
    
    NSArray *tempArr = getMatchesFromString(string,@"href+=+\"(?!#+)(.*?)+\"",NSRegularExpressionCaseInsensitive);
    
    NSMutableArray* vvv = [NSMutableArray new];
    for (NSString* test in tempArr) {
        NSString* t = strimStringFrom(test,@[@"href=|\"?"]);
        if(![t containsString:@"http"]) {
            [vvv addObject:@{test:[[@"href=\"http://" stringByAppendingString:t] stringByAppendingString:@"\""]}];
        }
    }
    NSMutableString* tttt = [NSMutableString stringWithString:string];
    for (NSDictionary* t in vvv) {
        if([string containsString:[[t allKeys] firstObject]]) {
            string = [tttt stringByReplacingOccurrencesOfString:[[t allKeys] firstObject] withString:[t objectForKey:[[t allKeys] firstObject]]];
        }
    }
    return string;
}


-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self stopActivityIndicator];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
#ifdef DEBUG
    NSLog(@"%@", request.URL);
#endif
    if (navigationType==UIWebViewNavigationTypeLinkClicked) {
        if ([request.URL.absoluteString containsString:@"http"]) {

            // It was a link
            [self showActiveAlertWithTitle:NSLocalizedString(@"alert_title", nil) withMessage:NSLocalizedString(@"category_more_info_msg", nil) withFistBtnTitle:NSLocalizedString(@"Yes", nil) forFirstAction:^{
                [[UIApplication sharedApplication] openURL:request.URL];
            } withSecondBtnTitle:NSLocalizedString(@"CANCEL", nil) forSeconAction:nil];
            return NO;
        }
    }
    
    return YES;
}
@end
