//
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/13/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "CreateProfileViewController.h"
#import "MenuViewController.h"
#import "UDASignInViewController.h"
#import "AppDelegate.h"
#import "WSCreateUser.h"
#import "WSB2CGetAccessToken.h"
#import "WSB2CGetUserDetails.h"
#import "WSB2CGetRequestToken.h"
#import "WSPreferences.h"
#import "NSString+Utis.h"
#import "UITextField+Extensions.h"
#import "UIButton+Extension.h"
#import "UIView+Extension.h"
#import "UILabel+Extension.h"
#import "HomeLXViewController.h"
#import "Constant.h"
#import "MyProfileLXViewController.h"
#import "AppData.h"
#import "AppDelegate.h"
#import "UserObject.h"
#import "UtilStyle.h"
#import "PreferenceObject.h"
#import "Country.h"
#import "TermsOfUseViewController.h"
#import "PolicyViewController.h"
#import "NSString+AESCrypt.h"

#define TEXT_FIELD_FONT_SIZE 18

@interface CreateProfileViewController ()<DataLoadDelegate, UITextFieldDelegate, DropDownViewDelegate, UIGestureRecognizerDelegate>
{
    WSB2CGetAccessToken* wsB2CGetAccessToek;
    WSB2CGetRequestToken* wsB2CGetRequestToken;
    WSB2CGetUserDetails* wsGetUser;
    WSPreferences *wsPreferences;
    AppDelegate* appdelegate;
    UITapGestureRecognizer *tappedOutsideKeyboards;
    NSArray *countries;
    UILabel *termOfUseLbl;
    UILabel *policyLbl;
}

@property (nonatomic, strong) WSCreateUser *wsCreateUser;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnSC;

@end

@implementation CreateProfileViewController

#pragma mark - LIFECYCLE
- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *tab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkBox:)];
    self.commitmentLabel.userInteractionEnabled = YES;
    [self.commitmentLabel addGestureRecognizer:tab];
    countries = getCountryList();
    [self setUIStyte];
    [self createBackBarButton];
    [self setNavigationBarWithDefaultColorAndTitle:@"Sign Up"];
    _btnSC.constant = 110 * SCREEN_SCALE;
    [self setTextViewsDefaultBottomBolder];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    tappedOutsideKeyboards = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tappedOutsideKeyboards.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [self.navigationController.view addGestureRecognizer:tappedOutsideKeyboards];
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.view layoutIfNeeded];
    if (!_isUpdateProfile) {
        [self trackingScreenByName:@"Sign up"];
    }
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController.view removeGestureRecognizer:tappedOutsideKeyboards];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - SETUP UI
- (void) setUIStyte {
    if ([self.confirmMessageView.subviews count] <= 0) {
        [self buildAgreeTextViewFromString:NSLocalizedString(@"bin_confirm_msg",nil)];
    }
    
    self.phoneNumberText.keyboardType = UIKeyboardTypePhonePad;
    self.phoneNumberText.tag = PHONE_NUMBER_TEXTFIELD_TAG;
    self.emailText.keyboardType = UIKeyboardTypeEmailAddress;
    self.firstNameText.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    self.lastNameText.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    
    self.emailText.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailText.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    self.zipCodeText.autocorrectionType = UITextAutocorrectionTypeNo;
    self.zipCodeText.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    self.numberCardText.keyboardType = UIKeyboardTypeNumberPad;
    self.numberCardText.autocorrectionType = UITextAutocorrectionTypeNo;
    self.numberCardText.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    self.salutationDropDown.leadingTitle = 0.0f;
    self.salutationDropDown.title = @"Select one";
    self.salutationDropDown.listItems = @[@"Dr", @"Miss", @"Mr", @"Mrs", @"Ms"];
    self.salutationDropDown.titleColor = colorFromHexString(TEXTFIELD_PLACEHOLDER_COLOR_STANDARD);
    self.salutationDropDown.delegate = self;
    self.salutationDropDown.itemTextColor = colorFromHexString(LABEL_DROPDOWN_COLOR);
    
    self.countryCodeDropDown.type = OtherDropDownType;
    self.countryCodeDropDown.leadingTitle = 0.0f;
    self.countryCodeDropDown.itemLineBreakMode = NSLineBreakByTruncatingMiddle;
    self.countryCodeDropDown.title = @"Country Code";
    
    
    UIFont *titleFont = [UIFont fontWithName:FONT_MarkForMC_MED size:FONT_SIZE_18*SCREEN_SCALE_BASE_WIDTH_375];
    UIFont *itemFont = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:FONT_SIZE_16*SCREEN_SCALE_BASE_WIDTH_375];
    [self.countryCodeDropDown setTitleFont:titleFont];
    [self.countryCodeDropDown setItemsFont:itemFont];
    [self.salutationDropDown setTitleFont:titleFont];
    [self.salutationDropDown setItemsFont:itemFont];
    NSMutableArray *listCountryData = [[NSMutableArray alloc] init];
    for (Country *country in countries) {
        [listCountryData addObject:country.countryDescription];
        
        if([country.countryCode isEqualToString:@"USA"])
        {
            self.countryCodeDropDown.title = [NSString stringWithFormat:@"%@ (%@)",country.countryCode, country.countryNumber];
            self.countryCodeDropDown.titleOther = country.countryDescription;
            self.countryCodeDropDown.valueStatus = NewValueStatus;
        }
    }
    self.countryCodeDropDown.listItems = listCountryData;
    self.countryCodeDropDown.itemDisplay = 5;
    self.countryCodeDropDown.titleColor = colorFromHexString(TEXTFIELD_PLACEHOLDER_COLOR_STANDARD);
    self.countryCodeDropDown.delegate = self;
    self.countryCodeDropDown.itemTextColor = colorFromHexString(LABEL_DROPDOWN_COLOR);
    
    
    self.firstNameText.delegate = self;
    self.lastNameText.delegate = self;
    self.emailText.delegate = self;
    self.phoneNumberText.delegate = self;
    self.zipCodeText.delegate = self;
    self.numberCardText.delegate = self;
    self.passwordText.delegate = self;
//    self.confirmPasswordText.delegate = self;
    
    /*
    [self createAsteriskForTextField:self.firstNameText];
    [self createAsteriskForTextField:self.lastNameText];
    [self createAsteriskForTextField:self.emailText];
    [self createAsteriskForTextField:self.phoneNumberText];
    [self createAsteriskForTextField:self.zipCodeText];
    [self createAsteriskForTextField:self.numberCardText];
    [self createAsteriskForTextField:self.passwordText];
    [self createAsteriskForTextField:self.confirmPasswordText];
     */
    
    [self.firstNameText setDefaultStyleForTextField];
    [self.lastNameText setDefaultStyleForTextField];
    [self.emailText setDefaultStyleForTextField];
    [self.phoneNumberText setDefaultStyleForTextField];
    [self.zipCodeText setDefaultStyleForTextField];
    [self.numberCardText setDefaultStyleForTextField];
    [self.passwordText setDefaultStyleForTextField];
//    [self.confirmPasswordText setDefaultStyleForTextField];
    
    [self.countryLbl setDefaultStyleForTextFieldTitle];
    [self.salutationLbl setDefaultStyleForTextFieldTitle];
    [self.firstNameLbl setDefaultStyleForTextFieldTitle];
    [self.lastNameLbl setDefaultStyleForTextFieldTitle];
    [self.emailLbl setDefaultStyleForTextFieldTitle];
    [self.mobilePhoneLbl setDefaultStyleForTextFieldTitle];
    [self.zipcodeLbl setDefaultStyleForTextFieldTitle];
    [self.numberCardLbl setDefaultStyleForTextFieldTitle];
    [self.passwordLbl setDefaultStyleForTextFieldTitle];
    [self.confirmLbl setDefaultStyleForTextFieldTitle];
    [self.passwordRequirementMessageLbl setDefaultStyleForTextFieldTitle];
    
    self.signInHere.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:FONT_SIZE_12 * SCREEN_SCALE_BASE_WIDTH_375];
    self.signInHere.textColor = [UIColor whiteColor];
    [self.signInHere setUserInteractionEnabled:YES];
    UITapGestureRecognizer *backToSign = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backToSignInAction)];
    [self.signInHere addGestureRecognizer:backToSign];

    self.confirmMessageLbl.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:18 * SCREEN_SCALE_BASE_WIDTH_375];
    self.confirmMessageLbl.textColor = [UIColor whiteColor];
    
    self.countryLbl.text = NSLocalizedString(@"Country Code", nil);
    self.salutationLbl.text = NSLocalizedString(@"Please select a salutation", nil);
    self.firstNameLbl.text = NSLocalizedString(@"First Name", nil);
    self.lastNameLbl.text = NSLocalizedString(@"Last Name", nil);
    self.emailLbl.text = NSLocalizedString(@"Email", nil);
    self.mobilePhoneLbl.text = NSLocalizedString(@"Mobile Phone Number", nil);
    self.zipcodeLbl.text = NSLocalizedString(@"Zip Code", nil);
    self.numberCardLbl.text = NSLocalizedString(@"First Six Digits of Credit Card Number", nil);
    self.passwordLbl.text = NSLocalizedString(@"Password", nil);
    self.confirmLbl.text = NSLocalizedString(@"Confirm Password", nil);
    self.confirmMessageLbl.text = NSLocalizedString(@"I acknowledge that I have read and agree to the Terms of Use and Privacy Policy", nil);
    self.passwordRequirementMessageLbl.text = NSLocalizedString(@"Password must be between 10 and 25 characters, including at least 1 uppercase, 1 lowercase, 1 special character and 1 number.", nil);
    self.signInHere.text = NSLocalizedString(@"Already have an account? Sign in here", nil);
    
    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
    if (profileDictionary && [self isKindOfClass:[MyProfileLXViewController class]]) {
        
        NSString *salutation = [profileDictionary objectForKey:keySalutation];
        if (salutation.length > 1) {
            self.salutationDropDown.titleColor = colorFromHexString(TEXTFIELD_TEXT_COLOR_STANDARD);
            self.salutationDropDown.title = salutation;
        }
        NSString *countryCode = [profileDictionary objectForKey:keyCountryCode];
        if (countryCode.length > 0) {
            Country *currentCountry = [[Country alloc] init];
            NSString *zipCode = [profileDictionary objectForKey:keyZipCode];
            if ([countryCode isEqualToString:@"1"]) {
                if (zipCode.length > 0) {
                    currentCountry = ([[zipCode substringWithRange:NSMakeRange(0, 1)] isNumber]) ? getCountryByCodeName(@"USA") : getCountryByCodeName(@"CAN");
                }else {
                    currentCountry = getCountryByCodeName(@"USA");
                }
            }else{
                currentCountry = getCountryByNumber(countryCode);
            }
            self.countryCodeDropDown.titleColor = colorFromHexString(TEXTFIELD_TEXT_COLOR_STANDARD);
            self.countryCodeDropDown.title = [NSString stringWithFormat:@"%@ (%@)",currentCountry.countryCode, currentCountry.countryNumber];
            self.countryCodeDropDown.titleOther = currentCountry.countryDescription;
        }
        
        self.firstNameText.text = [profileDictionary objectForKey:keyFirstName];
        self.lastNameText.text = [profileDictionary objectForKey:keyLastName];
        self.emailText.text = [profileDictionary objectForKey:keyEmail];
        self.phoneNumberText.text = [profileDictionary objectForKey:keyMobileNumber];
        self.zipCodeText.text = [profileDictionary objectForKey:keyZipCode];
        
        isUseLocation = [[SessionData shareSessiondata] isUseLocation];
        [self setImageLocation:isUseLocation];
        
        currentFirstName = [profileDictionary objectForKey:keyFirstName];
        currentEmail = [profileDictionary objectForKey:keyEmail] ;
        currentPhone = [profileDictionary objectForKey:keyMobileNumber];
        currentLastName = [profileDictionary objectForKey:keyLastName];
        currentZipCode = [profileDictionary objectForKey:keyZipCode];
        
        self.firstNameText.text = [currentFirstName createMaskForText:NO];
        self.emailText.text = [currentEmail createMaskForText:YES];
        self.lastNameText.text = [currentLastName createMaskForText:NO];
//        self.phoneNumberText.text = (currentPhone.length > 3) ? [currentPhone createMaskStringBeforeNumberCharacter:3]:currentPhone;
        self.phoneNumberText.text = [currentPhone createMaskForText:NO];
        self.emailText.enabled = NO;
        self.zipCodeText.text = currentZipCode;
        
    }else{
        
        currentFirstName = @"";
        currentEmail = @"";
        currentPhone = @"";
        currentLastName = @"";
        currentZipCode = @"";
        currentCardNumber = @"";
    }
    [self createPlaceHolderForTextField];
    
    
    [self.myView setBackgroundColorForView];
    [self.scrollView setBackgroundColorForView];
    
    [self enableSubmitButton:NO];
    [self.submitButton setTitle:NSLocalizedString(@"Sign Up", nil) forState:UIControlStateNormal] ;
    [self.submitButton.titleLabel setFont:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:FONT_SIZE_14*SCREEN_SCALE_BASE_WIDTH_375]];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.checkBoxImageTopConstraint.constant = (IPAD) ? 12.0f : 2.0f;
    });
}

- (void)createPlaceHolderForTextField{
    self.firstNameText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:NSLocalizedString(@"First Name", nil)];
    self.lastNameText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:NSLocalizedString(@"Last Name", nil)];
    self.emailText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:NSLocalizedString(@"email@address.com", nil)];
    self.phoneNumberText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:NSLocalizedString(@"555-555-5555", nil)];
    self.zipCodeText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:NSLocalizedString(@"99999", nil)];
    self.numberCardText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:NSLocalizedString(@"XXXXXX", nil)];
    self.passwordText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:NSLocalizedString(@"**********", nil)];
    /*
     self.confirmPasswordText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:NSLocalizedString(@"**********", nil)];
     */
}

#pragma mark CONFIRM MESSAGE PROCESS

- (void)buildAgreeTextViewFromString:(NSString *)localizedString
{
    // 1. Split the localized string on the # sign:
    NSArray *localizedStringPieces = [localizedString componentsSeparatedByString:@"#"];
    self.confirmMessageView.userInteractionEnabled = YES;
    // 2. Loop through all the pieces:
    NSUInteger msgChunkCount = localizedStringPieces ? localizedStringPieces.count : 0;
    CGPoint wordLocation = CGPointMake(0.0, 0.0);
    for (NSUInteger i = 0; i < msgChunkCount; i++)
    {
        NSString *chunk = [localizedStringPieces objectAtIndex:i];
        if ([chunk isEqualToString:@""])
        {
            continue;     // skip this loop if the chunk is empty
        }
        
        // 3. Determine what type of word this is:
        BOOL isTermsOfServiceLink = [chunk hasPrefix:@"<ts>"];
        BOOL isPrivacyPolicyLink  = [chunk hasPrefix:@"<pp>"];
        BOOL isLink = (BOOL)(isTermsOfServiceLink || isPrivacyPolicyLink);
        
        // 4. Create label, styling dependent on whether it's a link:
        UILabel *label = [[UILabel alloc] init];
        [label setDefaultStyleForTextFieldTitle];
        
        label.text = chunk;
        label.userInteractionEnabled = isLink;
        
        if (isLink)
        {
            // 5. Set tap gesture for this clickable text:
            SEL selectorAction = isTermsOfServiceLink ? @selector(tapOnTermsOfServiceLink:) : @selector(tapOnPrivacyPolicyLink:);
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                         action:selectorAction];
            [label addGestureRecognizer:tapGesture];
            
            // Trim the markup characters from the label:
            if (isTermsOfServiceLink)
            {
                label.text = [label.text stringByReplacingOccurrencesOfString:@"<ts>" withString:@""];
                termOfUseLbl = label;
            }
            
            
            if (isPrivacyPolicyLink)
            {
                label.text = [label.text stringByReplacingOccurrencesOfString:@"<pp>" withString:@""];
                policyLbl = label;
            }
            
            
            NSArray * objects = [[NSArray alloc] initWithObjects:[UIColor colorWithWhite:1.0 alpha:0.8], [NSNumber numberWithInt:NSUnderlineStyleSingle], nil];
            NSArray * keys = [[NSArray alloc] initWithObjects:NSForegroundColorAttributeName, NSUnderlineStyleAttributeName, nil];
            
            NSDictionary * linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
            
            NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:label.text attributes:linkAttributes];
            
            [label setAttributedText:attributedString];
            
        }
        else
        {
            UITapGestureRecognizer *tappedOutsideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enableSubmitButtonByTappingOnConfirmMessage)];
            [label addGestureRecognizer:tappedOutsideKeyboard];
            label.userInteractionEnabled = YES;
//            [label setTextColor:[UIColor whiteColor]];
            label.tag = 1000;
        }
        
        // 6. Lay out the labels so it forms a complete sentence again:
        
        // If this word doesn't fit at end of this line, then move it to the next
        // line and make sure any leading spaces are stripped off so it aligns nicely:
        
        [label sizeToFit];
        
        if (self.confirmMessageView.frame.size.width < wordLocation.x + label.bounds.size.width)
        {
            wordLocation.x = 0.0;                       // move this word all the way to the left...
            wordLocation.y += label.frame.size.height;  // ...on the next line
            
            // And trim of any leading white space:
            NSRange startingWhiteSpaceRange = [label.text rangeOfString:@"^\\s*"
                                                                options:NSRegularExpressionSearch];
            if (startingWhiteSpaceRange.location == 0)
            {
                label.text = [label.text stringByReplacingCharactersInRange:startingWhiteSpaceRange
                                                                 withString:@""];
                [label sizeToFit];
            }
        }
        
        // Set the location for this label:
        label.frame = CGRectMake(wordLocation.x,
                                 wordLocation.y,
                                 label.frame.size.width,
                                 label.frame.size.height);
        
        // Show this label:
        [self.confirmMessageView addSubview:label];
        
        // Update the horizontal position for the next word:
        wordLocation.x += label.frame.size.width;
        
        [_confirmMessageView setNeedsLayout];
        [_confirmMessageView layoutIfNeeded];
    }
}

- (void)tapOnTermsOfServiceLink:(UITapGestureRecognizer *)tapGesture
{
    if([self isDisableSubmitTapped:tapGesture])
    {
        return;
    }
    
    [self dismissKeyboard];
    
    if (tapGesture.state == UIGestureRecognizerStateEnded)
    {
        TermsOfUseViewController *vc = [[TermsOfUseViewController alloc] init];
        //vc.delegate = self;
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        [self.navigationController pushViewController:vc animated:NO];
    }
}


- (void)tapOnPrivacyPolicyLink:(UITapGestureRecognizer *)tapGesture
{
    if([self isDisableSubmitTapped:tapGesture])
    {
        return;
    }
    
    [self dismissKeyboard];
    
    if (tapGesture.state == UIGestureRecognizerStateEnded)
    {
        PolicyViewController *vc = [[PolicyViewController alloc] init];
        //vc.delegate = self;
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        [self.navigationController pushViewController:vc animated:NO];
    }
}


-(BOOL)isDisableSubmitTapped:(id)sender
{
    if([sender isKindOfClass:[UITapGestureRecognizer class]])
    {
        UITapGestureRecognizer *tapRecognization = (UITapGestureRecognizer *)sender;
        CGPoint location = [tapRecognization locationInView:self.view];
        CGRect fingerRect = CGRectMake(location.x-5, location.y-5, 10, 10);
        
        if(CGRectIntersectsRect(fingerRect, self.submitButton.frame) && !self.submitButton.enabled)
        {
            [self dismissKeyboard];
            return YES;
        }
    }
    
    return NO;
}

-(void) enableSubmitButtonByTappingOnConfirmMessage
{
    self.submitButton.enabled = !self.submitButton.enabled;
    if (self.submitButton.enabled) {
        self.checkBoxImage.image = [UIImage imageNamed:@"radio_checked"];
    }else{
        self.checkBoxImage.image = [UIImage imageNamed:@"radio_unchecked"];
    }
    [self setSubmitButtonStyleWithState:self.submitButton.enabled];
}

-(void)backToSignInAction
{
    NSInteger countVC = self.navigationController.viewControllers.count - 1;
    if([self.navigationController.viewControllers[countVC - 1] isMemberOfClass:[UDASignInViewController class]])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UDASignInViewController *signinVC = [storyboard instantiateViewControllerWithIdentifier:@"UDASignInViewController"];
        
        [self.navigationController pushViewController:signinVC animated:YES];
    }
}


- (void)setSubmitButtonStyleWithState:(BOOL)enable {
    enable ? [self.submitButton setDefaultStyle] : [self.submitButton setDisableStyle];
}

- (void)initView{
    
    // Text Style For Title
    self.titleLable.attributedText = [UtilStyle setTextStyleForTitleViewControllerWithMessage:@"SIGN UP"];
    [self.titleLable setTextColor:colorFromHexString(DEFAULT_HIGHLIGHT_COLOR)];
    
    // Text Style For Greet Message
    NSString *message = NSLocalizedString(@"commitment_profile_message", nil);
    self.commitmentLabel.attributedText = [UtilStyle setLargeSizeStyleForLabelWithMessage:message];
    //[self setCheckBoxState:isCheck];
}
-(void) createAsteriskForTextField:(UITextField *)textField
{
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [textField setLeftView:leftView];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    
    asteriskImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"asterisk_icon"]];
    
    asteriskImage.frame = CGRectMake(0.0f, 0.0f, 20.0f, 20.0f);
    asteriskImage.contentMode = UIViewContentModeCenter;
    [textField setRightView:asteriskImage];
    [textField setRightViewMode:UITextFieldViewModeAlways];
    
}

-(void)setTextViewsDefaultBottomBolder
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.firstNameText setBottomBolderDefaultColor];
        [self.lastNameText setBottomBolderDefaultColor];
        [self.emailText setBottomBolderDefaultColor];
        [self.phoneNumberText setBottomBolderDefaultColor];
        [self.passwordText setBottomBolderDefaultColor];
//        [self.confirmPasswordText setBottomBolderDefaultColor];
        [self.salutationDropDown setBottomBolderDefaultColor];
        [self.countryCodeDropDown setBottomBolderDefaultColor];
        [self.zipCodeText setBottomBolderDefaultColor];
        [self.numberCardText setBottomBolderDefaultColor];
    });
}

#pragma mark - SETUP DATA
- (void) getUserInfo{
    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
    if (profileDictionary) {
        
        NSString *salutation = [profileDictionary objectForKey:keySalutation];
        self.salutationDropDown.titleColor = (salutation.length > 1)?colorFromHexString(TEXTFIELD_TEXT_COLOR_STANDARD):colorFromHexString(TEXTFIELD_PLACEHOLDER_COLOR_STANDARD);
        self.salutationDropDown.title = (salutation.length > 1)?salutation:@"Please select a salutation.";
        
        NSString *countryCode = [profileDictionary objectForKey:keyCountryCode];
        self.countryCodeDropDown.titleColor = (countryCode.length > 0)?colorFromHexString(TEXTFIELD_TEXT_COLOR_STANDARD):colorFromHexString(TEXTFIELD_PLACEHOLDER_COLOR_STANDARD);
        
        if (countryCode.length > 0) {
            Country *currentCountry = [[Country alloc] init];
            NSString *zipCode = [profileDictionary objectForKey:keyZipCode];
            if ([countryCode isEqualToString:@"1"]) {
                if (zipCode.length > 0) {
                    currentCountry = ([[zipCode substringWithRange:NSMakeRange(0, 1)] isNumber]) ? getCountryByCodeName(@"USA") : getCountryByCodeName(@"CAN");
                }else {
                    currentCountry = getCountryByCodeName(@"USA");
                }
                
            }else{
                currentCountry = getCountryByNumber(countryCode);
            }
            self.countryCodeDropDown.title = (countryCode.length > 0)?[NSString stringWithFormat:@"%@ (%@)",currentCountry.countryCode, currentCountry.countryNumber]:@"Please select a country code.";
            self.countryCodeDropDown.titleOther = (countryCode.length > 0)?currentCountry.countryDescription:@"Please select a country code.";
        }
        currentFirstName = [profileDictionary objectForKey:keyFirstName];
        currentEmail = [profileDictionary objectForKey:keyEmail] ;
        currentPhone = [profileDictionary objectForKey:keyMobileNumber];
        currentLastName = [profileDictionary objectForKey:keyLastName];
        currentZipCode = [profileDictionary objectForKey:keyZipCode];
        
        self.firstNameText.text = [currentFirstName createMaskForText:NO];
        self.emailText.text = [currentEmail createMaskForText:YES];
        self.lastNameText.text = [currentLastName createMaskForText:NO];
//        self.phoneNumberText.text = (currentPhone.length > 3) ? [currentPhone createMaskStringBeforeNumberCharacter:3]: currentPhone;
       self.phoneNumberText.text =  [currentPhone createMaskForText:NO];
        self.zipCodeText.text = currentZipCode;
        isUseLocation = [[SessionData shareSessiondata] isUseLocation];
        [self setImageLocation:isUseLocation];
    }
}
- (void)setUserObjectWithDict:(NSDictionary*)dict{
    [[SessionData shareSessiondata] setUserObjectWithDict:dict];
}

- (void)setUserDefaultInfoWithDict:(UserRegistrationItem*)item{
    [[SessionData shareSessiondata] setOnlineMemberID:item.OnlineMemberID];
    [[SessionData shareSessiondata] setOnlineMemberDetailIDs:item.OnlineMemberDetailIDs];
}
#pragma mark - ACTIONS
- (IBAction)checkBox:(id)sender {
    isCheck = !isCheck;
    [self setCheckBoxState:isCheck];
}

/*
- (IBAction)actionSwitchLocation:(id)sender {
    
    isUseLocation = !isUseLocation;
    [self setImageLocation:isUseLocation];
    if (![self isKindOfClass:[MyProfileLXViewController class]]) {
        [[SessionData shareSessiondata] setIsUseLocation:isUseLocation];
        if (isUseLocation) {
            [LocationComponent startRequestLocation];
        }
    }
    else{
        [[SessionData shareSessiondata] setIsUseLocation:isUseLocation];
        if (isUseLocation) {
            [LocationComponent startRequestLocation];
        }
    }
   [self changeUseLocation];
}
 */

- (IBAction)CheckboxAction:(id)sender {
    if (isCheck) {
        self.checkBoxImage.image = [UIImage imageNamed:@"radio_unchecked"];
        isCheck = NO;
    }else{
        self.checkBoxImage.image = [UIImage imageNamed:@"radio_checked"];
        isCheck = YES;
    }
    [self enableSubmitButton:isCheck];
}

- (void)enableSubmitButton:(BOOL)isEnable {
    self.submitButton.enabled = isEnable;
    [self setSubmitButtonStyleWithState:self.submitButton.enabled];
}

- (IBAction)SubmitAction:(id)sender {
    [self resignFirstResponderForAllTextField];
    [self verifyAccountData:NO];
}

- (IBAction)CancelAction:(id)sender {
    /*
    if ([AppData isCreatedProfile]) {
        
    SWRevealViewController *revealViewController = self.revealViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UDASignInViewController *signInViewController = [storyboard instantiateViewControllerWithIdentifier:@"UDASignInViewController"];
    [revealViewController pushFrontViewController:signInViewController animated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
         */
}

#pragma mark - API
- (void)updateProfileWithAPI{
    [self startActivityIndicator];
    NSMutableDictionary *userDict = [[NSMutableDictionary alloc] init];
    [userDict setValue:B2C_ConsumerKey forKey:@"ConsumerKey"];
    [userDict setValue:_isUpdateProfile ? @"Registration" : @"Registration" forKey:@"Functionality"];
    [userDict setValue:(self.salutationDropDown.title.length > 1) ? self.salutationDropDown.title : @" " forKey:@"Salutation"];
    [userDict setValue:currentFirstName forKey:@"FirstName"];
    [userDict setValue:currentLastName forKey:@"LastName"];
    
    NSString *phoneNumberWithCode = @"";
    if (self.countryCodeDropDown.selectedIndex == -1) {
        phoneNumberWithCode = [NSString stringWithFormat:@"%@",[currentPhone stringByReplacingOccurrencesOfString:@"-" withString:@""]];
    }else {
        Country *country = [countries objectAtIndex:self.countryCodeDropDown.selectedIndex];
        phoneNumberWithCode = [NSString stringWithFormat:@"%@%@",country.countryNumber,[currentPhone stringByReplacingOccurrencesOfString:@"-" withString:@""]];
    }

    [userDict setValue:phoneNumberWithCode forKey:@"MobileNumber"];
    [userDict setValue:currentEmail forKey:@"Email"];
    [userDict setValue:currentZipCode forKey:@"ZipCode"];
    [userDict setValue:currentPassword forKey:@"Password"];
    [userDict setValue:B2C_DeviceId forKey:@"MemberDeviceID"];
    [[SessionData shareSessiondata] setIsUseLocation:isUseLocation];
    
    self.wsCreateUser = [[WSCreateUser alloc] init];
    self.wsCreateUser.delegate = self;
    if (!_isUpdateProfile) {
        [self.wsCreateUser createUser:userDict];
    }else{
        [self.wsCreateUser updateUser:userDict];
    }
    
}

-(void)loadDataDoneFrom:(WSBase*)ws {
    
    if (ws.task == WS_CREATE_USER) {
        if (ws.data) {
            [self setUserDefaultInfoWithDict:ws.data[0]];
            [self trackingEventByName:@"Sign up" withAction:ClickActionType withCategory:AuthenticationCategoryType];
        }
        
        [AppData setCreatedProfileSuccessful];
        [self addPreferences];
    }
    else if (ws.task == WS_UPDATE_USER) {
        if (ws.data) {
            [self setUserDefaultInfoWithDict:ws.data[0]];
        }
        
        [self stopActivityIndicator];
        [self updateSuccessRedirect];
        
    }else if (ws.task == WS_GET_MY_PREFERENCE){
        [self stopActivityIndicator];
       
        [self updateSuccessRedirect];
       
    }else if(ws.task == WS_ADD_MY_PREFERENCE || ws.task == WS_UPDATE_MY_PREFERENCE){

        if (![self isKindOfClass:[MyProfileLXViewController class]]) {
            [self getPreference];
        }else{
            [self stopActivityIndicator];
            wsPreferences = [[WSPreferences alloc] init];
            [wsPreferences getPreference];
            [self createTabBarController];
        }
    }
}
- (void)showErrorNetwork{
    [self isChangeNavigationBackgroundColor:YES];
    [self showActiveAlertWithTitle:NSLocalizedString(@"Unable to get data", nil) withMessage:NSLocalizedString(@"Turn on cellular data or use Wi-Fi to access Luxury Card.", nil) withFistBtnTitle:NSLocalizedString(@"Settings", nil) forFirstAction:^{
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"prefs:root"]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root"]];
            [self isChangeNavigationBackgroundColor:NO];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root"]];
        }
    } withSecondBtnTitle:NSLocalizedString(@"OK", nil) forSeconAction:^{
        [self isChangeNavigationBackgroundColor:NO];
        [self stopActivityIndicator];
    }];
}
-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:[pssWrd_EncryptedKey AES256DecryptWithKey:EncryptKey]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self stopActivityIndicator];
    [self showCommonAlertForFaildLoodingData];
}

-(void)loadDataFailFrom:(WSBase *)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message
{
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }else{
        [self isChangeNavigationBackgroundColor:YES];
        if ([message isEqualToString:@"ENR68-2"]) {
            message = NSLocalizedString(@"An account with this email address already exists. Click OK to Sign In.", nil);
            [self showSpecialMessageForProfileWithMessage:message];
        }else if ([message isEqualToString:@"ENR61-1"]) {
            message =  NSLocalizedString(@"Please confirm that the value entered is correct.", nil);
            [self showSpecialMessageForProfileWithMessage:message];
        }
        else{
            [self showCommonAlertForFaildLoodingData];
        }
    }
}

-(void) showSpecialMessageForProfileWithMessage:(NSString *)message
{
    [self showActiveAlertWithTitle:NSLocalizedString(@"ERROR!", nil) withMessage:message withFistBtnTitle:NSLocalizedString(@"OK", nil) forFirstAction:^{
        if([message isEqualToString:NSLocalizedString(@"An account with this email address already exists. Click OK to Sign In.", nil)])
        {
            [self.emailText becomeFirstResponder];
            [self.emailText setBottomBorderRedColor];
            
        }
        [self isChangeNavigationBackgroundColor:NO];
    } withSecondBtnTitle:@"" forSeconAction:nil];
}


#pragma mark KEYBOARD PROCESS
-(void)handleKeyboardWillShow:(NSNotification *)paramNotification
{
    [self.salutationDropDown didTapBackground];
    [self.countryCodeDropDown didTapBackground];
    
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         keyboardHeight = keyboardRect.size.height;
                         [self updateEdgeInsetForShowKeyboard];
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

-(void)handleKeyboardWillHide:(NSNotification *)paramNotification
{
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
//    isAnimatingHideKeyboard = NO;
//    if(isShouldHideKeyboardWhenSwipe)
//        [UIView setAnimationsEnabled:NO];
    
    [self updateEdgeInsetForHideKeyboard];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:0
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL isFinished){
//                         isAnimatingHideKeyboard = !isFinished;
                     }];
    
    
}


-(void)updateEdgeInsetForShowKeyboard
{
    _btnSC.constant =  keyboardHeight + (110 * SCREEN_SCALE);
    
}

-(void)updateEdgeInsetForHideKeyboard
{
    _btnSC.constant = 110 * SCREEN_SCALE;
//    self.scrollView.contentInset = UIEdgeInsetsMake(0.0f,  0.0f, 0.0f, 0.0f);
    [self resignFirstResponderForAllTextField];
//    self.scrollBottom.constant = 0.0f;
//    self.viewActionBottomConstraint.constant = backupBottomConstraint;
//    [self.view layoutIfNeeded];
}

- (void)hidenKeyboardWhenSwipe{
//    [self.view.layer removeAllAnimations];
    [self updateEdgeInsetForHideKeyboard];
    [UIView setAnimationsEnabled:YES];
}

-(void)dismissKeyboard
{
    [self updateEdgeInsetForHideKeyboard];
    [self.salutationDropDown didTapBackground];
    [self.countryCodeDropDown didTapBackground];
}

#pragma mark LOCAL PROCESS
- (void)addPreferences{
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    NSMutableArray *arraySubDictValues = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *hotelDict = [[NSMutableDictionary alloc] init];
    [hotelDict setValue:@"Hotel" forKey:@"Type"];
    
    PreferenceObject *hotelPref = [self getPreferenceByType:HotelPreferenceType];
    NSString *value = ([[SessionData shareSessiondata] isUseLocation])?@"YES":@"NO";
    if ([SessionData shareSessiondata].arrayPreferences.count > 0 && hotelPref.preferenceID.length > 0){
        [hotelDict setValue:hotelPref.preferenceID forKey:@"MyPreferencesId"];
        [hotelDict setValue:hotelPref.value forKey:@"Preferredstarrating"];
        [hotelDict setValue:value forKey:@"SmokingPreference"];
        [arraySubDictValues addObject:hotelDict];
        [dataDict setValue:arraySubDictValues forKey:@"PreferenceDetails"];
        
        wsPreferences = [[WSPreferences alloc] init];
        wsPreferences.delegate = self;
        [wsPreferences updatePreference:dataDict];
        
    }else{
        [hotelDict setValue:@"" forKey:@"Preferredstarrating"];
        [hotelDict setValue:value forKey:@"SmokingPreference"];
        [arraySubDictValues addObject:hotelDict];
        
        //        //Begin: support for android.
        //        NSMutableDictionary *diningDict = [[NSMutableDictionary alloc] init];
        //        [diningDict setValue:@"Dining" forKey:@"Type"];
        //        [diningDict setValue:@"" forKey:@"CuisinePreferences"];
        //        [arraySubDictValues addObject:diningDict];
        //
        //        NSMutableDictionary *transportationDict = [[NSMutableDictionary alloc] init];
        //        [transportationDict setValue:@"Car Rental" forKey:@"Type"];
        //        [transportationDict setValue:@"" forKey:@"PreferredRentalVehicle"];
        //        [arraySubDictValues addObject:transportationDict];
        //        //End: support for android.
        
        [dataDict setValue:arraySubDictValues forKey:@"PreferenceDetails"];
        wsPreferences = [[WSPreferences alloc] init];
        wsPreferences.delegate = self;
        [wsPreferences addPreference:dataDict];
    }
    
}
-(PreferenceObject*)getPreferenceByType:(PreferenceType)type{
    if ([SessionData shareSessiondata].arrayPreferences.count > 0) {
        for (PreferenceObject *preference in [SessionData shareSessiondata].arrayPreferences) {
            if (preference.type == type) {
                return preference;
            }
        }
    }
    return nil;
}

- (void)getPreference{
    
    [self startActivityIndicator];
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    [dataDict setValue:B2C_ConsumerKey forKey:@"ConsumerKey"];
    [dataDict setValue:@"GetPreference" forKey:@"Functionality"];
    [dataDict setValue:[[SessionData shareSessiondata] OnlineMemberID] forKey:@"OnlineMemberId"];
    
    wsPreferences = [[WSPreferences alloc] init];
    wsPreferences.delegate = self;
    [wsPreferences getPreference];
    
}
-(void)verifyAccountData:(BOOL)isUpdate
{
    NSMutableString *message = [[NSMutableString alloc] init];
    if (isUpdate) {
        if(self.firstNameText.text.length == 0 || self.lastNameText.text.length == 0 || self.emailText.text.length == 0 || self.phoneNumberText.text.length == 0 || self.zipCodeText.text.length == 0)
        {
            [message appendString:@"* "];
            [message appendString:@"All fields are required."];
            if (self.salutationDropDown.title.length > 10) {
                [self verifyValueForDropdown:self.salutationDropDown andMessageError:message];
            }
            [self verifyValueForTextField:self.firstNameText andMessageError:message];
            [self verifyValueForTextField:self.lastNameText andMessageError:message];
            [self verifyValueForTextField:self.emailText andMessageError:message];
            if (self.countryCodeDropDown.title.length > 11) {
                [self verifyValueForDropdown:self.countryCodeDropDown andMessageError:message];
            }
            [self verifyValueForTextField:self.phoneNumberText andMessageError:message];
            [self verifyValueForTextField:self.zipCodeText andMessageError:message];
            
            [self createProfileInforWithMessage:message isCreatedSuccessful:NO];
        }
        else{
            if (self.salutationDropDown.title.length > 10) {
                [self verifyValueForDropdown:self.salutationDropDown andMessageError:message];
            }
            [self verifyValueForTextField:self.firstNameText andMessageError:message];
            [self verifyValueForTextField:self.lastNameText andMessageError:message];
            [self verifyValueForTextField:self.emailText andMessageError:message];
            if (self.countryCodeDropDown.title.length > 11) {
                [self verifyValueForDropdown:self.countryCodeDropDown andMessageError:message];
            }
            [self verifyValueForTextField:self.phoneNumberText andMessageError:message];
            [self verifyValueForTextField:self.zipCodeText andMessageError:message];
            
            [self createProfileInforWithMessage:message isCreatedSuccessful:(message.length == 0)];
        }
    }else{
        if(self.firstNameText.text.length == 0 || self.lastNameText.text.length == 0 || self.emailText.text.length == 0 || self.phoneNumberText.text.length == 0 || self.zipCodeText.text.length == 0 || self.numberCardText.text.length == 0 || self.passwordText.text.length == 0 /*|| self.confirmPasswordText.text.length == 0*/)
        {
            [message appendString:@"* "];
            [message appendString:@"All fields are required."];
            
            [self verifyValueForDropdown:self.salutationDropDown andMessageError:message];
            
            [self verifyValueForTextField:self.firstNameText andMessageError:message];
            [self verifyValueForTextField:self.lastNameText andMessageError:message];
            [self verifyValueForTextField:self.emailText andMessageError:message];
            
            [self verifyValueForDropdown:self.countryCodeDropDown andMessageError:message];
            
            [self verifyValueForTextField:self.phoneNumberText andMessageError:message];
            [self verifyValueForTextField:self.zipCodeText andMessageError:message];
            [self verifyValueForTextField:self.numberCardText andMessageError:message];
            [self verifyValueForTextField:self.passwordText andMessageError:message];
//            [self verifyValueForTextField:self.confirmPasswordText andMessageError:message];
            
            [self createProfileInforWithMessage:message isCreatedSuccessful:NO];
        }
        else{
            
            [self verifyValueForDropdown:self.salutationDropDown andMessageError:message];

            [self verifyValueForTextField:self.firstNameText andMessageError:message];
            [self verifyValueForTextField:self.lastNameText andMessageError:message];
            [self verifyValueForTextField:self.emailText andMessageError:message];
            
            [self verifyValueForDropdown:self.countryCodeDropDown andMessageError:message];
            
            [self verifyValueForTextField:self.phoneNumberText andMessageError:message];
            [self verifyValueForTextField:self.zipCodeText andMessageError:message];
            [self verifyValueForTextField:self.numberCardText andMessageError:message];
            [self verifyValueForTextField:self.passwordText andMessageError:message];
//            [self verifyValueForTextField:self.confirmPasswordText andMessageError:message];
            
            [self createProfileInforWithMessage:message isCreatedSuccessful:(message.length == 0)];
        }
    }
}

-(void)createProfileInforWithMessage:(NSString *)message isCreatedSuccessful:(BOOL)isSuccessful
{
    if(isSuccessful)
    {
        [self updateEdgeInsetForHideKeyboard];
        [self setTextViewsDefaultBottomBolder];
        
//      save profile is create
        if(_isUpdateProfile)
        {
            [self showActiveAlertWithTitle:@"" withMessage:NSLocalizedString(@"Are you sure you want to save these changes?", nil) withFistBtnTitle:NSLocalizedString(@"Yes", nil) forFirstAction:^{
                [self updateProfileWithAPI];
            } withSecondBtnTitle:@"Cancel" forSeconAction:nil];
        }
        else{
            [self updateProfileWithAPI];
        }
    }
    else
    {
        NSMutableString *newMessage = [[NSMutableString alloc] init];
        [newMessage appendString:@"\n"];
        [newMessage appendString:message];
        [newMessage appendString:@"\n"];
        
         [self isChangeNavigationBackgroundColor:YES];
        [self showActiveAlertWithTitle:NSLocalizedString(@"Please confirm that the value entered is correct:", nil) withMessage:newMessage withFistBtnTitle:NSLocalizedString(@"OK", nil) forFirstAction:^{
            [self makeBecomeFirstResponderForTextField];
             [self isChangeNavigationBackgroundColor:NO];
        } withSecondBtnTitle:@"" forSeconAction:nil];
         
    }
}


- (void) updateSuccessRedirect{
    /*
    appdelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    [UIView transitionWithView:appdelegate.window
                      duration:0.5
                       options:UIViewAnimationOptionPreferredFramesPerSecond60
                    animations:^{
                        //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        appdelegate.tabBarController = [[UITabBarController alloc] init];
                        UIViewController *homeLXViewController = [[HomeLXViewController alloc] init];
                        appdelegate.tabBarController.viewControllers = @[homeLXViewController];
                        //appdelegate.viewController = revealController;
                        appdelegate.window.rootViewController = appdelegate.tabBarController;
                        [appdelegate.window makeKeyWindow];
                        [self presentViewController:appdelegate.tabBarController animated:NO completion:nil];
                    }
                    completion:nil];
     */
     [self isChangeNavigationBackgroundColor:YES];
    [self showActiveAlertWithTitle:@"" withMessage:NSLocalizedString(@"All set! Your profile has been created!", nil) withFistBtnTitle:NSLocalizedString(@"OK", nil) forFirstAction:^{
        [self isChangeNavigationBackgroundColor:NO];
        [self createTabBarController];
    } withSecondBtnTitle:@"" forSeconAction:nil];
    
}

#pragma mark TEXTFIELD
-(void)resignFirstResponderForAllTextField
{
    if(self.firstNameText.isFirstResponder)
    {
        [self.firstNameText resignFirstResponder];
    }
    else if(self.lastNameText.isFirstResponder)
    {
        [self.lastNameText resignFirstResponder];
    }
    else if(self.emailText.isFirstResponder)
    {
        [self.emailText resignFirstResponder];
    }
    else if(self.phoneNumberText.isFirstResponder)
    {
        [self.phoneNumberText resignFirstResponder];
    }
    else if(self.passwordText.isFirstResponder)
    {
        [self.passwordText resignFirstResponder];
    }
    /*
    else if(self.confirmPasswordText.isFirstResponder)
    {
        [self.confirmPasswordText resignFirstResponder];
    }
     */
    else if (self.zipCodeText.isFirstResponder)
    {
        [self.zipCodeText resignFirstResponder];
    }
    else if (self.numberCardText.isFirstResponder)
    {
        [self.numberCardText resignFirstResponder];
    }
}
-(void) makeBecomeFirstResponderForTextField
{
    [self resignFirstResponderForAllTextField];
    if(![currentFirstName isValidName])
    {
        [self.firstNameText becomeFirstResponder];
    }
    else if(![currentLastName isValidName])
    {
        [self.lastNameText becomeFirstResponder];
    }
    else if(![currentEmail isValidEmail])
    {
        [self.emailText becomeFirstResponder];
    }
    else if(![currentPhone isValidPhoneNumber])
    {
        [self.phoneNumberText becomeFirstResponder];
    }
    else if (![self.zipCodeText.text isValidZipCode])
    {
        [self.zipCodeText becomeFirstResponder];
    }
    
    else if (![self.numberCardText.text isValidBinNumber])
    {
        [self.numberCardText becomeFirstResponder];
    }
    else if(![self isKindOfClass:[MyProfileLXViewController class]] &&  ![self.passwordText.text isValidPassword])
    {
        [self.passwordText becomeFirstResponder];
    }
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.text = [textField.text removeRedudantWhiteSpaceInText];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == self.firstNameText)
    {
        currentFirstName = textField.text;
        textField.text = [textField.text createMaskForText:NO];

    }
    else if(textField == self.lastNameText)
    {
        currentLastName = textField.text;
        textField.text =  [textField.text createMaskForText:NO];

    }
    else if(textField == self.phoneNumberText)
    {
        currentPhone = textField.text;
        textField.text =  [textField.text createMaskForText:NO];
        
//        if (textField.text.length > 3) {
//            textField.text = (textField.text.length > 3) ? [textField.text createMaskStringBeforeNumberCharacter:3]: textField.text;
//        }

    }
    else if(textField == self.emailText)
    {
        currentEmail = textField.text;
        if([textField.text containsString:@"@"])
        {
            textField.text = [textField.text createMaskForText:YES];
        }
    }
    else if (textField == self.zipCodeText)
    {
        currentZipCode = textField.text;
        //textField.text = [textField.text createMaskForText:NO];
    }
    else if (textField == self.numberCardText)
    {
        currentCardNumber = textField.text;
        //textField.text = [textField.text createMaskForText:NO];
    }
    else if(textField == self.passwordText)
    {
        currentPassword = textField.text;
    }
    /*
    else if(textField == self.confirmPasswordText)
    {
        currentConfirmPassword = textField.text;
    }
     */
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == self.firstNameText)
    {
        textField.text = currentFirstName;
    }
    
    else if(textField == self.lastNameText)
    {
        textField.text = currentLastName;
    }
    
    else if(textField == self.phoneNumberText)
    {
        textField.text = currentPhone;
        
//        if(textField.text.length > 0)
//        {
//            if(![[textField.text substringWithRange:NSMakeRange(0, 2)] isEqualToString:@"1-"])
//            {
//                NSMutableString *insertCountryCode = [[NSMutableString alloc] initWithString:textField.text];
//                [insertCountryCode insertString:@"1-" atIndex:0];
//                textField.text = insertCountryCode;
//            }
//        }
//        else
//        {
//            textField.text = @"1-";
//        }
    }
    else if(textField == self.emailText)
    {
        textField.text = currentEmail;
    }
    else if(textField == self.zipCodeText)
    {
        textField.text = currentZipCode;
    }
    else if(textField == self.numberCardText)
    {
        textField.text = currentCardNumber;
    }
    else if(textField == self.passwordText)
    {
        textField.text = currentPassword;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([self isKindOfClass:[MyProfileLXViewController class]]) {
        if (textField == self.emailText) {
            return NO;
        }
    }
    if (textField == self.passwordText /*|| textField == self.confirmPasswordText*/) {
        if ([string isEqualToString:@" "]) {
            return NO;
        }
    }
    
    NSString *inputString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (textField == self.firstNameText || textField == self.lastNameText) {
        if (inputString.length <= 19) {
            [self checkTextFieldIsChange:textField shouldChangeCharactersInRange:range replacementString:string];
        }
    }else if (textField == self.zipCodeText) {
        if (inputString.length <= 10) {
            [self checkTextFieldIsChange:textField shouldChangeCharactersInRange:range replacementString:string];
        }
    }else{
        [self checkTextFieldIsChange:textField shouldChangeCharactersInRange:range replacementString:string];
    }
    
    return [self updateTextFiel:textField shouldChangeCharactersInRange:range replacementString:string];
    
}

-(void)checkTextFieldIsChange:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
}

-(BOOL)updateTextFiel:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(string.length > 1)
    {
        NSString *stringContent = (textField == self.emailText) ? string : [string removeRedudantWhiteSpaceInText];
        NSMutableString *newString = [[NSMutableString alloc] initWithString:stringContent];
        
        NSString *resultText = [textField.text stringByReplacingCharactersInRange:range
                                                                       withString:string];
        resultText = [resultText removeRedudantWhiteSpaceInText];
        
        if(textField == self.firstNameText || textField == self.lastNameText)
        {
//            if([newString isValidName])
//            {
//                textField.text = newString;
//            }
//            else if(string.length > 19)
//            {
//                textField.text = [newString substringToIndex:19];
//            }
//            else
//            {
//                textField.text = newString;
//            }
            textField.text = (resultText.length > 19) ? [resultText substringWithRange:NSMakeRange(0, 19)] : resultText;
        }
        if(textField == self.passwordText /*|| textField == self.confirmPasswordText*/)
        {
            if([newString isValidPassword])
            {
                textField.text = newString;
            }
            else if(string.length > 25)
            {
                textField.text = [newString substringToIndex:25];
            }
            else
            {
                textField.text = newString;
            }
        }
        if(textField == self.phoneNumberText)
        {
            if([newString isValidPhoneNumber])
            {
                textField.text = newString;
            }
            /*
            else{
                if(newString.length <= 10)
                {
                    [newString insertString:@"1-" atIndex:0];
                }
                else{
                    if(![[newString substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"-"])
                    {
                        [newString insertString:@"-" atIndex:1];
                    }
                }
                
                if(newString.length > 5 && ![[newString substringWithRange:NSMakeRange(5, 1)] isEqualToString:@"-"])
                {
                    [newString insertString:@"-" atIndex:5];
                }
                
                if(newString.length > 9 && ![[newString substringWithRange:NSMakeRange(9, 1)] isEqualToString:@"-"])
                {
                    [newString insertString:@"-" atIndex:9];
                }
                
                textField.text = (newString.length > 14) ? [newString substringWithRange:NSMakeRange(0, 14)] : newString;
            }
            */
        }
        
        if(textField == self.emailText)
        {
            textField.text = (resultText.length > 100)?[resultText substringWithRange:NSMakeRange(0, 100)] : resultText;
        }
        if(textField == self.zipCodeText)
        {
            textField.text = (resultText.length > 10)?[resultText substringWithRange:NSMakeRange(0, 10)] : resultText;
        }
        
        return NO;
    }
    
    if(textField == self.firstNameText || textField == self.lastNameText)
    {
        if(textField.text.length == 19 && ![string isEqualToString:@""])
        {
            return NO;
        }
        return YES;
    }
    if(textField == self.passwordText /*|| textField == self.confirmPasswordText*/)
    {
        if(textField.text.length == 25 && ![string isEqualToString:@""])
        {
            return NO;
        }
        return YES;
    }
    if (textField == self.numberCardText) {
        if (textField.text.length > 5 && ![string isEqualToString:@""]) {
            return NO;
        }
        return YES;
    }
    if (textField == self.zipCodeText) {
        if (textField.text.length > 9 && ![string isEqualToString:@""]) {
            return NO;
        }
        return YES;
    }
    else if(textField == self.phoneNumberText)
    {
        if (textField.text.length > 20 && ![string isEqualToString:@""]) {
            return NO;
        }
        /*
        if(textField.text.length == 14 && ![string isEqualToString:@""])
        {
            return NO;
        }
        
        if(range.location < 2 && [string isEqualToString:@""])
        {
            return NO;
        }
        
        if(range.location < textField.text.length)
        {
            textField.text = [self reformatForText:textField WithNewString:string WithRange:range];
            NSRange newRange;
            if([string isEqualToString:@""])
            {
                newRange = (textField.text.length < range.location) ? NSMakeRange(textField.text.length, 0) : NSMakeRange(range.location, 0);
            }
            else
            {
                newRange = (range.location == 5 || range.location == 9) ? NSMakeRange(range.location + 2, 0) : NSMakeRange(range.location + 1, 0);
                
            }
            [textField updateCursorPositionAtRange:newRange];
            return NO;
        }
        
        if(textField.text.length == 5 && ![string isEqualToString:@""])
        {
            textField.text = [NSString stringWithFormat:@"%@-%@",textField.text,string];
            return NO;
        }
        
        if(textField.text.length == 7 && [string isEqualToString:@""])
        {
            textField.text = [textField.text substringToIndex:5];
            return NO;
        }
        
        if(textField.text.length == 9 && ![string isEqualToString:@""])
        {
            textField.text = [NSString stringWithFormat:@"%@-%@",textField.text,string];
            return NO;
        }
        
        if(textField.text.length == 11 && [string isEqualToString:@""])
        {
            textField.text = [textField.text substringToIndex:9];
            return NO;
        }
         */
        
        return YES;
    }
    else if(textField == self.emailText)
    {
        if(textField.text.length == 100 && ![string isEqualToString:@""])
        {
            return NO;
        }
        
        if([textField.text occurrenceCountOfCharacter:'@'] == 1 && [string isEqualToString:@"@"]){
            return NO;
        }
        
        return YES;
    }
    
    return YES;
}

#pragma mark LOGICAL FUNCTION
-(BOOL)verifyValueForTextField:(UITextField *)textFied andMessageError:(NSMutableString *)message
{
    NSString *currentText = nil;
    NSString *errorMsg = nil;
    BOOL isValid = NO;
    if(textFied == self.firstNameText)
    {
        currentText = (textFied.isFirstResponder ? [textFied.text removeRedudantWhiteSpaceInText] :currentFirstName);
        
        NSMutableString *tempError = [[NSMutableString alloc] init];
        if (currentText.length < 2) {
            isValid = NO;
            if (tempError.length > 0) {
                [tempError appendString:@"\n* "];
            }
            [tempError appendString:NSLocalizedString(@"input_invalid_first_name_msg", nil)];
        }else if (![(textFied.isFirstResponder ? textFied.text :currentText) isValidName]) {
            isValid = NO;
            if (tempError.length > 0) {
                [tempError appendString:@"\n* "];
            }
            [tempError appendString:NSLocalizedString(@"Please enter a valid first name. Acceptable special characters are - and space.", nil)];
        }else{
            isValid = YES;
        }
        
        errorMsg = tempError;
    }
    else if(textFied == self.lastNameText)
    {
        currentText = (textFied.isFirstResponder ? [textFied.text removeRedudantWhiteSpaceInText] :currentLastName);
        
        NSMutableString *tempError = [[NSMutableString alloc] init];
        if (currentText.length < 2) {
            isValid = NO;
            if (tempError.length > 0) {
                [tempError appendString:@"\n* "];
            }
            [tempError appendString:NSLocalizedString(@"input_invalid_last_name_msg", nil)];
        }else if (![(textFied.isFirstResponder ? textFied.text :currentText) isValidName]) {
            isValid = NO;
            if (tempError.length > 0) {
                [tempError appendString:@"\n* "];
            }
            [tempError appendString:NSLocalizedString(@"Please enter a valid last name. Acceptable special characters are - and space.", nil)];
        }else{
            isValid = YES;
        }
        
        errorMsg = tempError;
    }
    else if(textFied == self.emailText)
    {
        currentText = currentEmail;
        errorMsg = NSLocalizedString(@"input_invalid_email_msg", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidEmail];
    }
    else if(textFied == self.phoneNumberText)
    {
        currentText = currentPhone;
        errorMsg = NSLocalizedString(@"input_invalid_phone_number_msg", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidPhoneNumber];
    }
    else if (textFied == self.zipCodeText)
    {
        currentText = currentZipCode;
        errorMsg = @"Enter a valid zip code.";
        isValid = [(textFied.isFirstResponder ? [textFied.text removeRedudantWhiteSpaceInText] : currentText) isValidZipCode];
    }
    else if (textFied == self.numberCardText)
    {
        currentText = currentCardNumber;
        errorMsg = NSLocalizedString(@"Enter a valid first six digits of your credit card number.", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text : currentText) isValidBinNumber];
    }
    else if(textFied == self.passwordText)
    {
        currentText = currentPassword;
        errorMsg = NSLocalizedString(@"Password must be between 10 and 25 characters, including at least 1 uppercase, 1 lowercase, 1 special character and 1 number.", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidPassword];
    }
    /*
    else if(textFied == self.confirmPasswordText)
    {
        currentText = currentConfirmPassword;
        errorMsg = NSLocalizedString(@"input_invalid_confirm_password_msg", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isEqualToString:currentPassword];
    }
    */
    if((textFied.isFirstResponder ? textFied.text :currentText).length > 0)
    {
        if(!isValid)
        {
            (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
            [message appendString:errorMsg];
            [textFied setBottomBorderRedColor];
        }
        else
        {
            [textFied setBottomBolderDefaultColor];
        }
    }
    else
    {
        [textFied setBottomBorderRedColor];
    }
    
    return isValid;
}
- (BOOL)verifyValueForDropdown:(DropDownView*)dropdown andMessageError:(NSMutableString *)message{
    
    BOOL isValid = NO;
    
    if (dropdown == self.salutationDropDown) {
        if (self.salutationDropDown.valueStatus == DefaultValueStatus) {
            (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
            [message appendString:@"Please choose your salutation."];
            isValid = NO;
            [self.salutationDropDown setBottomBorderRedColor];
        }else {
            isValid = YES;
            [self.salutationDropDown setBottomBolderDefaultColor];
        }
        /*
         if ((self.firstNameText.text.length > 0 && self.lastNameText.text.length > 0 && self.emailText.text.length > 0 && self.phoneNumberText.text.length > 0 && self.zipCodeText.text.length > 0 && self.numberCardText.text.length > 0 && self.passwordText.text.length > 0 && self.confirmPasswordText.text.length > 0)) {
         if (self.salutationDropDown.valueStatus == DefaultValueStatus) {
         (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
         [message appendString:@"Select a salutation."]; //All fields are required.
         isValid = NO;
         [self.salutationDropDown setBottomBorderRedColor];
         }
         }else{
         if (self.salutationDropDown.valueStatus == DefaultValueStatus) {
         isValid = NO;
         [self.salutationDropDown setBottomBorderRedColor];
         }else{
         isValid = YES;
         [self.salutationDropDown setBottomBolderDefaultColor];
         }
         }
         */
    }
    else if (dropdown == self.countryCodeDropDown) {
        if (self.countryCodeDropDown.valueStatus == DefaultValueStatus) {
            (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
            [message appendString:@"Select a country code."];
            isValid = NO;
            [self.countryCodeDropDown setBottomBorderRedColor];
        }else {
            isValid = YES;
            [self.countryCodeDropDown setBottomBolderDefaultColor];
        }
        /*
         if ((self.firstNameText.text.length > 0 && self.lastNameText.text.length > 0 && self.emailText.text.length > 0 && self.phoneNumberText.text.length > 0 && self.zipCodeText.text.length > 0 && self.numberCardText.text.length > 0 && self.passwordText.text.length > 0 && self.confirmPasswordText.text.length > 0)) {
         if (self.countryCodeDropDown.valueStatus == DefaultValueStatus) {
         (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
         [message appendString:@"Select a country code."];
         isValid = NO;
         [self.countryCodeDropDown setBottomBorderRedColor];
         }
         }else{
         if (self.countryCodeDropDown.valueStatus == DefaultValueStatus) {
         isValid = NO;
         [self.countryCodeDropDown setBottomBorderRedColor];
         }else{
         isValid = YES;
         [self.countryCodeDropDown setBottomBolderDefaultColor];
         }
         }
         */
    }
    return isValid;
}

- (void) setCheckBoxState:(BOOL)check
{
    if (!check) {
        check = NO;
    }
    (check == YES) ? [self.checkBoxButton setImage:[UIImage imageNamed:@"tick_icon"] forState:UIControlStateNormal] : [self.checkBoxButton setImage:[UIImage imageNamed:@"untick_icon"] forState:UIControlStateNormal];
}
- (void)setImageLocation:(BOOL)isLocation {
    self.locationStatusImage.image = [UIImage imageNamed:(isLocation) ? @"location_on" : @"location_off"];
}


- (void)setRequestLocation:(BOOL)isRequest{
    [[NSUserDefaults standardUserDefaults] setBool:isRequest forKey:@"IsRequestLocation"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) changeUseLocation {
}
-(NSString *)reformatForText:(UITextField *)textField WithNewString:(NSString *)newString WithRange:(NSRange)range
{
    NSMutableString *mutableString = [[NSMutableString alloc] initWithString:textField.text];
    NSRange newRange = range;
    if([newString isEqualToString:@""] && (range.location == 5 ||  range.location == 9))
    {
        newRange = NSMakeRange(range.location - 1, 1);
    }
    [mutableString replaceCharactersInRange:newRange withString:newString];
    mutableString = [[NSMutableString alloc] initWithString:[mutableString stringByReplacingOccurrencesOfString:@"-" withString:@""]];
    
    if(mutableString.length >= 1 && textField.tag == PHONE_NUMBER_TEXTFIELD_TAG)
    {
        [mutableString insertString:@"-" atIndex:1];
    }
    
    if(mutableString.length > 5)
    {
        [mutableString insertString:@"-" atIndex:5];
    }
    
    if(mutableString.length > 9 && textField.tag == PHONE_NUMBER_TEXTFIELD_TAG)
    {
        [mutableString insertString:@"-" atIndex:9];
    }
    
    return mutableString;
}
#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{

    CGPoint touchPoint = [touch locationInView:touch.view];
    
    UIView *viewTouch = touch.view.superview;
    if ([viewTouch isKindOfClass:[UITableViewCell class]]) {
        return NO;
    }else {
        if ([self isKindOfClass:[MyProfileLXViewController class]]) {
            for (UIView* view in touch.view.subviews) {
                if ((UITextField*)view == self.emailText && CGRectContainsPoint(self.emailText.frame, touchPoint)) {
                    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo]; 
                    if (profileDictionary) {
                        self.emailText.text = [profileDictionary objectForKey:keyEmail];
                        break;
                    }
                }else{
                    if([self.emailText.text containsString:@"@"])
                    {
                        self.emailText.text = [self.emailText.text createMaskForText:YES];
                    }
                }
            }
        }
        
    }
    
    return YES;
    
}

#pragma mark - DropDownViewDelegate
- (void)didSelectItem:(DropDownView *)dropDownView atIndex:(int)index{
    if (dropDownView == self.countryCodeDropDown) {
        Country *country = [countries objectAtIndex:index];
        self.countryCodeDropDown.title = [NSString stringWithFormat:@"%@ (%@)",country.countryCode, country.countryNumber];
        self.countryCodeDropDown.titleOther = country.countryDescription;
    }
    [self changeValueDropDown];
}
- (void)didShow:(DropDownView *)dropDownView {
    [self.view endEditing:YES];
    if (dropDownView == self.salutationDropDown) {
        [self.countryCodeDropDown didTapBackground];
    }else if (dropDownView == self.countryCodeDropDown) {
        [self.salutationDropDown didTapBackground];
    }
}

- (void)changeValueDropDown{
    
}
@end
