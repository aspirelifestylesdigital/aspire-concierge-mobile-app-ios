//
//  IntroductionViewController.m
//  LuxuryCard
//
//  Created by Viet Vo on 11/10/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "IntroductionViewController.h"
#import "UDASignInViewController.h"
#import "CreateProfileViewController.h"
#import "WSB2CGetTiles.h"
#import "TileItem.h"
#import "UIImageView+AFNetworking.h"
#import "NSString+Utis.h"
#import "UIButton+Extension.h"
#import "Constant.h"
#import "UIImageEffects.h"

@interface IntroductionViewController ()<DataLoadDelegate>

@property (nonatomic, strong) NSArray *galleryList;

@end

@implementation IntroductionViewController{
    NSInteger currentIndex;
}

@synthesize galleryView;
@synthesize galleryList;

#pragma mark - LIFECYCLE
- (void)viewDidLoad {
    isNotChangeNavigationBarColor = YES;
    [super viewDidLoad];
    [self setupView];
    
}

-(void) viewWillAppear:(BOOL)animated
{
    isHiddenNavigationBar = YES;
    [super viewWillAppear:animated];
    [self trackingScreenByName:@"Welcome Screens"];
}

-(void)viewDidLayoutSubviews{
    [self.signupButton removeConstraint:self.bottomConstraint];
    if(SYSTEM_VERSION_LESS_THAN(@"11.0"))
    {
        [self.signupButton.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor constant:10.0];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SETUP UI
- (void)setupView {
    self.galleryView.delegate = self;
    self.galleryView.dataSource = self;
    self.galleryView.type = iCarouselTypeRotary;
//    self.galleryView.clipsToBounds = YES;
//    self.galleryView.layer.cornerRadius = 10.0f;
    
    
    [self.pageControl setPageIndicatorTintColor:[UIColor clearColor]];
    [self.pageControl setCurrentPageIndicatorTintColor:[UIColor clearColor]];
    [self.pageControl addTarget:self action:@selector(OnTouchPageControl) forControlEvents:UIControlEventTouchUpInside];
    currentIndex = 0;
    self.pageControl.currentPage = currentIndex;
    
    [self.signupButton setDefaultStyle];
    [self.signinButton setDefaultStyle];
    
    self.titleDescLabel.font = [UIFont fontWithName:FONT_MarkForMC_BOLD size:FONT_SIZE_21 * SCREEN_SCALE_BASE_WIDTH_375];
    self.contentDescLabel.font = [UIFont fontWithName:FONT_MarkForMC_LIGHT size:FONT_SIZE_16 * SCREEN_SCALE_BASE_WIDTH_375];
    
    [self.signinButton setTitle:NSLocalizedString(@"Sign In", nil) forState:UIControlStateNormal];
    self.signinButton.titleLabel.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:FONT_SIZE_14 * SCREEN_SCALE_BASE_WIDTH_375];
    [self.signupButton setTitle:NSLocalizedString(@"Sign Up", nil) forState:(UIControlState)UIControlStateNormal];
    self.signupButton.titleLabel.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:FONT_SIZE_14 * SCREEN_SCALE_BASE_WIDTH_375];
}

#pragma mark - SETUP DATA
- (void)initData {
    [self startActivityIndicator];
    WSB2CGetTiles *wsGetTiles = [[WSB2CGetTiles alloc] init];
    wsGetTiles.delegate = self;
    wsGetTiles.categoryCode = @"appgallery";
    wsGetTiles.categories = [[NSArray alloc] initWithObjects:@"Welcome", nil];
    [wsGetTiles loadTilesForCategory];
}

#pragma mark - ACTION PAGE CONTROL
- (void)OnTouchPageControl {
    if (currentIndex < self.pageControl.currentPage || self.pageControl.currentPage == self.galleryList.count - 1) {
        [self.galleryView scrollToItemAtIndex:self.galleryView.currentItemIndex + 1 animated:YES];
    }else {
        [self.galleryView scrollToItemAtIndex:self.galleryView.currentItemIndex - 1 animated:YES];
    }
    currentIndex = self.pageControl.currentPage;
}

#pragma mark - ACTIONS BUTTON
- (IBAction)OnTouchSignIn:(id)sender {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if ([SessionData shareSessiondata].arrayPreferences.count > 0) {
        [[SessionData shareSessiondata].arrayPreferences removeAllObjects];
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UDASignInViewController *signinVC = [storyboard instantiateViewControllerWithIdentifier:@"UDASignInViewController"];
    
    [self.navigationController pushViewController:signinVC animated:YES];
}

- (IBAction)OnTouchSignUp:(id)sender {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if ([SessionData shareSessiondata].arrayPreferences.count > 0) {
        [[SessionData shareSessiondata].arrayPreferences removeAllObjects];
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CreateProfileViewController *createProfileViewController = [storyboard instantiateViewControllerWithIdentifier:@"CreateProfileViewController"];
    
    [self.navigationController pushViewController:createProfileViewController animated:YES];
}

#pragma mark - iCarousel DELEGATE & DATASOURCE
- (NSInteger)numberOfItemsInCarousel:(__unused iCarousel *)carousel {
    return self.galleryList.count;
}
- (UIView *)carousel:(__unused iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    TileItem *item = [self.galleryList objectAtIndex:index];
    if (view == nil) {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, (327 * SCREEN_SCALE), (396.0 * SCREEN_SCALE))];
//        [view setBackgroundColor:[UIColor lightGrayColor]];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, (327 * SCREEN_SCALE), (396.0 * SCREEN_SCALE))];
        imageView.tag = 101;
        if (item.imageURL.length > 0) {
            [imageView setImageWithURL:[NSURL URLWithString:[item.imageURL removeBlankFromURL]] placeholderImage:[UIImage imageNamed:@"gallery_placeholder"]];
        }
        imageView.contentMode = UIViewContentModeScaleToFill;
        [view addSubview:imageView];
        
        /*
        UIView *frontView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, (327 * SCREEN_SCALE), (396.0 * SCREEN_SCALE))];
        frontView.tag = 102;
        [frontView setBackgroundColor:colorFromHexStringWithAlpha(@"#212121", 1.0)];
        if(index == 0)
        {
            [frontView setBackgroundColor:colorFromHexStringWithAlpha(@"#212121", 0.0)];
        }
        [view addSubview:frontView];
         */
    }
    
    self.titleDescLabel.text = [item.title capitalizedString];
    self.contentDescLabel.text = item.tileText;
    
    return view;
}

/*
- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    
    if (option == iCarouselOptionFadeMin)
    {
        return 0.0;
    }
    else if (option == iCarouselOptionFadeMinAlpha)
    {
        return 0.6;
    }
    return value;
}

- (void)carouselDidScroll:(iCarousel *)carousel
{
    
    NSLog(@"carousel.scrollOffset %@", [NSNumber numberWithFloat:carousel.scrollOffset]);
    if(carousel.numberOfItems == 0)
    {
        return;
    }
    
    CGFloat alpha = fabs(carousel.scrollOffset - carousel.currentItemIndex);
    if(alpha > (self.galleryList.count - 1))
    {
        alpha = fabs(self.galleryList.count - carousel.scrollOffset);
    }
        
    NSLog(@"alpha %@", [NSNumber numberWithFloat:alpha]);
    NSInteger nextIndex = (carousel.currentItemIndex + 1) % carousel.numberOfItems;
    UIView *nextView = [carousel itemViewAtIndex:nextIndex];
    UIView *nextFrontView = [nextView viewWithTag:102];
    if (nextFrontView)
    {
        [nextFrontView setBackgroundColor:colorFromHexStringWithAlpha(@"#212121", 1.0 - alpha)];
    }
    
    
    UIView *currentView = [carousel itemViewAtIndex:carousel.currentItemIndex];
    UIView *currentFrontView = [currentView viewWithTag:102];
    if (currentFrontView)
    {
         [currentFrontView setBackgroundColor:colorFromHexStringWithAlpha(@"#212121", alpha)];
    }
    
    NSInteger prevIndex = (carousel.currentItemIndex + carousel.numberOfItems - 1) % carousel.numberOfItems;
    UIView *previousView = [carousel itemViewAtIndex:prevIndex];
    UIView *previousFrontView = [previousView viewWithTag:102];
    if (previousFrontView)
    {
         [previousFrontView setBackgroundColor:colorFromHexStringWithAlpha(@"#212121", 1.0 - alpha)];
    }
}
*/

- (void)carouselCurrentItemIndexDidChange:(__unused iCarousel *)carousel
{
    
    self.pageControl.currentPage = currentIndex = self.galleryView.currentItemIndex;
    TileItem *item = [self.galleryList objectAtIndex:carousel.currentItemIndex];
    if (item) {
        self.titleDescLabel.text = [item.title capitalizedString];
        self.contentDescLabel.text = item.tileText;
    }
}


#pragma mark - API
-(void)loadDataDoneFrom:(WSB2CGetTiles *)ws
{
    [self stopActivityIndicator];
    [self.view layoutIfNeeded];
    if (ws.data.count > 0) {
        self.pageControl.hidden = NO;
        self.galleryList = [ws.data subarrayWithRange:NSMakeRange(0, (ws.data.count > 5) ? 5 : ws.data.count)];
        
        /*
         TileItem *item1 = [[TileItem alloc] init];
         item1.title = @"Item 1";
         item1.tileText = @"Description of Item 1";
         item1.tileImage = ((TileItem *)ws.data[0]).tileImage;
         NSMutableArray *tempArray = [NSMutableArray arrayWithObjects:ws.data[0],item1,ws.data[0], nil];
         self.galleryList = tempArray;
         */
        
        self.pageControl.numberOfPages = [self.galleryList count];
        self.galleryView.scrollEnabled = (self.galleryList.count <= 1) ? NO : YES;
        [self.galleryView reloadData];
    }
    
}


- (void)WSBaseNetworkUnavailable{
    self.pageControl.hidden = YES;
    [self stopActivityIndicator];
}

@end
