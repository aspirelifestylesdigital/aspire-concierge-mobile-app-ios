//
//  IntroductionViewController.h
//  LuxuryCard
//
//  Created by Viet Vo on 11/10/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "BaseViewController.h"

@interface IntroductionViewController : BaseViewController <iCarouselDelegate, iCarouselDataSource>

#pragma mark - HeaderView
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

#pragma mark - GalleryView
@property (weak, nonatomic) IBOutlet iCarousel *galleryView;

#pragma mark - DescriptionView
@property (weak, nonatomic) IBOutlet UIView *descriptionView;
@property (weak, nonatomic) IBOutlet UILabel *titleDescLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentDescLabel;

#pragma mark - BottomView
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *signinButton;
@property (weak, nonatomic) IBOutlet UIButton *signupButton;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;

@end
