//
//  MenuViewController.h
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/12/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

typedef enum : NSUInteger {
    MenuItem_Profile = 0,
    MenuItem_Reward_Benefit = 1,
    MenuItem_AirportLoungeAccess = 2,
    MenuItem_HotelTravel = 3,
    MenuItem_LuxuryMagazine = 4,
    MenuItem_AboutThisApp = 5,
    MenuItem_TOU = 6,
    MenuItem_PrivacyPolicy = 7,
    MenuItem_Signout = 8
} MenuItem;

@protocol ShowingRevealController <NSObject>

-(void)showReavealController;

@end

@interface MenuViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UITableView *menuTableView;
@property (nonatomic,weak) id<ShowingRevealController> delegate;
@property (weak, nonatomic) IBOutlet UIView *viewHeader;
@property (nonatomic, weak) IBOutlet UILabel *currentDate;
@property (nonatomic, weak) IBOutlet UILabel *menuTitle;
@end
