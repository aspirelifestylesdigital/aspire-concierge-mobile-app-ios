//
//  MenuViewController.m
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/12/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "MenuViewController.h"
#import "Constant.h"
#import "UIView+Extension.h"
#import "AppData.h"
#import "ExploreViewController.h"
#import "AboutAppViewController.h"
#import "TermsOfUseViewController.h"
//#import "PrivacyPolicyViewController.h"
#import "PolicyViewController.h"

//
#import "HomeLXViewController.h"
#import "MyProfileLXViewController.h"
#import "AppDelegate.h"
#import "Common.h"
#import "UtilStyle.h"
#import "UDASignInViewController.h"

#import "MyRequestViewController.h"
#import "MyRequestPagerViewController.h"
#import "MenuTableViewCell.h"
#import "MyProfileLXViewController.h"
#import "WebformViewController.h"
#import "IntroductionViewController.h"
#import "MenuProfileViewController.h"

@interface MenuViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    AppDelegate *appDelegate;
}


@property (nonatomic, strong) NSDictionary *menuDataDict;
@property (nonatomic, strong) NSArray *menuSectionTitle;
@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.menuDataDict = [AppData getMenuItems];
    self.menuSectionTitle = [self.menuDataDict allKeys];
    
    self.menuTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.menuTableView setBackgroundColorForView];
    self.menuTableView.alwaysBounceVertical = NO;
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(displayFrontView:)];
    [self.view addGestureRecognizer:panGesture];

    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    isHiddenNavigationBar = YES;
    
    self.menuTitle.attributedText = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Menu", nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_MarkForMC_BLACK size:FONT_SIZE_32 * SCREEN_SCALE_BASE_WIDTH_375], NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    [self.menuTableView registerNib:[UINib nibWithNibName:@"MenuTableViewCell" bundle:nil] forCellReuseIdentifier:@"MenuTableViewCell"];
    
    self.currentDate.attributedText = [[NSAttributedString alloc] initWithString:currentDate() attributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_MarkForMC_MED size:FONT_SIZE_13 * SCREEN_SCALE_BASE_WIDTH_375], NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.currentDate.alpha = 0.5;
    self.viewHeader.backgroundColor = colorFromHexString(GRAY_BACKGROUND_COLOR);
}

-(void)displayFrontView:(UIPanGestureRecognizer*)panGesture
{
    /*
    SWRevealViewController *revealViewController = [self revealViewController];
    [revealViewController _handleRevealGesture:panGesture];
     */
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.menuTableView setUserInteractionEnabled:YES];

    // Refine index menu
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    //    if(currentNavigation.navigationController.viewControllers.count > 0) {
    if([[self.navigationController.viewControllers lastObject] isKindOfClass:[HomeLXViewController class]]) {
        indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    } else if([[self.navigationController.viewControllers lastObject] isKindOfClass:[ExploreViewController class]]) {
        indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
    } else if([[self.navigationController.viewControllers lastObject] isKindOfClass:[CreateProfileViewController class]]) {
        indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    } else if([[self.navigationController.viewControllers lastObject] isKindOfClass:[MyProfileLXViewController class]]) {
        indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    }
    //    }
    [self.menuTableView reloadData];
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma marl - UITableView Data Source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 60.0f*SCREEN_SCALE;
    }
    return 50.0f * SCREEN_SCALE;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *menuSection = [self.menuDataDict allKeys];
    NSString *menuTitle = [menuSection objectAtIndex:section];
    NSArray *allValuesInSection = [self.menuDataDict objectForKey:menuTitle];
    return allValuesInSection.count;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.menuDataDict allKeys].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MenuTableViewCell"];

    NSString *sectionTitle = [self.menuSectionTitle objectAtIndex:indexPath.section];
    NSArray *valuesInSection = [self.menuDataDict objectForKey:sectionTitle];
    cell.menuItemName.text = [valuesInSection objectAtIndex:indexPath.row];
    cell.menuItemName.textColor = [UIColor whiteColor];
    
    CGFloat textSize = FONT_SIZE_26 * SCREEN_SCALE_BASE_WIDTH_375;
    NSString *fontName = FONT_MarkForMC_BLACK;
    if(indexPath.section == 1)
    {
        textSize = FONT_SIZE_16 * SCREEN_SCALE_BASE_WIDTH_375;
        fontName = FONT_MarkForMC_REGULAR;
    }
    cell.menuItemName.attributedText = [[NSAttributedString alloc] initWithString:[valuesInSection objectAtIndex:indexPath.row] attributes:@{NSFontAttributeName:[UIFont fontWithName:fontName size:textSize]}];
    [cell setBackgroundColor:[UIColor clearColor]];
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        UIImageView *accessoryView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40*SCREEN_SCALE, 40*SCREEN_SCALE)];
        [accessoryView setImage:[UIImage imageNamed:@"arrow_pro_white"]];
        [cell setAccessoryView:accessoryView];
        cell.contentView.superview.backgroundColor = [UIColor blackColor];
        [cell.accessoryView setHidden:NO];
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
        [cell.accessoryView setHidden:YES];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Set back default Color
    if (!(indexPath.section == 1 && indexPath.row == 1)) {
        [self.menuTableView setUserInteractionEnabled:NO];
    }
    
    appDelegate.currentCollorForMenu = colorFromHexString(DEFAULT_BACKGROUND_COLOR);
    
    UIViewController *newFrontController = nil;
    
    if(indexPath.section == 0)
    {
        if(indexPath.row == 0)
        {
            newFrontController = [[MenuProfileViewController alloc] init];
            [self.navigationController pushViewController:newFrontController animated:YES];
            return;
        }
//        else if(indexPath.row == 1)
//        {
//            newFrontController = [[WebformViewController alloc] init];
//            ((WebformViewController*)newFrontController).navigationTitle = NSLocalizedString(@"Rewards & Benefits Guide", nil);
//            ((WebformViewController*)newFrontController).urlString = @"";
//            [self.navigationController pushViewController:newFrontController animated:NO];
//            return;
//        }
        else if(indexPath.row == 1)
        {
            newFrontController = [[WebformViewController alloc] init];
            ((WebformViewController*)newFrontController).navigationTitle = NSLocalizedString(@"Airport Lounge Access", nil);
            ((WebformViewController*)newFrontController).urlString = AIRPORT_LOUNGE_ACCESS_LINK;
            [self trackingScreenByName:@"Airport Lounge Access"];
            [self.navigationController pushViewController:newFrontController animated:NO];
            return;
        }
        else if(indexPath.row == 2)
        {
            newFrontController = [[WebformViewController alloc] init];
            ((WebformViewController*)newFrontController).navigationTitle = NSLocalizedString(@"Hotel & Travel 2018", nil);
            ((WebformViewController*)newFrontController).urlString = HOTEL_TRAVEL_LINK;
             [self trackingScreenByName:@"Hotel & Travel Magazine"];
            [self.navigationController pushViewController:newFrontController animated:NO];
            return;
        }
        else if(indexPath.row == 3)
        {
            newFrontController = [[WebformViewController alloc] init];
            ((WebformViewController*)newFrontController).navigationTitle = NSLocalizedString(@"LUXURY MAGAZINE", nil);
            ((WebformViewController*)newFrontController).urlString = LUXURY_MAGAZINE_LINK;
             [self trackingScreenByName:@"Luxury Magazine"];
            [self.navigationController pushViewController:newFrontController animated:NO];
            return;
        }
        else
        {
            return;
        }
    }
    else if(indexPath.section == 1)
    {
        if(indexPath.row == 0)
        {
            newFrontController = [[AboutAppViewController alloc] init];
            [self.navigationController pushViewController:newFrontController animated:YES];
            return;
        }
        else if(indexPath.row == 1){
            newFrontController = [[TermsOfUseViewController alloc] init];
            [self.navigationController pushViewController:newFrontController animated:NO];
            return;
        }
        else if(indexPath.row == 2){
            newFrontController = [[PolicyViewController alloc] init];
            [self.navigationController pushViewController:newFrontController animated:NO];
            return;
        }
        else if(indexPath.row == 3)
        {
            [self trackingEventByName:@"Sign out" withAction:ClickActionType withCategory:SignOutCategoryType];
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:CURRENT_CITY_CODE];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:CURRENT_CATEGORY_CODE];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            if ([SessionData shareSessiondata].arrayPreferences.count > 0) {
                [[SessionData shareSessiondata].arrayPreferences removeAllObjects];
            }
    
            [((HomeLXViewController*)[[self.navigationController viewControllers] lastObject]) forceRelease];
            
            [UIView transitionWithView:appDelegate.window
                              duration:0.5
                               options:UIViewAnimationOptionPreferredFramesPerSecond60
                            animations:^{
                                
                                IntroductionViewController *introduceVC = [[IntroductionViewController alloc] init];
                                UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:introduceVC];
                                appDelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
                                appDelegate.window.rootViewController = navigationController;
                                [appDelegate.window makeKeyAndVisible];
                                
                            }
                            completion:
             ^(BOOL finshed){
                 appDelegate.tabBarController.viewControllers = nil;
                 [appDelegate.tabBarController removeFromParentViewController];
             }];
            
            return;
        }
    }
}


@end
