//
//  SearchViewController.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@protocol SearchDelegate <NSObject>

-(void)getDataBaseOnSearchText:(NSString *)text withOffer:(BOOL) isOffer withBookOnline:(BOOL)isBookOnline;

@end

@interface SearchViewController : BaseViewController

@property(nonatomic, weak) IBOutlet UIButton    *offerCheckBox;
@property(nonatomic, weak) IBOutlet UIButton    *bookCheckBox;
@property(nonatomic, weak) IBOutlet UIButton    *searchButton;
@property (weak, nonatomic) IBOutlet UIView *searchButtonView;

@property(nonatomic, weak) IBOutlet UITextField *searchText;
@property(nonatomic, weak) IBOutlet UIView      *searchTextView;
@property(nonatomic, weak) IBOutlet UILabel     *offerText;
@property(nonatomic, weak) IBOutlet UILabel     *bookText;
@property(nonatomic, weak) id<SearchDelegate>   delegate;
@property(nonatomic, strong) NSString           *currentKeyWord;
@property(nonatomic, assign) BOOL               currentIsOffer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBtnBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *stackViewTopConstraint;


- (IBAction)searchTappedAction:(id)sender;
- (void) checkOffer:(BOOL) ischeck;
@end
