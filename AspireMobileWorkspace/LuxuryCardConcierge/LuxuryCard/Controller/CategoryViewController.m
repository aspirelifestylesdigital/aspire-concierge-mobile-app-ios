//
//  CategoryViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "CategoryViewController.h"
#import "CategoryItem.h"
#import "CategoryCollectionViewCell.h"
#import "Constant.h"
#import "AppData.h"
#import "CustomPopTransition.h"
#import "SelectingCategoryDelegate.h"
#import "SubCategoryViewController.h"
#import "CategoryCityGuideViewController.h"
#import "Common.h"
#import "CityTableViewCell.h"
#import "AppData.h"

@interface CategoryViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    CGFloat verticalSpaceDistance;
    CGFloat horizontalSpaceDistance;
    CGPoint subViewCenter;
    
    
    IBOutlet UICollectionViewFlowLayout *flowLayoutCollection;
}

@end

@implementation CategoryViewController

- (void)viewDidLoad {
    isShowAskConciergeBarButton = YES;
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
//    flowLayoutCollection = [UICollectionViewFlowLayout new];
//    [self.collectionView setCollectionViewLayout:flowLayoutCollection];
    [self.tableView setBackgroundColor:colorFromHexString(DEFAULT_BACKGROUND_COLOR)];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self trackingScreenByName:@"Category list"];
    //  Set up Pop pan gesture
    
//    [self setUpCustomizedPanGesturePopRecognizer];
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}

-(void)initView
{
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"category_title", nil)];
    [self createBackBarButton];
    [self.tableView registerNib:[UINib nibWithNibName:@"CityTableViewCell" bundle:nil] forCellReuseIdentifier:@"CityTableViewCell"];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 70.f;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

-(void)initData
{
    self.categoryLst = [AppData getSelectionCategoryList];
    if(self.currentCity.subCategories.count == 0) {
        NSMutableArray* temp = [NSMutableArray arrayWithArray:self.categoryLst];
        [temp removeObjectAtIndex:2];
        self.categoryLst = temp;
    }
}

#pragma mark table DELEGATE

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.categoryLst.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CityTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"CityTableViewCell"];
    CategoryItem *item = [self.categoryLst objectAtIndex:indexPath.row];
    [cell initCellWithCategoryData:item];
    if ([item.code isEqualToString:@"city guide"]) {
        UIImageView *accessoryView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40*SCREEN_SCALE, 40*SCREEN_SCALE)];
        [accessoryView setImage:[UIImage imageNamed:@"right_arrow_white"]];
        [cell setAccessoryView:accessoryView];
        cell.contentView.superview.backgroundColor = [UIColor blackColor];
        [cell.accessoryView setHidden:NO];
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
        [cell.accessoryView setHidden:YES];
  }

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CategoryItem *item = [self.categoryLst objectAtIndex:indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:false];
    
    if([item.code isEqualToString:@"city guide"])
    {
        if(self.currentCity.subCategories.count > 0)
        {
            // wait for text-color change
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [NSThread sleepForTimeInterval:(0.1)];
                dispatch_async(dispatch_get_main_queue(), ^{
                    CategoryCityGuideViewController *cityGuideViewController = [[CategoryCityGuideViewController alloc] init];
                    cityGuideViewController.delegate = self.delegate;
                    cityGuideViewController.subCategories = self.currentCity.subCategories;
                    [self.navigationController pushViewController:cityGuideViewController animated:NO];
                });
            });
        }
    }
    else
    {
        [self trackingEventByName:[item.categoryName capitalizedString] withAction:SelectActionType withCategory:CategorySelectionCategoryType];

        
        [AppData setCurrentCategoryWithCode:item];

        CategoryItem *itemForData = item;
        
        if([item.code isEqualToString:@"dining"])
        {
            itemForData.ID = self.currentCity.diningID;
        }
        
        if(self.delegate)
            [self.delegate Explore:self onSelectCategory:itemForData];
    }
}


#pragma mark LOGICAL FUNCTION
-(void) showAlertForMoreInformationWithURL:(NSURL*)url ForCell:(CategoryCollectionViewCell *)cell
{
    [self showActiveAlertWithTitle:NSLocalizedString(@"alert_title", nil) withMessage:NSLocalizedString(@"category_more_info_msg", nil) withFistBtnTitle:NSLocalizedString(@"Yes", nil) forFirstAction:^{
        [[UIApplication sharedApplication] openURL:url];
        cell.name.textColor = [UIColor whiteColor];
        cell.name.shadowColor = [UIColor blackColor];
        [cell.maskView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.4]];
    } withSecondBtnTitle:NSLocalizedString(@"CANCEL", nil) forSeconAction:^{
        cell.name.textColor = [UIColor whiteColor];
        cell.name.shadowColor = [UIColor blackColor];
        [cell.maskView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.4]];
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
