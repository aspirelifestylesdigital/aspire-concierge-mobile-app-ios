//
//  ForgotPasswordThanksViewController.m
//  LuxuryCard
//
//  Created by Chung Mai on 3/22/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "ForgotPasswordThanksViewController.h"
#import "UIButton+Extension.h"

@interface ForgotPasswordThanksViewController ()

@end

@implementation ForgotPasswordThanksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self createBackBarButton];
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"Forgot Password", nil)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) initView
{
    self.checkEmailTitle.textColor = [UIColor whiteColor];
    self.checkEmailTitle.text = NSLocalizedString(@"Check your email", nil);
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.checkEmailTitle.text];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 49;
    NSDictionary *dict = @{NSParagraphStyleAttributeName : paragraphStyle , NSFontAttributeName : [UIFont fontWithName:FONT_MarkForMC_BOLD size:(21.0f * SCREEN_SCALE_BASE_WIDTH_375)]};
    [attributedString addAttributes:dict range:NSMakeRange(0, self.checkEmailTitle.text.length)];
    
    self.checkEmailTitle.attributedText =  attributedString;
    
    self.messageLbl.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:16.0 * SCREEN_SCALE_BASE_WIDTH_375];
    self.messageLbl.textColor = [UIColor whiteColor];
    self.messageLbl.text = NSLocalizedString(@"We have sent password recovery details to the email address you provided.", nil);
    
    
    self.noteLbl.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:16.0 * SCREEN_SCALE_BASE_WIDTH_375];
    self.noteLbl.textColor = [UIColor whiteColor];
    self.noteLbl.text = NSLocalizedString(@"If you are still unable to gain access to your concierge account, contact us at: 1.888.XXX.XXXX", nil);
    
    [self.returnBtn setTitle:NSLocalizedString(@"Return to Sign In", nil) forState: UIControlStateNormal];
    self.returnBtn.titleLabel.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:14.0 * SCREEN_SCALE_BASE_WIDTH_375];
    [self.returnBtn setDefaultStyle];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)returnToSignInAction:(id)sender {
    NSInteger countViewController = self.navigationController.viewControllers.count;
    [self.navigationController popToViewController:self.navigationController.viewControllers[countViewController - 3] animated:YES];
}
@end
