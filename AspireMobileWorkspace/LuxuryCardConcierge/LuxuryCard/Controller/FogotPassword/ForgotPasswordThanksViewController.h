//
//  ForgotPasswordThanksViewController.h
//  LuxuryCard
//
//  Created by Chung Mai on 3/22/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ForgotPasswordThanksViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UILabel *checkEmailTitle;
@property (weak, nonatomic) IBOutlet UILabel *messageLbl;
@property (weak, nonatomic) IBOutlet UILabel *noteLbl;

@property (weak, nonatomic) IBOutlet UIButton *returnBtn;
- (IBAction)returnToSignInAction:(id)sender;
@end
