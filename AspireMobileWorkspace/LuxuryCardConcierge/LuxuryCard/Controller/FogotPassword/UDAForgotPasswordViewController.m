//
//  UDAForgotPasswordViewController.m
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 7/19/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "UDAForgotPasswordViewController.h"
#import "UITextField+Extensions.h"
#import "NSString+Utis.h"
#import "UIButton+Extension.h"
#import "UtilStyle.h"
#import "WSForgotPassword.h"
#import "UDASignInViewController.h"
#import "ForgotPasswordThanksViewController.h"
#import "AppData.h"

@interface UDAForgotPasswordViewController ()<DataLoadDelegate, UITextFieldDelegate> {
    BOOL isInputEmail;
}
@property (nonatomic, strong) WSForgotPassword *wsForgotPassword;

@end

@implementation UDAForgotPasswordViewController

#pragma mark OVERIDE VIEW CONTROLLER
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *tappedOutsideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tappedOutsideKeyboard];
    [self createBackBarButton];
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"Forgot Password", nil)];
    [self setUIStyte];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self trackingScreenByName:@"Forgot password"];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setTextViewsDefaultBottomBolder];
    });
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark INITIAL STYLE
- (void) setUIStyte {
    
    isInputEmail = NO;
    [self setStatusSubmitButton];
    self.emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    self.emailTextField.delegate = self;
    self.emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.emailTextField.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:18 * SCREEN_SCALE_BASE_WIDTH_375];
    self.emailTextField.textColor = [UIColor whiteColor];
    self.emailTextField.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.emailTextField.placeholder];
    
    self.emailTitle.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:14 * SCREEN_SCALE_BASE_WIDTH_375];
    self.emailTitle.textColor = [UIColor colorWithWhite:1.0 alpha:0.8];
    
    self.titleLabel.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:16 * SCREEN_SCALE_BASE_WIDTH_375];
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.text = NSLocalizedString(@"Enter the email address associated with your account and we will send you an email with your new password.", nil);
    
    [self.submitButton setTitle:@"Submit" forState:UIControlStateNormal];
     self.submitButton.titleLabel.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:14.0 * SCREEN_SCALE_BASE_WIDTH_375];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.topLayoutConstraint.constant = (IPAD) ? 280.0f : 200.0f;
    });
}


#pragma mark - Action
- (IBAction)ForgotPasswordAction:(id)sender {
    
    NSMutableString *message = [[NSMutableString alloc] init];
    if(self.emailTextField.text.length == 0){
        [message appendString:@"* "];
        [message appendString:NSLocalizedString(@"input_invalid_email_msg", nil)];
    }else {
        if([self verifyValueForTextField:self.emailTextField].length > 0) {
            [message appendString:@"* "];
            [message appendString:[self verifyValueForTextField:self.emailTextField]];
        }
    }
    
    [self showError:message];
}


- (void)backAction {
    [self updateSuccessRedirect];
}

- (void)resignFirstResponderForAllTextField {
    if(self.emailTextField.isFirstResponder){
        [self.emailTextField resignFirstResponder];
    }
}

-(void)dismissKeyboard {
    [self resignFirstResponderForAllTextField];
}


-(void)showError:(NSString *)message {
    if(message.length > 0){        
        [self showActiveAlertWithTitle:NSLocalizedString(@"Please confirm that the value entered is correct:", nil) withMessage:message withFistBtnTitle:NSLocalizedString(@"OK", nil) forFirstAction:^{
            [self makeBecomeFirstResponderForTextField];
        } withSecondBtnTitle:@"" forSeconAction:nil];
    }
    else{
        if(!isNetworkAvailable()) {
            [self showErrorNetwork];
            
        }else{
            [self resignFirstResponderForAllTextField];
            [self setTextViewsDefaultBottomBolder];
            [self submitForgorPasswordWithEmail:self.emailTextField.text];
        }
    }
}

-(void)setTextViewsDefaultBottomBolder {
    [self.emailTextField setBottomBolderDefaultColor];
}

-(void) makeBecomeFirstResponderForTextField {
    [self resignFirstResponderForAllTextField];
    if(![self.emailTextField.text isValidEmail]){
        [self.emailTextField becomeFirstResponder];
    }
}

-(NSString*)verifyValueForTextField:(UITextField *)textFied{
    
    NSString *errorMsg = @"";
    if(textFied == self.emailTextField && ![(textFied.isFirstResponder ? textFied.text :self.emailTextField.text) isValidEmail]){
        errorMsg = NSLocalizedString(@"input_invalid_email_msg", nil);
        [textFied setBottomBorderRedColor];
    }else{
        [textFied setBottomBolderDefaultColor];
    }
    
    return errorMsg;
}

- (void)submitForgorPasswordWithEmail:(NSString*)email {
        [self startActivityIndicator];
        NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
        [dataDict setValue:B2C_ConsumerKey forKey:@"ConsumerKey"];
        [dataDict setValue:@"ForgotPassword" forKey:@"Functionality"];
        [dataDict setValue:email forKey:@"Email"];
    
        self.wsForgotPassword = [[WSForgotPassword alloc] init];
        self.wsForgotPassword.delegate = self;
        [self.wsForgotPassword submitForgotPassword:dataDict];
}

- (void)setStatusSubmitButton{
    if(isInputEmail){
        [self.submitButton setDefaultStyle];
        self.submitButton.enabled = YES;
    }
    else{
        [self.submitButton setDisableStyle];
        self.submitButton.enabled = NO;
    }
}
#pragma mark TEXT FIELD DELEGATE

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *inputString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (textField == self.emailTextField) {
        isInputEmail = inputString.length > 0;
    }

    [self setStatusSubmitButton];
    
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.text = [textField.text removeRedudantWhiteSpaceInText];
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == self.emailTextField) {
        self.emailTextField.text = textField.text;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if(textField == self.emailTextField){
        textField.text = self.emailTextField.text;
    }
}


#pragma mark - Delegate from API

-(void)loadDataDoneFrom:(WSBase*)ws
{
    [self stopActivityIndicator];
    if (((WSForgotPassword*)ws).isSuccesful) {
        [AppData storedTimeForgotPasswordForEmail:self.emailTextField.text];
        
        ForgotPasswordThanksViewController *passwordRecory = [[ForgotPasswordThanksViewController alloc] init];
        [self.navigationController pushViewController:passwordRecory animated:YES];
    }
    
}
- (void)updateSuccessRedirect{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)loadDataFailFrom:(WSBase *)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message {
    
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }else{
        [self showAlertWithTitle:NSLocalizedString(@"Please confirm that the value entered is correct:", nil) andMessage:[NSString stringWithFormat:@"* %@",message] andBtnTitle:NSLocalizedString(@"OK", nil)];
    }

}
@end
