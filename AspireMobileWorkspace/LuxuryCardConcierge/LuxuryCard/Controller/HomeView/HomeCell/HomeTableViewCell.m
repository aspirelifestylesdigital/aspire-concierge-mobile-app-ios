//
//  HomeTableViewCell.m
//  LuxuryCard
//
//  Created by user on 3/20/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "HomeTableViewCell.h"

@implementation HomeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lbName.font = [UIFont fontWithName:FONT_MarkForMC_BLACK size:FONT_SIZE_26 * SCREEN_SCALE_BASE_WIDTH_375];
    self.lbName.textColor = [UIColor whiteColor];
    self.imgBackground.layer.cornerRadius = BUTTON_CORNER_RADIUS+2;
    
//    self.imgBackground.layer.borderColor = colorFromHexString(WHITE_COLOR).CGColor;
//    self.imgBackground.layer.borderWidth = 1.0f;
    self.imgBackground.backgroundColor = colorFromHexString(@"#999999");
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
