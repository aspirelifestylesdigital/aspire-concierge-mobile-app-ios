//
//  HomeHeaderTableViewCell.m
//  LuxuryCard
//
//  Created by user on 3/20/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "HomeHeaderTableViewCell.h"
#import "Common.h"

@implementation HomeHeaderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lbName.font = [UIFont fontWithName:FONT_MarkForMC_BLACK size:FONT_SIZE_32 * SCREEN_SCALE_BASE_WIDTH_375];
    self.lbDate.font = [UIFont fontWithName:FONT_MarkForMC_MED size:FONT_SIZE_13 * SCREEN_SCALE_BASE_WIDTH_375];
    
    self.lbDate.textColor = [UIColor whiteColor];
    self.lbName.textColor = [UIColor whiteColor];
    
    self.lbDate.text =  currentDate();
    self.lbDate.alpha = 0.5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
