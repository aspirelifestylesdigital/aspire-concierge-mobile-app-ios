//
//  HomeHeaderTableViewCell.h
//  LuxuryCard
//
//  Created by user on 3/20/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface HomeHeaderTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbDate;
@property (weak, nonatomic) IBOutlet UILabel *lbName;

@end
