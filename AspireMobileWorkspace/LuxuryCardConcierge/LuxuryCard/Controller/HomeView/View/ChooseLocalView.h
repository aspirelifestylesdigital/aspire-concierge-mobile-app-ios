//
//  ChooseLocalView.h
//  LuxuryCard
//
//  Created by user on 3/20/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseLocalView : UIView
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *bLocal;
@property (weak, nonatomic) IBOutlet UIButton *bAsk;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthBAskConstraint;
@property (weak, nonatomic) IBOutlet UIView *vCover;

@end
