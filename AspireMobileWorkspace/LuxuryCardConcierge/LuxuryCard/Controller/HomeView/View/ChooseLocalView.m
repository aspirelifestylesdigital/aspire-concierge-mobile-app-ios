//
//  ChooseLocalView.m
//  LuxuryCard
//
//  Created by user on 3/20/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "ChooseLocalView.h"
#import "Constant.h"
#import "Common.h"

@implementation ChooseLocalView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self customInit];
    }
    return self;
}

-(void)customInit {
    [[NSBundle mainBundle] loadNibNamed:@"ChooseLocalView" owner:self options:nil];
    [self addSubview:_contentView];
    self.contentView.frame = self.frame;
    self.contentView.backgroundColor = colorFromHexString(DEFAULT_BACKGROUND_COLOR);
    
    self.bLocal.backgroundColor = [UIColor whiteColor];
    [self.bLocal setTitle:NSLocalizedString(@"Choose a Destination", nil) forState:UIControlStateNormal];
    self.bLocal.titleLabel.textAlignment = NSTextAlignmentLeft;
    self.bLocal.titleLabel.numberOfLines = 1;
    [self.bLocal setTitleColor:colorFromHexString(TEXT_BLACK_COLOR) forState:UIControlStateNormal];
    self.bLocal.titleLabel.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:FONT_SIZE_16];
    self.bLocal.layer.cornerRadius = BUTTON_CORNER_RADIUS + 1;
    self.bLocal.alpha = 0.8;
    
    self.vCover.layer.cornerRadius = BUTTON_CORNER_RADIUS + 1;
    self.vCover.backgroundColor = colorFromHexString(WHITE_COLOR);
    
    self.bAsk.backgroundColor = [UIColor whiteColor];
    [self.bAsk setImage:[UIImage imageNamed:@"next_icon"] forState:UIControlStateNormal];
    self.bAsk.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.bAsk.titleLabel.numberOfLines = 1;
    [self.bAsk setTitleColor:colorFromHexString(TEXT_BLACK_COLOR) forState:UIControlStateNormal];
    self.bAsk.titleLabel.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:FONT_SIZE_14];
    self.bAsk.layer.cornerRadius = BUTTON_CORNER_RADIUS + 1;
    
}

@end
