//
//  HomeLXViewController.m
//  LuxuryCard
//
//  Created by user on 3/20/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "HomeLXViewController.h"
#import "HomeTableViewCell.h"
#import "HomeHeaderTableViewCell.h"
#import "AppData.h"
#import "ChooseLocalView.h"
#import "CityViewController.h"
#import "WebformViewController.h"
#import "ExploreViewController.h"
#import "WSB2CGetTiles.h"
#import "TileItem.h"
#import "UIImageView+AFNetworking.h"
#import "AppDelegate.h"


#define SELECT_LOCATION_VIEW_HEGHT 58*SCREEN_SCALE

@interface HomeLXViewController () <UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, DataLoadDelegate, SelectingCityDelegate>

@end

@implementation HomeLXViewController
{
//    UIView *selectLocationView;
    UIButton *bSelectLocation;
    ChooseLocalView *selectLocationView;
    CGFloat lastContentOffset;
}

- (void)viewDidLoad {
    isShowAskConciergeButton = YES;
    [super viewDidLoad];
   
    isHiddenNavigationBar = YES;
    // Do any additional setup after loading the view from its nib.
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.tableView registerNib:[UINib nibWithNibName:@"HomeTableViewCell" bundle:nil] forCellReuseIdentifier:@"HomeTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"HomeHeaderTableViewCell" bundle:nil] forCellReuseIdentifier:@"HomeHeaderTableViewCell"];
    self.tableView.estimatedRowHeight = 170.f*SCREEN_SCALE;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [selectLocationView.bLocal addTarget:self action:@selector(chooseCity) forControlEvents:UIControlEventTouchUpInside];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadDataHome)
                                                 name:@"HomeReloadNoti"
                                               object:nil];
}

- (NSArray*)getDefaultCategory{
    TileItem *item;
    CityItem *selectedCity = [AppData getSelectedCity];
    NSMutableArray *homeCategoryList = [[NSMutableArray alloc] init];
    if(!selectedCity || [selectedCity.name isEqualToString:@""])
    {
        item = [[TileItem alloc] init];
        item.ID = @"6311";
        item.title = @"Need to arrange \na rental car?";
        item.tileImage = @"rental_car";
        [homeCategoryList addObject:item];
        
        item = [[TileItem alloc] init];
        item.ID = @"6312";
        item.title = @"Looking for a \nplace to stay?";
        item.tileImage = @"place_stay";
        [homeCategoryList addObject:item];
    }
    
    item = [[TileItem alloc] init];
    item.ID = @"LUXURY MAGAZINE Spring 2018";
    item.title = @"LUXURY MAGAZINE \nSpring 2018";
    item.tileImage = @"luxury_magazine";
    [homeCategoryList addObject:item];
    
    item = [[TileItem alloc] init];
    item.ID = @"Hotel & Travel 2018";
    item.title = @"Hotel & Travel 2018";
    item.tileImage = @"hotel_travel";
    [homeCategoryList addObject:item];
    
    return homeCategoryList;
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    BOOL isShown = [AppData isShownTooltip:@"RequestTab"];
    if(isShown){
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [self showToolTipAtView:appDelegate.tabBarController.tabBar.items[1] inView:nil withKey:@"RequestTab"];
    }
    CityItem *selectedCity = [AppData getSelectedCity];
    if(selectedCity)
    {
        [selectLocationView.bLocal setTitle:selectedCity.name forState:UIControlStateNormal];
    }
    else
    {
        [selectLocationView.bLocal setTitle:NSLocalizedString(@"Choose a Destination", nil) forState:UIControlStateNormal];
    }
    [self layouSelectLocationView];
    [self isChangeNavigationBackgroundColor:YES];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self changeBackgroundForSelectedTabbarItem];
    });
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
     [self trackingScreenByName:@"Home"];
    [self.tableView reloadData];
    NSIndexPath* top = [NSIndexPath indexPathForRow:NSNotFound inSection:0];
    [self.tableView scrollToRowAtIndexPath:top atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self isChangeNavigationBackgroundColor:NO];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self dismistTooltip];
}

- (void)initView{
    [self.bAskConcierge setImage:[UIImage imageNamed:@"phone_call"] forState:UIControlStateNormal];
    [self.bAskConcierge setBackgroundColor:colorFromHexString(DEFAULT_BACKGROUND_COLOR)];
    [self.bAskConcierge setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

-(void)initData
{
//    CityItem *selectedCity = [AppData getSelectedCity];
//    if(!selectedCity)
//    {
//        self.categoryList = [self getDefaultCategory];
//    }
//    else
//    {
//        [self getCategoryList];
//    }
    [self getCategoryList];
}

- (void)reloadDataHome{
    [self getCategoryList];
}
- (void)getDataBaseOnCity:(CityItem *)item{
    
}

- (void)getCategoryList{
    [self startActivityIndicator];
    WSB2CGetTiles *wsGetTiles = [[WSB2CGetTiles alloc] init];
    wsGetTiles.delegate = self;
    wsGetTiles.categoryCode = @"appgallery";
    wsGetTiles.categories = [[NSArray alloc] initWithObjects:@"home", nil];
    [wsGetTiles loadTilesForCategory];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)chooseCityForExplore{
    CityItem *selectedCity = [AppData getSelectedCity];
    CityViewController *vc = [[CityViewController alloc] init];
    vc.currentCity = selectedCity;
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:NO];
}

- (void)chooseCity{
    CityItem *selectedCity = [AppData getSelectedCity];
    if (!selectedCity) {
        CityViewController *vc = [[CityViewController alloc] init];
        vc.currentCity = selectedCity;
        vc.isNeedToPopViewController = YES;
        vc.delegate = self;
        [self.navigationController pushViewController:vc animated:NO];
    }else{
        ExploreViewController *exploreViewController = [[ExploreViewController alloc] init];
        exploreViewController.currentCity = selectedCity;
        [self.navigationController pushViewController:exploreViewController animated:NO];
    }
    
}

- (void)clearCity{
    CityItem *selectedCity = [AppData getSelectedCity];
    if (selectedCity) {
        CityViewController *vc = [[CityViewController alloc] init];
        vc.currentCity = selectedCity;
        vc.delegate = self;
        [self.navigationController pushViewController:vc animated:NO];
    }else{
        CityViewController *vc = [[CityViewController alloc] init];
        vc.delegate = self;
        [self.navigationController pushViewController:vc animated:NO];
    }
    
}


// Table View
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        HomeHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HomeHeaderTableViewCell"];
        NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
        if (profileDictionary) {
            cell.lbName.text = [NSString stringWithFormat:@"Welcome, %@.",[profileDictionary objectForKey:keyFirstName]];
            cell.lbDate.text =  currentDate();
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }else {
        HomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HomeTableViewCell"];
        if (self.categoryList.count > 0) {
            TileItem *item =  self.categoryList[indexPath.row];
            cell.lbName.text = item.title;
            if ([item.tileImage containsString:@"http"]) {
                __weak typeof(cell) weakCell = cell;
                
                NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[item.imageURL removeBlankFromURL]]] ;
                
                [cell.imgBackground setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@""] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                    
                    weakCell.imgBackground.image = image;
                    
                } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
                    
                }];
            }else{
                cell.imgBackground.image = [UIImage imageNamed:item.tileImage];
            }
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return UITableViewAutomaticDimension;
    }else if (indexPath.section == 1){
        return 177*SCREEN_SCALE + 10;
    }else{
        return UITableViewAutomaticDimension;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else{
        return self.categoryList.count;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        if (!selectLocationView) {
            selectLocationView = [[ChooseLocalView alloc] init];
            [selectLocationView.bLocal addTarget:self action:@selector(chooseCity) forControlEvents:UIControlEventTouchUpInside];
            [selectLocationView.bAsk addTarget:self action:@selector(clearCity) forControlEvents:UIControlEventTouchUpInside];
        }
        CityItem *selectedCity = [AppData getSelectedCity];
        if(!selectedCity)
        {
            [selectLocationView.bAsk setImage:[UIImage imageNamed:@"next_icon"] forState:UIControlStateNormal];
        }else{
            [selectLocationView.bAsk setImage:[UIImage imageNamed:@"close_icon"] forState:UIControlStateNormal];
        }
        return selectLocationView;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        return SELECT_LOCATION_VIEW_HEGHT + 10;
    }
    return 0;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        TileItem *item = self.categoryList[indexPath.row];
        if ([item.ID isEqualToString:@"Hotel & Travel 2018"]) {
            WebformViewController *webformViewController = [[WebformViewController alloc] init];
            webformViewController.urlString = @"http://google.com";
            webformViewController.navigationTitle = item.title;
            [self.navigationController pushViewController:webformViewController animated:NO];
        }else if ([item.ID isEqualToString:@"LUXURY MAGAZINE Spring 2018"]){
            WebformViewController *webformViewController = [[WebformViewController alloc] init];
            webformViewController.urlString = LUXURY_MAGAZINE_LINK;
            webformViewController.navigationTitle = NSLocalizedString(@"LUXURY MAGAZINE", nil);
            [self.navigationController pushViewController:webformViewController animated:NO];
        }else if ([item.tileLink containsString:@"http"] || [item.tileLink containsString:@"www"]){
            WebformViewController *webformViewController = [[WebformViewController alloc] init];
            webformViewController.urlString = item.tileLink;
            webformViewController.navigationTitle = item.title;
            [self.navigationController pushViewController:webformViewController animated:NO];
        }else {
            CityItem *selectedCity = [AppData getSelectedCity];
            if(!selectedCity)
            {
                [self chooseCityForExplore];
            }
            else
            {
                ExploreViewController *exploreViewController = [[ExploreViewController alloc] init];
                exploreViewController.currentCity = selectedCity;
                exploreViewController.categoryCode = item.tileLink;
                [self.navigationController pushViewController:exploreViewController animated:NO];
            }
        }
    }
}

- (void) layouSelectLocationView{
    CGRect newFrame = [selectLocationView convertRect:self.view.bounds toView:nil];
    
    
    [UIView animateWithDuration:0.1
                     animations:^{
                         selectLocationView.widthBAskConstraint.constant = newFrame.origin.y > (isIphoneX() == YES ? 90 :  70*SCREEN_SCALE) ? 10 : 70;
                         [selectLocationView layoutIfNeeded]; // Called on parent view
                     }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
//    // Convert the co-ordinates of the view into the window co-ordinate space
//     CGRect newFrame = [selectLocationView convertRect:self.view.bounds toView:nil];
//    if (lastContentOffset > scrollView.contentOffset.y)
//    {
////        NSLog(@"%f", newFrame.origin.y);
////        NSLog(@"Scrolling Up");
//        if (newFrame.origin.y > 70*SCREEN_SCALE) {
//            [UIView animateWithDuration:0.1
//                             animations:^{
//                                 selectLocationView.widthBAskConstraint.constant = selectLocationView.bLocal.frame.size.height * 0*SCREEN_SCALE;
//                                 [selectLocationView layoutIfNeeded]; // Called on parent view
//                             }];
//        }
//    }
//    else if (lastContentOffset < scrollView.contentOffset.y)
//    {
////        NSLog(@"%f", newFrame.origin.y);
////        NSLog(@"Scrolling Down");
//        if (newFrame.origin.y < 70*SCREEN_SCALE) {
//            [UIView animateWithDuration:0.1
//                             animations:^{
//                                 selectLocationView.widthBAskConstraint.constant = selectLocationView.bLocal.frame.size.height * 1.45*SCREEN_SCALE;
//                                 [selectLocationView layoutIfNeeded]; // Called on parent view
//                             }];
//        }
//    }
//
//    lastContentOffset = scrollView.contentOffset.y;
    
    [self layouSelectLocationView];
}

- (void)loadDataDoneFrom:(WSB2CGetTiles *)ws{
    [self stopActivityIndicator];

    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    if (ws.data.count > 0) {
        for (int i = 0; i < ws.data.count; i++) {
            [tempArray addObject:ws.data[i]];
        }
    }
//    NSArray *defaultList = [self getDefaultCategory];
//    for (int i = 0; i < defaultList.count; i++) {
//        [tempArray addObject:defaultList[i]];
//    }
    NSArray *sortedArray = [tempArray sortedArrayUsingComparator:^NSComparisonResult(TileItem *obj1, TileItem *obj2){ return [obj1.tileText compare:obj2.tileText];}];
    [tempArray removeAllObjects];
    [tempArray addObjectsFromArray:sortedArray];
    
    
    self.categoryList = tempArray;
    [self.tableView reloadData];
}
@end
