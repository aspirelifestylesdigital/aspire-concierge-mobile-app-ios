//
//  HomeLXViewController.h
//  LuxuryCard
//
//  Created by user on 3/20/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "BaseViewController.h"
@class HomeLXViewController;
@protocol HomeLXViewControllerDelegate <NSObject>   //define delegate protocol
- (void) reloadDataForHome;  //define delegate method to be implemented within another class
@end //end protocol

@interface HomeLXViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic, strong) NSArray *categoryList;
@property (weak, nonatomic) IBOutlet UIButton *bAskConcierge;
@property (nonatomic, weak) id <HomeLXViewControllerDelegate> homeLXViewControllerDelegate;
@end
