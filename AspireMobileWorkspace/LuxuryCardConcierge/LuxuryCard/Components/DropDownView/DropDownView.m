//
//  DropDownView.m
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 7/25/17.
//  Copyright © 2017 Sunrixe Software Solutions Corporation. All rights reserved.
//

#import "DropDownView.h"
#import "ControlStyleHeader.h"
#import "UtilStyle.h"

#define MAX_ITEM_DISPLAY  7

@interface DropDownView() <UITableViewDelegate, UITableViewDataSource>{
    
    BOOL isCollapsed;
    //int selectedIndex;
    UILabel *titleLabel;
    UITableView *tableContent;
    UITapGestureRecognizer *tapGestureBackground;
}
@end

@implementation DropDownView

#pragma mark - Init
- (instancetype)init {
    if (self = [super init]) {
        [self initLayer];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder])
        [self initLayer];
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame])
        [self initLayer];
    return self;
}

- (void)initLayer {
    
    _selectedIndex = -1;
    _itemDisplay = MAX_ITEM_DISPLAY;
    isCollapsed = YES;
    _isDirectionDown = YES;
    _itemLineBreakMode = NSLineBreakByTruncatingTail;
    
    _valueStatus = DefaultValueStatus;
    
    _type = StandandDropDownType;
    
    _leadingTitle = 16.0f;
    _titleTextAlignment = NSTextAlignmentLeft;
    _titleColor = colorFromHexString(TEXTFIELD_PLACEHOLDER_COLOR_STANDARD);
    _titleFontSize = TEXTFIELD_FONT_SIZE_STANDARD * SCREEN_SCALE_BASE_WIDTH_375;
    _titleFont = [UIFont fontWithName:TEXTFIELD_FONT_NAME_STANDARD size:TEXTFIELD_FONT_SIZE_STANDARD * SCREEN_SCALE_BASE_WIDTH_375];
    
    _itemTextAlignment = NSTextAlignmentLeft;
    
    _itemBackgroundColor = [UIColor whiteColor];
    _itemTextColor = colorFromHexString(TEXTFIELD_TEXT_COLOR_STANDARD);
    _itemsFont = [UIFont fontWithName:TEXTFIELD_FONT_NAME_STANDARD size:TEXTFIELD_FONT_SIZE_STANDARD * SCREEN_SCALE_BASE_WIDTH_375 ];
    _itemHeight = 50.0f * SCREEN_SCALE; // (IPAD)?30.0f:44.0f;
}

#pragma mark - Setter
- (void)setLeadingTitle:(CGFloat)leadingTitle{
    _leadingTitle = leadingTitle;
}

- (void)setTitle:(NSString *)title{
    _title = title;
    titleLabel.text = _title;
}

- (void)setTitleOther:(NSString *)titleOther {
    _titleOther = titleOther;
}

- (void)setTitleTextAlignment:(NSTextAlignment)titleTextAlignment{
    if(titleTextAlignment) {
        _titleTextAlignment = titleTextAlignment;
    }
}
- (void)setTitleFont:(UIFont*)titleFont{
    if(titleFont) {
        _titleFont = titleFont;
    }
}
- (void)setItemLineBreakMode:(NSLineBreakMode)itemLineBreakMode {
    _itemLineBreakMode = itemLineBreakMode;
}

- (void)setTitleColor:(UIColor *)titleColor{
    if(titleColor) {
        _titleColor = titleColor;
        titleLabel.textColor = titleColor;
    }
}

- (void)setTitleFontSize:(CGFloat)titleFontSize{
    if(titleFontSize) {
        _titleFontSize = titleFontSize;
    }
}

- (void)setItemTextAlignment:(NSTextAlignment)itemTextAlignment{
    if(itemTextAlignment) {
        _itemTextAlignment = itemTextAlignment;
    }
}

- (void)setItemHeight:(double)itemHeight{
    if(itemHeight){
        _itemHeight = itemHeight;
    }
}

- (void)setItemBackgroundColor:(UIColor *)itemBackgroundColor{
    if(itemBackgroundColor){
        _itemBackgroundColor = itemBackgroundColor;
    }
}

- (void)setItemTextColor:(UIColor *)itemTextColor{
    if(itemTextColor) {
        _itemTextColor = itemTextColor;
    }
}

- (void)setItemFontSize:(CGFloat)itemFontSize{
    if(itemFontSize) {
        _itemFontSize = itemFontSize;
    }
}

- (void)setItemsFont:(UIFont *)itemsFont{
    if (itemsFont) {
        _itemsFont = itemsFont;
    }
}

- (void)setItemDisplay:(NSInteger)itemDisplay {
    if (itemDisplay > 0) {
        _itemDisplay = itemDisplay;
    }
}
- (void)setIsDirectionDown:(BOOL)isDirectionDown {
    _isDirectionDown = isDirectionDown;
}
- (void)setValueStatus:(DropDownValueStatus)valueStatus{
    _valueStatus = valueStatus;
}

- (void)setSelectedIndex:(int)selectedIndex{
    _selectedIndex = selectedIndex;
}

- (void)setType:(DropDownType)type {
    _type = type;
}

#pragma mark - Setups
- (void)layoutSubviews{
    [super layoutSubviews];
    
    if (_type == StandandDropDownType) {
        if ([self.listItems containsObject:_title]) {
            _selectedIndex = (int)[self.listItems indexOfObject:_title];
        }else{
            _selectedIndex = -1;
        }
    }else if (_type == OtherDropDownType){
        if ([self.listItems containsObject:_titleOther]) {
            _selectedIndex = (int)[self.listItems indexOfObject:_titleOther];
        }else{
            _selectedIndex = -1;
        }
    }

    if (titleLabel == nil) {
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(_leadingTitle, 0, self.frame.size.width - 30.0f, self.frame.size.height)];
        titleLabel.textColor = _titleColor;
        titleLabel.text = _title;
        titleLabel.textAlignment = _titleTextAlignment;
        
        titleLabel.font = _titleFont;
        [self addSubview:titleLabel];
    }
    
    if (self.dropDownImage == nil) {
        self.dropDownImage = [[UIImageView alloc] init];
        [self addSubview:self.dropDownImage];
        self.dropDownImage.translatesAutoresizingMaskIntoConstraints = NO;
        [self.dropDownImage.topAnchor constraintEqualToAnchor:self.topAnchor].active =  YES;
        [self.dropDownImage.bottomAnchor constraintEqualToAnchor:self.bottomAnchor].active = YES;
        [self.dropDownImage.trailingAnchor constraintEqualToAnchor:self.trailingAnchor].active = YES;
        [self.dropDownImage.widthAnchor constraintEqualToConstant:30].active = YES;
        
//        self.dropDownImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width - (16.0f + 15.0f), (self.frame.size.height - 30.0f)/2, 30.0f, 30.0f)];
//        [self.dropDownImage setImage:[UIImage imageNamed:@"back_white_icon"]];
//        self.dropDownImage.transform = CGAffineTransformMakeRotation(-M_PI_2);
//        [self addSubview:self.dropDownImage];
    }
    [self.dropDownImage setImage:[UIImage imageNamed:@"back_white_icon"]];
    self.dropDownImage.contentMode = UIViewContentModeScaleAspectFit;
    self.dropDownImage.transform = CGAffineTransformMakeRotation(-M_PI_2);
    
    if (tableContent == nil) {
        tableContent = [[UITableView alloc] initWithFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height)] ;
        [tableContent registerClass:[UITableViewCell class] forCellReuseIdentifier:@"DropDownCell"];
    }
    tableContent.delegate = self;
    tableContent.dataSource = self;
    tableContent.backgroundColor = _itemBackgroundColor;
    tableContent.separatorColor = [UtilStyle colorForSeparateLine];
    UIEdgeInsets edge = tableContent.separatorInset;
    edge.right = edge.left ;
    tableContent.separatorInset = edge;
    
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTap:)];
    //tapGesture.cancelsTouchesInView = NO;
    [self addGestureRecognizer:tapGesture];
    
}

- (void)didTap:(UIGestureRecognizer*)gesture {
    
    isCollapsed = !isCollapsed;
    if(!isCollapsed) {
        CGFloat height = (CGFloat)(_listItems.count > _itemDisplay ? _itemHeight * _itemDisplay : _itemHeight * (double)(_listItems.count));
        
        tableContent.layer.zPosition = 1;
        [tableContent removeFromSuperview];
        [self.superview addSubview:tableContent];
        [tableContent reloadData];
        
        [UIView animateWithDuration:0.25 animations:^{
            
            if(_isDirectionDown) {
                tableContent.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y + self.frame.size.height + 1, self.frame.size.width, height);
                self.dropDownImage.transform = CGAffineTransformMakeRotation(M_PI_2);
            }
            else {
                tableContent.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y - 1 - height, self.frame.size.width, height);
            }
        }];
        
        if(_delegate != nil){
            if([_delegate respondsToSelector:@selector(didShow:)])
                [_delegate didShow:self];
        }
        
//        UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
//        view.tag = 99121;
//        [self.superview insertSubview:view belowSubview:tableContent];
//        
//        tapGestureBackground = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapBackground:)];
//        [view addGestureRecognizer:tapGestureBackground];
        
    }
    else{
        [self collapseTableView];
    }
}

-(void)didTapBackground : (UIGestureRecognizer *)gesture {
    isCollapsed = TRUE;
    [self collapseTableView];
}

- (void)didTapBackground {
    isCollapsed = TRUE;
    [self collapseTableView];
}

-(void)collapseTableView{
    if(isCollapsed){
        [UIView animateWithDuration:0.25 animations:^{
            
            if(_isDirectionDown) {
                tableContent.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y + self.frame.size.height + 1, self.frame.size.width, 0);
                self.dropDownImage.transform = CGAffineTransformMakeRotation(-M_PI_2);

            }else {
                tableContent.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 0);
            }
        }];
        
       // [[self.superview viewWithTag:99121] removeFromSuperview];
        
        if(_delegate != nil){
            if ([_delegate respondsToSelector:@selector(didHiden:)]) {
                [_delegate didHiden:self];
            }
        }
    }
}

- (void)setBottomBorderRedColor
{
    CALayer *borderLayer = [CALayer layer];
    CGFloat borderWidth = 1.0f;
//    borderLayer.borderColor = [UIColor redColor].CGColor;
    borderLayer.borderColor = colorFromHexStringWithAlpha(@"#ff0000", 0.5).CGColor;
    borderLayer.frame = CGRectMake(0.0f, self.frame.size.height - borderWidth, self.frame.size.width, 1.0f);
    borderLayer.borderWidth = borderWidth;
    [self.layer addSublayer:borderLayer];
    self.layer.masksToBounds = YES;
}


- (void)setBottomBolderDefaultColor
{
    CALayer *borderLayer = [CALayer layer];
    CGFloat borderWidth = 1.0f;
    borderLayer.borderColor = colorFromHexString(@"#99A1A8").CGColor;
    borderLayer.frame = CGRectMake(0.0f, self.frame.size.height - borderWidth, self.frame.size.width, 1.0f);
    borderLayer.borderWidth = borderWidth;
    [self.layer addSublayer:borderLayer];
    self.layer.masksToBounds = YES;
}

#pragma mark - UITableView Datasource & Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _listItems.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DropDownCell"];
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DropDownCell"];
    }
    cell.textLabel.textAlignment = _itemTextAlignment;
    cell.textLabel.text = _listItems[indexPath.row];
    
    cell.textLabel.font = _itemsFont;
    cell.textLabel.textColor = _itemTextColor;
    cell.textLabel.lineBreakMode = _itemLineBreakMode;
    
    cell.backgroundColor = _itemBackgroundColor;
    cell.tintColor = self.tintColor;
    
    UIView *bgColor = [[UIView alloc] init];
    bgColor.backgroundColor = colorFromHexString(@"#E5EBEE");
    [cell setSelectedBackgroundView:bgColor];
    
    if (indexPath.row == _selectedIndex) {
        [cell setBackgroundView:bgColor];
    }else{
        [cell setBackgroundView:nil];
    }
    
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return _itemHeight;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    _selectedIndex = (int)indexPath.row;
    if (_valueStatus == DefaultValueStatus) {
        _valueStatus = NewValueStatus;
    }else{
        if (![_title isEqualToString:_listItems[_selectedIndex]]) {
            _valueStatus = UpdateValueStatus;
        }
    }
    _title = titleLabel.text = _listItems[_selectedIndex];
    _titleColor = titleLabel.textColor = colorFromHexString(TEXTFIELD_TEXT_COLOR_STANDARD);
    isCollapsed = TRUE;
    [self collapseTableView];
    if(_delegate != nil){
        if([_delegate respondsToSelector:@selector(didSelectItem:atIndex:)])
            [_delegate didSelectItem:self atIndex:_selectedIndex];
    }
    
}

@end
