//
//  DropDownView.h
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 7/25/17.
//  Copyright © 2017 Sunrixe Software Solutions Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DropDownView;

@protocol DropDownViewDelegate <NSObject>

@optional
- (void)didSelectItem:(DropDownView*)dropDownView atIndex:(int)index;
- (void)didShow:(DropDownView*)dropDownView;
- (void)didHiden:(DropDownView*)dropDownView;

@end

typedef enum : NSUInteger {
    DefaultValueStatus,
    NewValueStatus,
    UpdateValueStatus,
} DropDownValueStatus;

typedef enum : NSUInteger {
    StandandDropDownType,
    OtherDropDownType,
} DropDownType;

@interface DropDownView : UIView

@property (nonatomic, weak) id <DropDownViewDelegate> delegate;

//Title
@property (nonatomic) IBInspectable CGFloat leadingTitle;
@property (nonatomic, strong) IBInspectable NSString *title;
@property (nonatomic, strong) IBInspectable NSString *titleOther;
@property (nonatomic, retain) IBInspectable UIColor *titleColor;
@property (nonatomic, retain) IBInspectable UIFont *titleFont;
@property (nonatomic) IBInspectable CGFloat titleFontSize;
@property (nonatomic) NSTextAlignment titleTextAlignment;

//Item
@property (nonatomic) IBInspectable double itemHeight;
@property (nonatomic, retain) IBInspectable UIColor *itemBackgroundColor;
@property (nonatomic, retain) IBInspectable UIColor *itemTextColor;
@property (nonatomic) IBInspectable CGFloat itemFontSize;
@property (nonatomic)  UIFont *itemsFont;
@property (nonatomic) NSTextAlignment itemTextAlignment;
@property (nonatomic) NSLineBreakMode itemLineBreakMode;
@property (nonatomic) IBInspectable NSInteger itemDisplay;
@property (nonatomic, strong) UIImageView *dropDownImage;

//Direction
@property (nonatomic) IBInspectable BOOL isDirectionDown;

//Value status
@property (nonatomic) DropDownValueStatus valueStatus;
@property (nonatomic, assign) int selectedIndex;

//Data
@property (nonatomic, retain) NSArray *listItems;

@property (nonatomic) DropDownType type;

//UI
- (void)setBottomBorderRedColor;
- (void)setBottomBolderDefaultColor;
- (void)didTapBackground;
- (void)setTitleFont:(UIFont*)titleFont;
- (void)setItemsFont:(UIFont *)itemsFont;
@end
