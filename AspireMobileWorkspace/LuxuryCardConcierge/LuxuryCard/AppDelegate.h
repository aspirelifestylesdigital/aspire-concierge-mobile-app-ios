//
//  AppDelegate.h
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/12/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "CoreDataHelper.h"

@class UserObject;
@class ALRadialMenu;

@interface AppDelegate : UIResponder <UIApplicationDelegate,UITabBarControllerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIViewController *viewController;
@property (strong, nonatomic) ALRadialMenu *socialMenu;
@property (strong, nonatomic, readonly) CoreDataHelper *coreDataHelper;
@property (strong, nonatomic) UITabBarController *tabBarController;
@property (strong, nonatomic) UIView *conciergeTabBarItemView;

//B2C data
@property (nonatomic, strong)NSDate* B2C_ExpiredAt;

// Webservices
@property (strong, nonatomic) NSString* AuthToken;
@property (strong, nonatomic) NSDate* AuthExpDate;
@property (strong, nonatomic) UIColor *currentCollorForMenu;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) UIView *tabbarItemBGView;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
-(void) saveAccessToken:(NSString*) accesToken refreshToken:(NSString*) refreshToken expired:(NSString*) expiredTime;

@end

