//
//  AppData.h
//  MobileConcierge
//
//  Created by Home on 5/7/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CityItem.h"
#import "CategoryItem.h"

typedef enum ShapeType : NSUInteger {
    kCircle,
    kRectangle,
    kOblateSpheroid
} ShapeType;

@interface AppData : NSObject
+(NSDictionary *)getMenuItems;
+(void)storedTimeAcceptedPolicy;
+(NSTimeInterval)getTimeAcceptedPolicy;
+(BOOL)isAcceptedPolicyOverSixMonths;
+(void)setCreatedProfileSuccessful;
+(BOOL)isCreatedProfile;
+ (AppData *)getInstance;
+(NSArray *)getSelectionCityList;
+(NSArray *)getSelectionCategoryList;
+(NSArray *)getCityGuideList;
+(NSArray *)getSubCategoryItemList;
+(NSArray *)getDefaultAnswers;

+(BOOL) isCurrentCity;
+(CityItem *) getSelectedCity;
+(void)setCurrentCityWithCode:(NSString *)cityName;
+(CategoryItem *) getCurrentCategory;
+(void)setCurrentCategoryWithCode:(CategoryItem *)categoryItem;
+(NSDictionary *)getPreferencesItems;
+(BOOL) isApplyFeatureWithKey:(NSString*) key;

+ (void) setUserCityLocationDevice:(NSString*)city;
+ (NSString*) getUserCityLocationDevice;
+ (BOOL) shouldGetDistanceForListDiningFromCity:(CityItem*)cityName;

+ (void) startService;
+ (BOOL)isTempPasswordOver24hForEmail:(NSString *)email;
+ (void) storedTimeForgotPasswordForEmail:(NSString *)email;

/**
 Check new feature will apply on all app

 @return return YES to test or apply new feature to all app
 */
+ (BOOL) isApplyNewFeatureWithKey:(NSString*)keyFeature;
+(NSString*) showTooltip:(NSString *)key;
+(BOOL)isShownTooltip:(NSString *)key;
@end
