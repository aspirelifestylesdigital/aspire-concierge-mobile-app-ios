//
//  WSB2CGetUserDetails.m
//  ALC
//
//  Created by Anh Tran on 11/3/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CGetUserDetails.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "NSDictionary+SBJSONHelper.h"
#import "BaseResponseObject.h"
#import "AppDelegate.h"
#import "GetUserDetailsResponseObject.h"

@implementation WSB2CGetUserDetails
-(void) getUserDetails{
    [self startAPIAfterAuthenticate];
}

-(void)startAPIAfterAuthenticate{
    self.task = WS_B2C_GET_USER_DETAILS;
    self.subTask = WS_ST_NONE;
    
    NSString* url = [MCD_API_URL stringByAppendingString:GetUserDetailsUrl];
    if ([[SessionData shareSessiondata] AccessToken]) {
        [self POST:url withParams:[self buildRequestParams]];
    }
}

-(void) processDataResults:(NSDictionary*)jsonResult forTask:(NSInteger)ptask forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat
{
    if(jsonResult){
        GetUserDetailsResponseObject *result = [[GetUserDetailsResponseObject alloc] initFromDict:jsonResult];
        if(result.isSuccess){
            
            NSMutableDictionary *member = [[NSMutableDictionary alloc] init];
            [member setObject:[jsonResult[@"Member"] stringForKey:@"EMAIL"] forKey:@"Email"];
            [member setObject:[jsonResult[@"Member"] stringForKey:@"SALUTATION"] forKey:@"Salutation"];
            [member setObject:[jsonResult[@"Member"] stringForKey:@"FIRSTNAME"] forKey:@"FirstName"];
            [member setObject:[jsonResult[@"Member"] stringForKey:@"LASTNAME"] forKey:@"LastName"];
            [member setObject:[jsonResult[@"Member"] stringForKey:@"MOBILENUMBER"] forKey:@"MobileNumber"];
            [member setObject:[jsonResult[@"Member"] stringForKey:@"PROGRAM"] forKey:@"Program"];
            [member setObject:[jsonResult[@"Member"] stringForKey:@"POSTALCODE"] forKey:@"ZipCode"];
            [member setObject:[jsonResult[@"Member"] stringForKey:@"ONLINEMEMBERID"] forKey:@"OnlineMemberID"];
            
            if(member){
                NSArray* array=[jsonResult arrayForKey:@"MemberDetails"];
                if(array.count>0){
                    NSDictionary *memberDetails = [array objectAtIndex:0];
                    [[SessionData shareSessiondata] setOnlineMemberDetailIDs:[memberDetails stringForKey:@"ONLINEMEMBERDETAILID"]];
                    [[SessionData shareSessiondata] setOnlineMemberID:[member stringForKey:@"OnlineMemberID"]];
                    if (![[SessionData shareSessiondata] CurrentPolicyVersion]) {
                        [[SessionData shareSessiondata] setCurrentPolicyVersion:[memberDetails stringForKey:@"POLICYVERSION"]];
                    }
                    [member setBool:[memberDetails boolForKey:@"OPTSTATUS"] forKey:@"OptStatus"];
                }
                
                self.userDetails = [[UserObject alloc] initFromDict:member];
                [[SessionData shareSessiondata] setUserObjectWithDict:member];
                [self.delegate loadDataDoneFrom:self];
            }
        } else if(self.delegate != nil){
            
            [self.delegate loadDataFailFrom:nil withErrorCode:400 errorMessage:result.message];
        }
    }else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}

-(void)processDataResultWithError:(NSError *)error
{
    BaseResponseObject *result = [[BaseResponseObject alloc] init];
    result.task = self.task;
    [self.delegate loadDataFailFrom:result withErrorCode:error.code];
}

-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    
    [dictKeyValues setObject:[[SessionData shareSessiondata] AccessToken] forKey:@"AccessToken"];
//    [dictKeyValues setObject:@"mQ0q1KwlKSPwTYcyVBFJHtUPh5BTqQGp7itE9KA4VM58f0%2bbZkuvTkrxD1gaa3drVfhRl%2fQUiZWCLLjKXzzipIq8zAMsiK8iT11IptsAbDY%3d" forKey:@"AccessToken"];
    [dictKeyValues setObject:@"GetUserDetails" forKey:@"Functionality"];
    [dictKeyValues setObject:B2C_ConsumerKey forKey:@"ConsumerKey"];
    [dictKeyValues setObject:[[SessionData shareSessiondata] OnlineMemberID] forKey:@"OnlineMemberId"];
    [dictKeyValues setObject:B2C_MemberRefNo forKey:@"MemberRefNo"];
        
    return dictKeyValues;
}

@end
