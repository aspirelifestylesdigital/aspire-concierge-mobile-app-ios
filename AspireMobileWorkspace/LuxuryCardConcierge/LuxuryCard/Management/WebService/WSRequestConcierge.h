//
//  WSRequestConcierge.h
//  LuxuryCard
//
//  Created by Viet Vo on 8/29/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "WSB2CBase.h"

@interface WSRequestConcierge : WSB2CBase <DataLoadDelegate>

@property (strong, nonatomic) NSMutableDictionary *dataDict;

- (void)updateConcierge:(NSMutableDictionary*)data;
- (void)getRecentConcierge:(NSMutableDictionary*)data;
- (void)getConciergeDetail:(NSMutableDictionary*)data;

@end
