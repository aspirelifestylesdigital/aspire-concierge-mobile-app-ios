//
//  WSSignIn.m
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 7/14/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSSignIn.h"
#import "UserRegistrationResponseObject.h"
#import "UserRegistrationItem.h"
#import "WSB2CGetAccessToken.h"
#import "WSB2CGetRequestToken.h"
#import "WSB2CGetUserDetails.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "NSDictionary+SBJSONHelper.h"
#import "EnumConstant.h"
#import "NSString+AESCrypt.h"

@interface WSSignIn (){
    
    WSB2CGetAccessToken* wsB2CGetAccessToek;
    WSB2CGetRequestToken* wsB2CGetRequestToken;
    WSB2CGetUserDetails* wsGetUser;
}

@end

@implementation WSSignIn

-(void)signIn:(NSMutableDictionary*) userDict{
    _user = userDict;
    [self startAPIAfterAuthenticateWithAPI:PostUserLogin ];
}

-(void)startAPIAfterAuthenticateWithAPI:(NSString*)apiStr{
    self.task = WS_AUTHENTICATION_LOGIN;
    self.subTask = WS_AUTHENTICATION;
    
    NSString* url = [MCD_API_URL stringByAppendingString:apiStr];
    [self POST:url withParams:[self buildRequestParams]];
    
}
-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        UserRegistrationResponseObject *result = [[UserRegistrationResponseObject alloc] initFromDict:jsonResult];
        result.task = task;
        if(self.delegate !=nil && result.isSuccess){
            
            self.data = [result data];
             [self setUserDefaultInfoWithDict:self.data[0]];
            //Request token
            wsB2CGetRequestToken = [[WSB2CGetRequestToken alloc] init];
            wsB2CGetRequestToken.delegate = self;
            [wsB2CGetRequestToken getRequestToken];
            
        } else if(self.delegate != nil){
            
            [self.delegate loadDataFailFrom:nil withErrorCode:400 errorMessage:([result.errorCode isEqualToString:@"ENR70-1"]) ? result.errorCode : result.message];
        }
        
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
    
}
- (void)setUserDefaultInfoWithDict:(UserRegistrationItem*)item{
    [[SessionData shareSessiondata] setHasForgotPassword:item.hasForgotPassword];
    [[SessionData shareSessiondata] setOnlineMemberID:item.OnlineMemberID];
    [[SessionData shareSessiondata] setOnlineMemberDetailIDs:item.OnlineMemberDetailIDs];
}

-(void)processDataResultWithError:(NSError *)error
{
    [self.delegate loadDataFailFrom:nil withErrorCode:error.code];
}

#pragma mark - DataLoadDelegate
-(void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_GET_REQUEST_TOKEN){
        wsB2CGetAccessToek = [[WSB2CGetAccessToken alloc] init];
        wsB2CGetAccessToek.delegate = self;
        [wsB2CGetAccessToek requestAccessToken:encodeURLFromString(wsB2CGetRequestToken.requestToken) member:[[SessionData shareSessiondata] OnlineMemberID]];
    } else if(ws.task == WS_GET_ACCESS_TOKEN){
        //TODO: get user details
        wsGetUser = [[WSB2CGetUserDetails alloc] init];
        wsGetUser.delegate = self;
        [wsGetUser getUserDetails];
    } else if(ws.task == WS_B2C_GET_USER_DETAILS){
        if(wsGetUser.userDetails != nil){
            self.userDetails = wsGetUser.userDetails;
            [self.delegate loadDataDoneFrom:self];
        } else {
            BaseResponseObject *result = [[BaseResponseObject alloc] init];
            result.task = self.task;
            result.status = 400;
            [self.delegate loadDataFailFrom:result withErrorCode:result.status];
        }
    }
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    result.task = self.task;
    [self.delegate loadDataFailFrom:result withErrorCode:result.status];
}

-(void)loadDataFailFrom:(id<WSBaseProtocol>)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message
{
    [self.delegate loadDataFailFrom:ws withErrorCode:errorCode errorMessage:message];
}

-(NSMutableDictionary*) buildRequestParams{
    
    NSMutableDictionary* dictKeyValues = [[NSMutableDictionary alloc] init];
    [dictKeyValues setObject:[_user objectForKey:@"ConsumerKey"] forKey:@"ConsumerKey"];
    [dictKeyValues setObject:[_user objectForKey:@"Email"] forKey:@"Email2"];
    [dictKeyValues setObject:[_user objectForKey:@"Functionality"] forKey:@"Functionality"];
    [dictKeyValues setObject:[_user objectForKey:@"MemberDeviceID"] forKey:@"MemberDeviceID"];
    [dictKeyValues setObject:[_user objectForKey:[pssWrd_EncryptedKey AES256DecryptWithKey:EncryptKey]] forKey:[pssWrd_EncryptedKey AES256DecryptWithKey:EncryptKey]];

    return dictKeyValues;
}
@end

