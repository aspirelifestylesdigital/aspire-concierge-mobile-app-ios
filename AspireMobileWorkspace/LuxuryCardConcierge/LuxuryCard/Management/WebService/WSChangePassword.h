//
//  WSChangePassword.h
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 8/23/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "WSB2CBase.h"

@interface WSChangePassword : WSB2CBase <DataLoadDelegate>

@property (assign) BOOL isSuccesful;
@property (strong, nonatomic) NSMutableDictionary *dataDict;

-(void)submitChangePassword:(NSMutableDictionary*) data;
@end
