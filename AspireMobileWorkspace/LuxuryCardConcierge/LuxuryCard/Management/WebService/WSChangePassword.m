//
//  WSChangePassword.m
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 8/23/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "WSChangePassword.h"
#import "NSString+AESCrypt.h"

@implementation WSChangePassword

-(void)submitChangePassword:(NSMutableDictionary *)data {
    _dataDict = data;
    _isSuccesful = NO;
    [self startAPIAfterAuthenticateWithAPI:PostChangePasswordUrl];
}

-(void)startAPIAfterAuthenticateWithAPI:(NSString*)apiStr{
    self.task = WS_AUTHENTICATION_LOGIN;
    self.subTask = WS_AUTHENTICATION;
    
    NSString* url = [MCD_API_URL stringByAppendingString:apiStr];
    [self POST:url withParams:[self buildRequestParams]];
    
}
-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{

    if(jsonResult) {
        if(self.delegate !=nil && [jsonResult boolForKey:@"success"]){
            _isSuccesful = YES;
            [self.delegate loadDataDoneFrom:self];
        }else{
            NSArray* mesArr = [jsonResult arrayForKey:@"message"];
            if(mesArr && mesArr.count > 0){
                NSDictionary * error = [mesArr objectAtIndex:0];
                int errorCode = 400;
                if ([error objectForKey:@"code"] != nil && [[error objectForKey:@"code"] isEqualToString:@"ENR70-1"]) {
                    errorCode = 432;
                }
                [self.delegate loadDataFailFrom:nil withErrorCode:errorCode errorMessage:NSLocalizedString(@"The password you entered is incorrect.", nil)];
            }else{
                [self.delegate loadDataFailFrom:nil withErrorCode:400 errorMessage:@"Unknown error"];
            }
        }
        
    }
    else if(self.delegate != nil) {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}

-(void)processDataResultWithError:(NSError *)error
{
    [self.delegate loadDataFailFrom:nil withErrorCode:error.code];
}

#pragma mark - DataLoadDelegate

-(void)loadDataDoneFrom:(WSBase*)ws{
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
}

-(NSMutableDictionary*) buildRequestParams{
    
    NSMutableDictionary* dictKeyValues = [[NSMutableDictionary alloc] init];
    [dictKeyValues setObject:B2C_ConsumerKey forKey:@"ConsumerKey"];
    [dictKeyValues setObject:@"ChangePassword" forKey:@"Functionality"];
    [dictKeyValues setObject:[_dataDict objectForKey:@"Email"] forKey:@"Email2"];
    [dictKeyValues setObject:[_dataDict objectForKey:[OPSS_EncryptedKey AES256DecryptWithKey:EncryptKey]] forKey:[OPSS_EncryptedKey AES256DecryptWithKey:EncryptKey]];
    [dictKeyValues setObject:[_dataDict objectForKey:[NPSS_EncryptedKey AES256DecryptWithKey:EncryptKey]] forKey:[NPSS_EncryptedKey AES256DecryptWithKey:EncryptKey]];
    return dictKeyValues;
}

@end
