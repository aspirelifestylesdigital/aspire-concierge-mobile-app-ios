//
//  WSB2CGetSubCategory.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/30/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSB2CGetSubCategory.h"
#import "SubCategoryResponseObject.h"

@implementation WSB2CGetSubCategory

-(void) loadSubCategoryForCategory
{
    NSString* url = [B2C_IA_API_URL stringByAppendingString:GetSubCategories];
    [self POST:url withParams:[self buildRequestParams]];
}


-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    
    // cause of security scanning, not allow get and set password directly
    NSString* userPass = B2C_PW;
    [dictKeyValues setObject:B2C_SUBDOMAIN forKey:@"subDomain"];
    [dictKeyValues setObject:userPass forKey:@"password"];
    [dictKeyValues setObject:self.categoryId forKey:@"categoryID"];
    return dictKeyValues;
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        SubCategoryResponseObject *response = [[SubCategoryResponseObject alloc] initFromDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && response.isSuccess){
            self.data = [response data];
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.isSuccess];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}

-(void)processDataResultWithError:(NSError *)error
{
    [self.delegate loadDataFailFrom:nil withErrorCode:error.code];
}

@end
