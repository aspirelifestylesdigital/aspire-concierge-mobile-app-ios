//
//  WSCreateUser.h
//  ALC
//
//  Created by Anh Tran on 8/29/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"
#import "UserObject.h"
@interface WSCreateUser : WSB2CBase <DataLoadDelegate>

@property (strong, nonatomic) NSMutableDictionary *user;
@property (strong, nonatomic) UserObject* userDetails;
-(void)createUser:(NSMutableDictionary*) userDict;
-(void)updateUser:(NSMutableDictionary*)userDict;
@end
