//
//  WSPreferences.m
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 7/25/17.
//  Copyright © 2017 Sunrixe Software Solutions Corporation. All rights reserved.
//

#import "WSPreferences.h"
#import "WSB2CGetAccessToken.h"
#import "WSB2CGetRequestToken.h"
#import "WSB2CRefreshToken.h"
#import "WSB2CGetUserDetails.h"
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "NSDictionary+SBJSONHelper.h"
#import "BaseResponseObject.h"
#import "PreferenceObject.h"
#import "AppData.h"

@interface WSPreferences(){
    
    WSB2CGetAccessToken* wsB2CGetAccessToken;
    WSB2CGetRequestToken* wsB2CGetRequestToken;
    WSB2CRefreshToken* wsB2CRefreshToken;
    WSB2CGetUserDetails *wsGetUser;
}
@end

@implementation WSPreferences

- (void)addPreference:(NSMutableDictionary *)data{
    _dataDict = data;
    self.task = WS_ADD_MY_PREFERENCE;
    self.subTask = WS_ST_NONE;
    [self getRequestToken];
    
}

- (void)updatePreference:(NSMutableDictionary *)data{
    _dataDict = data;
    self.task = WS_UPDATE_MY_PREFERENCE;
    self.subTask = WS_ST_NONE;
    [self getRequestToken];
}

- (void)getPreference{
    self.task = WS_GET_MY_PREFERENCE;
    self.subTask = WS_ST_NONE;
    [self getRequestToken];
}

- (void)getRequestToken {
//    if(!wsB2CGetRequestToken)
    wsB2CGetRequestToken = [[WSB2CGetRequestToken alloc] init];
    wsB2CGetRequestToken.delegate = self;
    [wsB2CGetRequestToken getRequestToken];
}

-(void)startAPIAfterAuthenticateWithAPI:(NSString*)apiString{
    
    NSString* url = [MCD_API_URL stringByAppendingString:apiString];
    [self POST:url withParams:[self buildRequestParams]];
    
}
-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat {
    
    if(jsonResult) {
        if ([jsonResult isKindOfClass:[NSArray class]] && task == WS_GET_MY_PREFERENCE) {
            NSArray *arrayData = [self removeDuplicatePreference:(NSArray*)jsonResult byKey:@"TYPE"];
            NSMutableArray *arrayPreferences = [[NSMutableArray alloc] init];
            for (NSDictionary *item in arrayData) {
                PreferenceObject *iPreference = [[PreferenceObject alloc] initFromDict:item];
                if (iPreference.type == HotelPreferenceType) {
                    NSString *valueUseLocation = [item objectForKey:@"VALUE3"];
                    if (valueUseLocation.length > 0) {
                        [[SessionData shareSessiondata] setIsUseLocation:[valueUseLocation.uppercaseString isEqualToString:@"YES"]];
                    }
                    
                    NSString *valueCity = [item objectForKey:@"VALUE4"];
                    if (valueCity.length > 0) {
                        [AppData setCurrentCityWithCode:valueCity];
                    }
                }
                [arrayPreferences addObject:iPreference];
            }
            [SessionData shareSessiondata].arrayPreferences = [NSMutableArray arrayWithArray:arrayPreferences];
            [self.delegate loadDataDoneFrom:self];
            
            //
            wsGetUser = [[WSB2CGetUserDetails alloc] init];
            [wsGetUser getUserDetails];
            
        }else{
            BOOL status = [jsonResult boolForKey:@"success"];
            if (status == NO) {
                NSArray* mesArr = [jsonResult arrayForKey:@"message"];
                if(mesArr && mesArr.count > 0){
                    NSDictionary * error = [mesArr objectAtIndex:0];
                    [self.delegate loadDataFailFrom:nil withErrorCode:400 errorMessage:[error objectForKey:@"message"]];
                }else{
                    [self.delegate loadDataFailFrom:nil withErrorCode:400 errorMessage:@"Unknown error"];
                }
                
            }else{
                [self.delegate loadDataDoneFrom:self];
            }
        }
    }
    else if(self.delegate != nil) {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}

- (NSArray *) removeDuplicatePreference:(NSArray *)array byKey:(NSString*)key {
    NSMutableSet *tempValues = [[NSMutableSet alloc] init];
    NSMutableArray *ret = [NSMutableArray array];
    for(id obj in [array reverseObjectEnumerator]) {
        if(![tempValues containsObject:[obj valueForKey:key]]) {
            [tempValues addObject:[obj valueForKey:key]];
            [ret addObject:obj];
        }
    }
    return ret;
}

-(void)processDataResultWithError:(NSError *)error {
    [self.delegate loadDataFailFrom:nil withErrorCode:error.code];
}

#pragma mark - DataLoadDelegate
-(void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_GET_REQUEST_TOKEN){
        wsB2CGetAccessToken = [[WSB2CGetAccessToken alloc] init];
        wsB2CGetAccessToken.delegate = self;
        [wsB2CGetAccessToken requestAccessToken:encodeURLFromString(wsB2CGetRequestToken.requestToken) member:[[SessionData shareSessiondata] OnlineMemberID]];
    }else if(ws.task == WS_GET_ACCESS_TOKEN){
        
        switch (self.task) {
            case WS_ADD_MY_PREFERENCE:
            {
                [self startAPIAfterAuthenticateWithAPI:AddPreferences];
            }
                break;
            case WS_UPDATE_MY_PREFERENCE:
            {
                [self startAPIAfterAuthenticateWithAPI:UpdatePerferences];
            }
                break;
            case WS_GET_MY_PREFERENCE:
            {
                [self startAPIAfterAuthenticateWithAPI:GetPreferences];
            }
                break;
                
            default:
                break;
        }
    }
}


-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    result.task = self.task;
    [self.delegate loadDataFailFrom:result withErrorCode:errorCode];
}

-(void)loadDataFailFrom:(id<WSBaseProtocol>)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message
{
    [self.delegate loadDataFailFrom:ws withErrorCode:errorCode errorMessage:message];
}

-(NSMutableDictionary*) buildRequestParams{
    
    NSMutableDictionary* dictKeyValues = [[NSMutableDictionary alloc] init];

    switch (self.task) {
        case WS_ADD_MY_PREFERENCE:
        {
            NSMutableDictionary* subDictMember = [[NSMutableDictionary alloc] init];
            [subDictMember setObject:B2C_ConsumerKey forKey:@"ConsumerKey"];
            [subDictMember setObject:@"Preferences" forKey:@"Functionality"];
            [subDictMember setObject:[[SessionData shareSessiondata] AccessToken] forKey:@"AccessToken"];
            [subDictMember setObject:[[SessionData shareSessiondata] OnlineMemberID] forKey:@"OnlineMemberId"];
            [dictKeyValues setObject:subDictMember forKey:@"Member"];

            [dictKeyValues setObject:[_dataDict objectForKey:@"PreferenceDetails"]  forKey:@"PreferenceDetails"];
        }
            break;
        case WS_UPDATE_MY_PREFERENCE:
        {
            NSMutableDictionary* subDictMember = [[NSMutableDictionary alloc] init];
            [subDictMember setObject:B2C_ConsumerKey forKey:@"ConsumerKey"];
            [subDictMember setObject:@"Preferences" forKey:@"Functionality"];
            [subDictMember setObject:[[SessionData shareSessiondata] AccessToken] forKey:@"AccessToken"];
            [subDictMember setObject:[[SessionData shareSessiondata] OnlineMemberID]  forKey:@"OnlineMemberId"];
            [dictKeyValues setObject:subDictMember forKey:@"Member"];
            
            [dictKeyValues setObject:[_dataDict objectForKey:@"PreferenceDetails"]  forKey:@"PreferenceDetails"];
        }
            break;
        case WS_GET_MY_PREFERENCE:
        {
            [dictKeyValues setObject:B2C_ConsumerKey forKey:@"ConsumerKey"];
            [dictKeyValues setObject:@"GetPreference" forKey:@"Functionality"];
            [dictKeyValues setObject:[[SessionData shareSessiondata] AccessToken] forKey:@"AccessToken"];
            [dictKeyValues setObject:[[SessionData shareSessiondata] OnlineMemberID] forKey:@"OnlineMemberId"];
        }
            break;
            
        default:
            break;
    }
    
    return dictKeyValues;
}

@end
