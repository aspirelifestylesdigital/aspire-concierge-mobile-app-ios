//
//  WSForgotPassword.h
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 7/19/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSB2CBase.h"

@interface WSForgotPassword : WSB2CBase <DataLoadDelegate>

@property (assign) BOOL isSuccesful;
@property (strong, nonatomic) NSMutableDictionary *dataDict;

-(void)submitForgotPassword:(NSMutableDictionary*) data;

@end
