//
//  WSB2CBase.h
//  ALC
//
//  Created by Anh Tran on 11/1/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSBase.h"
#import "CategoryItem.h"
#import "CityItem.h"

@interface WSB2CBase : WSBase <DataLoadDelegate>

@property (assign,nonatomic) BOOL isSyncing;

@property (nonatomic, strong) NSString *categoryName;
@property(strong, nonatomic) NSString *categoryCode;
@property(strong, nonatomic) CategoryItem *currentCategory;
@property(strong, nonatomic) CityItem *currentCity;

@end
