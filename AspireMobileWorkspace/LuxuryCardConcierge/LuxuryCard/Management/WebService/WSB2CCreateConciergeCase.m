//
//  WSB2CCreateNewConciergeCase.m
//  MobileConcierge
//
//  Created by user on 5/15/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSB2CCreateConciergeCase.h"
#import "CreateConciergeCaseResponseObject.h"
#import "UserObject.h"
#import "AskConciergeRequest.h"

@implementation WSB2CCreateConciergeCase
-(void) askConciergeWithMessage:(NSDictionary*)dict
{
    NSString *editType = dict[@"editType"];
    NSString* url = nil;
    
    if([editType isEqualToString:UPDATE_EDIT_TYPE]){

        url = [MCD_API_URL stringByAppendingString:UpdateConciergeRequest];
    } else if([editType isEqualToString:CANCEL_EDIT_TYPE]){
        url = [MCD_API_URL stringByAppendingString:UpdateConciergeRequest];
        self.task = WS_CANCEL_CONCIERGE;
        self.subTask = WS_ST_NONE;
    }
    else if ([editType isEqualToString:ADD_EDIT_TYPE]){
        url = [MCD_API_URL stringByAppendingString:CreateConciergeRequestUrl];
    }
    
    
    [self POST:url withParams:[[NSMutableDictionary alloc] initWithDictionary:[AskConciergeRequest buildRequestMessageWithData:dict]]];
    
    // return response
}
-(NSMutableDictionary*) buildRequestParamsWithMessage:(NSDictionary*)dict{
    NSMutableDictionary *contentDict = [[NSMutableDictionary alloc] init];
    
    
    [contentDict setObject:[self configurationAccessTokenDict] forKey:@"forThisExampleImplementationOnly_ConfigurationAccessToken"];
    [contentDict setObject:dict forKey:@"newCaseRequest"];
    NSLog(@"%@",contentDict);
    return contentDict;
}

- (NSMutableDictionary*)configurationAccessTokenDict{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    NSDictionary *keyDict = @{@"key":@"D8FE24E1-E0D6-4EED-BCE1-231D6CB7F183"}; //@{@"key":@"4ef83516-67fb-4bad-8ed4-879f575661de"};
    
    NSString* userPass = B2C_PW;
    [dict setObject:keyDict forKey:@"licenseKey"];
    [dict setValue:B2C_SUBDOMAIN forKey:@"applicationName"];
    [dict setValue:userPass forKey:@"programPassword"];
    
    return dict;
}

- (NSString*) getJSONStringFromArray:(NSMutableArray*)array{
    NSError* error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:&error];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}
-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        CreateConciergeCaseResponseObject *response = [[CreateConciergeCaseResponseObject alloc] initFromDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && response.isSuccess){
            self.data = [response data];
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.isSuccess];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
}
@end
