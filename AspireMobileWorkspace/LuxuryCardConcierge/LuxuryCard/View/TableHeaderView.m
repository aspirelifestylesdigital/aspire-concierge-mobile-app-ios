//
//  TableHeaderView.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/21/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "TableHeaderView.h"
#import "Common.h"
#import "Constant.h"
#import "UIButton+Extension.h"
#import "StyleConstant.h"

@implementation TableHeaderView

-(void)setupViewWithMessage:(NSString *)message withResultText:(NSString*)resultText
{
    resetScaleViewBaseOnScreen(self);
    self.messageLbl.text = message == nil?NSLocalizedString(@"no_result_searching_msg", nil):message;
    self.messageLbl.font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:FONT_SIZE_14 * SCREEN_SCALE_BASE_WIDTH_375];
    self.messageLbl.textColor = colorFromHexString(WHITE_COLOR);
    
    [self.askConciergeBtn setTitle:NSLocalizedString(@"New Request", nil) forState:UIControlStateNormal];
    [self.askConciergeBtn setDefaultStyle];
    
    self.resultsLabel.textColor = colorFromHexString(WHITE_COLOR);
    self.resultsLabel.text = resultText ? resultText : @"";
    self.resultsLabel.font = [UIFont fontWithName:FONT_MarkForMC_BLACK size:FONT_SIZE_21*SCREEN_SCALE_BASE_WIDTH_375];
    self.askConciergeBtn.hidden = NO;
}

@end
