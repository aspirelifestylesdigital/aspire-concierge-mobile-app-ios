//
//  ExploreTableViewCell.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/9/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "ExploreTableViewCell.h"
#import "Constant.h"
#import "UIImageView+AFNetworking.h"
#import "NSString+Utis.h"
#import "Common.h"
#import "THLabel.h"
#import <QuartzCore/QuartzCore.h>
#import "UtilStyle.h"
#import "StyleConstant.h"
#import "UILabel+Extension.h"

@interface ExploreTableViewCell() {
    
}

@end

@implementation ExploreTableViewCell
@synthesize testLbael;
- (void)awakeFromNib {
    [super awakeFromNib];
    self.nameLbl.textColor = colorFromHexString(WHITE_COLOR);
    self.addressLbl.textColor = colorFromHexString(WHITE_COLOR);
    self.offerLbl.textColor = colorFromHexString(WHITE_COLOR);
    self.benefitLbl.textColor = colorFromHexString(WHITE_COLOR);
    [self.nameLbl setFont:[UIFont fontWithName:FONT_MarkForMC_BLACK size:FONT_SIZE_21 * SCREEN_SCALE_BASE_WIDTH_375]];
    [self.addressLbl setFont:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:FONT_SIZE_12 * SCREEN_SCALE_BASE_WIDTH_375]];
     [self.offerLbl setFont:[UIFont fontWithName:FONT_MarkForMC_BOLD size:FONT_SIZE_14 * SCREEN_SCALE_BASE_WIDTH_375]];
    [self.benefitLbl setFont:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:FONT_SIZE_14 * SCREEN_SCALE_BASE_WIDTH_375]];
    self.exploreImg.contentMode = UIViewContentModeScaleAspectFill;
    self.exploreImg.alignLeft = YES;
    [self.nameLbl sizeToFit];
    [self.addressLbl sizeToFit];
    [self.offerLbl sizeToFit];
    [self.benefitLbl sizeToFit];
    
    [self.maskImageView.layer setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.4].CGColor];
    [self.categoryName setFont:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:16.0 * SCREEN_SCALE]];
    self.categoryName.shadowColor = [UIColor blackColor];
    self.categoryName.shadowOffset = CGSizeMake(0,4.0);
    self.categoryName.shadowBlur = 5.0f;
    [self.contentView setBackgroundColor:colorFromHexString(EXPLORE_BACKGROUND_COLOR)];
    
//    [testLbael setFont:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:16.0 * SCREEN_SCALE]];
//    testLbael.textColor = [UIColor whiteColor];
//    testLbael.adjustsFontSizeToFitWidth = YES;
//    testLbael.layer.shadowOpacity = 1;
//    testLbael.layer.shadowOffset = CGSizeMake(0, 4);
//    testLbael.layer.shadowRadius = 3;
}

- (void) UIforMaximumRow:(int)numberOfRow{
    if(self.nameLbl.lineCount > 1){
        self.benefitLbl.numberOfLines = 1;
    }else{
        self.benefitLbl.numberOfLines = 2;
    }
}

- (void)initCellWithData:(BaseItem *)item {
    
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    UIView *view = [self viewWithTag:1000];
    if(!view)
    {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - 1, self.bounds.size.width, 1.0f)];
        [view setBackgroundColor:colorFromHexString(BLACK_COLOR)];
        view.tag = 1000;
        [self addSubview:view];
    }
    else
    {
        view.frame = CGRectMake(0, self.bounds.size.height - 1, self.bounds.size.width, 1.0f);
    }
    
    for (UILabel* label in @[self.nameLbl,self.addressLbl,self.offerLbl, self.benefitLbl]) {
        label.adjustsFontSizeToFitWidth = NO;
        label.lineBreakMode = NSLineBreakByTruncatingTail;
    }
    
    
    self.addressLbl.hidden = self.addressLbl.text.length == 0;
    if(self.delegate) {
        if(self.tag == [self.delegate BaseTableViewCellWhatCellSelected:self]) {
//            [self.contentView setBackgroundColor:colorFromHexString(EXPLORE_BACKGROUND_COLOR)];
            [view setBackgroundColor:colorFromHexString(BLACK_COLOR)];
        }
    }
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    [self.maskImageView.layer setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.4].CGColor];
     UIView *view = [self viewWithTag:1000];
    if(view)
    {
        [view setBackgroundColor:colorFromHexString(BLACK_COLOR)];
    }
    if(highlighted) {
        [self.contentView setBackgroundColor:colorFromHexString(EXPLORE_HIGHLIGHT_COLOR)];
    } else {
        [self.contentView setBackgroundColor:colorFromHexString(EXPLORE_BACKGROUND_COLOR)];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    // Configure the view for the selected state
    [super setSelected:selected animated:animated];
    if(selected)
    {
        [self.maskImageView.layer setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.4].CGColor];
        UIView *view = [self viewWithTag:1000];
        if(view)
        {
            [view setBackgroundColor:colorFromHexString(BLACK_COLOR)];
        }
        [self.contentView setBackgroundColor:colorFromHexString(EXPLORE_HIGHLIGHT_COLOR)];
    } else {
        [self.contentView setBackgroundColor:colorFromHexString(EXPLORE_BACKGROUND_COLOR)];
    }
}

- (void)prepareForReuse {
    [super prepareForReuse];
//    [self.contentView setBackgroundColor:colorFromHexString(EXPLORE_BACKGROUND_COLOR)];
}
@end
