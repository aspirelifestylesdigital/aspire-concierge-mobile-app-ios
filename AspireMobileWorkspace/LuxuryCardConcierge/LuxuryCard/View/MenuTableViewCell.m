//
//  MenuTableViewCell.m
//  LuxuryCard
//
//  Created by Chung Mai on 11/14/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "MenuTableViewCell.h"

@implementation MenuTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void) setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    [super setHighlighted:highlighted animated:animated];
    if(highlighted)
    {
        [self.contentView setBackgroundColor:colorFromHexString(EXPLORE_HIGHLIGHT_COLOR)]; //#E5EBEE
        self.contentView.superview.backgroundColor = colorFromHexString(EXPLORE_HIGHLIGHT_COLOR);
    }else{
        [self.contentView setBackgroundColor:colorFromHexString(BLACK_COLOR)];//#E5EBEE
        self.contentView.superview.backgroundColor = colorFromHexString(BLACK_COLOR);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
    if(selected)
    {
        [self.contentView setBackgroundColor:colorFromHexString(EXPLORE_HIGHLIGHT_COLOR)];//#E5EBEE
        self.contentView.superview.backgroundColor = colorFromHexString(EXPLORE_HIGHLIGHT_COLOR);
    }else{
        [self.contentView setBackgroundColor:colorFromHexString(BLACK_COLOR)];//#E5EBEE
        self.contentView.superview.backgroundColor = colorFromHexString(BLACK_COLOR);
    }
}

@end
