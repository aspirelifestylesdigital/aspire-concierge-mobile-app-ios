//
//  CityTableViewCell.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "CityTableViewCell.h"
#import "CityItem.h"
#import "CategoryItem.h"
#import "Common.h"
#import "StyleConstant.h"

@interface CityTableViewCell(){
    
    __weak IBOutlet UIView *bottomLInt;
}

@end

@implementation CityTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.name.textColor = [UIColor whiteColor];
    self.name.font = [UIFont fontWithName:FONT_MarkForMC_BLACK size:FONT_SIZE_24 * SCREEN_SCALE_BASE_WIDTH_375];
    self.address.textColor =  [UIColor whiteColor];
    self.address.font = [UIFont fontWithName:FONT_MarkForMC_MED size:FONT_SIZE_12 * SCREEN_SCALE_BASE_WIDTH_375];
    [self.contentView setBackgroundColor:[UIColor blackColor]];
}

- (void) setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    [super setHighlighted:highlighted animated:animated];
    if(highlighted)
    {
        [self.contentView setBackgroundColor:colorFromHexString(EXPLORE_HIGHLIGHT_COLOR)]; //#E5EBEE
        self.contentView.superview.backgroundColor = colorFromHexString(EXPLORE_HIGHLIGHT_COLOR);
    }else{
        [self.contentView setBackgroundColor:colorFromHexString(BLACK_COLOR)];//#E5EBEE
        self.contentView.superview.backgroundColor = colorFromHexString(BLACK_COLOR);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    if(selected)
    {
       [self.contentView setBackgroundColor:colorFromHexString(EXPLORE_HIGHLIGHT_COLOR)];//#E5EBEE
        self.contentView.superview.backgroundColor = colorFromHexString(EXPLORE_HIGHLIGHT_COLOR);
    }else{
        [self.contentView setBackgroundColor:colorFromHexString(BLACK_COLOR)];//#E5EBEE
        self.contentView.superview.backgroundColor = colorFromHexString(BLACK_COLOR);
    }
}

-(void) initCellWithData:(CityItem *)item
{
    self.name.text = item.name; //[item.name uppercaseString];
    self.address.text = item.address;
    self.avartaImage.image = item.image;
}

-(void)initCellWithCategoryData:(CategoryItem *)item
{
    self.name.text = item.categoryName.capitalizedString;
    self.address.text = nil;
    self.avartaImage.image = nil;
    self.name.font = [UIFont fontWithName:FONT_MarkForMC_BLACK size:FONT_SIZE_26 * SCREEN_SCALE_BASE_WIDTH_375];
    
//    self.address.textColor = [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1];
}

- (void)setHiddenBottomLine:(BOOL)isHide {
    bottomLInt.hidden = isHide;
}

- (void)prepareForReuse {
    bottomLInt.hidden = YES;
    [self.contentView setBackgroundColor:[UIColor blackColor]];
}
@end
