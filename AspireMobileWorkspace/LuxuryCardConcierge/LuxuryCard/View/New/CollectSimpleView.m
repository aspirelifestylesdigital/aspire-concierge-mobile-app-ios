//
//  CollectSimpleView.m
//  Test
//
//  Created by Dai Pham on 7/19/17.
//  Copyright © 2017 dai.pham.s3corp.com.vn. All rights reserved.
//

#import "CollectSimpleView.h"

@interface CollectSimpleView()<UICollectionViewDelegate,UICollectionViewDataSource> {
    
    __weak IBOutlet UICollectionViewFlowLayout *collectViewFlowLayout;
    __weak IBOutlet UICollectionView *collectView;
}

@end

@implementation CollectSimpleView
#pragma mark - 🚸 INIT 🚸
- (instancetype)init {
    if(self = [super init]) {
     self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
    }
    
    return self;
}

- (void) awakeFromNib {
    [super awakeFromNib];
    
    
}

#pragma mark - 🚸 COLLECT VIEW DELEGATE 🚸


@end
