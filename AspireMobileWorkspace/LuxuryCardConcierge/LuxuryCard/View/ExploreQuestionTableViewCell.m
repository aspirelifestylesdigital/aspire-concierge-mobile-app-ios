//
//  ExploreQuestionTableViewCell.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/31/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "ExploreQuestionTableViewCell.h"
#import "Common.h"
#import "UtilStyle.h"
#import "StyleConstant.h"

@implementation ExploreQuestionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.nameLabel.textColor = colorFromHexString(WHITE_COLOR);
    self.addressLabel.textColor = colorFromHexString(WHITE_COLOR);
    self.descriptionLabel.textColor = colorFromHexString(WHITE_COLOR);
    [self.nameLabel setFont:[UIFont fontWithName:FONT_MarkForMC_BLACK size:FONT_SIZE_21 * SCREEN_SCALE_BASE_WIDTH_375]];
    [self.addressLabel setFont:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:FONT_SIZE_12 * SCREEN_SCALE_BASE_WIDTH_375]];
    [self.descriptionLabel setFont:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:FONT_SIZE_14 * SCREEN_SCALE_BASE_WIDTH_375]];
    [self.contentView setBackgroundColor:colorFromHexString(EXPLORE_BACKGROUND_COLOR)];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    UIView *view = [self viewWithTag:1000];
    if(!view)
    {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - 1, self.bounds.size.width, 1.0f)];
        [view setBackgroundColor:colorFromHexString(BLACK_COLOR)];
        view.tag = 1000;
        [self addSubview:view];
    }
    else
    {
        view.frame = CGRectMake(0, self.bounds.size.height - 1, self.bounds.size.width, 1.0f);
    }
    if(self.delegate) {
        if(self.tag == [self.delegate BaseTableViewCellWhatCellSelected:self]) {
//            [self.contentView setBackgroundColor:colorFromHexString(EXPLORE_BACKGROUND_COLOR)];
            [view setBackgroundColor:colorFromHexString(BLACK_COLOR)];
        }
    }
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    UIView *view = [self viewWithTag:1000];
    if(view)
    {
        [view setBackgroundColor:colorFromHexString(BLACK_COLOR)];
    }
    if(highlighted) {
        [self.contentView setBackgroundColor:colorFromHexString(EXPLORE_HIGHLIGHT_COLOR)];
    }else{
        [self.contentView setBackgroundColor:colorFromHexString(EXPLORE_BACKGROUND_COLOR)];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    // Configure the view for the selected state
    [super setSelected:selected animated:animated];
    if(selected)
    {
        UIView *view = [self viewWithTag:1000];
        if(view)
        {
            [view setBackgroundColor:colorFromHexString(BLACK_COLOR)];
        }
        [self.contentView setBackgroundColor:colorFromHexString(EXPLORE_HIGHLIGHT_COLOR)];
    }else{
        [self.contentView setBackgroundColor:colorFromHexString(EXPLORE_BACKGROUND_COLOR)];
    }
}
- (void)prepareForReuse {
    [super prepareForReuse];
//    [self.contentView setBackgroundColor:colorFromHexString(EXPLORE_BACKGROUND_COLOR)];
}
@end
