//
//  MenuTableViewCell.h
//  LuxuryCard
//
//  Created by Chung Mai on 11/14/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"

@interface MenuTableViewCell : BaseTableViewCell

@property (nonatomic, weak) IBOutlet UILabel *menuItemName;

@end
