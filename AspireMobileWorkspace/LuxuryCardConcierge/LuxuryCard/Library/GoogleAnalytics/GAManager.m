//
//  GAManager.m
//  LuxuryCard
//
//  Created by Dai Pham on 9/7/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "GAManager.h"

static GAManager* shared;

@implementation GAManager

+ (GAManager*)configure {
    if(!shared) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            shared = [GAManager new];
        });
    }
    
    return shared;
}

#pragma mark - 🚸 INIT 🚸
- (instancetype) init {
    if(self = [super init]) {
        // do any thing
        [[FIRConfiguration sharedInstance] setLoggerLevel:FIRLoggerLevelError];
        [FIRApp configure];
    }
    return self;
}

#pragma mark - 🚸 STATIC 🚸
+ (void)trackingWithEventName:(NSString *const)nameEvent withItemInfo:(NSDictionary*)itemInfo{
    if(nameEvent.length == 0 || itemInfo.count == 0) {
        printf("%s",[[NSString stringWithFormat:@"\n--------\n%s ERROR: PLEASE PROVIDE {NAME EVENT} AND {ITEMINFO} FOR GOOGLE ANALYTICS\n--------\n\n",__PRETTY_FUNCTION__] UTF8String]);
        return;
    }
    
    [FIRAnalytics logEventWithName:nameEvent
                        parameters:/*@{
                                    kFIRParameterItemID:[NSString stringWithFormat:@"id-%@",nameEvent],
                                    kFIRParameterItemName:nameEvent,
                                    kFIRParameterContentType:@"Screen"
                                    }*/itemInfo];
}
@end
