//
//  NSTimer+Block.h
//  ALC
//
//  Created by Dai Pham on 5/10/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSTimer (Block)

#pragma mark STATIC
+ (NSTimer*) timerWithInterval:(float)f andBlock:(void(^)(NSTimer*))block;

+ (NSTimer*) timerWithTimeoutMain:(float)f andBlock:(void(^)(NSTimer*))block;

+ (NSTimer*) timerWithTimeout:(float)f andBlock:(void(^)(NSTimer*))block;

+ (NSTimer*) timerWithIntervalX:(float)f andBlock:(void(^)(NSTimer*))block;


@end
