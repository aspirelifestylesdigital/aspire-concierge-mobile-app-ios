//
//  LCPageControl.m
//  LuxuryCard
//
//  Created by Chung Mai on 4/4/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "LCPageControl.h"

@implementation LCPageControl

-(id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    activeImage = [UIImage imageNamed:@"pagecontrol_current_indicator"];
    inactiveImage = [UIImage imageNamed:@"pagecontrol_indicator"];
    
    return self;
}

//-(void) updateDots
//{
//    for (int i = 0; i < [self.subviews count]; i++)
//    {
//        UIImageView* dot = [self.subviews objectAtIndex:i];
//        if (i == self.currentPage) dot.image = activeImage;
//        else dot.image = inactiveImage;
//    }
//}

-(void) updateDots
{
    for (int i = 0; i < [self.subviews count]; i++)
    {
        UIImageView * dot = [self imageViewForSubview:  [self.subviews objectAtIndex: i]];
        if (i == self.currentPage) dot.image = activeImage;
        else dot.image = inactiveImage;
    }
}
- (UIImageView *) imageViewForSubview: (UIView *) view
{
    UIImageView * dot = nil;
    if ([view isKindOfClass: [UIView class]])
    {
        for (UIView* subview in view.subviews)
        {
            if ([subview isKindOfClass:[UIImageView class]])
            {
                dot = (UIImageView *)subview;
                break;
            }
        }
        if (dot == nil)
        {
            dot = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, view.frame.size.width, view.frame.size.height)];
            [view addSubview:dot];
        }
    }
    else
    {
        dot = (UIImageView *) view;
    }
    
    return dot;
}

-(void) setCurrentPage:(NSInteger)page
{
    [super setCurrentPage:page];
    [self updateDots];
}

@end
