//
//  LCPageControl.h
//  LuxuryCard
//
//  Created by Chung Mai on 4/4/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LCPageControl : UIPageControl
{
    UIImage* activeImage;
    UIImage* inactiveImage;
}

@end
