//
//  MenuSearch.h
//  Test
//
//  Created by 😱 on 7/14/17.
//  Copyright © 2017 dai.pham.s3corp.com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, OptionSearchType) {
    OptionSearchTypeCheckbox=1,
    OptionSearchTypeClear
};

@class MenuSearch;

@protocol MenuSearchProtocol <NSObject>
@optional

/**
 If return NO, No open keyboard, should do something

 @param view MenuSearch
 @return NO or YES
 */
- (BOOL) MenuSearchIsTextFieldShouldBeginEditing:(MenuSearch*)view;

/**
 send text user input
 warning: if you implement protocol:MenuSearchIsTextFieldShouldBeginEditing, you have to return YES
 @param view MenuSearch
 @param sender TextField Search
 */
- (void) MenuSearch:(MenuSearch*)view onTextSearchDidChange:(UITextField*)sender;


/**
 called when a option search is cleared

 @param view MenuSearch
 @param optionSearch Object Option Search
 */
- (void) MenuSearch:(MenuSearch*)view onClearOptionSearch:(id) optionSearch;
@end

@interface MenuSearch : UIView
@property (weak,nonatomic) id<MenuSearchProtocol> delegate;
@property (weak, nonatomic) IBOutlet UIStackView *stackView;


/**
 set attributed text for text field search
 
 @param attributeText attribute text with custom range color or any thing ....
 @param optionSearchs list option search for search
 */
- (void) setAttributeTextSearch:(NSAttributedString*) attributeText withListOptionSearchs:(NSArray*)optionSearchs;

- (void) setOptionSearchType:(OptionSearchType) type;
@end
