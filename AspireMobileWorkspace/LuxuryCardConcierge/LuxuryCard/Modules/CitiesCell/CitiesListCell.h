//
//  CitiesListCell.h
//  TestIOS
//
//  Created by Dai Pham on 7/17/17.
//  Copyright © 2017 Dai Pham. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CitiesListCell : UITableViewCell

- (void) presentObject:(id) obj;
- (void) hiddenSeparateLine;
@end
