//
//  ViewLog.m
//  MobileConciergeUSDemo
//
//  Created by Dai Pham on 8/5/17.
//  Copyright © 2017 Sunrixe Software Solutions Corporation. All rights reserved.
//

#import "ViewLog.h"

#import "NSTimer+Block.h"

@interface ViewLog() {
    
    IBOutlet UILabel *txtLog;
    UITapGestureRecognizer* gesture;
}

@end

@implementation ViewLog

#pragma mark - INIT
- (id)init {
    if(self = [super init]) {
        self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];
    }
    
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.userInteractionEnabled = YES;
    gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideView:)];
    [self addGestureRecognizer:gesture];
    gesture.cancelsTouchesInView = YES;
}

- (void)dealloc {
    [self removeGestureRecognizer:gesture];
}
#pragma mark - INTERFACE
- (void)displayLog:(NSString *)log {
    if(log.length > 0) {
        txtLog.text = [txtLog.text stringByAppendingFormat:@"\n%@",log];
    }
    
    [NSTimer timerWithTimeout:10 andBlock:^(NSTimer* tmr){
        [self cutLog:nil];
    }];
}

- (NSRange) getMatchesFromString:(NSString*) string withPattern:(NSString*) pattern {
    NSRange result = NSMakeRange(NSNotFound, 0);
    NSRegularExpression *regexUL = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:NULL];
    NSArray *uls = [regexUL matchesInString:string options:0 range:NSMakeRange(0, [string length])] ;
    
    NSInteger count = 0;
    for (NSTextCheckingResult *match in uls) {
        if(count == 4)
            result = [match range];
        count ++;
    }
    if(count < 3)
        result = NSMakeRange(NSNotFound, 0);
    
    return result;
}

#pragma mark - ACTION
- (void)hideView:(id)sender {
    [self cutLog:^{
        [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionShowHideTransitionViews animations:^{
            self.hidden = YES;
        } completion:nil];
    }];
}

- (void) cutLog:(void(^)())isHide {
    NSRange tempArr = [self getMatchesFromString:txtLog.text withPattern:@"\n"];
    if(tempArr.location != NSNotFound) {
        txtLog.text = [txtLog.text substringFromIndex:NSMaxRange(tempArr)];
    } else {
        if(isHide)
            isHide();
    }
}
@end
