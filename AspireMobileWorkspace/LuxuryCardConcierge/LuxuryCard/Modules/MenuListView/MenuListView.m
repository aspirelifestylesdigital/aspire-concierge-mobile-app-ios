//
//  MenuListView.m
//  LuxuryCard
//
//  Created by Dai Pham on 9/8/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "MenuListView.h"
#import "Common.h"
#import "Constant.h"

#define FONT_SIZE 16 * SCREEN_SCALE

@class MenuItemView;
@protocol MenuItemViewProtocol<NSObject>
- (void) MenuItemView:(MenuItemView*)view selectItem:(id) item;
@end

@interface MenuItemView: UIView {
    
    IBOutlet UILabel *label;
    IBOutlet UIView *bottomLine;
    
    BOOL isSelected;
    UIColor* textColor;
    UIColor* selectedTextColor;
}

@property (weak) id<MenuItemViewProtocol> delegate;

- (void) setStateSelected:(BOOL) isSelected;
- (void) setText:(NSString*) text;
- (void) setFont:(UIFont*) font;
- (void) setTextColor:(UIColor*) color;
- (void) setSelectedTextColor:(UIColor*) color;
- (void) setBottomLineColor:(UIColor*) color;
@end


@interface MenuListView()<MenuItemViewProtocol> {
    // private property
    UIColor* itemColor;
    UIColor* selectedItemColor;
    UIColor* backgroundColor;
    
    // private outlet
    IBOutlet UIStackView *stackView;
    
    NSMutableArray* listMenuItems;
    NSInteger selectedIndex;
    UIFont* fontItem;
}
@end

@implementation MenuListView

#pragma mark - 🚸 INIT 🚸
- (instancetype)init {
    if(self = [super init]) {
        self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];
    }
    listMenuItems = [NSMutableArray new];
    
    return self;
}

#pragma mark - 🚸 INTERFACE 🚸
- (void)setBGColor:(UIColor *)color {
    if(!color)
        return;
    backgroundColor = color;
    [self config];
}

- (void)setItemColor:(UIColor *)color {
    if(!color)
        return;
    itemColor = color;
    [self config];
}

- (void)setSeletedItemColor:(UIColor *)color {
    if(!color)
        return;
    selectedItemColor = color;
    [self config];
}

- (void)setFontForItem:(UIFont *)font {
    if(!font)
        return;
    fontItem = font;
}

- (void) reload {
    [self config];
}

- (void)setDefaultIndex:(NSInteger)index {
    selectedIndex = index;
    [self config];
}
#pragma mark - 🚸 Handle Select Menu Item 🚸
- (void)MenuItemView:(MenuItemView *)view selectItem:(id)item {
    if(self.delegate) {
        if (![self.delegate respondsToSelector:@selector(MenuListView:didSelectedItem:atIndex:)]) {
            NSLog(@"PLEASE IMPLEMENTATION FUNC {MenuListView:didSelectedItem:atIndex:} FOR GET EVENT");
            return;
        }
        [self.delegate MenuListView:self didSelectedItem:item atIndex:view.tag];
        selectedIndex = view.tag;
        [self config];
    }
}
#pragma mark - 🚸 PRIVATE 🚸
- (void) config {
    
    // remove all subviews before
    if(listMenuItems.count > 0) {
        for (MenuItemView* view in [listMenuItems reverseObjectEnumerator]) {
            [stackView removeArrangedSubview:view];
            [view removeFromSuperview];
        }
        [listMenuItems removeAllObjects];
    }
    
    if(!self.delegate) {
        NSLog(@"SET DELEGATE FOR MENULISTVIEW BEFORE USE");
        return;
    }
    
    if(![self.delegate respondsToSelector:@selector(MenuListViewListItemsForView:)]) {
        NSLog(@"PLEASE IMPLEMENTATION FUNC {MenuListViewListItemsForView} FOR MENULISTVIEW");
        return;
    }
    
    if(!backgroundColor)
        backgroundColor = [UIColor blackColor]; //colorFromHexString(@"#253847"); //#253847
    if(!itemColor)
        itemColor = colorFromHexString(@"#99A1A8");
    
    if(!selectedItemColor)
        selectedItemColor = colorFromHexString(@"#ffffff");
    
    if(!fontItem)
        fontItem = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:FONT_SIZE];
    
    self.backgroundColor = backgroundColor;
    
    NSArray* items = [self.delegate MenuListViewListItemsForView:self];
    if(items.count > 0) {
        NSInteger i = 0;
        for (NSString* item in items) {
            
            // !!!: Insert & Add Menu Item
            MenuItemView* itemView = [MenuItemView new];
            itemView.tag = i;
            itemView.delegate = self;
            
            [itemView setBottomLineColor:selectedItemColor];
            [itemView setSelectedTextColor:selectedItemColor];
            [itemView setTextColor:itemColor];
            [itemView setFont:fontItem];
            [itemView setText:item];
            
            if(selectedIndex > items.count - 1)
                selectedIndex = 0;            
            [itemView setStateSelected:selectedIndex == i];
            
            [stackView insertArrangedSubview:itemView atIndex:stackView.arrangedSubviews.count];
            [listMenuItems addObject:itemView];
            i++;
        }
    }
}
@end

@implementation MenuItemView
- (instancetype) init {
    if(self = [super init]) {
        self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([MenuListView class]) owner:self options:0] lastObject];
    }
    
    return self;
}

-  (void)awakeFromNib {
    [super awakeFromNib];
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selected:)];
    [self addGestureRecognizer:tap];
}

- (void)setStateSelected:(BOOL)is {
    isSelected = is;
    
//    [bottomLine setHidden:!isSelected];
    label.textColor = isSelected?selectedTextColor:textColor;
}

-(void)setText:(NSString *)text {
    if(text == nil)
        label.text = @"";
    else
        label.text = text;
}

- (void)setFont:(UIFont *)font {
    if(!font)
        return;
    [label setFont:font];
}

- (void)setTextColor:(UIColor *)color {
    if(!color)
        return;
    textColor = color;
}

- (void)setSelectedTextColor:(UIColor *)color {
    if(!color)
        return;
    selectedTextColor = color;
}

- (void)setBottomLineColor:(UIColor *)color {
    if(!color)
        return;
    [bottomLine setBackgroundColor:color];
}

- (void) selected:(UIGestureRecognizer*) gesture {
    if(self.delegate && label.text.length > 0)
        [self.delegate MenuItemView:self selectItem:label.text];
}
@end
