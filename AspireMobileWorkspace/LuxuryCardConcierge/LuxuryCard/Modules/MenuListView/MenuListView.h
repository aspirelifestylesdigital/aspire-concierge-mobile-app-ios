//
//  MenuListView.h
//  LuxuryCard
//
//  Created by Dai Pham on 9/8/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "Common.h"

@class MenuListView;

#pragma mark - 🚸 MenuListViewProtocol 🚸
@protocol MenuListViewProtocol<NSObject>

/**
 Tell this view list items applied

 @param view MenuListView
 @return list string name item
 */
- (NSArray*) MenuListViewListItemsForView:(MenuListView*) view;

/**
 Tell the delegate seleted item
 
 @param view MenuListView
 @param item item seleted
 @param index index of item selected
 */
- (void) MenuListView:(MenuListView*)view didSelectedItem:(id) item atIndex:(NSInteger) index;

@end

#pragma mark - 🚸 MenuListView 🚸
@interface MenuListView : UIView

@property (weak) id<MenuListViewProtocol> delegate;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;


/**
 set background color for view. Default use color follow constant.h

 @param color pass a UIColor
 */
- (void) setBGColor:(UIColor*) color;

/**
 set color for normal item. Default use color follow constant.h

 @param color pass a UIColor
 */
- (void) setItemColor:(UIColor*) color;

/**
 set color for selected item. Default use color follow constant.h

 @param color pass a UIColor
 */
- (void) setSeletedItemColor:(UIColor*) color;

/**
 set font for menu item

 @param font font for menu item
 */
- (void) setFontForItem:(UIFont*) font;

/**
 refresh Menu when apply new data
 */
- (void) reload;

- (void) setDefaultIndex:(NSInteger)index;
@end
