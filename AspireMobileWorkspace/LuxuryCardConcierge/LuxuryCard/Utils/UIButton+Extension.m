//
//  UIButton+Extension.m
//  MobileConcierge
//
//  Created by Home on 5/7/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "UIButton+Extension.h"
#import "Constant.h"
#import "Common.h"
#import "StyleConstant.h"

@implementation UIButton (Extension)

- (void)setHighlighted:(BOOL)highlighted{
    [super setHighlighted:highlighted];
    if(self.highlighted) {
        [self setAlpha:0.8];
    }
    else {
        [self setAlpha:1.0];
    }
}

-(void)setBackgroundColorForNormalStatus
{
    UIGraphicsBeginImageContext(CGSizeMake(1.0f, 1.0f));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, colorFromHexString(@"ffffff").CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 1));
    UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self setBackgroundImage:colorImage forState:UIControlStateNormal];
    [self.layer setCornerRadius:2.0f];
    self.titleLabel.font =[UIFont fontWithName:FONT_MarkForMC_REGULAR size:14.0 * SCREEN_SCALE_BASE_WIDTH_375];
}

-(void) setBackgroundColorForTouchingStatus
{
    UIGraphicsBeginImageContext(CGSizeMake(1.0f, 1.0f));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, colorFromHexStringWithAlpha(@"ffffff", 0.3).CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 1));
    UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self setBackgroundImage:colorImage forState:UIControlStateHighlighted];
    
}

-(void) setBackgroundColorForSelectedStatus
{
    UIGraphicsBeginImageContext(CGSizeMake(1.0f, 1.0f));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, colorFromHexString(BUTTON_BG_COLOR_HIGHLIGHT).CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 1));
    UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self setBackgroundImage:colorImage forState:UIControlStateSelected];
}


-(void) setBackgroundColorForDisableStatus
{
    UIGraphicsBeginImageContext(CGSizeMake(1.0f, 1.0f));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, colorFromHexStringWithAlpha(BLACK_COLOR, 0.4).CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 1));
    UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self setBackgroundImage:colorImage forState:UIControlStateDisabled];
}

-(void) setWhiteBackgroundColorForTouchingStatus
{
    UIGraphicsBeginImageContext(CGSizeMake(1.0f, 1.0f));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:0.2f].CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 1));
    UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self setBackgroundImage:colorImage forState:UIControlStateHighlighted];
}


-(void)configureDecorationForButton
{
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.titleLabel setFont:[UIFont fontWithName:BUTTON_FONT_NAME_STANDARD size:BUTTON_FONT_SIZE_STANDARD * SCREEN_SCALE_BASE_WIDTH_375]];
    
    NSDictionary *attributes = @{NSForegroundColorAttributeName: self.titleLabel.textColor,
                                 NSFontAttributeName: self.titleLabel.font};
    self.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.titleLabel.text attributes:attributes];
}

-(void)configureDecorationForExploreButton
{
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.titleLabel setFont:[UIFont fontWithName:FONT_MarkForMC_MED size:FONT_SIZE_12 * SCREEN_SCALE_BASE_WIDTH_375]];
}

-(void)configureDecorationBlackTitleButton
{
    [self setTitleColor:colorFromHexString(DEFAULT_BACKGROUND_COLOR) forState:UIControlStateNormal];
}

-(void)configureDecorationBlackTitleButtonForTouchingStatus
{
    [self setTitleColor:colorFromHexString(BUTTON_BG_COLOR_HIGHLIGHT) forState:UIControlStateHighlighted];
}

-(void)centerVertically
{
    CGSize titleSize = [self.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: self.titleLabel.font}];
    CGSize imageSize = self.imageView.image.size;
    
    CGFloat gap = 5 ;
    NSInteger sign = 1;
    
    
    self.titleEdgeInsets = UIEdgeInsetsMake((imageSize.height+gap)*sign, -imageSize.width, 0, 0);
    
    self.imageEdgeInsets = UIEdgeInsetsMake(-(titleSize.height+gap)*sign, 0, 0, -titleSize.width);
}

-(void)setUnderlineForTextWithDefaultColorForNormalStatus:(NSString*)title
{
    NSMutableAttributedString *normalString = [[NSMutableAttributedString alloc] initWithString:title];
    
    [normalString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [normalString length])];
    [self setAttributedTitle:normalString forState:UIControlStateNormal];
}


-(void)setUnderlineForTextWithCustomizedColorForTouchingStatus:(NSString*)title
{
    NSMutableAttributedString *tappedString = [[NSMutableAttributedString alloc] initWithString:title];
    [tappedString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, tappedString.length)];
    [tappedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:157.0f/255.0f green:3.0f/255.0f blue:41.0f/255.0f alpha:1.0f] range:NSMakeRange(0, tappedString.length)];
    [self setAttributedTitle:tappedString forState:UIControlStateHighlighted];
}

-(void)setUnderlineForText:(NSString*)title withColor:(UIColor *)color withStatus:(UIControlState)status
{
    NSMutableAttributedString *tappedString = [[NSMutableAttributedString alloc] initWithString:title];
    [tappedString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, tappedString.length)];
    [tappedString addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0, tappedString.length)];
    [self setAttributedTitle:tappedString forState:status];
}

-(void)setDefaultStyle
{
    [self.layer setCornerRadius:2.0f];
    [self setBackgroundColor:[UIColor whiteColor]];
    [self setTitleColor:[UIColor colorWithWhite:0.0 alpha:0.8] forState:UIControlStateNormal];
    self.titleLabel.font =[UIFont fontWithName:FONT_MarkForMC_REGULAR size:14.0 * SCREEN_SCALE_BASE_WIDTH_375];
}

-(void)setGrayColorStyle
{
    [self.layer setCornerRadius:2.0f];
    [self setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.3]];
    [self setTitleColor:[UIColor colorWithWhite:0.0 alpha:0.8] forState:UIControlStateNormal];
    self.titleLabel.font =[UIFont fontWithName:FONT_MarkForMC_REGULAR size:14.0 * SCREEN_SCALE_BASE_WIDTH_375];
}

-(void)setDisableStyle
{
    [self.layer setCornerRadius:2.0f];
    [self setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.8]];
    [self setTitleColor:[UIColor colorWithWhite:0.0 alpha:0.8] forState:UIControlStateDisabled];
    self.titleLabel.font =[UIFont fontWithName:FONT_MarkForMC_REGULAR size:14.0 * SCREEN_SCALE_BASE_WIDTH_375];
}
@end
