//
//  UITableView+Extensions.h
//  FoodyMobile
//
//  Created by Duc Trinh on 8/23/12.
//  Copyright (c) 2012 Foody Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (Extensions)

- (void)addDummyTableHeaderWithHeight:(float)height;
- (void)setDummyFooterView;
- (void)setDummyFooterViewWithText:(NSString *)text;
- (void)setDummyFooterViewWithText:(NSString *)text height:(CGFloat)footerHeight button:(UIButton *)button;
- (void)removeDummyFooterView;
//- (void)setDummyFooterViewWithImageAndText:(NSString *)text;
- (void)setDummyFooterViewWithImageAndText:(NSString *)text height:(CGFloat)footerHeight isShowReload:(BOOL)isReload;

- (void)addLoadMoreFooterView;
- (void)setLoadMoreText:(NSString *)text isDown:(BOOL)isArrowDown padding:(float)paddingNum;
- (void)hideLoadMoreText;
- (void)startLoadMoreIndicator;
- (void)startLoadMoreIndicatorWithYPos:(double)YPos;
- (void)removeLoadMoreIndicator;
- (void)stopLoadMoreIndicator;

- (void)addRefreshHeaderView;
- (void)setRefreshText:(NSString *)text isDown:(BOOL)isDown;
- (void)setRefreshText:(NSString *)text isDown:(BOOL)isDown isWhite:(BOOL)isWhiteColor;
- (void)startRefreshIndicator;
- (void)startRefreshIndicator:(BOOL)isWhite;
- (void)stopRefreshIndicator;
- (void)removeRefreshIndicator;
@end
