//
//  Common.m
//  Reservations
//
//  Created by Linh Le on 10/22/12.
//  Copyright (c) 2012 Linh Le. All rights reserved.
//

#import "Common.h"
#import "NSString+Utis.h"
#import "Constant.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "NSString+HTML.h"
#import "AppDelegate.h"
#import <sys/sysctl.h>
#import "Reachability.h"
#import "UserObject.h"
#import "WSPreferences.h"

#define SPECIAL_CHARACTER @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%&*()-+_=[]:;\",.?/ "

@implementation Common

int NUMBER_ROW_UPDATED = 20;

static NSArray *SHORT_MONTH;
ScreenSize screenSize = UNDEFINED;

ScreenSize getScreenSize()
{
    if (screenSize == UNDEFINED) {
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        if (screenBounds.size.height == 568) {
            // code for 4-inch screen
            screenSize = FOUR_INCH;
        }
        else if(screenBounds.size.height == 480) {
            // code for 3.5-inch screen
            screenSize = THREE_DOT_FIVE_INCH;
        }        
    }
    
    return screenSize;
}

NSString* getDistance(float item)
{
    // Get distance
    NSString *result;
    
    if (item >= 1000)
    {
        result = [[NSString alloc] initWithFormat:@"%.1f km", item /1000 ] ;
    }
    else
    {
        int intDistance = roundf(item);
        result = [[NSString alloc] initWithFormat:@"%d m", intDistance] ;
        
    }

    return result;
}

NSString* urlencode(NSString* url)
{
    NSMutableString * output = [NSMutableString string];
    const unsigned char * source = (const unsigned char *)[url UTF8String];
    int sourceLen = (int)strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}

NSString* getUniqueIdentifier()
{
    NSString *uDID = [[NSUserDefaults standardUserDefaults] objectForKey:@"UDID"];
    if (uDID == nil) {
        // create UDID
        CFUUIDRef theUUID = CFUUIDCreate(NULL);
        CFStringRef string = CFUUIDCreateString(NULL, theUUID);
        CFRelease(theUUID);
        uDID = (__bridge  NSString *)string;
        // store to user default
        [[NSUserDefaults standardUserDefaults] setObject:uDID forKey:@"UDID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    return uDID;
}

NSString* getPhotoUrlBySizeAndSecret(NSString * oldUrl, int width, int height, NSString *secret)
{
    if (!oldUrl || !secret) {
        return nil;
    }
    
    NSString *baseUrl = [oldUrl stringByDeletingLastPathComponent];
    NSString *photoName = [oldUrl lastPathComponent];
    NSRange dotRange = NSMakeRange(NSNotFound, 0);
    if(photoName.length > 0)
        dotRange = [photoName rangeOfString:@"."];
    NSString *photoBaseName = [photoName substringToIndex:dotRange.location];
    NSString *photoExt = [photoName substringFromIndex:dotRange.location+1];

    NSString *photoCombineInfo = [NSString stringWithFormat:@"%@_%dx%d.%@%@", photoBaseName, width, height, photoExt, secret];
    NSString *sig = [[photoCombineInfo MD5] substringToIndex:10];
    
    return [NSString stringWithFormat:@"%@/%@_%dx%d_%@.%@", baseUrl, photoBaseName, width, height, sig, photoExt];
}


NSString* reverseString(NSString *inputString)
{
    int len =(int) [inputString length];
    NSMutableString *reverseName = [[NSMutableString alloc] initWithCapacity:len];
    for(int i=len-1;i>=0;i = i - 2)
    {
        [reverseName appendString:[NSString stringWithFormat:@"%c%c",[inputString characterAtIndex:i - 1], [inputString characterAtIndex:i]]];
    }
    return reverseName;
}

// flatent html
NSString* flattenHTML(NSString *html)
{
	
    NSScanner *theScanner;
    NSString *text = nil;
	
    theScanner = [NSScanner scannerWithString:html];
	
    while ([theScanner isAtEnd] == NO) {
		
        // find start of tag
        [theScanner scanUpToString:@"<" intoString:NULL] ;
		
        // find end of tag
        [theScanner scanUpToString:@">" intoString:&text] ;
		
        // replace the found tag with a space
        //(you can filter multi-spaces out later if you wish)
        
        
        html = [html stringByReplacingOccurrencesOfString:
				[ NSString stringWithFormat:@"%@>", text]
											   withString:@" "];
        
        
		
    }
    
    return html;
}

//scale image
UIImage* scaleImageWithSize(UIImage *imagetemp ,CGSize newSize)
{
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [imagetemp drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

NSString* formatDateToString(NSDate* date, NSString*format)
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    
    return [dateFormatter stringFromDate:date];
}


NSString* formatFullDate(NSDate* date,BOOL isInAMPM)
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if (isInAMPM) {
        [dateFormatter setDateFormat:@"dd MMMM yyyy hh:mm a"];
    }
    else
    {
        [dateFormatter setDateFormat:@"dd MMMM yyyy HH:mm"];
    }
    return [dateFormatter stringFromDate:date];
}

NSString* formatFullDateForReview(NSDate* date,BOOL isInAMPM)
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if (isInAMPM) {
        [dateFormatter setDateFormat:@"dd MMMM yyyy@%@hh:mm a"];
    }
    else
    {
        [dateFormatter setDateFormat:@"dd MMMM yyyy@%@HH:mm"];
    }
    NSString* returString =[dateFormatter stringFromDate:date];
    returString = [returString stringByReplacingOccurrencesOfString:@"am" withString:@"AM"];
    returString = [returString stringByReplacingOccurrencesOfString:@"pm" withString:@"PM"];
    return returString;
}

NSString* formatDateForBooking(NSDate *date){
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    return [dateFormatter stringFromDate:date];
}

extern NSString* formatDateForContent(NSDate *date)
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMMM yyyy"];
    //[dateFormatter setDateStyle:NSDateFormatterLongStyle]; // day, Full month and year
    //[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    return [dateFormatter stringFromDate:date];
}

NSString* dateToTimeStampForBooking(NSDate *date)
{
    if (date != nil) {
        NSTimeZone *timeZone = [NSTimeZone localTimeZone];
        return [NSString stringWithFormat:@"%.0f",[date timeIntervalSince1970] + [timeZone secondsFromGMTForDate:date]];
    }
    return nil;
}

NSString* formatDateForBookingDisplay(NSDate *date){
    SHORT_MONTH = [NSArray arrayWithObjects:@"Jan",@"Feb",@"Mar",@"Apr",@"May", @"Jun", @"Jul", @"Aug",@"Sep", @"Oct",@"Nov", @"Dec", nil];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    NSDateFormatter *d = [[NSDateFormatter alloc] init];
    [d setDateFormat:@"EEE"];
    NSString *day = [d stringFromDate:date];
    [d setDateFormat:@"dd"];
    NSString *dateInMonth = [d stringFromDate:date];
    return [NSString stringWithFormat:@"%@, %@ %@", day, [SHORT_MONTH objectAtIndex:[components month]-1], dateInMonth];
}

NSString* formatDate(NSDate* date, NSString *format){
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    return [dateFormatter stringFromDate:date];

}

NSDate* adjustDateToFirstTime(NSDate* date){
    NSDate *adjustedDate;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMMM yyyy"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    
    [dateFormatter setFormatterBehavior:NSDateFormatterBehaviorDefault];
    [dateFormatter setDateFormat:@"dd MMMM yyyy HH:mm"];
    adjustedDate = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@ %@", formattedDate, @"08:00"]];
    
    return adjustedDate;
}

NSString* formatTimeForSearch(NSDate *date){
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    NSString *time = [formatter stringFromDate:date];
    if(time.length > 0){
        NSString *firstChar = [time substringToIndex:1];
        if([firstChar isEqualToString:@"0"]){
            time = [time substringFromIndex:1];
        }
    }
    return time;
}

NSString* formatTimeSlot(NSInteger timeSlotId)
{
    if(timeSlotId == 0){
        return @"00:00";
    }
    
    NSMutableString *timeSlot = [NSMutableString stringWithFormat:@"%ld", (long)timeSlotId];
    if([timeSlot length] <=2 ){
        [timeSlot insertString:@"0:" atIndex:0];
    }
    else if ([timeSlot length] == 3) {
        [timeSlot insertString:@":" atIndex:1];
    }
    else
    {
        [timeSlot insertString:@":" atIndex:2];
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehaviorDefault];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSDate *intime = [dateFormatter dateFromString:timeSlot];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *time = [[dateFormatter stringFromDate:intime] uppercaseString];

    if(time.length > 0){
        NSString *firstChar = [time substringToIndex:1];
        if([firstChar isEqualToString:@"0"]){
            time = [time substringFromIndex:1];
        }
    }
    return time;
}

// return distance day
NSString* getDistanceDay(NSDate* starDay)
{
    int interval = [[NSDate date] timeIntervalSinceDate:starDay];
    
    NSString *timeUnit;
    int timeValue = 0;
    if(timeValue<0){
        timeValue = interval;
        timeUnit = @"just now";
    }else  if (interval < 60)
    {
        timeValue = interval;
        timeUnit = @"seconds";
    }
    else if (interval< 3600)
    {
        timeValue = round(interval / 60);
        
        if (timeValue == 1) {
            timeUnit = @"minute";
        } else {
            timeUnit = @"minutes";
        }
    }
    else if (interval< 86400) {
        timeValue = round(interval / 60 / 60);
        
        if (timeValue == 1) {
            timeUnit = @"hour";
            
        } else {
            timeUnit = @"hours";
        }
    }
    else if (interval < 604800)
    {
        int diff = round(interval / 60 / 60 / 24);
        if (diff == 1)
        {
            timeUnit = @"yesterday";
        }
        else if (diff == 7)
        {
            timeUnit = @"last week";
        }
        else
        {
            timeValue = diff;
            timeUnit = @"days";
        }
    }
    else
    {
        int days = round(interval / 60 / 60 / 24);
        
        if (days < 7) {
            
            timeValue = days;
            
            if (timeValue == 1) {
                timeUnit = @"day";
            } else {
                timeUnit = @"days";
            }
            
        } else if (days < 30) {
            int weeks = days / 7;
            
            timeValue = weeks;
            
            if (timeValue == 1) {
                timeUnit = @"week";
            } else {
                timeUnit = @"weeks";
            }
            
        } else if (days < 365) {
            
            int months = days / 30;
            timeValue = months;
            
            if (timeValue == 1) {
                timeUnit = @"month";
            } else {
                timeUnit = @"months";
            }
            
        } else if (days < 30000)
        {
            // this is roughly 82 years. After that, we'll say 'forever'
            int years = days / 365;
            timeValue = years;
            
            if (timeValue == 1) {
                timeUnit = @"year";
            } else {
                timeUnit = @"years";
            }
            
        } else {
            return @"forever ago";
        }
    }
    
    if(timeValue == 0)
    {
        return [NSString stringWithFormat:@"%@", timeUnit];
    }
    return [NSString stringWithFormat:@"%d %@ ago", timeValue, timeUnit];
}

// return 64 base encode
NSString* base64forData(NSData* theData)
{
    
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

NSMutableAttributedString* letterSpacing(float spacing, NSString* string){
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    [attributedString addAttribute:NSKernAttributeName
                             value:@(spacing)
                             range:NSMakeRange(0, [string length] - 1)];
    
    return attributedString;
}


NSDate* convertUnixTimeToDate(double timestamp)
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];
    return date;
}

float iosVersion()
{
    NSString *ver = [[UIDevice currentDevice] systemVersion];
    float ver_float = [ver floatValue];
    return ver_float;
}

BOOL isSameDay(NSDate* date1, NSDate*date2)
{
    if (date1 == nil || date2 == nil) {
        return NO;
    }
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}


NSString *formatHTMLString(NSString *htmlString, NSString * tag, int width, int height){
    
    NSRegularExpression *regularExpression = [NSRegularExpression regularExpressionWithPattern:[NSString stringWithFormat:@"<%@%@",tag,@"[^>]+(/>|>){1}"] options:0 error:nil];
    NSArray *matchResult = [regularExpression matchesInString:htmlString options:0 range:NSMakeRange(0, [htmlString length])];
    if(matchResult && (matchResult.count>0)){
        NSRegularExpression *regularWidth = [NSRegularExpression regularExpressionWithPattern:@"width=\"[0-9]+" options:0 error:nil];
        NSRegularExpression *regularHeight = [NSRegularExpression regularExpressionWithPattern:@"height=\"[0-9]+" options:0 error:nil];

        NSString *matchNumber;
        NSString *matchedItem;
        NSTextCheckingResult *tempRange;
        NSMutableArray *matchedStringResults = [NSMutableArray array];
        int number;
        for (NSTextCheckingResult *textRange in matchResult) {
//            textRange = [matchResult objectAtIndex:i];
            matchedItem = [htmlString substringWithRange:textRange.range];
            // find width to replace values. If havent got these attributes, add them in
            //find Width
            tempRange = [regularWidth firstMatchInString:matchedItem options:0 range:NSMakeRange(0, matchedItem.length)];
            matchNumber = [matchedItem substringWithRange:tempRange.range];
            if (matchNumber && (matchNumber.length > 0)) {
                number  = [[matchNumber stringByReplacingOccurrencesOfString:@"width=\"" withString:@""] intValue];
                if (number > width) {
                    matchedItem = [matchedItem stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"width=\"%d", number] withString:[NSString stringWithFormat:@"width=\"%d", width]];
                }
            } else {
                matchedItem = [matchedItem stringByReplacingOccurrencesOfString:tag withString:[NSString stringWithFormat:@"%@ width=\"%d\"", tag, width]];
            }
            
            //find height
            if(height > 0){
                tempRange = [regularHeight firstMatchInString:matchedItem options:0 range:NSMakeRange(0, matchedItem.length)];
                matchNumber = [matchedItem substringWithRange:tempRange.range];
                if (matchNumber && (matchNumber.length > 0)) {
                    number  = [[matchNumber stringByReplacingOccurrencesOfString:@"height=\"" withString:@""] intValue];
                    if (number > height) {
                        matchedItem = [matchedItem stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"height=\"%d", number] withString:[NSString stringWithFormat:@"height=\"%d", height]];
                    }
                } else {
                    matchedItem = [matchedItem stringByReplacingOccurrencesOfString:tag withString:[NSString stringWithFormat:@"%@ height=\"%d\"", tag, height]];
                }
                
            }
            [matchedStringResults addObject:[NSArray arrayWithObjects:[htmlString substringWithRange:textRange.range], matchedItem, nil]];
        }                  

        for (int i=0; i<matchedStringResults.count;i++){
            htmlString = [htmlString stringByReplacingOccurrencesOfString:[(NSArray*)[matchedStringResults objectAtIndex:i] objectAtIndex:0] withString:[(NSArray*)[matchedStringResults objectAtIndex:i] objectAtIndex:1]];
        }

    }
    return htmlString;
}

UIImageView* createCricleMask(UIImageView *img,int radiusConner)
{
    img.layer.cornerRadius = radiusConner;
    img.layer.masksToBounds = YES;
    return img;
}

NSDate* countDateNextMonth(int month){
    NSCalendar *cal = [[NSCalendar alloc ] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSTimeZone *zone = [NSTimeZone timeZoneWithName:@"GMT"];
    [cal setTimeZone:zone];
    NSDateComponents *components = [[NSDateComponents alloc] init] ;
    // Update the start date
    [components setMonth:month];
    return [cal dateByAddingComponents:components toDate:[NSDate date] options:0];
}

NSDate* countDateNextHour(int hour){
    NSCalendar *cal = [[NSCalendar alloc ] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSTimeZone *zone = [NSTimeZone timeZoneWithName:@"GMT"];
    [cal setTimeZone:zone];
    NSDateComponents *components = [[NSDateComponents alloc] init] ;
    // Update the start date
    [components setHour:hour];
    return [cal dateByAddingComponents:components toDate:[NSDate date] options:0];
}

NSDate* countDateNextMinute(int minute){
    NSCalendar *cal = [[NSCalendar alloc ] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSTimeZone *zone = [NSTimeZone timeZoneWithName:@"GMT"];
    [cal setTimeZone:zone];
    NSDateComponents *components = [[NSDateComponents alloc] init] ;

    [components setMinute:minute];
    return [cal dateByAddingComponents:components toDate:[NSDate date] options:0];
}

NSString * removeZeroTrail(NSString * stringSource)
{
    // This function use to remove zero trail in latitude and longitude to prevent error signature encode
    int lenStringSource = (int)[stringSource length];
    NSString *lastChar = [stringSource substringFromIndex:lenStringSource -1];
    if ([lastChar isEqualToString:@"0"])
    {
        return removeZeroTrail([stringSource substringToIndex:lenStringSource -1]);
    }
    else
    {
        return stringSource;
    }
}
NSString* getMonthName(NSDate *date){
    SHORT_MONTH = [NSArray arrayWithObjects:@"JANUARY",@"FEBRUARY",@"MARCH",@"APRIL",@"MAY", @"JUNE", @"JULY", @"AUGUST",@"SEPTEMBER", @"OCTOBER",@"NOVEMBER", @"DECEMBER", nil];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    NSLog(@"%@",[NSString stringWithFormat:@"%@", [SHORT_MONTH objectAtIndex:[components month]-1]]);
    return [NSString stringWithFormat:@"%@", [SHORT_MONTH objectAtIndex:[components month]-1]];
    
    
}
NSString* getMonthNamePromotion(NSDate *date){
    SHORT_MONTH = [NSArray arrayWithObjects:@"January",@"February",@"March",@"April",@"May", @"June", @"July", @"August",@"September", @"October",@"November", @"December", nil];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    NSLog(@"%@",[NSString stringWithFormat:@"%@", [SHORT_MONTH objectAtIndex:[components month]-1]]);
    return [NSString stringWithFormat:@"%@", [SHORT_MONTH objectAtIndex:[components month]-1]];
    
    
}
NSString* getDayOfWeek(NSDate *date)
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat:@"EEEE"];
    NSLog(@"%@", [dateFormatter stringFromDate:date]);
    return [dateFormatter stringFromDate:date];
}

NSString* getDateOfMonth(NSDate *date)
{
     NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    switch ([components day]) {
        case 1:
//        case 11:
        case 21:
        case 31:
            return @"ST";
            break;
        case 2:
//        case 12:
        case 22:
            return @"ND";
            break;
        case 3:
//        case 13:
        case 23:
            return @"RD";
        default:
            break;
    }
    return @"TH";
}

NSString* getHourOfDate(NSDate *date)
{
     NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:date];
    int hour =(int) [components hour];
    if(hour > 12)
    {
        return [NSString stringWithFormat:@"%liPM",[components hour] - 12];
    }
    return [NSString stringWithFormat:@"%liAM",(long)[components hour]];
}
CGSize getSizeWithFont(NSString *string,UIFont *font)
{
    CGSize contentSize;
    if(iosVersion() < 7.0)
    {
        contentSize=[string sizeWithFont:font constrainedToSize:CGSizeMake(320, 999) lineBreakMode:NSLineBreakByWordWrapping];
    }
    else
    {
        
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    font,NSFontAttributeName,NSLineBreakByWordWrapping,NSParagraphStyleAttributeName, nil];
        
        CGRect rect = [string boundingRectWithSize:CGSizeMake(320, 999) options:NSStringDrawingUsesLineFragmentOrigin  attributes:attributes context:nil ];
        contentSize =rect.size;
        
    }
    return contentSize;
}

CGSize getSizeWithFontAndFrame(NSString *string,UIFont *font,int width,int height)
{
    CGSize contentSize;
    if(iosVersion() < 7.0)
    {
        contentSize=[string sizeWithFont:font constrainedToSize:CGSizeMake(width, height) lineBreakMode:NSLineBreakByWordWrapping];
    }
    else
    {
        
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    font,NSFontAttributeName,NSLineBreakByWordWrapping,NSParagraphStyleAttributeName, nil];
        
        CGRect rect = [string boundingRectWithSize:CGSizeMake(width, height) options:NSStringDrawingUsesLineFragmentOrigin  attributes:attributes context:nil ];
        contentSize =rect.size;
        
    }
    return contentSize;
}


NSString *currentDate(){
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE, MMMM dd"];
    return [dateFormatter stringFromDate:[NSDate date]];
}


NSString* stringByStrippingHTML(NSString* data) {
    NSRange r;
    NSString *s = data;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound){
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    }
    
    s = [s stringByDecodingHTMLEntities];
    return s;
}


void resetScaleViewBaseOnScreen(UIView* view){
    if(view == nil){
        return;
    }
    
    if([view isKindOfClass:[UITextField class]]){
        NSLayoutConstraint *heightConstraint = getHeightConstranint(view);
        if(heightConstraint!=nil){
            heightConstraint.constant = heightConstraint.constant*SCREEN_SCALE;
        }
        
        NSLayoutConstraint *widthConstraint = getWidthConstraint(view);
        if(widthConstraint)
        {
            widthConstraint.constant = widthConstraint.constant * SCREEN_SCALE;
        }
        
        UIFont* font = ((UITextField*) view).font;
        [((UITextField*) view) setFont:[UIFont fontWithName:font.fontName size:font.pointSize*SCREEN_SCALE]];
    } else if([view isKindOfClass:[UITextView class]]){
        NSLayoutConstraint *heightConstraint = getHeightConstranint(view);
        if(heightConstraint!=nil){
            heightConstraint.constant = heightConstraint.constant*SCREEN_SCALE;
        }
        UIFont* font = ((UITextView*) view).font;
        [((UITextView*) view) setFont:[UIFont fontWithName:font.fontName size:font.pointSize*SCREEN_SCALE]];
    } else if([view isKindOfClass:[UIButton class]]){
        NSLayoutConstraint *heightConstraint = getHeightConstranint(view);
        if(heightConstraint!=nil){
            heightConstraint.constant = heightConstraint.constant*SCREEN_SCALE;
        }
        
        NSLayoutConstraint *widthConstraint = getWidthConstraint(view);
        if(widthConstraint)
        {
            widthConstraint.constant = widthConstraint.constant * SCREEN_SCALE;
        }
        
        UIFont* font = ((UIButton*) view).titleLabel.font;
        [((UIButton*) view).titleLabel setFont:[UIFont fontWithName:font.fontName size:font.pointSize*SCREEN_SCALE]];
    } else if([view isKindOfClass:[UILabel class]]){
        NSLayoutConstraint *heightConstraint = getHeightConstranint(view);
        if(heightConstraint!=nil){
            heightConstraint.constant = heightConstraint.constant*SCREEN_SCALE;
        }
        
        NSLayoutConstraint *widthConstraint = getWidthConstraint(view);
        if(widthConstraint)
        {
            widthConstraint.constant = widthConstraint.constant * SCREEN_SCALE;
        }
        UIFont* font = ((UILabel*) view).font;
        [((UILabel*) view) setFont:[UIFont fontWithName:font.fontName size:font.pointSize*SCREEN_SCALE]];
    }
    else if([view isKindOfClass:[UIImageView class]]){
        NSLayoutConstraint *heightConstraint = getHeightConstranint(view);
        if(heightConstraint!=nil){
            heightConstraint.constant = heightConstraint.constant*SCREEN_SCALE;
        }
        
        NSLayoutConstraint *widthConstraint = getWidthConstraint(view);
        if(widthConstraint)
        {
            widthConstraint.constant = widthConstraint.constant * SCREEN_SCALE;
        }
    }
    
    else if(([view isKindOfClass:[UIView class]] || [view isKindOfClass:[UIScrollView class]] || [view isKindOfClass:[UIStackView class]])){
        NSLayoutConstraint *heightConstraint = getHeightConstranint(view);
        if(heightConstraint!=nil && heightConstraint.constant > 2){
            heightConstraint.constant = heightConstraint.constant*SCREEN_SCALE;
        }
        
        NSLayoutConstraint *widthConstraint = getWidthConstraint(view);
        if(widthConstraint && widthConstraint.constant > 2)
        {
            widthConstraint.constant = widthConstraint.constant * SCREEN_SCALE;
        }
        for (UIView* sub in [view subviews]) {
            resetScaleViewBaseOnScreen(sub);
        }
    }
}

void resetScaleIndividualViewBaseOnScreenSize(UIView *view)
{
    NSLayoutConstraint *heightConstraint = getHeightConstranint(view);
    if(heightConstraint!=nil){
        heightConstraint.constant = heightConstraint.constant*SCREEN_SCALE;
    }
    
    NSLayoutConstraint *widthConstraint = getWidthConstraint(view);
    if(widthConstraint)
    {
        widthConstraint.constant = widthConstraint.constant * SCREEN_SCALE;
    }
    
    NSLayoutConstraint *topConstraint = getTopConstraint(view);
    if(topConstraint)
    {
        topConstraint.constant = topConstraint.constant * SCREEN_SCALE;
    }
    
    NSLayoutConstraint *bottomConstraint = getBottomConstraint(view);
    if(bottomConstraint)
    {
        bottomConstraint.constant = bottomConstraint.constant * SCREEN_SCALE;
    }
}


NSLayoutConstraint* getHeightConstranint(UIView* view){
    NSLayoutConstraint *heightConstraint;
    for (NSLayoutConstraint *constraint in view.constraints) {
        if (constraint.firstAttribute == NSLayoutAttributeHeight) {
            return constraint;
        }
    }
    return heightConstraint;
}

NSLayoutConstraint* getWidthConstraint(UIView *view)
{
    NSLayoutConstraint *widthConstraint;
    for (NSLayoutConstraint *constraint in view.constraints) {
        if (constraint.firstAttribute == NSLayoutAttributeWidth) {
            return constraint;
        }
    }
    return widthConstraint;
}

NSLayoutConstraint* getTopConstraint(UIView *view)
{
    NSLayoutConstraint *topConstraint;
    for (NSLayoutConstraint *constraint in view.superview.constraints) {
        if ((constraint.firstItem == view && constraint.firstAttribute == NSLayoutAttributeTop) || (constraint.secondItem == view && constraint.secondAttribute == NSLayoutAttributeTop)) {
            return constraint;
        }
    }
    return topConstraint;
}

NSLayoutConstraint* getBottomConstraint(UIView *view)
{
    NSLayoutConstraint *bottomConstraint;
    for (NSLayoutConstraint *constraint in view.constraints) {
        if ((constraint.firstItem == view && constraint.firstAttribute == NSLayoutAttributeBottom) || (constraint.secondItem == view && constraint.secondAttribute == NSLayoutAttributeBottom)) {
            return constraint;
        }
    }
    return bottomConstraint;
}

NSDate* convertStringToDate(NSString* dateString, NSString* dateFormat){
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    return [dateFormatter dateFromString:dateString];
}

NSDate* convertStringToDateWithTimeZone(NSString* dateString, NSString* dateFormat, NSString* timezone){
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:timezone];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:dateFormat];
    return [dateFormatter dateFromString:dateString];
}


BOOL isValidDateTimeWithin24h(NSDate* date, NSDate* time){
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components1 = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:time];
    NSInteger hour = [components1 hour];
    NSInteger minute = [components1 minute];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorian components: NSUIntegerMax fromDate: date];
    [components setHour: hour];
    [components setMinute: minute];
    [components setSecond: 0];
    NSDate *fullReservation = [gregorian dateFromComponents: components];
    
    if([fullReservation compare:[[NSDate date] dateByAddingTimeInterval:60*60*24*1]] == NSOrderedAscending){
        return NO;
    }
    return YES;
}

BOOL isWithin24h(NSDate* date1, NSDate* date2){
    if([date1 compare:[date2 dateByAddingTimeInterval:60*60*24*1]] == NSOrderedAscending){
        return YES;
    }
    return NO;
}
BOOL isMoreDateh(NSDate* date1, NSDate* date2){
    if([date1 compare:date2] == NSOrderedAscending){
        return YES;
    }
    return NO;
}


UIColor* colorFromHexString(NSString *hexString)
{
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

UIColor* colorFromHexStringWithAlpha(NSString *hexString, CGFloat alpha)
{
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:alpha];
}

NSAttributedString* handleHTMLString(NSString*str, NSString *fontName, CGFloat pointSize, NSString *textColor){
    //    str = [self stringByStrippingHTMLWithString:str];
    str = [str stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; text-align:justify;font-size:%fpx; color: %@}</style>",fontName,pointSize, textColor]];
    
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [str dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                        NSFontAttributeName: [UIFont fontWithName:fontName size:pointSize]}
                                            documentAttributes: nil
                                            error: nil
                                            ];
    return attributedString;
}

NSString* justifyHTMLString(NSString*text, NSString *fontName, CGFloat pointSize, NSString *textColor)
{
    return [NSString stringWithFormat:@"<html><head></head><body style=\"font-family: '%@';text-align:justify; font-size:%fpx; color: %@\"> %@ </body></html>",fontName,pointSize, textColor, text];
}

NSString* centerHTMLString(NSString*text, NSString *fontName, CGFloat pointSize, NSString *textColor)
{
    return [NSString stringWithFormat:@"<html><head></head><body style=\"font-family: '%@';text-align:center; font-size:%fpx; color: %@\"> %@ </body></html>",fontName,pointSize, textColor, text];
}
//haven't review

NSString* stringFromObject(id object)
{
    if (object) {
        if ([object isKindOfClass:[NSString class]]) {
            return object;
        } else if ([object isKindOfClass:[NSNull class]]) {
            return @"";
        }
        else if ([object isKindOfClass:[NSNumber class]]) {
            return [object stringValue];
        }
        return [object description];
    }
    return @"";
}

NSString* encodeURLFromString(NSString *string){
    return [string stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];

}

CGPoint getPositionView(UIView* view){
    CGPoint superPoint = view.frame.origin;
    UIView *superView = view.superview;
    while (superView != nil) {
        superPoint.x = superPoint.x + superView.frame.origin.x;
        superPoint.y = superPoint.y + superView.frame.origin.y;
        superView = superView.superview;
    }
    return superPoint;

}

#pragma mark - Get Top Controller


BOOL isNetworkAvailable()
{
    Reachability *reachability = [Reachability reachabilityWithHostname:@"www.google.com"];
    [reachability startNotifier];
    
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if(status == NotReachable)
    {
        //No internet
        [reachability stopNotifier];
        
        return NO;
    }
    else if (status == ReachableViaWiFi)
    {
        //WiFi
        [reachability stopNotifier];
        
        return YES;
    }
    else if (status == ReachableViaWWAN)
    {
        //3G
        [reachability stopNotifier];
        
        return YES;
    }
    [reachability stopNotifier];
    
    return YES;
}


#pragma mark - Check Device

NSString* platformType(NSString *platform)
{
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"iPhone 4 (Verizon)";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5 (GSM)";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM)";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s (GSM)";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    if ([platform isEqualToString:@"iPhone8,1"])    return @"iPhone 6s";
    if ([platform isEqualToString:@"iPhone8,2"])    return @"iPhone 6s Plus";
    if ([platform isEqualToString:@"iPhone8,4"])    return @"iPhone SE";
    if ([platform isEqualToString:@"iPhone9,1"])    return @"iPhone 7";
    if ([platform isEqualToString:@"iPhone9,3"])    return @"iPhone 7";
    if ([platform isEqualToString:@"iPhone9,2"])    return @"iPhone 7 Plus";
    if ([platform isEqualToString:@"iPhone9,4"])    return @"iPhone 7 Plus";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    if ([platform isEqualToString:@"iPod6,1"])      return @"iPod Touch 6G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad 1G";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (Wi-Fi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2 (Wi-Fi)";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini (Wi-Fi)";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini (GSM)";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (Wi-Fi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3 (GSM)";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4 (Wi-Fi)";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4 (GSM)";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air (Wi-Fi)";
    if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air (Cellular)";
    if ([platform isEqualToString:@"iPad4,3"])      return @"iPad Air (China)";
    if ([platform isEqualToString:@"iPad4,4"])      return @"iPad Mini 2G (Wi-Fi)";
    if ([platform isEqualToString:@"iPad4,5"])      return @"iPad Mini 2G (Cellular)";
    if ([platform isEqualToString:@"iPad4,6"])      return @"iPad Mini 2G (China)";
    if ([platform isEqualToString:@"iPad4,7"])      return @"iPad Mini 3 (Wi-Fi)";
    if ([platform isEqualToString:@"iPad4,8"])      return @"iPad Mini 3 (Cellular)";
    if ([platform isEqualToString:@"iPad4,9"])      return @"iPad Mini 3 (China)";
    if ([platform isEqualToString:@"iPad5,1"])      return @"iPad Mini 4 (Wi-Fi)";
    if ([platform isEqualToString:@"iPad5,2"])      return @"iPad Mini 4 (Cellular)";
    if ([platform isEqualToString:@"iPad5,3"])      return @"iPad Air 2 (Wi-Fi)";
    if ([platform isEqualToString:@"iPad5,4"])      return @"iPad Air 2 (Cellular)";
    if ([platform isEqualToString:@"iPad6,3"])      return @"iPad Pro 9.7\" (Wi-Fi)";
    if ([platform isEqualToString:@"iPad6,4"])      return @"iPad Pro 9.7\" (Cellular)";
    if ([platform isEqualToString:@"iPad6,7"])      return @"iPad Pro 12.9\" (Wi-Fi)";
    if ([platform isEqualToString:@"iPad6,8"])      return @"iPad Pro 12.9\" (Cellular)";
    if ([platform isEqualToString:@"AppleTV2,1"])   return @"Apple TV 2G";
    if ([platform isEqualToString:@"AppleTV3,1"])   return @"Apple TV 3";
    if ([platform isEqualToString:@"AppleTV3,2"])   return @"Apple TV 3 (2013)";
    if ([platform isEqualToString:@"AppleTV5,3"])   return @"Apple TV 4";
    if ([platform isEqualToString:@"Watch1,1"])     return @"Apple Watch Series 1 (38mm, S1)";
    if ([platform isEqualToString:@"Watch1,2"])     return @"Apple Watch Series 1 (42mm, S1)";
    if ([platform isEqualToString:@"Watch2,6"])     return @"Apple Watch Series 1 (38mm, S1P)";
    if ([platform isEqualToString:@"Watch2,7"])     return @"Apple Watch Series 1 (42mm, S1P)";
    if ([platform isEqualToString:@"Watch2,3"])     return @"Apple Watch Series 2 (38mm, S2)";
    if ([platform isEqualToString:@"Watch2,4"])     return @"Apple Watch Series 2 (42mm, S2)";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    
    return platform;
}


NSString* removeSpecialCharacters(NSString* value){
    NSMutableCharacterSet *charactersToKeep = [NSMutableCharacterSet characterSetWithCharactersInString:SPECIAL_CHARACTER];
    [charactersToKeep formUnionWithCharacterSet:[NSCharacterSet alphanumericCharacterSet]];
    
    NSCharacterSet *charactersToRemove = [charactersToKeep invertedSet];
    
    NSString *trimmedReplacement = [[value componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@"" ];
    
    //NSLog(@"orrigional: %@",trimmedReplacement);
    
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\u0080-\\u0400]"
                                                                           options:NSRegularExpressionUseUnicodeWordBoundaries
                                                                             error:&error];
    trimmedReplacement = [regex stringByReplacingMatchesInString:trimmedReplacement
                                                         options:0
                                                           range:NSMakeRange(0, [trimmedReplacement length])
                                                    withTemplate:@""];
    //NSLog(@"trimmed 1: %@",trimmedReplacement);
    
    regex = [NSRegularExpression regularExpressionWithPattern:@"[\\u0500-\\uFFFF]"
                                                      options:NSRegularExpressionUseUnicodeWordBoundaries
                                                        error:&error];
    trimmedReplacement = [regex stringByReplacingMatchesInString:trimmedReplacement
                                                         options:0
                                                           range:NSMakeRange(0, [trimmedReplacement length])
                                                    withTemplate:@""];
    //NSLog(@"trimmed 2: %@",trimmedReplacement);
    
    return trimmedReplacement;
}

NSMutableDictionary* removeSpecialCharactersfromDict(NSMutableDictionary* dict){
    for (NSString* key in [dict allKeys]) {
        if([[dict objectForKey:key] isKindOfClass:[NSString class]]){
            [dict setObject:removeSpecialCharacters([dict objectForKey:key]) forKey:key];
        }
    }
    return dict;
}

BOOL isNotIncludedSpecialCharacters(NSString* text){
    return [text isEqualToString:removeSpecialCharacters(text)];
}

BOOL isJailbroken()
{
#if !(TARGET_IPHONE_SIMULATOR)
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:@"/Applications/Cydia.app"] ||
        [[NSFileManager defaultManager] fileExistsAtPath:@"/Library/MobileSubstrate/MobileSubstrate.dylib"] ||
        [[NSFileManager defaultManager] fileExistsAtPath:@"/bin/bash"] ||
        [[NSFileManager defaultManager] fileExistsAtPath:@"/usr/sbin/sshd"] ||
        [[NSFileManager defaultManager] fileExistsAtPath:@"/etc/apt"] ||
        [[NSFileManager defaultManager] fileExistsAtPath:@"/private/var/lib/apt/"] ||
        [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"cydia://package/com.example.package"]])  {
        return YES;
    }
    
    FILE *f = NULL ;
    if ((f = fopen("/bin/bash", "r")) ||
        (f = fopen("/Applications/Cydia.app", "r")) ||
        (f = fopen("/Library/MobileSubstrate/MobileSubstrate.dylib", "r")) ||
        (f = fopen("/usr/sbin/sshd", "r")) ||
        (f = fopen("/etc/apt", "r")))  {
        fclose(f);
        return YES;
    }
    fclose(f);
    
    NSError *error;
    
    NSString *filePath = [NSString stringWithFormat:@"/private/%@.txt",[[NSUUID UUID] UUIDString]];
    NSString *stringToBeWritten = @"This is a test to check jailbreak device.";
    [stringToBeWritten writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
    [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    if(error == nil)
    {
        return YES;
    }
    
#endif
    return NO;
}

/**
 strim string with pattern
 
 @param from target string
 @param pattern array pattern applied
 @return string strimed
 */
NSString* strimStringFrom(NSString* from, NSArray* pattern) {
    NSString* temp = from;
    for (NSString* p in pattern) {
        temp = [temp stringByReplacingOccurrencesOfString:p withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0,temp.length)];
    }
    return temp;
}


/**
 return list<String> satisfy with pattern
 
 @param string string need check
 @param pattern pattern you want
 @param expression type expression (follow NSRegularExpression)
 @return return array store string satisfy with pattern
 */
NSArray* getMatchesFromString(NSString* string, NSString* pattern, NSUInteger expression) {
    NSRegularExpression *regexUL = [NSRegularExpression regularExpressionWithPattern:pattern options:expression error:NULL];
    NSArray *uls = [regexUL matchesInString:string options:0 range:NSMakeRange(0, [string length])] ;
    NSMutableArray *matchesuls = [NSMutableArray arrayWithCapacity:[uls count]];
    for (NSTextCheckingResult *match in uls) {
        NSRange matchRange = [match range];
        [matchesuls addObject:[string substringWithRange:matchRange]];
    }
    return matchesuls;
}

// Chung Comment
//void trackGAICategory(NSString* category, NSString* action, NSString* label,NSNumber* value){
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//
//    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:category
//                                                          action:action
//                                                           label:label
//                                                           value:value] build]];
//}

NSArray* getCountryList(){
    NSArray *data = [NSArray arrayWithObjects:
                     [[Country alloc] init:@"Afghanistan" withCode:@"AFG" withNumber:@"93" withDesciption:@"Afghanistan (AFG) (93)"],
                     [[Country alloc] init:@"Albania" withCode:@"ALB" withNumber:@"355" withDesciption:@"Albania (ALB) (355)"],
                     [[Country alloc] init:@"Algeria" withCode:@"DZA" withNumber:@"213" withDesciption:@"Algeria (DZA) (213)"],
                     [[Country alloc] init:@"American Samoa" withCode:@"ASM" withNumber:@"1684" withDesciption:@"American Samoa (ASM) (1684)"],
                     [[Country alloc] init:@"Andorra" withCode:@"AND" withNumber:@"376" withDesciption:@"Andorra (AND) (376)"],
                     [[Country alloc] init:@"Angola" withCode:@"AGO" withNumber:@"244" withDesciption:@"Angola (AGO) (244)"],
                     [[Country alloc] init:@"Anguilla" withCode:@"AIA" withNumber:@"1264" withDesciption:@"Anguilla (AIA) (1264)"],
                     [[Country alloc] init:@"Antarctica" withCode:@"ATA" withNumber:@"672" withDesciption:@"Antarctica (ATA) (672)"],
                     [[Country alloc] init:@"Antigua and Barbuda" withCode:@"ATG" withNumber:@"1268" withDesciption:@"Antigua and Barbuda (ATG) (1268)"],
                     [[Country alloc] init:@"Argentina" withCode:@"ARG" withNumber:@"54" withDesciption:@"Argentina (ARG) (54)"],
                     [[Country alloc] init:@"Armenia" withCode:@"ARM" withNumber:@"374" withDesciption:@"Armenia (ARM) (374)"],
                     [[Country alloc] init:@"Aruba" withCode:@"ABW" withNumber:@"297" withDesciption:@"Aruba (ABW) (297)"],
                     [[Country alloc] init:@"Australia" withCode:@"AUS" withNumber:@"61" withDesciption:@"Australia (AUS) (61)"],
                     [[Country alloc] init:@"Austria" withCode:@"AUT" withNumber:@"43" withDesciption:@"Austria (AUT) (43)"],
                     [[Country alloc] init:@"Azerbaijan" withCode:@"AZE" withNumber:@"994" withDesciption:@"Azerbaijan (AZE) (994)"],
                     [[Country alloc] init:@"Bahamas" withCode:@"BHS" withNumber:@"1242" withDesciption:@"Bahamas (BHS) (1242)"],
                     [[Country alloc] init:@"Bahrain" withCode:@"BHR" withNumber:@"973" withDesciption:@"Bahrain (BHR) (973)"],
                     [[Country alloc] init:@"Bangladesh" withCode:@"BGD" withNumber:@"880" withDesciption:@"Bangladesh (BGD) (880)"],
                     [[Country alloc] init:@"Barbados" withCode:@"BRB" withNumber:@"1246" withDesciption:@"Barbados (BRB) (1246)"],
                     [[Country alloc] init:@"Belarus" withCode:@"BLR" withNumber:@"375" withDesciption:@"Belarus (BLR) (375)"],
                     [[Country alloc] init:@"Belgium" withCode:@"BEL" withNumber:@"32" withDesciption:@"Belgium (BEL) (32)"],
                     [[Country alloc] init:@"Belize" withCode:@"BLZ" withNumber:@"501" withDesciption:@"Belize (BLZ) (501)"],
                     [[Country alloc] init:@"Benin" withCode:@"BEN" withNumber:@"229" withDesciption:@"Benin (BEN) (229)"],
                     [[Country alloc] init:@"Bermuda" withCode:@"BMU" withNumber:@"1441" withDesciption:@"Bermuda (BMU) (1441)"],
                     [[Country alloc] init:@"Bhutan" withCode:@"BTN" withNumber:@"975" withDesciption:@"Bhutan (BTN) (975)"],
                     [[Country alloc] init:@"Bolivia" withCode:@"BOL" withNumber:@"591" withDesciption:@"Bolivia (BOL) (591)"],
                     [[Country alloc] init:@"Bosnia and Herzegovina" withCode:@"BIH" withNumber:@"387" withDesciption:@"Bosnia and Herzegovina (BIH) (387)"],
                     [[Country alloc] init:@"Botswana" withCode:@"BWA" withNumber:@"267" withDesciption:@"Botswana (BWA) (267)"],
                     [[Country alloc] init:@"Bouvet Island" withCode:@"BUI" withNumber:@"55" withDesciption:@"Bouvet Island (BUI) (55)"],
                     [[Country alloc] init:@"Brazil" withCode:@"BRA" withNumber:@"55" withDesciption:@"Brazil (BRA) (55)"],
                     [[Country alloc] init:@"British Indian Ocean Territory" withCode:@"BIO" withNumber:@"246" withDesciption:@"British Indian Ocean Territory (BIO) (246)"],
                     [[Country alloc] init:@"Brunei Darussalam" withCode:@"BRN" withNumber:@"673" withDesciption:@"Brunei Darussalam (BRN) (673)"],
                     [[Country alloc] init:@"Bulgaria" withCode:@"BGR" withNumber:@"359" withDesciption:@"Bulgaria (BGR) (359)"],
                     [[Country alloc] init:@"Burkina Faso" withCode:@"BFA" withNumber:@"226" withDesciption:@"Burkina Faso (BFA) (226)"],
                     [[Country alloc] init:@"Burundi" withCode:@"BDI" withNumber:@"257" withDesciption:@"Burundi (BDI) (257)"],
                     [[Country alloc] init:@"Cambodia" withCode:@"KHM" withNumber:@"855" withDesciption:@"Cambodia (KHM) (855)"],
                     [[Country alloc] init:@"Cameroon" withCode:@"CMR" withNumber:@"237" withDesciption:@"Cameroon (CMR) (237)"],
                     [[Country alloc] init:@"Canada" withCode:@"CAN" withNumber:@"1" withDesciption:@"Canada (CAN) (1)"],
                     [[Country alloc] init:@"Cape Verde" withCode:@"CPV" withNumber:@"238" withDesciption:@"Cape Verde (CPV) (238)"],
                     [[Country alloc] init:@"Cayman Islands" withCode:@"CYM" withNumber:@"1345" withDesciption:@"Cayman Islands (CYM) (1345)"],
                     [[Country alloc] init:@"Central African Republic" withCode:@"CAF" withNumber:@"236" withDesciption:@"Central African Republic (CAF) (236)"],
                     [[Country alloc] init:@"Chad" withCode:@"TCD" withNumber:@"235" withDesciption:@"Chad (TCD) (235)"],
                     [[Country alloc] init:@"Chile" withCode:@"CHL" withNumber:@"56" withDesciption:@"Chile (CHL) (56)"],
                     [[Country alloc] init:@"China" withCode:@"CHN" withNumber:@"86" withDesciption:@"China (CHN) (86)"],
                     [[Country alloc] init:@"Christmas Island" withCode:@"CHI" withNumber:@"61" withDesciption:@"Christmas Island (CHI) (61)"],
                     [[Country alloc] init:@"Cocos (Keeling) Islands" withCode:@"CKI" withNumber:@"61" withDesciption:@"Cocos (Keeling) Islands (CKI) (61)"],
                     [[Country alloc] init:@"Colombia" withCode:@"COL" withNumber:@"57" withDesciption:@"Colombia (COL) (57)"],
                     [[Country alloc] init:@"Comoros" withCode:@"COM" withNumber:@"269" withDesciption:@"Comoros (COM) (269)"],
                     [[Country alloc] init:@"Congo" withCode:@"COG" withNumber:@"242" withDesciption:@"Congo (COG) (242)"],
                     [[Country alloc] init:@"Congo, the Democratic Republic of the" withCode:@"COD" withNumber:@"243" withDesciption:@"Congo, the Democratic Republic of the (COD) (243)"],
                     [[Country alloc] init:@"Cook Islands" withCode:@"COK" withNumber:@"682" withDesciption:@"Cook Islands (COK) (682)"],
                     [[Country alloc] init:@"Costa Rica" withCode:@"CRI" withNumber:@"506" withDesciption:@"Costa Rica (CRI) (506)"],
                     [[Country alloc] init:@"Cote D'Ivoire" withCode:@"CIV" withNumber:@"225" withDesciption:@"Cote D'Ivoire (CIV) (225)"],
                     [[Country alloc] init:@"Croatia" withCode:@"HRV" withNumber:@"385" withDesciption:@"Croatia (HRV) (385)"],
                     [[Country alloc] init:@"Cuba" withCode:@"CUB" withNumber:@"53" withDesciption:@"Cuba (CUB) (53)"],
                     [[Country alloc] init:@"Cyprus" withCode:@"CYP" withNumber:@"357" withDesciption:@"Cyprus (CYP) (357)"],
                     [[Country alloc] init:@"Czech Republic" withCode:@"CZE" withNumber:@"420" withDesciption:@"Czech Republic (CZE) (420)"],
                     [[Country alloc] init:@"Denmark" withCode:@"DNK" withNumber:@"45" withDesciption:@"Denmark (DNK) (45)"],
                     [[Country alloc] init:@"Djibouti" withCode:@"DJI" withNumber:@"253" withDesciption:@"Djibouti (DJI) (253)"],
                     [[Country alloc] init:@"Dominica" withCode:@"DMA" withNumber:@"1767" withDesciption:@"Dominica (DMA) (1767)"],
                     [[Country alloc] init:@"Dominican Republic" withCode:@"DOM" withNumber:@"1809" withDesciption:@"Dominican Republic (DOM) (1809)"],
                     [[Country alloc] init:@"Ecuador" withCode:@"ECU" withNumber:@"593" withDesciption:@"Ecuador (ECU) (593)"],
                     [[Country alloc] init:@"Egypt" withCode:@"EGY" withNumber:@"20" withDesciption:@"Egypt (EGY) (20)"],
                     [[Country alloc] init:@"El Salvador" withCode:@"SLV" withNumber:@"503" withDesciption:@"El Salvador (SLV) (503)"],
                     [[Country alloc] init:@"Equatorial Guinea" withCode:@"GNQ" withNumber:@"240" withDesciption:@"Equatorial Guinea (GNQ) (240)"],
                     [[Country alloc] init:@"Eritrea" withCode:@"ERI" withNumber:@"291" withDesciption:@"Eritrea (ERI) (291)"],
                     [[Country alloc] init:@"Estonia" withCode:@"EST" withNumber:@"372" withDesciption:@"Estonia (EST) (372)"],
                     [[Country alloc] init:@"Ethiopia" withCode:@"ETH" withNumber:@"251" withDesciption:@"Ethiopia (ETH) (251)"],
                     [[Country alloc] init:@"Falkland Islands (Malvinas)" withCode:@"FLK" withNumber:@"500" withDesciption:@"Falkland Islands (Malvinas) (FLK) (500)"],
                     [[Country alloc] init:@"Faroe Islands" withCode:@"FRO" withNumber:@"298" withDesciption:@"Faroe Islands (FRO) (298)"],
                     [[Country alloc] init:@"Fiji" withCode:@"FJI" withNumber:@"679" withDesciption:@"Fiji (FJI) (679)"],
                     [[Country alloc] init:@"Finland" withCode:@"FIN" withNumber:@"358" withDesciption:@"Finland (FIN) (358)"],
                     [[Country alloc] init:@"France" withCode:@"FRA" withNumber:@"33" withDesciption:@"France (FRA) (33)"],
                     [[Country alloc] init:@"French Guiana" withCode:@"GUF" withNumber:@"594" withDesciption:@"French Guiana (GUF) (594)"],
                     [[Country alloc] init:@"French Polynesia" withCode:@"PYF" withNumber:@"689" withDesciption:@"French Polynesia (PYF) (689)"],
                     [[Country alloc] init:@"French Southern Territories" withCode:@"FST" withNumber:@"262" withDesciption:@"French Southern Territories (FST) (262)"],
                     [[Country alloc] init:@"Gabon" withCode:@"GAB" withNumber:@"241" withDesciption:@"Gabon (GAB) (241)"],
                     [[Country alloc] init:@"Gambia" withCode:@"GMB" withNumber:@"220" withDesciption:@"Gambia (GMB) (220)"],
                     [[Country alloc] init:@"Georgia" withCode:@"GEO" withNumber:@"995" withDesciption:@"Georgia (GEO) (995)"],
                     [[Country alloc] init:@"Germany" withCode:@"DEU" withNumber:@"49" withDesciption:@"Germany (DEU) (49)"],
                     [[Country alloc] init:@"Ghana" withCode:@"GHA" withNumber:@"233" withDesciption:@"Ghana (GHA) (233)"],
                     [[Country alloc] init:@"Gibraltar" withCode:@"GIB" withNumber:@"350" withDesciption:@"Gibraltar (GIB) (350)"],
                     [[Country alloc] init:@"Greece" withCode:@"GRC" withNumber:@"30" withDesciption:@"Greece (GRC) (30)"],
                     [[Country alloc] init:@"Greenland" withCode:@"GRL" withNumber:@"299" withDesciption:@"Greenland (GRL) (299)"],
                     [[Country alloc] init:@"Grenada" withCode:@"GRD" withNumber:@"1473" withDesciption:@"Grenada (GRD) (1473)"],
                     [[Country alloc] init:@"Guadeloupe" withCode:@"GLP" withNumber:@"590" withDesciption:@"Guadeloupe (GLP) (590)"],
                     [[Country alloc] init:@"Guam" withCode:@"GUM" withNumber:@"1671" withDesciption:@"Guam (GUM) (1671)"],
                     [[Country alloc] init:@"Guatemala" withCode:@"GTM" withNumber:@"502" withDesciption:@"Guatemala (GTM) (502)"],
                     [[Country alloc] init:@"Guinea" withCode:@"GIN" withNumber:@"224" withDesciption:@"Guinea (GIN) (224)"],
                     [[Country alloc] init:@"Guinea-Bissau" withCode:@"GNB" withNumber:@"245" withDesciption:@"Guinea-Bissau (GNB) (245)"],
                     [[Country alloc] init:@"Guyana" withCode:@"GUY" withNumber:@"592" withDesciption:@"Guyana (GUY) (592)"],
                     [[Country alloc] init:@"Haiti" withCode:@"HTI" withNumber:@"509" withDesciption:@"Haiti (HTI) (509)"],
                     [[Country alloc] init:@"Holy See (Vatican City State)" withCode:@"VAT" withNumber:@"379" withDesciption:@"Holy See (Vatican City State) (VAT) (379)"],
                     [[Country alloc] init:@"Honduras" withCode:@"HND" withNumber:@"504" withDesciption:@"Honduras (HND) (504)"],
                     [[Country alloc] init:@"Hong Kong" withCode:@"HKG" withNumber:@"852" withDesciption:@"Hong Kong (HKG) (852)"],
                     [[Country alloc] init:@"Hungary" withCode:@"HUN" withNumber:@"36" withDesciption:@"Hungary (HUN) (36)"],
                     [[Country alloc] init:@"Iceland" withCode:@"ISL" withNumber:@"354" withDesciption:@"Iceland (ISL) (354)"],
                     [[Country alloc] init:@"India" withCode:@"IND" withNumber:@"91" withDesciption:@"India (IND) (91)"],
                     [[Country alloc] init:@"Indonesia" withCode:@"IDN" withNumber:@"62" withDesciption:@"Indonesia (IDN) (62)"],
                     [[Country alloc] init:@"Iran, Islamic Republic of" withCode:@"IRN" withNumber:@"98" withDesciption:@"Iran, Islamic Republic of (IRN) (98)"],
                     [[Country alloc] init:@"Iraq" withCode:@"IRQ" withNumber:@"964" withDesciption:@"Iraq (IRQ) (964)"],
                     [[Country alloc] init:@"Ireland" withCode:@"IRL" withNumber:@"353" withDesciption:@"Ireland (IRL) (353)"],
                     [[Country alloc] init:@"Israel" withCode:@"ISR" withNumber:@"972" withDesciption:@"Israel (ISR) (972)"],
                     [[Country alloc] init:@"Italy" withCode:@"ITA" withNumber:@"39" withDesciption:@"Italy (ITA) (39)"],
                     [[Country alloc] init:@"Jamaica" withCode:@"JAM" withNumber:@"1876" withDesciption:@"Jamaica (JAM) (1876)"],
                     [[Country alloc] init:@"Japan" withCode:@"JPN" withNumber:@"81" withDesciption:@"Japan (JPN) (81)"],
                     [[Country alloc] init:@"Jordan" withCode:@"JOR" withNumber:@"962" withDesciption:@"Jordan (JOR) (962)"],
                     [[Country alloc] init:@"Kazakhstan" withCode:@"KAZ" withNumber:@"7" withDesciption:@"Kazakhstan (KAZ) (7)"],
                     [[Country alloc] init:@"Kenya" withCode:@"KEN" withNumber:@"254" withDesciption:@"Kenya (KEN) (254)"],
                     [[Country alloc] init:@"Kiribati" withCode:@"KIR" withNumber:@"686" withDesciption:@"Kiribati (KIR) (686)"],
                     [[Country alloc] init:@"North Korea" withCode:@"PRK" withNumber:@"850" withDesciption:@"North Korea (PRK) (850)"],
                     [[Country alloc] init:@"Korea, Republic of" withCode:@"KOR" withNumber:@"82" withDesciption:@"Korea, Republic of (KOR) (82)"],
                     [[Country alloc] init:@"Kuwait" withCode:@"KWT" withNumber:@"965" withDesciption:@"Kuwait (KWT) (965)"],
                     [[Country alloc] init:@"Kyrgyzstan" withCode:@"KGZ" withNumber:@"996" withDesciption:@"Kyrgyzstan (KGZ) (996)"],
                     [[Country alloc] init:@"Laos" withCode:@"LAO" withNumber:@"856" withDesciption:@"Laos (LAO) (856)"],
                     [[Country alloc] init:@"Latvia" withCode:@"LVA" withNumber:@"371" withDesciption:@"Latvia (LVA) (371)"],
                     [[Country alloc] init:@"Lebanon" withCode:@"LBN" withNumber:@"961" withDesciption:@"Lebanon (LBN) (961)"],
                     [[Country alloc] init:@"Lesotho" withCode:@"LSO" withNumber:@"266" withDesciption:@"Lesotho (LSO) (266)"],
                     [[Country alloc] init:@"Liberia" withCode:@"LBR" withNumber:@"231" withDesciption:@"Liberia (LBR) (231)"],
                     [[Country alloc] init:@"Libyan Arab Jamahiriya" withCode:@"LBY" withNumber:@"218" withDesciption:@"Libyan Arab Jamahiriya (LBY) (218)"],
                     [[Country alloc] init:@"Liechtenstein" withCode:@"LIE" withNumber:@"423" withDesciption:@"Liechtenstein (LIE) (423)"],
                     [[Country alloc] init:@"Lithuania" withCode:@"LTU" withNumber:@"370" withDesciption:@"Lithuania (LTU) (370)"],
                     [[Country alloc] init:@"Luxembourg" withCode:@"LUX" withNumber:@"352" withDesciption:@"Luxembourg (LUX) (352)"],
                     [[Country alloc] init:@"Macao" withCode:@"MAC" withNumber:@"853" withDesciption:@"Macao (MAC) (853)"],
                     [[Country alloc] init:@"Macedonia (FYROM)" withCode:@"MKD" withNumber:@"389" withDesciption:@"Macedonia (FYROM) (MKD) (389)"],
                     [[Country alloc] init:@"Madagascar" withCode:@"MDG" withNumber:@"261" withDesciption:@"Madagascar (MDG) (261)"],
                     [[Country alloc] init:@"Malawi" withCode:@"MWI" withNumber:@"265" withDesciption:@"Malawi (MWI) (265)"],
                     [[Country alloc] init:@"Malaysia" withCode:@"MYS" withNumber:@"60" withDesciption:@"Malaysia (MYS) (60)"],
                     [[Country alloc] init:@"Maldives" withCode:@"MDV" withNumber:@"960" withDesciption:@"Maldives (MDV) (960)"],
                     [[Country alloc] init:@"Mali" withCode:@"MLI" withNumber:@"223" withDesciption:@"Mali (MLI) (223)"],
                     [[Country alloc] init:@"Malta" withCode:@"MLT" withNumber:@"356" withDesciption:@"Malta (MLT) (356)"],
                     [[Country alloc] init:@"Marshall Islands" withCode:@"MHL" withNumber:@"692" withDesciption:@"Marshall Islands (MHL) (692)"],
                     [[Country alloc] init:@"Martinique" withCode:@"MTQ" withNumber:@"596" withDesciption:@"Martinique (MTQ) (596)"],
                     [[Country alloc] init:@"Mauritania" withCode:@"MRT" withNumber:@"222" withDesciption:@"Mauritania (MRT) (222)"],
                     [[Country alloc] init:@"Mauritius" withCode:@"MUS" withNumber:@"230" withDesciption:@"Mauritius (MUS) (230)"],
                     [[Country alloc] init:@"Mayotte" withCode:@"MYT" withNumber:@"262" withDesciption:@"Mayotte (MYT) (262)"],
                     [[Country alloc] init:@"Mexico" withCode:@"MEX" withNumber:@"52" withDesciption:@"Mexico (MEX) (52)"],
                     [[Country alloc] init:@"Micronesia, Federated States of" withCode:@"FSM" withNumber:@"691" withDesciption:@"Micronesia, Federated States of (FSM) (691)"],
                     [[Country alloc] init:@"Moldova" withCode:@"MDA" withNumber:@"373" withDesciption:@"Moldova (MDA) (373)"],
                     [[Country alloc] init:@"Monaco" withCode:@"MCO" withNumber:@"377" withDesciption:@"Monaco (MCO) (377)"],
                     [[Country alloc] init:@"Mongolia" withCode:@"MNG" withNumber:@"976" withDesciption:@"Mongolia (MNG) (976)"],
                     [[Country alloc] init:@"Montserrat" withCode:@"MSR" withNumber:@"1664" withDesciption:@"Montserrat (MSR) (1664)"],
                     [[Country alloc] init:@"Morocco" withCode:@"MAR" withNumber:@"212" withDesciption:@"Morocco (MAR) (212)"],
                     [[Country alloc] init:@"Mozambique" withCode:@"MOZ" withNumber:@"258" withDesciption:@"Mozambique (MOZ) (258)"],
                     [[Country alloc] init:@"Myanmar" withCode:@"MMR" withNumber:@"95" withDesciption:@"Myanmar (MMR) (95)"],
                     [[Country alloc] init:@"Namibia" withCode:@"NAM" withNumber:@"364" withDesciption:@"Namibia (NAM) (364)"],
                     [[Country alloc] init:@"Nauru" withCode:@"NRU" withNumber:@"674" withDesciption:@"Nauru (NRU) (674)"],
                     [[Country alloc] init:@"Nepal" withCode:@"NPL" withNumber:@"977" withDesciption:@"Nepal (NPL) (977)"],
                     [[Country alloc] init:@"Netherlands" withCode:@"NLD" withNumber:@"31" withDesciption:@"Netherlands (NLD) (31)"],
                     [[Country alloc] init:@"Netherlands Antilles" withCode:@"ANT" withNumber:@"599" withDesciption:@"Netherlands Antilles (ANT) (599)"],
                     [[Country alloc] init:@"New Caledonia" withCode:@"NCL" withNumber:@"687" withDesciption:@"New Caledonia (NCL) (687)"],
                     [[Country alloc] init:@"New Zealand" withCode:@"NZL" withNumber:@"64" withDesciption:@"New Zealand (NZL) (64)"],
                     [[Country alloc] init:@"Nicaragua" withCode:@"NIC" withNumber:@"505" withDesciption:@"Nicaragua (NIC) (505)"],
                     [[Country alloc] init:@"Niger" withCode:@"NER" withNumber:@"227" withDesciption:@"Niger (NER) (227)"],
                     [[Country alloc] init:@"Nigeria" withCode:@"NGA" withNumber:@"234" withDesciption:@"Nigeria (NGA) (234)"],
                     [[Country alloc] init:@"Niue" withCode:@"NIU" withNumber:@"683" withDesciption:@"Niue (NIU) (683)"],
                     [[Country alloc] init:@"Norfolk Island" withCode:@"NFK" withNumber:@"672" withDesciption:@"Norfolk Island (NFK) (672)"],
                     [[Country alloc] init:@"Northern Mariana Islands" withCode:@"MNP" withNumber:@"1670" withDesciption:@"Northern Mariana Islands (MNP) (1670)"],
                     [[Country alloc] init:@"Norway" withCode:@"NOR" withNumber:@"47" withDesciption:@"Norway (NOR) (47)"],
                     [[Country alloc] init:@"Oman" withCode:@"OMN" withNumber:@"968" withDesciption:@"Oman (OMN) (968)"],
                     [[Country alloc] init:@"Pakistan" withCode:@"PAK" withNumber:@"92" withDesciption:@"Pakistan (PAK) (92)"],
                     [[Country alloc] init:@"Palau" withCode:@"PLW" withNumber:@"680" withDesciption:@"Palau (PLW) (680)"],
                     [[Country alloc] init:@"Palestinian Territory, Occupied" withCode:@"PTO" withNumber:@"970" withDesciption:@"Palestinian Territory, Occupied (PTO) (970)"],
                     [[Country alloc] init:@"Panama" withCode:@"PAN" withNumber:@"507" withDesciption:@"Panama (PAN) (507)"],
                     [[Country alloc] init:@"Papua New Guinea" withCode:@"PNG" withNumber:@"675" withDesciption:@"Papua New Guinea (PNG) (675)"],
                     [[Country alloc] init:@"Paraguay" withCode:@"PRY" withNumber:@"595" withDesciption:@"Paraguay (PRY) (595)"],
                     [[Country alloc] init:@"Peru" withCode:@"PER" withNumber:@"51" withDesciption:@"Peru (PER) (51)"],
                     [[Country alloc] init:@"Philippines" withCode:@"PHL" withNumber:@"63" withDesciption:@"Philippines (PHL) (63)"],
                     [[Country alloc] init:@"Pitcairn" withCode:@"PCN" withNumber:@"64" withDesciption:@"Pitcairn (PCN) (64)"],
                     [[Country alloc] init:@"Poland" withCode:@"POL" withNumber:@"48" withDesciption:@"Poland (POL) (48)"],
                     [[Country alloc] init:@"Portugal" withCode:@"PRT" withNumber:@"351" withDesciption:@"Portugal (PRT) (351)"],
                     [[Country alloc] init:@"Puerto Rico" withCode:@"PRI" withNumber:@"1787" withDesciption:@"Puerto Rico (PRI) (1787)"],
                     [[Country alloc] init:@"Qatar" withCode:@"QAT" withNumber:@"974" withDesciption:@"Qatar (QAT) (974)"],
                     [[Country alloc] init:@"Reunion" withCode:@"REU" withNumber:@"262" withDesciption:@"Reunion (REU) (262)"],
                     [[Country alloc] init:@"Romania" withCode:@"ROM" withNumber:@"40" withDesciption:@"Romania (ROM) (40)"],
                     [[Country alloc] init:@"Russian Federation" withCode:@"RUS" withNumber:@"7" withDesciption:@"Russian Federation (RUS) (7)"],
                     [[Country alloc] init:@"Rwanda" withCode:@"RWA" withNumber:@"250" withDesciption:@"Rwanda (RWA) (250)"],
                     [[Country alloc] init:@"Saint Helena" withCode:@"SHN" withNumber:@"290" withDesciption:@"Saint Helena (SHN) (290)"],
                     [[Country alloc] init:@"Saint Kitts and Nevis" withCode:@"KNA" withNumber:@"1869" withDesciption:@"Saint Kitts and Nevis (KNA) (1869)"],
                     [[Country alloc] init:@"Saint Lucia" withCode:@"LCA" withNumber:@"1758" withDesciption:@"Saint Lucia (LCA) (1758)"],
                     [[Country alloc] init:@"Saint Pierre and Miquelon" withCode:@"SPM" withNumber:@"508" withDesciption:@"Saint Pierre and Miquelon (SPM) (508)"],
                     [[Country alloc] init:@"Saint Vincent and the Grenadines" withCode:@"VCT" withNumber:@"1784" withDesciption:@"Saint Vincent and the Grenadines (VCT) (1784)"],
                     [[Country alloc] init:@"Samoa" withCode:@"WSM" withNumber:@"685" withDesciption:@"Samoa (WSM) (685)"],
                     [[Country alloc] init:@"San Marino" withCode:@"SMR" withNumber:@"378" withDesciption:@"San Marino (SMR) (378)"],
                     [[Country alloc] init:@"Sao Tome and Principe" withCode:@"STP" withNumber:@"239" withDesciption:@"Sao Tome and Principe (STP) (239)"],
                     [[Country alloc] init:@"Saudi Arabia" withCode:@"SAU" withNumber:@"966" withDesciption:@"Saudi Arabia (SAU) (966)"],
                     [[Country alloc] init:@"Senegal" withCode:@"SEN" withNumber:@"211" withDesciption:@"Senegal (SEN) (211)"],
                     [[Country alloc] init:@"Serbia and Montenegro" withCode:@"SAM" withNumber:@"381" withDesciption:@"Serbia and Montenegro (SAM) (381)"],
                     [[Country alloc] init:@"Seychelles" withCode:@"SYC" withNumber:@"248" withDesciption:@"Seychelles (SYC) (248)"],
                     [[Country alloc] init:@"Sierra Leone" withCode:@"SLE" withNumber:@"232" withDesciption:@"Sierra Leone (SLE) (232)"],
                     [[Country alloc] init:@"Singapore" withCode:@"SGP" withNumber:@"65" withDesciption:@"Singapore (SGP) (65)"],
                     [[Country alloc] init:@"Slovakia" withCode:@"SVK" withNumber:@"421" withDesciption:@"Slovakia (SVK) (421)"],
                     [[Country alloc] init:@"Slovenia" withCode:@"SVN" withNumber:@"386" withDesciption:@"Slovenia (SVN) (386)"],
                     [[Country alloc] init:@"Solomon Islands" withCode:@"SLB" withNumber:@"677" withDesciption:@"Solomon Islands (SLB) (677)"],
                     [[Country alloc] init:@"Somalia" withCode:@"SOM" withNumber:@"525" withDesciption:@"Somalia (SOM) (525)"],
                     [[Country alloc] init:@"South Africa" withCode:@"ZAF" withNumber:@"27" withDesciption:@"South Africa (ZAF) (27)"],
                     [[Country alloc] init:@"South Georgia and the South Sandwich Islands" withCode:@"SGS" withNumber:@"500" withDesciption:@"South Georgia and the South Sandwich Islands (SGS) (500)"],
                     [[Country alloc] init:@"Spain" withCode:@"ESP" withNumber:@"34" withDesciption:@"Spain (ESP) (34)"],
                     [[Country alloc] init:@"Sri Lanka" withCode:@"LKA" withNumber:@"94" withDesciption:@"Sri Lanka (LKA) (94)"],
                     [[Country alloc] init:@"Sudan" withCode:@"SDN" withNumber:@"249" withDesciption:@"Sudan (SDN) (249)"],
                     [[Country alloc] init:@"Suriname" withCode:@"SUR" withNumber:@"597" withDesciption:@"Suriname (SUR) (597)"],
                     [[Country alloc] init:@"Svalbard and Jan Mayen" withCode:@"SJM" withNumber:@"47" withDesciption:@"Svalbard and Jan Mayen (SJM) (47)"],
                     [[Country alloc] init:@"Swaziland" withCode:@"SWZ" withNumber:@"268" withDesciption:@"Swaziland (SWZ) (268)"],
                     [[Country alloc] init:@"Sweden" withCode:@"SWE" withNumber:@"46" withDesciption:@"Sweden (SWE) (46)"],
                     [[Country alloc] init:@"Switzerland" withCode:@"CHE" withNumber:@"41" withDesciption:@"Switzerland (CHE) (41)"],
                     [[Country alloc] init:@"Syrian Arab Republic" withCode:@"SYR" withNumber:@"963" withDesciption:@"Syrian Arab Republic (SYR) (963)"],
                     [[Country alloc] init:@"Taiwan" withCode:@"TWN" withNumber:@"886" withDesciption:@"Taiwan (TWN) (886)"],
                     [[Country alloc] init:@"Tajikistan" withCode:@"TJK" withNumber:@"992" withDesciption:@"Tajikistan (TJK) (992)"],
                     [[Country alloc] init:@"Tanzania" withCode:@"TZA" withNumber:@"255" withDesciption:@"Tanzania (TZA) (255)"],
                     [[Country alloc] init:@"Thailand" withCode:@"THA" withNumber:@"66" withDesciption:@"Thailand (THA) (66)"],
                     [[Country alloc] init:@"Timor-Leste" withCode:@"TML" withNumber:@"670" withDesciption:@"Timor-Leste (TML) (670)"],
                     [[Country alloc] init:@"Togo" withCode:@"TGO" withNumber:@"228" withDesciption:@"Togo (TGO) (228)"],
                     [[Country alloc] init:@"Tokelau" withCode:@"TKL" withNumber:@"690" withDesciption:@"Tokelau (TKL) (690)"],
                     [[Country alloc] init:@"Tonga" withCode:@"TON" withNumber:@"676" withDesciption:@"Tonga (TON) (676)"],
                     [[Country alloc] init:@"Trinidad and Tobago" withCode:@"TTO" withNumber:@"1868" withDesciption:@"Trinidad and Tobago (TTO) (1868)"],
                     [[Country alloc] init:@"Tunisia" withCode:@"TUN" withNumber:@"216" withDesciption:@"Tunisia (TUN) (216)"],
                     [[Country alloc] init:@"Turkey" withCode:@"TUR" withNumber:@"90" withDesciption:@"Turkey (TUR) (90)"],
                     [[Country alloc] init:@"Turkmenistan" withCode:@"TKM" withNumber:@"993" withDesciption:@"Turkmenistan (TKM) (993)"],
                     [[Country alloc] init:@"Turks and Caicos Islands" withCode:@"TCA" withNumber:@"1649" withDesciption:@"Turks and Caicos Islands (TCA) (1649)"],
                     [[Country alloc] init:@"Tuvalu" withCode:@"TUV" withNumber:@"688" withDesciption:@"Tuvalu (TUV) (688)"],
                     [[Country alloc] init:@"Uganda" withCode:@"UGA" withNumber:@"256" withDesciption:@"Uganda (UGA) (256)"],
                     [[Country alloc] init:@"Ukraine" withCode:@"UKR" withNumber:@"380" withDesciption:@"Ukraine (UKR) (380)"],
                     [[Country alloc] init:@"United Arab Emirates" withCode:@"ARE" withNumber:@"971" withDesciption:@"United Arab Emirates (ARE) (971)"],
                     [[Country alloc] init:@"United Kingdom" withCode:@"GBR" withNumber:@"44" withDesciption:@"United Kingdom (GBR) (44)"],
                     [[Country alloc] init:@"United States Minor Outlying Islands" withCode:@"USM" withNumber:@"1581" withDesciption:@"United States Minor Outlying Islands (USM) (1581)"],
                     [[Country alloc] init:@"Uruguay" withCode:@"URY" withNumber:@"598" withDesciption:@"Uruguay (URY) (598)"],
                     [[Country alloc] init:@"Uzbekistan" withCode:@"UZB" withNumber:@"998" withDesciption:@"Uzbekistan (UZB) (998)"],
                     [[Country alloc] init:@"Vanuatu" withCode:@"VUT" withNumber:@"678" withDesciption:@"Vanuatu (VUT) (678)"],
                     [[Country alloc] init:@"Venezuela" withCode:@"VEN" withNumber:@"58" withDesciption:@"Venezuela (VEN) (58)"],
                     [[Country alloc] init:@"Viet Nam" withCode:@"VNM" withNumber:@"84" withDesciption:@"Viet Nam (VNM) (84)"],
                     [[Country alloc] init:@"Virgin Islands, British" withCode:@"VGB" withNumber:@"1284" withDesciption:@"Virgin Islands, British (VGB) (1284)"],
                     [[Country alloc] init:@"Virgin Islands, U.s." withCode:@"VIR" withNumber:@"1340" withDesciption:@"Virgin Islands, U.s. (VIR) (1340)"],
                     [[Country alloc] init:@"Wallis and Futuna" withCode:@"WLF" withNumber:@"681" withDesciption:@"Wallis and Futuna (WLF) (681)"],
                     [[Country alloc] init:@"Western Sahara" withCode:@"ESH" withNumber:@"212" withDesciption:@"Western Sahara (ESH) (212)"],
                     [[Country alloc] init:@"Yemen" withCode:@"YEM" withNumber:@"967" withDesciption:@"Yemen (YEM) (967)"],
                     [[Country alloc] init:@"Zambia" withCode:@"ZMB" withNumber:@"260" withDesciption:@"Zambia (ZMB) (260)"],
                     [[Country alloc] init:@"Zimbabwe" withCode:@"ZWE" withNumber:@"263" withDesciption:@"Zimbabwe (ZWE) (263)"]
                     ,nil];
    
    NSSortDescriptor *aSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"countryNumber" ascending:YES comparator:^(id obj1, id obj2) {
        
        if ([obj1 integerValue] > [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        if ([obj1 integerValue] < [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
    NSArray *sortedArray = [NSMutableArray arrayWithArray:[data sortedArrayUsingDescriptors:[NSArray arrayWithObject:aSortDescriptor]]];
    
    
//    NSSortDescriptor *sortDescriptor;
//    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"countryNumber"
//                                                 ascending:YES];
//    NSArray *sortedArray = [data sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    Country *usaCountry = [[Country alloc] init:@"United States" withCode:@"USA" withNumber:@"1" withDesciption:@"United States (USA) (1)"];
    NSMutableArray *sortedArrayWithUSA = [[NSMutableArray alloc] init];
    [sortedArrayWithUSA addObject:usaCountry];
    [sortedArrayWithUSA arrayByAddingObjectsFromArray:sortedArray];
    for (Country *country in sortedArray) {
        [sortedArrayWithUSA addObject:country];
    }
    sortedArray = sortedArrayWithUSA;
    return sortedArray;
}

extern Country* getCountryByNumber(NSString *number) {
    for (Country *iCountry in getCountryList()) {
        if ([iCountry.countryNumber isEqualToString:number]) {
            return iCountry;
        }
    }
    return nil;
}

extern Country* getCountryByCodeName(NSString *codeName) {
    for (Country *iCountry in getCountryList()) {
        if ([iCountry.countryCode isEqualToString:codeName]) {
            return iCountry;
        }
    }
    return nil;
}


extern NSDate* dateFromISO8601Time(NSString *dateString) {
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
    [dateFormater setLocale: [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    dateFormater.timeZone = [NSTimeZone timeZoneWithAbbreviation: @"UTC"];
    return [dateFormater dateFromString:dateString];
}

extern NSDate* dateFromTime(NSString *dateString) {
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormater setLocale: [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
//    dateFormater.timeZone = [NSTimeZone timeZoneWithAbbreviation: @"UTC"];
    return [dateFormater dateFromString:dateString];
}


extern NSString* stringDateWithFormat(NSString *format, NSDate *date) {
    NSDateFormatter *stringDateFormater = [[NSDateFormatter alloc] init];
    [stringDateFormater setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
//    stringDateFormater.timeZone = [NSTimeZone timeZoneWithAbbreviation: @"UTC"];
    [stringDateFormater setDateFormat:format];
    return [stringDateFormater stringFromDate:date];
}

extern NSDate* dateFromStringWithFormat(NSString *format, NSString *dateString) {
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:format];
    [dateFormater setLocale: [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
//    dateFormater.timeZone = [NSTimeZone timeZoneWithAbbreviation: @"UTC"];
    return [dateFormater dateFromString:dateString];
}

extern bool isIphoneX (){
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
        
        if ((int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
            return YES;
        }
        
    }
    
    return NO;
}

@end
