//
//  UtilStyle.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/20/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "UtilStyle.h"
#import "Common.h"
#import "Constant.h"
#import "StyleConstant.h"

@implementation UtilStyle

+(UIColor *) colorForSeparateLine
{
    return colorFromHexString(@"#253847");
}

+(NSAttributedString *) setTextStyleForTextFieldWithMessage:(NSString *)text
{
    if(text == nil) {
        return nil;
    }
    return [[NSAttributedString alloc] initWithString:text attributes:@{NSForegroundColorAttributeName: colorFromHexString(TEXTFIELD_TEXT_COLOR_STANDARD), NSFontAttributeName:[UIFont fontWithName:TEXTFIELD_FONT_NAME_STANDARD size:TEXTFIELD_FONT_SIZE_STANDARD * SCREEN_SCALE]}];
}


+(NSAttributedString *) setPlaceHolderTextStyleForTextFieldWithMessage:(NSString *)text
{
    return [[NSAttributedString alloc] initWithString:text attributes:@{NSForegroundColorAttributeName: [UIColor colorWithWhite:1.0 alpha:0.6],
        NSFontAttributeName:[UIFont fontWithName:TEXTFIELD_FONT_NAME_STANDARD size:TEXTFIELD_FONT_SIZE_STANDARD * SCREEN_SCALE]}];
}

+(NSAttributedString *) setTextStyleForTitleViewControllerWithMessage:(NSString *)text
{
    NSRange range = NSMakeRange(0, text.length);
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:text];
    [attributeString addAttribute:NSKernAttributeName value:@(1.6) range:range];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont fontWithName:TITLE_VIEW_CONTROLLER_FONT_NAME size:TITLE_VIEW_CONTROLLER_FONT_SIZE*SCREEN_SCALE] range:range];
    [attributeString addAttribute:NSForegroundColorAttributeName value:colorFromHexString(TITLE_VIEW_CONTROLLER_TEXT_COLOR) range:range];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init] ;
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    paragraphStyle.lineSpacing = TITLE_VIEW_CONTROLLER_LINE_SPACING;
    [attributeString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    
    return attributeString;
}

+(NSAttributedString *) setLargeSizeStyleForLabelWithMessage:(NSString *)text
{
    NSRange range = NSMakeRange(0, text.length);
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:text];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont fontWithName:LABEL_FONT_NAME_TITLE size:LABEL_FONT_SIZE_TITLE*SCREEN_SCALE] range:range];
    [attributeString addAttribute:NSForegroundColorAttributeName value:colorFromHexString(LABEL_COLOR_TITLE) range:range];
    
    return attributeString;
}
@end
