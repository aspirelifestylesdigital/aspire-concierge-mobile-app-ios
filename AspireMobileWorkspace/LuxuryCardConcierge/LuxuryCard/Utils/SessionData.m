//
//  SessionData.m
//  MobileConcierge
//
//  Created by user on 6/21/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "SessionData.h"
#import "Common.h"
#import "Constant.h"
#import "NSData+AESCrypt.h"
#import "NSString+AESCrypt.h"

@implementation SessionData
{
    UserObject *userObject;
}


+ (instancetype)shareSessiondata{
    static SessionData *sharedMySession = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMySession = [[self alloc] init];
    });
    return sharedMySession;
}

-(id)init {
    self = [super init];
    if (self != nil) {
        // initialize stuff here
    }
    return self;
}
/////////
//                                      UUID
/////////
- (void)setUUID:(NSString *)uuid{
    if (uuid) {
        [self setUserDefaultValue:uuid withKey:keyUUID];
    }
}
- (NSString *)UUID{
    return [self getUserDefaultValueWithKey:keyUUID];
}
/////////
//                                      OnlineMemberID
/////////
- (void) setOnlineMemberID:(NSString *)onlineMemberID{
    if (onlineMemberID) {
        [self setUserDefaultValue:onlineMemberID withKey:keyOnlineMemberID];
    }
}
- (NSString *)OnlineMemberID{
    return [self getUserDefaultValueWithKey:keyOnlineMemberID];
}

/////////
//                                      OnlineMemberDetailIDs
/////////
- (void)setOnlineMemberDetailIDs:(NSString *)onlineMemberDetailIDs{
    if (onlineMemberDetailIDs) {
        [self setUserDefaultValue:onlineMemberDetailIDs withKey:keyOnlineMemberDetailIDs];
    }
}
- (NSString *)OnlineMemberDetailIDs{
    return [self getUserDefaultValueWithKey:keyOnlineMemberDetailIDs];
}
/////////
//                                      BINNumber
/////////
- (void)setBINNumber:(NSString *)bin{
    if (bin) {
        [self setUserDefaultValue:bin withKey:keyBINNumber];
    }
}
- (NSString *)BINNumber{
    return [self getUserDefaultValueWithKey:keyBINNumber];
}
/////////
//                                      RequestToken
/////////
- (void)setRequestToken:(NSString *)requestToken{
    if (requestToken) {
        [self setUserDefaultValue:requestToken withKey:keyRequestToken];
    }
}
- (NSString *)RequestToken{
    return [self getUserDefaultValueWithKey:keyRequestToken];
}
/////////
//                                      AccessToken
/////////
- (void)setAccessToken:(NSString *)accessToken{
    if (accessToken) {
        [self setUserDefaultValue:accessToken withKey:keyAccessToken];
    }
}

- (void)setTransactionID:(NSString*)transactionID{
    if (transactionID) {
        [self setUserDefaultValue:transactionID withKey:keyTransactionID];
    }
}
- (NSString*)TransactionID{
    return [self getUserDefaultValueWithKey:keyTransactionID];
}

- (NSString *)AccessToken{
    return [self getUserDefaultValueWithKey:keyAccessToken];
}
/////////
//                                      RefreshToken
/////////
- (void) setRefreshToken:(NSString *)refreshToken{
    if (refreshToken) {
        [self setUserDefaultValue:refreshToken withKey:keyRefreshToken];
    }
}
- (NSString *)RefreshToken{
    return [self getUserDefaultValueWithKey:keyRefreshToken];
}
/////////
//                                      ExprirationTime
/////////
- (void) setExpirationTime:(NSNumber *)exprirationTime{
    if (exprirationTime) {
        [self setUserDefaultNumberValue:exprirationTime withKey:keyExprirationTime];
    }
}
- (NSNumber *)ExpirationTime{
    return [self getUserDefaultNumberValueWithKey:keyExprirationTime];
}
/////////
//                                      CurrentPolicyVersion
/////////
- (void) setCurrentPolicyVersion:(NSString *)currentPolicyVersion{
    if (currentPolicyVersion) {
        [self setUserDefaultValue:currentPolicyVersion withKey:keyCurrentPolicyVersion];
    }
}
- (NSString *)CurrentPolicyVersion{
    return [self getUserDefaultValueWithKey:keyCurrentPolicyVersion];
}

- (void)setIsUseLocation:(BOOL)isLocation {
    [[NSUserDefaults standardUserDefaults] setBool:isLocation forKey:keyIsUseLocation];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)isUseLocation {
   return [[NSUserDefaults standardUserDefaults] boolForKey:keyIsUseLocation];
}

- (void)setHasForgotPassword:(BOOL)isForgot {
    [[NSUserDefaults standardUserDefaults] setBool:isForgot forKey:[hasFGPSS_EncrypetedKey AES256DecryptWithKey:EncryptKey]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)hasForgotPassword {
    return [[NSUserDefaults standardUserDefaults] boolForKey:[hasFGPSS_EncrypetedKey AES256DecryptWithKey:EncryptKey]];
}


/////////
//                                      Userobject
/////////
- (UserObject *)UserObject{
    return  userObject;
}

- (void)setUserObjectWithDict:(NSDictionary *)dict{
    
    NSString *mobileNumberWithCode = [[dict objectForKey:keyMobileNumber] stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSString *countryCode = @"";
    NSString *mobileNumber = @"";
    
    if (mobileNumberWithCode.length > 0) {
        NSInteger index = 4;
        switch (mobileNumberWithCode.length) {
            case 1:
                index = 1;
                break;
            case 2:
                index = 2;
                break;
            case 3:
                index = 3;
                break;
            default:
                index = 4;
                break;
        }
        Country *country = [self getCountryCodeFromString:mobileNumberWithCode fromIndex:index];
        if (country) {
            countryCode = country.countryNumber;
        }
    }
    
    mobileNumber = (countryCode.length > 0) ? [mobileNumberWithCode substringFromIndex:countryCode.length]:mobileNumberWithCode;
    
    [dict setValue:mobileNumber forKey:keyMobileNumber];
    [dict setValue:countryCode forKey:keyCountryCode];
    
    NSData *dataUser = [NSKeyedArchiver archivedDataWithRootObject:dict];
    
    if (dataUser) {
        if (![self isEncryptUserData]) {
            [self setIsEncryptUserData];
        }
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
        [[NSUserDefaults standardUserDefaults] setObject:[dataUser AES256EncryptWithKey:EncryptKey] forKey:keyProfile];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (NSDictionary*)getUserInfo {
    NSData *dataUser = [[NSUserDefaults standardUserDefaults] dataForKey:keyProfile];
    if (dataUser) {
        if (![self isEncryptUserData]) {
            [self setIsEncryptUserData];
            [[NSUserDefaults standardUserDefaults] setObject:[dataUser AES256EncryptWithKey:EncryptKey] forKey:keyProfile];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }else {
            dataUser = [dataUser AES256DecryptWithKey:EncryptKey];
        }
    }
    return [NSKeyedUnarchiver unarchiveObjectWithData:dataUser];
}

- (Country*)getCountryCodeFromString:(NSString*)string fromIndex:(NSInteger)index{
    
    if (string.length > 0 && index <= 4 && index > 0) {
        
        Country *country =  getCountryByNumber([string substringWithRange:NSMakeRange(0, index)]);
        if (country) {
            return  country;
        }else{
            if (index > 1) {
                return [self getCountryCodeFromString:string fromIndex:index - 1];
            }
        }
    }
    return nil;
}


-(BOOL) isEncryptUserData
{
    return [[NSUserDefaults standardUserDefaults]
            boolForKey:@"IsEncryptUserData"];
}

-(void) setIsEncryptUserData
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:YES] forKey:@"IsEncryptUserData"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

///*
    //
///*

- (void)setUserDefaultValue:(NSString*)value withKey:(NSString*)key{
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString*)getUserDefaultValueWithKey:(NSString*)key{
    return [[NSUserDefaults standardUserDefaults]
                            stringForKey:key];
}

- (void)setUserDefaultNumberValue:(NSNumber*)value withKey:(NSString*)key{
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSNumber*)getUserDefaultNumberValueWithKey:(NSString*)key{
    return (NSNumber *)[[NSUserDefaults standardUserDefaults]
            objectForKey:key];
}


@end
