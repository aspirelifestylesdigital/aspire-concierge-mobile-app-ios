//
//  Constant.h
//  ALC
//
//  Created by Anh Tran on 8/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constant : NSObject

#define EncryptKey @"#concierge.luxury.com#91@#B8%*6EBC8045E1-84B3-E171A0C026F4@"
/**
 * More Information in URL
 **/
#define PRIVACY_POLICY_URL @"https://www.aspirelifestyles.com/en/privacy-policy"
#define MASTERCARD_REGISTER_URL @"http://www.mastercard.com/sea/consumer/world_worldelite_mastercard.html"
#define MASTERCARD_TRAVEL_URL @"https://travel.mastercard.com/travel/arc.cfm"
#define PRICELESS_URL @"https://priceless.com"
#define GOLF_URL @"https://priceless.com/golf"
#define MASTERCARD_SHARING @"https://concierge.mastercard.com/sign_in_mc.aspx?utm_source=tw&utm_medium=mcusa&utm_campaign=concierge&popup"

/*
 **** Staging
 */
/*
#define MCD_API_URL             @"https://apiservice-stg.aspirelifestyles.com/ConciergeOauthWebAPI/"
#define B2C_ConsumerKey         @"BCD"
#define B2C_ConsumerSecret      @"BCD-CA28FC02-910"
#define B2C_Callback_Url        @"urn:ietf:wg:oauth:2.0:oob"
#define B2C_VERIFICATION_CODE   @"1001"
#define B2C_MemberRefNo         @"TestMemRef127"
#define B2C_DeviceId            @"DeviceID10"
#define BIN_CODE                @"545212"
#define B2C_EPCProgramCode      @"BCD USA"

#define B2C_IA_API_URL          @"https://tools.vipdesk.com/b2cwebservices.testing/"
#define B2C_SUBDOMAIN           @"www.luxurycardconcierge.com"
#define B2C_PW                  @"6F77455D-B68B-4AFB-96F1-EAC9399D8A9F"
 */
/*
 **** Product
*/

#define MCD_API_URL           @"https://apiservice.vipdesk.com/ConciergeWebAPIProd/"
#define B2C_ConsumerKey       @"BCD"
#define B2C_ConsumerSecret    @"BCD-DB40GD12-888"
#define B2C_Callback_Url      @"bcd:jhse:wa:oauth:2.0:lkw"
#define B2C_VERIFICATION_CODE @"1001"
#define B2C_MemberRefNo       @"TestMemRef127"
#define B2C_DeviceId          @"BCDDevice"
#define BIN_CODE              @"545212"
#define B2C_EPCProgramCode    @"BLACK CARD"

#define B2C_IA_API_URL        @"https://tools.vipdesk.com/b2cwebservices/"
#define B2C_SUBDOMAIN         @"www.luxurycardconcierge.com"
#define B2C_PW                @"6F77455D-B68B-4AFB-96F1-EAC9399D8A9F"



//Define DCR api - Dev env dcrstag
#define DCR_API_URL @"https://api.aspirelifestyles.com/dcrstg/api/v1/"
#define DCR_SUBCRIPTION_VALUE @"f6dc32a970aa45b18ea23edb590a91ae"


#define DCR_SUBCRIPTION_KEY @"Ocp-Apim-Subscription-Key"
#define DCR_ITEM_SEARCH_API @"items/search"
#define DCR_ITEM_DETAILS_API @"items"


//define error code
#define B2C_UNAVAILABLE_USER        @"ENR68-2"
#define B2C_INVALID_ACCESS_TOKEN    @"ENR73-2"
#define B2C_INVALID_USER_EMAIL      @"ENR008-3"
#define B2C_INVALID_PHONE_NUMBER    @"ENR007-3"
#define B2C_LOGIN_FAIL    @"ENR70-1"


/**************************** Development API V1.0  ****************************/

//B2C API
#define GetRequestToken               @"OAuth/AuthoriseRequestToken"
#define GetAccessToken                @"OAuth/GetAccessToken"
#define RefreshAccessToken            @"OAuth/RefereshToken"

#define PostRegistration              @"ManageUsers/Registration"
#define PostUpdateRegistration        @"ManageUsers/UpdateRegistration"
#define PostUserLogin                 @"ManageUsers/Login"
#define GetUserDetailsUrl             @"ManageUsers/GetUserDetails"
#define PostForgotPasswordUrl         @"ManageUsers/ForgotPassword"
#define PostChangePasswordUrl         @"ManageUsers/ChangePassword"
#define UpdateRegistration            @"ManageUsers/UpdateRegistration"

#define GetPreferences                @"ManageUsers/GetPreference"
#define AddPreferences                @"ManageUsers/AddPreference"
#define UpdatePerferences             @"ManageUsers/UpdatePreference"

#define CreateConciergeRequestUrl     @"ManageRequest/CreateConciergeRequest"
#define UpdateConciergeRequest        @"ManageRequest/UpdateConciergeRequest"
#define GetRecentConciergeRequest     @"ManageRequest/GetRecentRequestList"
#define GetDetailConciergeRequest     @"ManageRequest/GetConciergeRequestDetails"

#define GetQuestionsAndAnswers      @"instantanswers/GetQuestionsAndAnswers"
#define GetQuestionAndAnswers       @"instantanswers/GetQuestionAndAnswers"
#define GetSubCategories            @"instantanswers/GetSubCategories"
#define GetContentFulls             @"conciergecontent/GetContentFulls"
#define GetContentFull              @"conciergecontent/GetContentFull"
#define GetTiles                    @"conciergecontent/GetTiles"
#define GetUtility                  @"utility/GetClientCopy"
#define GetDataBasedOnSearchText    @"utility/SearchIAAndCCA"
#define checkBIN                    @"utility/CheckMasterCardBIN"

//end B2C

// config user authenticate
#define userAuthenticate @"admin"
#define passAuthenticate @"Admin123!@#$"
//

/**
 * Http Methods
 **/
#define kHTTPGET    @"GET"
#define kHTTPPUT    @"PUT"
#define kHTTPPOST   @"POST"
#define kHTTPDEL    @"DELETE"

/**
 * Error Code
 **/
#define kSTATUSOK               200
#define kSTATUSTOKENEXPIRE     401

// Preference Link
#define ACCOUNT_MANAGEMENT_LINK     @"https://www.myluxurycard.com/"
#define AIRPORT_LOUNGE_ACCESS_LINK  @"https://www.prioritypass.com/"
#define LUXURY_MAGAZINE_LINK        @"http://www.luxurymagazine.com/"
#define HOTEL_TRAVEL_LINK           @"http://www.luxurymagazine.com/vipbook"


// EncryptedKey

#define TimeFP_EncryptedKey             @"JTRtpFIVK8o68ntU75WA1Q5yKVOM+DKqRFjSXSM24Dc="
#define pssWrd_EncryptedKey             @"l3JdK3PT09XfoxS3LKPwGA=="
/***
    /// Define Font
***/
FOUNDATION_EXPORT NSString *const FONT_MarkForMC_BOLD;
FOUNDATION_EXPORT NSString *const FONT_MarkForMC_MED;
FOUNDATION_EXPORT NSString *const FONT_MarkForMC_LIGHT;
FOUNDATION_EXPORT NSString *const FONT_MarkForMC_REGULAR;
FOUNDATION_EXPORT NSString *const FONT_MarkForMC_REGULAR_It;
FOUNDATION_EXPORT NSString *const FONT_MarkForMC_THIN_IT;
FOUNDATION_EXPORT NSString *const FONT_MarkForMC_BLACK;
FOUNDATION_EXPORT NSString *const FONT_SF_MED;

/// Default Color Hex
FOUNDATION_EXPORT NSString *const DEFAULT_BACKGROUND_COLOR;
FOUNDATION_EXPORT NSString *const GRAY_BACKGROUND_COLOR;
FOUNDATION_EXPORT NSString *const BLACK_COLOR;
FOUNDATION_EXPORT NSString *const WHITE_COLOR;
FOUNDATION_EXPORT NSString *const GRAY_COLOR;
FOUNDATION_EXPORT NSString *const EXPLORE_BACKGROUND_COLOR;
FOUNDATION_EXPORT NSString *const EXPLORE_HIGHLIGHT_COLOR;


FOUNDATION_EXPORT NSString *const DEFAULT_PLACEHOLDER_COLOR;
FOUNDATION_EXPORT NSString *const DEFAULT_HIGHLIGHT_COLOR;
FOUNDATION_EXPORT NSString *const DEFAULT_ALERT_MESSAGE_COLOR;
FOUNDATION_EXPORT NSString *const CALL_CONCIERGE_TITLE_TEXT_COLOR;

FOUNDATION_EXPORT NSString *const ASKCONCIERGE_BUTTON_BACKGROUND_COLOR;
FOUNDATION_EXPORT NSString *const ASKCONCIERGE_TEXTVIEW_COLOR;

FOUNDATION_EXPORT NSString *const NAVIGATION_BAR_TITLE_TEXT_COLOR;
FOUNDATION_EXPORT NSString*const TEXT_BLACK_COLOR;
#define APP_COLOR_FOR_TEXT [UIColor colorWithRed:157.0f/255.0f green:3.0f/255.0f blue:41.0f/255.0f alpha:1.0f];

/***
 *Define static key
 ***/
#define FLAG_STARTED @"0"
#define stLoginStatus  @"isUserLogin"
#define stSessionToken  @"sessionToken"

/***
 *SCREEN SIZE
 ***/
#define SCREEN_SCALE  (((float)[UIScreen mainScreen].bounds.size.width)/414.0)
#define SCREEN_SCALE_BASE_WIDTH_375  (((float)[UIScreen mainScreen].bounds.size.width)/375.0)
#define SCREEN_SCALE_BY_HEIGHT  (((float)[UIScreen mainScreen].bounds.size.width)/736.0)
#define SCREEN_WIDTH  (((float)[UIScreen mainScreen].bounds.size.width))
#define SCREEN_HEIGHT (((float)[UIScreen mainScreen].bounds.size.height))
#define IPHONE_5S (((float)[UIScreen mainScreen].bounds.size.width) == 320.0)

#define MARGIN_KEYBOARD 5.0

#define IPAD ([[[UIDevice currentDevice] model] rangeOfString:@"iPad"].location != NSNotFound)

/***
 *For Background UIActivity
 ***/
#define SPINNER_BACKGROUND_WIDTH    60
#define SPINNER_BACKGROUND_HEIGHT   60
#define SPINNER_VIEW_WIDTH      16
#define SPINNER_VIEW_HEIGHT     16
#define NAVIGATION_HEIGHT       64
/* Table constant */
#define REFRESH_INDICATOR_HEIGHT                                    20
#define TABLE_PULL_HEIGHT                                           30.0
#define TABLE_PULL_LENGTH                                           20.0


#define HEADER_COLOR [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.1]
#define FONT_SIZE_15 15.0

#define FONT_SIZE_23 23.0
#define FONT_SIZE_21 21.0
#define FONT_SIZE_18 18.0
#define FONT_SIZE_14 14.0

#define FONT_SIZE_19 19.0
#define FONT_SIZE_32 32.0
#define FONT_SIZE_13 13.0
#define FONT_SIZE_12 12.0
#define FONT_SIZE_16 16.0
#define FONT_SIZE_24 24.0
#define FONT_SIZE_26 26.0
#define FONT_SIZE_28 28.0
#define FONT_SIZE_30 30.0
#define FONT_SIZE_35 35.0
#define FONT_SIZE_20 20.0

//#define LETTER_SPACING 1.0f

#define kJpegPhotoExt                   @"jpg"
#define kPngPhotoExt                    @"png"
#define kPhotoUploadMaxSize             2097152

#define BUTTON_CORNER_RADIUS            2

/***
 *Date format
 ***/
#define DATE_FORMAT @"MMM dd yyyy"
#define DATE_SEND_REQUEST_FORMAT @"yyyy-MM-dd"
#define DATETIME_SEND_REQUEST_FORMAT @"yyyy-MM-dd HH:mm:ss"
#define TIME_FORMAT @"h:mm a"
#define TIME_SEND_REQUEST_FORMAT @"HH:mm:ss"
#define STORYBOARD_ID_LOCATION_POP_UP_VIEW_CONTROLLER @"LocationPopUpViewController"
#define DID_OPEN_LOCATION_POP_UP @"DIDOPENLOCATIONPOPUP"
#define USER_DEFAULT_YOUR_COUNTRY @"USERDEFAULTYOURCOUNTRY"
#define USER_DEFAULT_YOUR_CITY @"USERDEFAULTYOURCITY"
#define UTC_TIMEZONE @"UTC"
#define SPECIAL_CHAR @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%&*()-+_=[]:;\",.?/ "


#define DCR_ITEM_TYPE_VALUE             @[@"dining", @"accommodation",@"sightseeing_activity",@"vacation_package",@"event",@"privilege"]
#define DCR_DATE_FORMAT                 @"yyyy-MM-dd'T'HH:mm:ssZZZZ"


#define VERIFIED_BIN                    @"verifiedBINNumber"
#define CURRENT_CITY_CODE               @"currentCityCode"
#define CURRENT_CATEGORY_CODE           @"currentCategoryCode"
#define SIX_MONTH_SECONDS_TIME          (6 * 30 * 24 * 60 * 60)
#define TWEENTY_FOUR_HOUR_SECONDS_TIME  (24 * 60 * 60)
#define BIN_TEXTFIELD_TAG               1000
#define PHONE_NUMBER_TEXTFIELD_TAG      1001
#define ZIP_CODE_TEXTFIELD_TAG          1002

#define USER_CITY_LOCATION_DEVICE       @"USER_CITY_LOCATION_DEVICE"

#define LIST_ALL_TILES  @[@"Specialty Travel",@"Hotels",@"Flowers",@"Tickets",@"Golf",@"Golf Merchandise",@"Vacation Packages",@"Cruises",@"VIP Travel Services",@"Private Jet Travel",@"Transportation",@"Wine",@"Dining",@"Retail Shopping"]

#define LIST_REGIONS @[@"Africa",@"Asia Pacific",@"Latin America",@"Caribbean",@"Europe"]

//My Request Type
#define DINING_REQUEST_TYPE                     @"D Restaurant"
#define HOTEL_REQUEST_TYPE                      @"T HOTEL AND B&B"
#define OTHER_REQUEST_TYPE                      @"O Client Specific"
#define GOLF_REQUEST_TYPE                       @"S GOLF"
#define GOLF_RED_REQUEST_TYPE                   @"S Golf"
#define TRANSPORTATION_REQUEST_TYPE             @"T LIMO AND SEDAN"
#define TRANSFER_REQUEST_TYPE                   @"T Limo and Sedan"
#define ENTERTAINMENT_REQUEST_TYPE              @"E CONCERT/THEATER"
#define TOUR_REQUEST_TYPE                       @"C SIGHTSEEING/TOURS"
#define AIRPORT_SERVICE_REQUEST_TYPE            @"I AIRPORT SERVICES"
#define ENTERTAINMENT_REQUEST_TYPE_SERVICE      @"E SELF/SERVICE"
#define CRUISE_REQUEST_TYPE                     @"T CRUISE"
#define FLOWERS_REQUEST_TYPE                    @"G FLOWERS/GIFT BASKET"
#define TRAVEL_REQUEST_TYPE                     @"T OTHER"
#define VACATION_PACKAGE_REQUEST_TYPE           @"T VACATION PACKAGE"

#define DOMESTIC_PHONE_NUMBER                   @"8447242500"
#define INTERNATIONAL_PHONE_NUMBER              @"13022557444"

//Status Request

#define OPEN_STATUS  @"Open"
#define CLOSE_STATUS  @"Closed"
#define CANCEL_MODE  @"CANCEL"
#define INPROGRESS_STATUS @"InProgress"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@end
