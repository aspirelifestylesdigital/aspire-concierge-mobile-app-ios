//
//  NSString+Utis.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/5/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utis)
-(BOOL)isValidEmail;
-(BOOL)isValidPhoneNumber;
-(BOOL)isValidZipCode;
-(BOOL)isValidBinNumber;
-(BOOL)isValidPassword;
-(BOOL)isValidWeakPassword;
-(BOOL)isValidStrongPassword;
-(BOOL)isNumber;
-(NSString *)createMaskStringBeforeNumberCharacter:(NSInteger)number;
-(NSString *)createMaskForText:(BOOL)isEmail;
-(NSString *)MD5;
-(BOOL)isValidName;
-(BOOL)isValidNameWithSpecialCharacter;
-(NSString *)removeBlankFromURL;
- (NSUInteger)occurrenceCountOfCharacter:(UniChar)character;
-(NSString *)removeRedudantWhiteSpaceInText;
-(NSString *)removeRedudantNewLineInText;
-(NSString *)replaceWhiteSpacingTag;
//-(NSString *)formatDateForText;
//-(NSString *)formatDateTimeForText;
@end
