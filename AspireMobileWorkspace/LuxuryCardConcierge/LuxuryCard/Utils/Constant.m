//
//  Constant.m
//  ALC
//
//  Created by Anh Tran on 8/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "Constant.h"

/***
 /// Define Font
 ***/
NSString *const FONT_MarkForMC_BOLD = @"Roboto-Bold";
NSString *const FONT_MarkForMC_BLACK = @"Roboto-Black";
NSString *const FONT_MarkForMC_MED = @"Roboto-Medium";
NSString *const FONT_MarkForMC_LIGHT = @"Roboto-Light";
NSString *const FONT_MarkForMC_REGULAR = @"Roboto-Regular";
NSString *const FONT_MarkForMC_REGULAR_It = @"Roboto-Italic";
NSString *const FONT_MarkForMC_THIN_IT = @"Roboto-ThinItalic";
NSString *const FONT_SF_MED = @"SFUIDisplay-Medium";
//NSString *const DEFAULT_BACKGROUND_COLOR = @"#011627";
NSString *const DEFAULT_BACKGROUND_COLOR = @"#000000";
NSString *const GRAY_BACKGROUND_COLOR = @"#171717";
NSString *const BLACK_COLOR = @"#000000";
NSString *const WHITE_COLOR = @"#FFFFFF";
NSString *const GRAY_COLOR =  @"#D3D3D3";
NSString *const EXPLORE_BACKGROUND_COLOR = @"#181818";
NSString *const EXPLORE_HIGHLIGHT_COLOR = @"#2C2C2C";
NSString *const DEFAULT_PLACEHOLDER_COLOR = @"#99A1A8";
NSString *const DEFAULT_HIGHLIGHT_COLOR = @"#e1cb78";
NSString *const DEFAULT_ALERT_MESSAGE_COLOR = @"#696969";
NSString *const CALL_CONCIERGE_TITLE_TEXT_COLOR = @"#8F8E94";
NSString *const ASKCONCIERGE_BUTTON_BACKGROUND_COLOR = @"#414141";
NSString *const ASKCONCIERGE_TEXTVIEW_COLOR = @"#545454";



NSString *const TEXT_BLACK_COLOR = @"#000000";
NSString *const NAVIGATION_BAR_TITLE_TEXT_COLOR = @"#ffffff";
@implementation Constant

@end
