//
//  UITextField+Extensions.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/4/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Extensions)
-(void)setBottomBorderRedColor;
-(void)setBottomBolderDefaultColor;
-(void)setBottomBorderHightlightColor;
-(void)updateCursorPositionAtRange:(NSRange)range;
-(void)setTextStyleForText:(NSString *)text WithFontName:(NSString *)fontName WithColor:(UIColor *)color WithTextSize:(float)textSize WithTextCenter:(BOOL)isCenter WithSpacing:(float)spacing ForPlaceHolder:(BOOL)isPlaceHolder;

-(void)setDefaultStyleForTextField;
@end
