//
//  UITabBarController+HideTabBar.h
//  LuxuryCard
//
//  Created by Chung Mai on 11/9/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITabBarController (HideTabBar)

@property (nonatomic, getter=isTabBarHidden) BOOL tabBarHidden;

- (void)setTabBarHidden:(BOOL)hidden animated:(BOOL)animated;

@end
