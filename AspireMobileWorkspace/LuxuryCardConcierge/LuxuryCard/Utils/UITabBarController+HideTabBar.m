//
//  UITabBarController+HideTabBar.m
//  LuxuryCard
//
//  Created by Chung Mai on 11/9/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "UITabBarController+HideTabBar.h"
#import <objc/runtime.h>

#define kAnimationDuration .0

@implementation UITabBarController (HideTabBar)

- (BOOL)isTabBarHidden {
    return self.tabBar.frame.size.height == 0.01;
}


- (void)setTabBarHidden:(BOOL)hidden {
    
    [self setTabBarHidden:hidden animated:NO];
}


- (void)setTabBarHidden:(BOOL)hidden animated:(BOOL)animated {
    BOOL isHidden = self.tabBarHidden;
    
    if(hidden == isHidden)
        return;
    
    NSNumber *tabBarHeight = (NSNumber*)objc_getAssociatedObject(self, @"TABBAR_HEIGHT");
    if(hidden){
        if(!tabBarHeight){
            objc_setAssociatedObject(self, @"TABBAR_HEIGHT", [NSNumber numberWithFloat:self.tabBar.frame.size.height], OBJC_ASSOCIATION_RETAIN);
        }
    }

    CGRect tabBarFrame = CGRectMake(self.tabBar.frame.origin.x, self.tabBar.frame.origin.y, self.tabBar.frame.size.width, hidden ? 0.01 : tabBarHeight.floatValue);

    [UIView animateWithDuration:kAnimationDuration
                     animations:^{
                         self.tabBar.frame = tabBarFrame;
                     }
     ];
}

@end
