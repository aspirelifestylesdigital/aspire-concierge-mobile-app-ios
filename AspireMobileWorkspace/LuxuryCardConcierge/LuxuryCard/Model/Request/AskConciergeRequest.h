//
//  AskConciergeRequest.h
//  LuxuryCard
//
//  Created by Chung Mai on 9/11/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseItem.h"
#import "ConciergeDetailObject.h"
#import "ConciergeObject.h"

#define ADD_EDIT_TYPE  @"ADD"
#define UPDATE_EDIT_TYPE    @"AMEND"
#define CANCEL_EDIT_TYPE    @"CANCEL"
@interface AskConciergeRequest : NSObject

+(NSDictionary *) buildRequestMessageWithData:(NSDictionary *)data;
+(NSString *)editAskConciergeTextWithConciergeData:(ConciergeDetailObject *)item;
+(NSString *)buildAskConciergeTextWithConciergeData:(ConciergeDetailObject *)item;
+(NSString *)convertCategoryCodeToMyRequestType:(NSString *)categoryCode;
+(NSString *)convertCategoryNameToMyRequestType:(NSString *)categoryName andSubcategory:(NSString *)subCategory;
+(NSArray *)buildRequestDetailForConciergeData:(ConciergeDetailObject *)item;
+(NSString *)buildRequestWithConciergeData:(ConciergeObject *)item;
@end
