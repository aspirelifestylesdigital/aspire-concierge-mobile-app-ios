//
//  AskConciergeRequest.m
//  LuxuryCard
//
//  Created by Chung Mai on 9/11/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "AskConciergeRequest.h"
#import "Constant.h"
#import "EnumConstant.h"
#import "ConciergeDetailObject.h"
#import "PropertyItem.h"
#import "NSString+Utis.h"

#define OTHER_FUNCTIONALITY  @"Others"
#define OTHER_REQUEST_FUNCTIONALITY  @"Other Request"
#define SOURCE @"Mobile App"

#define kAccessToken    @"AccessToken"
#define kConsumerKey    @"ConsumerKey"
#define kEPCClientCode  @"EPCClientCode"
#define kEPCProgramCode @"EPCProgramCode"
#define kSalutation     @"Salutation"
#define kFirstName      @"FirstName"
#define kLastName       @"LastName"
#define kVeriCode       @"VeriCode"
#define kEditType       @"EditType"
#define kSource         @"Source"
#define kFunctionality  @"Functionality"
#define kRequestType    @"RequestType"
#define kPrefResponse   @"PrefResponse"
#define kEmailAddress1  @"EmailAddress1"
#define kPhoneNumber    @"PhoneNumber"
#define kRequestDetails @"RequestDetails"
#define kAttachmentPath @"AttachmentPath"
#define kOnlineMemberId @"OnlineMemberId"
#define kTransactionID  @"TransactionID"


@implementation AskConciergeRequest

+(NSDictionary *) buildRequestMessageWithData:(NSDictionary *)data
{
    NSMutableDictionary* dictKeyValues = nil;
    NSString *editType = data[@"editType"];
    
    if([editType isEqualToString:ADD_EDIT_TYPE])
    {
        
        dictKeyValues = [AskConciergeRequest buildParametersForNewRequestWithEditType:editType WithData:data];
    }
    else if([editType isEqualToString:UPDATE_EDIT_TYPE])
    {
        dictKeyValues = [AskConciergeRequest buildParametersForUpdatingRequestWithEditType:editType WithData:data];
    }else if([editType isEqualToString:CANCEL_EDIT_TYPE])
    {
        dictKeyValues = [AskConciergeRequest buildParametersForUpdatingRequestWithEditType:editType WithData:data];
    }
    return dictKeyValues;
}

+(NSMutableDictionary *)buildParametersForUpdatingRequestWithEditType:(NSString *)editType WithData:(NSDictionary *)data
{
    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
    
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc] init];
    [dictKeyValues setObject:[[SessionData shareSessiondata] AccessToken] forKey:kAccessToken];
    [dictKeyValues setObject:[[SessionData shareSessiondata] OnlineMemberID]  forKey:kOnlineMemberId];
    [dictKeyValues setObject:B2C_VERIFICATION_CODE forKey:kVeriCode];
    
    [dictKeyValues setObject:B2C_ConsumerKey forKey:kConsumerKey];
    [dictKeyValues setObject:B2C_ConsumerKey forKey:kEPCClientCode];
    [dictKeyValues setObject:B2C_EPCProgramCode forKey:kEPCProgramCode];
    
    [dictKeyValues setObject:SOURCE forKey:kSource];
    [dictKeyValues setObject:OTHER_FUNCTIONALITY forKey:kFunctionality];
    [dictKeyValues setObject:OTHER_REQUEST_TYPE forKey:kRequestType];
    
    [dictKeyValues setObject:data[kTransactionID] forKey:kTransactionID];
    //    [dictKeyValues setObject:EDIT_TYPE forKey:kVeriCode];
    [dictKeyValues setObject:editType forKey:kEditType];
    
    if (profileDictionary) {
        NSString *salutation = [profileDictionary objectForKey:keySalutation];
        [dictKeyValues setValue:(salutation.length > 0)?salutation:@"N/A" forKey:kSalutation];
        [dictKeyValues setValue:[profileDictionary objectForKey:keyFirstName] forKey:kFirstName];
        [dictKeyValues setValue:[profileDictionary objectForKey:keyLastName] forKey:kLastName];
        [dictKeyValues setValue:[profileDictionary objectForKey:keyMobileNumber] forKey:kPhoneNumber];
        [dictKeyValues setValue:[profileDictionary objectForKey:keyEmail] forKey:kEmailAddress1];
        NSString *requestDetail = @"";
        
        if ([data[@"phone"] boolValue] == YES && [data[@"mail"] boolValue] == YES) {
            [dictKeyValues setValue:@"Email, Mobile" forKey:kPrefResponse];
        } else {
            if ([data[@"phone"] boolValue] == YES) {
                [dictKeyValues setValue:@"Mobile" forKey:kPrefResponse];
            }else if ([data[@"mail"] boolValue] == YES) {
                [dictKeyValues setValue:@"Email" forKey:kPrefResponse];
            }
        }
        requestDetail = [requestDetail stringByAppendingString:data[kRequestDetails]];
        [dictKeyValues setValue:requestDetail forKey:kRequestDetails];
    }
    
    //[dictKeyValues setObject:[data objectForKey:kAttachmentPath] forKey:kAttachmentPath];
    
    return dictKeyValues;
}

+(NSMutableDictionary *)buildParametersForNewRequestWithEditType:(NSString *)editType WithData:(NSDictionary *)data
{
    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
    
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc] init];
    [dictKeyValues setObject:[[SessionData shareSessiondata] AccessToken] forKey:kAccessToken];
    [dictKeyValues setObject:[[SessionData shareSessiondata] OnlineMemberID]  forKey:kOnlineMemberId];
    [dictKeyValues setObject:B2C_VERIFICATION_CODE forKey:kVeriCode];
    
    [dictKeyValues setObject:B2C_ConsumerKey forKey:kConsumerKey];
    [dictKeyValues setObject:B2C_ConsumerKey forKey:kEPCClientCode];
    [dictKeyValues setObject:B2C_EPCProgramCode forKey:kEPCProgramCode];
    
    [dictKeyValues setObject:SOURCE forKey:kSource];
    [dictKeyValues setObject:OTHER_FUNCTIONALITY forKey:kFunctionality];
    [dictKeyValues setObject:OTHER_REQUEST_TYPE forKey:kRequestType];
    
    //    [dictKeyValues setObject:EDIT_TYPE forKey:kVeriCode];
    [dictKeyValues setObject:editType forKey:kEditType];
    
    if (profileDictionary) {
        NSString *salutation = [profileDictionary objectForKey:keySalutation];
        [dictKeyValues setValue:(salutation.length > 0)?salutation:@"N/A" forKey:kSalutation];
        [dictKeyValues setValue:[profileDictionary objectForKey:keyFirstName] forKey:kFirstName];
        [dictKeyValues setValue:[profileDictionary objectForKey:keyLastName] forKey:kLastName];
        [dictKeyValues setValue:[profileDictionary objectForKey:keyMobileNumber] forKey:kPhoneNumber];
        [dictKeyValues setValue:[profileDictionary objectForKey:keyEmail] forKey:kEmailAddress1];
        NSString *requestDetail = @"";
        
        if ([data[@"phone"] boolValue] == YES && [data[@"mail"] boolValue] == YES) {
            [dictKeyValues setValue:@"Email, Mobile" forKey:kPrefResponse];
        } else {
            if ([data[@"phone"] boolValue] == YES) {
                [dictKeyValues setValue:@"Mobile" forKey:kPrefResponse];
            }else if ([data[@"mail"] boolValue] == YES) {
                [dictKeyValues setValue:@"Email" forKey:kPrefResponse];
            }
        }
        requestDetail = [requestDetail stringByAppendingString:data[kRequestDetails]];
        [dictKeyValues setValue:requestDetail forKey:kRequestDetails];
    }
    
    //[dictKeyValues setObject:[data objectForKey:kAttachmentPath] forKey:kAttachmentPath];
    
    return dictKeyValues;
}

#pragma mark BUILD EDIT/DUPLICATE REQUEST TEXT
+(NSString *)editAskConciergeTextWithConciergeData:(ConciergeDetailObject *)item
{
    return [item requestDetailForDisplay];
}


#pragma mark BUILD NEW REQUEST TEXT
+(NSString *)buildAskConciergeTextWithConciergeData:(ConciergeDetailObject *)item
{
    NSString *message = @"";
    if([item.requestType isEqualToString:DINING_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildAskConciergeTextForDiningRequestType:item];
    }
    else if([item.requestType isEqualToString:HOTEL_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildAskConciergeTextForHotelRequestType:item];
    }
    else if([item.requestType isEqualToString:GOLF_REQUEST_TYPE] || [item.requestType isEqualToString:GOLF_RED_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildAskConciergeTextForGolfRequestType:item];
    }
    else if([item.requestType isEqualToString:TRANSPORTATION_REQUEST_TYPE] || [item.requestType isEqualToString:TRANSFER_REQUEST_TYPE])
    {
//        if([item.requestDetail containsString:@"DriverName"]){
//            message = [AskConciergeRequest buildAskConciergeTextForCarRentalRequestType:item];
//        }
//        else{
//            message = [AskConciergeRequest buildAskConciergeTextForTransferRequestType:item];
//        }
        message = [AskConciergeRequest buildAskConciergeTextForTransferRequestType:item];
    }
    else if([item.requestType isEqualToString:ENTERTAINMENT_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildAskConciergeTextForEntertainmentRequestType:item];
    }
    else if([item.requestType isEqualToString:TOUR_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildAskConciergeTextForTourRequestType:item];
    }
    else if([item.requestType isEqualToString:AIRPORT_SERVICE_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildAskConciergeTextForAirPortRequestType:item];
    }
    else if([item.requestType isEqualToString:FLOWERS_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildAskConciergeTextForFlowerRequestType:item];
    }
    else if([item.requestType isEqualToString:TRAVEL_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildAskConciergeTextForPrivateJetRequestType:item];
    }
    else if([item.requestType isEqualToString:CRUISE_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildAskConciergeTextForCruiseRequestType:item];
    }
    else if([item.requestType isEqualToString:VACATION_PACKAGE_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildAskConciergeTextForVacationPackageRequestType:item];
    }
    else
    {
        message = [AskConciergeRequest buildAskConciergeTextForOtherRequestType:item];
    }
    return message;
}


+ (NSString *)buildAskConciergeTextForDiningRequestType:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"Name: %@\n",(item.name ? item.name : @"")];
    [message appendFormat:@"Date/time: %@\n",item.eventDate ? item.eventDate : @""];
    [message appendFormat:@"# of adults: %@\n",item.numberOfAdults ? item.numberOfAdults : @""];
    [message appendFormat:@"# of children: %@\n",item.numberOfChildrens ?  : @""];
    [message appendFormat:@"City: %@\n",item.city ? item.city : @""];
    [message appendFormat:@"State: %@\n",item.state ? item.state : @""];
    [message appendFormat:@"Country: %@\n",item.country ? item.country : @""];
    [message appendFormat:@"Other comments: %@",item.commonMessage ? item.commonMessage : @""];
    return message;
}

+(NSString *)buildAskConciergeTextForHotelRequestType:(ConciergeDetailObject *)item
{
    
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"Name: %@\n",item.name ? item.name : @""];
    [message appendFormat:@"Check in: %@\n",item.startDateWithoutTime ? item.startDateWithoutTime : @""];
    [message appendFormat:@"Check out: %@\n",item.endDateWithoutTime ? item.endDateWithoutTime : @""];
    [message appendFormat:@"# of adults: %@\n",item.numberOfAdults ? item.numberOfAdults : @""];
    [message appendFormat:@"# of children: %@\n",item.numberOfChildrens ? item.numberOfChildrens : @""];
    [message appendFormat:@"City: %@\n",item.city ? item.city : @""];
    [message appendFormat:@"State: %@\n",item.state ? item.state : @""];
    [message appendFormat:@"Country: %@\n",item.country ? item.country : @""];
    [message appendFormat:@"Other comments: %@",item.commonMessage ? item.commonMessage : @""];
    return message;
}

+(NSString *)buildAskConciergeTextForOtherRequestType:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    if(![item.name isEqualToString:@""])
    {
        [message appendFormat:@"Name: %@\n",item.name];
        [message appendFormat:@"Other comments: "];
    }else{
        [message appendFormat:@"Other comments: Common Message"];
    }
//    [message appendFormat:@"Other comments: %@\n",item.requestDetail ? item.requestDetail : @""] ;
    
    return message;
}

+(NSString *)buildAskConciergeTextForGolfRequestType:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"Name: %@\n",item.name ? item.name : @""];
    [message appendFormat:@"Date: %@\n",item.startDate ? item.startDate : @""];
    [message appendFormat:@"# of golfers: %@\n",item.numberOfAdults ? item.numberOfAdults : @""];
    [message appendFormat:@"City: %@\n",item.city ? item.city : @""];
    [message appendFormat:@"State: %@\n",item.state ? item.state : @""];
    [message appendFormat:@"Country: %@\n",item.country ? item.country : @""];
    [message appendFormat:@"Other comments: %@",item.commonMessage ? item.commonMessage : @""];
    return message;
}

+(NSString *)buildAskConciergeTextForCarRentalRequestType:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"Driver: %@\n",item.name ? item.name : @""];
    [message appendFormat:@"Pick up date: %@\n",item.pickUpDate ? item.pickUpDate : @""];
    [message appendFormat:@"Pick up location: %@\n",item.pickUpLocation ? item.pickUpLocation : @""];
    [message appendFormat:@"Drop off location: %@\n",item.dropOffLocation ? item.dropOffLocation : @""];
    [message appendFormat:@"# in party: %@\n",item.numberOfAdults ? item.numberOfAdults : @""];
    [message appendFormat:@"Other comments: %@",item.commonMessage ? item.commonMessage : @""];
    return message;
}

+(NSString *)buildAskConciergeTextForTransferRequestType:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"Name: %@\n",item.name ? item.name : @""];
    [message appendFormat:@"Pick up date: %@\n",item.pickUpDate ? item.pickUpDate : @""];
    [message appendFormat:@"Pick up location: %@\n",item.pickUpLocation ? item.pickUpLocation : @""];
    [message appendFormat:@"Drop off location: %@\n",item.dropOffLocation ? item.dropOffLocation : @""];
    [message appendFormat:@"# in party: %@\n",item.numberOfAdults ? item.numberOfAdults : @""];
    [message appendFormat:@"Other comments: %@",item.commonMessage ? item.commonMessage : @""];
    return message;
}

+(NSString *)buildAskConciergeTextForEntertainmentRequestType:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    
    [message appendFormat:@"Name: %@\n",item.name ? item.name : @""];
    [message appendFormat:@"Date: %@\n",item.startDate ? item.startDate : @""];
    [message appendFormat:@"# of tickets: %@\n", item.numberOfAdults ? item.numberOfAdults : @""];
    [message appendFormat:@"City: %@\n",item.city ? item.city : @""];
    [message appendFormat:@"State: %@\n",item.state ? item.state : @""];
    [message appendFormat:@"Country: %@\n",item.country ? item.country : @""];
    [message appendFormat:@"Other comments: %@",item.commonMessage ? item.commonMessage : @""];
    return message;
}


// Build for Tour, Flight and Cruise Request Type
+(NSString *)buildAskConciergeTextForTourRequestType:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"Name: %@\n",item.name ? item.name : @""];
    [message appendFormat:@"Start date: %@\n",item.startDate ? item.startDate : @""];
//    [message appendFormat:@"End date: %@\n",item.endDate ? item.endDate : @""];
    [message appendFormat:@"Start location: %@\n",item.startLocation ? item.startLocation : @""];
//    [message appendFormat:@"End location: %@\n",item.endLocation ? item.endLocation : @""];
    [message appendFormat:@"# of adults: %@\n",item.numberOfAdults ? item.numberOfAdults : @""];
    [message appendFormat:@"# of children: %@\n",item.numberOfChildrens ? item.numberOfChildrens : @""];
    [message appendFormat:@"# of infants: %@\n",item.numberOfInfants ? item.numberOfInfants : @""];
    [message appendFormat:@"Other comments: %@",item.commonMessage ? item.commonMessage : @""];
    return message;
}

+(NSString *)buildAskConciergeTextForAirPortRequestType:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"Name: %@\n",item.name ? item.name : @""];
//    [message appendFormat:@"Start date: %@\n",item.startDate ? item.startDate : @""];
//    [message appendFormat:@"End date: %@\n",item.endDate ? item.endDate : @""];
//    [message appendFormat:@"Start location: %@\n",item.startLocation ? item.startLocation : @""];
//    [message appendFormat:@"End location: %@\n",item.endLocation ? item.endLocation : @""];
//    [message appendFormat:@"# of adults: %@\n",item.numberOfAdults ? item.numberOfAdults : @""];
//    [message appendFormat:@"# of children: %@\n",item.numberOfChildrens ? item.numberOfChildrens : @""];
//    [message appendFormat:@"# of infants: %@\n",item.numberOfInfants ? item.numberOfInfants : @""];
    [message appendFormat:@"Other comments: %@",item.commonMessage ? item.commonMessage : @""];
    return message;
}

+(NSString *)buildAskConciergeTextForFlowerRequestType:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"Arrangement: %@\n",item.name ? item.name : @""];
    [message appendFormat:@"Delivery date: %@\n",item.deliveryDate ? item.deliveryDate : @""];
    [message appendFormat:@"Delivery address: %@\n",item.dAddress ? item.dAddress : @""];
    [message appendFormat:@"Quantity: %@\n",item.bouquetQuantity ? item.bouquetQuantity : @""];
    [message appendFormat:@"Other comments: %@",item.commonMessage ? item.commonMessage : @""];
    return message;
}

+(NSString *)buildAskConciergeTextForPrivateJetRequestType:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"Name: %@\n",item.name ? item.name : @""];
    [message appendFormat:@"Start date: %@\n",item.startDate ? item.startDate : @""];
    [message appendFormat:@"Start location: %@\n",item.startLocation ? item.startLocation : @""];
    [message appendFormat:@"End location: %@\n",item.endLocation ? item.endLocation : @""];
    [message appendFormat:@"# of passengers: %@\n",item.numberOfPassengers ? item.numberOfPassengers : @""];
    [message appendFormat:@"Other comments: %@",item.commonMessage ? item.commonMessage : @""];
    return message;
}

+(NSString *)buildAskConciergeTextForCruiseRequestType:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"Name: %@\n",item.name ? item.name : @""];
    [message appendFormat:@"Start date: %@\n",item.startDateWithoutTime ? item.startDateWithoutTime : @""];
//    [message appendFormat:@"End date: %@\n",item.endDateWithoutTime ? item.endDateWithoutTime : @""];
    [message appendFormat:@"Start location: %@\n",item.startLocation ? item.startLocation : @""];
//    [message appendFormat:@"End location: %@\n",item.endLocation ? item.endLocation : @""];
    [message appendFormat:@"# of adults: %@\n",item.numberOfAdults ? item.numberOfAdults : @""];
    [message appendFormat:@"# of children: %@\n",item.numberOfChildrens ? item.numberOfChildrens : @""];
//    [message appendFormat:@"# of infants: %@\n",item.numberOfInfants ? item.numberOfInfants : @""];
    [message appendFormat:@"Other comments: %@",item.commonMessage ? item.commonMessage : @""];
    return message;
}

+(NSString *)buildAskConciergeTextForVacationPackageRequestType:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"Name: %@\n",item.name ? item.name : @""];
    [message appendFormat:@"Date: %@\n",item.startDate ? item.startDate : @""];
    [message appendFormat:@"# of adults: %@\n",item.numberOfAdults ? item.numberOfAdults : @""];
    [message appendFormat:@"# of children: %@\n",item.numberOfChildrens ? item.numberOfChildrens : @""];
    [message appendFormat:@"Other comments: %@",item.commonMessage ? item.commonMessage : @""];
    return message;
}

#pragma mark BUILD REQUEST TEXT

+(NSString *)buildRequestWithConciergeData:(ConciergeObject *)item
{
    return [item requestDetailForDisplay];;
    /*
    NSString *message = @"";
    if([item.requestType isEqualToString:DINING_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildRequestTextForDiningRequestType:item];
    }
    else if([item.requestType isEqualToString:HOTEL_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildRequestTextForHotelRequestType:item];
    }
    else if([item.requestType isEqualToString:GOLF_REQUEST_TYPE] || [item.requestType isEqualToString:GOLF_RED_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildRequestTextForGolfRequestType:item];
    }
    else if([item.requestType isEqualToString:TRANSPORTATION_REQUEST_TYPE] || [item.requestType isEqualToString:TRANSFER_REQUEST_TYPE])
    {
        if([item.requestDetails containsString:@"DriverName"]){
            message = [AskConciergeRequest buildRequestTextForCarRentalRequestType:item];
        }
        else{
            message = [AskConciergeRequest buildRequestTextForTransferRequestType:item];
        }
        
    }
    else if([item.requestType isEqualToString:ENTERTAINMENT_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildRequestTextForEntertainmentRequestType:item];
    }
    else if([item.requestType isEqualToString:TOUR_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildRequestTextForTourRequestType:item];
    }
    else if([item.requestType isEqualToString:AIRPORT_SERVICE_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildRequestTextForAirPortRequestType:item];
    }
    else if([item.requestType isEqualToString:FLOWERS_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildRequestTextForFlowerRequestType:item];
    }
    else if([item.requestType isEqualToString:TRAVEL_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildRequestTextForPrivateJetRequestType:item];
    }
    else if([item.requestType isEqualToString:CRUISE_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildRequestTextForCruiseRequestType:item];
    }
    else if([item.requestType isEqualToString:VACATION_PACKAGE_REQUEST_TYPE])
    {
        message = [AskConciergeRequest buildRequestTextForVacationPackageRequestType:item];
    }
    else
    {
        message = [AskConciergeRequest buildRequestTextForOtherRequestType:item];
    }
    return message;
     */
}


+ (NSString *)buildRequestTextForDiningRequestType:(ConciergeObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    
    [message appendFormat:@"%@\n",item.requestTypeMapping];
    
    if(item.name.length > 0)
    {
        [message appendFormat:@"%@\n",item.name];
    }
    
    if(item.eventDate.length > 0)
    {
        [message appendFormat:@"%@",item.eventDate];
    }

    return message;
}

+(NSString *)buildRequestTextForHotelRequestType:(ConciergeObject *)item
{
    
    NSMutableString *message = [[NSMutableString alloc] init];
    
    [message appendFormat:@"%@\n",item.requestTypeMapping];
    
    if(item.name.length > 0)
    {
        [message appendFormat:@"%@\n",item.name];
    }
    
    if(item.startDate.length > 0)
    {
        [message appendFormat:@"%@",item.startDate];
    }
    
    if(item.endDate.length > 0)
    {
        [message appendFormat:@" - "];
        [message appendFormat:@"%@",item.endDate];
    }

    return message;
}

+(NSString *)buildRequestTextForOtherRequestType:(ConciergeObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"%@",item.requestTypeMapping];
    
    return message;
}

+(NSString *)buildRequestTextForGolfRequestType:(ConciergeObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"%@\n",item.requestTypeMapping];
    
    if(item.name.length > 0)
    {
        [message appendFormat:@"%@\n",item.name];
    }
    
    if(item.startDate.length > 0)
    {
        [message appendFormat:@"%@",item.startDate];
    }

    return message;
}

+(NSString *)buildRequestTextForCarRentalRequestType:(ConciergeObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"%@\n",item.requestTypeMapping];
    
    if(item.name.length > 0)
    {
        [message appendFormat:@"%@\n",item.name];
    }
    
    if(item.pickupDate.length > 0)
    {
        [message appendFormat:@"%@",item.pickupDate];
    }
    return message;
}

+(NSString *)buildRequestTextForTransferRequestType:(ConciergeObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"%@\n",item.requestTypeMapping];
    
    if(item.name.length > 0)
    {
        [message appendFormat:@"%@\n",item.name];
    }
    
    if(item.pickupDate.length > 0)
    {
        [message appendFormat:@"%@",item.pickupDate];
    }
    return message;
}

+(NSString *)buildRequestTextForEntertainmentRequestType:(ConciergeObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"%@\n",item.requestTypeMapping];
    
    if(item.name.length > 0)
    {
        [message appendFormat:@"%@\n",item.name];
    }
    
    if(item.eventDate.length > 0)
    {
        [message appendFormat:@"%@",item.eventDate];
    }
    return message;
}


// Build for Tour, Flight and Cruise Request Type
+(NSString *)buildRequestTextForTourRequestType:(ConciergeObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"%@\n",item.requestTypeMapping];
    
    if(item.name.length > 0)
    {
        [message appendFormat:@"%@\n",item.name];
    }
    
    if(item.startDate.length > 0)
    {
        [message appendFormat:@"%@",item.startDate];
    }
    
    if(item.endDate.length > 0)
    {
        [message appendFormat:@" - "];
        [message appendFormat:@"%@",item.endDate];
    }
    return message;
}

+(NSString *)buildRequestTextForAirPortRequestType:(ConciergeObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"%@\n",item.requestTypeMapping];
    
    if(item.startDate.length > 0)
    {
        [message appendFormat:@"%@",item.startDate];
    }
    
    if(item.endDate.length > 0)
    {
        [message appendFormat:@" - "];
        [message appendFormat:@"%@",item.endDate];
    }
    return message;
}

+(NSString *)buildRequestTextForFlowerRequestType:(ConciergeObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"%@\n",item.requestTypeMapping];
    
    if(item.deliveryDate.length > 0)
    {
        [message appendFormat:@"%@",item.deliveryDate];
    }
    
    return message;
}

+(NSString *)buildRequestTextForPrivateJetRequestType:(ConciergeObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"%@\n",item.requestTypeMapping];
    
    
    if(item.startDate.length > 0)
    {
        [message appendFormat:@"%@",item.startDate];
    }

    return message;
}

+(NSString *)buildRequestTextForCruiseRequestType:(ConciergeObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"%@\n",item.requestTypeMapping];
    
    if(item.startDate.length > 0)
    {
        [message appendFormat:@"%@",item.startDate];
    }
    
    if(item.endDate.length > 0)
    {
        [message appendFormat:@" - "];
        [message appendFormat:@"%@",item.endDate];
    }
    return message;
}

+(NSString *)buildRequestTextForVacationPackageRequestType:(ConciergeObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    [message appendFormat:@"%@\n",item.requestTypeMapping];
    
    if(item.name.length > 0)
    {
        [message appendFormat:@"%@\n",item.name];
    }
    
    if(item.startDate.length > 0)
    {
        [message appendFormat:@"%@",item.startDate];
    }
    
    if(item.endDate.length > 0)
    {
        [message appendFormat:@" - "];
        [message appendFormat:@"%@",item.endDate ];
    }
    return message;
}

+(NSString *)convertCategoryCodeToMyRequestType:(NSString *)categoryCode
{
    if([categoryCode isEqualToString:@"dining"])
    {
        return DINING_REQUEST_TYPE;
    }
    else if([categoryCode isEqualToString:@"hotels"])
    {
        return HOTEL_REQUEST_TYPE;
    }
    else if([categoryCode isEqualToString:@"flowers"])
    {
        return FLOWERS_REQUEST_TYPE;
    }
    else if([categoryCode isEqualToString:@"entertainment"])
    {
        return ENTERTAINMENT_REQUEST_TYPE;
    }
    else if([categoryCode isEqualToString:@"golf"])
    {
        return GOLF_REQUEST_TYPE;
    }
    else if([categoryCode isEqualToString:@"vacation packages"])
    {
        return VACATION_PACKAGE_REQUEST_TYPE;
    }
    else if([categoryCode isEqualToString:@"cruise"])
    {
        return CRUISE_REQUEST_TYPE;
    }
    else if([categoryCode isEqualToString:@"tours"])
    {
        return TOUR_REQUEST_TYPE;
    }
    else if([categoryCode isEqualToString:@"travel"])
    {
        return TRAVEL_REQUEST_TYPE;
    }
    else if([categoryCode isEqualToString:@"airport services"])
    {
        return AIRPORT_SERVICE_REQUEST_TYPE;
    }
    else if([categoryCode isEqualToString:@"travel services"])
    {
        return TRAVEL_REQUEST_TYPE;
    }
    else if([categoryCode isEqualToString:@"transportation"])
    {
        return TRANSPORTATION_REQUEST_TYPE;
    }
    
    //Shopping and City Guide
    return OTHER_REQUEST_TYPE;
}

+(NSString *)convertCategoryNameToMyRequestType:(NSString *)categoryName andSubcategory:(NSString *)subCategory
{
    if([categoryName isEqualToString:@"Dining"] || ([categoryName isEqualToString:@"Specialty Travel"] && [subCategory isEqualToString:@"Culinary Experiences"]))
    {
        return DINING_REQUEST_TYPE;
    }
    else if([categoryName isEqualToString:@"Hotels"])
    {
        return HOTEL_REQUEST_TYPE;
    }
    else if([categoryName isEqualToString:@"Flowers"])
    {
        return FLOWERS_REQUEST_TYPE;
    }
    else if([categoryName isEqualToString:@"Tickets"] || ([categoryName isEqualToString:@"Specialty Travel"] && [subCategory isEqualToString:@"Entertainment Experiences"]) || ([categoryName isEqualToString:@"Specialty Travel"] && [subCategory isEqualToString:@"Major Sports Events"]))
    {
        return ENTERTAINMENT_REQUEST_TYPE;
    }
    else if([categoryName isEqualToString:@"Golf"] || [categoryName isEqualToString:@"Golf Merchandise"] || ([categoryName isEqualToString:@"Specialty Travel"] && [subCategory isEqualToString:@"Golf Experiences"]))
    {
        return GOLF_REQUEST_TYPE;
    }
    else if([categoryName isEqualToString:@"Vacation Packages"])
    {
        return VACATION_PACKAGE_REQUEST_TYPE;
    }
    else if([categoryName isEqualToString:@"Cruises"])
    {
        return CRUISE_REQUEST_TYPE;
    }
    else if([categoryName isEqualToString:@"VIP Travel Services"] && [subCategory isEqualToString:@"Sightseeing"])
    {
        return TOUR_REQUEST_TYPE;
    }
    else if([categoryName isEqualToString:@"Private Jet Travel"])
    {
        return TRAVEL_REQUEST_TYPE;
    }
    else if([categoryName isEqualToString:@"VIP Travel Services"] && [subCategory isEqualToString:@"Airport Services"])
    {
        return AIRPORT_SERVICE_REQUEST_TYPE;
    }
    else if([categoryName isEqualToString:@"VIP Travel Services"] && [subCategory isEqualToString:@"Travel Services"])
    {
        return @"";
    }
    else if([categoryName isEqualToString:@"Transportation"])
    {
        return TRANSPORTATION_REQUEST_TYPE;
    }
    // Shopping, City Guide and Travel Services
    return OTHER_REQUEST_TYPE;
}

#pragma mark BUILD REQUEST DETAIL

+(NSArray *)buildRequestDetailForConciergeData:(ConciergeDetailObject *)item{
    NSMutableArray *arrayText = [[NSMutableArray alloc] init];
    
//     Add Request Type
    PropertyItem *property = [[PropertyItem alloc] init];
    
    NSArray *detailInfo = [item.requestDetail componentsSeparatedByString:@"\n"];
    NSString *categoryName = detailInfo[0];
    if([categoryName containsString:@"CategoryName"] || [categoryName containsString:@"Category Name"])
    {
        NSArray *categoryNameInfo = [categoryName componentsSeparatedByString:@":"];
        NSString *requestType = categoryNameInfo[1];
        requestType = [requestType capitalizedString];
        requestType = [requestType stringByTrimmingCharactersInSet:
                       [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        property.title = @"REQUEST TYPE";
        property.value = requestType;
        [arrayText addObject:property];
        
        property = [[PropertyItem alloc] init];
        property.title = @"REQUEST DETAILS";
        property.value = [item requestDetailForDisplay];
        [arrayText addObject:property];
    }
    else{
        item.requestTypeMapping = [item.requestTypeMapping capitalizedString];
        property.title = @"REQUEST TYPE";
        property.value = item.requestTypeMapping;
        [arrayText addObject:property];
        
        property = [[PropertyItem alloc] init];
        property.title = @"REQUEST DETAILS";
        property.value = [item requestDetailForDisplay];
        [arrayText addObject:property];
    }
    
//    Last updated
    [AskConciergeRequest createLastUpdatedFieldWithData:item forRequest:arrayText];
    /*
    if([item.requestType isEqualToString:DINING_REQUEST_TYPE])
    {
        arrayText = [AskConciergeRequest buildRequestDetailForDiningRequestType:item];
    }
    else if([item.requestType isEqualToString:HOTEL_REQUEST_TYPE])
    {
        arrayText = [AskConciergeRequest buildRequestDetailForHotelRequestType:item];
    }
    else if([item.requestType isEqualToString:GOLF_REQUEST_TYPE] || [item.requestType isEqualToString:GOLF_RED_REQUEST_TYPE])
    {
        arrayText = [AskConciergeRequest buildRequestDetailForGolfRequestType:item];
    }
    else if([item.requestType isEqualToString:TRANSPORTATION_REQUEST_TYPE] || [item.requestType isEqualToString:TRANSFER_REQUEST_TYPE])
    {
        if([item.requestDetail containsString:@"DriverName"]){
            arrayText = [AskConciergeRequest buildRequestDetailForCarRentalRequestType:item];
        }
        else{
            arrayText = [AskConciergeRequest buildRequestDetailForTransferRequestType:item];
        }
        
    }
    else if([item.requestType isEqualToString:ENTERTAINMENT_REQUEST_TYPE])
    {
        arrayText = [AskConciergeRequest buildRequestDetailForEntertainmentRequestType:item];
    }
    else if([item.requestType isEqualToString:TOUR_REQUEST_TYPE])
    {
        arrayText = [AskConciergeRequest buildRequestDetailForTourRequestType:item];
    }
    else if([item.requestType isEqualToString:AIRPORT_SERVICE_REQUEST_TYPE])
    {
        arrayText = [AskConciergeRequest buildRequestDetailForAirPortRequestType:item];
    }
    else if([item.requestType isEqualToString:FLOWERS_REQUEST_TYPE])
    {
        arrayText = [AskConciergeRequest buildRequestDetailForFlowerRequestType:item];
    }
    else if([item.requestType isEqualToString:TRAVEL_REQUEST_TYPE])
    {
        arrayText = [AskConciergeRequest buildRequestDetailForPrivateJetRequestType:item];
    }
    else if([item.requestType isEqualToString:CRUISE_REQUEST_TYPE])
    {
        arrayText = [AskConciergeRequest buildRequestDetailForCruiseRequestType:item];
    }
    else if([item.requestType isEqualToString:VACATION_PACKAGE_REQUEST_TYPE])
    {
        arrayText = [AskConciergeRequest buildRequestDetailForVacationPackageRequestType:item];
    }
    else
    {
        arrayText = [AskConciergeRequest buildRequestDetailForOtherRequestType:item];
    }
     */
    return [arrayText copy];
}

+ (NSArray *)buildRequestDetailForDiningRequestType:(ConciergeDetailObject *)item
{
    NSMutableArray *arrayText = [[NSMutableArray alloc] init];
    
    [AskConciergeRequest createNameFieldWithData:item forRequest:arrayText withHandle:^NSMutableString*(){
        return [AskConciergeRequest createNameDataWithCityStateCountry:item];
    }];
    
    [AskConciergeRequest createWhenFieldWithData:item forRequest:arrayText withHandle:^NSMutableString*(){
        NSString *formatDate = item.eventDate;
        if(formatDate.length > 0)
        {
            return [NSMutableString stringWithFormat:@"%@",formatDate];
        }
        return nil;
    }];
    
    [AskConciergeRequest createPartySizeFieldWithData:item forRequest:arrayText withHandle:^NSMutableString *(){
        return [AskConciergeRequest createPartySizeDataWithAdultChildNumber:item];
    }];
    
    [AskConciergeRequest createLastUpdatedFieldWithData:item forRequest:arrayText];
    
    return arrayText;
}

+(NSArray *)buildRequestDetailForHotelRequestType:(ConciergeDetailObject *)item
{
    
    NSMutableArray *arrayText = [[NSMutableArray alloc] init];
    
    [AskConciergeRequest createNameFieldWithData:item forRequest:arrayText withHandle:^NSMutableString*(){
        return [AskConciergeRequest createNameDataWithCityStateCountry:item];
    }];
    
    
    [AskConciergeRequest createWhenFieldWithData:item forRequest:arrayText withHandle:^NSMutableString*(){
        return [AskConciergeRequest createWhenDataWithStartEndDate:item];
    }];
    
    [AskConciergeRequest createPartySizeFieldWithData:item forRequest:arrayText withHandle:^NSMutableString *(){
        return [AskConciergeRequest createPartySizeDataWithAdultChildNumber:item];
    }];
    
    [AskConciergeRequest createLastUpdatedFieldWithData:item forRequest:arrayText];
    
    return arrayText;
}

+(NSArray *)buildRequestDetailForOtherRequestType:(ConciergeDetailObject *)item
{
    
    NSMutableArray *arrayText = [[NSMutableArray alloc] init];
    
    [AskConciergeRequest createNameFieldWithData:item forRequest:arrayText withHandle:^NSMutableString*(){
        NSMutableString *message = [[NSMutableString alloc] init];
        
        if(item.name && item.name.length > 0)
        {
            [message appendFormat:@"%@", item.name];
            [message appendFormat:@"\n"];
        }
        
//        
//        if(item.requestDetail && item.requestDetail.length > 0)
//        {
//            [message appendFormat:@"%@", item.requestDetail];
//        }
        
        return message;
    }];
    
    [AskConciergeRequest createLastUpdatedFieldWithData:item forRequest:arrayText];
    
    return arrayText;
    
}

+(NSArray *)buildRequestDetailForGolfRequestType:(ConciergeDetailObject *)item
{
    NSMutableArray *arrayText = [[NSMutableArray alloc] init];
    
    [AskConciergeRequest createNameFieldWithData:item forRequest:arrayText withHandle:^NSMutableString*(){
        return [AskConciergeRequest createNameDataWithCityStateCountry:item];
    }];
    
    [AskConciergeRequest createWhenFieldWithData:item forRequest:arrayText withHandle:^NSMutableString*(){
        NSString *formatDate = item.startDate;
        if(formatDate.length > 0)
        {
            return [NSMutableString stringWithFormat:@"%@",formatDate];
        }
        return nil;
    }];
    
    [AskConciergeRequest createPartySizeFieldWithData:item forRequest:arrayText withHandle:^NSMutableString *(){
        NSMutableString *message = [[NSMutableString alloc] init];
        if(item.numberOfAdults.length > 0)
        {
            [message appendFormat:@"%@", item.numberOfAdults];
            [message appendFormat:item.numberOfAdults.integerValue > 1 ? @" Adults" : @" Adult"];
        }
        return message;
    }];
    
    [AskConciergeRequest createLastUpdatedFieldWithData:item forRequest:arrayText];
    
    return arrayText;
}

+(NSArray *)buildRequestDetailForCarRentalRequestType:(ConciergeDetailObject *)item
{
    NSMutableArray *arrayText = [[NSMutableArray alloc] init];
    
    [AskConciergeRequest createNameFieldWithData:item forRequest:arrayText withHandle:^NSMutableString*(){
        return [AskConciergeRequest createNameDataWithPickDropLocation:item];
    }];
    
    [AskConciergeRequest createWhenFieldWithData:item forRequest:arrayText withHandle:^NSMutableString*(){
        NSString *formatDate = item.pickUpDate;
        if(formatDate.length > 0)
        {
            return [NSMutableString stringWithFormat:@"%@",formatDate];
        }
        return nil;
    }];
    
    [AskConciergeRequest createPartySizeFieldWithData:item forRequest:arrayText withHandle:^NSMutableString *(){
        NSMutableString *message = [[NSMutableString alloc] init];
        if(item.numberOfAdults.length > 0)
        {
            [message appendFormat:@"%@", item.numberOfAdults];
            [message appendFormat:item.numberOfAdults.integerValue > 1 ? @" Adults" : @" Adult"];
        }
        return message;
    }];
    
    [AskConciergeRequest createLastUpdatedFieldWithData:item forRequest:arrayText];
    
    return arrayText;
}

+(NSArray *)buildRequestDetailForTransferRequestType:(ConciergeDetailObject *)item
{
    NSMutableArray *arrayText = [[NSMutableArray alloc] init];
    
    [AskConciergeRequest createNameFieldWithData:item forRequest:arrayText withHandle:^NSMutableString*(){
        return [AskConciergeRequest createNameDataWithPickDropLocation:item];
    }];
    
    [AskConciergeRequest createWhenFieldWithData:item forRequest:arrayText withHandle:^NSMutableString*(){
        NSString *formatDate = item.pickUpDate;
        if(formatDate.length > 0)
        {
            return [NSMutableString stringWithFormat:@"%@",formatDate];
        }
        return nil;
    }];
    
    [AskConciergeRequest createPartySizeFieldWithData:item forRequest:arrayText withHandle:^NSMutableString *(){
        NSMutableString *message = [[NSMutableString alloc] init];
        if(item.numberOfAdults.length > 0)
        {
            [message appendFormat:@"%@", item.numberOfAdults];
            [message appendFormat:item.numberOfAdults.integerValue > 1 ? @" Adults" : @" Adult"];
        }
        return message;
    }];
    
    [AskConciergeRequest createLastUpdatedFieldWithData:item forRequest:arrayText];
    
    return arrayText;
}

+(NSArray *)buildRequestDetailForEntertainmentRequestType:(ConciergeDetailObject *)item
{
    NSMutableArray *arrayText = [[NSMutableArray alloc] init];
    
    [AskConciergeRequest createNameFieldWithData:item forRequest:arrayText withHandle:^NSMutableString*(){
        return [AskConciergeRequest createNameDataWithCityStateCountry:item];
    }];
    
    [AskConciergeRequest createWhenFieldWithData:item forRequest:arrayText withHandle:^NSMutableString*(){
        NSString *formatDate = item.eventDate;
        if(formatDate.length > 0)
        {
            return [NSMutableString stringWithFormat:@"%@",formatDate];
        }
        return nil;
    }];

    
    [AskConciergeRequest createPartySizeFieldWithData:item forRequest:arrayText withHandle:^NSMutableString *(){
        NSMutableString *message = [[NSMutableString alloc] init];
        if(item.numberOfAdults.length > 0)
        {
            [message appendFormat:@"%@", item.numberOfAdults];
            [message appendFormat:item.numberOfAdults.integerValue > 1 ? @" Adults" : @" Adult"];
        }
        return message;
    }];
    
    [AskConciergeRequest createLastUpdatedFieldWithData:item forRequest:arrayText];
    
    return arrayText;
}

+(NSArray *)buildRequestDetailForTourRequestType:(ConciergeDetailObject *)item
{
    NSMutableArray *arrayText = [[NSMutableArray alloc] init];
    
    [AskConciergeRequest createNameFieldWithData:item forRequest:arrayText withHandle:^NSMutableString*(){
        return [AskConciergeRequest createNameDataWithStartEndLocation:item];
    }];
    
    [AskConciergeRequest createWhenFieldWithData:item forRequest:arrayText withHandle:^NSMutableString*(){
        return [AskConciergeRequest createWhenDataWithStartEndDate:item];
    }];
    
    [AskConciergeRequest createPartySizeFieldWithData:item forRequest:arrayText withHandle:^NSMutableString *(){
        return [AskConciergeRequest createPartySizeDataWithAdultChildNumber:item];
    }];
    
    [AskConciergeRequest createLastUpdatedFieldWithData:item forRequest:arrayText];
    
    return arrayText;
}

+(NSArray *)buildRequestDetailForAirPortRequestType:(ConciergeDetailObject *)item
{
    NSMutableArray *arrayText = [[NSMutableArray alloc] init];
    
    [AskConciergeRequest createNameFieldWithData:item forRequest:arrayText withHandle:^NSMutableString*(){
        return [AskConciergeRequest createNameDataWithStartEndLocation:item];
    }];
    
    [AskConciergeRequest createWhenFieldWithData:item forRequest:arrayText withHandle:^NSMutableString*(){
        return [AskConciergeRequest createWhenDataWithStartEndDate:item];
    }];
    
    [AskConciergeRequest createPartySizeFieldWithData:item forRequest:arrayText withHandle:^NSMutableString *(){
        return [AskConciergeRequest createPartySizeDataWithAdultChildNumber:item];
    }];
    
    [AskConciergeRequest createLastUpdatedFieldWithData:item forRequest:arrayText];
    
    return arrayText;
}

+(NSArray *)buildRequestDetailForCruiseRequestType:(ConciergeDetailObject *)item
{
    NSMutableArray *arrayText = [[NSMutableArray alloc] init];
    
    [AskConciergeRequest createNameFieldWithData:item forRequest:arrayText withHandle:^NSMutableString*(){
        return [AskConciergeRequest createNameDataWithStartEndLocation:item];
    }];
    
    [AskConciergeRequest createWhenFieldWithData:item forRequest:arrayText withHandle:^NSMutableString*(){
        return [AskConciergeRequest createWhenDataWithStartEndDate:item];
    }];
    
    [AskConciergeRequest createPartySizeFieldWithData:item forRequest:arrayText withHandle:^NSMutableString *(){
        return [AskConciergeRequest createPartySizeDataWithAdultChildNumber:item];
    }];
    
    [AskConciergeRequest createLastUpdatedFieldWithData:item forRequest:arrayText];
    
    return arrayText;
}

+(NSArray *)buildRequestDetailForFlowerRequestType:(ConciergeDetailObject *)item
{
    
    NSMutableArray *arrayText = [[NSMutableArray alloc] init];
    
    [AskConciergeRequest createNameFieldWithData:item forRequest:arrayText withHandle:^NSMutableString*(){
        NSMutableString *message = [[NSMutableString alloc] init];
        
        if(item.name && item.name.length > 0)
        {
            [message appendFormat:@"%@", item.name];
        }
        
        if(item.dAddress && item.dAddress.length > 0)
        {
            [message appendFormat:@"\n"];
            [message appendFormat:@"%@", item.dAddress];
        }
        
        return message;
    }];

    
    [AskConciergeRequest createWhenFieldWithData:item forRequest:arrayText withHandle:^NSMutableString*(){
        NSString *formatDate = item.deliveryDate;
        if(formatDate.length > 0)
        {
            return [NSMutableString stringWithFormat:@"%@",formatDate];
        }
        return nil;
    }];
    
    [AskConciergeRequest createLastUpdatedFieldWithData:item forRequest:arrayText];
    
    return arrayText;
}

+(NSArray *)buildRequestDetailForPrivateJetRequestType:(ConciergeDetailObject *)item
{
    NSMutableArray *arrayText = [[NSMutableArray alloc] init];
    
    [AskConciergeRequest createNameFieldWithData:item forRequest:arrayText withHandle:^NSMutableString*(){
        return [AskConciergeRequest createNameDataWithStartEndLocation:item];
    }];
    
    [AskConciergeRequest createWhenFieldWithData:item forRequest:arrayText withHandle:^NSMutableString*(){
        NSString *formatDate = item.startDate;
        if(formatDate.length > 0)
        {
            return [NSMutableString stringWithFormat:@"%@",formatDate];
        }
        return nil;
    }];
    
    [AskConciergeRequest createPartySizeFieldWithData:item forRequest:arrayText withHandle:^NSMutableString *(){
        NSMutableString *message = [[NSMutableString alloc] init];
        
        if(item.numberOfPassengers && item.numberOfPassengers.length > 0)
        {
            [message appendFormat:@"%@", item.numberOfPassengers];
            [message appendFormat:item.numberOfAdults.integerValue > 1 ? @" Adults" : @" Adult"];
        }
        
        return message;
    }];
    
    [AskConciergeRequest createLastUpdatedFieldWithData:item forRequest:arrayText];
    
    return arrayText;
}

+(NSArray *)buildRequestDetailForVacationPackageRequestType:(ConciergeDetailObject *)item
{
    NSMutableArray *arrayText = [[NSMutableArray alloc] init];
    
    [AskConciergeRequest createNameFieldWithData:item forRequest:arrayText withHandle:^NSMutableString*(){
        return [AskConciergeRequest createNameDataWithCityStateCountry:item];
    }];
    
    [AskConciergeRequest createWhenFieldWithData:item forRequest:arrayText withHandle:^NSMutableString*(){
        NSString *formatDate = item.startDate;
        if(formatDate.length > 0)
        {
            return [NSMutableString stringWithFormat:@"%@",formatDate];
        }
        return nil;
    }];
    
    [AskConciergeRequest createPartySizeFieldWithData:item forRequest:arrayText withHandle:^NSMutableString *(){
        return [AskConciergeRequest createPartySizeDataWithAdultChildNumber:item];
    }];
    
    [AskConciergeRequest createLastUpdatedFieldWithData:item forRequest:arrayText];
    
    return arrayText;
}

#pragma mark CREATE NAME FIELD
+(void) createNameFieldWithData:(ConciergeDetailObject *)item forRequest:(NSMutableArray *)arrayText withHandle:(NSMutableString* (^)()) handler
{
    NSMutableString *message = handler();
    
    PropertyItem *property = [[PropertyItem alloc] init];
    property.title = item.requestTypeMapping;
    
    if(message.length > 0)
    {
        property.value = [NSString stringWithString:message];
    }
    
    [arrayText addObject:property];
}

+(NSMutableString *) createNameDataWithPickDropLocation:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    
    if(item.name.length > 0)
    {
        [message appendFormat:@"%@", item.name];
    }
    
    if(item.pickUpLocation.length > 0)
    {
        if(message.length > 0)
        {
            [message appendFormat:@"\n"];
        }
        
        [message appendFormat:@"%@", item.startLocation];
    }
    
    if(item.dropOffLocation.length > 0)
    {
        [message appendFormat:@" - "];
        [message appendFormat:@"%@", item.endLocation];
    }
    
    return message;
}

+(NSMutableString *) createNameDataWithStartEndLocation:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    
    if(item.name.length > 0)
    {
        [message appendFormat:@"%@", item.name];
    }
    
    if(item.startLocation.length > 0)
    {
        [message appendFormat:@"\n"];
        [message appendFormat:@"%@", item.startLocation];
    }
    
    if(item.endLocation.length > 0)
    {
        [message appendFormat:@" - "];
        [message appendFormat:@"%@", item.endLocation];
    }
    
    return message;
}

+(NSMutableString *) createNameDataWithCityStateCountry:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    
    if(item.name.length > 0)
    {
        [message appendFormat:@"%@", item.name];
    }
    
    
    if(item.country.length > 0 && ([[item.country lowercaseString] isEqualToString:@"united states"] || [[item.country lowercaseString] isEqualToString:@"canada"]))
    {
        if(item.city.length > 0)
        {
            if(message.length > 0)
            {
                [message appendFormat:@"\n"];
            }
            
            [message appendFormat:@"%@", item.city];
        }
        
        if(item.state.length > 0)
        {
            if(item.city.length > 0){
                [message appendFormat:@","];
            }
            
            [message appendFormat:@"%@", item.state];
        }
        
    }
    else
    {
        [message appendFormat:@"%@", item.country];
    }
    return message;
}

#pragma mark CREATE WHEN FIELD
+(void) createWhenFieldWithData:(ConciergeDetailObject *)item forRequest:(NSMutableArray *)arrayText withHandle:(NSMutableString* (^)()) handler
{
    NSMutableString *message = handler();
    
    if(message.length > 0)
    {
        PropertyItem *property = [[PropertyItem alloc] init];
        property.title = @"WHEN";
        property.value = [NSString stringWithString:message];
        [arrayText addObject:property];
    }
}

+(NSMutableString *) createWhenDataWithStartEndDate:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    
    if(item.startDate && item.startDate.length > 0)
    {
        [message appendFormat:@"%@", item.startDateWithoutTime];
    }
    
    if(item.endDate.length > 0)
    {
        [message appendFormat:@" - "];
        [message appendFormat:@"%@",item.endDateWithoutTime];
    }
    
    return message;
}

#pragma mark CREATE PARTY SIZE FIELD
+(void) createPartySizeFieldWithData:(ConciergeDetailObject *)item forRequest:(NSMutableArray *)arrayText withHandle:(NSMutableString* (^)()) handler
{
    NSMutableString *message = handler();
    
    if(message.length > 0)
    {
        PropertyItem *property = [[PropertyItem alloc] init];
        property.title = @"PARTY SIZE";
        property.value = [NSString stringWithString:message];
        [arrayText addObject:property];
    }
}

+(NSMutableString *) createPartySizeDataWithAdultChildNumber:(ConciergeDetailObject *)item
{
    NSMutableString *message = [[NSMutableString alloc] init];
    
    if(item.numberOfAdults && item.numberOfAdults.length > 0)
    {
        [message appendFormat:@"%@", item.numberOfAdults];
        [message appendFormat:item.numberOfAdults.integerValue > 1 ? @" Adults, " : @" Adult, "];
    }
    
    if(item.numberOfChildrens && item.numberOfChildrens.length > 0)
    {
        [message appendFormat:@"%@", item.numberOfChildrens];
        [message appendFormat:item.numberOfChildrens.integerValue > 1 ? @" Children" : @" Child"];
    }
    
    return message;
}

#pragma mark LAST UPDATE FIELD
+(void) createLastUpdatedFieldWithData:(ConciergeDetailObject *)item forRequest:(NSMutableArray *)arrayText
{
    if(item.createdDate.length > 0)
    {
        PropertyItem *property = [[PropertyItem alloc] init];
        property.title = [item.requestStatus isEqualToString:CLOSE_STATUS] ? @"CLOSED" : @"LAST UPDATED";
        property.value = item.createdDate;
        [arrayText addObject:property];
    }
}
@end
