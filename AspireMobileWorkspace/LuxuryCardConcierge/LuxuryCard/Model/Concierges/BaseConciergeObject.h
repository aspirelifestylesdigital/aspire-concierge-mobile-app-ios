//
//  BaseConciergeObject.h
//  LuxuryCard
//
//  Created by Chung Mai on 4/3/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseConciergeObject : NSObject

@property (strong, nonatomic) NSString* requestType;
@property (strong, nonatomic) NSString* requestTypeMapping;
@property (strong, nonatomic) NSString* requestTypeForVenus;
@property (strong, nonatomic) NSString* requestDetail;
@property (strong, nonatomic) NSString* requestCategory;

-(NSString *)getStringType:(NSString*)type;
-(NSString*)convertStringToDate:(NSString*)strDate;
-(NSString*)convertStringToDateWithOutTime:(NSString*)strDate;
-(NSString*)stringValueForDict:(NSString*)string;
-(NSString*)requestDetailForDisplay;

@end
