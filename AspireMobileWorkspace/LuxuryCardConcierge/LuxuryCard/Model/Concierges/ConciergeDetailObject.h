//
//  ConciergeDetailObject.h
//  LuxuryCard
//
//  Created by Viet Vo on 8/29/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EnumConstant.h"
#import "BaseConciergeObject.h"

@interface ConciergeDetailObject : BaseConciergeObject
@property (assign, nonatomic) BOOL isCreated;
@property (strong, nonatomic) NSString* ePCCaseID;
@property (strong, nonatomic) NSDictionary* requestDetailObject;
@property (strong, nonatomic) NSString* transactionId;
@property (strong, nonatomic) NSString* createdDate;
@property (strong, nonatomic) NSString* emailAddress1;
@property (strong, nonatomic) NSString* eventDate;
@property (strong, nonatomic) NSString* firstName;
@property (strong, nonatomic) NSString* functionality;
@property (strong, nonatomic) NSString* lastName;
@property (strong, nonatomic) NSString* phoneNumber;
@property (strong, nonatomic) NSString* pickUpDate;
@property (strong, nonatomic) NSString* prefresponse;
@property (strong, nonatomic) NSString* requestMode;
@property (strong, nonatomic) NSString* requestStatus;
@property (strong, nonatomic) NSString* salutation;
@property (strong, nonatomic) NSString* startDate;
@property (strong, nonatomic) NSString* endDate;

@property (strong, nonatomic) NSString* startDateWithoutTime;
@property (strong, nonatomic) NSString* endDateWithoutTime;
//
@property (strong, nonatomic) NSString* numberOfAdults;
@property (strong, nonatomic) NSString* numberOfPassengers;
@property (strong, nonatomic) NSString* numberOfChildrens;
//
@property (strong, nonatomic) NSString* state;
@property (strong, nonatomic) NSString* city;
@property (strong, nonatomic) NSString* country;
//
@property (assign, nonatomic) NSInteger requestTypeID;
@property (strong, nonatomic) NSString* requestTypeName;


//private value for type
@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* commonMessage;
@property (strong, nonatomic) NSString* pickUpLocation;
@property (strong, nonatomic) NSString* dropOffLocation;

//
@property (strong, nonatomic) NSString* startLocation;
@property (strong, nonatomic) NSString* endLocation;
@property (strong, nonatomic) NSString* numberOfInfants;
@property (strong, nonatomic) NSString* deliveryDate;
@property (strong, nonatomic) NSString* flowerType;
@property (strong, nonatomic) NSString* dAddress;
@property (strong, nonatomic) NSString* bouquetQuantity;

- (id)initFromDict:(NSDictionary*)dict;
//- (ConciergeDetailObject *)copyWithZone:(NSZone *)zone;
@end
