//
//  ConciergeDetailObject.m
//  LuxuryCard
//
//  Created by Viet Vo on 8/29/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "ConciergeDetailObject.h"
#import "Constant.h"
#import "Common.h"

@interface ConciergeDetailObject() <NSCopying>

@end

@implementation ConciergeDetailObject

- (id)initFromDict:(NSDictionary*)dict {
    self.isCreated = YES;
    self.ePCCaseID = [dict objectForKey:@"EPCCaseID"];
    self.transactionId = [self stringValueForDict:[dict objectForKey:@"TransactionID"]];
    self.createdDate = [self convertStringToDate:[dict objectForKey:@"CREATEDDATE"]];
    self.emailAddress1 = [self stringValueForDict:[dict objectForKey:@"EMAILADDRESS1"]];
    self.eventDate = [self convertStringToDate:[dict objectForKey:@"EVENTDATE"]];
    self.firstName = [self stringValueForDict:[dict objectForKey:@"FIRSTNAME"]];
    self.functionality = [self stringValueForDict:[dict objectForKey:@"FUNCTIONALITY"]];
    self.lastName = [self stringValueForDict:[dict objectForKey:@"LASTNAME"]];
    self.phoneNumber = [self stringValueForDict:[dict objectForKey:@"PHONENUMBER"]];
    self.pickUpDate = [self convertStringToDate:[dict objectForKey:@"PICKUPDATE"]];
    self.prefresponse = [self stringValueForDict:[dict objectForKey:@"PREFRESPONSE"]];
    self.requestDetail = [self stringValueForDict:[dict objectForKey:@"REQUESTDETAILS"]];
    if(self.requestDetail.length > 0)
    {
        self.requestDetail = [self.requestDetail stringByReplacingOccurrencesOfString:@"##" withString:@"\n"];
    }
    self.requestDetailObject = [self getObjectByString:[self stringValueForDict:[dict objectForKey:@"REQUESTDETAILS"]]];
    self.requestMode = [self stringValueForDict:[dict objectForKey:@"REQUESTMODE"]];
    self.requestStatus = [self stringValueForDict:[dict objectForKey:@"REQUESTSTATUS"]];
    self.requestTypeMapping = [self getStringType:[self stringValueForDict:[dict objectForKey:@"REQUESTTYPE"]]];
    self.requestType = [self stringValueForDict:[dict objectForKey:@"REQUESTTYPE"]];
    self.salutation = [self stringValueForDict:[dict objectForKey:@"SALUTATION"]];
    self.startDate = [self convertStringToDate:[dict objectForKey:@"STARTDATE"]];
    self.endDate = [self convertStringToDate:[dict objectForKey:@"ENDDATE"]];
    
    self.startDateWithoutTime = [self convertStringToDateWithOutTime:[dict objectForKey:@"STARTDATE"]];
    self.endDateWithoutTime = [self convertStringToDateWithOutTime:[dict objectForKey:@"ENDDATE"]];
    //
    self.numberOfAdults = [[self stringValueForDict:[dict objectForKey:@"NUMBEROFADULTS"]] isEqualToString:@""] ? @"0" : [self stringValueForDict:[dict objectForKey:@"NUMBEROFADULTS"]];
    self.numberOfPassengers = [self stringValueForDict:[dict objectForKey:@"NUMBEROFPASSENGERS"]];
    self.numberOfChildrens = [[self stringValueForDict:[dict objectForKey:@"NUMBEROFCHILDRENS"]] isEqualToString:@""] ? @"0" : [self stringValueForDict:[dict objectForKey:@"NUMBEROFCHILDRENS"]];
    //
    self.state = [self stringValueForDict:[dict objectForKey:@"STATE"]];
    self.city = [self stringValueForDict:[dict objectForKey:@"CITY"]];
    self.country = [self stringValueForDict:[dict objectForKey:@"COUNTRY"]];
    //
    self.name = [self getNameByType:self.requestType];
    self.commonMessage = [self stringValueForDict:[self.requestDetailObject objectForKey:@"SpecialReq"]];
    
    self.pickUpLocation = [self stringValueForDict:[self.requestDetailObject objectForKey:@"PickLocation"]];
    self.dropOffLocation = [self stringValueForDict:[self.requestDetailObject objectForKey:@"DropLocation"]];
    self.startLocation = [self stringValueForDict:[self.requestDetailObject objectForKey:@"StartLocation"]];
    self.endLocation = [self stringValueForDict:[self.requestDetailObject objectForKey:@"EndLocation"]];
    self.numberOfInfants = [[self stringValueForDict:[self.requestDetailObject objectForKey:@"NumberOfInfants"]] isEqualToString:@""] ? @"0" : [self stringValueForDict:[self.requestDetailObject objectForKey:@"NumberOfInfants"]];
    self.deliveryDate = [self convertStringToDate:[dict objectForKey:@"EVENTDATE"]];    self.flowerType = [self stringValueForDict:[self.requestDetailObject objectForKey:@"FlowerType"]];
    
    self.dAddress = [self stringValueForDict:[self.requestDetailObject objectForKey:@"DAddress"]];
    self.bouquetQuantity = [self stringValueForDict:[self.requestDetailObject objectForKey:@"BouquetQuantity"]];
    return self;
}


- (NSDictionary *)getObjectByString:(NSString *)item
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    NSArray *requestDetailsArray = item ? [item componentsSeparatedByString:@"|"] : nil;
    if(requestDetailsArray.count > 0)
    {
        for (int i = 0; i< requestDetailsArray.count; i++) {
            if ([requestDetailsArray[i] rangeOfString:@":"].location != NSNotFound) {
                NSString *key = [requestDetailsArray[i] substringToIndex:[requestDetailsArray[i] rangeOfString:@":"].location];
                key = [key stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                
                NSString *value = [requestDetailsArray[i] substringFromIndex:[requestDetailsArray[i] rangeOfString:@":"].location+1];
                value = [value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                
                [dict setValue:value forKey:key];
            }
            else{
                
                [dict setValue:item forKey:@"SpecialReq"];
            }
        }
    }
    return dict;
}

- (NSString*)getNameByType:(NSString*)type{
    NSString *name = @"";
    if(self.requestDetailObject)
    {
        if([type isEqualToString:DINING_REQUEST_TYPE])
        {
            name = [self stringValueForDict:[self.requestDetailObject objectForKey:@"RestaurantName"]];
        }
        else if([type isEqualToString:HOTEL_REQUEST_TYPE])
        {
            name = [self stringValueForDict:[self.requestDetailObject objectForKey:@"HotelName"]];
        }
        else if([type isEqualToString:GOLF_REQUEST_TYPE] || [type isEqualToString:GOLF_RED_REQUEST_TYPE])
        {
            name = [self stringValueForDict:[self.requestDetailObject objectForKey:@"ReservationName"]];
            if ([name isEqualToString:@""]) {
                name = [self stringValueForDict:[self.requestDetailObject objectForKey:@"GolfCourseName"]];
            }
        }
        else if([type isEqualToString:TRANSPORTATION_REQUEST_TYPE] || [type isEqualToString:TRANSFER_REQUEST_TYPE])
        {
            name = [self stringValueForDict:[self.requestDetailObject objectForKey:@"DriverName"]];
        }
        else if([type isEqualToString:ENTERTAINMENT_REQUEST_TYPE_SERVICE] || [type isEqualToString:ENTERTAINMENT_REQUEST_TYPE])
        {
            name = [self stringValueForDict:[self.requestDetailObject objectForKey:@"EventName"]];
        }
        else if([type isEqualToString:TOUR_REQUEST_TYPE] || [type isEqualToString:AIRPORT_SERVICE_REQUEST_TYPE])
        {
            name = [self stringValueForDict:[self.requestDetailObject objectForKey:@"TourName"]];
        }else if ([type isEqualToString:CRUISE_REQUEST_TYPE]){
            name = [self stringValueForDict:[self.requestDetailObject objectForKey:@"CruiseName"]];
        }
    }
    
    return [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

-(NSString *)requestTypeForVenus{
    NSArray *detailInfo = [self.requestDetail componentsSeparatedByString:@"##"];
    NSString *categoryName = detailInfo[0];
    if([categoryName containsString:@"CategoryName"])
    {
        NSArray *categoryNameInfo = [categoryName componentsSeparatedByString:@":"];
        NSString *requestType = categoryNameInfo[1];
        return [requestType stringByTrimmingCharactersInSet:
                [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
    
    return @"";
}


- (id)copyWithZone:(NSZone *)zone
{
    ConciergeDetailObject *copy = [[[self class] alloc] init];
    
    if (copy) {
        // Copy NSObject subclasses
        copy.isCreated              = _isCreated;
        copy.requestDetailObject    = _requestDetailObject;
        copy.transactionId          = _transactionId;
        copy.createdDate            = _createdDate;
        copy.emailAddress1          = _emailAddress1;
        copy.eventDate              = _eventDate;
        copy.firstName              = _firstName;
        copy.functionality          = _functionality;
        copy.lastName               = _lastName;
        copy.phoneNumber            = _phoneNumber;
        copy.pickUpDate             = _pickUpDate;
        copy.prefresponse           = _prefresponse;
        copy.requestDetail          = self.requestDetail;
        copy.requestMode            = _requestMode;
        copy.requestStatus          = _requestStatus;
        copy.requestType            = self.requestType;
        copy.requestTypeMapping     = self.requestTypeMapping;
        copy.salutation             = _salutation;
        copy.startDate              = _startDate;
        copy.endDate                = _endDate;
        copy.startDateWithoutTime   = _startDateWithoutTime;
        copy.endDateWithoutTime     = _endDateWithoutTime;
        copy.numberOfAdults         = _numberOfAdults;
        copy.numberOfPassengers     = _numberOfPassengers;
        copy.numberOfChildrens      = _numberOfChildrens;
        copy.state                  = _state;
        copy.city                   = _city;
        copy.country                = _country;
        copy.requestTypeID          = _requestTypeID;
        copy.requestTypeName        = _requestTypeName;
        copy.name                   = _name;
        copy.commonMessage          = _commonMessage;
        copy.pickUpLocation         = _pickUpLocation;
        copy.dropOffLocation        = _dropOffLocation;
        copy.startLocation          = _startLocation;
        copy.endLocation            = _endLocation;
        copy.numberOfInfants        = _numberOfInfants;
        copy.deliveryDate           = _deliveryDate;
        copy.flowerType             = _flowerType;
        copy.dAddress               = _dAddress;
        copy.bouquetQuantity        = _bouquetQuantity;
    }
    
    return copy;
}

@end
