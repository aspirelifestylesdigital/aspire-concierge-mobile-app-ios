//
//  ConciergeObject.m
//  LuxuryCard
//
//  Created by Viet Vo on 8/29/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//


/*
 - Dining
 - Recommend a Dining
 - Hotel
 - Recommend a Hotel
 - Others
 - Golf
 - Golf recommend
 - Car Rental
 - Transfer
 - Entertainment
 - Book a Tour
 - Recommend a Tour
 - Flight
 - Cruise
 - FlowerDelivery
 - PrivateJet
 */

#import "ConciergeObject.h"
#import "Constant.h"
#import "Common.h"

@implementation ConciergeObject

@synthesize rowNumber, ePCCaseID, transactionID, prefResponse, phoneNumber, emailAddress1, requestStatus, requestType, city, country, eventDate, pickupDate, startDate, endDate, createdDate, requestMode, contentString, myRequestType, functionality, situation, numberOfAdults, numberOfChildrens, numberOfPassengers, dietrestrict, requestDetailObject;


- (id)initFromDict:(NSDictionary*)dict{
    rowNumber = [[dict objectForKey:@"RowNumber"] integerValue];
    ePCCaseID = [dict objectForKey:@"EPCCaseID"];
    transactionID = [dict objectForKey:@"TransactionID"];
    prefResponse = [dict objectForKey:@"PREFRESPONSE"];
    phoneNumber = [dict objectForKey:@"PHONENUMBER"];
    emailAddress1 = [dict objectForKey:@"EMAILADDRESS1"];
    requestStatus = [dict objectForKey:@"REQUESTSTATUS"];
    requestType = [dict objectForKey:@"REQUESTTYPE"];
    self.requestTypeMapping = [self getStringType:[self stringValueForDict:[dict objectForKey:@"REQUESTTYPE"]]];
    city = [dict objectForKey:@"CITY"];
    country = [dict objectForKey:@"COUNTRY"];
    eventDate = [self convertStringToDate:[dict objectForKey:@"EVENTDATE"]];
    pickupDate = [self convertStringToDate:[dict objectForKey:@"PICKUPDATE"]];
    startDate = [self convertStringToDateWithOutTime:[dict objectForKey:@"STARTDATE"]];
    endDate = [self convertStringToDateWithOutTime:[dict objectForKey:@"ENDDATE"]];
    createdDate = [self convertStringToDate:[dict objectForKey:@"CREATEDDATE"]];
    requestMode = [dict objectForKey:@"REQUESTMODE"];
    self.requestDetail = [dict objectForKey:@"REQUESTDETAILS"];
    if(self.requestDetail.length > 0)
    {
        self.requestDetail = [self.requestDetail stringByReplacingOccurrencesOfString:@"##" withString:@"\n"];
    }
    self.name = [self getNameByType:self.requestType];
    functionality = [dict objectForKey:@"FUNCTIONALITY"];
    situation = [dict objectForKey:@"SITUATION"];
    numberOfAdults = [dict objectForKey:@"NUMBEROFADULTS"];
    numberOfChildrens = [dict objectForKey:@"NUMBEROFCHILDRENS"];
    numberOfPassengers = [dict objectForKey:@"NUMBEROFPASSENGERS"];
    dietrestrict = [dict objectForKey:@"DIETRESTRICT"];
    
    
    myRequestType = [self parsedRequestType:[dict objectForKey:@"REQUESTTYPE"]];

    requestDetailObject = [self getObjectByString:[self stringValueForDict:[dict objectForKey:@"REQUESTDETAILS"]]];
    self.pickUpLocation = [self stringValueForDict:[self.requestDetailObject objectForKey:@"PickLocation"]];
    self.dropOffLocation = [self stringValueForDict:[self.requestDetailObject objectForKey:@"DropLocation"]];
    self.deliveryDate = [self convertStringToDate:[dict objectForKey:@"EVENTDATE"]];
    return self;
}

-(NSString*)getStringName {
    return [self getNameByType:self.requestType];
}


- (NSDictionary *)getObjectByString:(NSString *)item
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    NSArray *requestDetailsArray = item ? [item componentsSeparatedByString:@"|"] : nil;
    if(requestDetailsArray.count > 0)
    {
        for (int i = 0; i< requestDetailsArray.count; i++) {
            if ([requestDetailsArray[i] rangeOfString:@":"].location != NSNotFound) {
                NSString *key = [requestDetailsArray[i] substringToIndex:[requestDetailsArray[i] rangeOfString:@":"].location];
                key = [key stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                
                NSString *value = [requestDetailsArray[i] substringFromIndex:[requestDetailsArray[i] rangeOfString:@":"].location+1];
                value = [value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                
                [dict setValue:value forKey:key];
            }
        }
    }
    return dict;
}

- (NSString*)getNameByType:(NSString*)type{
    NSString *name = @"";
    if(self.requestDetailObject)
    {
        if([type isEqualToString:DINING_REQUEST_TYPE])
        {
            name = [self stringValueForDict:[self.requestDetailObject objectForKey:@"RestaurantName"]];
        }
        else if([type isEqualToString:HOTEL_REQUEST_TYPE])
        {
            name = [self stringValueForDict:[self.requestDetailObject objectForKey:@"HotelName"]];
        }
        else if([type isEqualToString:GOLF_REQUEST_TYPE] || [type isEqualToString:GOLF_RED_REQUEST_TYPE])
        {
            name = [self stringValueForDict:[self.requestDetailObject objectForKey:@"ReservationName"]];
            if ([name isEqualToString:@""]) {
                name = [self stringValueForDict:[self.requestDetailObject objectForKey:@"GolfCourseName"]];
            }
        }
        else if([type isEqualToString:TRANSPORTATION_REQUEST_TYPE] || [type isEqualToString:TRANSFER_REQUEST_TYPE])
        {
            name = [self stringValueForDict:[self.requestDetailObject objectForKey:@"DriverName"]];
        }
        else if([type isEqualToString:ENTERTAINMENT_REQUEST_TYPE_SERVICE] || [type isEqualToString:ENTERTAINMENT_REQUEST_TYPE])
        {
            name = [self stringValueForDict:[self.requestDetailObject objectForKey:@"EventName"]];
        }
        else if([type isEqualToString:TOUR_REQUEST_TYPE] || [type isEqualToString:AIRPORT_SERVICE_REQUEST_TYPE])
        {
            name = [self stringValueForDict:[self.requestDetailObject objectForKey:@"TourName"]];
        }else if ([type isEqualToString:CRUISE_REQUEST_TYPE]){
            name = [self stringValueForDict:[self.requestDetailObject objectForKey:@"CruiseName"]];
        }
    }
    
    return [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

-(enum MY_REQUEST_TYPE) parsedRequestType:(NSString*) type{
    if ([type isEqualToString:DINING_REQUEST_TYPE]) {
        return MY_REQUEST_TYPE_DINING;
    }
    if ([type isEqualToString:HOTEL_REQUEST_TYPE]) {
        return MY_REQUEST_TYPE_HOTEL;
    }
    if ([type isEqualToString:GOLF_REQUEST_TYPE] || [type isEqualToString:GOLF_RED_REQUEST_TYPE]) {
        return MY_REQUEST_TYPE_GOLF;
    }
    if ([type isEqualToString:TRANSPORTATION_REQUEST_TYPE] || [type isEqualToString:TRANSFER_REQUEST_TYPE]) {
        return MY_REQUEST_TYPE_CAR_RENTAL;
    }
    if ([type isEqualToString:ENTERTAINMENT_REQUEST_TYPE]) {
        return MY_REQUEST_TYPE_ENTERTAIMENT;
    }
    if ([type isEqualToString:ENTERTAINMENT_REQUEST_TYPE_SERVICE]) {
        return MY_REQUEST_TYPE_ENTERTAIMENT;
    }
    if ([type isEqualToString:TOUR_REQUEST_TYPE]) {
        return MY_REQUEST_TYPE_BOOK_A_TOUR;
    }
    if ([type isEqualToString:AIRPORT_SERVICE_REQUEST_TYPE]) {
        return MY_REQUEST_TYPE_FLIGHT;
    }
    if ([type isEqualToString:CRUISE_REQUEST_TYPE]) {
        return MY_REQUEST_TYPE_RECOMMEND_CRUISE;
    }
    if ([type isEqualToString:FLOWERS_REQUEST_TYPE]) {
        return MY_REQUEST_TYPE_FLOWER_DELIVERY;
    }
    if ([type isEqualToString:TRAVEL_REQUEST_TYPE]) {
        return MY_REQUEST_TYPE_PRIVATE_JET;
    }
    return MY_REQUEST_TYPE_OTHERS;
}





@end
