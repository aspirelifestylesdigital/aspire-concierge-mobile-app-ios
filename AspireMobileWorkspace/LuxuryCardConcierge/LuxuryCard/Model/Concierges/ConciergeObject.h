//
//  ConciergeObject.h
//  LuxuryCard
//
//  Created by Viet Vo on 8/29/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EnumConstant.h"
#import "BaseConciergeObject.h"


@interface ConciergeObject : BaseConciergeObject


@property (nonatomic) NSInteger rowNumber;
@property (strong, nonatomic) NSString* ePCCaseID;
@property (strong, nonatomic) NSString* transactionID;
@property (strong, nonatomic) NSString* prefResponse;
@property (strong, nonatomic) NSString* phoneNumber;
@property (strong, nonatomic) NSString* emailAddress1;
@property (strong, nonatomic) NSString* requestStatus;
@property (strong, nonatomic) NSString* city;
@property (strong, nonatomic) NSString* country;

@property (strong, nonatomic) NSString* eventDate;
@property (strong, nonatomic) NSString* pickupDate;
@property (strong, nonatomic) NSString* startDate;
@property (strong, nonatomic) NSString* endDate;
@property (strong, nonatomic) NSString* createdDate;
@property (strong, nonatomic) NSString* requestMode;

@property (strong, nonatomic) NSString* contentString;

///------
@property (strong, nonatomic) NSString* functionality;
@property (strong, nonatomic) NSString* situation;
@property (strong, nonatomic) NSString* numberOfAdults;
@property (strong, nonatomic) NSString* numberOfChildrens;
@property (strong, nonatomic) NSString* numberOfPassengers;
@property (strong, nonatomic) NSString* dietrestrict;

@property (strong, nonatomic) NSDictionary* requestDetailObject;

@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* pickUpLocation;
@property (strong, nonatomic) NSString* dropOffLocation;
@property (strong, nonatomic) NSString* deliveryDate;

@property enum MY_REQUEST_TYPE myRequestType;

- (id)initFromDict:(NSDictionary*)dict;
-(NSString*)getStringName;
-(NSString*)convertStringToDate:(NSString*)strDate;
- (NSString*)getNameByType:(NSString*)type;
-(enum MY_REQUEST_TYPE) parsedRequestType:(NSString*) type;
- (NSDictionary *)getObjectByString:(NSString *)item;

@end
