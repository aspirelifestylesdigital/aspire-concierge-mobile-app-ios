//
//  BaseConciergeObject.m
//  LuxuryCard
//
//  Created by Chung Mai on 4/3/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//
#import "Constant.h"
#import "BaseConciergeObject.h"
#import "Common.h"

@implementation BaseConciergeObject


-(NSString *)getStringType:(NSString*)type {
    if ([type isEqualToString:DINING_REQUEST_TYPE]) {
        return  @"Restaurant Request";
    } else if ([type isEqualToString:HOTEL_REQUEST_TYPE]) {
        return  @"HOTEL AND B&B Request";
    } else if ([type isEqualToString:GOLF_REQUEST_TYPE] || [type isEqualToString:GOLF_RED_REQUEST_TYPE]) {
        return  @"Golf Request";
    } else if ([type isEqualToString:TRANSPORTATION_REQUEST_TYPE]) {
        return  @"Limo and Sedan Request";
    } else if ([type isEqualToString:TRANSFER_REQUEST_TYPE]) {
        return  @"Limo and Sedan Request";
    } else if ([type isEqualToString:ENTERTAINMENT_REQUEST_TYPE]) {
        return  @"Entertainment Request";
    } else if ([type isEqualToString:ENTERTAINMENT_REQUEST_TYPE_SERVICE]) {
        return  @"Entertainment Request";
    } else if ([type isEqualToString:TOUR_REQUEST_TYPE]) {
        return  @"Sightseeing/Tours Request";
    } else if ([type isEqualToString:AIRPORT_SERVICE_REQUEST_TYPE]) {
        return  @"Airport Services Request";
    } else if ([type isEqualToString:CRUISE_REQUEST_TYPE]) {
        return  @"Cruise Request";
    } else if ([type isEqualToString:FLOWERS_REQUEST_TYPE]) {
        return  @"Flowers/Gift Basket Request";
    } else if ([type isEqualToString:TRAVEL_REQUEST_TYPE]) {
        return  @"Private Jet Request";
    }
    else if ([type isEqualToString:VACATION_PACKAGE_REQUEST_TYPE]) {
        return  @"Vacation Packages Request";
    }else {
        return  @"Other Request";
    }
}

-(NSString*)convertStringToDate:(NSString*)strDate{
    NSString *date = [self stringValueForDict:strDate];
    if ([date isEqualToString:@""]) {
        return @"";
    }
    if ([date rangeOfString:@"T"].location != NSNotFound) {
        return stringDateWithFormat(@"MMMM dd, yyyy h:mm aa", dateFromISO8601Time(date));
    }else{
        return stringDateWithFormat(@"MMMM dd, yyyy h:mm aa", dateFromTime(date));
    }
}


-(NSString*)convertStringToDateWithOutTime:(NSString*)strDate{
    NSString *date = [self stringValueForDict:strDate];
    if ([date isEqualToString:@""]) {
        return @"";
    }
    if ([date rangeOfString:@"T"].location != NSNotFound) {
        return stringDateWithFormat(@"MMMM dd, yyyy", dateFromISO8601Time(date));
    }else{
        return stringDateWithFormat(@"MMMM dd, yyyy", dateFromTime(date));
    }
}

-(NSString*)stringValueForDict:(NSString*)string{
    if (![string isKindOfClass:[NSNull class]] && string != nil && string) {
        return string;
    }
    return @"";
}

-(NSString*)requestDetailForDisplay{
    NSArray *detailInfo = [self.requestDetail componentsSeparatedByString:@"\n"];
    NSString *categoryName = detailInfo[0];
    
    if([categoryName containsString:@"CategoryName"] || [categoryName containsString:@"Category Name"])
    {
        NSArray *categoryNameInfo = [categoryName componentsSeparatedByString:@":"];
        self.requestCategory = categoryNameInfo[1];
        if(self.requestDetail.length > categoryName.length)
        {
            return [self.requestDetail substringFromIndex:(categoryName.length + 1)];
        }
        return @"";
    }
    else{
        return self.requestDetail;
    }
}

@end
