//
//  BINItem.h
//  MobileConcierge
//
//  Created by Chung Mai on 7/5/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BINItem : NSObject

@property(assign, nonatomic) BOOL valid;
@property(strong, nonatomic) NSString *message;

@end
