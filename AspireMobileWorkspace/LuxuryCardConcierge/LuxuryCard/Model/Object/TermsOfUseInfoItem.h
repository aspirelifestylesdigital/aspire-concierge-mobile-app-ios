//
//  TermsOfUseInfoItem.h
//  MobileConcierge
//
//  Created by user on 6/26/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "BaseItem.h"

@interface TermsOfUseInfoItem : BaseItem
@property (nonatomic, strong) NSString *textInfo;
@end
