//
//  TileItem.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/6/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "TileItem.h"
#import "CCAMapCategories.h"

@implementation TileItem

-(NSString *)name
{
    return self.title;
}

-(NSString *)offer
{
    return self.tileText;
}

-(NSString *)imageURL
{
    return self.tileImage;
}

-(NSString *)address
{
    NSString* add = @" ";
    if([[CCAMapCategories getCategoriesIDsMutilCities] containsObject:self.ID]) {
        add = @"Multiple Cities";
    }
    return add;
}

-(BOOL) isOffer
{
    return YES;
}

-(NSString *)categoryName
{
    if([self.tileCategory compare:@"Dining" options:NSCaseInsensitiveSearch] == NSOrderedSame || ([self.tileCategory compare:@"Specialty Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.tileSubCategory compare:@"Culinary Experiences" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return @"DINING";
    }
    else if([self.tileCategory compare:@"Hotels" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"HOTELS";
    }
    else if(([self.currentCategoryCode isEqualToString:@"flowers"] || [self.currentCategoryCode isEqualToString:@"all"]) && [self.tileCategory compare:@"Flowers" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"FLOWERS";
    }
    else if(([self.tileCategory compare:@"Tickets" options:NSCaseInsensitiveSearch] == NSOrderedSame)
            || ([self.tileCategory compare:@"Specialty Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame &&
                ([self.tileSubCategory compare:@"Entertainment Experiences" options:NSCaseInsensitiveSearch] == NSOrderedSame
                 || [self.tileSubCategory compare:@"Major Sports Events" options:NSCaseInsensitiveSearch] == NSOrderedSame)))
    {
        return @"ENTERTAINMENT";
    }
    else if(([self.currentCategoryCode isEqualToString:@"golf"] || [self.currentCategoryCode isEqualToString:@"all"])
            && (([self.tileCategory compare:@"Golf" options:NSCaseInsensitiveSearch] == NSOrderedSame)
            || ([self.tileCategory compare:@"Golf Merchandise" options:NSCaseInsensitiveSearch] == NSOrderedSame)
            || ([self.tileCategory compare:@"Specialty Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame &&
                [self.tileSubCategory compare:@"Golf Experiences" options:NSCaseInsensitiveSearch] == NSOrderedSame)))
    {
        return @"GOLF";
    }
    else if([self.tileCategory compare:@"Vacation Packages" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"VACATION\n PACKAGES";
    }
    else if([self.tileCategory compare:@"Cruises" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"CRUISE";
    }
    else  if([self.tileCategory compare:@"VIP Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.tileSubCategory compare:@"Sightseeing" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"TOURS";
    }
    else if([self.tileCategory compare:@"Private Jet Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"TRAVEL";
    }
    else  if([self.tileCategory compare:@"VIP Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.tileSubCategory compare:@"Airport Services" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"AIRPORT\n SERVICES";
    }
    else  if([self.tileCategory compare:@"VIP Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.tileSubCategory compare:@"Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"TRAVEL\n SERVICES";
    }
    else if([self.tileCategory compare:@"Transportation" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"TRANSPORTATION";
    }
    else if(([self.currentCategoryCode isEqualToString:@"shopping"] || [self.currentCategoryCode isEqualToString:@"all"])
            && ([self.tileCategory compare:@"Golf Merchandise" options:NSCaseInsensitiveSearch] == NSOrderedSame
            || [self.tileCategory compare:@"Flowers" options:NSCaseInsensitiveSearch] == NSOrderedSame
            || [self.tileCategory compare:@"Wine" options:NSCaseInsensitiveSearch] == NSOrderedSame
            || [self.tileCategory compare:@"Retail Shopping" options:NSCaseInsensitiveSearch] == NSOrderedSame
            /*|| [self.tileCategory compare:@"Golf" options:NSCaseInsensitiveSearch] == NSOrderedSame*/))
    {
        return @"SHOPPING";
    }
    else
    {
        return @"";
    }
}

-(NSString *)categoryCode
{
    if([self.tileCategory compare:@"Dining" options:NSCaseInsensitiveSearch] == NSOrderedSame || ([self.tileCategory compare:@"Specialty Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.tileSubCategory compare:@"Culinary Experiences" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return @"dining";
    }
    else if([self.tileCategory compare:@"Hotels" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"hotels";
    }
    else if(([self.currentCategoryCode isEqualToString:@"flowers"] || [self.currentCategoryCode isEqualToString:@"all"])
            && ([self.tileCategory compare:@"Flowers" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return @"flowers";
    }
    else if(([self.tileCategory compare:@"Tickets" options:NSCaseInsensitiveSearch] == NSOrderedSame)
            || ([self.tileCategory compare:@"Specialty Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame &&
                ([self.tileSubCategory compare:@"Entertainment Experiences" options:NSCaseInsensitiveSearch] == NSOrderedSame
                 || [self.tileSubCategory compare:@"Major Sports Events" options:NSCaseInsensitiveSearch] == NSOrderedSame)))
    {
        return @"entertainment";
    }
    else if(([self.currentCategoryCode isEqualToString:@"golf"] || [self.currentCategoryCode isEqualToString:@"all"])
            && (([self.tileCategory compare:@"Golf" options:NSCaseInsensitiveSearch] == NSOrderedSame)
                || ([self.tileCategory compare:@"Golf Merchandise" options:NSCaseInsensitiveSearch] == NSOrderedSame)
                || ([self.tileCategory compare:@"Specialty Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame &&
                    [self.tileSubCategory compare:@"Golf Experiences" options:NSCaseInsensitiveSearch] == NSOrderedSame)))
    {
        return @"golf";
    }
    else if([self.tileCategory compare:@"Vacation Packages" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"vacation packages";
    }
    else if([self.tileCategory compare:@"Cruises" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"cruise";
    }
    else  if([self.tileCategory compare:@"VIP Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.tileSubCategory compare:@"Sightseeing" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"tours";
    }
    else if([self.tileCategory compare:@"Private Jet Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"travel";
    }
    else  if([self.tileCategory compare:@"VIP Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.tileSubCategory compare:@"Airport Services" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"airport services";
    }
    else  if([self.tileCategory compare:@"VIP Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.tileSubCategory compare:@"Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"travel services";
    }
    else if([self.tileCategory compare:@"Transportation" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"transportation";
    }
    else if(([self.currentCategoryCode isEqualToString:@"shopping"] || [self.currentCategoryCode isEqualToString:@"all"])
            && ([self.tileCategory compare:@"Golf Merchandise" options:NSCaseInsensitiveSearch] == NSOrderedSame
                || [self.tileCategory compare:@"Flowers" options:NSCaseInsensitiveSearch] == NSOrderedSame
                || [self.tileCategory compare:@"Wine" options:NSCaseInsensitiveSearch] == NSOrderedSame
                || [self.tileCategory compare:@"Retail Shopping" options:NSCaseInsensitiveSearch] == NSOrderedSame
                /*|| [self.tileCategory compare:@"Golf" options:NSCaseInsensitiveSearch] == NSOrderedSame*/))
    {
        return @"shopping";
    }
    else
    {
        return @"";
    }
}

@end
