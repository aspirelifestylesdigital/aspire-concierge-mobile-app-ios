//
//  GalleryItem.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/8/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GalleryItem : NSObject

@property (nonatomic, strong) NSString *galleryTitle;
@property (nonatomic, strong) NSString *galleryDescription;
@property (nonatomic, strong) UIImage *galleryImage;
@property (nonatomic, strong) NSString *colorHex;

@end
