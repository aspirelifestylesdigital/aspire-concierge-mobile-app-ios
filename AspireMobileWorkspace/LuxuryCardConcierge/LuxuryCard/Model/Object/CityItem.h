//
//  CityItem.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CityItem : NSObject

@property(nonatomic, strong) NSString *ID;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *cityCode;
@property(nonatomic, strong) NSString *regionCode;
@property(nonatomic, strong) NSString *stateCode;
@property(nonatomic, strong) NSString *countryCode;
@property(nonatomic, strong) NSString *address;
@property(nonatomic, strong) NSString *imageURL;
@property(nonatomic, strong) UIImage *image;
@property(nonatomic, strong) NSString *diningID;
@property(nonatomic, strong) NSString *GeographicRegion;
@property(nonatomic, strong) NSString *SubCategory;
@property(nonatomic, strong) NSArray *subCategories;//For City Guide Data

@end
