//
//  SubCategoryItem.h
//  MobileConcierge
//
//  Created by Chung Mai on 5/30/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SubCategoryItem : NSObject

@property(nonatomic, strong) NSString *ID;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *descriptionItem;
@property(nonatomic, strong) NSString *imageURL;
@property(nonatomic, strong) NSString *level;
@property(nonatomic, strong) NSString *parentCategoryID;
@property(nonatomic, strong) NSString *superCategoryID;
@property(nonatomic, strong) UIImage *image;

-(void)setMappingDataForSubCategory;

@end
