//
//  ExploreItem.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/9/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "BaseItem.h"
#import "AppData.h"

@implementation BaseItem
- (id) init {
    if(self = [super init]) {
        self.distance = CGFLOAT_MAX;
    }
    return self;
}

- (NSString*) strimStringFrom:(NSString*) from withPatterns:(NSArray*) pattern {
    NSString* temp = from;
    for (NSString* p in pattern) {
        temp = [temp stringByReplacingOccurrencesOfString:p withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0,temp.length)];
    }
    return temp;
}
@end
