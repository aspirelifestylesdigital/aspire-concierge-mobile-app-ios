//
//  CategoryItem.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "CategoryItem.h"

@implementation CategoryItem

- (CategoryItem *)copyWithZone:(NSZone *)zone
{
    CategoryItem *copy = [[[self class] alloc] init];
    
    if (copy) {
        copy.ID = self.ID;
        copy.supperCategoryID = self.supperCategoryID;
        copy.categoryName = self.categoryName;
        copy.subCategoryName = self.subCategoryName;
        copy.imageURL = self.imageURL;
        copy.categoryImg = self.categoryImg;
        copy.code = self.code;
        copy.categoriesForService = self.categoriesForService;
    }
    
    return copy;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.ID forKey:@"categoryID"];
    [encoder encodeObject:self.categoryName forKey:@"categoryName"];
    [encoder encodeObject:self.subCategoryName forKey:@"subCategoryName"];
    [encoder encodeObject:self.supperCategoryID forKey:@"supperCategoryID"];
    [encoder encodeObject:self.imageURL forKey:@"imageURL"];
    [encoder encodeObject:self.categoryImg forKey:@"categoryImg"];
    [encoder encodeObject:self.code forKey:@"code"];
    [encoder encodeObject:self.categoriesForService forKey:@"categoriesForService"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.ID = [decoder decodeObjectForKey:@"categoryID"];
        self.categoryName = [decoder decodeObjectForKey:@"categoryName"];
        self.subCategoryName = [decoder decodeObjectForKey:@"subCategoryName"];
        self.supperCategoryID = [decoder decodeObjectForKey:@"supperCategoryID"];
        self.imageURL = [decoder decodeObjectForKey:@"imageURL"];
        self.categoryImg = [decoder decodeObjectForKey:@"categoryImg"];
        self.code = [decoder decodeObjectForKey:@"code"];
        self.categoriesForService = [decoder decodeObjectForKey:@"categoriesForService"];
    }
    return self;
}

@end
