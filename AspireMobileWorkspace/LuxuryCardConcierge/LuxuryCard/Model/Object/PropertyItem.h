//
//  PropertyItem.h
//  LuxuryCard
//
//  Created by Chung Mai on 3/29/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PropertyItem : NSObject

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *value;

@end
