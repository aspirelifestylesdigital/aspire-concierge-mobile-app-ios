//
//  Country.h
//  LuxuryCard
//
//  Created by Viet Vo on 9/8/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Country : NSObject

@property (strong, nonatomic) NSString* countryName;
@property (strong, nonatomic) NSString* countryCode;
@property (strong, nonatomic) NSString* countryNumber;
@property (strong, nonatomic) NSString* countryDescription;

- (id)init:(NSString*)name withCode:(NSString*)code withNumber:(NSString*)number withDesciption:(NSString*)description;
@end
