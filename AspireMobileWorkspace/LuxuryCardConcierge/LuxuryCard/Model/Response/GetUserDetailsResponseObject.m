//
//  GetUserDetailsResponseObject.m
//  MobileConcierge
//
//  Created by user on 6/27/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "GetUserDetailsResponseObject.h"

@implementation GetUserDetailsResponseObject
-(id)initFromDict:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        [self parseCommonResponse:dict];
        if(self.isSuccess)
        {
            self.data = [self parseJson:dict toObject:nil];
        }
    }
    return self;
}

-(id)parseJson:(NSDictionary *)dict toObject:(NSString *)className
{
    return dict;
}

-(void)parseCommonResponse:(NSDictionary *)dict
{
    self.status =  !dict[@"success"] ? YES : [dict boolForKey:@"success"];
    NSArray* mesArr = [dict arrayForKey:@"message"];
    if(mesArr && mesArr.count > 0){
        NSDictionary * error = [mesArr objectAtIndex:0];
        self.message = [error stringForKey:@"message"];
        self.errorCode = [error stringForKey:@"code"];
    }
    else
    {
        self.message = [dict objectForKey:@"message"];
    }
}
@end
