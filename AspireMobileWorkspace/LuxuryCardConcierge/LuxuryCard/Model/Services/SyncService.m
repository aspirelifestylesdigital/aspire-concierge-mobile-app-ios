//
//  SyncService.h
//  USDemo
//
//  Created by Dai Pham on 6/7/17.
//  Copyright (c) 2017. All rights reserved.
//


//app
#import "SyncService.h"
#import "AFHTTPSessionManager.h"

#define MSG_SERVER_WRONG            local(@"I'm currently having trouble connecting to the server. Please try again shortly.")

//**************************************************
@interface SyncService ()@end

@implementation SyncService

+ (void)requestTokenOnDone:(void (^)(id))onDone onError:(void (^)(id))onError {
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [manager.requestSerializer setValue:B2C_ConsumerKey forHTTPHeaderField:@"ConsumerKey"];
    [manager.requestSerializer setValue:B2C_ConsumerSecret forHTTPHeaderField:@"ConsumerSecret"];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    [dictKeyValues setObject:B2C_Callback_Url forKey:@"CallBackURL"];
    [dictKeyValues setObject:[[SessionData shareSessiondata] OnlineMemberID] forKey:@"OnlineMemberId"];
    [dictKeyValues setObject:B2C_DeviceId forKey:@"MemberDeviceId"];
//    [manager GET:[MCD_API_URL stringByAppendingString:GetRequestToken] parameters:dictKeyValues success:^(AFHTTPRequestOperation *operation, id responseObject) {
    [manager GET:[MCD_API_URL stringByAppendingString:GetRequestToken] parameters:dictKeyValues progress:nil success:^(NSURLSessionDataTask * _Nonnull aftask, id  _Nullable responseObject) {
        if(onDone) {
            onDone(responseObject);
        }
        [manager invalidateSessionCancelingTasks:YES];
    }failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        /*failure:^(AFHTTPRequestOperation *operation, NSError *error) {*/
        if(onError)
        {
            onError(error);
        }
        [manager invalidateSessionCancelingTasks:YES];
    }];
}

#pragma mark STATIC - API register device, get user info of device
+ (void)postUrl:(NSString *)url withParams:(NSDictionary *)params OnDone:(void (^)(id))onDone onError:(void (^)(id))onError {
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSLog(@"%s: %@%@",__PRETTY_FUNCTION__,url,params);
    manager.requestSerializer =  [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    AFJSONResponseSerializer *serializer = [AFJSONResponseSerializer serializer];
    [serializer setRemovesKeysWithNullValues:YES];
    [manager setResponseSerializer:serializer];
    
//    [manager POST:url parameters:params timeoutInterval:60 success:^(AFHTTPRequestOperation *operation, id responseObject) {
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull aftask, id  _Nullable responseObject) {
        if(onDone) {
            onDone(responseObject);
        }
        [manager invalidateSessionCancelingTasks:YES];
        
    }
     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    /*failure:^(AFHTTPRequestOperation *operation, NSError *error) {*/
        
        if(onError)
        {
            onError(error);
        }
         [manager invalidateSessionCancelingTasks:YES];
    }];
}

+ (void)getUrl:(NSString *)url withParams:(NSDictionary *)params OnDone:(void (^)(id))onDone onError:(void (^)(id))onError {
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
//    [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull aftask, id  _Nullable responseObject) {
        if(onDone) {
            onDone(responseObject);
        }
        [manager invalidateSessionCancelingTasks:YES];
    }
          failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    /*failure:^(AFHTTPRequestOperation *operation, NSError *error) {*/
        if(onError)
        {
            onError(error);
        }
              [manager invalidateSessionCancelingTasks:YES];
    }];
    
}

+ (void)getUrl:(NSString *)url withTimeout:(NSTimeInterval)timeoutInterval withParams:(NSDictionary *)params OnDone:(void (^)(id))onDone onError:(void (^)(id))onError {
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
//    [manager GET:url parameters:params timeoutInterval:timeoutInterval success:^(AFHTTPRequestOperation *operation, id responseObject) {
    [manager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull aftask, id  _Nullable responseObject) {
        if(onDone) {
            onDone(responseObject);
        }
        [manager invalidateSessionCancelingTasks:YES];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        /*failure:^(AFHTTPRequestOperation *operation, NSError *error) {*/
        if(onError)
        {
            onError(error);
        }
        [manager invalidateSessionCancelingTasks:YES];
    }];
    
}
@end
