//
//  PreferenceObject.m
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 7/26/17.
//  Copyright © 2017 Sunrixe Software Solutions Corporation. All rights reserved.
//

#import "PreferenceObject.h"

@implementation PreferenceObject
@synthesize preferenceID, value, type;

- (id)initFromDict:(NSDictionary*)dict{
    
    preferenceID = [dict objectForKey:@"MYPREFERENCESID"];
    NSString *tempValue = [dict objectForKey:@"VALUE"];
    value = ([tempValue.uppercaseString isEqualToString:@"NA"]) ? @"" : tempValue;
    NSString *typeString = [dict objectForKey:@"TYPE"];
    if ([typeString.uppercaseString isEqualToString:@"DINING"]) {
        type = DiningPreferenceType;
    }else if ([typeString.uppercaseString isEqualToString:@"HOTEL"]) {
        type = HotelPreferenceType;
    }else if ([typeString.uppercaseString isEqualToString:@"CAR RENTAL"]) {
        type = TransportationPreferenceType;
    }
    return self;
}

@end
