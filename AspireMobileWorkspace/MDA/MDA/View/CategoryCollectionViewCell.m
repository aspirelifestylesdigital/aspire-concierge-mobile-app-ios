//
//  CategoryCollectionViewCell.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "CategoryCollectionViewCell.h"
#import "CategoryItem.h"
#import "Common.h"
#import <QuartzCore/QuartzCore.h>
#import "THLabel.h"
#import "UtilStyle.h"

#import "UIImage+Resize.h"
#import "ImageCacheList.h"

#import "UILabel+Extension.h"

const NSString * imageCacheKeyPrefix_test = @"Image_Landing-Controller2-";

@interface CategoryCollectionViewCell() {

    IBOutlet UILabel *testLabel;
    CategoryItem* object;
}

@end

@implementation CategoryCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    resetScaleViewBaseOnScreen(self.contentView);

    testLabel.adjustsFontSizeToFitWidth = YES;
    testLabel.layer.shadowOpacity = 1;
    testLabel.layer.shadowOffset = CGSizeMake(0, 4);
    testLabel.layer.shadowRadius = 3;
    
}

- (void) layoutSubviews {
    [super layoutSubviews];
    UIImage * image = [[ImageCacheList cache] objectForKey: [NSString stringWithFormat:@"%@%@",imageCacheKeyPrefix_test,object.categoryName]];
    if(image.size.width > 50) {
        [self.categoryImg setImage:[UIImage imageWithData:UIImageJPEGRepresentation(image,0.9)]];
        return;
    }
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self getImage:[NSString stringWithFormat:@"%@%@",imageCacheKeyPrefix_test,object.code] imagePath:object.categoryName completion:^(UIImage* image){
            [self.categoryImg setImage:[UIImage imageWithData:UIImageJPEGRepresentation(image,0.9)]];
        }];
    });
}

- (void) getImage: (NSString *) key
        imagePath: (NSString *) imagePath
       completion: (void (^)(UIImage * image)) handler
{
    dispatch_async(dispatch_get_main_queue(), ^(void){
        UIImage * image = [[ImageCacheList cache] objectForKey: key];
        
        if( ! image)
        {
            image = object.categoryImg; //[object.categoryImg imageScaledToFillSize:self.categoryImg.frame.size];  // Macro (UIImage*) for no image
            [[ImageCacheList cache] setObject:image forKey: key];
        }
    
        handler(image);
    });
}

-(void) initCellWithData:(CategoryItem *)item withDisableItem:(BOOL)isDisable;
{
    object = item;
    self.categoryImg.image = item.categoryImg;
//    self.name.text = item.categoryName;
    testLabel.text = item.categoryName;
    if([item.categoryName containsString:@"\n"]) {
        [testLabel setLineSpacing:5*SCREEN_SCALE];
        testLabel.numberOfLines = 2;
    }
    if(isDisable){
        [self.maskView setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.8]];
    }
    else{
        [self.maskView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.4]];
    }
    
//    self.name.adjustsFontSizeToFitWidth = YES;
//    [self.name setMinimumScaleFactor:0.5];
//    [self.name adjustFontSizeToFillItsContents];
}

-(void)setCellToDefault
{
    testLabel.textColor = [UIColor whiteColor];
    [self.maskView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.4]];
}

- (void) prepareForReuse {
    testLabel.numberOfLines = 1;
}
@end
