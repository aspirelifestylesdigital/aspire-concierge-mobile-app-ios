//
//  AppSize.m
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 10/19/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "AppSize.h"
#import "Constant.h"

@implementation AppSize

+(CGFloat) titleGreetingSize
{
    return 44.0f * SCREEN_SCALE;
}

+(CGFloat) largeTitleTextSize
{
    return 36.0f * SCREEN_SCALE;
}

+(CGFloat) letter26px
{
    return 26.0f * SCREEN_SCALE;
}

+(CGFloat) largeDescriptionTextSize
{
    return 24.0f * SCREEN_SCALE;
}

+(CGFloat) descriptionGreetingSize
{
    return 20.0f * SCREEN_SCALE;
}

+(CGFloat) titleTextSize
{
    return 18.0f * SCREEN_SCALE;
}

+(CGFloat) descriptionTextSize
{
    return 16.0f * SCREEN_SCALE;
}

+(CGFloat) headerTextSize
{
    return 14.0f * SCREEN_SCALE;
}

+(CGFloat) moreInfoTextSize
{
    return 12.0f * SCREEN_SCALE;
}


+(CGFloat) titleLetterSpacing
{
    return 1.6;
}

+(CGFloat) descriptionLetterSpacing
{
    return 1.4;
}

+(CGFloat) smallLetterSpacing
{
    return 1.2;
}

+(CGFloat) titleLineSpacing
{
    return 1.25;
}

@end
