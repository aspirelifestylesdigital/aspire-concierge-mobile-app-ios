//
//  NSLayoutConstraint+Multiplier.h
//  BDA
//
//  Created by Viet Vo on 2/21/18.
//  Copyright © 2018 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSLayoutConstraint (Multiplier)

-(instancetype)updateMultiplier:(CGFloat)multiplier;

@end
