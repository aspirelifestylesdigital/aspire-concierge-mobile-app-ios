//
//  MCStandardUIButton.m
//  MobileConcierge
//
//  Created by 😱 on 7/4/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "MCStandardUIButton.h"
#import "MCBaseUIButton.h"

@implementation MCStandardUIButton

- (instancetype) init {
    if (self = [super init]) {
        self.btnType = MCButtonTypeStandard;
    }
    return self;
}

- (void) awakeFromNib {
    [super awakeFromNib];
    
    self.btnType = MCButtonTypeStandard;
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.btnType = MCButtonTypeStandard;
    }
    return self;
}

- (id) initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.btnType = MCButtonTypeStandard;
    }
    return self;
}

@end
