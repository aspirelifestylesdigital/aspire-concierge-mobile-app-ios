//
//  MCBaseUILabel.m
//  MobileConcierge
//
//  Created by 😱 on 7/4/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "MCBaseUILabel.h"

@implementation MCBaseUILabel

#pragma mark - PROPERTIES

- (void) setLabelType:(MCLabelType)labelType {
    
    NSString* colorString = nil;
    NSString* fontName = nil;
    CGFloat   fontSize = LABEL_FONT_SIZE_DETAIL;
    
    switch (labelType) {
        case MCLabelTypeTitle:
            colorString = LABEL_COLOR_TITLE;
            fontName = LABEL_FONT_NAME_TITLE;
            fontSize = LABEL_FONT_SIZE_TITLE;
            break;
        case MCLabelTypeDetail:
            colorString = LABEL_COLOR_DETAIL;
            fontName = LABEL_FONT_NAME_DETAIL;
            fontSize = LABEL_FONT_SIZE_DETAIL;
            break;
        case MCLabelTypeDescription:
            colorString = LABEL_COLOR_DESCRIPTION;
            fontName = LABEL_FONT_NAME_DESCRIPTION;
            fontSize = LABEL_FONT_SIZE_DESCRIPTION;
            break;
        case MCLabelTypeNote:
            colorString = LABEL_COLOR_NOTE;
            fontName = LABEL_FONT_NAME_NOTE;
            fontSize = LABEL_FONT_SIZE_NOTE;
            break;
    }
    
    if(colorString)
        [self setTextFontHexColor:colorString orColor:nil];
    
    if(fontName)
        [self setCustomFontName:fontName withSize:fontSize];
}

#pragma mark - PRIVATE
- (void) setCustomFontName:(NSString*)fontName withSize:(CGFloat)fontSize {
    [self setFont:[UIFont fontWithName:fontName size:fontSize]];
}

- (void) setTextFontHexColor:(NSString*)hexColor orColor:(UIColor*)color {
    if(color) {
        [self setTextColor:color];
    } else {
        if (hexColor.length > 6) {
            [self setTextColor:colorFromHexString(hexColor)];
        }
    }
}

- (void) setBackgroundFromHexColor:(NSString*)hexColor orColor:(UIColor*) color {
    if(color) {
        [self setBackgroundColor:color];
    } else {
        if (hexColor.length > 6) {
            [self setBackgroundColor:colorFromHexString(hexColor)];
        }
    }
}
@end
