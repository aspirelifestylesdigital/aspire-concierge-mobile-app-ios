//
//  MCCommonUILabel.m
//  MobileConcierge
//
//  Created by 😱 on 7/4/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "StyleConstant.h"
#import "MCDetailUILabel.h"

@implementation MCDetailUILabel

- (instancetype) init {
    if (self = [super init]) {
        self.labelType = MCLabelTypeDetail;
    }
    return self;
}

- (void) awakeFromNib {
    [super awakeFromNib];
    
    self.labelType = MCLabelTypeDetail;
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.labelType = MCLabelTypeDetail;
    }
    return self;
}

- (id) initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.labelType = MCLabelTypeDetail;
    }
    return self;
}

@end
