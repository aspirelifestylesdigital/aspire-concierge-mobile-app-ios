//
//  UDASignInViewController.m
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 7/14/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "UDASignInViewController.h"
#import "SWRevealViewController.h"
#import "CreateProfileViewController.h"
#import "UDAForgotPasswordViewController.h"
#import "HomeViewController.h"
#import "WSSignIn.h"
#import "WSB2CGetAccessToken.h"
#import "WSB2CGetUserDetails.h"
#import "WSB2CGetRequestToken.h"
#import "UserRegistrationItem.h"
#import "UtilStyle.h"
#import "UIButton+Extension.h"
#import "NSString+Utis.h"
#import "NSString+AESCrypt.h"
#import "UITextField+Extensions.h"
#import "AppDelegate.h"
#import "MenuViewController.h"
#import "ChangePasswordViewController.h"
#import "PasscodeItem.h"

#import "AppData.h"
#import "PreferenceObject.h"
#import "WSB2CPasscodeVerfication.h"
#import "PasscodeViewController.h"
#import "MenuViewController.h"
#import "WSPreferences.h"
#import <AspireApiFramework/AspireApiFramework.h>


@interface UDASignInViewController () <DataLoadDelegate, UITextFieldDelegate>{
    
    BOOL isInputEmail, isInputPassword;
    WSB2CGetAccessToken* wsB2CGetAccessToek;
    WSB2CGetRequestToken* wsB2CGetRequestToken;
    WSB2CGetUserDetails* wsGetUser;
    WSPreferences *wsPreferences;
    WSB2CPasscodeVerfication *wsPasscodeVerification;
    AppDelegate* appdelegate;
}

@property (nonatomic, strong) WSSignIn *wsSignIn;

@end

@implementation UDASignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *tappedOutsideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tappedOutsideKeyboard];
    
    [self setUIStyte];
    [self trackingScreenByName:@"Sign in"];
    
    
    [_signInButton setTitle:NSLocalizedString(@"SIGN IN", nil) forState:UIControlStateNormal];
    [_signUpButton setTitle:NSLocalizedString(@"Sign up", nil) forState:UIControlStateNormal];
    [_forgotButton setTitle:NSLocalizedString(@"Forgot Password?", nil) forState:UIControlStateNormal];
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldChanged:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:self.passwordTextField];
    isInputEmail = self.emailTextField.text.length > 0;
    isInputPassword = self.passwordTextField.text.length > 0;
    [self setStatusSignInButton];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setTextViewsDefaultBottomBolder];
    });
    [self.navigationController setNavigationBarHidden:YES];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setTextViewsDefaultBottomBolder {
    [self.emailTextField setBottomBolderDefaultColor];
    [self.passwordTextField setBottomBolderDefaultColor];
}

-(void)resignFirstResponderForAllTextField{
    if(self.emailTextField.isFirstResponder){
        [self.emailTextField resignFirstResponder];
    }
    else if(self.passwordTextField.isFirstResponder){
        [self.passwordTextField resignFirstResponder];
    }
    
}

-(void)dismissKeyboard
{
    [self resignFirstResponderForAllTextField];
}

-(void)verifyData{
    NSString *errorAll = NSLocalizedString(@"All fields are required.", nil);
    NSMutableString *message = [[NSMutableString alloc] init];
    if (self.emailTextField.text.length == 0 && self.passwordTextField.text.length == 0) {
        [message appendString:@"* "];
        [message appendString:errorAll];
        [self.emailTextField setBottomBorderRedColor];
        [self.passwordTextField setBottomBorderRedColor];
        
    }else{
        if(self.emailTextField.text.length == 0){
            [message appendString:@"* "];
            [message appendString:[NSString stringWithFormat:@"%@ \n",errorAll]];
            [self.emailTextField setBottomBorderRedColor];
        }
        if (self.passwordTextField.text.length == 0){
            [message appendString:@"* "];
            [message appendString:[NSString stringWithFormat:@"%@ \n",errorAll]];
            [self.passwordTextField setBottomBorderRedColor];
        }
        if([self verifyValueForTextField:self.emailTextField].length > 0) {
            [message appendString:@"* "];
            [message appendString:[self verifyValueForTextField:self.emailTextField]];
            [message appendString:@"\n"];
        }
        if([self verifyValueForTextField:self.passwordTextField].length > 0) {
            [message appendString:@"* "];
            [message appendString:[self verifyValueForTextField:self.passwordTextField]];
            [message appendString:@"\n"];
        }
    }
    
    [self showError:message];
}

-(void)showError:(NSString *)message
{
    if(message.length > 0){
        
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"Please confirm that the value entered is correct:", nil);
        alert.msgAlert = message;
        alert.firstBtnTitle = NSLocalizedString(@"OK", nil);
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;
            alert.lblAlertMessage.textAlignment = NSTextAlignmentLeft;
            [alert.view layoutIfNeeded];
        });
        
        alert.blockFirstBtnAction = ^(void){
            [self makeBecomeFirstResponderForTextField];
        };
        
        [self showAlert:alert forNavigation:NO];
    }
    else{
        [self resignFirstResponderForAllTextField];
        [self setTextViewsDefaultBottomBolder];
        [self signInWithEmail:self.emailTextField.text withPassword:self.passwordTextField.text];
    }
}

-(void) makeBecomeFirstResponderForTextField
{
    [self resignFirstResponderForAllTextField];
    if(![self.emailTextField.text isValidEmail]){
        [self.emailTextField becomeFirstResponder];
    }
    else if(![self.passwordTextField.text isValidWeakPassword]){
        [self.passwordTextField becomeFirstResponder];
    }
}

-(NSString*)verifyValueForTextField:(UITextField *)textFied{
    
    NSString *errorMsg = @"";
    if(textFied == self.emailTextField && ![(textFied.isFirstResponder ? textFied.text :self.emailTextField.text) isValidEmail]){
        errorMsg = NSLocalizedString(@"Enter a valid email address.", nil);
        [textFied setBottomBorderRedColor];
    }
    else if(textFied == self.passwordTextField && ![(textFied.isFirstResponder ? textFied.text :self.passwordTextField.text) isValidWeakPassword]){
        //        errorMsg = @"Password field must allow 5 character password.";//NSLocalizedString(@"Password must be between 10 and 25 characters, including at least 1 upper case, 1 lower case, 1 special character and 1 number.", nil);
        errorMsg = NSLocalizedString(@"The password you entered is incorrect.", nil);
        [textFied setBottomBorderRedColor];
    }
    else{
        [textFied setBottomBolderDefaultColor];
    }
    
    return errorMsg;
}
- (void)WSBaseNetworkUnavailable{
    [self stopActivityIndicator];
}

- (void)signInWithEmail:(NSString*)email withPassword:(NSString*)password {
    [self startActivityIndicator];
    __weak typeof(self) _self = self;
    [ModelAspireApiManager loginWithUsername:email password:password completion:^(NSError *error) {
        
        if (!error) {
            if ([[User current] getPasscode].length != 0) {
                [self checkPassCode:[[User current] getPasscode]];
            } else {
                [self showAlertForRetryPasscode];
            }
        }else{
            [_self stopActivityIndicator];
            switch ([AspireApiError getErrorTypeAPISignInFromError:error]) {
                case SIGNIN_INVALID_USER_ERR:
                    [_self handleAPIErrorWithCode:NSLocalizedString(@"Please confirm that the value entered is correct.",nil )];
                    break;
                case UNKNOWN_ERR:
                    [_self handleAPIErrorWithCode:@""];
                    break;
                case NETWORK_UNAVAILABLE_ERR:
                    [_self showErrorNetwork];
                    break;
                default:
                    [_self handleAPIErrorWithCode:@""];
                    break;
            }
            
        }
    }];
    /*
     [self startActivityIndicator];
     NSMutableDictionary *userDict = [[NSMutableDictionary alloc] init];
     [userDict setValue:B2C_ConsumerKey forKey:@"ConsumerKey"];
     [userDict setValue:@"Login" forKey:@"Functionality"];
     [userDict setValue:email forKey:@"Email"];
     [userDict setValue:[password AES256EncryptWithKey:EncryptKey] forKey:[keyCurrentSecret AES256DecryptWithKey:EncryptKey]];
     [userDict setValue:B2C_DeviceId forKey:@"MemberDeviceID"];
     
     self.wsSignIn = [[WSSignIn alloc] init];
     self.wsSignIn.delegate = self;
     [self.wsSignIn signIn:userDict];
     */
    
}

- (void) setUIStyte {
    
    isInputEmail = isInputPassword = NO;
    self.emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    self.passwordTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    self.passwordTextField.secureTextEntry = YES;
    self.emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    self.emailTextField.delegate = self;
    self.passwordTextField.delegate = self;
    
    [self createAsteriskForTextField:self.emailTextField];
    [self createAsteriskForTextField:self.passwordTextField];
    
    self.emailTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.emailTextField.text];
    self.passwordTextField.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.passwordTextField.text];
    self.emailTextField.attributedPlaceholder = [UtilStyle
                                                 setPlaceHolderTextStyleForTextFieldWithMessage:NSLocalizedString(@"Email", nil)];
    self.passwordTextField.attributedPlaceholder = [UtilStyle
                                                    setPlaceHolderTextStyleForTextFieldWithMessage:NSLocalizedString(@"Password", nil)];
    
    
    [self setStatusSignInButton];
    
    [self.backgroundSignInView setBackgroundColor:[AppColor backgroundColor]];
    
    [self.signInButton setBackgroundColorForNormalStatus];
    [self.signInButton setBackgroundColorForTouchingStatus];
    [self.signInButton configureDecorationForButton];
    
    [self.forgotButton configureDecorationForButton];
    [self.signUpButton configureDecorationForButton];
}

- (void)setStatusSignInButton{
    self.signInButton.enabled = (isInputEmail && isInputPassword);
}

-(void) createAsteriskForTextField:(UITextField *)textField
{
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 8, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [textField setLeftView:leftView];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    
}
- (void)getPreference{
    wsPreferences = [[WSPreferences alloc] init];
    wsPreferences.delegate = self;
    [wsPreferences getPreference];
}

#pragma mark TEXT FIELD DELEGATE
-(void)textFieldChanged:(NSNotification*)notification {
    isInputPassword = self.passwordTextField.text.length > 0;
    [self setStatusSignInButton];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *inputString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (textField == self.passwordTextField) {
        if ([string isEqualToString:@" "]) {
            return NO;
        }
        isInputPassword = (inputString.length > 0);
        if (textField.text.length >= 25 && range.length == 0){
            return NO;
        }
    }else if (textField == self.emailTextField){
        if(textField.text.length == EMAIL_MAX_LENGTH && ![string isEqualToString:@""])
        {
            return NO;
        }
        inputString = [inputString removeRedudantWhiteSpaceInText];
        isInputEmail = inputString.length > 0;
    }
    [self setStatusSignInButton];
    
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.text = [textField.text removeRedudantWhiteSpaceInText];
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == self.emailTextField)
    {
        self.emailTextField.text = textField.text;
    }
    else if(textField == self.passwordTextField)
    {
        self.passwordTextField.text = textField.text;
    }
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if(textField == self.emailTextField){
        textField.text = self.emailTextField.text;
    }
    else if(textField == self.passwordTextField){
        textField.text = self.passwordTextField.text;
    }
}


#pragma mark - Delegate from API

-(void)loadDataDoneFrom:(WSBase*)ws
{
    if (ws.task == WS_AUTHENTICATION_LOGIN) {
        if (ws.data) {
            [self setUserDefaultInfoWithDict:ws.data[0]];
        }
        //Request token
        wsB2CGetRequestToken = [[WSB2CGetRequestToken alloc] init];
        wsB2CGetRequestToken.delegate = self;
        wsB2CGetRequestToken.task = WS_GET_REQUEST_TOKEN;
        [wsB2CGetRequestToken getRequestToken];
    }
    else if(ws.task == WS_GET_REQUEST_TOKEN){
        wsB2CGetAccessToek = [[WSB2CGetAccessToken alloc] init];
        wsB2CGetAccessToek.delegate = self;
        [wsB2CGetAccessToek requestAccessToken:encodeURLFromString(wsB2CGetRequestToken.requestToken) member:[[SessionData shareSessiondata] OnlineMemberID]];
    }
    else if(ws.task == WS_GET_ACCESS_TOKEN){
        //TODO: get user details
        [[SessionData shareSessiondata] setIsUseLocation: NO];
        [self getPreference];
    }
    else if (ws.task == WS_GET_MY_PREFERENCE){
        NSDictionary *profileDetailDict = [[SessionData shareSessiondata] getUserInfo];
        NSString *passcodeText = [profileDetailDict valueForKey:keyPasscode];
        if(passcodeText.length > 0)
        {
            wsPasscodeVerification = [[WSB2CPasscodeVerfication alloc] init];
            wsPasscodeVerification.delegate = self;
            NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
            [dataDict setValue:passcodeText forKey:keyPasscode];
            [wsPasscodeVerification verifyPasscode:dataDict];
        }
        else
        {
            [self stopActivityIndicator];
            [self showAlertForRetryPasscode];
        }
    }
    else if(ws.task == WS_B2C_VERIFY_PASSCODE)
    {
        PasscodeItem *passcodeItem = (PasscodeItem *)ws.data[0];
        if(passcodeItem.valid)
        {
            [self updateSuccessRedirect];
//            wsGetUser = [[WSB2CGetUserDetails alloc] init];
//            wsGetUser.delegate = self;
//            [wsGetUser getUserDetails];
        }
        else
        {
            [self stopActivityIndicator];
            [self showAlertForRetryPasscode];
        }
    }
    else if(ws.task == WS_B2C_GET_USER_DETAILS)
    {
        [self updateSuccessRedirect];
        [self stopActivityIndicator];
    }
}

-(void)showAlertForRetryPasscode
{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"ERROR!", nil);
    alert.msgAlert =  NSLocalizedString(@"We’re sorry, but your Program Passcode is not valid. Click OK to enter a new value.", nil);;;
    alert.firstBtnTitle = NSLocalizedString(@"OK", nil);
    alert.secondBtnTitle = NSLocalizedString(@"CANCEL", nil);
    
    __weak typeof(self) weakSelf = self;
    alert.blockFirstBtnAction = ^{
        weakSelf.emailTextField.text = @"";
        weakSelf.passwordTextField.text = @"";
        isInputEmail = NO;
        isInputPassword = NO;
        PasscodeViewController *passcodeViewController = [[PasscodeViewController alloc] init];
        [self.navigationController pushViewController:passcodeViewController animated:YES];
    };
    
    [self showAlert:alert forNavigation:NO];
}

-(void)checkPassCode:(NSString *)passcodeText {
    wsPasscodeVerification = [[WSB2CPasscodeVerfication alloc] init];
    wsPasscodeVerification.delegate = self;
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    [dataDict setValue:passcodeText forKey:keyPasscode];
    [wsPasscodeVerification verifyPasscode:dataDict];
}

- (void) updateSuccessRedirect{
    
    //    if ([[SessionData shareSessiondata] hasForgotAccount]) {
    //        ChangePasswordViewController *changePasswordVC = [[ChangePasswordViewController alloc] init];
    //        [self presentViewController:changePasswordVC animated:YES completion:nil];
    //    }else{
    appdelegate = appdelegate=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    [UIView transitionWithView:appdelegate.window
                      duration:0.5
                       options:UIViewAnimationOptionPreferredFramesPerSecond60
                    animations:^{
                        
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        MenuViewController *menuViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
                        UINavigationController *menuNavigationViewController = [[UINavigationController alloc] initWithRootViewController:menuViewController];
                        UIViewController *fontViewController = [[HomeViewController alloc] init];
                        
                        SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:menuNavigationViewController frontViewController:fontViewController];
                        
                        revealController.delegate = appdelegate;
                        revealController.rearViewRevealWidth = SCREEN_WIDTH;
                        revealController.rearViewRevealOverdraw = 0.0f;
                        revealController.rearViewRevealDisplacement = 0.0f;
                        appdelegate.viewController = revealController;
                        appdelegate.window.rootViewController = appdelegate.viewController;
                        [appdelegate.window makeKeyWindow];
                        
                        UIViewController *newFrontController = [[HomeViewController alloc] init];
                        UINavigationController *newNavigationViewController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                        [revealController pushFrontViewController:newNavigationViewController animated:YES];
                    }
                    completion:nil];
    //    }
    [self trackingEventByName:@"Sign in" withAction:ClickActionType withCategory:AuthenticationCategoryType];
    
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:keyProfile];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }else{
        [self showAPIErrorAlertWithMessage:NSLocalizedString(@"An error has occurred. Check your internet settings or try again.", nil) andTitle:NSLocalizedString(@"Cannot Get Data", nil)];
    }
}

-(void)loadDataFailFrom:(WSBase *)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message {
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }else{
        AlertViewController *alert = [[AlertViewController alloc] init];
        
        if ([message isEqualToString:@"ENR70-1"]) {
            alert.titleAlert = NSLocalizedString(@"ERROR!", nil);;
            [self.emailTextField setBottomBorderRedColor];
            [self.passwordTextField setBottomBorderRedColor];
            
            alert.msgAlert = NSLocalizedString(@"Please confirm that the value entered is correct.", nil);
            
        }else{
            alert.titleAlert = NSLocalizedString(@"ERROR!", nil);
            alert.msgAlert = NSLocalizedString(@"An error has occurred. Check your internet settings or try again.", nil);
        }
        alert.firstBtnTitle = NSLocalizedString(@"OK", nil);
        alert.blockFirstBtnAction = ^{
            [self makeBecomeFirstResponderForTextField];
        };
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;
        });
        
        [self showAlert:alert forNavigation:NO];
    }
}


- (void)setUserDefaultInfoWithDict:(UserRegistrationItem*)item{
    [[SessionData shareSessiondata] setOnlineMemberID:item.OnlineMemberID];
    [[SessionData shareSessiondata] setOnlineMemberDetailIDs:item.OnlineMemberDetailIDs];
}

#pragma mark - Action
- (IBAction)ForgetPasswordAction:(id)sender {
    self.emailTextField.text = @"";
    self.passwordTextField.text = @"";
    isInputEmail = isInputPassword = NO;
    [self.view endEditing:YES];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UDAForgotPasswordViewController *forgotPasswordViewController = [storyboard instantiateViewControllerWithIdentifier:@"UDAForgotPasswordViewController"];
    [self.navigationController pushViewController:forgotPasswordViewController animated:YES];
    
}

- (IBAction)SignInAction:(id)sender {
    [self verifyData];
}

- (IBAction)SignUpAction:(id)sender {
    self.emailTextField.text = @"";
    self.passwordTextField.text = @"";
    isInputEmail = isInputPassword = NO;
    [self.emailTextField endEditing:YES];
    [self.passwordTextField endEditing:YES];
    [self navigationToCreateProfileViewController];
}

-(void)navigationToCreateProfileViewController {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CreateProfileViewController *createProfileViewController = [storyboard instantiateViewControllerWithIdentifier:@"CreateProfileViewController"];
    
    if ([AppData isCreatedProfile])  {
        SWRevealViewController *revealViewController = self.revealViewController;
        
        [revealViewController pushFrontViewController:createProfileViewController animated:YES];
    } else
        [self.navigationController pushViewController:createProfileViewController animated:YES];
}


- (void) handleAPIErrorWithCode:(NSString*)errorCode{
    [self.emailTextField setBottomBorderRedColor];
    [self.passwordTextField setBottomBorderRedColor];
    if (![errorCode isEqualToString:@""]) {
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"ERROR!", nil);
        alert.msgAlert = errorCode;
        alert.firstBtnTitle = NSLocalizedString(@"OK", nil);
        alert.blockFirstBtnAction = ^{
            [self makeBecomeFirstResponderForTextField];
        };
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;
        });
        
        [self showAlert:alert forNavigation:NO];
        
        
        [self.emailTextField setBottomBorderRedColor];
        [self.passwordTextField setBottomBorderRedColor];
    }else{
        [self showApiErrorWithMessage:@""];
    }
    
}

@end
