//
//  SearchViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/10/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "SearchViewController.h"
#import "UIView+Extension.h"
#import "UIButton+Extension.h"
#import "Constant.h"
#import "Common.h"

@interface SearchViewController ()<UITextFieldDelegate>
{
    CGFloat backupBottomConstraint;
    
    BOOL isCheckOffer;
}
@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UITapGestureRecognizer *tappedOutsideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tappedOutsideKeyboard.cancelsTouchesInView = YES;
    [self.view addGestureRecognizer:tappedOutsideKeyboard];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWill:) name:UIKeyboardWillHideNotification object:nil];
    backupBottomConstraint = self.searchBtnBottomConstraint.constant;
    [self trackingScreenByName:@"Search"];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)initView
{
    [self backNavigationItem];
    [self.offerCheckBox setImage:[UIImage imageNamed:@"checkbox_uncheck"] forState:UIControlStateNormal];
    [self.offerCheckBox setImage:[UIImage imageNamed:@"checkbox_check"] forState:UIControlStateSelected];
    [self.offerCheckBox addTarget:self action:@selector(enableSubmitButton:) forControlEvents:UIControlEventTouchUpInside];
    self.offerCheckBox.selected = isCheckOffer;
    [self.offerCheckBox setBackgroundColor:[UIColor clearColor]];
    
    if(self.currentKeyWord.length == 0)
    {
        self.offerCheckBox.enabled = NO;
    }
    
    [self.bookCheckBox setImage:[UIImage imageNamed:@"checkbox_uncheck"] forState:UIControlStateNormal];
    [self.bookCheckBox setImage:[UIImage imageNamed:@"checkbox_check"] forState:UIControlStateSelected];
    [self.bookCheckBox addTarget:self action:@selector(enableSubmitButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.bookCheckBox setBackgroundColor:[UIColor clearColor]];
    self.bookCheckBox.enabled = NO;
    
    [self.bookText setText:NSLocalizedString(@"Can Book Online", nil)];
    [self.offerText setText:NSLocalizedString(@"With Offers", nil)];
    self.offerText.font = [UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize titleTextSize]];
    self.offerText.textColor = [AppColor textColor];
    
    UITapGestureRecognizer *offerTextGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enableSubmitButton:)];
    [self.offerText setUserInteractionEnabled:YES];
    [self.offerText addGestureRecognizer:offerTextGesture];
    
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"SEARCH", nil)];
    
    [self.searchButton setBackgroundColorForNormalStatus];
    [self.searchButton setBackgroundColorForTouchingStatus];
    //[self.searchButton setBackgroundColorForDisableStatus];
    
    if(self.currentKeyWord.length == 0)
    {
        self.searchButton.enabled = NO;
    }
    
    self.searchButton.titleLabel.font = [UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize titleTextSize]];
    self.searchButton.titleLabel.textColor = [UIColor whiteColor];
    [self.searchButton setTitle:NSLocalizedString(@"SEARCH", nil) forState:UIControlStateNormal];
    
    UIImageView *searchIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"search_icon"]];
    searchIcon.frame = CGRectMake(0.0f, 0.0f, 30.0f, 30.0f);
    self.searchText.leftView = searchIcon;
    self.searchText.leftViewMode = UITextFieldViewModeAlways;
    self.searchText.delegate = self;
    self.searchText.textColor = [AppColor textColor];
    self.searchText.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Search", nil) attributes:@{NSForegroundColorAttributeName:[AppColor placeholderTextColor]}];
    
    UIFont *font = [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]];
    self.searchText.font = font;
    
    if(self.currentKeyWord.length == 0)
    {
        NSString *placeHolderMsg = NSLocalizedString(@"Search", nil);
        NSMutableAttributedString *placeHolderAttribute = [[NSMutableAttributedString alloc] initWithString:placeHolderMsg];
        [placeHolderAttribute addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, placeHolderMsg.length)];
        [placeHolderAttribute addAttribute:NSForegroundColorAttributeName value:[AppColor placeholderTextColor] range:NSMakeRange(0, placeHolderMsg.length)];
        self.searchText.attributedPlaceholder = placeHolderAttribute;
    }
    else{
        self.searchText.text = self.currentKeyWord;
    }
    [self.searchButtonView setBackgroundColor:[AppColor backgroundColor]];
    [self.searchTextView setbackgroundColorForLineMenu];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.stackViewTopConstraint.constant = (IPAD) ? 8.0f : 20.0f;
    });
}

- (void) checkOffer:(BOOL)ischeck {
    isCheckOffer = ischeck;
}

-(void)handleKeyboardWillShow:(NSNotification *)paramNotification
{
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         self.searchBtnBottomConstraint.constant = keyboardRect.size.height + MARGIN_KEYBOARD;
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

-(void)handleKeyboardWill:(NSNotification *)paramNotification
{
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         self.searchBtnBottomConstraint.constant =  backupBottomConstraint;
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
    
}


-(void)dismissKeyboard
{
    if([self.searchText becomeFirstResponder])
    {
        [self.searchText resignFirstResponder];
    }
}

-(void) enableSubmitButton:(id)sender
{
    [self dismissKeyboard];
    if([self.searchText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0)
    {
        [self.offerCheckBox setHighlighted:NO];
        self.offerCheckBox.selected = !(self.offerCheckBox.selected);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)searchTappedAction:(id)sender {
    [self searchAction];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return [self searchAction];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL isPressedBackspaceAfterSingleSpaceSymbol = range.length==1 && string.length==0;
    NSString* textNeedCheck = textField.text;
    if(isPressedBackspaceAfterSingleSpaceSymbol)
        textNeedCheck = [[textNeedCheck substringToIndex:range.location] stringByAppendingString:[textNeedCheck substringWithRange:NSMakeRange(range.location+range.length, textNeedCheck.length - range.location - range.length)]];
    else
        textNeedCheck = [textNeedCheck stringByAppendingString:string];
    if([textNeedCheck stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length && ![self checkIsAllIsWhiteSpaceForText:textNeedCheck])
    {
        self.searchButton.enabled = YES;
        self.bookCheckBox.enabled = YES;
        self.offerCheckBox.enabled = YES;
    }
    else{
        if([textNeedCheck stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0){
            self.searchButton.enabled = NO;
            self.bookCheckBox.enabled = NO;
            self.bookCheckBox.selected = NO;
            self.offerCheckBox.enabled = NO;
            self.offerCheckBox.selected = NO;
        }
        else{
            self.searchButton.enabled = YES;
            self.bookCheckBox.enabled = YES;
            self.offerCheckBox.enabled = YES;
        }
    }
    return YES;
}

-(BOOL)searchAction
{
    if([self.searchText.text length] > 0 || self.offerCheckBox.selected)
    {
        if(self.delegate)
        {
            [self.searchText resignFirstResponder];
            [self.delegate getDataBaseOnSearchText:[self strimStringFrom:self.searchText.text withPatterns:@[@"^ *"]] withOffer:self.offerCheckBox.selected withBookOnline:self.bookCheckBox.selected];
        }
        return YES;
    }
    else{
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"ALERT!", nil);;
        alert.msgAlert = NSLocalizedString(@"Enter a search term or select a check box.", nil);
        alert.firstBtnTitle = NSLocalizedString(@"OK", nil);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
        });
        
        [self showAlert:alert forNavigation:YES];
        return NO;
    }
}

- (BOOL) checkIsAllIsWhiteSpaceForText:(NSString*) text {
    BOOL isYES = YES;
    NSArray* arr = [text componentsSeparatedByString:@""];
    for (NSString* t in arr) {
        if([t stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0) {
            isYES = NO;
            break;
        }
    }
    return isYES;
}

- (NSString*) strimStringFrom:(NSString*) from withPatterns:(NSArray*) pattern {
    NSString* temp = from;
    for (NSString* p in pattern) {
        temp = [temp stringByReplacingOccurrencesOfString:p withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0,temp.length)];
    }
    return temp;
}

@end
