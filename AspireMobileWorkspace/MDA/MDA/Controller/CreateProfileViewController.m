//
//  SignInViewController.m
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/13/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "CreateProfileViewController.h"
#import "MenuViewController.h"
#import "SWRevealViewController.h"
#import "UDASignInViewController.h"
#import "AppDelegate.h"
#import "WSCreateUser.h"
#import "WSB2CGetAccessToken.h"
#import "WSB2CGetUserDetails.h"
#import "WSB2CGetRequestToken.h"
#import "WSPreferences.h"
#import "NSString+Utis.h"
#import "UITextField+Extensions.h"
#import "UIButton+Extension.h"
#import "UIView+Extension.h"
#import "UILabel+Extension.h"
#import "HomeViewController.h"
#import "Constant.h"
#import "MyProfileViewController.h"
#import "AppData.h"
#import "AppDelegate.h"
#import "UserObject.h"
#import "UserRegistrationItem.h"
#import "LocationComponent.h"
#import "PreferenceObject.h"
#import "WSB2CPasscodeVerfication.h"
#import "PasscodeItem.h"
#import "UtilStyle.h"
#import "NSString+AESCrypt.h"
#import <AspireApiFramework/AspireApiFramework.h>


#define TEXT_FIELD_FONT_SIZE 18

@interface CreateProfileViewController ()<DataLoadDelegate, UITextFieldDelegate, DropDownViewDelegate, UIGestureRecognizerDelegate>
{
    WSB2CGetAccessToken* wsB2CGetAccessToek;
    WSB2CGetRequestToken* wsB2CGetRequestToken;
    WSB2CGetUserDetails* wsGetUser;
    WSPreferences *wsPreferences;
    WSB2CPasscodeVerfication *wsPasscodeVerification;
    AppDelegate* appdelegate;
    UITapGestureRecognizer *tappedOutsideKeyboards;
}

@property (nonatomic, strong) WSCreateUser *wsCreateUser;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnSC;

@end

@implementation CreateProfileViewController
#pragma mark - LIFECYCLE
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateStatusLocation:)
                                                 name:LOCATION_COMPONENT_ISAUTHORIZED_NOTIFICATION
                                               object:nil];
    
    UITapGestureRecognizer *tab = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkBox:)];
    self.commitmentLabel.userInteractionEnabled = YES;
    [self.commitmentLabel addGestureRecognizer:tab];
    
    [self setUIStyte];
    if (_isUpdateProfile == NO || !_isUpdateProfile) {
        [self trackingScreenByName:@"Sign up"];
    }else{
        [self trackingScreenByName:@"My profile"];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    tappedOutsideKeyboards = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tappedOutsideKeyboards.delegate = self;
    [self.navigationController.view addGestureRecognizer:tappedOutsideKeyboards];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [self setTextViewsDefaultBottomBolder];
    [self.view layoutIfNeeded];
    
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController.view removeGestureRecognizer:tappedOutsideKeyboards];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SETUP UI
- (void) setUIStyte {
    
    self.phoneNumberText.keyboardType = UIKeyboardTypePhonePad;
    self.phoneNumberText.tag = PHONE_NUMBER_TEXTFIELD_TAG;
    self.emailText.keyboardType = UIKeyboardTypeEmailAddress;
    self.firstNameText.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    self.lastNameText.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    
    self.emailText.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailText.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    self.salutationDropDown.leadingTitle = 8.0f;
    self.salutationDropDown.title = NSLocalizedString(@"Please select a salutation.", nil);
    self.salutationDropDown.listItems = @[NSLocalizedString(@"Dr", nil),
                                          NSLocalizedString(@"Miss", nil),
                                          NSLocalizedString(@"Mr", nil),
                                          NSLocalizedString(@"Ms", nil)];
    self.salutationDropDown.titleColor = [AppColor placeholderTextColor];
    self.salutationDropDown.delegate = self;
    
    self.firstNameText.delegate = self;
    self.lastNameText.delegate = self;
    self.emailText.delegate = self;
    self.phoneNumberText.delegate = self;
    self.passcodeText.delegate = self;
    self.passwordText.delegate = self;
    self.confirmPasswordText.delegate = self;
    
    [self createAsteriskForTextField:self.firstNameText];
    [self createAsteriskForTextField:self.lastNameText];
    [self createAsteriskForTextField:self.emailText];
    [self createAsteriskForTextField:self.phoneNumberText];
    [self createAsteriskForTextField:self.passcodeText];
    [self createAsteriskForTextField:self.passwordText];
    [self createAsteriskForTextField:self.confirmPasswordText];
    
    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
    if (profileDictionary.count > 2) {
        
        NSString *salutation = [profileDictionary objectForKey:keySalutation];
        if (salutation.length > 1) {
            self.salutationDropDown.titleColor = [AppColor textColor];
            self.salutationDropDown.title = salutation;
        }
        self.firstNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:[profileDictionary objectForKey:keyFirstName]];
        self.lastNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:[profileDictionary objectForKey:keyLastName]];
        self.emailText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:[profileDictionary objectForKey:keyEmail]];
        self.phoneNumberText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:[profileDictionary objectForKey:keyMobileNumber]];
        
        isUseLocation = [[SessionData shareSessiondata] isUseLocation];
        [self setImageLocation:isUseLocation];
        
        currentFirstName = [profileDictionary objectForKey:keyFirstName];
        currentEmail = [profileDictionary objectForKey:keyEmail] ;
        NSString *tempPhone = [profileDictionary objectForKey:keyMobileNumber];
        currentPhone = ([tempPhone containsString:@"+"])?tempPhone:[NSString stringWithFormat:@"+%@",tempPhone];
        currentLastName = [profileDictionary objectForKey:keyLastName];
        
        self.firstNameText.text = [currentFirstName createMaskForText:NO];
        self.emailText.text = [currentEmail createMaskForText:YES];
        self.lastNameText.text = [currentLastName createMaskForText:NO];
        self.phoneNumberText.text = [currentPhone createMaskForText:NO];
        self.emailText.enabled = NO;
        
    }else{
        
        currentFirstName = @"";
        currentEmail = @"";
        currentPhone = @"";
        currentLastName = @"";
        
        self.firstNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.firstNameText.text];
        self.lastNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.lastNameText.text];
        self.emailText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.emailText.text];
        self.phoneNumberText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.phoneNumberText.text];
        self.passcodeText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.passcodeText.text];
        self.passwordText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.passwordText.text];
        self.confirmPasswordText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:self.confirmPasswordText.text];
        
        self.firstNameText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.firstNameText.placeholder];
        self.lastNameText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.lastNameText.placeholder];
        self.emailText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.emailText.placeholder];
        self.phoneNumberText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.phoneNumberText.placeholder];
        self.passcodeText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.passcodeText.placeholder];
        self.passwordText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.passwordText.placeholder];
        self.confirmPasswordText.attributedPlaceholder = [UtilStyle setPlaceHolderTextStyleForTextFieldWithMessage:self.confirmPasswordText.placeholder];
    }
    
    [self.myView setBackgroundColorForView];
    [self.scrollView setBackgroundColorForView];
    [self.signInButton setBackgroundColorForNormalStatus];
    [self.signInButton setBackgroundColorForTouchingStatus];
    [self.signInButton configureDecorationForButton];
    
    //[self backgroundColorForDisableStatusButton];
    [self.submitButton setBackgroundColorForNormalStatus];
    self.submitButton.enabled = NO;
    [self.submitButton setBackgroundColorForTouchingStatus];
    [self.submitButton configureDecorationForButton];
    
    [self.cancelButton setBackgroundColorForNormalStatus];
    [self.cancelButton setBackgroundColorForTouchingStatus];
    [self.cancelButton configureDecorationForButton];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.checkBoxImageTopConstraint.constant = (IPAD) ? 12.0f : 2.0f;
        _btnSC.constant = (IPAD)?120:80;
    });
}
- (void)backgroundColorForDisableStatusButton
{
    UIGraphicsBeginImageContext(CGSizeMake(1.0f, 1.0f));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [AppColor disableButtonColor].CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 1));
    UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.submitButton setBackgroundImage:colorImage forState:UIControlStateNormal];
}

- (void)initView{
    
    // Text Style For Title
    self.titleLable.attributedText = [UtilStyle setTextStyleForTitleViewControllerWithMessage:[NSLocalizedString(@"Sign up", nil) uppercaseString]];
    
    // Text Style For Greet Message
    NSString *message = NSLocalizedString(@"I would like to stay informed and receive the monthly concierge newsletter at the email address provided.", nil);
    self.commitmentLabel.attributedText = [UtilStyle setLargeSizeStyleForLabelWithMessage:message];
    //[self setCheckBoxState:isCheck];
    
    _firstNameText.placeholder = NSLocalizedString(@"First Name", nil);
    _lastNameText.placeholder = NSLocalizedString(@"Last Name", nil);
    _emailText.placeholder = NSLocalizedString(@"Email", nil);
    _phoneNumberText.placeholder = NSLocalizedString(@"Mobile Phone", nil);
    _passcodeText.placeholder = NSLocalizedString(@"Program Passcode", nil);
    _passwordText.placeholder = NSLocalizedString(@"Password", nil);
    _confirmPasswordText.placeholder = NSLocalizedString(@"Retype Password", nil);
    _useLocationLabel.text = NSLocalizedString(@"Use My Location", nil);
    _onLabel.text = [NSLocalizedString(@"ON", nil) uppercaseString];
    _offLabel.text = [NSLocalizedString(@"OFF", nil) uppercaseString];
    _termLabel.text = NSLocalizedString(@"I acknowledge that I have read and agree to the Terms of Use and Privacy Policy", nil);
    
    [_submitButton setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
    [_cancelButton setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
}

#pragma mark - SETUP DATA
- (void)setUserObjectWithDict:(NSDictionary*)dict{
    [[SessionData shareSessiondata] setUserObjectWithDict:dict];
}

- (void)setUserDefaultInfoWithDict:(UserRegistrationItem*)item{
    [[SessionData shareSessiondata] setOnlineMemberID:item.OnlineMemberID];
    [[SessionData shareSessiondata] setOnlineMemberDetailIDs:item.OnlineMemberDetailIDs];
}

- (void) getUserInfo{
    if ([User isValid]) {
        UserObject* user = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
        NSString *salutation = user.salutation;
        self.salutationDropDown.titleColor = (salutation.length > 1)?[AppColor textColor]:[AppColor placeholderTextColor];
        self.salutationDropDown.title = (salutation.length > 1)?salutation:@"Please select a salutation.";
        currentFirstName = user.firstName;
        currentEmail = user.email ;
        NSString *tempPhone = user.mobileNumber;
        currentPhone = (tempPhone.length < 15) && ![tempPhone containsString:@"+"] ? [NSString stringWithFormat:@"+%@",tempPhone] : tempPhone;
        currentLastName = user.lastName;
        self.firstNameText.text = [currentFirstName createMaskForText:NO];
        self.emailText.text = [currentEmail createMaskForText:YES];
        self.lastNameText.text = [currentLastName createMaskForText:NO];
        self.phoneNumberText.text = [currentPhone createMaskForText:NO];
        
        //        isUseLocation = false;
        if ([User current].locations.count > 0)
            isUseLocation = user.isUseLocation;
        [self setImageLocation:isUseLocation];
    }
    /*
    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
    if (profileDictionary.count > 2) {
        
        NSString *salutation = [profileDictionary objectForKey:keySalutation];
        self.salutationDropDown.titleColor = (salutation.length > 1)?[AppColor textColor]:[AppColor placeholderTextColor];
        self.salutationDropDown.title = (salutation.length > 1)?salutation:NSLocalizedString(@"Please select a salutation.", nil);
        currentFirstName = [profileDictionary objectForKey:keyFirstName];
        currentEmail = [profileDictionary objectForKey:keyEmail] ;
        NSString *tempPhone = [profileDictionary objectForKey:keyMobileNumber];
        currentPhone = ([tempPhone containsString:@"+"])?tempPhone:[NSString stringWithFormat:@"+%@",tempPhone];
        currentLastName = [profileDictionary objectForKey:keyLastName];
    
        self.firstNameText.text = [currentFirstName createMaskForText:NO];
        self.emailText.text = [currentEmail createMaskForText:YES];
        self.lastNameText.text = [currentLastName createMaskForText:NO];
        self.phoneNumberText.text = [currentPhone createMaskForText:NO];
        isUseLocation = [[SessionData shareSessiondata] isUseLocation];
        [self setImageLocation:isUseLocation];
    }
     */
}
#pragma mark - ACTIONS
- (void) setCheckBoxState:(BOOL)check
{
    if (!check) {
        check = NO;
    }
    (check == YES) ? [self.checkBoxButton setImage:[UIImage imageNamed:@"checkbox_check"] forState:UIControlStateNormal] : [self.checkBoxButton setImage:[UIImage imageNamed:@"checkbox_uncheck"] forState:UIControlStateNormal];
}
- (IBAction)checkBox:(id)sender {
    isCheck = !isCheck;
    [self setCheckBoxState:isCheck];
}

- (IBAction)actionSwitchLocation:(id)sender {
    
    isUseLocation = !isUseLocation;
    [self setImageLocation:isUseLocation];
    if (![self isKindOfClass:[MyProfileViewController class]]) {
        [[SessionData shareSessiondata] setIsUseLocation:isUseLocation];
        if (isUseLocation) {
            [LocationComponent startRequestLocation];
        }
    }
    else{
        [[SessionData shareSessiondata] setIsUseLocation:isUseLocation];
        if (isUseLocation) {
            [LocationComponent startRequestLocation];
        }
    }
   [self changeUseLocation];
}

- (void)setImageLocation:(BOOL)isLocation {
    self.locationStatusImage.image = [UIImage imageNamed:(isLocation) ? @"location_on" : @"location_off"];
}

-(void)updateStatusLocation:(NSNotification *)notification {
    
    NSDictionary* dictNoti = notification.userInfo;
    BOOL isLocation = [dictNoti boolForKey:LOCATION_COMPONENT_ISAUTHORIZED_NOTIFICATION];
    if (!isLocation) {
        if ([dictNoti intForKey:@"ERROR_CODE"] == 2) {
//            isUseLocation = NO;
//            [self setImageLocation:isUseLocation];
//            [[SessionData shareSessiondata] setIsUseLocation:isUseLocation];
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
                [self setRequestLocation:YES];
            });
        }else if ([dictNoti intForKey:@"ERROR_CODE"] == 1) {
            if([[NSUserDefaults standardUserDefaults] boolForKey:@"IsRequestLocation"]) {
                [self setRequestLocation:NO];
            }else{
                [self showErrorLocationService];
            }
        }
    }
    [self changeUseLocation];
}

- (void)setRequestLocation:(BOOL)isRequest{
    [[NSUserDefaults standardUserDefaults] setBool:isRequest forKey:@"IsRequestLocation"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (void)updateProfileButtonStatus{

}
-(void) enableSignInButton:(UIButton *)button
{
    [button setHighlighted:NO];
    self.checkBoxButton.selected = !(button.selected);
    [button setBackgroundColor:[UIColor clearColor]];
    [self.submitButton setEnabled:button.selected];
}

- (IBAction)CheckboxAction:(id)sender {
    if (isCheck) {
        self.checkBoxImage.image = [UIImage imageNamed:@"checkbox_uncheck"];
        isCheck = NO;
    }else{
        self.checkBoxImage.image = [UIImage imageNamed:@"checkbox_check"];
        isCheck = YES;
    }
    [self enableSubmitButton:isCheck];
}

- (void)enableSubmitButton:(BOOL)isEnable {
    self.submitButton.enabled = isEnable;
}

- (IBAction)SubmitAction:(id)sender {
    [self dismissKeyboard];
    [self verifyAccountData:NO];
}

- (IBAction)CancelAction:(id)sender {
    if ([AppData isCreatedProfile]) {
    SWRevealViewController *revealViewController = self.revealViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UDASignInViewController *signInViewController = [storyboard instantiateViewControllerWithIdentifier:@"UDASignInViewController"];
    [revealViewController pushFrontViewController:signInViewController animated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - ALERT
- (void)showErrorLocationService {
    
    //    isUseLocation = NO;
    //    [self setImageLocation:isUseLocation];
    //    [[SessionData shareSessiondata] setIsUseLocation:isUseLocation];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Location Service Disabled", nil) message:NSLocalizedString(@"To enable, please go to Settings and turn on Location Service for this app.", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction*  action) {
        
    }];
    
    UIAlertAction *settingAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"SETTINGS", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction  *action) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }];
    
    [alertController addAction:okAction];
    [alertController addAction:settingAction];
    alertController.preferredAction = settingAction;
    
    [self presentViewController:alertController animated: YES completion: nil];
}
-(void)showAlertForRetryPasscode
{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert = NSLocalizedString(@"ERROR!", nil);
    alert.msgAlert = NSLocalizedString(@"This is not a valid Program Passcode. Click OK to try again.", nil);;
    alert.firstBtnTitle = NSLocalizedString(@"OK", nil);
    
    /*
    dispatch_async(dispatch_get_main_queue(), ^{
        alert.seconBtn.hidden = YES;
        alert.midView.hidden = YES;
    });
    */
    
    alert.secondBtnTitle = NSLocalizedString(@"CANCEL", nil);
    
    __weak typeof(self) weakSelf = self;
    alert.blockFirstBtnAction = ^{
        [weakSelf.passcodeText setBottomBorderRedColor];
        weakSelf.passcodeText.text = @"";
        [weakSelf.passcodeText becomeFirstResponder];
    };
    
    alert.blockSecondBtnAction = ^{
        [weakSelf.navigationController popViewControllerAnimated:NO];
    };
    [self showAlert:alert forNavigation:NO];
}

#pragma mark API PROCESS
- (void)updateProfileWithAPI{
    [self startActivityIndicator];
    NSMutableDictionary *dictDai = [[NSMutableDictionary alloc] init];
    [dictDai setValue:(self.salutationDropDown.title.length > 1) ? [self.salutationDropDown.title converSalutationForAPI] : @" " forKey:@"Salutation"];
    [dictDai setValue:currentFirstName forKey:@"FirstName"];
    [dictDai setValue:currentLastName forKey:@"LastName"];
    [dictDai setValue:currentPhone forKey:@"MobileNumber"];
    [dictDai setValue:currentEmail forKey:@"Email"];
    [dictDai setValue:currentPassword forKey:@"Password"];
    [dictDai setValue:[[SessionData shareSessiondata] passcode] forKey:APP_USER_PREFERENCE_Passcode];
    [dictDai setValue:isUseLocation? @"Yes": @"No" forKey:APP_USER_PREFERENCE_Location];
    [dictDai setValue:@"USA" forKey:@"Country"];
    [dictDai setValue:APP_NAME forKey:@"referenceName"];
    __weak typeof(self) _self = self;
    [dictDai setValue:@"na" forKey:@"Dining Preference"];
    [dictDai setValue:@"na" forKey:@"Hotel Preference"];
    [dictDai setValue:@"na" forKey:@"Car Type Preference"];
    [dictDai setValue:@"Unknown" forKey:@"City"];
    [dictDai setValue:@"Unknown" forKey:APP_USER_PREFERENCE_Selected_City];
    [ModelAspireApiManager createProfileWithUserInfo:dictDai completion:^(User *user, NSError *error) {
        [_self stopActivityIndicator];
        if (user) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                [_self trackingEventByName:@"Sign up" withAction:ClickActionType withCategory:AuthenticationCategoryType];
                [AppData setCreatedProfileSuccessful];
                
                AlertViewController *alert = [[AlertViewController alloc] init];
                alert.titleAlert = nil;
                alert.msgAlert = NSLocalizedString(@"Profile created successfully.", nil);
                alert.firstBtnTitle = NSLocalizedString(@"OK", nil);
                dispatch_async(dispatch_get_main_queue(), ^{
                    alert.seconBtn.hidden = YES;
                    alert.midView.alpha = 0.0f;;
                    alert.lblAlertMessage.textAlignment = NSTextAlignmentCenter;
                    [alert.view layoutIfNeeded];
                });
                
                alert.blockFirstBtnAction = ^(void){
                    [_self updateSuccessRedirect];
                };
                
                [_self showAlert:alert forNavigation:NO];
            });
        }else{
            dispatch_sync(dispatch_get_main_queue(), ^{
                NSString *mess;
                switch ([AspireApiError getErrorTypeAPISignupFromError:error]) {
                    case ACCESSTOKEN_INVALID_ERR:
                        [_self showApiErrorWithMessage:@""];
                        break;
                    case SIGNUP_EXIST_ERR:
                        mess =@"";
                        break;
                    case UNKNOWN_ERR:
                        [_self showApiErrorWithMessage:@""];
                        break;
                    default:
                        [_self showApiErrorWithMessage:@""];
                        break;
                }
                if (mess != nil) {
                    AlertViewController *alert = [[AlertViewController alloc] init];
                    alert.titleAlert = NSLocalizedString(@"ERROR!", nil);;
                    alert.msgAlert = NSLocalizedString(@"An account with this email address already exists. Click Cancel to Sign In.", nil);;
                    alert.firstBtnTitle = NSLocalizedString(@"OK", nil);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        alert.seconBtn.hidden = YES;
                        alert.midView.alpha = 0.0f;;
                    });
                    
                    [self showAlert:alert forNavigation:NO];
                }
            });
        }
    }];
    /*
    [self startActivityIndicator];
    NSMutableDictionary *userDict = [[NSMutableDictionary alloc] init];
    [userDict setValue:B2C_ConsumerKey forKey:@"ConsumerKey"];
    [userDict setValue:_isUpdateProfile ? @"Registration" : @"Registration" forKey:@"Functionality"];
    [userDict setValue:(self.salutationDropDown.title.length > 1) ? self.salutationDropDown.title : @" " forKey:@"Salutation"];
    [userDict setValue:currentFirstName forKey:@"FirstName"];
    [userDict setValue:currentLastName forKey:@"LastName"];
    NSString *currentPhoneTemp = [currentPhone stringByReplacingOccurrencesOfString:@"+" withString:@""];
    [userDict setValue:currentPhoneTemp forKey:@"MobileNumber"];
    [userDict setValue:currentEmail forKey:@"Email"];
    [userDict setValue:[currentPassword AES256EncryptWithKey:EncryptKey] forKey:[keyCurrentSecret AES256DecryptWithKey:EncryptKey]];
    [userDict setValue:B2C_DeviceId forKey:@"MemberDeviceID"];
    [[SessionData shareSessiondata] setIsUseLocation:isUseLocation];
    self.wsCreateUser = [[WSCreateUser alloc] init];
    self.wsCreateUser.delegate = self;
    if (!_isUpdateProfile) {
        [self.wsCreateUser createUser:userDict];
    }else{
        [self.wsCreateUser updateUser:userDict];
    }*/
    
}
- (void)addPreferences{
    
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    NSMutableArray *arraySubDictValues = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *hotelDict = [[NSMutableDictionary alloc] init];
    [hotelDict setValue:@"Hotel" forKey:@"Type"];
    
    PreferenceObject *hotelPref = [self getPreferenceByType:HotelPreferenceType];
    NSString *value = ([[SessionData shareSessiondata] isUseLocation])?@"YES":@"NO";
    
    NSString *userName = [[NSString alloc] initWithFormat:@"%@ %@", currentFirstName, currentLastName];
    
    if ([SessionData shareSessiondata].arrayPreferences.count > 0 && hotelPref.preferenceID.length > 0){
        [hotelDict setValue:hotelPref.preferenceID forKey:@"MyPreferencesId"];
        [hotelDict setValue:hotelPref.value forKey:@"Preferredstarrating"];
        [hotelDict setValue:value forKey:@"SmokingPreference"];
        [hotelDict setValue:APP_NAME forKey:@"BedPreference"];
        [arraySubDictValues addObject:hotelDict];
        [dataDict setValue:arraySubDictValues forKey:@"PreferenceDetails"];
        
        wsPreferences = [[WSPreferences alloc] init];
        wsPreferences.delegate = self;
        [wsPreferences updatePreference:dataDict];
        
    }else{
        [hotelDict setValue:@"" forKey:@"Preferredstarrating"];
        [hotelDict setValue:value forKey:@"SmokingPreference"];
        [hotelDict setValue:self.passcodeText.text forKey:@"RoomPreference"];
        [hotelDict setValue:APP_NAME forKey:@"BedPreference"];
        
        [arraySubDictValues addObject:hotelDict];
        
        //Begin: support for android.
        NSMutableDictionary *diningDict = [[NSMutableDictionary alloc] init];
        [diningDict setValue:@"Dining" forKey:@"Type"];
        [diningDict setValue:@"" forKey:@"CuisinePreferences"];
        [arraySubDictValues addObject:diningDict];
        
        NSMutableDictionary *transportationDict = [[NSMutableDictionary alloc] init];
        [transportationDict setValue:@"Car Rental" forKey:@"Type"];
        [transportationDict setValue:@"" forKey:@"PreferredRentalVehicle"];
        [arraySubDictValues addObject:transportationDict];
        //End: support for android.
        
        [dataDict setValue:arraySubDictValues forKey:@"PreferenceDetails"];
        wsPreferences = [[WSPreferences alloc] init];
        wsPreferences.delegate = self;
        [wsPreferences addPreference:dataDict];
    }
}
-(PreferenceObject*)getPreferenceByType:(PreferenceType)type{
    if ([SessionData shareSessiondata].arrayPreferences.count > 0) {
        for (PreferenceObject *preference in [SessionData shareSessiondata].arrayPreferences) {
            if (preference.type == type) {
                return preference;
            }
        }
    }
    return nil;
}

- (void)getPreference{
    
    wsPreferences = [[WSPreferences alloc] init];
    wsPreferences.delegate = self;
    [wsPreferences getPreference];
}

-(void)loadDataDoneFrom:(WSBase*)ws {
    
    if (ws.task == WS_CREATE_USER) {
        if (ws.data) {
            [self setUserDefaultInfoWithDict:ws.data[0]];
            [self trackingEventByName:@"Sign up" withAction:ClickActionType withCategory:AuthenticationCategoryType];
        }
        [AppData setCreatedProfileSuccessful];
        
        [self addPreferences];
        
    } else if (ws.task == WS_GET_MY_PREFERENCE){
        [self stopActivityIndicator];
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = nil;
        alert.msgAlert = NSLocalizedString(@"Profile created successfully.", nil);
        alert.firstBtnTitle = NSLocalizedString(@"OK", nil);
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
            alert.lblAlertMessage.textAlignment = NSTextAlignmentCenter;
            [alert.view layoutIfNeeded];
        });
        
        alert.blockFirstBtnAction = ^(void){
            [self updateSuccessRedirect];
        };
        
        [self showAlert:alert forNavigation:NO];
    }else if(ws.task == WS_ADD_MY_PREFERENCE || ws.task == WS_UPDATE_MY_PREFERENCE){
        
        if (![self isKindOfClass:[MyProfileViewController class]]) {
            [self getPreference];
        }else{
            [self stopActivityIndicator];
            getPreferencesFromAPI(nil);
            [self updateSuccessRedirect];
        }
    }
    
    // verify passcode successfully
    else if(ws.task == WS_B2C_VERIFY_PASSCODE)
    {
        [self stopActivityIndicator];
        PasscodeItem *passcodeItem = (PasscodeItem *)ws.data[0];
        if(passcodeItem.valid)
        {
            [self updateProfileWithAPI];
        }
        else
        {
            [self showAlertForRetryPasscode];
        }
    }
}

-(void)loadDataFailFrom:(WSBase *)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message
{
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
    }else{
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"ERROR!", nil);
        if ([message isEqualToString:@"ENR68-2"]) {
            alert.msgAlert =  NSLocalizedString(@"An account with this email address already exists. Click Cancel to Sign In.", nil);
            [self.emailText setBottomBorderRedColor];
            [self.emailText becomeFirstResponder];
        }else if ([message isEqualToString:@"ENR61-1"]) {
            alert.msgAlert =  NSLocalizedString(@"Please confirm that the value entered is correct.",nil);
        }else {
            alert.msgAlert = NSLocalizedString(@"An error has occurred. Check your internet settings or try again.", nil);
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.lblAlertMessage.textAlignment = NSTextAlignmentLeft;
            [alert.view layoutIfNeeded];
        });
        alert.firstBtnTitle = NSLocalizedString(@"OK", nil);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
        });
        
        [self showAlert:alert forNavigation:NO];
    }
}


- (void)WSBaseNetworkUnavailable{
    [self stopActivityIndicator];
}

#pragma mark KEYBOARD PROCESS
-(void)handleKeyboardWillShow:(NSNotification *)paramNotification
{
    NSValue *keyboardRectAsObject = [[paramNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         keyboardHeight = keyboardRect.size.height;
                         [self updateEdgeInsetForShowKeyboard];
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

-(void)handleKeyboardWillHide:(NSNotification *)paramNotification
{
    UIViewAnimationCurve curve = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[paramNotification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [UIView setAnimationCurve:curve];
                         [self updateEdgeInsetForHideKeyboard];
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
    
}


-(void)updateEdgeInsetForShowKeyboard
{
    _btnSC.constant = ((IPAD)?120:80) + keyboardHeight;
}


-(void)updateEdgeInsetForHideKeyboard
{
    _btnSC.constant = ((IPAD)?120:80);
    [self resignFirstResponderForAllTextField];
}

-(void)resignFirstResponderForAllTextField
{
    if(self.firstNameText.isFirstResponder)
    {
        [self.firstNameText resignFirstResponder];
    }
    else if(self.lastNameText.isFirstResponder)
    {
        [self.lastNameText resignFirstResponder];
    }
    else if(self.emailText.isFirstResponder)
    {
        [self.emailText resignFirstResponder];
    }
    else if(self.phoneNumberText.isFirstResponder)
    {
        [self.phoneNumberText resignFirstResponder];
    }
    else if(self.passcodeText.isFirstResponder)
    {
        [self.passcodeText resignFirstResponder];
    }
    else if(self.passwordText.isFirstResponder)
    {
        [self.passwordText resignFirstResponder];
    }
    else if(self.confirmPasswordText.isFirstResponder)
    {
        [self.confirmPasswordText resignFirstResponder];
    }
}

-(void)dismissKeyboard
{
    [self updateEdgeInsetForHideKeyboard];
}

#pragma mark LOCAL PROCESS
-(BOOL)verifyValueForTextField:(UITextField *)textFied andMessageError:(NSMutableString *)message
{
    NSString *currentText = nil;
    NSString *errorMsg = nil;
    BOOL isValid = NO;
    if(textFied == self.firstNameText)
    {
        currentText = currentFirstName;
        
        NSMutableString *tempError = [[NSMutableString alloc] init];
        if (![(textFied.isFirstResponder ? [textFied.text removeRedudantWhiteSpaceInText] :currentText) isValidName]) {
            isValid = NO;
            if (tempError.length > 0) {
                [tempError appendString:@"\n* "];
            }
            [tempError appendString:NSLocalizedString(@"A valid first name must be at least 2 characters long.", nil)];
        }else if (![(textFied.isFirstResponder ? textFied.text :currentText) isValidNameWithSpecialCharacter]) {
            isValid = NO;
            if (tempError.length > 0) {
                [tempError appendString:@"\n* "];
            }
            [tempError appendString:NSLocalizedString(@"Please enter a valid first name. Acceptable special characters are -, ' and space.", nil)];
            
        }else{
            isValid = YES;
        }
        
        errorMsg = tempError;
    }
    else if(textFied == self.lastNameText)
    {
        currentText = currentLastName;
        
        NSMutableString *tempError = [[NSMutableString alloc] init];
        if (![(textFied.isFirstResponder ? [textFied.text removeRedudantWhiteSpaceInText] :currentText) isValidName]) {
            isValid = NO;
            if (tempError.length > 0) {
                [tempError appendString:@"\n* "];
            }
            [tempError appendString:NSLocalizedString(@"A valid last name must be at least 2 characters long.", nil)];
        }else if (![(textFied.isFirstResponder ? textFied.text :currentText) isValidNameWithSpecialCharacter]) {
            isValid = NO;
            if (tempError.length > 0) {
                [tempError appendString:@"\n* "];
            }
            [tempError appendString:NSLocalizedString(@"Please enter a valid last name. Acceptable special characters are -, ' and space.", nil)];
        }else{
            isValid = YES;
        }
        
        errorMsg = tempError;
    }
    else if(textFied == self.emailText)
    {
        currentText = currentEmail;
        errorMsg = NSLocalizedString(@"Enter a valid email address.", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidEmail];
    }
    else if(textFied == self.phoneNumberText)
    {
        currentText = (textFied.isFirstResponder) ? textFied.text : currentPhone;
        errorMsg = NSLocalizedString(@"Please enter a valid phone number.", nil);
        
        if (![currentText isValidPhoneNumber]) {
            isValid = NO;
            errorMsg = NSLocalizedString(@"Please enter a valid phone number.", nil);
        }else{
            if ([currentText isValidCountryCode]) {
                currentText = [currentText stringByReplacingOccurrencesOfString:@"+" withString:@""];
                NSString *countryCode = [currentText getCoutryCode];
                if (currentText.length <= countryCode.length) {
                    isValid = NO;
                    errorMsg = NSLocalizedString(@"Please enter a valid phone number.", nil);
                }else {
                    isValid = YES;
                }
            }else {
                isValid = NO;
                errorMsg = NSLocalizedString(@"Please enter a valid country code.", nil);
            }
        }
    }
    else if(textFied == self.passcodeText)
    {
        currentText = currentPasscode;
        errorMsg = NSLocalizedString(@"Please enter a valid passcode.", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidPasscode];
    }
    else if(textFied == self.passwordText)
    {
        currentText = currentPassword;
        errorMsg = NSLocalizedString(@"Password must be between 10 and 25 characters, including at least 1 upper case, 1 lower case, 1 special character and 1 number.", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isValidStrongPassword];
    }
    else if(textFied == self.confirmPasswordText)
    {
        currentText = currentConfirmPassword;
        errorMsg = NSLocalizedString(@"The password entries do not match.", nil);
        isValid = [(textFied.isFirstResponder ? textFied.text :currentText) isEqualToString:currentPassword];
    }
    
    if((textFied.isFirstResponder ? textFied.text :currentText).length > 0)
    {
        if(!isValid)
        {
            (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
            [message appendString:errorMsg];
            [textFied setBottomBorderRedColor];
        }
        else
        {
            [textFied setBottomBolderDefaultColor];
        }
    }
    else
    {
        [textFied setBottomBorderRedColor];
    }
    
    return isValid;
}
- (BOOL)verifyValueForDropdown:(DropDownView*)dropdown andMessageError:(NSMutableString *)message{
    
    BOOL isValid = NO;
    if (dropdown == self.salutationDropDown) {
        if (self.salutationDropDown.valueStatus == DefaultValueStatus) {
            (message.length == 0) ? [message appendString:@"* "] : [message appendString:@"\n* "];
            [message appendString:NSLocalizedString(@"You have to choose a salutation.", nil)]; //All fields are required.
            isValid = NO;
            [self.salutationDropDown setBottomBorderRedColor];
        }else {
            isValid = YES;
            [self.salutationDropDown setBottomBolderDefaultColor];
        }
    }
    return isValid;
}

-(void)verifyAccountData:(BOOL)isUpdate
{
    NSMutableString *message = [[NSMutableString alloc] init];
    if (isUpdate) {
        if(self.firstNameText.text.length == 0 || self.lastNameText.text.length == 0 || self.emailText.text.length == 0 || self.phoneNumberText.text.length == 0)
        {
            [message appendString:@"* "];
            [message appendString:NSLocalizedString(@"All fields are required.", nil)];
            if (self.salutationDropDown.title.length > 10) {
                [self verifyValueForDropdown:self.salutationDropDown andMessageError:message];
            }
            [self verifyValueForTextField:self.firstNameText andMessageError:message];
            [self verifyValueForTextField:self.lastNameText andMessageError:message];
            [self verifyValueForTextField:self.emailText andMessageError:message];
            [self verifyValueForTextField:self.phoneNumberText andMessageError:message];
            
            [self createProfileInforWithMessage:message isCreatedSuccessful:NO];
        }
        else{
            if (self.salutationDropDown.title.length > 10) {
                [self verifyValueForDropdown:self.salutationDropDown andMessageError:message];
            }
            [self verifyValueForTextField:self.firstNameText andMessageError:message];
            [self verifyValueForTextField:self.lastNameText andMessageError:message];
            [self verifyValueForTextField:self.emailText andMessageError:message];
            [self verifyValueForTextField:self.phoneNumberText andMessageError:message];
            
            [self createProfileInforWithMessage:message isCreatedSuccessful:(message.length == 0)];
        }
    }else{
        if(self.firstNameText.text.length == 0 || self.lastNameText.text.length == 0 || self.emailText.text.length == 0 || self.phoneNumberText.text.length == 0 || self.passcodeText.text.length == 0 || self.passwordText.text.length == 0 || self.confirmPasswordText.text.length == 0)
        {
            [message appendString:@"* "];
            [message appendString:NSLocalizedString(@"All fields are required.", nil)];
            
            [self verifyValueForDropdown:self.salutationDropDown andMessageError:message];
            
            [self verifyValueForTextField:self.firstNameText andMessageError:message];
            [self verifyValueForTextField:self.lastNameText andMessageError:message];
            [self verifyValueForTextField:self.emailText andMessageError:message];
            [self verifyValueForTextField:self.phoneNumberText andMessageError:message];
            [self verifyValueForTextField:self.passcodeText andMessageError:message];
            [self verifyValueForTextField:self.passwordText andMessageError:message];
            [self verifyValueForTextField:self.confirmPasswordText andMessageError:message];
            
            [self createProfileInforWithMessage:message isCreatedSuccessful:NO];
        }
        else{
            
            [self verifyValueForDropdown:self.salutationDropDown andMessageError:message];

            [self verifyValueForTextField:self.firstNameText andMessageError:message];
            [self verifyValueForTextField:self.lastNameText andMessageError:message];
            [self verifyValueForTextField:self.emailText andMessageError:message];
            [self verifyValueForTextField:self.phoneNumberText andMessageError:message];
            [self verifyValueForTextField:self.passcodeText andMessageError:message];
            [self verifyValueForTextField:self.passwordText andMessageError:message];
            [self verifyValueForTextField:self.confirmPasswordText andMessageError:message];
            
            [self createProfileInforWithMessage:message isCreatedSuccessful:(message.length == 0)];
        }
    }
}

-(void)createProfileInforWithMessage:(NSString *)message isCreatedSuccessful:(BOOL)isSuccessful
{
    if(isSuccessful)
    {
        [self updateEdgeInsetForHideKeyboard];
        [self setTextViewsDefaultBottomBolder];
        if(![self isKindOfClass:[MyProfileViewController class]])
        {
            NSMutableDictionary *infoDict = [[NSMutableDictionary alloc] init];
            [infoDict setValue:self.passcodeText.text forKey:keyPasscode];
            
            [self startActivityIndicator];
            
            wsPasscodeVerification = [[WSB2CPasscodeVerfication alloc] init];
            wsPasscodeVerification.task = WS_B2C_VERIFY_PASSCODE;
            wsPasscodeVerification.delegate = self;
            [wsPasscodeVerification verifyPasscode:infoDict];
        }
        else
        {
            [self updateProfileWithAPI];
        }
        
    }
    else
    {
        NSMutableString *newMessage = [[NSMutableString alloc] init];
        [newMessage appendString:@"\n"];
        [newMessage appendString:message];
        [newMessage appendString:@"\n"];
        
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"Please confirm that the value entered is correct:", nil);
        alert.msgAlert = newMessage;
        alert.firstBtnTitle = NSLocalizedString(@"OK", nil);
        dispatch_async(dispatch_get_main_queue(), ^{
            alert.seconBtn.hidden = YES;
            alert.midView.alpha = 0.0f;;
            alert.lblAlertMessage.textAlignment = NSTextAlignmentLeft;
            [alert.view layoutIfNeeded];
        });
        
        alert.blockFirstBtnAction = ^(void){
            [self makeBecomeFirstResponderForTextField];
        };
        
        [self showAlert:alert forNavigation:NO];
         
    }
}

- (void) updateSuccessRedirect{
    
    appdelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
   
    [UIView transitionWithView:appdelegate.window
                      duration:0.5
                       options:UIViewAnimationOptionPreferredFramesPerSecond60
                    animations:^{
                        
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        
                        MenuViewController *menuViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
                        UINavigationController *menuNavigationViewController = [[UINavigationController alloc] initWithRootViewController:menuViewController];
                        UIViewController *fontViewController = [[HomeViewController alloc] init];
                        
                        SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:menuNavigationViewController frontViewController:fontViewController];
                        
                        revealController.delegate = appdelegate;
                        revealController.rearViewRevealWidth = SCREEN_WIDTH;
                        revealController.rearViewRevealOverdraw = 0.0f;
                        revealController.rearViewRevealDisplacement = 0.0f;
                        appdelegate.viewController = revealController;
                        appdelegate.window.rootViewController = appdelegate.viewController;
                        [appdelegate.window makeKeyWindow];
                        
                        UIViewController *newFrontController = [[HomeViewController alloc] init];
                        UINavigationController *newNavigationViewController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                        [revealController pushFrontViewController:newNavigationViewController animated:YES];
                        
                    }
                    completion:nil];
}

#pragma mark TEXT FIELD DELEGATE
-(void) makeBecomeFirstResponderForTextField
{
    [self resignFirstResponderForAllTextField];
    if(![currentFirstName isValidName] || ![currentFirstName isValidNameWithSpecialCharacter])
    {
        [self.firstNameText becomeFirstResponder];
    }
    else if(![currentLastName isValidName] || ![currentLastName isValidNameWithSpecialCharacter])
    {
        [self.lastNameText becomeFirstResponder];
    }
    else if(![currentEmail isValidEmail])
    {
        [self.emailText becomeFirstResponder];
    }
    else if(![currentPhone isValidPhoneNumber] || ![currentPhone isValidCountryCode])
    {
        [self.phoneNumberText becomeFirstResponder];
    }
    else if(![self.passwordText.text isValidPassword])
    {
        [self.passwordText becomeFirstResponder];
    }
    else if(![self.confirmPasswordText.text isEqualToString:currentPassword])
    {
        [self.confirmPasswordText becomeFirstResponder];
    }
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.text = [textField.text removeRedudantWhiteSpaceInText];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == self.firstNameText)
    {
        currentFirstName = textField.text;
        textField.text = [textField.text createMaskForText:NO];

    }
    else if(textField == self.lastNameText)
    {
        currentLastName = textField.text;
        textField.text =  [textField.text createMaskForText:NO];

    }
    else if(textField == self.phoneNumberText) {
        currentPhone = (![textField.text containsString:@"+"] && ![textField.text isEqualToString:@""]) ? [NSString stringWithFormat:@"+%@",textField.text] : textField.text;
        textField.text = currentPhone;
        textField.text =  [textField.text createMaskForText:NO];
    }
    else if(textField == self.emailText)
    {
        currentEmail = textField.text;
        if([textField.text containsString:@"@"])
        {
            textField.text = [textField.text createMaskForText:YES];
        }
    }
    else if(textField == self.passcodeText)
    {
        currentPasscode = textField.text;
    }
    else if(textField == self.passwordText)
    {
        currentPassword = textField.text;
    }
    else if(textField == self.confirmPasswordText)
    {
        currentConfirmPassword = textField.text;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == self.firstNameText)
    {
        textField.text = currentFirstName;
    }
    
    else if(textField == self.lastNameText)
    {
        textField.text = currentLastName;
    }
    
    else if(textField == self.phoneNumberText)
    {
        textField.text = currentPhone;
    }
    else if(textField == self.emailText)
    {
        textField.text = currentEmail;
    }
    else if(textField == self.passwordText)
    {
        textField.text = currentPassword;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([self isKindOfClass:[MyProfileViewController class]]) {
        if (textField == self.emailText) {
            return NO;
        }
    }
    if (textField == self.passwordText || textField == self.confirmPasswordText) {
        if ([string isEqualToString:@" "]) {
            return NO;
        }
    }
    NSString *inputString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (textField == self.firstNameText || textField == self.lastNameText) {
        if (inputString.length <= 19) {
            [self checkTextFieldIsChange:textField shouldChangeCharactersInRange:range replacementString:string];
        }
    }else if (textField == self.phoneNumberText) {
        if ([inputString containsString:@"+"]) {
            if (inputString.length <= PHONE_NUMBER_LENGTH + 1) {
                [self checkTextFieldIsChange:textField shouldChangeCharactersInRange:range replacementString:string];
            }
        }else {
            if (inputString.length <= PHONE_NUMBER_LENGTH) {
                [self checkTextFieldIsChange:textField shouldChangeCharactersInRange:range replacementString:string];
            }
        }
    }else{
        [self checkTextFieldIsChange:textField shouldChangeCharactersInRange:range replacementString:string];
    }
    
    return [self updateTextFiel:textField shouldChangeCharactersInRange:range replacementString:string];
}

-(void)checkTextFieldIsChange:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
}

-(BOOL)updateTextFiel:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(string.length > 1)
    {
        NSMutableString *newString = [[NSMutableString alloc] initWithString:[string removeRedudantWhiteSpaceInText]];
        NSString *resultText = [textField.text stringByReplacingCharactersInRange:range
                                                                       withString:string];
        resultText = [resultText removeRedudantWhiteSpaceInText];
        
        if(textField == self.firstNameText || textField == self.lastNameText)
        {
            textField.text = (resultText.length > 19) ? [resultText substringWithRange:NSMakeRange(0, 19)] : resultText;
        }
        if(textField == self.passwordText || textField == self.confirmPasswordText)
        {
            if([newString isValidStrongPassword])
            {
                textField.text = newString;
            }
            else if(string.length > 25)
            {
                textField.text = [newString substringToIndex:25];
            }
            else
            {
                textField.text = newString;
            }
        }
        if(textField == self.phoneNumberText)
        {
            if([newString isValidPhoneNumber] && [newString isValidCountryCode])
            {
                textField.text = newString;
            }
        }
        
        if(textField == self.emailText)
        {
            //textField.text = newString;
            textField.text = (resultText.length > EMAIL_MAX_LENGTH)?[resultText substringWithRange:NSMakeRange(0, EMAIL_MAX_LENGTH)] : resultText;
        }
        
        return NO;
    }
    
    if(textField == self.firstNameText || textField == self.lastNameText)
    {
        if(textField.text.length == 19 && ![string isEqualToString:@""])
        {
            return NO;
        }
        return YES;
    }
    if(textField == self.passwordText || textField == self.confirmPasswordText || textField == self.passcodeText)
    {
        if(textField.text.length == 25 && ![string isEqualToString:@""])
        {
            return NO;
        }
        return YES;
    }
    else if(textField == self.phoneNumberText)
    {
        int maxLength = ([textField.text containsString:@"+"]) ? PHONE_NUMBER_LENGTH+1 : PHONE_NUMBER_LENGTH;
        if(textField.text.length == maxLength && ![string isEqualToString:@""]) {
            return NO;
        }
        return YES;
    }
    else if(textField == self.emailText)
    {
        if(textField.text.length == EMAIL_MAX_LENGTH && ![string isEqualToString:@""])
        {
            return NO;
        }
        if([textField.text occurrenceCountOfCharacter:'@'] == 1 && [string isEqualToString:@"@"]){
            return NO;
        }
        
        return YES;
    }
    
    return YES;
}

#pragma mark LOGICAL FUNCTION
-(void)setTextViewsDefaultBottomBolder
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.firstNameText setBottomBolderDefaultColor];
        [self.lastNameText setBottomBolderDefaultColor];
        [self.emailText setBottomBolderDefaultColor];
        [self.phoneNumberText setBottomBolderDefaultColor];
        [self.passcodeText setBottomBolderDefaultColor];
        [self.passwordText setBottomBolderDefaultColor];
        [self.confirmPasswordText setBottomBolderDefaultColor];
        [self.salutationDropDown setBottomBolderDefaultColor];
    });
}
-(void) createAsteriskForTextField:(UITextField *)textField
{
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 8, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [textField setLeftView:leftView];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    
    asteriskImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"asterisk_icon"]];
    
    asteriskImage.frame = CGRectMake(0.0f, 0.0f, 20.0f, 20.0f);
    asteriskImage.contentMode = UIViewContentModeCenter;
    [textField setRightView:asteriskImage];
    [textField setRightViewMode:UITextFieldViewModeAlways];
    
}

- (void) changeUseLocation {
    
}

- (void) handleAsterickIcon{
    
}

-(NSString *)reformatForText:(UITextField *)textField WithNewString:(NSString *)newString WithRange:(NSRange)range
{
    NSMutableString *mutableString = [[NSMutableString alloc] initWithString:textField.text];
    NSRange newRange = range;
    if([newString isEqualToString:@""] && (range.location == 5 ||  range.location == 9))
    {
        newRange = NSMakeRange(range.location - 1, 1);
    }
    [mutableString replaceCharactersInRange:newRange withString:newString];
    mutableString = [[NSMutableString alloc] initWithString:[mutableString stringByReplacingOccurrencesOfString:@"-" withString:@""]];
    
    if(mutableString.length >= 1 && textField.tag == PHONE_NUMBER_TEXTFIELD_TAG)
    {
        [mutableString insertString:@"-" atIndex:1];
    }
    
    if(mutableString.length > 5)
    {
        [mutableString insertString:@"-" atIndex:5];
    }
    
    if(mutableString.length > 9 && textField.tag == PHONE_NUMBER_TEXTFIELD_TAG)
    {
        [mutableString insertString:@"-" atIndex:9];
    }
    
    return mutableString;
}
#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    
    CGPoint touchPoint = [touch locationInView:touch.view];
    
    UIView *view = touch.view.superview;
    if ([view isKindOfClass:[UITableViewCell class]]) {
        return NO;
    }else {
        if ([self isKindOfClass:[MyProfileViewController class]]) {
            for (UIView* view in touch.view.subviews) {
                if ((UITextField*)view == self.emailText && CGRectContainsPoint(self.emailText.frame, touchPoint)) {
                    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
                    if (profileDictionary.count > 2) {
                        self.emailText.text = [profileDictionary objectForKey:keyEmail];
                        break;
                    }
                }else{
                    if([self.emailText.text containsString:@"@"])
                    {
                        self.emailText.text = [self.emailText.text createMaskForText:YES];
                    }
                }
            }
        }
        
    }
    
    return YES;
    
}

#pragma mark - DropDownViewDelegate
- (void)didSelectItem:(DropDownView *)dropDownView atIndex:(int)index{
    [self changeValueDropDown];
}

- (void)changeValueDropDown{
    
}

@end
