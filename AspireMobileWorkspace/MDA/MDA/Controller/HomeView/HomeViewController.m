//
//  HomeViewController.m
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/13/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "HomeViewController.h"
#import "SWRevealViewController.h"
#import "UIView+Extension.h"
#import "NSString+Utis.h"
#import "UIButton+Extension.h"
#import "UILabel+Extension.h"
#import "NSLayoutConstraint+Multiplier.h"
#import "Constant.h"
#import "GalleryViewController.h"
#import "AskConciergeViewController.h"
#import "ExploreViewController.h"
#import "Common.h"
#import "AppData.h"
#import "CityViewController.h"
#import "AskConciergeConfirmationViewController.h"
#import "ChangePasswordViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    if ([[SessionData shareSessiondata] hasForgotPassword]) {
//        ChangePasswordViewController *changePasswordVC = [[ChangePasswordViewController alloc] init];
//        [self presentViewController:changePasswordVC animated:NO completion:nil];
//    }else
    if (_gotoAskConcierge == YES) {
        AskConciergeViewController *vc = [[AskConciergeViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }

    [self setTextStyleForView];
    [self createMenuBarButton];
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"CONCIERGE", nil)];
    [self trackingScreenByName:@"Home"];
}

-(void)setTextStyleForView
{
    [self.btnGallery setTitleColor:colorFromHexString(@"#F2F2F2") forState:UIControlStateNormal];
    [self.btnGallery setTitle:NSLocalizedString(@"SEE WHAT WE CAN DO FOR YOU", nil) forState:UIControlStateNormal];
    NSString *message = NSLocalizedString(@"SEE WHAT WE CAN DO FOR YOU", nil);
    NSRange range = NSMakeRange(0, message.length);
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:message];
    [attributeString addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize headerTextSize]] range:range];
    [attributeString addAttribute:NSKernAttributeName value:@(1.2) range:range];
    self.btnGallery.titleLabel.attributedText = attributeString;
    
    [self.btnGallery setWhiteBackgroundColorForTouchingStatus];
    self.btnGallery.layer.borderWidth = 1;
    self.btnGallery.layer.borderColor = [[UIColor whiteColor] CGColor];
}


-(void) initView{
    [self.footerView setBackgroundColorForView];
    
    self.lblWelcome.attributedText = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Welcome", nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Georgia" size:44.0f * SCREEN_SCALE], NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setAlignment:self.lblWelcomeMessage.textAlignment];
    [style setLineSpacing:5];
    
    self.lblWelcomeMessage.attributedText = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"to your Mobile Concierge.\nFrom the big ideas to all the details,\n let us handle it for you.", nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionGreetingSize]], NSForegroundColorAttributeName:[UIColor whiteColor],NSParagraphStyleAttributeName:style}];
    
    self.lblAskConciergeMessage.attributedText = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Tell us what you need,\nand we'll take it from there.", nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionTextSize]], NSForegroundColorAttributeName:[AppColor placeholderTextColor]}];
    
    self.lblExploreMessage.attributedText = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Browse through our best offers and recommendations. If you get inspired, you can book right here.", nil) attributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize descriptionTextSize]], NSForegroundColorAttributeName:[AppColor placeholderTextColor]}];
    
    [self.lblExploreMessage setLineSpacing:5*SCREEN_SCALE];
    [self.lblAskConciergeMessage setLineSpacing:5*SCREEN_SCALE];
    
    //[self.btnExplore setContentEdgeInsets:UIEdgeInsetsMake(self.btnExplore.contentEdgeInsets.top*SCREEN_SCALE, 0, 0, 0)];
    //[self.btnAskConcierge setContentEdgeInsets:UIEdgeInsetsMake(self.btnAskConcierge.contentEdgeInsets.top*SCREEN_SCALE, 0, 0, 0)];
    
    // 
    NSDictionary *attributes = @{NSForegroundColorAttributeName:
                                     [AppColor backgroundColor],
                                 
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_BOLD size:[AppSize descriptionTextSize]],
                                 
                                 NSKernAttributeName: @1.6};
    
    NSDictionary *attributesSelected = @{NSForegroundColorAttributeName:
                                             [AppColor highlightButtonColor],
                                 
                                         NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_BOLD size:[AppSize descriptionTextSize]],
                                 
                                         NSKernAttributeName: @1.6};
    
    [self.btnExplore setAttributedTitle:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"EXPLORE", nil) attributes:attributes]  forState:UIControlStateNormal];
    [self.btnExplore setAttributedTitle:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"EXPLORE", nil) attributes:attributesSelected]  forState:UIControlStateHighlighted];
    
    
    [self.btnAskConcierge setAttributedTitle:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"ASK THE CONCIERGE", nil) attributes:attributes]  forState:UIControlStateNormal];
    [self.btnAskConcierge setAttributedTitle:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"ASK THE CONCIERGE", nil) attributes:attributesSelected]  forState:UIControlStateHighlighted];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self updateLayoutConstraints];
        self.topConstraintFooterView.constant = self.topConstraintFooterView.constant*SCREEN_SCALE;
        self.topConstraintGalleryView.constant = self.topConstraintGalleryView.constant*SCREEN_SCALE;
        self.topConstraintWelcomeView.constant = self.topConstraintWelcomeView.constant*SCREEN_SCALE;
        self.topConstraintHeightAskButton.constant = self.topConstraintHeightAskButton.constant*SCREEN_SCALE;
        if (IPAD) {
            [self.heightConstaintFooterWithTopView updateMultiplier:60/SCREEN_HEIGHT];
        }
        //self.topExploreLabelConstraint.constant = self.topExploreLabelConstraint.constant*SCREEN_SCALE;
        //self.topAskConciergeLabelConstraint.constant = self.topAskConciergeLabelConstraint.constant*SCREEN_SCALE;
    });
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [self setNavigationBarColor];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)touchAskConcierge:(id)sender {
    self.btnAskConcierge.highlighted = NO;
    AskConciergeViewController *vc = [[AskConciergeViewController alloc] init];
    
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)touchViewGallery:(id)sender {
    GalleryViewController *vc = [[GalleryViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)touchExplore:(id)sender {
    UIViewController *newFrontController = nil;
    CityItem *selectedCity = [AppData getSelectedCity];
    if(selectedCity)
    {
        ExploreViewController *exploreViewController = [[ExploreViewController alloc] init];
        exploreViewController.currentCity = selectedCity;
        newFrontController = exploreViewController;
        
    }
    else
    {
        newFrontController = [[CityViewController alloc] init];
    }
    [self.navigationController pushViewController:newFrontController animated:YES];
}

- (void)updateLayoutConstraints {
    
    CGFloat marginTop = 6.0f;
    CGFloat heightTextButton = 16 * SCREEN_SCALE;
    
    CGFloat topInsetAskConcierge = (CGRectGetHeight(self.btnAskConcierge.superview.frame) - (heightTextButton + marginTop + CGRectGetHeight(self.lblAskConciergeMessage.frame)))/2;
    [self.btnAskConcierge setContentEdgeInsets:UIEdgeInsetsMake(topInsetAskConcierge, 0, 0, 0)];
    self.topAskConciergeLabelConstraint.constant = (topInsetAskConcierge + heightTextButton + marginTop);
    
    CGFloat topInsetExplore = (CGRectGetHeight(self.btnExplore.superview.frame) - (heightTextButton + marginTop + CGRectGetHeight(self.lblExploreMessage.frame)))/2;
    [self.btnExplore setContentEdgeInsets:UIEdgeInsetsMake(topInsetExplore, 0, 0, 0)];
    self.topExploreLabelConstraint.constant = (topInsetExplore + heightTextButton + marginTop);
}


@end
