//
//  ExploreViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/9/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "ExploreViewController.h"
#import "UIButton+Extension.h"
#import "ExploreTableViewCell.h"
#import "CityViewController.h"
#import "CityItem.h"
#import "CategoryViewController.h"
#import "CategoryItem.h"
#import "SearchViewController.h"
#import "Constant.h"
#import "SearchHeaderView.h"
#import "UIView+Extension.h"
#import "BaseResponseObject.h"
#import "UITableView+Extensions.h"
#import "AFImageDownloader.h"
#import "UIImageView+AFNetworking.h"
#import "AFURLResponseSerialization.h"
#import "WSBase.h"

#import "SWRevealViewController.h"
#import "SelectingCategoryDelegate.h"
#import "CategoryCityGuideViewController.h"

#import <Foundation/Foundation.h>

#import "AppData.h"

#import "ExploreQuestionTableViewCell.h"

#import "AnswerItem.h"
#import "CategoryItem.h"
#import "TileItem.h"
#import "SearchItem.h"

#import "NSString+Utis.h"
#import "NSTimer+Block.h"

#import "WSB2CGetTiles.h"
#import "WSB2CGetQuestions.h"
#import "WSB2CSearch.h"

#import <objc/runtime.h>

#import "TableHeaderView.h"
#import "ImageUtilities.h"
#import "UILabel+Extension.h"

#import "MenuTestLocation.h"
#import "ExploreVenueDetailViewController.h"

#import "PreferenceObject.h"
#import "LocationComponent.h"
#import "SyncService.h"

#import "CCAMapCategories.h"
#import "AppDelegate.h"
#import <AspireApiFramework/AspireApiFramework.h>

#define TEST_LOCATION @"TEST_LOCATION"

typedef enum : NSUInteger {
    EXPLORE_NORMAL,
    EXPLORE_SEARCH
} TYPE_LOAD_DATA_EXPLORE;

@interface ExploreViewController ()<UITableViewDelegate, UITableViewDataSource, SelectingCityDelegate, SearchDelegate, DataLoadDelegate, SelectingCategoryDelegate, UITextFieldDelegate,MenuTestLocationProtocol,UIScrollViewDelegate,BaseTableViewCellDelegate>
{
    CGFloat searchViewYPos;
    CGFloat lastContentOffset;
    CGFloat heightForHeaderView;
    CGFloat orginalHeightHeaderView;
    BOOL isHideMenuButtons;
    enum WSTASK currentWSTask;
    NSUInteger session;
    BOOL isTappedClearBtn;
    
    BOOL isAllowedUseLocation;
    NSString* cuisinePreference;
    BOOL shouldCheckingDifferent;
    
    IBOutlet UIStackView *vsStackView;
    MenuTestLocation* menuLocation;
    
    NSInteger selectedItem;
    
    // force reload index selected item
    BOOL shouldRefreshIndexSelectedCellAfterChangeCuisinePreference, isSelectedAnotherCategory;
    
    // timer checking loading
    NSTimer* tmrCheckingLoading, *tmrCheckNetwork;
    BOOL isStopRequestCauseNetworkUnvailable;
}

@property (nonatomic, strong) CategoryItem *currentCategory;
@property (nonatomic, copy) NSString *searchText;
@property (nonatomic, copy) NSString *currentTitle;;
@property (nonatomic, strong) WSB2CGetQuestions *wsGetQuestion;
@property (nonatomic, strong) WSB2CGetTiles *wsGetTiles;
@property (nonatomic, strong) WSB2CSearch *wsSearch;

@property (nonatomic, assign) BOOL isHasMoreData;
@property (nonatomic, strong) SearchHeaderView *searchView;
@property (nonatomic, strong) TableHeaderView *tableHeaderView;
@end

@implementation ExploreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    selectedItem = -1;
    
    UITapGestureRecognizer *tappedOutsideKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tappedOutsideKeyboard.cancelsTouchesInView = NO;
    
    [self.view addGestureRecognizer:tappedOutsideKeyboard];
    
    // mark - check after use update permission location from this page
    isAllowedUseLocation = [[SessionData shareSessiondata] isUseLocation];
    if([SessionData shareSessiondata].arrayPreferences.count > 0) {
        for (PreferenceObject* obj in [SessionData shareSessiondata].arrayPreferences) {
            if(obj.type == DiningPreferenceType) {
                cuisinePreference = obj.value;
                break;
            }
        }
    }
    
    /*
     *  Listern User Change permission in settings
     */
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(forceReloadWhenUserChangePermissionLocation:)
                                                 name:LOCATION_COMPONENT_ISAUTHORIZED_NOTIFICATION_FOR_EXPLOREVIEW
                                               object:nil];
    
    
    [LocationComponent getLocation].onChecking = ^{
        if(self.searchText.length > 0) {
            [self startCheckingLoading];
        }
    };
    
    [self trackingScreenByName:@"Explore"];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setNavigationBarWithDefaultColorAndTitle:self.currentTitle];
    
    
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:LOCATION_COMPONENT_ISAUTHORIZED_NOTIFICATION_FOR_EXPLOREVIEW object:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    UIPanGestureRecognizer *popNavigationBarRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.revealViewController action:@selector(_handleRevealGesture:)];
    [self.navigationController.navigationBar addGestureRecognizer:popNavigationBarRecognizer];
    orginalHeightHeaderView = self.searchViewOutletHeightConstraint.constant;
    
    if([self.searchView.searchText isFirstResponder])
    {
        [self.searchView.searchText resignFirstResponder];
    }
    
    if(isTappedClearBtn)
    {
        [self getDataBaseOnCity:self.currentCity];
    }
    shouldRefreshIndexSelectedCellAfterChangeCuisinePreference = YES;
    UserObject *userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
    if(shouldCheckingDifferent && [self.currentCategory.code isEqualToString:@"dining"] && ![self.currentCategory.supperCategoryID isEqualToString:@"80"]) {
        NSString* currentCuisine = cuisinePreference;
        if(userObject.arrayPreferences.count > 0) {
            for (PreferenceObject* obj in userObject.arrayPreferences) {
                if(obj.type == DiningPreferenceType && ![obj.value.lowercaseString isEqualToString:@"na".lowercaseString]) {
                    currentCuisine = obj.value;
                    break;
                }
            }
        }
        // check if use change permission && change preference cuisine, then force resort table
        if(isAllowedUseLocation != [[SessionData shareSessiondata] isUseLocation] && [AppData shouldGetDistanceForListDiningFromCity:self.currentCity]) {
            isAllowedUseLocation = [[SessionData shareSessiondata] isUseLocation];
            cuisinePreference = currentCuisine;
            
            if(currentWSTask == WS_B2C_SEARCH) {
                if(self.wsSearch) {
                    [self searchWithText:self.searchText withHasOffer:self.wsSearch.hasOffer];
                } else {
                    [self searchWithText:self.searchText withHasOffer:NO];
                }
                [self startCheckingLoading];
            } else
                [self getDataBaseOnCity:self.currentCity];
            
        } else if( ![currentCuisine isEqualToString:cuisinePreference]) {
            cuisinePreference = currentCuisine;
            [self resortData];
        }
    }
    shouldCheckingDifferent = YES;
    
    isTappedClearBtn = NO;
}

-(void)dismissKeyboard
{
    if([self.searchView.searchText isFirstResponder])
    {
        [self.searchView.searchText resignFirstResponder];
    }
}

-(void)initView
{
    [self createMenuBarButton];
    self.currentTitle = NSLocalizedString(@"EXPLORE", nil);
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ExploreTableViewCell" bundle:nil] forCellReuseIdentifier:@"ExploreTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ExploreQuestionTableViewCell" bundle:nil] forCellReuseIdentifier:@"ExploreQuestionTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"DiningListTableViewCell" bundle:nil] forCellReuseIdentifier:@"DiningListTableViewCell"];
    
    self.tableView.rowHeight = 120.f*SCREEN_SCALE;//UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 120.f*SCREEN_SCALE;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    AFImageDownloader *downloader = [UIImageView sharedImageDownloader];
    AFImageResponseSerializer* serializer =  (AFImageResponseSerializer *) downloader.sessionManager.responseSerializer;
    serializer.acceptableContentTypes = [serializer.acceptableContentTypes setByAddingObject:@"image/jpg"];
    self.searchViewOutletHeightConstraint.constant = 0.0f;
    //    [self.searchViewOutlet setbackgroundColorForLineMenu];
    
    //  Set up menu bar
    [self.cityBtn setImage:[UIImage imageNamed:@"location_white_icon"] forState:UIControlStateNormal];
    [self.cityBtn setImage:[UIImage imageNamed:@"location_interaction_icon"] forState:UIControlStateHighlighted];
    [self.cityBtn setTitle:self.currentCity.name forState:UIControlStateNormal];
    self.cityBtn.titleLabel.numberOfLines = 2;
    self.cityBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.cityBtn centerVertically];
    [self.cityBtn configureDecorationForExploreButton];
//    [self.cityBtn configureDecorationBlackTitleButtonForTouchingStatus];
    [self.cityBtn addTarget:self action:@selector(selectCityAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.categoryBtn setImage:[UIImage imageNamed:@"category_white_icon"] forState:UIControlStateNormal];
    [self.categoryBtn setImage:[UIImage imageNamed:@"category_interaction_icon"] forState:UIControlStateHighlighted];
    [self.categoryBtn setTitle:self.currentCategory.categoryName forState:UIControlStateNormal];
    self.categoryBtn.titleLabel.numberOfLines = 2;
    self.categoryBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.categoryBtn centerVertically];
    [self.categoryBtn configureDecorationForExploreButton];
//    [self.categoryBtn configureDecorationBlackTitleButtonForTouchingStatus];
    [self.categoryBtn addTarget:self action:@selector(selectCategoryAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.searchBtn setImage:[UIImage imageNamed:@"search_white_icon"] forState:UIControlStateNormal];
    [self.searchBtn setImage:[UIImage imageNamed:@"search_interaction_icon"] forState:UIControlStateHighlighted];
    [self.searchBtn setTitle:NSLocalizedString(@"Search", nil) forState:UIControlStateNormal];
    self.searchBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.searchBtn.titleLabel.numberOfLines = 2;
    self.searchBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.searchBtn centerVertically];
    [self.searchBtn configureDecorationForExploreButton];
//    [self.searchBtn configureDecorationBlackTitleButtonForTouchingStatus];
    [self.searchBtn addTarget:self action:@selector(selectSearchAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.firstLine setbackgroundColorForLineMenu];
    [self.secondLine setbackgroundColorForLineMenu];
    
    /*
     *  Check if bundle if internal => Turn on custom location for TEST
     */
//    if([[[NSBundle mainBundle] bundleIdentifier] isEqualToString:@"ios.MasterCardConciergeUSDemoInternal"]) {
//        menuLocation = [MenuTestLocation new];
//        menuLocation.delegate =self;
//        menuLocation.hidden = YES;
//        NSArray* data = [[NSUserDefaults standardUserDefaults] objectForKey:TEST_LOCATION];
//        if(data.count == 2) {
//            [menuLocation setData:@[[data firstObject],[data lastObject]]];
//        }
//    }
}

-(void) initData
{
    // Only show Location Service alert if the category is Dining
    [[SessionData shareSessiondata] setIsShownLocationServiceAlert:YES];
    
    self.currentCategory = [[CategoryItem alloc] init];
    self.currentCategory.categoryName = NSLocalizedString(@"Category", nil);
    self.currentCategory.code = @"all";
    self.currentCategory.categoriesForService = LIST_ALL_TILES;
    
    self.exploreLst = [[NSMutableArray alloc] init];
    
    [self loadDataForViewWithType:EXPLORE_NORMAL withCityguideCategoryName:nil];
}

/*
 *  Reload data when received message change permission location in setting
 */
- (void) forceReloadWhenUserChangePermissionLocation:(NSNotification *)notification {

    if([self.currentCategory.code isEqualToString:@"dining"] && ![self.currentCategory.supperCategoryID isEqualToString:@"80"] && [AppData shouldGetDistanceForListDiningFromCity:self.currentCity]) {
        if(currentWSTask == WS_B2C_SEARCH) {
            if(self.wsSearch) {
                [self searchWithText:self.searchText withHasOffer:self.wsSearch.hasOffer];
            } else {
                [self searchWithText:self.searchText withHasOffer:NO];
            }
            [self startCheckingLoading];
        } else
            [self loadDataForViewWithType:EXPLORE_NORMAL withCityguideCategoryName:nil];
    }
}

- (void) resortData {
    
    if(self.exploreLst.count > 0) {
        
        BOOL isSortedDistance = [AppData shouldGetDistanceForListDiningFromCity:self.currentCity];
        BOOL isSortedCuisine = NO;
        
        isSortedCuisine = cuisinePreference.length > 0;
        
        if (![self.currentCategory.code isEqualToString:@"dining"]) {
            isSortedCuisine = NO;
            isSortedDistance = NO;
        }
        
        if(isSortedCuisine) {
            NSMutableArray* tempABC = [NSMutableArray new];
            NSMutableArray* tempCuisine = [NSMutableArray new];
            for (id item in self.exploreLst) {
                if(((BaseItem*)item).userDefined1 != nil) {
                    if([[cuisinePreference lowercaseString] containsString:[((BaseItem*)item).userDefined1 lowercaseString]] || [[((BaseItem*)item).userDefined1 lowercaseString] containsString:[cuisinePreference lowercaseString]]) {
                        [tempCuisine addObject:item];
                    } else {
                        [tempABC addObject:item];
                    }
                } else {
                    [tempABC addObject:item];
                }
            }
            
            NSArray *sortedABC = [tempABC sortedArrayUsingComparator:^NSComparisonResult(BaseItem *obj1, BaseItem *obj2){ return [[obj1.name lowercaseString] compare:[obj2.name lowercaseString] options:NSLiteralSearch];}];
            
            NSArray *sortedCuisine = [tempCuisine sortedArrayUsingComparator:^NSComparisonResult(AnswerItem *obj1, AnswerItem *obj2){ return [[obj1.name lowercaseString] compare:[obj2.name lowercaseString]  options:NSLiteralSearch];}];
            
            [self.exploreLst removeAllObjects];
            if(sortedCuisine.count > 0)
                [self.exploreLst addObjectsFromArray:sortedCuisine];
            if(sortedABC.count > 0)
                [self.exploreLst addObjectsFromArray:sortedABC];
        }
        
        if(isSortedDistance) {
            NSMutableArray* tempABC = [NSMutableArray new];
            NSMutableArray* tempCuisineABC = [NSMutableArray new];
            NSMutableArray* tempCuisineDistance = [NSMutableArray new];
            NSMutableArray* tempDistance = [NSMutableArray new];
            double maxDistance = 10*1609.344;
            if([LIST_REGIONS containsObject:self.currentCity.regionCode]) {
                maxDistance = 50*1609.344;
            }
            for (BaseItem* item in self.exploreLst) {
                if(isSortedCuisine) {
                    if(item.distance <= maxDistance)/* 10 miles */ {
                        BOOL check = [[item.userDefined1 lowercaseString] containsString:[cuisinePreference lowercaseString]];
                        if([item.userDefined1 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0)
                            check = ([[item.userDefined1 lowercaseString] containsString:[cuisinePreference lowercaseString]] ||
                                     [[cuisinePreference lowercaseString] containsString:[item.userDefined1 lowercaseString]]);
                        if(check) {
                            [tempCuisineDistance addObject:item];
                        } else {
                            [tempDistance addObject:item];
                        }
                    } else {
                        BOOL check = [[item.userDefined1 lowercaseString] containsString:[cuisinePreference lowercaseString]];
                        if([item.userDefined1 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0)
                            check = ([[item.userDefined1 lowercaseString] containsString:[cuisinePreference lowercaseString]] ||
                                     [[cuisinePreference lowercaseString] containsString:[item.userDefined1 lowercaseString]]);
                        if(check) {
                            [tempCuisineABC addObject:item];
                        } else
                            [tempABC addObject:item];
                    }
                } else {
                    
                    if(item.distance <= maxDistance)/* 10 miles */ {
                        
                        [tempDistance addObject:item];
                        
                    } else {
                        
                        [tempABC addObject:item];
                    }
                }
            }
            
            NSArray *sortedCuisineABC = [tempCuisineABC sortedArrayUsingComparator:^NSComparisonResult(BaseItem *obj1, BaseItem *obj2){ return [[obj1.name lowercaseString] compare:[obj2.name lowercaseString]  options:NSLiteralSearch];}];
            
            NSArray *sortedABC = isSortedCuisine?tempABC:[tempABC sortedArrayUsingComparator:^NSComparisonResult(BaseItem *obj1, BaseItem *obj2){ return [[obj1.name lowercaseString] compare:[obj2.name lowercaseString]  options:NSLiteralSearch];}];
            
            NSArray *sortedCuisineDistance = [tempCuisineDistance sortedArrayUsingComparator:^NSComparisonResult(AnswerItem *obj1, AnswerItem *obj2){ return obj1.distance > obj2.distance;}];
            
            NSArray *sortedDistance = [tempDistance sortedArrayUsingComparator:^NSComparisonResult(AnswerItem *obj1, AnswerItem *obj2){ return obj1.distance > obj2.distance;}];
            
            [self.exploreLst removeAllObjects];
            if(sortedCuisineDistance.count > 0)
                [self.exploreLst addObjectsFromArray:sortedCuisineDistance];
            if(sortedCuisineABC.count > 0)
                [self.exploreLst addObjectsFromArray:sortedCuisineABC];
            if(sortedDistance.count > 0)
                [self.exploreLst addObjectsFromArray:sortedDistance];
            if(sortedABC.count > 0)
                [self.exploreLst addObjectsFromArray:sortedABC];
        }
        
        if(!isSortedCuisine && !isSortedDistance) {
            NSArray *sortedABC = [self.exploreLst sortedArrayUsingComparator:^NSComparisonResult(BaseItem *obj1, BaseItem *obj2){ return [[obj1.name lowercaseString] compare:[obj2.name lowercaseString]];}];
            [self.exploreLst removeAllObjects];
            [self.exploreLst addObjectsFromArray:sortedABC];
        }
    }
    [self.tableView reloadData];
}
#pragma mark - BUTTON ACTION

-(void)selectCityAction:(id)sender
{
    CityViewController *cityViewController = [[CityViewController alloc] init];
    cityViewController.delegate = self;
    cityViewController.currentCity = self.currentCity;
    
    [self.navigationController pushViewController:cityViewController animated:YES];
}

-(void)selectCategoryAction:(id)sender
{
    CategoryViewController *categoryViewController = [[CategoryViewController alloc] init];
    categoryViewController.delegate = self;
    categoryViewController.currentCity = self.currentCity;
    
    [self.navigationController pushViewController:categoryViewController animated:NO];
}

-(void)selectSearchAction:(id)sender
{
    if([self.searchView.searchText isFirstResponder])
    {
        [self.searchView.searchText resignFirstResponder];
    }
    SearchViewController *searchViewController = [[SearchViewController alloc] init];
    searchViewController.delegate = self;
    searchViewController.currentKeyWord = ![sender isKindOfClass:[UIButton class]] ? self.searchText : @"";
    
    [self.navigationController pushViewController:searchViewController animated:YES];
    if(!isTappedClearBtn)
        [searchViewController checkOffer:self.searchView.isSearchOffer];
}

-(void)navigateToCategoryCityGuide:(id)sender
{
    if(self.currentCity.subCategories.count > 0)
    {
        CategoryCityGuideViewController *categoryCityGuideViewController = [[CategoryCityGuideViewController alloc] init];
        categoryCityGuideViewController.delegate = self;
        categoryCityGuideViewController.subCategories = self.currentCity.subCategories;
        
        [self.navigationController pushViewController:categoryCityGuideViewController animated:YES];
    }
}

#pragma mark - MENU TEST LOCATION
- (void) MenuTestLocation:(MenuTestLocation *)view onSelectEvent:(NSUInteger)index withUserInfo:(id)userInfo {
    if(index == 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                       message:NSLocalizedString(@"Would you like to?", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *firstAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Only clear", nil)
                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                  [view clear];
                                                                  
                                                                  [[NSUserDefaults standardUserDefaults] removeObjectForKey:TEST_LOCATION];
                                                                  [[NSUserDefaults standardUserDefaults] synchronize];
                                                              }];
        UIAlertAction *secondAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Clear & Hide", nil)
                                                               style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                   [view clear];
                                                                   
                                                                   [[NSUserDefaults standardUserDefaults] removeObjectForKey:TEST_LOCATION];
                                                                   [[NSUserDefaults standardUserDefaults] synchronize];
                                                                   [view hideKeyboard];
                                                                   
                                                                   [UIView transitionWithView:menuLocation
                                                                                     duration:0.2
                                                                                      options:UIViewAnimationOptionTransitionCrossDissolve
                                                                                   animations:^{
                                                                                       menuLocation.hidden = YES;
                                                                                   }
                                                                                   completion:NULL];
                                                                   [self Explore:(id)view onSelectCategory:self.currentCategory];
                                                               }];
        [alert addAction:firstAction];
        [alert addAction:secondAction];
        
        [self.navigationController presentViewController:alert animated:YES completion:nil];
    } else if (index == 1) {
        if([userInfo isKindOfClass:[NSArray class]]) {
            NSArray* data = userInfo;
            if([data containsObject:@"0"]) {
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:TEST_LOCATION];
            } else {
                [[NSUserDefaults standardUserDefaults] setObject:userInfo forKey:TEST_LOCATION];
            }
            [[NSUserDefaults standardUserDefaults] synchronize];
            [UIView transitionWithView:menuLocation
                              duration:0.2
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                menuLocation.hidden = YES;
                            }
                            completion:NULL];
            [self getDataBaseOnCity:self.currentCity];
            [menuLocation hideKeyboard];
        }
    }
}

#pragma mark - 🚸 TableVIewCell Delegate 🚸
- (CGFloat) BaseTableViewCellWhatCellSelected:(BaseTableViewCell *)cell {
    //    NSLog(@"%s: Selected Cell: %ld",__PRETTY_FUNCTION__,(long)selectedItem);
    return selectedItem;
}

#pragma mark TABLEVIEW DELEGATE
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.exploreLst.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if((currentWSTask == WS_B2C_GET_QUESTIONS && [self.currentCategory.supperCategoryID isEqualToString:@"80"]) || currentWSTask == WS_B2C_SEARCH)
    {
        ExploreQuestionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ExploreQuestionTableViewCell"];
        BaseItem *item = (BaseItem*)[self.exploreLst objectAtIndex:indexPath.row];
        cell.delegate =self;
        cell.tag = indexPath.row;
        cell.nameLabel.text = item.name;
        if(currentWSTask == WS_B2C_SEARCH && [item isKindOfClass:[SearchItem class]]) {
            if([((SearchItem*)item).product isEqualToString:@"IA"]) {
                cell.addressLabel.text = item.address3.length > 0?[self replaceMutilSpaceForString:item.address3 withPatterns:@"  +"]:@"  ";
            } else {
                NSString* add = @" ";
                if([[CCAMapCategories getCategoriesIDsMutilCities] containsObject:item.ID]) {
                    add = NSLocalizedString(@"Multiple Cities", nil);
                }
                cell.addressLabel.text = add;
            }
        } else {
            cell.addressLabel.text = item.address.length > 0?[self replaceMutilSpaceForString:item.address withPatterns:@"  +"]:@"  ";
        }
        
        cell.descriptionLabel.text = [self replaceMutilSpaceForString:item.offer withPatterns:@"  +"];
        cell.offerImage.hidden = !item.isOffer;
        [cell.nameLabel setLineSpacing:1.6];
        [cell.addressLabel setLineSpacing:1.6];
        //        [cell.descriptionLabel setLineSpacing:1.6];
        /*
         *  Display distance for Test
         */
        //        if(item.distance != CGFLOAT_MAX/* && [[[NSBundle mainBundle] bundleIdentifier] isEqualToString:@"ios.MasterCardConciergeUSDemoInternal"]*/)
        //            cell.addressLabel.text = [cell.addressLabel.text stringByAppendingFormat:@" (%.2f miles)",(item.distance/1609.344)];
        return cell;
    }
    else
    {
        ExploreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ExploreTableViewCell"];
        cell.delegate =self;
        cell.tag = indexPath.row;
        BaseItem *item = [_exploreLst objectAtIndex:indexPath.row];
        cell.nameLbl.text = item.name;
        
        //        if(currentWSTask == WS_B2C_GET_TILES){
        //            cell.addressLbl.text = item.address ? item.address : @"Multiple Cities";
        //        }
        //        else{
        //            cell.addressLbl.text = item.address;
        //        }
        if([[item.categoryCode lowercaseString] isEqualToString:@"dining"]) {
            cell.addressLbl.text = item.address;//? item.address : @"Multiple Cities";
            if([self.currentCategory.supperCategoryID isEqualToString:@"80"])
                cell.addressLbl.text = item.address3;
            else {
                NSMutableString* temp = [NSMutableString stringWithString:@""];
                if([@[@"México"] containsObject:self.currentCity.countryCode]) {
                    if(item.cityName.length > 0)
                        [temp appendFormat:@"%@",item.cityName];
                    if(item.state.length > 0)
                        [temp appendFormat:temp.length>0?@", %@":@"%@",item.state];
                    
                } else {
                    if(item.cityName.length > 0)
                        [temp appendFormat:@"%@",item.cityName];
                    if(item.countryName.length > 0)
                        [temp appendFormat:temp.length>0?@", %@":@"%@",item.countryName];
                }
                if(temp.length > 0)
                    cell.addressLbl.text = temp;
            }
            
        } else {
            cell.addressLbl.text = item.address.length > 0?item.address:@" ";
        }
        
        cell.offerLbl.text = [self replaceMutilSpaceForString:item.offer withPatterns:@"  +"];
        
        cell.exploreImg.image = [UIImage imageNamed:@"placehoder_explore_list"];
        if(item.imageURL && item.imageURL.length > 0){
            __weak typeof(cell) weakCell = cell;
            
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[item.imageURL removeBlankFromURL]]] ;
            
            [cell.exploreImg setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"placehoder_explore_detail"] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                
                weakCell.exploreImg.image = image;
                
            } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
            }];
            
        }
        else if(item.image){
            cell.exploreImg.image = item.image;
            
        }
        if ([self.currentCategory.code isEqualToString:@"all"]) {
            cell.categoryLabel.text = [item.categoryCode isEqualToString:@"cityguide"] ? NSLocalizedString(@"City Guide",nil) : item.categoryName;
            cell.maskImageView.hidden = NO;
            
            if([item.categoryName containsString:@"\n"]) {
                cell.categoryLabel.numberOfLines = 2;
            }
        }
        else{
            cell.categoryLabel.text = @"";
            cell.maskImageView.hidden = YES;
        }
        [cell.contentView layoutIfNeeded];
        cell.starImage.hidden = !item.isOffer;
        
        /*
         *  Display distance for Test
         */
        //        if(item.distance != CGFLOAT_MAX/* && [[[NSBundle mainBundle] bundleIdentifier] isEqualToString:@"ios.MasterCardConciergeUSDemoInternal"]*/)
        //            cell.addressLbl.text = [cell.addressLbl.text stringByAppendingFormat:@" (%.2f miles)",(item.distance/1609.344)];
        
        
        //        if(self.currentCategory.isCheckGeographicRegion) {
        //            TileItem* eo = (TileItem*)item;
        //            cell.nameLbl.text = eo.GeographicRegion;
        //            cell.addressLbl.text = eo.SubCategory;
        //        }
        
        [cell.nameLbl setLineSpacing:1.6];
        [cell.addressLbl setLineSpacing:1.6];
        [cell.offerLbl setLineSpacing:1.6];
        return cell;
    }
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BaseItem *item = [_exploreLst objectAtIndex:indexPath.row];
    selectedItem = indexPath.row;
    //    if([AppData isApplyNewFeatureWithKey:@"NewExploreVenueDetail"]) {
    ExploreVenueDetailViewController *vc = [[ExploreVenueDetailViewController alloc] initWithNibName:@"ExploreVenueDetailViewController" bundle:nil];
    vc.item = item;
    vc.cityName = self.currentCity.name;
    [self.navigationController pushViewController:vc animated:YES];
    if (shouldRefreshIndexSelectedCellAfterChangeCuisinePreference) {
        shouldRefreshIndexSelectedCellAfterChangeCuisinePreference = NO;
        [self.tableView reloadData];
    }
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(isStopRequestCauseNetworkUnvailable) {
        return;
    }
    
    if(currentWSTask == WS_B2C_SEARCH && self.wsSearch.hasNextItem)
    {
        if([self isLastRowVisible])
        {
            if(isLoading)
            {
                [self loadMoreIndicatorIcon:isLoading];
                return;
            }
            [self loadMoreDataForSearch];
            NSInteger lastRowIndex = [tableView numberOfRowsInSection:0] - 1;
            if ((indexPath.row == lastRowIndex))
            {
                [self loadMoreIndicatorIcon:isLoading];
            }
        }
        else
        {
            NSInteger lastRowIndex = [tableView numberOfRowsInSection:0] - 3;
            if ((indexPath.row == lastRowIndex)) {
                if(isLoading)
                {
                    [self loadMoreIndicatorIcon:isLoading];
                    return;
                }
                [self loadMoreDataForSearch];
            }
            
            lastRowIndex = [tableView numberOfRowsInSection:0] - 1;
            if ((indexPath.row == lastRowIndex))
            {
                [self loadMoreIndicatorIcon:isLoading];
            }
        }
        
    }
    else if(self.wsGetQuestion.hasNextItem)
    {
        NSInteger lastRowIndex = [tableView numberOfRowsInSection:0] - 3;
        if ((indexPath.row == lastRowIndex)) {
            if(isLoading)
            {
                return;
            }
            isLoading = YES;
            [self startCheckingLoading];
            self.wsGetQuestion.session = session;
            self.wsGetQuestion.pageIndex += 1;
            [self.wsGetQuestion loadQuestionsForCategory];
        }
        
        // Only show loading icon when last row is showing
        lastRowIndex = [tableView numberOfRowsInSection:0] - 1;
        if ((indexPath.row == lastRowIndex))
        {
            [self loadMoreIndicatorIcon:isLoading];
        }
    }
    
}


-(BOOL)isLastRowVisible {
    NSArray *indexes = [self.tableView indexPathsForVisibleRows];
    // ignore this case
    if(indexes.count < 3)
        return YES;
    
    for (NSIndexPath *index in indexes) {
        if (index.row == self.exploreLst.count - 1) {
            return YES;
        }
    }
    
    return NO;
}

-(void)loadMoreDataForSearch
{
    [self startCheckingLoading];
    
    isLoading = YES;
    self.wsSearch.session = session;
    self.wsSearch.pageIndex += 1;
    [self.wsSearch retrieveDataFromServer];
}


-(void)loadMoreIndicatorIcon:(BOOL)loading
{
    if(loading)
    {
        [self.tableView startLoadMoreIndicator];
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 30, 0);
        [UIView commitAnimations];;
    }
}


#pragma mark - DATALOADER DELEGATE
-(void)loadDataDoneFrom:(WSBase *)ws{
    
    if(ws.session != session)
    {
        return;
    }
    
    currentWSTask = ws.task;
    
    if(ws.task == WS_B2C_GET_TILES)
    {
        [self.exploreLst addObjectsFromArray:ws.data];
    }
    else if(ws.task == WS_B2C_GET_QUESTIONS)
    {
        NSArray *answerItems = [ws.data valueForKeyPath:@"answers"];
        for (NSArray *arr in answerItems )
        {
            [self addObjectToExploreListWithData:arr withWS:ws];
        }
    }
    else{
        if(currentWSTask == WS_B2C_SEARCH && ws.data.count == 0 && ws.pageIndex == 1)
        {
            self.tableHeaderView = [self.tableView.tableHeaderView viewWithTag:3001];
            if(!self.tableHeaderView)
            {
                self.tableHeaderView = [[[NSBundle mainBundle] loadNibNamed:@"TableHeaderView" owner:self options:nil] objectAtIndex:0];
                self.tableHeaderView.tag = 3001;
                
                self.tableHeaderView.frame = self.tableView.bounds;
                [self.tableHeaderView setupViewWithMessage: NSLocalizedString(@"Our apologies, but it appears that what you’ve searched for is not included in our curated content. Our concierge team is, however, available to help you. \n\nYou may reach the concierge team by clicking Ask the Concierge.", nil)];
                [self.tableHeaderView.askConciergeBtn addTarget:self action:@selector(askConciergeAction:) forControlEvents:UIControlEventTouchUpInside];
            }
            [self.tableView setTableHeaderView:self.tableHeaderView];
        }
        else{
            self.tableHeaderView = [self.tableView.tableHeaderView viewWithTag:3001];
            if(self.tableHeaderView)
            {
                [self.tableView setTableHeaderView:nil];
            }
            
            [self addObjectToExploreListWithData:ws.data withWS:ws];
        }
    }
}

-(void)addObjectToExploreListWithData:(NSArray *)arr withWS:(WSBase *)ws
{
    if(ws.session == session)
    {
        [self.exploreLst addObjectsFromArray:arr];
    }
}

-(void)loadDataFailFrom:(id<WSBaseProtocol>)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message
{
    
    if(![message isEqualToString:@"cancelled"])
    {
        id rootViewController = [UIApplication sharedApplication].delegate.window.rootViewController;
        if([rootViewController isKindOfClass:[UINavigationController class]])
        {
            rootViewController = ((UINavigationController *)rootViewController).viewControllers.firstObject;
        }
        
        // return then popup already present.
        if(((UIViewController*)rootViewController).presentedViewController) {
            return;
        }
        
        AlertViewController *alert = [[AlertViewController alloc] init];
        alert.titleAlert = NSLocalizedString(@"Cannot Get Data", nil);
        alert.msgAlert = NSLocalizedString(@"Turn on cellular data or use Wi-Fi to access Mobile Concierge.", nil);
        alert.firstBtnTitle = NSLocalizedString(@"SETTINGS", nil);
        alert.blockFirstBtnAction = ^(void){
            if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"prefs:root"]]) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root"]];
            } else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root"]];
            }
        };
        alert.secondBtnTitle = NSLocalizedString(@"OK", nil);
        
        alert.providesPresentationContextTransitionStyle = YES;
        alert.definesPresentationContext = YES;
        [alert setModalPresentationStyle:UIModalPresentationOverCurrentContext];
        
        [rootViewController presentViewController:alert animated:YES completion:nil];
    }
}


- (void) WSBaseNetworkUnavailable {
    isStopRequestCauseNetworkUnvailable = YES;
    [tmrCheckNetwork invalidate];
    tmrCheckNetwork = [NSTimer timerWithInterval:1 andBlock:^(NSTimer* tmr){
        isStopRequestCauseNetworkUnvailable = !isNetworkAvailable();
        printf("%s",[@"\nCHECK NETWORKING AVAILABLE\n" UTF8String]);
        if(!isStopRequestCauseNetworkUnvailable) {
           [tmrCheckNetwork invalidate];
            if(self.exploreLst.count == 0) {
                if(currentWSTask == WS_B2C_SEARCH) {
                    if(self.wsSearch) {
                        [self searchWithText:self.searchText withHasOffer:self.wsSearch.hasOffer];
                    } else {
                        [self searchWithText:self.searchText withHasOffer:NO];
                    }
                } else
                    [self loadDataForViewWithType:EXPLORE_NORMAL withCityguideCategoryName:nil];
            }
        } else {
            [self cancelAllRequest];
            [self stopLoading];
        }
    }];
}

- (void) stopLoading {
    
    if(self.exploreLst.count == 0) {
        self.tableHeaderView = [self.tableView.tableHeaderView viewWithTag:3001];
        if(!self.tableHeaderView)
        {
            self.tableHeaderView = [[[NSBundle mainBundle] loadNibNamed:@"TableHeaderView" owner:self options:nil] objectAtIndex:0];
            self.tableHeaderView.tag = 3001;
            
            self.tableHeaderView.frame = self.tableView.bounds;
            
            [self.tableHeaderView setupViewWithMessage: NSLocalizedString(@"Our apologies, but it appears that what you’ve searched for is not included in our curated content. Our concierge team is, however, available to help you. \n\nYou may reach the concierge team by clicking Ask the Concierge.", nil)];
            
            [self.tableHeaderView.askConciergeBtn addTarget:self action:@selector(askConciergeAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        [self.tableView setTableHeaderView:self.tableHeaderView];
    }
    
    [self resortData];
    [self stopActivityIndicatorWithoutMask];
    [self.tableView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    [self.tableView stopLoadMoreIndicator];
    isLoading = NO;
    [tmrCheckingLoading invalidate];
}

- (void) forceStopLoading {
    
    [self resortData];
    [self stopActivityIndicatorWithoutMask];
    [self.tableView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    [self.tableView stopLoadMoreIndicator];
    isLoading = NO;
    [tmrCheckingLoading invalidate];
}

#pragma mark - DELEGATE PROCESS
-(void) getDataBaseOnCity:(CityItem *)item
{
    
    NSString *build = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    if([build isEqualToString:@"9"])
        isSelectedAnotherCategory = YES;
    
    // back to this view
    [self.navigationController popToViewController:self animated:YES];
    
    NSString* cityguideCategoryName = @"";
    if(_wsGetQuestion) {
        cityguideCategoryName = _wsGetQuestion.categoryName;
        currentWSTask = -1;
    }
    
    [self cancelAllRequest];
    currentWSTask = -1;
    
    //new session
    selectedItem = -1;
    
    //reset table view
    [self.exploreLst removeAllObjects];
    [self.tableView reloadData];
    
    self.searchView.isSearchOffer = NO;
    self.searchView.isSearchBookOnline = NO;
    [self.searchView removeFromSuperview];
    self.searchViewOutletHeightConstraint.constant = 0.01f;
    [self.tableView setTableHeaderView:nil];
    [self.view layoutIfNeeded];
    self.currentCity = item;
    [self.cityBtn setTitle:item.name forState:UIControlStateNormal];
    [self.cityBtn centerVertically];
    
    //Save user city selection
    [AppData setCurrentCityWithCode:item.name];
    
    //Update UI
    [self updateUIBasedOnSelection:self.currentCategory];
    if(!self.searchBtn.enabled) {
        return;
    }
    
    //    if([self.currentCategory.supperCategoryID isEqualToString:@"80"]) {
    //        if(self.currentCity.subCategories.count == 0) {
    //            self.currentCategory = [[CategoryItem alloc] init];
    //            self.currentCategory.categoryName = @"Category";
    //            self.currentCategory.code = @"all";
    //            self.currentCategory.categoriesForService = LIST_ALL_TILES;
    //            [self updateUIBasedOnSelection:self.currentCategory];
    //        }
    //    }
    
    [self loadDataForViewWithType:EXPLORE_NORMAL withCityguideCategoryName:cityguideCategoryName];
}

-(void) Explore:(id)view onSelectCategory:(CategoryItem *)item
{
    // back to this view
    [self.navigationController popToViewController:self animated:YES];
    
    NSString *build = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    if(![build isEqualToString:@"9"])
        isSelectedAnotherCategory = YES;
    
    if ([item.supperCategoryID isEqualToString:@"80"]) {
        NSString *cityGuideString = NSLocalizedString(@"City Guide", nil);
        NSString *eventName = [NSString stringWithFormat:@"%@ - %@",cityGuideString, [item.categoryName capitalizedString]];
        [self trackingEventByName:eventName withAction:SelectActionType withCategory:CategorySelectionCategoryType];
    }
    
    if([item.code isEqualToString:@"dining"] && ![item.supperCategoryID isEqualToString:@"80"] && [[SessionData shareSessiondata] isUseLocation]) {
        [[SessionData shareSessiondata] setIsShownLocationServiceAlert:NO];
        [LocationComponent startRequestLocation];
    }
    
    [self cancelAllRequest];
    currentWSTask = -1;
    
    //New session
    session += 1;
    selectedItem = -1;
    
    //reset table view
    [self.exploreLst removeAllObjects];
    [self.tableView reloadData];
    
    self.searchView.isSearchOffer = NO;
    self.searchView.isSearchBookOnline = NO;
    [self.searchView removeFromSuperview];
    self.searchViewOutletHeightConstraint.constant = 0.01f;
    [self.tableView setTableHeaderView:nil];
    [self.view layoutIfNeeded];
    self.currentCategory = item;
    
    //Update UI
    [self updateUIBasedOnSelection:self.currentCategory];
    [self loadDataForViewWithType:EXPLORE_NORMAL withCityguideCategoryName:nil];
}

-(void)getDataBaseOnSearchText:(NSString *)text withOffer:(BOOL) isOffer withBookOnline:(BOOL)isBookOnline
{
    // back to this view
    [self.navigationController popToViewController:self animated:YES];
    
    [self cancelAllRequest];
    
    isTappedClearBtn = NO; //New searching is performed
    self.searchView = [self.tableView viewWithTag:3000];
    if(!self.searchView)
    {
        self.searchView = [[[NSBundle mainBundle] loadNibNamed:@"SearchHeaderView" owner:self options:nil] objectAtIndex:0];
        self.searchView.tag = 3000;
        [self.searchViewOutlet addSubview:self.searchView];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] init];
        [str appendAttributedString:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"Search results for ", nil) attributes:@{ NSFontAttributeName : [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]], NSForegroundColorAttributeName : [AppColor textColor]}]];
        [str appendAttributedString:[[NSAttributedString alloc] initWithString:text.length == 0 ? NSLocalizedString(@"offers", nil) : text attributes:@{NSForegroundColorAttributeName : [AppColor textColor], NSFontAttributeName :[UIFont fontWithName:FONT_MarkForMC_BOLD size:[AppSize titleTextSize]]}]];
        self.searchView.searchText.attributedText = str;
    });
    
    if(isOffer)
    {
        [self.searchView.offerClear addTarget:self action:@selector(searchWithoutOffers) forControlEvents:UIControlEventTouchUpInside];
    }
    
    
    self.searchView.searchText.delegate = self;
    
    self.searchView.isSearchOffer = isOffer;
    self.searchView.isSearchBookOnline = isBookOnline;
    self.searchText = text;
    [self.searchView setupView];
    heightForHeaderView = [self.searchView calculateHeightView];
    self.searchView.frame = CGRectMake(0.f, 0.f, SCREEN_WIDTH, heightForHeaderView);
    [self.searchView setBottomLine];
    
    self.searchViewOutletHeightConstraint.constant = heightForHeaderView;
    
    // !!!: load view with type SEARCH
    [self loadDataForViewWithType:EXPLORE_SEARCH withCityguideCategoryName:nil];
    
}

#pragma mark - PRIVATE
- (void) startCheckingLoading {
    isStopRequestCauseNetworkUnvailable = NO;
    [tmrCheckingLoading invalidate];
    typeof(self) __weak _self = self;
    tmrCheckingLoading = [NSTimer timerWithInterval:0.5 andBlock:^(NSTimer* tmr) {
        typeof(self) __self = _self;
        
        BOOL isWsSearchLoad = NO;
        BOOL isWsGetTiles = NO;
        BOOL isWsGetQuestion = NO;
        if(__self.wsSearch) {
            isWsSearchLoad = __self.wsSearch.isSyncing;
        }
        if(__self.wsGetTiles) {
            isWsGetTiles = __self.wsGetTiles.isSyncing;
        }
        if(__self.wsGetQuestion) {
            isWsGetQuestion = __self.wsGetQuestion.isSyncing;
        }
        /*
        printf("\n--------------------------\n⏳ Tiles:    %s\n⏳ Search:   %s\n⏳ Question: %s\n⏳ Distance: %s\n--------------------------\n\n",isWsGetTiles?[@"🔵" UTF8String]:[@"🔴" UTF8String],isWsSearchLoad?[@"🔵" UTF8String]:[@"🔴" UTF8String],isWsGetQuestion?[@"🔵" UTF8String]:[@"🔴" UTF8String],[LocationComponent isGettingDistance]?[@"🔵" UTF8String]:[@"🔴" UTF8String]);
         */
        if(!isWsGetTiles && !isWsSearchLoad && !isWsGetQuestion && ![LocationComponent isGettingDistance]) {
            {
                printf("%s", [[@"Stop overloading\n\n" uppercaseString] UTF8String]);
                [__self->tmrCheckingLoading invalidate];
                [__self stopLoading];
            }
        }
        
    }];
}

- (void) loadDataForViewWithType:(TYPE_LOAD_DATA_EXPLORE) type withCityguideCategoryName:(NSString*) categoryName{
    
    [tmrCheckingLoading invalidate];
    
    [self startCheckingLoading];
    
    // run loading
    [self startActivityIndicatorWithoutMask];
    
    // search
    if(type == EXPLORE_SEARCH) {
        [self searchWithText:self.searchText withHasOffer:self.searchView.isSearchOffer];
    }
    // not search
    else if(type == EXPLORE_NORMAL) {
        session++;
        
        if([self.currentCategory.supperCategoryID isEqualToString:@"80"] && self.currentCity.subCategories.count > 0)
        {
            //Update current category based on City Selection
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"code == %@", self.currentCategory.code];
            NSArray *newCategoryItem = [self.currentCity.subCategories filteredArrayUsingPredicate:predicate];
            if(newCategoryItem.count > 0)
            {
                
                [self startActivityIndicatorWithoutMask];
                self.currentCategory = [newCategoryItem objectAtIndex:0];
                self.wsGetQuestion = [[WSB2CGetQuestions alloc] init];
                self.wsGetQuestion.delegate = self;
                self.wsGetQuestion.task = WS_B2C_GET_QUESTIONS;
                self.wsGetQuestion.categoryCode = @"cityguide";
                self.wsGetQuestion.categoryName = categoryName?:self.currentCategory.categoryName;
                self.wsGetQuestion.currentCategory = self.currentCategory;
                self.wsGetQuestion.currentCity = self.currentCity;
                self.wsGetQuestion.session = session;
                self.wsGetQuestion.categoryId = self.currentCategory.ID;
                
                [self.wsGetQuestion loadQuestionsForCategory];
                return;
            }
        }
        else if(![self.currentCategory.supperCategoryID isEqualToString:@"80"] &&([self.currentCategory.code isEqualToString:@"dining"] && self.currentCity.diningID.length > 0))
        {
            self.wsGetQuestion = [[WSB2CGetQuestions alloc] init];
            self.wsGetQuestion.delegate = self;
            self.wsGetQuestion.task = WS_B2C_GET_QUESTIONS;
            self.wsGetQuestion.session = session;
            self.wsGetQuestion.currentCategory = self.currentCategory;
            self.wsGetQuestion.categoryCode = self.currentCategory.code;
            self.wsGetQuestion.categoryName = self.currentCategory.categoryName;
            self.wsGetQuestion.categoryId = self.currentCity.diningID;
            self.wsGetQuestion.currentCity = self.currentCity;
            
            [self.wsGetQuestion loadQuestionsForCategory];
            [self invokeGetTileAPIWithData:nil withSearchText:nil];
            return;
        } else
            //Integration Instance Answers API
            if([self.currentCategory.supperCategoryID isEqualToString:@"80"] || ([self.currentCategory.code isEqualToString:@"dining"] && self.currentCity.diningID.length > 0))
            {
                // Get Questions
                self.wsGetQuestion = [[WSB2CGetQuestions alloc] init];
                self.wsGetQuestion.delegate = self;
                self.wsGetQuestion.currentCategory = self.currentCategory;
                self.wsGetQuestion.task = WS_B2C_GET_QUESTIONS;
                self.wsGetQuestion.session = session;
                self.wsGetQuestion.currentCity = self.currentCity;
                self.wsGetQuestion.categoryId = self.currentCategory.ID;
                if([self.currentCategory.supperCategoryID isEqualToString:@"80"])
                {
                    self.wsGetQuestion.categoryCode = @"cityguide";
                    self.wsGetQuestion.categoryName = categoryName?:self.currentCategory.categoryName;
                }
                else if([self.currentCategory.code isEqualToString:@"dining"])
                {
                    self.wsGetQuestion.categoryCode = self.currentCategory.code;
                    self.wsGetQuestion.categoryName = categoryName?:self.currentCategory.categoryName;
                }
                
                [self.wsGetQuestion loadQuestionsForCategory];
                [self invokeGetTileAPIWithData:nil withSearchText:nil];
                return;
            }
        
            else if([self.currentCategory.code isEqualToString:@"all"])
            {
                
                [self getDataForAllCategory];
                return;
            }
        //Integration CCA API
            else if(self.currentCategory.categoriesForService)
            {
                
                [self invokeGetTileAPIWithData:nil withSearchText:nil];
                return;
            }
    }
}

-(void)getDataForAllCategory
{
    [self invokeGetTileAPIWithData:nil withSearchText:nil];
    
    if(self.currentCity.diningID.length > 0)
    {
        self.wsGetQuestion = [[WSB2CGetQuestions alloc] init];
        self.wsGetQuestion.delegate = self;
        self.wsGetQuestion.task = WS_B2C_GET_QUESTIONS;
        self.wsGetQuestion.session = session;
        self.wsGetQuestion.currentCity = self.currentCity;
        self.wsGetQuestion.currentCategory = self.currentCategory;
        self.wsGetQuestion.categoryId = self.currentCity.diningID;
        self.wsGetQuestion.categoryCode = @"dining";
        self.wsGetQuestion.categoryName = NSLocalizedString(@"DINING_category", nil) ;
        self.wsGetQuestion.pageSize = 20;
        self.wsGetQuestion.pageIndex = 1;
        [self.wsGetQuestion loadQuestionsForCategory];
    }
}

-(void)invokeGetTileAPIWithData:(NSArray *)categories withSearchText:(NSString *)searchText;
{

    self.wsGetTiles = [[WSB2CGetTiles alloc] init];
    self.wsGetTiles.delegate = self;
    self.wsGetTiles.task = searchText ? WS_B2C_SEARCH : WS_B2C_GET_TILES;
    self.wsGetTiles.session = session;
    self.wsGetTiles.categories = categories ? categories : self.currentCategory.categoriesForService;
    self.wsGetTiles.categoryCode = self.currentCategory.code;
    self.wsGetTiles.categoryName = self.currentCategory.categoryName;
    self.wsGetTiles.currentCategory = self.currentCategory;
    self.wsGetTiles.currentCity = self.currentCity;
    self.wsGetTiles.searchText = searchText;
    [self.wsGetTiles loadTilesForCategory];
}


-(void)updateUIBasedOnSelection:(CategoryItem*)item
{
    if([item.supperCategoryID isEqualToString:@"80"] )
    {
        self.currentTitle = item.categoryName;
        [self.searchBtn setTitle:NSLocalizedString(@"Discover\nMore", nil) forState:UIControlStateNormal];
        [self.searchBtn setImage:[self createDummyImage] forState:UIControlStateNormal];
        [self.searchBtn setImage:[self createDummyImage] forState:UIControlStateHighlighted];
//        [self.searchBtn configureDecorationBlackTitleButtonForTouchingStatus];
        [self.searchBtn centerVertically];
        [self.searchBtn removeTarget:self action:@selector(selectSearchAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.searchBtn addTarget:self action:@selector(navigateToCategoryCityGuide:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.categoryBtn setTitle:NSLocalizedString(@"City Guide",nil) forState:UIControlStateNormal];
        [self.categoryBtn centerVertically];
        
        if(self.currentCity.subCategories.count == 0) {
            self.searchBtn.enabled = NO;
            [self cancelAllRequest];
            [self stopLoading];
        } else {
            self.searchBtn.enabled = YES;
        }
    }
    
    else
    {
        self.currentTitle = NSLocalizedString(@"EXPLORE", nil);
        
        [self.searchBtn setImage:[UIImage imageNamed:@"search_white_icon"] forState:UIControlStateNormal];
        [self.searchBtn setImage:[UIImage imageNamed:@"search_interaction_icon"] forState:UIControlStateHighlighted];
        [self.searchBtn setTitle:NSLocalizedString(@"Search", nil) forState:UIControlStateNormal];
//        [self.searchBtn setTitleColor:self.searchBtn.titleLabel.textColor forState:UIControlStateHighlighted];
        [self.searchBtn centerVertically];
        
        [self.searchBtn removeTarget:self action:@selector(navigateToCategoryCityGuide:) forControlEvents:UIControlEventTouchUpInside];
        [self.searchBtn addTarget:self action:@selector(selectSearchAction:) forControlEvents:UIControlEventTouchUpInside];
        
        if([item.code isEqualToString:@"all"] && isSelectedAnotherCategory)
        {
            [self.categoryBtn setTitle:NSLocalizedString(@"All", nil) forState:UIControlStateNormal];
        } else
        if([item.code isEqualToString:@"all"] && !isSelectedAnotherCategory)
        {
            [self.categoryBtn setTitle:NSLocalizedString(@"Category", nil) forState:UIControlStateNormal];
        }
        else{
            NSString *newName = [self.currentCategory.categoryName stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            [self.categoryBtn setTitle:[newName capitalizedString] forState:UIControlStateNormal];
        }
        if(!self.searchBtn.enabled)
            self.searchBtn.enabled = YES;
        [self.categoryBtn centerVertically];
    }
}
- (UIImage *)createDummyImage
{
    UILabel *imageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 72.0, 21.0)];
    UIGraphicsBeginImageContextWithOptions(imageLabel.bounds.size, NO, 0);
    
    if ([imageLabel respondsToSelector:@selector(drawViewHierarchyInRect:afterScreenUpdates:)])
        [imageLabel drawViewHierarchyInRect:imageLabel.bounds afterScreenUpdates:YES];  // if we have efficient iOS 7 method, use it ...
    else
        [imageLabel.layer renderInContext:UIGraphicsGetCurrentContext()];         // ... otherwise, fall back to tried and true methods
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

-(void)searchWithText:(NSString *)text withHasOffer:(BOOL)isOffer
{
    //new session
    session += 1;
    
    //reset table view
    [self.exploreLst removeAllObjects];
    [self.tableView reloadData];
    
    //clear to prevent load more
    self.wsGetQuestion = nil;
    self.wsGetTiles = nil;
    self.wsSearch = nil;
    [self.tableView setTableHeaderView:nil];
    
    [self startActivityIndicator];
    if(![self.currentCategory.code isEqualToString:@"all"] && self.currentCategory.categoriesForService.count > 0 && ![self.currentCategory.code isEqualToString:@"dining"])
    {
        [self invokeGetTileAPIWithData:nil withSearchText:self.searchText];
    }
    else
    {
        self.wsSearch = [[WSB2CSearch alloc] init];
        self.wsSearch.session = session;
        self.wsSearch.delegate = self;
        self.wsSearch.task = WS_B2C_SEARCH;
        self.wsSearch.searchText = text;
        self.wsSearch.hasOffer = isOffer;
        self.wsSearch.cities = ![self.currentCity.cityCode isEqualToString:@"all"] ? @[(self.currentCity.encodeCityName.length > 0) ? self.currentCity.encodeCityName : self.currentCity.name] : nil;
        self.wsSearch.categoryCode = self.currentCategory.code;
        self.wsSearch.categoryName = self.currentCategory.categoryName;
        self.wsSearch.currentCity = self.currentCity;
        self.wsSearch.currentCategory = self.currentCategory;
        [self.wsSearch retrieveDataFromServer];
    }
}

- (void) cancelAllRequest {
    self.wsSearch.isSyncing = NO;
    self.wsGetQuestion.isSyncing = NO;
    self.wsGetTiles.isSyncing = NO;;
    self.wsGetQuestion.delegate = nil;
    self.wsGetTiles.delegate= nil;
    self.wsSearch.delegate = nil;
    self.wsGetQuestion = nil;
    self.wsGetTiles= nil;
    self.wsSearch = nil;
}

#pragma mark TEXT FIELD DELEGATE
-(void)searchWithoutOffers
{
    [self startCheckingLoading];
    if(self.searchText.length == 0){
        [self.wsSearch cancelRequest];
        self.searchText = @"";
        self.wsSearch = nil;
        [self.exploreLst removeAllObjects];
        [self.tableView reloadData];
        [self selectSearchAction:nil];
    }
    else{
        self.searchView.isSearchOffer = NO;
        [self.searchView setupView];
        heightForHeaderView = [self.searchView calculateHeightView];
        //self.searchView.frame = CGRectMake(0.f, 0.f, SCREEN_WIDTH, heightForHeaderView);
        [self.searchView setBottomLine];
        
        self.searchViewOutletHeightConstraint.constant = heightForHeaderView;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSMutableAttributedString *str = [[NSMutableAttributedString alloc] init];
            [str appendAttributedString:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"Search results for ", nil) attributes:@{ NSFontAttributeName : [UIFont fontWithName:FONT_MarkForMC_REGULAR size:[AppSize titleTextSize]], NSForegroundColorAttributeName : [AppColor textColor]}]];
            [str appendAttributedString:[[NSAttributedString alloc] initWithString:self.searchText.length == 0 ? NSLocalizedString(@"offers", nil) : self.searchText attributes:@{NSForegroundColorAttributeName : [AppColor textColor], NSFontAttributeName :[UIFont fontWithName:FONT_MarkForMC_BOLD size:[AppSize titleTextSize]]}]];
            self.searchView.searchText.attributedText = str;
        });
        [self searchWithText:self.searchText withHasOffer:NO];
    }
}
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    isTappedClearBtn = YES;
    [self.wsSearch cancelRequest];
    
    textField.text = @"";
    self.searchText = @"";
    [self.searchView removeFromSuperview];
    self.searchViewOutletHeightConstraint.constant = 0.01f;
    self.wsSearch = nil;
    [self.exploreLst removeAllObjects];
    [self.tableView reloadData];
    [self selectSearchAction:nil];
    
    return NO;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self selectSearchAction:nil];
    return NO;
}

#pragma mark - SCROLLVIEW DELEGATE
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.tableView)
    {
        if (isLoading) {
            return;
        }
        
        if (scrollView.contentOffset.y < 0) {
            if(scrollView.contentOffset.y < -70) {
                if(!menuLocation || menuLocation.hidden == NO) return;
                [vsStackView insertArrangedSubview:menuLocation atIndex:0];
                [UIView transitionWithView:menuLocation
                                  duration:0.2
                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                animations:^{
                                    menuLocation.hidden = NO;
                                }
                                completion:NULL];
            }
            /*
             if (scrollView.contentOffset.y < - (TABLE_PULL_HEIGHT + TABLE_PULL_LENGTH)) {
             [self.tableView setRefreshText:NSLocalizedString(@"TABLE_REFRESH_RELEASE_TEXT", @"") isDown:NO];
             } else {
             [self.tableView setRefreshText:NSLocalizedString(@"TABLE_REFRESH_PULL_TEXT", @"") isDown:YES];
             }
             */
        }
        else{
            if(menuLocation && menuLocation.hidden == NO && scrollView.contentOffset.y > 100) {
                [menuLocation hideKeyboard];
                
                [UIView transitionWithView:menuLocation
                                  duration:0.1
                                   options:UIViewAnimationOptionPreferredFramesPerSecond60
                                animations:^{
                                    
                                    //                                    menuLocation.hidden = YES;
                                    
                                }
                                completion:^(BOOL is){
                                    [vsStackView removeArrangedSubview:menuLocation];
                                    menuLocation.hidden = YES;
                                }];
            }
            
            if([self isLastRowVisible])
                return;
            
            [super scrollViewDidScroll:scrollView];
        }
    }
}

-(void)changePositionForView:(BOOL)isHide{
    /*
     if(isHide)
     {
     [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
     self.menuButtonsTopConstraint.constant = -100.f * SCREEN_SCALE;
     [self.view layoutIfNeeded];
     } completion:nil];
     }
     else{
     [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
     self.menuButtonsTopConstraint.constant = 0.0f;
     [self.view layoutIfNeeded];
     } completion:nil];
     }
     */
}

-(void)loadDataForScreen:(BOOL)isRefresh
{
    [self.tableView stopRefreshIndicator];
    [self.tableView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    isLoading = NO;
}

- (void)refreshContentForTableView
{
    isLoading = YES;
    [self.tableView startRefreshIndicator];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    self.tableView.contentInset = UIEdgeInsetsMake(30, 0, 0, 0);
    self.tableView.contentOffset = CGPointMake(0, 0 - self.tableView.contentInset.top);
    [UIView commitAnimations];
    
}

- (NSString*) replaceMutilSpaceForString:(NSString*)text withPatterns:(NSString*) pattern {
    NSString* temp = text;
    
    temp = [temp stringByReplacingOccurrencesOfString:pattern withString:@" " options:NSRegularExpressionSearch range:NSMakeRange(0,temp.length)];
    
    return temp;
}
@end
