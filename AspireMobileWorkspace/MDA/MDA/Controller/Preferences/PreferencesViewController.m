//
//  PreferencesViewController.m
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 7/24/17.
//  Copyright © 2017 Sunrixe Software Solutions Corporation. All rights reserved.
//

#import "PreferencesViewController.h"
#import "AppData.h"
#import "UtilStyle.h"
#import "UIButton+Extension.h"
#import "NSString+Utis.h"

#import "WSPreferences.h"
#import "PreferenceObject.h"
#import <AspireApiFramework/AspireApiFramework.h>


#define DEFAULT_STRING_PLACEHOLDER_DINING          NSLocalizedString(@"Please select your preferred cuisine.", nil)
#define DEFAULT_STRING_PLACEHOLDER_HOTEL           NSLocalizedString(@"Please select a rating.", nil)
#define DEFAULT_STRING_PLACEHOLDER_TRASPORTATION   NSLocalizedString(@"Please select a preferred vehicle type.", nil)


@interface PreferencesViewController () <DropDownViewDelegate, DataLoadDelegate> {
    NSMutableArray *listToAdd, *listToUpdate;
    BOOL isAddingPreference, isSubmited;
    __weak IBOutlet UILabel *transportLbl;
    __weak IBOutlet UILabel *hotelLbl;
    __weak IBOutlet UILabel *diningLbl;
}

@property (nonatomic, strong) WSPreferences *wsPreferences;

@end

@implementation PreferencesViewController

- (void)viewDidLoad {
    isNotAskConciergeBarButton = YES;
    [super viewDidLoad];
    [self setupPreferenceData];
    [self getPreference];
    [self createBackBarButton];
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"PREFERENCES", nil)];
    [self setupView];
    [self trackingScreenByName:@"Preferences"];
    diningLbl.text = NSLocalizedString(@"DINING", nil);
    hotelLbl.text = NSLocalizedString(@"HOTELS", nil);
    transportLbl.text = NSLocalizedString(@"TRANSPORTATION_preferences", nil);
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Process Data
- (void)setupPreferenceData {
    isSubmited = NO;
    isAddingPreference = NO;
    [self setPreferenceData];
}

- (void)setPreferenceData{
    UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
    
    self.diningDropDownView.titleColor = self.hotelDropDownView.titleColor = self.transportationDropDownView.titleColor = [AppColor placeholderTextColor];
    
    self.diningDropDownView.title = DEFAULT_STRING_PLACEHOLDER_DINING;
    self.hotelDropDownView.title = DEFAULT_STRING_PLACEHOLDER_HOTEL;
    self.transportationDropDownView.title = DEFAULT_STRING_PLACEHOLDER_TRASPORTATION;
    
    if (userObject.arrayPreferences.count > 0) {
        for (PreferenceObject *preferece in userObject.arrayPreferences) {
            switch (preferece.type) {
                case DiningPreferenceType:
                {
                    if (preferece.value.length > 0 && ![preferece.value isEqualToString:@"na"]) {
                        self.diningDropDownView.title = preferece.value;
                        self.diningDropDownView.titleColor = [AppColor textColor];
                        //[self.diningDropDownView layoutSubviews];
                    }
                }
                    break;
                case HotelPreferenceType:
                {
                    if (preferece.value.length > 0 && ![preferece.value isEqualToString:@"na"]) {
                        self.hotelDropDownView.title = preferece.value;
                        self.hotelDropDownView.titleColor = [AppColor textColor];
                        //[self.hotelDropDownView layoutSubviews];
                    }
                }
                    break;
                case TransportationPreferenceType:
                {
                    if (preferece.value.length > 0 && ![preferece.value isEqualToString:@"na"]) {
                        self.transportationDropDownView.title = preferece.value;
                        self.transportationDropDownView.titleColor = [AppColor textColor];
                        //[self.transportationDropDownView layoutSubviews];
                    }
                }
                    break;
                default:
                    break;
            }
        }
    }
    self.diningDropDownView.valueStatus = DefaultValueStatus;
    self.hotelDropDownView.valueStatus = DefaultValueStatus;
    self.transportationDropDownView.valueStatus = DefaultValueStatus;
}

#pragma mark - Setup
- (void)setupView {
    
    self.line1View.backgroundColor = [AppColor separatingLineColor];
    self.line2View.backgroundColor = [AppColor separatingLineColor];
    
    self.diningDropDownView.listItems = [[AppData getPreferencesItemsLocalizable] objectForKey:@"DINING"];
    self.diningDropDownView.listItemsValue = [[AppData getPreferencesItems] objectForKey:@"DINING"];
    self.diningDropDownView.delegate = self;
    
    self.hotelDropDownView.listItems = [[AppData getPreferencesItemsLocalizable] objectForKey:@"HOTEL"];
    self.hotelDropDownView.listItemsValue = [[AppData getPreferencesItems] objectForKey:@"HOTEL"];
    self.hotelDropDownView.delegate = self;
    
    self.transportationDropDownView.listItems = [[AppData getPreferencesItemsLocalizable] objectForKey:@"TRANSPORTATION"];
    self.transportationDropDownView.listItemsValue = [[AppData getPreferencesItems] objectForKey:@"TRANSPORTATION"];
    self.transportationDropDownView.delegate = self;
    
    [self.updateButton setBackgroundColorForNormalStatus];
    [self.updateButton setBackgroundColorForTouchingStatus];
    [self.updateButton configureDecorationForButton];
    
    [self.cancelButton setBackgroundColorForNormalStatus];
    [self.cancelButton setBackgroundColorForTouchingStatus];
    [self.cancelButton configureDecorationForButton];
    
    [self.updateButton setTitle:NSLocalizedString(@"UPDATE", nil) forState:UIControlStateNormal];
    [self.cancelButton setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize titleTextSize]],
                                 NSKernAttributeName: @1.4};
    self.updateButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.updateButton.titleLabel.text attributes:attributes];
    self.cancelButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.cancelButton.titleLabel.text attributes:attributes];
    
    [self setButtonStatus:NO];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.topLayoutConstraintDiningView.constant = (IPAD) ? 10.f : 30.0f;
    });
}

- (void)setButtonStatus:(BOOL)status {
    self.updateButton.enabled = status;
    self.cancelButton.enabled = status;
}

- (BOOL)isChangeData{
    NSMutableArray *arrayPreferences = [[SessionData shareSessiondata].arrayPreferences copy];
    if (arrayPreferences > 0) {
        for (PreferenceObject *preference in arrayPreferences) {
            switch (preference.type) {
                case DiningPreferenceType:
                {
                    NSString *valueTitle = ([self.diningDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_DINING]) ? @"" : self.diningDropDownView.title;
                    if ([valueTitle.uppercaseString isEqualToString:preference.value.uppercaseString]) {
                        self.diningDropDownView.valueStatus = DefaultValueStatus;
                    }else{
                        self.diningDropDownView.valueStatus = UpdateValueStatus;
                    }
                }
                    break;
                case HotelPreferenceType:
                {
                    NSString *valueTitle = ([self.hotelDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_HOTEL]) ? @"" : self.hotelDropDownView.title;
                    if ([valueTitle.uppercaseString isEqualToString:preference.value.uppercaseString]) {
                        self.hotelDropDownView.valueStatus = DefaultValueStatus;
                    }else{
                        self.hotelDropDownView.valueStatus = UpdateValueStatus;
                    }
                }
                    break;
                case TransportationPreferenceType:
                {
                    NSString *valueTitle = ([self.transportationDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_TRASPORTATION]) ? @"" : self.transportationDropDownView.title;
                    if ([valueTitle.uppercaseString isEqualToString:preference.value.uppercaseString]) {
                        self.transportationDropDownView.valueStatus = DefaultValueStatus;
                    }else{
                        self.transportationDropDownView.valueStatus = UpdateValueStatus;
                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
    }
    return (self.diningDropDownView.valueStatus != DefaultValueStatus || self.hotelDropDownView.valueStatus != DefaultValueStatus || self.transportationDropDownView.valueStatus != DefaultValueStatus);
}

#pragma mark - DropDownView Delegate
- (void)didSelectItem:(DropDownView *)dropDownView atIndex:(int)index{
    
    NSString *valueSelected = [dropDownView.listItems objectAtIndex:index];
    if ([valueSelected.uppercaseString isEqualToString:NSLocalizedString(@"None", nil).uppercaseString]) {
        
        if (dropDownView == self.diningDropDownView) {
            self.diningDropDownView.titleColor = [AppColor placeholderTextColor];
            self.diningDropDownView.title = DEFAULT_STRING_PLACEHOLDER_DINING;
        }else if (dropDownView == self.hotelDropDownView) {
            self.hotelDropDownView.titleColor = [AppColor placeholderTextColor];
            self.hotelDropDownView.title = DEFAULT_STRING_PLACEHOLDER_HOTEL;
        }else if (dropDownView == self.transportationDropDownView) {
            self.transportationDropDownView.titleColor = [AppColor placeholderTextColor];
            self.transportationDropDownView.title = DEFAULT_STRING_PLACEHOLDER_TRASPORTATION;
        }
    }
    [self setButtonStatus:[self isChangeData]];
}

#pragma mark - Actions Button
- (IBAction)UpdateAction:(id)sender {
    
    
    if(!isNetworkAvailable()) {
        [self showErrorNetwork];
        
    }else{
        [self setButtonStatus:NO];
        [self startActivityIndicator];
        isSubmited = YES;
        
        [self addPreference];
    }
    /*
    if(!isNetworkAvailable()) {
        [self showErrorNetwork];
        
    }else{
        [self setButtonStatus:NO];
        [self startActivityIndicator];
        isSubmited = YES;
        listToAdd = [[NSMutableArray alloc] init];
        listToUpdate = [[NSMutableArray alloc] init];
        
        for (UIView *item in self.view.subviews) {
            if ([item isKindOfClass:[DropDownView class]]) {
                if (((DropDownView*)item).valueStatus == NewValueStatus) {
                    [listToAdd addObject:item];
                }else if(((DropDownView*)item).valueStatus == UpdateValueStatus){
                    [listToUpdate addObject:item];
                }
            }
        }
        if (listToAdd.count > 0 && listToUpdate.count > 0) {
            isAddingPreference = YES;
            [self addPreference];
        }else{
            if (listToAdd.count > 0) {
                [self addPreference];
            }else if (listToUpdate.count > 0){
//                [self updatePreference];
            }
        }
    }
     */
}

- (IBAction)CancelAction:(id)sender {
    [self setPreferenceData];
    [self setButtonStatus:NO];
}
#pragma mark - Private Methods
- (void)addPreference{
    __weak typeof(self) _self = self;
    if([User isValid])
    {
        UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
        NSMutableDictionary *dictDai = [[NSMutableDictionary alloc] init];
        [dictDai setValue:userObject.salutation forKey:@"Salutation"];
        [dictDai setValue:userObject.firstName forKey:@"FirstName"];
        [dictDai setValue:userObject.lastName forKey:@"LastName"];
        [dictDai setValue:userObject.mobileNumber forKey:@"MobileNumber"];
        [dictDai setValue:userObject.email forKey:@"Email"];
        [dictDai setValue:[[SessionData shareSessiondata] passcode] forKey:APP_USER_PREFERENCE_Passcode];
        [dictDai setValue:userObject.isUseLocation ? @"Yes" : @"No" forKey:APP_USER_PREFERENCE_Location];
        [dictDai setValue:([self.diningDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_DINING]) ?@"na":self.diningDropDownView.title forKey:@"Dining Preference"];
        [dictDai setValue:([self.hotelDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_HOTEL])?@"na":self.hotelDropDownView.title forKey:@"Hotel Preference"];
        [dictDai setValue:([self.transportationDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_TRASPORTATION])?@"na":self.transportationDropDownView.title forKey:@"Car Type Preference"];
        [dictDai setValue: !userObject.currentCity || [userObject.currentCity isEqualToString:@""] ? @"Unknown" : userObject.currentCity forKey:@"City"];
        [dictDai setValue: !userObject.currentCity || [userObject.currentCity isEqualToString:@""] ? @"Unknown" : userObject.currentCity forKey: APP_USER_PREFERENCE_Selected_City];
        [dictDai setValue:@"USA" forKey:@"Country"];
        [dictDai setValue:APP_NAME forKey:@"referenceName"];
        
        [ModelAspireApiManager updateProfileCurrentUserWithUserInfo:dictDai completion:^(NSError *error) {
            
            [_self stopActivityIndicator];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (!error) {
                    
                    if (isSubmited) {
                        isSubmited = NO;
                        [_self showAlertUpdatedPreferences];
                    }
                }else{
                    switch ([AspireApiError getErrorTypeAPIUpdateProfileFromError:error]) {
                        case UNKNOWN_ERR:
                            // show general message for update profile
                            [_self showApiErrorWithMessage:@""];
                            NSLog(@"[RESULT] => LOGIN FAIL: %@",@"UNKNOWN_ERR");
                            break;
                        case ACCESSTOKEN_INVALID_ERR:
                            // show message invalid accesstoken
                            [_self handleInvalidCredentials];
                            NSLog(@"[RESULT] => LOGIN FAIL: %@",@"ACCESSTOKEN_INVALID_ERR");
                            // login again
                            break;
                        default:
                            [_self showApiErrorWithMessage:@""];
                            NSLog(@"[RESULT] => LOGIN FAIL: %@",@"OUT OF ENUM AspireApiErrorType");
                            break;
                    }
                }
            });
            
        }];
    }
    
    /*
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    NSMutableArray *arraySubDictValues = [[NSMutableArray alloc] init];
    for (DropDownView *item in listToAdd) {
        if (item == self.diningDropDownView) {
            NSMutableDictionary *diningDict = [[NSMutableDictionary alloc] init];
            [diningDict setValue:@"Dining" forKey:@"Type"];
            [diningDict setValue:([self.diningDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_DINING])?@"":self.diningDropDownView.title forKey:@"CuisinePreferences"];
            [arraySubDictValues addObject:diningDict];
            
        }else if (item == self.hotelDropDownView) {
            NSMutableDictionary *hotelDict = [[NSMutableDictionary alloc] init];
            [hotelDict setValue:@"Hotel" forKey:@"Type"];
            [hotelDict setValue:([self.hotelDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_HOTEL])?@"":self.hotelDropDownView.title forKey:@"Preferredstarrating"];
            [arraySubDictValues addObject:hotelDict];
        }else if (item == self.transportationDropDownView) {
            NSMutableDictionary *transportationDict = [[NSMutableDictionary alloc] init];
            [transportationDict setValue:@"Car Rental" forKey:@"Type"];
            [transportationDict setValue:([self.transportationDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_TRASPORTATION])?@"":self.transportationDropDownView.title forKey:@"PreferredRentalVehicle"];
            [arraySubDictValues addObject:transportationDict];
        }
    }
    
    [dataDict setValue:arraySubDictValues forKey:@"PreferenceDetails"];
    self.wsPreferences = [[WSPreferences alloc] init];
    self.wsPreferences.delegate = self;
    [self.wsPreferences addPreference:dataDict];
    */
}

//- (void)updatePreference{
//    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
//    NSMutableArray *arraySubDictValues = [[NSMutableArray alloc] init];
//    for (DropDownView *item in listToUpdate) {
//        if (item == self.diningDropDownView) {
//            NSMutableDictionary *diningDict = [[NSMutableDictionary alloc] init];
//            [diningDict setValue:[self getPreferenceIDByType:DiningPreferenceType] forKey:@"MyPreferencesId"];
//            [diningDict setValue:@"Dining" forKey:@"Type"];
//            [diningDict setValue:([self.diningDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_DINING])?@"":self.diningDropDownView.title forKey:@"CuisinePreferences"];
//            [arraySubDictValues addObject:diningDict];
//
//        }else if (item == self.hotelDropDownView) {
//            NSMutableDictionary *hotelDict = [[NSMutableDictionary alloc] init];
//            [hotelDict setValue:[self getPreferenceIDByType:HotelPreferenceType] forKey:@"MyPreferencesId"];
//            [hotelDict setValue:@"Hotel" forKey:@"Type"];
//            [hotelDict setValue:([self.hotelDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_HOTEL])?@"":self.hotelDropDownView.title forKey:@"Preferredstarrating"];
//            [arraySubDictValues addObject:hotelDict];
//        }else if (item == self.transportationDropDownView) {
//            NSMutableDictionary *transportationDict = [[NSMutableDictionary alloc] init];
//            [transportationDict setValue:[self getPreferenceIDByType:TransportationPreferenceType] forKey:@"MyPreferencesId"];
//            [transportationDict setValue:@"Car Rental" forKey:@"Type"];
//            [transportationDict setValue:([self.transportationDropDownView.title isEqualToString:DEFAULT_STRING_PLACEHOLDER_TRASPORTATION])?@"":self.transportationDropDownView.title forKey:@"PreferredRentalVehicle"];
//            [arraySubDictValues addObject:transportationDict];
//        }
//    }
//    [dataDict setValue:arraySubDictValues forKey:@"PreferenceDetails"];
//
//    self.wsPreferences = [[WSPreferences alloc] init];
//    self.wsPreferences.delegate = self;
//    [self.wsPreferences updatePreference:dataDict];
//}

- (void)getPreference{
    [self startActivityIndicator];
    __weak typeof(self) _self = self;
    [ModelAspireApiManager retrieveProfileCurrentUser:^(User *user, NSError *error) {
        [_self stopActivityIndicator];
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error) {
                
                UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
                userObject.salutation = [userObject.salutation converSalutationFormAPI];
                [[SessionData shareSessiondata] setUserObject:userObject];
                [_self setPreferenceData];
            }else{
                switch ([AspireApiError getErrorTypeAPIRetrieveProfileFromError:error]) {
                    case UNKNOWN_ERR:
                        // show general message for update profile
                        [_self showApiErrorWithMessage:@""];
                        NSLog(@"[RESULT] => LOGIN FAIL: %@",@"UNKNOWN_ERR");
                        break;
                    case ACCESSTOKEN_INVALID_ERR:
                        [_self handleInvalidCredentials];
                        // show message invalid accesstoken
                        NSLog(@"[RESULT] => LOGIN FAIL: %@",@"ACCESSTOKEN_INVALID_ERR");
                        // login again
                        break;
                    default:
                        [_self showApiErrorWithMessage:@""];
                        NSLog(@"[RESULT] => LOGIN FAIL: %@",@"OUT OF ENUM AspireApiErrorType");
                        break;
                }
            }
        });
    }];
    /*
    if(!isNetworkAvailable()) {
        [self showErrorNetwork];
    }else{
        [self startActivityIndicator];
        self.wsPreferences = [[WSPreferences alloc] init];
        self.wsPreferences.delegate = self;
        [self.wsPreferences getPreference];
    }
     */
    
}
-(NSString*)getPreferenceIDByType:(PreferenceType)type{
    for (PreferenceObject *preference in [SessionData shareSessiondata].arrayPreferences) {
        if (preference.type == type) {
            return preference.preferenceID;
        }
    }
    return @"";
}

- (void)showAlertUpdatedPreferences {
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert =nil;
    alert.msgAlert = NSLocalizedString(@"You have successfully updated your preference.", nil);
    alert.firstBtnTitle = NSLocalizedString(@"OK", nil);
    dispatch_async(dispatch_get_main_queue(), ^{
        alert.seconBtn.hidden = YES;
        alert.midView.alpha = 0.0f;;
        [alert.view layoutIfNeeded];
    });
    alert.blockFirstBtnAction = ^(void){
    };
    [self showAlert:alert forNavigation:NO];
}

#pragma mark - Delegate from API
-(void)loadDataDoneFrom:(WSBase*)ws{
    if (ws.task == WS_GET_MY_PREFERENCE) {
        [self setPreferenceData];
        [self stopActivityIndicator];
        if (isSubmited) {
            isSubmited = NO;
            [self showAlertUpdatedPreferences];
        }
    }else if (ws.task == WS_ADD_MY_PREFERENCE){
        if (isAddingPreference) {
//            [self updatePreference];
            isAddingPreference = NO;
        }else{
            [self getPreference];
        }
    }else if (ws.task == WS_UPDATE_MY_PREFERENCE){
        [self getPreference];
    }
}


-(void)loadDataFailFrom:(WSBase *)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message {
    
    [self stopActivityIndicator];
    if(errorCode == 1005 || errorCode == -1009){
        [self showErrorNetwork];
        [self setButtonStatus:YES];
    }else{
        [self showAPIErrorAlertWithMessage:NSLocalizedString(@"An error has occurred. Check your internet settings or try again.", nil) andTitle:NSLocalizedString(@"Cannot Get Data", nil)];
        [self setButtonStatus:YES];
    }
}

@end
