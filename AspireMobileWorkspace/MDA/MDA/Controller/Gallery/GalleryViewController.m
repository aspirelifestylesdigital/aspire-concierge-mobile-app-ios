//
//  GalleryViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/8/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "GalleryViewController.h"
#import "GalleryDetailViewController.h"
#import "Constant.h"
#import "GalleryItem.h"
#import "UIView+Extension.h"
#import "Common.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "WSB2CGetTiles.h"
#import "TileItem.h"

@interface GalleryViewController ()<ChangingBackgroundColorDelegate, DataLoadDelegate>
{
    UIColor *currentPageBackgroundColor;
    WSB2CGetTiles *wsGetTiles;
}
@property (nonatomic, strong) IBOutlet UIPageControl *manualAddPageControl;
@end

@implementation GalleryViewController

- (void)viewDidLoad {
    self.pageControl.hidden = YES;
    isNotChangeNavigationBarColor = YES;
    [super viewDidLoad];
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"CONCIERGE", nil)];
    [self createMenuBarButton];
    
    self.manualAddPageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - 30.0f, SCREEN_WIDTH, 37.0f)];
    [self.manualAddPageControl addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventValueChanged];
    
    [self trackingScreenByName:@"Gallery"];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setTranslucent:YES];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setTranslucent:NO];
}

-(void)initData
{
    
    [self startActivityIndicator];
    wsGetTiles = [[WSB2CGetTiles alloc] init];
    wsGetTiles.delegate = self;
    wsGetTiles.categoryCode = @"appgallery";
    wsGetTiles.categories = [[NSArray alloc] initWithObjects:@"App Gallery", nil];
    [wsGetTiles loadTilesForCategory];
}

-(void)loadDataDoneFrom:(WSB2CGetTiles *)ws
{
    [self stopActivityIndicator];
    [self.view layoutIfNeeded];
    
    if (ws.data.count > 0) {
        self.pageControl.hidden = NO;
        self.galleryList = [ws.data subarrayWithRange:NSMakeRange(0, (ws.data.count > 15) ? 15 : ws.data.count)];
        
        [self.manualAddPageControl setNumberOfPages:self.galleryList.count];
        [self.manualAddPageControl setCurrentPage:0];
        
        self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
        self.pageViewController.delegate = self;
        self.pageViewController.dataSource = self;
        self.pageViewController.view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        GalleryDetailViewController *galleryDetail = [self viewControllerAtIndex:0];
        NSArray *detailViewControllers = [NSArray arrayWithObject:galleryDetail];
        
        [self.pageViewController setViewControllers:detailViewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        
        [self addChildViewController:self.pageViewController];
        [self.view addSubview:self.pageViewController.view];
        [self.pageViewController.view addSubview:self.manualAddPageControl];
        [self.pageViewController didMoveToParentViewController:self];
    }
    
}

-(void)setColorForPage:(NSInteger)currentPageIndex
{
    TileItem *item = [self.galleryList objectAtIndex:currentPageIndex];
    [self.manualAddPageControl setCurrentPage:currentPageIndex];
    currentPageBackgroundColor = colorFromHexString(item.shortDescription);
    //[self setNavigationBarWithColor:currentPageBackgroundColor];
    [self.view setBackgroundColor:currentPageBackgroundColor];
}

-(void)getMenuView:(UIPanGestureRecognizer *)panGesture
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.currentCollorForMenu = currentPageBackgroundColor;
    SWRevealViewController *revealViewController = [self revealViewController];
    [revealViewController _handleRevealGesture:panGesture];
}

- (void)showMenu{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.currentCollorForMenu = currentPageBackgroundColor;
    
    SWRevealViewController *revealViewController = [self revealViewController];
    [revealViewController tapGestureRecognizer];
    [revealViewController panGestureRecognizer];
    
    [revealViewController revealToggle:self];
}


-(void)updatePageControlPosition:(CGFloat)yPos
{
    CGRect pageControlFrame = CGRectMake(0.0f, (yPos + 37.0f/2.0f), SCREEN_WIDTH, 37.0f);
    self.manualAddPageControl.frame = pageControlFrame;
    [self.view setNeedsUpdateConstraints];
    [self.view layoutIfNeeded];
}

-(void)setNavigationBarWithColor:(UIColor *)color
{
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBarTintColor:color];
    [self.navigationController.navigationBar setTranslucent:YES];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName:[UIFont systemFontOfSize:[AppSize titleTextSize]]};
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(GalleryDetailViewController *)viewController index];
    
    if (index == 0) {
        return nil;
    }
    
    index--;
    
    return [self viewControllerAtIndex:index];
}

- (GalleryDetailViewController *)viewControllerAtIndex:(NSUInteger)index {
    GalleryDetailViewController *childViewController = [[GalleryDetailViewController alloc] initWithNibName:@"GalleryDetailViewController" bundle:nil];
    childViewController.index = index;
    TileItem *item = [self.galleryList objectAtIndex:index];
    childViewController.titleText = item.title;
    childViewController.descriptionText = item.tileText;
    childViewController.imageURL = item.imageURL;
    childViewController.colorHex = item.shortDescription;

    childViewController.delegate = self;
    return childViewController;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(GalleryDetailViewController *)viewController index];
    
    index++;
    
    if (index == self.galleryList.count) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)changePage:(UIPageControl *)sender
{
    NSInteger index = sender.currentPage;
    GalleryDetailViewController *galleryDetail = [self viewControllerAtIndex:index];
    NSArray *detailViewControllers = [NSArray arrayWithObject:galleryDetail];
    
    [self.pageViewController setViewControllers:detailViewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}

- (void)WSBaseNetworkUnavailable{
    self.pageControl.hidden = YES;
    [self stopActivityIndicator];
}
@end
