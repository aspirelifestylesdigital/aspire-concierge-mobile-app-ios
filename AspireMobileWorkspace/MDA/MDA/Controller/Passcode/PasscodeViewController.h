//
//  PasscodeViewController.h
//  MobileConciergeUSDemo
//
//  Created by Chung Mai on 10/23/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "PasswordTextField.h"
@interface PasscodeViewController : BaseViewController

@property(weak, nonatomic) IBOutlet UILabel *titleLbl;
@property(weak, nonatomic) IBOutlet PasswordTextField *passcodeTxtView;
@property(weak, nonatomic) IBOutlet UIButton *submitBtn;
@property(weak, nonatomic) IBOutlet UILabel *headerLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *submitBtnBottomConstraint;


-(IBAction)submitPasscode:(id)sender;
-(IBAction)setValidPasscode:(id)sender;

@end
