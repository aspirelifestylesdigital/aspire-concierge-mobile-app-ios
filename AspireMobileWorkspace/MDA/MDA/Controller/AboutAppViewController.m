//
//  AboutAppViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 5/16/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "AboutAppViewController.h"
#import "WSB2CBase.h"
#import "WSB2CGetAboutInfo.h"
#import "AboutInfoItem.h"

@interface AboutAppViewController ()<DataLoadDelegate, UIScrollViewDelegate, UIWebViewDelegate>
{
    NSString *content;
    UIBarButtonItem *backButtonItem;
    WSB2CGetAboutInfo *wsGetAboutInfo;
}


@end

@implementation AboutAppViewController

- (void)viewDidLoad {
    isNotAskConciergeBarButton = YES;
    [super viewDidLoad];
    [self createBackBarButtonWithIconName:@"clear_icon"];
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"ABOUT THIS APP", nil)];
    
    self.webView.delegate = self;
    self.webView.scrollView.delegate = self;
    [self.webView setBackgroundColor:[UIColor clearColor]];
    [self.webView setOpaque:NO];
    [self trackingScreenByName:@"About this app"];
}

- (void)handlePopRecognizer:(UIPanGestureRecognizer *)recognizer{
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //  Set up Pop pan gesture
    [self setUpCustomizedPanGesturePopRecognizer];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)initData
{
    [self startActivityIndicator];
    wsGetAboutInfo = [[WSB2CGetAboutInfo alloc] init];
    wsGetAboutInfo.delegate = self;
    [wsGetAboutInfo retrieveDataFromServer];
}


#pragma mark DATALOADDELEGATE


-(void)loadDataDoneFrom:(WSB2CGetAboutInfo *)wsAboutInfo
{
    if(wsAboutInfo.data.count > 0)
    {
        content = [self replaceWebLinkInString:((AboutInfoItem *)[wsAboutInfo.data objectAtIndex:0]).textInfo];
        NSRange bodyHtmlRange = [content rangeOfString:@"font-family"];
        if (bodyHtmlRange.location == NSNotFound) {
            [self.webView loadHTMLString:centerHTMLString(content, FONT_MarkForMC_REGULAR, (16.0 * SCREEN_SCALE), @"#FFFFFF") baseURL:nil];
        }else{
            [self.webView loadHTMLString:content baseURL:nil];
        }
    }
    else{
       [self stopActivityIndicator];
    }
}

- (NSString*) replaceWebLinkInString:(NSString*)string{
    
    NSArray *tempArr = getMatchesFromString(string,@"href+=+\"(?!#+)(.*?)+\"",NSRegularExpressionCaseInsensitive);
    
    NSMutableArray* vvv = [NSMutableArray new];
    for (NSString* test in tempArr) {
        NSString* t = strimStringFrom(test,@[@"href=|\"?"]);
        if(![t containsString:@"http"]) {
            [vvv addObject:@{test:[[@"href=\"http://" stringByAppendingString:t] stringByAppendingString:@"\""]}];
        }
    }
    NSMutableString* tttt = [NSMutableString stringWithString:string];
    for (NSDictionary* t in vvv) {
        if([string containsString:[[t allKeys] firstObject]]) {
            string = [tttt stringByReplacingOccurrencesOfString:[[t allKeys] firstObject] withString:[t objectForKey:[[t allKeys] firstObject]]];
        }
    }
    return string;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self stopActivityIndicator];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
#ifdef DEBUG
    NSLog(@"%@", request.URL);
#endif
    if (navigationType==UIWebViewNavigationTypeLinkClicked) {
        if ([request.URL.absoluteString containsString:@"http"]) {
            
            AlertViewController *alert = [[AlertViewController alloc] init];
            alert.titleAlert = NSLocalizedString(@"ALERT!", nil);
            alert.msgAlert = NSLocalizedString(@"You are now leaving Mobile Concierge. Do you wish to continue?", nil);
            alert.firstBtnTitle = NSLocalizedString(@"YES", nil);
            alert.blockFirstBtnAction = ^(void){
                [[UIApplication sharedApplication] openURL:request.URL];
            };
            
            alert.blockSecondBtnAction  = ^(void){};
            [self showAlert:alert forNavigation:YES];
            // It was a link
            
            return NO;
        }
    }
    
    return YES;
}
- (void)WSBaseNetworkUnavailable{
    [self stopActivityIndicator];
}
@end
