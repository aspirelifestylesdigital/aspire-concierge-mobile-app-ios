//
//  PolicyViewController.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/15/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "PolicyViewController.h"
#import "WSB2CGetPolicyInfo.h"
#import "PolicyInfoItem.h"
#import "UIView+Extension.h"

@interface PolicyViewController ()<UIWebViewDelegate, UIScrollViewDelegate>
@end

@implementation PolicyViewController

- (void)viewDidLoad {
    isNotAskConciergeBarButton = YES;
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    [self createMenuBarButton];
    [self createBackBarButtonWithIconName:@"clear_icon"];
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"PRIVACY POLICY", nil)];
    backButtonItem = self.navigationItem.leftBarButtonItem;
    backButtonItem.enabled = YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //  Set up Pop pan gesture
    [self setUpCustomizedPanGesturePopRecognizer];
}


- (void)handlePopRecognizer:(UIPanGestureRecognizer *)recognizer{
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) didScrollToEnd{
    backButtonItem.enabled = YES;
}

@end
