//
//  ExploreDetailPriceView.m
//  MobileConciergeUSDemo
//
//  Created by Nghia Dinh on 7/20/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "ExploreDetailPriceView.h"
#import "AnswerItem.h"
#import "Common.h"
#import "Constant.h"
#import "UtilStyle.h"

@interface ExploreDetailPriceView()

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *cuisineLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trailingConstraint;
@property (weak, nonatomic) IBOutlet UILabel *priceTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *cuisineTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *mainView;

@end

@implementation ExploreDetailPriceView


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self customInit];
    }
    return self;
}

-(void)customInit {
    [[NSBundle mainBundle] loadNibNamed:@"ExploreDetailPriceView" owner:self options:nil];
    [self addSubview:_contentView];
    self.contentView.frame = self.frame;
    
    //self.leadingConstraint.constant = self.leadingConstraint.constant*SCREEN_SCALE;
    //self.trailingConstraint.constant = self.trailingConstraint.constant*SCREEN_SCALE;
    
    self.priceLabel.font = [UIFont fontWithName:FONT_MarkForMC_LIGHT size:[AppSize descriptionGreetingSize]];
    self.cuisineLabel.font = [UIFont fontWithName:FONT_MarkForMC_LIGHT size:[AppSize descriptionGreetingSize]];
    NSDictionary *attr = @{
                           NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:12.0*SCREEN_SCALE],
                           NSForegroundColorAttributeName: [AppColor placeholderTextColor],
                           NSKernAttributeName: @(1.0),
                           };
    self.priceTitleLabel.attributedText = [[NSAttributedString alloc] initWithString: NSLocalizedString(@"PRICE RANGE", nil)  attributes:attr];
    self.cuisineTitleLabel.attributedText = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"CUISINE", nil) attributes:attr];}

-(void)setupData:(BaseItem*)item {
    if ([item isKindOfClass:[AnswerItem class]]) {
//        NSString *priceRange = [NSString stringWithFormat:@"%.2f",((AnswerItem*)item).price];
        NSString *cuisine = ((AnswerItem*)item).userDefined1;
        NSString *strPriceRange = @"USD";
//        for (int i = 0; i< [priceRange integerValue]; i++) {
//            strPriceRange = [strPriceRange stringByAppendingString:@"$"];
//        }
        self.priceLabel.text = strPriceRange;
        self.cuisineLabel.text = cuisine;
        [self layoutIfNeeded];
        [_mainView.topAnchor constraintEqualToAnchor:self.topAnchor constant:0].active = YES;
        [_mainView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor constant:0].active = YES;
    }
}


@end
