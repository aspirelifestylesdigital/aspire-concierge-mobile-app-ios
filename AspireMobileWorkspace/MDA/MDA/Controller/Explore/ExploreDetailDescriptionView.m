//
//  ExploreDetailDescriptionView.m
//  TestLink
//
//  Created by Nghia Dinh on 7/18/17.
//  Copyright © 2017 Nghia Dinh. All rights reserved.
//

#import "ExploreDetailDescriptionView.h"
#import "LinkLabel.h"
#import "AnswerItem.h"
#import "Common.h"
#import "UILabel+Extension.h"
#import "NSString+Utis.h"
#import "UtilStyle.h"
#import "Constant.h"

@interface ExploreDetailDescriptionView () <TTTAttributedLabelDelegate>
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet LinkLabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIButton *expandButton;
@property (nonatomic) TypeDescriptionView viewType;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *traillingConstraint;


@end

@implementation ExploreDetailDescriptionView


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self customInit];
    }
    return self;
}


-(void)customInit {
    [[NSBundle mainBundle] loadNibNamed:@"ExploreDetailDescriptionView" owner:self options:nil];
    [self addSubview:_contentView];
    self.contentView.frame = self.frame;
    _descriptionLabel.delegate = self;
    _titleLabel.font = [UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize moreInfoTextSize]];
}



-(void)removeTitleLabel {
    [_titleLabel removeFromSuperview];
    [_descriptionLabel.topAnchor constraintEqualToAnchor:self.topAnchor constant:8.0].active = YES;
}

-(void)makeEmptyView {
    [_titleLabel removeFromSuperview];
    [_descriptionLabel.topAnchor constraintEqualToAnchor:self.topAnchor constant:0].active = YES;
    self.descriptionLabel.text = nil;
    _mainView.hidden = YES;
}

-(void)setupData:(BaseItem*)item withDetailType:(DetailType)detailType withFullContent:(ContentFullCCAItem*)content withType:(TypeDescriptionView)type {
    self.viewType = type;
    switch (self.viewType) {
        case ViewDescription:
            [self setupDataForDescription:item withFullContent:content withDetailType:detailType];
            break;
        case ViewTerm:
            [self setupDataForTerm:item withFullContent:content withDetailType:detailType];
            break;
        case ViewOperatorHours:
            [self setupDataForHoursOperator:item withFullContent:content withDetailType:detailType];
            break;
        case ViewDefaultTerm:
            [self setupDataForDefaultTerm:item];
        default:
            break;
    }
    [_mainView.topAnchor constraintEqualToAnchor:self.topAnchor constant:0].active = YES;
    [_mainView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor constant:0].active = YES;
}
-(void)setupDataForDescription:(BaseItem*)item withFullContent:(ContentFullCCAItem*)content  withDetailType:(DetailType)detailType{
    if(detailType == DetailType_Dining) {
        [self removeTitleLabel];
    } else {
        NSDictionary *attr = @{
                               NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize moreInfoTextSize]],
                               NSForegroundColorAttributeName: [AppColor placeholderTextColor],
                               NSKernAttributeName: @(1.0),
                               };
        self.titleLabel.attributedText = [[NSAttributedString alloc] initWithString: NSLocalizedString(@"DESCRIPTION", nil)        attributes:attr];
    }
    //[self removeTitleLabel];
    NSString *description;
    if (detailType != DetailType_Other) {
        description = ((AnswerItem*)item).answerDescription;
    }else{
        description = content.descriptionContent;
    }
    description = [self formatHTMLString:description];
    description = [NSString stringWithFormat:@"<html><head><style>body{font-family: '%@'; font-size:%fpx; color:#011627; font-weight:normal;line-height: %fpx;}</style></head><body>%@</body></html>",FONT_MarkForMC_LIGHT, 18.0f*SCREEN_SCALE, 22.0 * SCREEN_SCALE, description];
    [_descriptionLabel setHTMLString:description];
    
    NSString *contentLabel = _descriptionLabel.attributedText.string;
    contentLabel = [contentLabel stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSArray* words = [contentLabel componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString* nospacestring = [words componentsJoinedByString:@""];

    if ([nospacestring length] == 0 ){
        [self makeEmptyView];
    }
  
}

-(void)setupDataForTerm:(BaseItem*)item withFullContent:(ContentFullCCAItem*)content  withDetailType:(DetailType)detailType{
    NSDictionary *attr = @{
                           NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize moreInfoTextSize]],
                           NSForegroundColorAttributeName: [AppColor placeholderTextColor],
                           NSKernAttributeName: @(1.0),
                           };
    NSString *description = [[NSString alloc] init];
    AnswerItem *answerItem = (AnswerItem*)item;
    switch (detailType) {
        case DetailType_Dining:
            _titleLabel.attributedText = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"TERMS & CONDITIONS", nil) attributes:attr];
            description = answerItem.offer2.length > 0 ? NSLocalizedString(@"This offer is available only to eligible Mastercard World and World Elite cardholders. In order to receive your dining benefit, reservations must be booked through Mobile Concierge. An entree must be purchased to receive your dining benefit. A minimum-spend per party may be required where indicated. Your dining benefit may not be combined with other offers.", nil) : @"";
            break;
        case DetailType_CityGuide:
            _titleLabel.attributedText = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"TERMS & CONDITIONS", nil) attributes:attr];
            description = NSLocalizedString(@"This offer is available only to eligible Mastercard World and World Elite cardholders. In order to receive your dining benefit, reservations must be booked through Mobile Concierge. An entree must be purchased to receive your dining benefit. A minimum-spend per party may be required where indicated. Your dining benefit may not be combined with other offers.", nil);
            break;
            
        case DetailType_Other:
            _titleLabel.attributedText = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"TERMS OF USE", nil) attributes:attr];
            description = [NSString stringWithFormat:@"%@\n%@",content.termsOfUse ? content.termsOfUse : @"", NSLocalizedString(@"This offer is available only to eligible Mastercard World and World Elite cardholders. In order to receive your dining benefit, reservations must be booked through Mobile Concierge. An entree must be purchased to receive your dining benefit. A minimum-spend per party may be required where indicated. Your dining benefit may not be combined with other offers.", nil)];;
            break;
        default:
            break;
    }
    if(description.length > 0) {
        description = [NSString stringWithFormat:@"<html><head><style>body{font-family: '%@'; font-size:%fpx; color:#011627; font-weight:normal;line-height: %fpx;}</style></head><body>%@</body></html>",FONT_MarkForMC_LIGHT, 18.0f*SCREEN_SCALE, 22.0 * SCREEN_SCALE, description];
        [_descriptionLabel setHTMLString:description];
    } else {
        [self makeEmptyView];
    }
}

-(void)setupDataForHoursOperator:(BaseItem*)item withFullContent:(ContentFullCCAItem*)content  withDetailType:(DetailType)detailType{
    NSString *description = [NSString stringWithFormat:@"%@", ((AnswerItem*)item).hoursOfOperation];
    if (description.length > 0){
        NSDictionary *attr = @{
                               NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize moreInfoTextSize]],
                               NSForegroundColorAttributeName: [AppColor placeholderTextColor],
                               NSKernAttributeName: @(1.0),
                               };
        self.titleLabel.attributedText = [[NSAttributedString alloc] initWithString: NSLocalizedString(@"HOURS OF OPERATION", nil)  attributes:attr];
        description = [NSString stringWithFormat:@"<html><head><style>body{font-family: '%@'; font-size:%fpx; color:#011627; font-weight:normal;line-height: %fpx;}</style></head><body>%@</body></html>",FONT_MarkForMC_LIGHT, 18.0f*SCREEN_SCALE, 22.0 * SCREEN_SCALE, description];
        [_descriptionLabel setHTMLString:description];
    }
    else {
        [self makeEmptyView];
    }
}

-(void)setupDataForDefaultTerm:(BaseItem*)item {
    if ([item isKindOfClass:[AnswerItem class]]) {
        BOOL isOffer = ((AnswerItem*)item).isOffer;
        if (isOffer) {
            NSDictionary *attr = @{
                                   NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize moreInfoTextSize]],
                                   NSForegroundColorAttributeName: [AppColor placeholderTextColor],
                                   NSKernAttributeName: @(1.0),
                                   };
            _titleLabel.attributedText = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"TERMS & CONDITIONS", nil) attributes:attr];
            NSString *description = NSLocalizedString(@"<p>In order to receive your dining benefit, reservations must be booked through Concierge. An entree must be purchased to receive your dining benefit. A minimum-spend per party may be required where indicated. Your dining benefit may not be combined with other offers.</p>", nil);
           
            description = [NSString stringWithFormat:@"<html><head><style>body{font-family: '%@'; font-size:%fpx; color:#011627; font-weight:normal;line-height: %fpx;}</style></head><body>%@</body></html>",FONT_MarkForMC_LIGHT, 18.0f*SCREEN_SCALE, 22.0 * SCREEN_SCALE, description];
            [_descriptionLabel setHTMLString:description];
        } else {
            [self makeEmptyView];
        }
    } else {
        [self makeEmptyView];
    }
}


-(NSString*)formatHTMLString:(NSString*)htmlString {
    NSString * formatedHTMLString = [htmlString removeRedudantNewLineInText];
    NSRange range = [formatedHTMLString rangeOfString:@"Click here"];
    if (range.location != NSNotFound) {
        formatedHTMLString = [formatedHTMLString substringToIndex:range.location];
        formatedHTMLString = [NSString stringWithFormat:@"%@ </p>",formatedHTMLString];
    }
    range = [formatedHTMLString rangeOfString:@"Contact us"];
    if (range.location != NSNotFound) {
        formatedHTMLString = [formatedHTMLString substringToIndex:range.location];
        formatedHTMLString = [NSString stringWithFormat:@"%@ </p>",formatedHTMLString];
    }
    while ([formatedHTMLString containsString:@"<img"])
    {
        NSRange range2 = [formatedHTMLString rangeOfString:@"<img"];
        if(range2.location != NSNotFound)
        {
            // get image link from Description
            NSString *imgString = [formatedHTMLString substringFromIndex:range2.location];
            
            NSString* result = [imgString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if ([result containsString:@"src"] && [result containsString:@"/>"]){
                result = [result substringToIndex:[result rangeOfString:@"/>"].location + 2];
            }
            
            /// trim image tag from Description
            formatedHTMLString = [formatedHTMLString stringByReplacingOccurrencesOfString:result withString:@""];
            formatedHTMLString = [[self flattenHtml:formatedHTMLString] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            formatedHTMLString = [formatedHTMLString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
        }
    }
    formatedHTMLString = [formatedHTMLString stringByReplacingOccurrencesOfString:@"<i></i>" withString:@""];
    formatedHTMLString = [formatedHTMLString stringByReplacingOccurrencesOfString:@"<b></b>" withString:@""];
    formatedHTMLString = [formatedHTMLString stringByReplacingOccurrencesOfString:@"<p></p>" withString:@""];
    formatedHTMLString = [formatedHTMLString removeRedudantNewLineInText];
    return formatedHTMLString;

}

- (NSString *)flattenHtml: (NSString *) html {
    NSScanner *theScanner;
    NSString *text = nil;
    theScanner = [NSScanner scannerWithString: html];
    while ([theScanner isAtEnd] == NO) {
        
        [theScanner scanUpToString: @"<" intoString: NULL];
        
        [theScanner scanUpToString: @">" intoString: &text];
        
        // Only Replace if you are outside of an html tag
    } // while
    return html;
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    if ([self.delegate respondsToSelector:@selector(didSelectURL:)]) {
        [self.delegate didSelectURL:url];
    }
}

-(void)attributedLabel:(TTTAttributedLabel *)label didLongPressLinkWithURL:(NSURL *)url atPoint:(CGPoint)point {
    if ([self.delegate respondsToSelector:@selector(didSelectURL:)]) {
        [self.delegate didSelectURL:url];
    }
}


@end
