//
//  MyProfileViewController.m
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/13/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "MyProfileViewController.h"
#import "ImageUtilities.h"
#import "NSString+Utis.h"
#import "Constant.h"
#import "SWRevealViewController.h"
#import "UIButton+Extension.h"
#import "HomeViewController.h"
#import "NSString+Utis.h"


#import "AppDelegate.h"
#import "UserObject.h"
#import "UITextField+Extensions.h"
#import "UtilStyle.h"
#import "WSBase.h"
#import "WSB2CGetUserDetails.h"
#import "LocationComponent.h"
#import "WSPreferences.h"
#import <AspireApiFramework/AspireApiFramework.h>
#import "PreferenceObject.h"

@interface MyProfileViewController ()<DataLoadDelegate> {
    WSPreferences *wsPreferences;
    WSB2CGetUserDetails *wsGetUser;
}


@end

@implementation MyProfileViewController
{
    NSString *firstNameOnTyping;
    NSString *lastNameOnTyping;
    NSString *emailOnTyping;
    NSString *phoneOnTyping;
    NSString *passwordOnTyping;
    NSString *confirmPasswordOnTyping;
    BOOL isTempUseLocation;
}

#pragma mark - Life Cycle
- (void)viewDidLoad
{
    isTempUseLocation = [[SessionData shareSessiondata] isUseLocation];
    isNotAskConciergeBarButton = YES;
    self.isUpdateProfile = YES;
    [super viewDidLoad];
    [self createBackBarButton];
    [self setNavigationBarWithDefaultColorAndTitle:NSLocalizedString(@"MY PROFILE", nil)];
    backupBottomConstraint = self.viewActionBottomConstraint.constant;
    [self setButtonStatus:NO];
    [self getUserProfile];
    if([[SessionData shareSessiondata] isUseLocation]) {
        [[SessionData shareSessiondata] setIsShownLocationServiceAlert:NO];
        [LocationComponent startRequestLocation];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    //  Set up Pop pan gesture
    [self setUpCustomizedPanGesturePopRecognizer];
    

    if(self.navigationController.viewControllers.count == 1)
    {
        HomeViewController *homeView = [[HomeViewController alloc] init];
        [self.navigationController setViewControllers:@[homeView,self]];
    }
    
    self.submitButton.hidden = YES;
    //self.cancelButton.hidden = YES;
    self.commitmentLabel.hidden = YES;
    
    [self setTextViewsDefaultBottomBolder];
    [self.view layoutIfNeeded];
    [self loadDefaultData];
//    [self setDefaultData];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (isTempUseLocation != isUseLocation) {
        [[SessionData shareSessiondata] setIsUseLocation:isTempUseLocation];
    }
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeSetupForCustomizedPanGesturePopRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Setup View

- (void)initView{
    self.phoneNumberText.tag = PHONE_NUMBER_TEXTFIELD_TAG;
    [self setButtonFrame];
    
    self.emailText.textColor = [AppColor placeholderTextColor];
    
    [self.updateButton setBackgroundColorForNormalStatus];
    [self.updateButton setBackgroundColorForTouchingStatus];
    [self.updateButton configureDecorationForButton];
    
    [self.cancelButtonMyProfile setBackgroundColorForNormalStatus];
    [self.cancelButtonMyProfile setBackgroundColorForTouchingStatus];
    [self.cancelButtonMyProfile configureDecorationForButton];
    
    // Text Style For Greet Message
    NSString *message = NSLocalizedString(@"I'd like to receive the monthly newsletter.", nil);
    self.commitmentLabel.attributedText = [UtilStyle setLargeSizeStyleForLabelWithMessage:message];
    
    [self setCheckBoxState:isCheck];
    self.commitmentLabel.text = NSLocalizedString(@"I'd like to receive the monthly newsletter.", nil);
    
    self.firstNameText.placeholder = NSLocalizedString(@"First Name", nil);
    self.lastNameText.placeholder = NSLocalizedString(@"Last Name", nil);
    self.emailText.placeholder = NSLocalizedString(@"Email", nil);
    self.phoneNumberText.placeholder = NSLocalizedString(@"Mobile Phone", nil);
    self.useLocationLabel.text = NSLocalizedString(@"Use My Location", nil);
    self.onLabel.text = [NSLocalizedString(@"ON", nil) uppercaseString];
    self.offLabel.text = [NSLocalizedString(@"OFF", nil) uppercaseString];
    
    [self.updateButton setTitle:NSLocalizedString(@"UPDATE", nil) forState:UIControlStateNormal];
    [self.cancelButtonMyProfile setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                 NSFontAttributeName: [UIFont fontWithName:FONT_MarkForMC_MED size:[AppSize titleTextSize]],
                                 NSKernAttributeName: @1.4};
    self.updateButton.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.updateButton.titleLabel.text attributes:attributes];
    self.cancelButtonMyProfile.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:self.cancelButtonMyProfile.titleLabel.text attributes:attributes];
    
    [self.updateButtonView setBackgroundColor:[AppColor backgroundColor]];
    [self.cancelButtonView setBackgroundColor:[AppColor backgroundColor]];
    
}

- (void)setButtonStatus:(BOOL)status{
    self.updateButton.enabled = status;
    self.cancelButtonMyProfile.enabled = status;
}


- (void) setButtonFrame{
}

-(void) createAsteriskForTextField:(UITextField *)textField{
    UIView *leftView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
    [leftView setBackgroundColor:[UIColor clearColor]];
    [textField setLeftView:leftView];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    
    asteriskImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"asterisk_icon"]];
    
    asteriskImage.frame = CGRectMake(0.0f, 0.0f, 20.0f, 20.0f);
    asteriskImage.contentMode = UIViewContentModeCenter;
    [textField setRightView:asteriskImage];
    [textField setRightViewMode:UITextFieldViewModeAlways];
}

#pragma mark - Setup Data

-(void)loadDefaultData {
    if ([User isValid]) {
        UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
        userObject.salutation = [userObject.salutation converSalutationFormAPI];
        [[SessionData shareSessiondata] setUserObject:userObject];
        
        NSString *salutation = userObject.salutation;
        if (salutation.length > 1) {
            self.salutationDropDown.titleColor = [AppColor textColor];
            self.salutationDropDown.title = salutation;
        }
        NSString *tempPhone = userObject.mobileNumber;
        self.firstNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:userObject.firstName];
        self.lastNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:userObject.lastName];
        self.emailText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:userObject.email];
        self.phoneNumberText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:(tempPhone.length < 15) && ![tempPhone containsString:@"+"] ? [NSString stringWithFormat:@"+%@",tempPhone] : tempPhone];
        
        isUseLocation = [[SessionData shareSessiondata] isUseLocation];
        isTempUseLocation = isUseLocation;
        [self setImageLocation:isUseLocation];
        
        currentFirstName = userObject.firstName;
        currentEmail = userObject.email;
        currentPhone = (tempPhone.length < 15) && ![tempPhone containsString:@"+"] ? [NSString stringWithFormat:@"+%@",tempPhone] : tempPhone;
        currentLastName = userObject.lastName;
        
        self.firstNameText.text = [currentFirstName createMaskForText:NO];
        self.emailText.text = [currentEmail createMaskForText:YES];
        self.lastNameText.text = [currentLastName createMaskForText:NO];
        self.phoneNumberText.text = (currentPhone.length > 3) ? [currentPhone createMaskStringBeforeNumberCharacter:3]:currentPhone;
        self.emailText.enabled = NO;
    }
}

-(void)getUserProfile
{
//    [self startActivityIndicator];
//    [self getPreference];
    
    [self startActivityIndicator];
    __weak typeof (self) _self = self;
    [ModelAspireApiManager retrieveProfileCurrentUser:^(User *user, NSError *error) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            [_self stopActivityIndicator];
            if (!error) {
                [_self setDefaultData];
                
                if ([User isValid]) {
                    UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
                    userObject.salutation = [userObject.salutation converSalutationFormAPI];
                    [[SessionData shareSessiondata] setUserObject:userObject];
                    
                    NSString *salutation = userObject.salutation;
                    if (salutation.length > 1) {
                        _self.salutationDropDown.titleColor = [AppColor textColor];
                        _self.salutationDropDown.title = salutation;
                    }
                    NSString *tempPhone = userObject.mobileNumber;
                    _self.firstNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:userObject.firstName];
                    _self.lastNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:userObject.lastName];
                    _self.emailText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:userObject.email];
                    _self.phoneNumberText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:(tempPhone.length < 15) && ![tempPhone containsString:@"+"] ? [NSString stringWithFormat:@"+%@",tempPhone] : tempPhone];
                    
                    isUseLocation = [[SessionData shareSessiondata] isUseLocation];
                    isTempUseLocation = isUseLocation;
                    [_self setImageLocation:isUseLocation];
                    
                    currentFirstName = userObject.firstName;
                    currentEmail = userObject.email;
                    currentPhone = (tempPhone.length < 15) && ![tempPhone containsString:@"+"] ? [NSString stringWithFormat:@"+%@",tempPhone] : tempPhone;
                    currentLastName = userObject.lastName;
                    
                    _self.firstNameText.text = [currentFirstName createMaskForText:NO];
                    _self.emailText.text = [currentEmail createMaskForText:YES];
                    _self.lastNameText.text = [currentLastName createMaskForText:NO];
                    _self.phoneNumberText.text = (currentPhone.length > 3) ? [currentPhone createMaskStringBeforeNumberCharacter:3]:currentPhone;
                    _self.emailText.enabled = NO;
                }
            }else{
                switch ([AspireApiError getErrorTypeAPIRetrieveProfileFromError:error]) {
                    case UNKNOWN_ERR:
                        // show general message for update profile
                        [_self showApiErrorWithMessage:@""];
                        NSLog(@"[RESULT] => LOGIN FAIL: %@",@"UNKNOWN_ERR");
                        break;
                    case ACCESSTOKEN_INVALID_ERR:
                        [_self handleInvalidCredentials];
                        // show message invalid accesstoken
                        NSLog(@"[RESULT] => LOGIN FAIL: %@",@"ACCESSTOKEN_INVALID_ERR");
                        // login again
                        break;
                    default:
                        [_self showApiErrorWithMessage:@""];
                        NSLog(@"[RESULT] => LOGIN FAIL: %@",@"OUT OF ENUM AspireApiErrorType");
                        break;
                }
            }
        });
    }];
    
}

- (void)getPreference{
    wsPreferences = [[WSPreferences alloc] init];
    wsPreferences.delegate = self;
    [wsPreferences getPreference];
}

- (void)updateSuccessRedirect{
    AlertViewController *alert = [[AlertViewController alloc] init];
    alert.titleAlert =nil;
    alert.msgAlert = NSLocalizedString(@"You have successfully updated your profile.", nil);
    alert.firstBtnTitle = NSLocalizedString(@"OK", nil);
    dispatch_async(dispatch_get_main_queue(), ^{
        alert.seconBtn.hidden = YES;
        alert.midView.alpha = 0.0f;;
        [alert.view layoutIfNeeded];
    });
    
    alert.blockFirstBtnAction = ^(void){
        isTempUseLocation = isUseLocation;
        [self getUserInfo];
        [self setTextViewsDefaultBottomBolder];
        [self setButtonStatus:NO];
    };
    
    [self showAlert:alert forNavigation:NO];
}


#pragma mark API PROCESS
- (void)updateProfileWithAPI{
    [self startActivityIndicator];
    
    UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
    NSMutableDictionary *dictDai = [[NSMutableDictionary alloc] init];
    [dictDai setValue:(self.salutationDropDown.title.length > 1) ? [self.salutationDropDown.title converSalutationForAPI] : @" " forKey:@"Salutation"];
    [dictDai setValue:currentFirstName forKey:@"FirstName"];
    [dictDai setValue:currentLastName forKey:@"LastName"];
    [dictDai setValue:currentPhone forKey:@"MobileNumber"];
    [dictDai setValue:currentEmail forKey:@"Email"];
    [dictDai setValue:currentPassword forKey:@"Password"];
    [dictDai setValue:[[SessionData shareSessiondata] passcode] forKey:APP_USER_PREFERENCE_Passcode];
    [dictDai setValue:isUseLocation? @"Yes": @"No" forKey:APP_USER_PREFERENCE_Location];
    [dictDai setValue:@"USA" forKey:@"Country"];
    [dictDai setValue:APP_NAME forKey:@"referenceName"];
    [dictDai setValue:[self getPreferenceByType:DiningPreferenceType].value forKey:@"Dining Preference"];
    [dictDai setValue:[self getPreferenceByType:HotelPreferenceType].value forKey:@"Hotel Preference"];
    [dictDai setValue:[self getPreferenceByType:TransportationPreferenceType].value forKey:@"Car Type Preference"];
    [dictDai setValue: !userObject.currentCity || [userObject.currentCity isEqualToString:@""] ? @"Unknown" : userObject.currentCity forKey:@"City"];
    [dictDai setValue: !userObject.currentCity || [userObject.currentCity isEqualToString:@""] ? @"Unknown" : userObject.currentCity forKey: APP_USER_PREFERENCE_Selected_City];
    __weak typeof(self) _self = self;
    [ModelAspireApiManager updateProfileCurrentUserWithUserInfo:dictDai completion:^(NSError *error) {
        [_self stopActivityIndicator];
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!error) {
                
                [_self updateSuccessRedirect];
            }else{
                switch ([AspireApiError getErrorTypeAPIUpdateProfileFromError:error]) {
                    case UNKNOWN_ERR:
                        // show general message for update profile
                        [_self showApiErrorWithMessage:@""];
                        NSLog(@"[RESULT] => LOGIN FAIL: %@",@"UNKNOWN_ERR");
                        break;
                    case ACCESSTOKEN_INVALID_ERR:
                        // show message invalid accesstoken
                        [_self handleInvalidCredentials];
                        NSLog(@"[RESULT] => LOGIN FAIL: %@",@"ACCESSTOKEN_INVALID_ERR");
                        // login again
                        break;
                    default:
                        [_self showApiErrorWithMessage:@""];
                        NSLog(@"[RESULT] => LOGIN FAIL: %@",@"OUT OF ENUM AspireApiErrorType");
                        break;
                }
            }
        });
    }];
}


- (void)setDefaultData {
    UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
    firstNameOnTyping = userObject.firstName;
    lastNameOnTyping = userObject.lastName;
    emailOnTyping = userObject.email;
    NSString *tempPhone = userObject.mobileNumber;
    phoneOnTyping = (tempPhone.length < 15) && ![tempPhone containsString:@"+"] ? [NSString stringWithFormat:@"+%@",tempPhone] : tempPhone;
    /*
    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
    if (profileDictionary) {
        firstNameOnTyping = [profileDictionary objectForKey:keyFirstName];
        lastNameOnTyping = [profileDictionary objectForKey:keyLastName];
        emailOnTyping = [profileDictionary objectForKey:keyEmail];
        NSString *tempPhone = [profileDictionary objectForKey:keyMobileNumber];
        phoneOnTyping = ([tempPhone containsString:@"+"])?tempPhone:[NSString stringWithFormat:@"+%@",tempPhone];
    }
     */
}

- (void)updateProfileButtonStatus{
    [self setButtonStatus:[self checkFieldIsChangeString]];
}

#pragma mark - On Change Data

- (void)changeUseLocation{
    [self setButtonStatus:[self checkFieldIsChangeString]];
}

- (void)changeValueDropDown{
    [self setButtonStatus:[self checkFieldIsChangeString]];
}

- (BOOL)checkFieldIsChangeString{
    
    if (isUseLocation != isTempUseLocation) {
        return YES;
    }
    if (firstNameOnTyping.length == 0 && lastNameOnTyping.length == 0 && emailOnTyping.length == 0 && phoneOnTyping.length == 0 && self.salutationDropDown.valueStatus == DefaultValueStatus) {
        return NO;
    }
    UserObject* userObject = (UserObject*)[[User current] convertToUserObjectWithClassName:NSStringFromClass([UserObject class]) andPreferencesClassName:NSStringFromClass([PreferenceObject class])];
    if (![self.salutationDropDown.title isEqualToString:userObject.salutation]) {
        return YES;
    }else if (![userObject.firstName isEqualToString:firstNameOnTyping]) {
        return YES;
    }else if (![userObject.lastName isEqualToString:lastNameOnTyping]){
        return YES;
    }
    else if (![userObject.email isEqualToString:emailOnTyping]){
        return YES;
    }
    else if (![userObject.mobileNumber isEqualToString:[phoneOnTyping stringByReplacingOccurrencesOfString:@"+" withString:@""]]){
        return YES;
    }
    return NO;
}

- (void)checkTextFieldIsChange:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *resultText = [textField.text stringByReplacingCharactersInRange:range
                                                                   withString:string];
    
    if (textField == self.firstNameText) {
        firstNameOnTyping = resultText;
    }else if (textField == self.lastNameText){
        lastNameOnTyping = resultText;
    }else if (textField == self.emailText){
        emailOnTyping = resultText;
    }else if (textField == self.phoneNumberText){
//        resultText = [resultText stringByReplacingOccurrencesOfString:@"+" withString:@""];
        phoneOnTyping = resultText;
    }else if (textField == self.passwordText){
        passwordOnTyping = resultText;
    }
    [self setButtonStatus:[self checkFieldIsChangeString]];
}


#pragma mark - Action Events
- (IBAction)touchUpdate:(id)sender {
    [self dismissKeyboard];
    [self verifyAccountData:YES];
}

- (IBAction)touchCancel:(id)sender {
    
    [self.phoneNumberText endEditing:YES];
    [self dismissKeyboard];
    [self setDefaultData];
    [self setTextViewsDefaultBottomBolder];
    [self setButtonStatus:NO];
    [[SessionData shareSessiondata] setIsUseLocation:isTempUseLocation];
    [self getUserInfo];
}

#pragma mark - API Delegate

-(void)loadDataDoneFrom:(WSBase*)ws
{
    if (ws.task == WS_CREATE_USER)
    {
        [self addPreferences];
    }
    else if(ws.task == WS_UPDATE_MY_PREFERENCE){
        [self stopActivityIndicator];
        [self updateSuccessRedirect];
    }
    else if(ws.task == WS_GET_MY_PREFERENCE)
    {
        wsGetUser = [[WSB2CGetUserDetails alloc] init];
        wsGetUser.delegate = self;
        [wsGetUser getUserDetails];
    }
    else if(ws.task == WS_B2C_GET_USER_DETAILS)
    {
        [self setDefaultData];
        
        NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
        if (profileDictionary.count > 2) {
            
            NSString *salutation = [profileDictionary objectForKey:keySalutation];
            if (salutation.length > 1) {
                self.salutationDropDown.titleColor = [AppColor textColor];
                self.salutationDropDown.title = salutation;
            }
            NSString *tempPhone = [profileDictionary objectForKey:keyMobileNumber];
            self.firstNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:[profileDictionary objectForKey:keyFirstName]];
            self.lastNameText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:[profileDictionary objectForKey:keyLastName]];
            self.emailText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:[profileDictionary objectForKey:keyEmail]];
            self.phoneNumberText.attributedText = [UtilStyle setTextStyleForTextFieldWithMessage:([tempPhone containsString:@"+"])?tempPhone:[NSString stringWithFormat:@"+%@",tempPhone]];
            
            isUseLocation = [[SessionData shareSessiondata] isUseLocation];
            isTempUseLocation = isUseLocation;
            [self setImageLocation:isUseLocation];
            
            currentFirstName = [profileDictionary objectForKey:keyFirstName];
            currentEmail = [profileDictionary objectForKey:keyEmail] ;
            currentPhone = ([tempPhone containsString:@"+"])?tempPhone:[NSString stringWithFormat:@"+%@",tempPhone];
            currentLastName = [profileDictionary objectForKey:keyLastName];
            
            self.firstNameText.text = [currentFirstName createMaskForText:NO];
            self.emailText.text = [currentEmail createMaskForText:YES];
            self.lastNameText.text = [currentLastName createMaskForText:NO];
            //self.phoneNumberText.text = (currentPhone.length > 3) ? [currentPhone createMaskStringBeforeNumberCharacter:3]:currentPhone;
            self.phoneNumberText.text = [currentPhone createMaskForText:NO];
            self.emailText.enabled = NO;
            
        }
        [self stopActivityIndicator];
    }
}

- (void)WSBaseNetworkUnavailable{
    [self stopActivityIndicator];
}



@end
