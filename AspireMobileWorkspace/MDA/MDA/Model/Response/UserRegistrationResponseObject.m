//
//  UserRegistrationResponseObject.m
//  MobileConcierge
//
//  Created by user on 6/22/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "UserRegistrationResponseObject.h"
#import "NSDictionary+SBJSONHelper.h"
#import "UserRegistrationItem.h"

@implementation UserRegistrationResponseObject
-(id)initFromDict:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        [self parseCommonResponse:dict];
        if(self.isSuccess)
        {
            self.data = [self parseJson:dict toObject:NSStringFromClass([UserRegistrationItem class])];
        }
    }
    return self;
}

-(id)parseJson:(NSDictionary *)dict toObject:(NSString *)className
{
    if([className isEqualToString:NSStringFromClass([UserRegistrationItem class])])
    {
        UserRegistrationItem *item = [[UserRegistrationItem alloc] init];
        item.OnlineMemberID = [dict stringForKey:@"OnlineMemberID"];
        item.hasForgotAccount = [dict boolForKey:@"HasForgotPassword"];
        NSArray *arr = dict[@"OnlineMemberDetailIDs"];
        NSString *onlStr = [arr objectAtIndex:0];

        item.OnlineMemberDetailIDs = [onlStr stringByTrimmingCharactersInSet:
                                      [NSCharacterSet whitespaceAndNewlineCharacterSet]];

        return [[NSArray alloc] initWithObjects:item, nil];
    }
    
    return nil;
}

-(void)parseCommonResponse:(NSDictionary *)dict
{
    self.status = [dict boolForKey:@"success"];
    NSArray* mesArr = [dict arrayForKey:@"message"];
    if(mesArr && mesArr.count > 0){
        NSDictionary * error = [mesArr objectAtIndex:0];
        self.message = [error stringForKey:@"message"];
        self.errorCode = [error stringForKey:@"code"];
    }
    else
    {
        self.message = [dict objectForKey:@"message"];
    }
}
@end
