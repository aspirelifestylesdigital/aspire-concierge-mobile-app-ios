//
//  TileResponseObject.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/6/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "TileResponseObject.h"
#import "TileItem.h"
#import "NSDictionary+SBJSONHelper.h"
#import "AppData.h"
#import "CategoryItem.h"
#import "CCAMapCategories.h"

@interface TileResponseObject()
{
    NSArray *categoryIDsForCurrentCategory;
    NSArray *categoriesForCCAData;
}
@end

@implementation TileResponseObject

-(id)initFromDict:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        dict = [dict dictionaryForKey:@"GetTilesResult"];
        self.data = [dict arrayForKey:@"Tiles" withProcessing:self toObject:NSStringFromClass([TileItem class])];
        [self parseCommonResponse:dict];
    }
    return self;
}

-(id)parseJson:(NSDictionary *)dict toObject:(NSString *)className
{
    if([className isEqualToString:NSStringFromClass([TileItem class])])
    {
        self.totalItem += 1;
        TileItem *item = [self parseJsonForQuestionItem:dict];
        
        if(self.searchText && ![self verifyDataForSearch:item])
        {
            return nil;
        }
        
        
        self.totalSearchItem += 1;
        return item;
    }
    
    
    return nil;
}
-(BOOL)verifyDataForSearch:(TileItem *)item
{
    if([item.name containsString:@"Valley"]) {
        NSLog(@"");
    }
    NSMutableString *itemString = [[NSMutableString alloc] init];
    if(item.title.length > 0)
        [itemString appendString:item.title];
    [itemString appendString:@" "];
    if(item.tileText.length > 0)
    [itemString appendString:item.tileText];
    [itemString appendString:@" "];
    if(item.shortDescription.length > 0)
    [itemString appendString:item.shortDescription];
    
    return [[itemString lowercaseString] containsString:[self.searchText lowercaseString]];
}

-(TileItem *)parseJsonForQuestionItem:(NSDictionary *)dict
{
    TileItem *item = [[TileItem alloc] init];
    item.ID = [dict stringForKey:@"ID"];
    item.title = [dict stringForKey:@"Title"];
    item.tileText = [dict stringForKey:@"TileText"];
    item.tileImage = [dict stringForKey:@"TileImage"];
    item.tileLink = [dict stringForKey:@"TileLink"];
    item.shortDescription = [dict stringForKey:@"ShortDescription"];
    item.GeographicRegion = [dict stringForKey:@"GeographicRegion"];
    item.tileSubCategory = [dict stringForKey:@"SubCategory"];
    item.tileCategory = [dict stringForKey:@"Category"];
    item.currentCategoryCode = self.categoryCode;
    
    if([self.categoryCode isEqualToString:@"appgallery"])
    {
        item.shortDescription = [self colorForGalleryPage:item.shortDescription];
    }
    
    return [self mappingItemForCategory:item];
}

-(void)parseCommonResponse:(NSDictionary *)dict
{
    self.status = [dict boolForKey:@"Success"];
    NSArray* mesArr = [dict arrayForKey:@"message"];
    if(mesArr && mesArr.count > 0){
        NSDictionary * error = [mesArr objectAtIndex:0];
        self.message = [error stringForKey:@"message"];
        self.errorCode = [error stringForKey:@"code"];
    }
    
    self.message = [dict objectForKey:@"message"];
}

-(NSString *)colorForGalleryPage:(NSString *)text
{
    NSRange rangeForColorText = NSMakeRange(NSNotFound, 0);
    if(text.length > 0)
        rangeForColorText = [text rangeOfString:@"#"];
    rangeForColorText.length = 7;
    return [text substringWithRange:rangeForColorText];
}

- (TileItem *)mappingItemForCategory:(TileItem *)item
{
    if([item.categoryCode compare:Global_Offers options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return item;
    }
    
    if([self.categoryCode isEqualToString:@"appgallery"])
    {
        return item;
    }
    else if([self.categoryCode isEqualToString:@"all"])
    {
        if([item.categoryCode isEqualToString:@""]){
            return nil;
        }
    }
    // filter item based on the current category
    else if( [self.categoryCode compare:item.categoryCode options:NSCaseInsensitiveSearch] != NSOrderedSame)
    {
        return nil;
    }
    
    // !!!: Feature for Hotels, Cruise, VP
    if(![item.tileSubCategory isEqualToString:@"Global Partners"])
    {
        if([item.GeographicRegion stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0 &&
           [item.tileSubCategory stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0)
        {
            return item;
        }
        
        NSArray* geographic = [self.currentCity.GeographicRegion componentsSeparatedByString:@"|"];
        NSArray* subcate = [self.currentCity.SubCategory componentsSeparatedByString:@"|"];
        BOOL isOK = NO;
        if([item.categoryCode isEqualToString:@"hotels"] || [item.categoryCode isEqualToString:@"vacation packages"])
        {
            // if city dont have GeographicRegion && item not is Global Parntners
            if(self.currentCity.GeographicRegion.length == 0 &&
               ![item.tileSubCategory isEqualToString:@"Global Partners"] &&
               item.tileSubCategory.length > 0)
                return nil;
            
            for (int i = 0; i < geographic.count; i++) {
                NSString* geo = [geographic objectAtIndex:i];
                NSString* sub = [subcate objectAtIndex:i];
                if([geo isEqualToString:@"all"]) {
                    if([sub isEqualToString:item.tileSubCategory] && ![self.currentCity.ExceptGeographicRegion containsString:item.GeographicRegion]) {
                        isOK = YES;
                        break;
                    }
                }
                else if([sub isEqualToString:@"all"])
                {
                    if([geo isEqualToString:item.GeographicRegion]){
                        isOK = YES;
                        break;
                    }
                }
                else {
                    if([geo isEqualToString:item.GeographicRegion] && [sub isEqualToString:item.tileSubCategory]) {
                        isOK = YES;
                        break;
                    }
                }
            }
            
            if(!isOK) {
                return nil;
            }
        }
        else
        {
            NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:[self.currentCity.GeographicRegion componentsSeparatedByString:@"|"]];
            geographic = [orderedSet array];
            if([item.GeographicRegion stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0){
                return item;
            }
            
            
            for (int i = 0; i < geographic.count; i++) {
                NSString* geo = [geographic objectAtIndex:i];
                if([geo isEqualToString:item.GeographicRegion]){
                    isOK = YES;
                    break;
                }
            }
            
            if(!isOK) {
                return nil;
            }
            //                [geographic containsObject:item.GeographicRegion] ? (return item) : (return nil);
        }
    }
    
    return item;
}

@end
