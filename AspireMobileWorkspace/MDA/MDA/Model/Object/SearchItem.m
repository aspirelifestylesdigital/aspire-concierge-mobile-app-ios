//
//  SearchItem.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/21/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "SearchItem.h"
#import "NSString+Utis.h"

@implementation SearchItem

-(NSString *)name
{
    return self.title;
}

-(NSString *)offer
{
    return [self.searchDescription removeRedudantNewLineInText];
}

-(BOOL)isOffer
{
    return self.hasOffer;
}

-(NSString *)categoryCode
{
    if([self.product isEqualToString:@"IA"] || ([self.searchCategory compare:@"Dining" options:NSCaseInsensitiveSearch] == NSOrderedSame) || ([self.searchCategory compare:@"Specialty Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.searchSubCategory compare:@"Culinary Experiences" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return @"dining";
    }
    else if([self.searchCategory compare:@"Hotels" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"hotels";
    }
    else if(([self.currentCategoryCode isEqualToString:@"flowers"] || [self.currentCategoryCode isEqualToString:@"all"])
            && ([self.searchCategory compare:@"Flowers" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return @"flowers";
    }
    else if(([self.currentCategoryCode isEqualToString:@"sport"] || [self.currentCategoryCode isEqualToString:@"all"])
            && ([self.searchCategory compare:@"Golf" options:NSCaseInsensitiveSearch] == NSOrderedSame
                || ([self.searchCategory compare:@"Specialty Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.searchSubCategory compare:@"Golf Experiences" options:NSCaseInsensitiveSearch] == NSOrderedSame)))
    {
        return @"sport";
    }
    else if(([self.searchCategory compare:@"Tickets" options:NSCaseInsensitiveSearch] == NSOrderedSame)
            || ([self.searchCategory compare:@"Specialty Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame &&
                ([self.searchSubCategory compare:@"Entertainment Experiences" options:NSCaseInsensitiveSearch] == NSOrderedSame
                 || [self.searchSubCategory compare:@"Major Sports Events" options:NSCaseInsensitiveSearch] == NSOrderedSame)))
    {
        return @"entertainment";
    }
    /*
    else if(([self.currentCategoryCode isEqualToString:@"golf"] || [self.currentCategoryCode isEqualToString:@"all"])
            && (([self.searchCategory compare:@"Golf" options:NSCaseInsensitiveSearch] == NSOrderedSame)
                || ([self.searchCategory compare:@"Golf Merchandise" options:NSCaseInsensitiveSearch] == NSOrderedSame)
                || ([self.searchCategory compare:@"Specialty Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame &&
                    [self.searchSubCategory compare:@"Golf Experiences" options:NSCaseInsensitiveSearch] == NSOrderedSame)))
    {
        return @"golf";
    }
     */
    else if([self.searchCategory compare:@"Vacation Packages" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"vacation packages";
    }
    /*
    else if([self.searchCategory compare:@"Cruises" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"cruise";
    }
     */
    else  if([self.searchCategory compare:@"VIP Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.searchSubCategory compare:@"Sightseeing" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"tours";
    }
    else if(([self.searchCategory compare:@"Private Jet Travel" options:NSCaseInsensitiveSearch] == NSOrderedSame)
            || ([self.searchCategory compare:@"VIP Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.searchSubCategory compare:@"Airport Services" options:NSCaseInsensitiveSearch] == NSOrderedSame)
            || ([self.searchCategory compare:@"VIP Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.searchSubCategory compare:@"Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return @"travel";
    }
    /*
    else  if([self.searchCategory compare:@"VIP Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.searchSubCategory compare:@"Airport Services" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"airport services";
    }
    else  if([self.searchCategory compare:@"VIP Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame && [self.searchSubCategory compare:@"Travel Services" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return @"travel services";
    }
     */
    else if([self.searchCategory compare:@"Transportation" options:NSCaseInsensitiveSearch] == NSOrderedSame && ([self.searchSubCategory compare:@"Car Rental" options:NSCaseInsensitiveSearch] == NSOrderedSame || [self.searchSubCategory compare:@"Limos and Private Car Service" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return @"transportation";
    }
    else if(([self.currentCategoryCode isEqualToString:@"shopping"] || [self.currentCategoryCode isEqualToString:@"all"])
            && ([self.searchCategory compare:@"Golf" options:NSCaseInsensitiveSearch] == NSOrderedSame
                || [self.searchCategory compare:@"Flowers" options:NSCaseInsensitiveSearch] == NSOrderedSame
                || [self.searchCategory compare:@"Wine" options:NSCaseInsensitiveSearch] == NSOrderedSame
                || [self.searchCategory compare:@"Retail Shopping" options:NSCaseInsensitiveSearch] == NSOrderedSame))
    {
        return @"shopping";
    }
    else if([self.searchCategory compare:Global_Offers options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return Global_Offers;
    }
    else
    {
        return @"";
    }
}
@end
