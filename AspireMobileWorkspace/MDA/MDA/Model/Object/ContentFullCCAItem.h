//
//  ContentFullCCAItem.h
//  MobileConcierge
//
//  Created by Chung Mai on 6/5/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContentFullCCAItem : NSObject

@property (nonatomic, assign) BOOL benefitInd;
@property (nonatomic, strong) NSString *category;
@property (nonatomic, strong) NSString *descriptionContent;
@property (nonatomic, strong) NSString *geographicRegion;
@property (nonatomic, assign) BOOL hotInd;
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, assign) BOOL offerInd;
@property (nonatomic, strong) NSString *offerText;
@property (nonatomic, strong) NSString *parentCategory;
@property (nonatomic, strong) NSString *subCategory;
@property (nonatomic, strong) NSString *termsOfUse;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *imgURL;
@property (nonatomic, assign) BOOL isOffer;

@end
