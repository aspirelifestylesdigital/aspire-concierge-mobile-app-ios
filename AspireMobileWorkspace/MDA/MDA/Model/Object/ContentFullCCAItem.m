//
//  ContentFullCCAItem.m
//  MobileConcierge
//
//  Created by Chung Mai on 6/5/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "ContentFullCCAItem.h"

@implementation ContentFullCCAItem

-(BOOL)isOffer {
    return ([self.offerText stringByReplacingOccurrencesOfString:@"<p>&nbsp;</p>" withString:@""].length > 0);
}

@end
