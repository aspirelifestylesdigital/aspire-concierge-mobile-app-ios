//
//  AppDelegate.m
//  AspireMobileDemo
//
//  Created by Chung Mai on 4/12/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "AppDelegate.h"
#import "MenuViewController.h"
#import "HomeViewController.h"
#import "UserObject.h"
#import "PrivacyPolicyViewController.h"
#import "Constant.h"
#import "GalleryViewController.h"
#import "SplashScreenViewController.h"
#import "NSTimer+Block.h"
#import "Common.h"
#import "UtilStyle.h"
#import "AppData.h"
#import "SessionData.h"
#import "LocationComponent.h"
#import "AppCoreData.h"
// views
#import "ViewLog.h"
//CCa service
#import "CCAService.h"
#import "CCAMapCategories.h"
#import "CCAGeographicManager.h"
#import "BaseTableViewCell.h"
#import <objc/runtime.h>
#import <Google/Analytics.h>
#import "WSPreferences.h"

@interface AppDelegate ()
{
    UserObject* loggedInUser;
    BOOL isAllowedUseLocation;
    ViewLog * vwLog;
    BOOL isMoreLaunch, isEnterBackground;
    
    NSArray* dataCustomLocation; // purpose for test
    UIViewController *blankViewController;
    WSPreferences *wsPreferences;
}
@end

@implementation AppDelegate
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
#define debug 1

- (CoreDataHelper*)cdh {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    if (!_coreDataHelper) {
        _coreDataHelper = [CoreDataHelper new];
        [_coreDataHelper setupCoreData];
    }
    return _coreDataHelper;
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    isMoreLaunch = YES;
    // start local Service
    [self startAllService];
    // Set white status
   // [application setStatusBarStyle:UIStatusBarStyleLightContent];
    //Begin Configure GA
    GAI *gai = [GAI sharedInstance];
    //For Testing
    //[gai trackerWithTrackingId:@"UA-109687104-1"];
    //For Production
    [gai trackerWithTrackingId:@"UA-109669524-1"];
    gai.trackUncaughtExceptions = YES;
    //  Remove navigation bar bottom line
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init]
                                      forBarPosition:UIBarPositionAny
                                          barMetrics:UIBarMetricsDefault];
    
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showLog:)
                                                 name:@"SHOW_LOG_APP"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateStatusLocation:)
                                                 name:LOCATION_COMPONENT_ISAUTHORIZED_NOTIFICATION
                                               object:nil];
    
    //start location
//    if ([AppData isCreatedProfile] && ![[SessionData shareSessiondata] hasForgotPassword]) {
//        isAllowedUseLocation = [[SessionData shareSessiondata] isUseLocation];
//        if(isAllowedUseLocation) {
//            [LocationComponent getLocation];
//        }
//    }
    
    NSDictionary *profileDictionary = [[SessionData shareSessiondata] getUserInfo];
    
    if (profileDictionary.count > 2) {
        wsPreferences = [[WSPreferences alloc] init];
        wsPreferences.delegate = nil;
        [wsPreferences getPreference];
//        getPreferencesFromAPI(nil);
        // start service get CCA
//        [CCAService startService];
    }


    [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(defaultsChanged:)
                                                     name:NSUserDefaultsDidChangeNotification
                                                   object:nil];

    
    //Set default color for menu
    self.currentCollorForMenu = [AppColor backgroundColor];
    // set root view controller
    UIWindow *window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window = window;
    
    /*
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    MenuViewController *menuViewController = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
    UINavigationController *menuNavigationViewController = [[UINavigationController alloc] initWithRootViewController:menuViewController];
    UIViewController *fontViewController = fontViewController = [storyboard instantiateViewControllerWithIdentifier:@"SplashViewController"];
    
    
    SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:menuNavigationViewController frontViewController:fontViewController];
    
    revealController.delegate = self;
    revealController.rearViewRevealWidth = SCREEN_WIDTH;
    revealController.rearViewRevealOverdraw = 0.0f;
    revealController.rearViewRevealDisplacement = 0.0f;
    self.viewController = revealController;*/
    
    UIViewController *fontViewController = [[SplashScreenViewController alloc] initWithNibName:@"SplashScreenViewController" bundle:nil];
    self.window.rootViewController = fontViewController; //self.viewController;
    [self.window makeKeyAndVisible];
    
    return YES;
}
- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(nullable UIWindow *)window{
    return UIInterfaceOrientationMaskPortrait;
}
#pragma mark - SERVICE
- (void) startAllService {
    
    dataCustomLocation = [[NSUserDefaults standardUserDefaults] objectForKey:@"TEST_LOCATION"];
    
    [AppCoreData startService];
    [CCAGeographicManager startService].onDone = ^{
        [CCAMapCategories shared].onDone = ^{
            [AppData startService];
        };
    };
}

- (void)defaultsChanged:(NSNotification *)notification {
    // Get the user defaults
    NSUserDefaults *defaults = (NSUserDefaults *)[notification object];
    if(!isMoreLaunch) {
        if(!dataCustomLocation && ![defaults objectForKey:@"TEST_LOCATION"])
            return;
        if(![dataCustomLocation isEqualToArray:[defaults objectForKey:@"TEST_LOCATION"]]) {
            
            [UIView transitionWithView:self.window
                              duration:0.5
                               options:UIViewAnimationOptionTransitionFlipFromLeft
                            animations:^{
                                UIViewController *fontViewController = [[SplashScreenViewController alloc] initWithNibName:@"SplashScreenViewController" bundle:nil];
                                self.window.rootViewController = fontViewController;
                                [self.window makeKeyAndVisible];
                            }
                            completion:^(BOOL isFinish){
                                if(isFinish)
                                    [self startAllService];
                            }];
            
        }
    }
    isMoreLaunch = NO;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    if (!blankViewController) {
        blankViewController = [UIViewController new];
        blankViewController.view.backgroundColor = [UIColor blackColor];
    }
    if (blankViewController.presentingViewController == nil && _preventShowMaskScreen != YES) {
        // Pass NO for the animated parameter. Any animation will not complete
        // before the snapshot is taken.
        [self.window.rootViewController presentViewController:blankViewController animated:NO completion:nil];
    }
    _preventShowMaskScreen = NO;
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
//    [[self cdh] saveContext];
    
    isEnterBackground = YES;
    
    
    // Your application can present a full screen modal view controller to
    // cover its contents when it moves into the background. If your
    // application requires a password unlock when it retuns to the
    // foreground, present your lock screen or authentication view controller here.
    
    if (!blankViewController) {
        blankViewController = [UIViewController new];
        blankViewController.view.backgroundColor = [UIColor blackColor];
        
    }
    if (blankViewController.presentingViewController == nil) {
        // Pass NO for the animated parameter. Any animation will not complete
        // before the snapshot is taken.
        [self.window.rootViewController presentViewController:blankViewController animated:NO completion:NULL];
    }
    [self trackingEventLeaveApp];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    // This should be omitted if your application presented a lock screen
    // in -applicationDidEnterBackground:
    if (blankViewController.presentingViewController != nil) {
        [self.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
    }
    [self trackingEventOpenApp];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    // start local Service
    if(isEnterBackground) {
        isEnterBackground = NO;
//        [self startAllService];
    }else{
        [self trackingEventOpenApp];
    }
    
    if (blankViewController.presentingViewController != nil) {
        
        [self.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
    }
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self saveContext];
}

-(void) saveAccessToken:(NSString*) accesToken refreshToken:(NSString*) refreshToken expired:(NSString*) expiredTime{
    if(expiredTime!=nil && expiredTime.length > 0){
        _B2C_ExpiredAt = [[NSDate date] dateByAddingTimeInterval:[expiredTime integerValue]];
    }
}

#pragma mark - LOG
- (void) showLog:(NSNotification *)notification {
    if([AppData isApplyNewFeatureWithKey:@"ViewLog"]) {
        [self createViewLog];
        NSUserDefaults *defaults = (NSUserDefaults *)[notification userInfo];
        if([defaults objectForKey:@"message"]) {
            [vwLog displayLog:[defaults objectForKey:@"message"]];
            [self hideLog:NO];
            return;
        }
        [self hideLog:YES];
    }
}

- (void) createViewLog {
    if(!vwLog) {
        vwLog = [ViewLog new];
        [[UIApplication sharedApplication].keyWindow addSubview:vwLog];
        vwLog.translatesAutoresizingMaskIntoConstraints = NO;
        [vwLog.superview addConstraint:[NSLayoutConstraint constraintWithItem:vwLog attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:vwLog.superview attribute:NSLayoutAttributeBottom multiplier:1 constant:0]];
        [vwLog.superview addConstraint:[NSLayoutConstraint constraintWithItem:vwLog attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:vwLog.superview attribute:NSLayoutAttributeTrailing multiplier:1 constant:0]];
        [vwLog.superview addConstraint:[NSLayoutConstraint constraintWithItem:vwLog attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:vwLog.superview attribute:NSLayoutAttributeLeading multiplier:1 constant:0]];
    }
}

- (void) hideLog:(BOOL) isHide {
    
    [UIView animateWithDuration:0.4 animations:^{
        vwLog.hidden = isHide;
    } completion:nil];
}
#pragma mark - CATCH TEXTFIELD INPUT
//- (void) textfieldDidChange:(NSNotification*) textField {
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"SHOW_LOG_APP" object:nil userInfo:@{@"message":[NSString stringWithFormat:@"%s: %@",__PRETTY_FUNCTION__,((UITextField*)[textField object]).text]}];
//}
#pragma mark - Location

-(void)updateStatusLocation:(NSNotification *)notification {
    NSDictionary* dictNoti = notification.userInfo;
    BOOL isLocation = [dictNoti boolForKey:LOCATION_COMPONENT_ISAUTHORIZED_NOTIFICATION];
    
    if (!isLocation) {
        
        if([[SessionData shareSessiondata] isShownLocationServiceAlert])
            return;
        if ([dictNoti intForKey:@"ERROR_CODE"] == 1 && ![[NSUserDefaults standardUserDefaults] boolForKey:@"IsRequestLocation"])
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Location Service Disabled", nil) message:NSLocalizedString(@"To enable, please go to Settings and turn on Location Service for this app.", nil) preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction*  action) {
            }];
            
            UIAlertAction *settingAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"SETTINGS", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction  *action) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            }];
            
            [alertController addAction:okAction];
            [alertController addAction:settingAction];
            alertController.preferredAction = settingAction;
            
            
            [[self getTopViewController] presentViewController:alertController animated:YES completion:^{
                [[SessionData shareSessiondata] setIsShownLocationServiceAlert:YES];
            }];
        }
        
    }
}
- (UIViewController *)getTopViewController {
    UIViewController *topViewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    while (topViewController.presentedViewController)
    {
        topViewController = topViewController.presentedViewController;
    }
    
    return topViewController;
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Location" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Location.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
//        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - Tracking

- (void)trackingEventOpenApp {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"User interactivity"
                                                          action:@"Open"
                                                           label:@"Open the app"
                                                           value:nil] build]];
}

- (void)trackingEventLeaveApp {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"User interactivity"
                                                          action:@"Leave"
                                                           label:@"Leave the app"
                                                           value:nil] build]];
}


@end
