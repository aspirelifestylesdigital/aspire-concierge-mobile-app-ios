//
//  WSB2CCreateNewConciergeCase.h
//  MobileConcierge
//
//  Created by user on 5/15/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSB2CBase.h"

@interface WSB2CCreateConciergeCase : WSB2CBase
-(void) askConciergeWithMessage:(NSDictionary*)dict;
@end
