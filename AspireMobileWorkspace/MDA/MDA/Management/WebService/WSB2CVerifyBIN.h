//
//  WSB2CVerifyBIN.h
//  MobileConcierge
//
//  Created by Chung Mai on 7/5/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSB2CBase.h"

@interface WSB2CVerifyBIN : WSB2CBase

@property(strong, nonatomic) NSString *bin;

-(void)verifyBIN;

@end
