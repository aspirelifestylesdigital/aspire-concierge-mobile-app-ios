//
//  WSB2CGetTiles.h
//  MobileConcierge
//
//  Created by Chung Mai on 6/6/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSB2CBase.h"

@interface WSB2CGetTiles : WSB2CBase

@property (nonatomic, strong) NSArray *categories;
@property (nonatomic, strong) NSString *searchText;

-(void) loadTilesForCategory;

@end
