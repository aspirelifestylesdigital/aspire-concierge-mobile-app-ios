//
//  WSSignIn.h
//  MobileConciergeUSDemo
//
//  Created by Viet Vo on 7/14/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSB2CBase.h"
#import "UserObject.h"

@interface WSSignIn : WSB2CBase <DataLoadDelegate>

@property (strong, nonatomic) NSMutableDictionary *user;
@property (strong, nonatomic) UserObject* userDetails;

-(void)signIn:(NSMutableDictionary*) userDict;

@end
