//
//  WSB2CPasscodeVerfication.m
//  MobileConciergeUSDemo
//
//  Created by Chung Mai on 10/19/17.
//  Copyright © 2017 Sunrise Software Solutions Corporation. All rights reserved.
//

#import "WSB2CPasscodeVerfication.h"
#import "BaseResponseObject.h"
#import "PasscodeResponseObject.h"

@interface WSB2CPasscodeVerfication()

@property (nonatomic, strong) NSDictionary *dataDict;

@end

@implementation WSB2CPasscodeVerfication

-(void) verifyPasscode:(NSDictionary *)infoDict
{
    self.task = WS_B2C_VERIFY_PASSCODE;
    _dataDict = infoDict;
    [self startAPIAfterAuthenticateWithAPI:CheckPasscode];
}

-(void)startAPIAfterAuthenticateWithAPI:(NSString*)apiStr{
    
    NSString* url = [B2C_API_URL stringByAppendingString:apiStr];
    [self POST:url withParams:[self buildRequestParams]];
    
}


-(NSMutableDictionary*) buildRequestParams{
    
    NSMutableDictionary* dictKeyValues = [[NSMutableDictionary alloc] init];
    [dictKeyValues setObject:B2C_SUBDOMAIN forKey:@"subDomain"];
    [dictKeyValues setObject:B2C_PW forKey:@"password"];
    [dictKeyValues setObject:[_dataDict objectForKey:keyPasscode] forKey:keyPasscode];
    return dictKeyValues;
}

-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    
    if(jsonResult) {
        PasscodeResponseObject *response = [[PasscodeResponseObject alloc]  initFromDict:jsonResult];
        if(self.delegate !=nil){
            if(response.isSuccess)
            {
                self.data = response.data;
               [self.delegate loadDataDoneFrom:self];
            }
            else
            {
                [self.delegate loadDataFailFrom:self withErrorCode:400 errorMessage:response.message];
            }
        }
        
    }
    else if(self.delegate != nil) {
        [self.delegate loadDataFailFrom:self withErrorCode:400 errorMessage:nil];
    }
}

-(void)processDataResultWithError:(NSError *)error
{
    [self.delegate loadDataFailFrom:self withErrorCode:400 errorMessage:nil];
}
@end
