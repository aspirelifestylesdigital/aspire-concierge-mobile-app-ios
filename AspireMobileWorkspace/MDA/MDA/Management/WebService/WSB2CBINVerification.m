//
//  WSB2CBINVerification.m
//  MobileConcierge
//
//  Created by Home on 5/6/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "WSB2CBINVerification.h"
#import "BaseResponseObject.h"
#import "EnumConstant.h"

@implementation WSB2CBINVerification

-(void)verifyBINNumber:(NSString *)binNumber
{
    [self startAPIAfterAuthenticate];
}

-(void)startAPIAfterAuthenticate{
    self.task = WS_CREATE_USER;
    self.subTask = WS_ST_NONE;
    
    
    NSString* url = [B2C_API_URL stringByAppendingString:@"Verifify BIn"];
    [self POST:url withParams:[self buildRequestParams]];
    
}
-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        BaseResponseObject *result = [[BaseResponseObject alloc] initFromDict:jsonResult];
        result.task = task;
        if(result.isSuccess){
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:result withErrorCode:result.status];
        }
        
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
    
}
-(void)processDataResultWithError:(NSError *)error
{
    [self.delegate loadDataFailFrom:nil withErrorCode:error.code];
}
#pragma mark - DataLoadDelegate

-(void)loadDataDoneFrom:(WSBase*)ws{
    [self.delegate loadDataDoneFrom:self];
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    result.task = self.task;
    [self.delegate loadDataFailFrom:result withErrorCode:result.status];
}

-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
    
    // cause of security scanning, not allow get and set password directly
    
    
    [dictKeyValues setObject:@"BIN" forKey:@"0000-00"];
    
    return dictKeyValues;
}

@end
