//
//  AppData.m
//  MobileConcierge
//
//  Created by Home on 5/7/17.
//  Copyright © 2017 s3Corp. All rights reserved.
//

#import "AppData.h"
#import <Foundation/Foundation.h>
#import "Constant.h"
#import "CityItem.h"
#import "CategoryItem.h"
#import "AnswerItem.h"

#import "CCAMapCategories.h"
#import "CCAGeographicManager.h"

@interface AppData()
{
    NSArray *cityItemList;
    NSArray *categoryItemList;
    NSArray *cityGuideList;
    NSArray *subCategoryItemList;
    NSArray *defaultAnswers;
    
    NSDictionary* listFeatures;
    BOOL validPasscode;
}

@end

static AppData *sharedMyManager;

@implementation AppData

+ (AppData *)getInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

+ (void)startService {
    if(!sharedMyManager) {
        [AppData getInstance];
    }
    printf("%s",[[NSString stringWithFormat:@"%s\n",__PRETTY_FUNCTION__] UTF8String]);
    [sharedMyManager createSelectionCityList];
    [sharedMyManager createCityGuideList];
}

- (instancetype) init {
    if(self = [super init]) {
        NSString * plistPath = [[NSBundle mainBundle] pathForResource:@"NewFeature" ofType:@"plist"];
        listFeatures = [NSDictionary dictionaryWithContentsOfFile:plistPath];
        [self createSelectionCategoryListForObject:self onDone:^(AppData* item,NSArray* list){
            item->categoryItemList = list;
        }];
        validPasscode = YES;
        
    }
    return self;
}

-(BOOL)isValidPasscode
{
    return validPasscode;
}

-(void)setValidPasscode:(BOOL)isValid
{
    validPasscode = isValid;
}

+ (NSArray *)getDefaultAnswers
{
    return sharedMyManager->defaultAnswers;
}

+(NSArray *)getSubCategoryItemList
{
    return sharedMyManager->subCategoryItemList;
}

+(NSArray *)getSelectionCityList
{
    for(CityItem *item in sharedMyManager->cityItemList)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cityCode == %@",item.name];
        NSArray *tempArray = [sharedMyManager->cityGuideList filteredArrayUsingPredicate:predicate];
        
        if(tempArray.count > 0)
        {
            item.ID = ((CityItem *)[tempArray objectAtIndex:0]).ID;
        }
        
    }
    
    return sharedMyManager-> cityItemList;
}

+ (NSArray *)getSelectionCategoryList
{
    return sharedMyManager->categoryItemList;
}

+(NSArray *)getCityGuideList
{
    return sharedMyManager->cityGuideList;
}


-(void)createCityGuideList
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSMutableArray *cityGuides = [[NSMutableArray alloc] init];
        //    CategoryItem *category = [[CategoryItem alloc] init];
        //    category.ID = @"";
        //    category.categoryName = @"Atlanta";
        //    category.code = @"Atlanta";
        //    category.categoryImg = [UIImage imageNamed:@"atlanta"];
        //    [cityGuides addObject:category];
        
        CityItem *city = [[CityItem alloc] init];
        city.ID = @"6311";
        city.name = @"Boston";
        city.cityCode = @"Boston";
        city.image = [UIImage imageNamed:@"boston"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6308";
        city.name = @"Chicago";
        city.cityCode = @"Chicago";
        city.image = [UIImage imageNamed:@"chicago"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6445";
        city.name = @"Dallas";
        city.cityCode = @"Dallas";
        city.image = [UIImage imageNamed:@"dallas"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6310";
        city.name = @"Las Vegas";
        city.cityCode = @"Las Vegas";
        city.image = [UIImage imageNamed:@"lasvegas"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6314";
        city.name = @"London";
        city.cityCode = @"London";
        city.image = [UIImage imageNamed:@"london"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6312";
        city.name = @"Los Ángeles";//@"Los Angeles";
        city.encodeCityName = @"Los A&#769;ngeles";
        city.cityCode = @"Los Angeles";
        city.image = [UIImage imageNamed:@"losangeles"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6307";
        city.name = @"Miami";
        city.cityCode = @"Miami";
        city.image = [UIImage imageNamed:@"miami"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6429";
        city.name = @"Montreal";
        city.cityCode = @"Montreal";
        city.image = [UIImage imageNamed:@"montreal"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6306";
        city.name = @"New York";
        city.cityCode = @"New York";
        city.image = [UIImage imageNamed:@"new_york"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6389";
        city.name = @"Orlando";
        city.cityCode = @"Orlando";
        city.image = [UIImage imageNamed:@"orlando"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6315";
        city.name = @"Paris";
        city.encodeCityName = @"Pari&#769;s";
        city.cityCode = @"Paris";
        city.image = [UIImage imageNamed:@"paris"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6458";
        city.name = @"Rome";
        city.cityCode = @"Rome";
        city.image = [UIImage imageNamed:@"rome"];
        [cityGuides addObject:city];
        
        //    category = [[CategoryItem alloc] init];
        //    category.ID = @"";
        //    category.categoryName = @"San Diego";
        //    category.code = @"San Diego";
        //    category.categoryImg = [UIImage imageNamed:@"sandiego"];
        //    [cityGuides addObject:category];
        
        city = [[CityItem alloc] init];
        city.ID = @"6313";
        city.name = @"San Francisco";
        city.cityCode = @"San Francisco";
        city.image = [UIImage imageNamed:@"sanfrancisco"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6444";
        city.name = @"Seattle";
        city.cityCode = @"Seattle";
        city.image = [UIImage imageNamed:@"seattle"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6428";
        city.name = @"Toronto";
        city.cityCode = @"Toronto";
        city.image = [UIImage imageNamed:@"toronto"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6459";
        city.name = @"Vancouver";
        city.cityCode = @"Vancouver";
        city.image = [UIImage imageNamed:@"vancouver"];
        [cityGuides addObject:city];
        
        city = [[CityItem alloc] init];
        city.ID = @"6309";
        city.name = @"Washington, D.C";
        city.cityCode = @"Washington, DC";
        city.image = [UIImage imageNamed:@"washington"];
        [cityGuides addObject:city];
        dispatch_async(dispatch_get_main_queue(), ^{
            printf("%s",[[NSString stringWithFormat:@"%s\n",__PRETTY_FUNCTION__] UTF8String]);
            cityGuideList = cityGuides;
        });
    });
}

-(void)createSelectionCategoryListForObject:(id)object onDone:(void(^)(id object,NSArray* list))onDone
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSMutableArray *categories = [[NSMutableArray alloc] init];
        
        CategoryItem *category = [[CategoryItem alloc] init];
        category.categoryName = @"";
        category.code = @"all";
        category.categoryImg = [UIImage imageNamed:@"all_category"];
        category.categoriesForService = LIST_ALL_TILES;
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.ID = @"2";
        category.categoryName = NSLocalizedString(@"DINING_category", nil);
        category.code = @"dining";
        category.categoryImg = [UIImage imageNamed:@"dining"];
        category.categoriesForService = @[@"Dining",@"Specialty Travel"];
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = NSLocalizedString(@"HOTELS", nil);
        category.code = @"hotels";
        category.categoryImg = [UIImage imageNamed:@"hotel"];
        category.categoriesForService = @[@"Hotels"];
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = NSLocalizedString(@"TRANSPORTATION", nil);
        category.code = @"transportation";
        category.categoryImg = [UIImage imageNamed:@"transportation"];
        category.categoriesForService = @[@"Transportation"];
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = NSLocalizedString(@"VACATION \nPACKAGES", nil);
        category.code = @"vacation packages";
        category.categoryImg = [UIImage imageNamed:@"vacation"];
        category.categoriesForService = @[@"Vacation Packages"];
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = NSLocalizedString(@"TOURS", nil);
        category.code = @"tours";
        category.categoryImg = [UIImage imageNamed:@"tour"];
        category.categoriesForService = @[@"VIP Travel Services"];
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = NSLocalizedString(@"ENTERTAINMENT", nil);
        category.code = @"entertainment";
        category.categoryImg = [UIImage imageNamed:@"entertainment"];
        category.categoriesForService = @[@"Tickets",@"Specialty Travel"];
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = NSLocalizedString(@"SPORT", nil);
        category.code = @"sport";
        category.ID = @"7";
        category.categoryImg = [UIImage imageNamed:@"sport"];
        category.categoriesForService = @[@"Golf",@"Specialty Travel"];
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = NSLocalizedString(@"SHOPPING", nil);
        category.code = @"shopping";
        category.categoryImg = [UIImage imageNamed:@"shopping"];
        category.categoriesForService = @[@"Golf",@"Flowers",@"Wine",@"Retail Shopping"];
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = NSLocalizedString(@"FLOWERS", nil);
        category.code = @"flowers";
        category.categoryImg = [UIImage imageNamed:@"flowers"];
        category.categoriesForService = @[@"Flowers"];
        [categories addObject:category];
        
        category = [[CategoryItem alloc] init];
        category.categoryName = NSLocalizedString(@"TRAVEL", nil);
        category.code = @"travel";
        category.categoryImg = [UIImage imageNamed:@"travel"];
        category.categoriesForService = @[@"Private Jet Travel",@"VIP Travel Services"];
        [categories addObject:category];
        
        /*
        category = [[CategoryItem alloc] init];
        category.categoryName = NSLocalizedString(@"GOLF", nil);
        category.code = @"golf";
        category.ID = @"7";
        category.categoryImg = [UIImage imageNamed:@"golf"];
        category.categoriesForService = @[@"Golf",@"Golf Merchandise",@"Specialty Travel"];
        [categories addObject:category];
         */
        
        /*
        category = [[CategoryItem alloc] init];
        category.categoryName = NSLocalizedString(@"CRUISE", nil);
        category.code = @"cruise";
        category.categoryImg = [UIImage imageNamed:@"cruise"];
        category.categoriesForService = @[@"Cruises"];
        [categories addObject:category];
         */
        
        /*
        category = [[CategoryItem alloc] init];
        category.ID = @"12";
        category.categoryName = NSLocalizedString(@"AIRPORT \nSERVICES", nil);
        category.code = @"airport services";
        category.categoryImg = [UIImage imageNamed:@"airport_service"];
        category.categoriesForService = @[@"VIP Travel Services"];
        [categories addObject:category];
         */
        
        /*
        category = [[CategoryItem alloc] init];
        category.categoryName = NSLocalizedString(@"TRAVEL \nSERVICES", nil);
        category.code = @"travel services";
        category.categoryImg = [UIImage imageNamed:@"travel_service"];
        category.categoriesForService = @[@"VIP Travel Services"];
        [categories addObject:category];
         */
        
        category = [[CategoryItem alloc] init];
        category.ID = @"5";
        category.categoryName = NSLocalizedString(@"CITY GUIDE", nil);
        category.code = @"city guide";
        category.categoryImg = [UIImage imageNamed:@"city_guide"];
        [categories addObject:category];
        dispatch_async(dispatch_get_main_queue(), ^{
            if(onDone)
                onDone(object,categories);
            //    categoryItemList = categories;
        });
    });
}

-(void)createSubCategoriesWithIDs:(NSArray *)categoryIDs forObject:(id)object onDone:(void (^)(id obj, NSArray* list)) onDone
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSMutableArray *subCategories = [[NSMutableArray alloc] init];
        for(NSInteger i = 0; i< categoryIDs.count; i++)
        {
            CategoryItem *item = [[CategoryItem alloc] init];
            item.ID = [categoryIDs objectAtIndex:i];
            item.supperCategoryID = @"80";
            switch (i) {
                 /*
                case 0:
                    item.categoryName = @"ACCOMMODATIONS";
                    item.code = @"accommodation";
                    item.categoryImg =  [UIImage imageNamed:@"asset_cityguide"];
                    item.categoryImgDetail =  [UIImage imageNamed:@"asset_cityguide_detail"];
                    break;
                  */
                    
                case 0:
                    item.categoryName = NSLocalizedString(@"DINING_category", nil);
                    item.code = @"dining";
                    item.categoryImg =  [UIImage imageNamed:@"dining_cityguide"];
                    item.categoryImgDetail =  [UIImage imageNamed:@"dining_cityguide_detail"];
                    break;
                case 1:
                    item.categoryName = NSLocalizedString(@"BARS/CLUBS", nil) ;
                    item.code = @"bars";
                    item.categoryImg =  [UIImage imageNamed:@"bar_club_cityguide"];
                    item.categoryImgDetail =  [UIImage imageNamed:@"bar_club_cityguide_detail"];
                    break;
                case 2:
                    item.categoryName = NSLocalizedString(@"SPAS", nil) ;
                    item.code = @"spas";
                    item.categoryImg =  [UIImage imageNamed:@"spa_wellness_cityguide"];
                    item.categoryImgDetail =  [UIImage imageNamed:@"spa_wellness_cityguide_detail"];
                    break;
                case 3:
                    item.categoryName = NSLocalizedString(@"SHOPPING", nil);
                    item.code = @"shopping";
                    item.categoryImg =  [UIImage imageNamed:@"shopping_cityguide"];
                    item.categoryImgDetail =  [UIImage imageNamed:@"shopping_cityguide_detail"];
                    break;
                case 4:
                    item.categoryName = NSLocalizedString(@"CULTURE", nil);
                    item.code = @"culture";
                    item.categoryImg =  [UIImage imageNamed:@"culture_cityguide"];
                    item.categoryImgDetail =  [UIImage imageNamed:@"culture_cityguide_detail"];
                    break;
                    
                default:
                    break;
            }
            [subCategories addObject:item];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if(onDone)
                onDone(object,subCategories);
        });
    });
}

-(void) createSelectionCityList
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSMutableArray *cityLst = [[NSMutableArray alloc] init];
        
        CityItem*city = [[CityItem alloc] init];
        city.name = @"Boston";
        city.cityCode = @"Boston";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.countryCode = @"Estados Unidos";
        city.image = [UIImage imageNamed:@"boston"];
        city.diningID = @"6958";
        NSArray *subCategoryId = @[@"7119",@"7120",@"7121",@"7122",@"7123"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
//        city = [[CityItem alloc] init];
//        city.name = @"Acapulco";
//        city.cityCode = @"Acapulco";
//        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
//        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
//        city.countryCode = @"Mexico";
//        city.image = [UIImage imageNamed:@"acapulco"];
//        city.diningID = @"6785";
//        subCategoryId = @[/*@"6478",*/@"6786",@"6787",@"6788",@"6789",@"6790"];
//        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
//        [cityLst addObject:city];
        
//        city = [[CityItem alloc] init];
//        city.name = @"Aguascalientes";
//        city.cityCode = @"Aguascalientes";
//        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
//        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
//        city.countryCode = @"Mexico";
//        city.image = [UIImage imageNamed:@"aguascalientes"];
//        city.diningID = @"6791";
//        subCategoryId = @[/*@"6478",*/@"6792",@"6793",@"6794",@"6795",@"6796"];
//        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
//        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Cancún";
        city.encodeCityName = @"Cancu&#769;n";
        city.cityCode = @"Cancun";
        city.stateCode = @"Quintana Roo";
        city.countryCode = @"México";
        city.image = [UIImage imageNamed:@"cancun"];
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.diningID = @"6797";
        subCategoryId = @[/*@"6478",*/@"7077",@"7078",@"7079",@"7081",@"7080"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Chicago";
        city.cityCode = @"Chicago";
//        city.stateCode = @"Illinois";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.countryCode = @"Estados Unidos";
        city.image = [UIImage imageNamed:@"chicago"];
        city.diningID = @"6847";
        subCategoryId = @[@"7124",@"7125",@"7126",@"7127",@"7128"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        
        city = [[CityItem alloc] init];
        city.name = @"Guadalajara";
        city.cityCode = @"Guadalajara";
        city.stateCode = @"Jalisco";
        city.countryCode = @"México";
        city.image = [UIImage imageNamed:@"guadalajara"];
        city.diningID = @"6803";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        subCategoryId = @[/*@"6478",*/@"7082",@"7083",@"7084",@"7085",@"7086"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
//        city = [[CityItem alloc] init];
//        city.name = @"Guanajuato";
//        city.cityCode = @"Guanajuato";
//        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
//        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
//        city.countryCode = @"Mexico";
//        city.image = [UIImage imageNamed:@"guanajuato"];
//        city.diningID = @"6809";
//        subCategoryId = @[/*@"6478",*/@"6810",@"6811",@"6812",@"6813",@"6814"];
//        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
//        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Las Vegas";
        city.cityCode = @"Las Vegas";
//        city.stateCode = @"Nevada";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.countryCode = @"Estados Unidos";
        city.image = [UIImage imageNamed:@"lasvegas"];
        city.diningID = @"6898";
        subCategoryId = @[/*@"6478",*/@"6984",@"6985",@"6986",@"6987",@"6988"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        
        city = [[CityItem alloc] init];
        city.name = @"Londres";
        city.cityCode = @"London";
        city.countryCode = @"Reino Unido";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.image = [UIImage imageNamed:@"london"];
        city.diningID = @"6899";
//        subCategoryId = @[/*@"6479",*/@"6913",@"6914",@"6915",@"6916",@"6917"];
//        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Los Ángeles"; //@"Los Angeles";
        city.cityCode = @"Los Angeles";
        city.encodeCityName = @"Los A&#769;ngeles";
//        city.stateCode = @"California";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.countryCode = @"Estados Unidos";
        city.image = [UIImage imageNamed:@"losangeles"];
        city.diningID = @"6900";
        subCategoryId = @[@"7134",@"7135",@"7136",@"7137",@"7138"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Los Cabos";
        city.stateCode = @"Baja California Sur";
        city.cityCode = @"Los Cabos";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.countryCode = @"México";
        city.image = [UIImage imageNamed:@"los"];
        city.diningID = @"6815";
        subCategoryId = @[/*@"6478",*/@"6996",@"6997",@"6998",@"6999",@"7000"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
//        city = [[CityItem alloc] init];
//        city.name = @"Mazatlan";
//        city.cityCode = @"Mazatlan";
//        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
//        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
//        city.countryCode = @"Mexico";
//        city.image = [UIImage imageNamed:@"mazatlan"];
//        city.diningID = @"6821";
//        subCategoryId = @[/*@"6478",*/@"6822",@"6823",@"6824",@"6825",@"6826"];
//        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
//        [cityLst addObject:city];
        
//        city = [[CityItem alloc] init];
//        city.name = @"Merida";
//        city.cityCode = @"Merida";
//        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
//        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
//        city.countryCode = @"Mexico";
//        city.image = [UIImage imageNamed:@"merida"];
//        city.diningID = @"6827";
//        subCategoryId = @[/*@"6478",*/@"6828",@"6829",@"6830",@"6831",@"6832"];
//        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
//        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Ciudad de México";
        city.encodeCityName = @"Ciudad de Me&#769;xico";
        city.cityCode = @"Mexico City";
        city.stateCode = @"Ciudad de México";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.countryCode = @"México";
        city.image = [UIImage imageNamed:@"mexico"];
        city.diningID = @"6833";
        subCategoryId = @[/*@"6478",*/@"7087",@"7088",@"7089",@"7090",@"7091"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Miami";
        city.cityCode = @"Miami";
//        city.stateCode = @"Florida";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.countryCode = @"Estados Unidos";
        city.image = [UIImage imageNamed:@"miami"];
        city.diningID = @"6901";
        subCategoryId = @[@"7139",@"7140",@"7141",@"7142",@"7143"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Monterrey";
        city.cityCode = @"Monterrey";
        city.stateCode = @"Nuevo León";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.countryCode = @"México";
        city.image = [UIImage imageNamed:@"monterrey"];
        city.diningID = @"6839";
        subCategoryId = @[/*@"6478",*/@"7092",@"7093",@"7094",@"7095",@"7096"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        

        city = [[CityItem alloc] init];
        city.name = @"New York";
        city.cityCode = @"New York";
//        city.stateCode = @"New York";
        city.countryCode = @"Estados Unidos";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.image = [UIImage imageNamed:@"new_york"];
        city.diningID = @"6902";
        subCategoryId = @[@"7144",@"7145",@"7146",@"7147",@"7148"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
//        city = [[CityItem alloc] init];
//        city.name = @"Oaxaca";
//        city.cityCode = @"Oaxaca";
//        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
//        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
//        city.countryCode = @"Mexico";
//        city.image = [UIImage imageNamed:@"oaxaca"];
//        city.diningID = @"6840";
//        subCategoryId = @[/*@"6478",*/@"6858",@"6859",@"6860",@"6861",@"6862"];
//        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
//        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Orlando";
        city.cityCode = @"Orlando";
//        city.stateCode = @"Florida";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.countryCode = @"Estados Unidos";
        city.image = [UIImage imageNamed:@"orlando"];
        city.diningID = @"6903";
        subCategoryId = @[@"7149",@"7150",@"7151",@"7152",@"7153"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"París";
        city.encodeCityName = @"Pari&#769;s";
        city.cityCode = @"Paris";
        city.countryCode = @"Francia";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.image = [UIImage imageNamed:@"paris"];
        city.diningID = @"6904";
//        subCategoryId = @[/*@"6479",*/@"6938",@"6939",@"6940",@"6941",@"6942"];
//        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
        [cityLst addObject:city];

//        city = [[CityItem alloc] init];
//        city.name = @"Playa del Carmen";
//        city.cityCode = @"Playa del Carmen";
//        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
//        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
//        city.countryCode = @"Mexico";
//        city.image = [UIImage imageNamed:@"playa"];
//        city.diningID = @"6841";
//        subCategoryId = @[/*@"6478",*/@"6863",@"6864",@"6865",@"6866",@"6867"];
//        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
//        [cityLst addObject:city];
        
//        city = [[CityItem alloc] init];
//        city.name = @"Puebla";
//        city.cityCode = @"Puebla";
//        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
//        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
//        city.countryCode = @"Mexico";
//        city.image = [UIImage imageNamed:@"puebla"];
//        city.diningID = @"6842";
//        subCategoryId = @[/*@"6478",*/@"6868",@"6869",@"6870",@"6871",@"6872"];
//        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
//        [cityLst addObject:city];
        
//        city = [[CityItem alloc] init];
//        city.name = @"Puerto Vallarta";
//        city.cityCode = @"Puerto Vallarta";
//        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
//        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
//        city.countryCode = @"Mexico";
//        city.image = [UIImage imageNamed:@"puerto"];
//        city.diningID = @"6843";
//        subCategoryId = @[/*@"6478",*/@"6873",@"6874",@"6875",@"6876",@"6877"];
//        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
//        [cityLst addObject:city];
        
//        city = [[CityItem alloc] init];
//        city.name = @"Queretaro";
//        city.cityCode = @"Queretaro";
//        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
//        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
//        city.countryCode = @"Mexico";
//        city.image = [UIImage imageNamed:@"queretaro"];
//        city.diningID = @"6844";
//        subCategoryId = @[/*@"6478",*/@"6878",@"6879",@"6880",@"6881",@"6882"];
//        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
//        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Roma";
        city.cityCode = @"Rome";
        city.countryCode = @"Italia";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.image = [UIImage imageNamed:@"rome"];
        city.diningID = @"6905";
//        subCategoryId = @[/*@"6479",*/@"6943",@"6944",@"6945",@"6946",@"6947"];
//        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
        [cityLst addObject:city];
    
        city = [[CityItem alloc] init];
        city.name = @"San Francisco";
        city.cityCode = @"San Francisco";
//        city.stateCode = @"California";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.cityCode];
        city.countryCode = @"Estados Unidos";
        city.image = [UIImage imageNamed:@"sanfrancisco"];
        city.diningID = @"6906";
        subCategoryId = @[@"7164",@"7165",@"7166",@"7167",@"7168"];
        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
            obj.subCategories = list;
        }];
        [cityLst addObject:city];
        
//        city = [[CityItem alloc] init];
//        city.name = @"Tijuana";
//        city.cityCode = @"Tijuana";
//        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
//        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
//        city.countryCode = @"Mexico";
//        city.image = [UIImage imageNamed:@"tijuana"];
//        city.diningID = @"6845";
//        subCategoryId = @[/*@"6478",*/@"6883",@"6884",@"6885",@"6886",@"6887"];
//        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
//        [cityLst addObject:city];
        
//        city = [[CityItem alloc] init];
//        city.name = @"Veracruz";
//        city.cityCode = @"Veracruz";
//        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
//        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
//        city.countryCode = @"Mexico";
//        city.image = [UIImage imageNamed:@"veracruz"];
//        city.diningID = @"6846";
//        subCategoryId = @[/*@"6478",*/@"6888",@"6889",@"6890",@"6891",@"6892"];
//        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
//        [cityLst addObject:city];
        
//        city = [[CityItem alloc] init];
//        city.name = @"Washington, D.C";
//        city.cityCode = @"Washington DC";
//        city.countryCode = @"United States";
//        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.cityCode];
//        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.cityCode];
//        city.image = [UIImage imageNamed:@"washington"];
//        city.diningID = @"6907";
//        subCategoryId = @[/*@"6478",*/@"6953",@"6954",@"6955",@"6956",@"6957"];
//        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
//        [cityLst addObject:city];
        
        NSArray *sortedArray = [cityLst sortedArrayUsingComparator:^NSComparisonResult(CityItem *obj1, CityItem *obj2){ return [obj1.name compare:obj2.name];}];
        
        [cityLst removeAllObjects];
        [cityLst addObjectsFromArray:sortedArray];
        
        city = [[CityItem alloc] init];
        city.name = @"África / Medio Oriente";
        city.encodeCityName = @"A&#769;frica / Medio Oriente";
        city.regionCode = @"Africa";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.regionCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.regionCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.regionCode];
        city.image = [UIImage imageNamed:@"africa"];
        city.diningID = @"7098";
//        [self createSubCategoriesWithIDs:@[@"6482"] forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"América del Sur y Central";
        city.encodeCityName = @"Ame&#769;rica del sur y central";
        city.regionCode = @"Latin America";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.regionCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.regionCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.regionCode];
        city.image = [UIImage imageNamed:@"latin_america"];
        city.diningID = @"7108";
        //        [self createSubCategoriesWithIDs:@[@"6481"] forObject:city onDone:^(CityItem* obj,NSArray* list){
        //            obj.subCategories = list;
        //        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Asia Pacífico";
        city.encodeCityName = @"Asia Pacifico";
        city.regionCode = @"Asia Pacific";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.regionCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.regionCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.regionCode];
        city.image = [UIImage imageNamed:@"asia_pacific"];
        city.diningID = @"6967";
        //subCategoryId = @[/*@"6478",*/@"6968",@"6969",@"6970",@"6971",@"6972"];
//        [self createSubCategoriesWithIDs:subCategoryId forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Canadá";
        city.encodeCityName = @"Canada&#769;";
        city.regionCode = @"Canada";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.regionCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.regionCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.regionCode];
        city.image = [UIImage imageNamed:@"canada"];
        city.diningID = @"7101";
        //    city.subCategories = [self createSubCategoriesWithIDs:@[@"6480"]];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Caribe";
        city.regionCode = @"Caribbean";
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.regionCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.regionCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.regionCode];
        city.image = [UIImage imageNamed:@"caribbean"];
        city.diningID = @"7105";
//        [self createSubCategoriesWithIDs:@[@"6542"] forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Estados Unidos";
        city.regionCode = @"United States";
        city.image = [UIImage imageNamed:@"united"];
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.regionCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.regionCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.regionCode];
        city.diningID = @"7103";
        //        [self createSubCategoriesWithIDs:@[@"6479"] forObject:city onDone:^(CityItem* obj,NSArray* list){
        //            obj.subCategories = list;
        //        }];
        [cityLst addObject:city];
        
        city = [[CityItem alloc] init];
        city.name = @"Europa";
        city.regionCode = @"Europe";
        city.image = [UIImage imageNamed:@"europe"];
        city.GeographicRegion = [CCAGeographicManager getGeographicForCity:city.regionCode];
        city.SubCategory = [CCAGeographicManager getSubcategoryForCity:city.regionCode];
        city.ExceptGeographicRegion = [CCAGeographicManager getExceptionGeographicForCity:city.regionCode];
        city.diningID = @"7100";
//        [self createSubCategoriesWithIDs:@[@"6479"] forObject:city onDone:^(CityItem* obj,NSArray* list){
//            obj.subCategories = list;
//        }];
        [cityLst addObject:city];
        
        //    CityItem *allCity = [[CityItem alloc] init];
        //    allCity.name = @"All";
        //    allCity.regionCode = @"all";
        //    allCity.image = [UIImage imageNamed:@"all_cityselection"];
        
        //    NSMutableArray *allItems = [[NSMutableArray alloc] init];
        //    [allItems addObject:allCity];
        //    [allItems addObjectsFromArray:sortedArray];
        dispatch_async(dispatch_get_main_queue(), ^{
            printf("%s",[[NSString stringWithFormat:@"%s\n",__PRETTY_FUNCTION__] UTF8String]);
            cityItemList = cityLst;
        });
    });
}
+(NSDictionary *)getMenuItems
{
    return @{@"0":@[NSLocalizedString(@"HOME", nil),
                    NSLocalizedString(@"ASK THE CONCIERGE", nil),
                    NSLocalizedString(@"EXPLORE", nil)],
             @"1":@[NSLocalizedString(@"MY PROFILE", nil),
                    NSLocalizedString(@"PREFERENCES", nil),
                    NSLocalizedString(@"CHANGE PASSWORD", nil)
                    ],
             @"2":@[NSLocalizedString(@"ABOUT THIS APP", nil),
                    NSLocalizedString(@"PRIVACY POLICY", nil),
                    NSLocalizedString(@"TERMS OF USE", nil),
//                    NSLocalizedString(@"GENERATE FAKE BIN", nil),
//                    NSLocalizedString(@"SIGN OUT", nil)],
                    NSLocalizedString(@"SIGN OUT", nil),
//                    NSLocalizedString(@"Invalid Passcode", nil)
                    ],
             };
    
}

+(void)storedTimeAcceptedPolicy
{
    [[NSUserDefaults standardUserDefaults] setDouble:[[NSDate date] timeIntervalSince1970] forKey:@"TimeAcceptedPolicy"];
    [[NSUserDefaults standardUserDefaults]  synchronize];
}

+(void)setCreatedProfileSuccessful
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ProfileCreatedSuccessful"];
    [[NSUserDefaults standardUserDefaults]  synchronize];
}


+ (BOOL) isCreatedProfile
{
    return ([[NSUserDefaults standardUserDefaults] dictionaryForKey:keyProfile] != nil);
}

+(NSTimeInterval)getTimeAcceptedPolicy
{
    return [[NSUserDefaults standardUserDefaults] doubleForKey:@"TimeAcceptedPolicy"];
}

+(BOOL)isAcceptedPolicyOverSixMonths
{
    return (([[NSDate date] timeIntervalSince1970] - [AppData getTimeAcceptedPolicy]) > SIX_MONTH_SECONDS_TIME);
}


+(void)setCurrentCityWithCode:(NSString *)cityName
{
    [[NSUserDefaults standardUserDefaults] setValue:cityName forKey:CURRENT_CITY_CODE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(BOOL)isCurrentCity
{
    if([[NSUserDefaults standardUserDefaults] valueForKey:CURRENT_CITY_CODE])
    {
        return YES;
    }
    return NO;
}

+(CityItem *)getSelectedCity
{
    NSString *currentCityCode = [[NSUserDefaults standardUserDefaults] valueForKey:CURRENT_CITY_CODE];
    if(currentCityCode)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@",currentCityCode];
        NSArray *cities = sharedMyManager-> cityItemList;
        NSArray *tempArray = [cities filteredArrayUsingPredicate:predicate];
        if(tempArray.count > 0)
        {
            return [tempArray objectAtIndex:0];
        }
    }
    return nil;
}

+ (BOOL) isApplyFeatureWithKey:(NSString *)key {
    BOOL result = NO;
    if(!sharedMyManager)
        return result;
    if([[sharedMyManager->listFeatures allKeys] containsObject:key]) {
        result = [[sharedMyManager->listFeatures objectForKey:key] boolValue];
    }
    return result;
}

+ (void)setUserCityLocationDevice:(NSString *)city {
    if(city.length > 0)
        [[NSUserDefaults standardUserDefaults] setValue:city forKey:USER_CITY_LOCATION_DEVICE];
    else
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_CITY_LOCATION_DEVICE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)getUserCityLocationDevice {
    return [[NSUserDefaults standardUserDefaults] valueForKey:USER_CITY_LOCATION_DEVICE];
}

+ (BOOL) isApplyNewFeatureWithKey:(NSString*)keyFeature {
//    !!!: Apply New Feature
    return [AppData isApplyFeatureWithKey:keyFeature];
}

+(NSDictionary *)getPreferencesItems {
    return @{
             @"DINING":@[@"None",
                         @"African",
                         @"American",
                         @"Arabic",
                         @"Argentinian",
                         @"Asian",
                         @"Australian",
                         @"Austrian",
                         @"Brazilian",
                         @"British",
                         @"Cajun/Creole",
                         @"Californian",
                         @"Canadian",
                         @"Cantonese",
                         @"Caribbean",
                         @"Chicken",
                         @"Chinese",
                         @"Classic Italian/Traditional",
                         @"Cuban",
                         @"Dutch",
                         @"European",
                         @"French",
                         @"German",
                         @"Greek",
                         @"Hawaii Regional",
                         @"Indian",
                         @"Indonesian",
                         @"International/Global",
                         @"Italian",
                         @"Jamaican",
                         @"Japanese",
                         @"Korean",
                         @"Latin",
                         @"Lebanese",
                         @"Mediterranean",
                         @"Mexican",
                         @"Middle Eastern",
                         @"Modern/Contempo American",
                         @"Modern/Contempo French",
                         @"Modern/Contempo German",
                         @"Modern/Contempo Italian",
                         @"Modern/Contempo Mexican",
                         @"Moroccan",
                         @"Persian",
                         @"Peruvian",
                         @"Pizza",
                         @"Polish",
                         @"Roman",
                         @"Rortuguese",
                         @"Russian",
                         @"Scandinavian",
                         @"Seafood",
                         @"Singaporean",
                         @"South American",
                         @"Southern American",
                         @"Spanish",
                         @"Steak/Steakhouse",
                         @"Sushi",
                         @"Swedish",
                         @"Swiss",
                         @"Szechuan",
                         @"Taiwanese",
                         @"Thai",
                         @"Turkish",
                         @"Tuscan",
                         @"Venetian",
                         @"Vietnamese"],
             @"HOTEL":@[@"None",
                        @"1 Star",
                        @"2 Stars",
                        @"3 Stars",
                        @"4 Stars",
                        @"5 Stars"],
             @"TRANSPORTATION":@[@"None",
                                 @"Luxury Sedan",
                                 @"Standard (Sedan)"],
             };
    
}

+(NSDictionary *)getPreferencesItemsLocalizable {
    return @{
             @"DINING":@[NSLocalizedString(@"None", nil),
                         NSLocalizedString(@"African", nil),
                         NSLocalizedString(@"American", nil),
                         NSLocalizedString(@"Arabic", nil),
                         NSLocalizedString(@"Argentinian", nil),
                         NSLocalizedString(@"Asian", nil),
                         NSLocalizedString(@"Australian", nil),
                         NSLocalizedString(@"Austrian", nil),
                         NSLocalizedString(@"Brazilian", nil),
                         NSLocalizedString(@"British", nil),
                         NSLocalizedString(@"Cajun/Creole", nil),
                         NSLocalizedString(@"Californian", nil),
                         NSLocalizedString(@"Canadian", nil),
                         NSLocalizedString(@"Cantonese", nil),
                         NSLocalizedString(@"Caribbean", nil),
                         NSLocalizedString(@"Chicken", nil),
                         NSLocalizedString(@"Chinese", nil),
                         NSLocalizedString(@"Classic Italian/Traditional", nil),
                         NSLocalizedString(@"Cuban", nil),
                         NSLocalizedString(@"Dutch", nil),
                         NSLocalizedString(@"European", nil),
                         NSLocalizedString(@"French", nil),
                         NSLocalizedString(@"German", nil),
                         NSLocalizedString(@"Greek", nil),
                         NSLocalizedString(@"Hawaii Regional", nil),
                         NSLocalizedString(@"Indian", nil),
                         NSLocalizedString(@"Indonesian", nil),
                         NSLocalizedString(@"International/Global", nil),
                         NSLocalizedString(@"Italian", nil),
                         NSLocalizedString(@"Jamaican", nil),
                         NSLocalizedString(@"Japanese", nil),
                         NSLocalizedString(@"Korean", nil),
                         NSLocalizedString(@"Latin", nil),
                         NSLocalizedString(@"Lebanese", nil),
                         NSLocalizedString(@"Mediterranean", nil),
                         NSLocalizedString(@"Mexican", nil),
                         NSLocalizedString(@"Middle Eastern", nil),
                         NSLocalizedString(@"Modern/Contempo American", nil),
                         NSLocalizedString(@"Modern/Contempo French", nil),
                         NSLocalizedString(@"Modern/Contempo German", nil),
                         NSLocalizedString(@"Modern/Contempo Italian", nil),
                         NSLocalizedString(@"Modern/Contempo Mexican", nil),
                         NSLocalizedString(@"Moroccan", nil),
                         NSLocalizedString(@"Persian", nil),
                         NSLocalizedString(@"Peruvian", nil),
                         NSLocalizedString(@"Pizza", nil),
                         NSLocalizedString(@"Polish", nil),
                         NSLocalizedString(@"Roman", nil),
                         NSLocalizedString(@"Rortuguese", nil),
                         NSLocalizedString(@"Russian", nil),
                         NSLocalizedString(@"Scandinavian", nil),
                         NSLocalizedString(@"Seafood", nil),
                         NSLocalizedString(@"Singaporean", nil),
                         NSLocalizedString(@"South American", nil),
                         NSLocalizedString(@"Southern American", nil),
                         NSLocalizedString(@"Spanish", nil),
                         NSLocalizedString(@"Steak/Steakhouse", nil),
                         NSLocalizedString(@"Sushi", nil),
                         NSLocalizedString(@"Swedish", nil),
                         NSLocalizedString(@"Swiss", nil),
                         NSLocalizedString(@"Szechuan", nil),
                         NSLocalizedString(@"Taiwanese", nil),
                         NSLocalizedString(@"Thai", nil),
                         NSLocalizedString(@"Turkish", nil),
                         NSLocalizedString(@"Tuscan", nil),
                         NSLocalizedString(@"Venetian", nil),
                         NSLocalizedString(@"Vietnamese", nil)],
             @"HOTEL":@[NSLocalizedString(@"None", nil), NSLocalizedString(@"1 Star", nil),
                        NSLocalizedString(@"2 Stars", nil),
                        NSLocalizedString(@"3 Stars", nil),
                        NSLocalizedString(@"4 Stars", nil),
                        NSLocalizedString(@"5 Stars", nil)],
             @"TRANSPORTATION":@[NSLocalizedString(@"None", nil),
                                 NSLocalizedString(@"Luxury Sedan", nil),
                                 NSLocalizedString(@"Standard (Sedan)", nil),],
             };
}

+ (BOOL)shouldGetDistanceForListDiningFromCity:(CityItem *)city {
    BOOL have = NO;
    NSString* deviceCity = [[AppData getUserCityLocationDevice] lowercaseString];
    NSString* selectCity = [city.name lowercaseString];
    have = [deviceCity containsString:selectCity];
    if([LIST_REGIONS containsObject:city.regionCode]) {
        have = YES;
    }
    return have;
}
@end
