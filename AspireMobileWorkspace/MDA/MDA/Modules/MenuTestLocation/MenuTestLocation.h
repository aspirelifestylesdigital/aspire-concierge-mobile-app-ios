//
//  MenuTestLocation.h
//  MobileConciergeUSDemo
//
//  Created by Dai Pham on 8/2/17.
//  Copyright © 2017 Sunrixe Software Solutions Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MenuTestLocation;

@protocol MenuTestLocationProtocol <NSObject>

@optional
- (void) MenuTestLocation:(MenuTestLocation*)view onSelectEvent:(NSUInteger)index withUserInfo:(id)userInfo;

@end

@interface MenuTestLocation : UIView
@property (weak,nonatomic) id<MenuTestLocationProtocol> delegate;

- (void) setData:(NSArray<NSString*>*) data;
- (void) setTitleForButtons:(NSArray<NSString*>*)titles;
- (void) clear;
- (void) hideKeyboard;
@end
